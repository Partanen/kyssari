#pragma once
#include "../core/types.h"

typedef struct Camera Camera;

struct Camera
{
    vec3    position;
    vec3    front;
    vec3    up;
    float   fov;    /* In radians */
    float   pitch;  /* In radians */
    float   yaw;
    float   roll;
    float   near_clip, far_clip;
    mat4x4  mat_look_at;
};

void
Camera_init(Camera *camera);

void
Camera_moveForward(Camera *camera, float speed);

void
Camera_moveBack(Camera *camera, float speed);

void
Camera_moveUp(Camera *camera, float speed);

void
Camera_moveDown(Camera *camera, float speed);

void
Camera_strafeLeft(Camera *camera, float speed);

void
Camera_strafeRight(Camera *camera, float speed);

void
Camera_turn(Camera *camera, float x, float y, float speed);

void
Camera_lookAt(Camera *camera, vec3 target);

mat4x4 *
Camera_updateAndGetViewMatrix(Camera *camera);

void
Camera_getForwardVector(Camera *camera);

/* Roll does nothing for the moment */
void
Camera_setRotation(Camera *camera, float yaw, float pitch, float roll);
