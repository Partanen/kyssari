#version 330 core

layout (std140) uniform Shader3DSceneAmbientLighting
{
    vec4 scene_ambient_light;
};

in vec3             tex_coord;
out vec4            frag_color;

uniform samplerCube tex;

void main(void)
{
    frag_color = vec4(vec3(scene_ambient_light.x,
        scene_ambient_light.y,scene_ambient_light.z), 1.0f) \
        * texture(tex, tex_coord);
};
