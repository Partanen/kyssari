static void (*reconciliation_func)(Entity*,input_sequence_t,vec3,vec3) =
    mp_reconcileInputs2;

static float _recon_diff_mul    = 0.350f;
static float _recon_max_sq_diff = 0.200f;

static void
_mp_updateScene(Entity *pawn, Transform *t, PhysicsComponent *pc, double dt);

void
mp_updateAndBufferPlayerInputs(Entity *pawn, double dt)
{
    /* Note: is dt even needed? */
    kb_state_t kb_state = 0;

    ScriptComponent     *sc = Entity_getScriptComponent(pawn);
    PlayerScriptData    *pd = (PlayerScriptData*)sc->data;
    Transform           *t  = &Entity_getTransform(pawn)->transform;
    ByteBuffer          byte_buffer;
    ModelComponent *mc = Entity_getModelComponent(pawn);
    RenderModel *weapon_rm = ModelComponent_getRenderModelById(mc,
        PLAYER_RENDER_MODEL_ID_WEAPON);

    bool32 walking = 0;

    if (isRelativeMouseMode() && mouseMovedThisFrame())
    {
        turnPlayerEntity(pawn, getMouseRelativeMovement()[0],
            getMouseRelativeMovement()[1]);
    }

    if (isKeyPressed(key_config.move_forward))
    {
        SET_BITFLAG_ON(kb_state, kb_state_t, KS_KEY_FORWARD);
        walking = 1;
    }

    if (isKeyPressed(key_config.move_left))
    {
        SET_BITFLAG_ON(kb_state, kb_state_t, KS_KEY_LEFT);
        walking = 1;
    }

    if (isKeyPressed(key_config.move_right))
    {
        SET_BITFLAG_ON(kb_state, kb_state_t, KS_KEY_RIGHT);
        walking = 1;
    }

    if (isKeyPressed(key_config.move_back))
    {
        walking = 1;
        SET_BITFLAG_ON(kb_state, kb_state_t, KS_KEY_BACK);
    }

    /* Play the walking sound */
    if (walking && pd->base.is_grounded
    && !isSoundEffectChannelPlaying(SOUND_CHANNEL_FOOTSTEPS))
    {
        SoundEffect_play(permanent_assets.se_steps,
            SOUND_CHANNEL_FOOTSTEPS, 0);
    }

    if (isKeyPressed(key_config.jump))
        SET_BITFLAG_ON(kb_state, kb_state_t, KS_KEY_JUMP);

    enum KsWeaponType old_weapon = pd->weapon;

    if (isKeyPressed(key_config.weaponswap_next))
    {
        if (!pd->base.weaponswap_pressed && pd->animation_timer <= 0.0f)
        {
            pd->base.weaponswap_pressed = 1;
            mp_playerWeaponSwapNext(pawn);
        }
    }
    else if (isKeyPressed(key_config.weaponswap_prev))
    {
        if (!pd->base.weaponswap_pressed && pd->animation_timer <= 0.0f)
        {
            pd->base.weaponswap_pressed = 1;
            mp_playerWeaponSwapPrev(pawn);
        }
    }
    else
    {
        pd->base.weaponswap_pressed = 0;
    }

    if (isKeyPressed(key_config.toggle) && !pd->base.f_pressed)
    {
        pd->base.f_pressed = 1;

        if (!pd->base.f_toggled)
            pd->base.f_toggled = 1;
        else
            pd->base.f_toggled = 0;

        /* For SMG, the visual needs to be updated */
        if (pd->weapon == SMG)
            setPlayerPawnVisualWeaponModel(weapon_rm, SMG, pd);
    }
    else if (!isKeyPressed(SDL_SCANCODE_F))
    {
        pd->base.f_pressed = 0;
    }

    bool32 locked_message_mutex = 0;

#   define LOCK_MSG_MUTEX() \
        if (!locked_message_mutex) \
        { \
            Mutex_lock(&message_buffer.mutex); \
            locked_message_mutex = 1; \
        }

#   define UNLOCK_MSG_MUTEX() \
    if (locked_message_mutex) Mutex_unlock(&message_buffer.mutex);

    /* Weapon was changed - send a message to the server */
    if (pd->weapon != old_weapon)
    {
        LOCK_MSG_MUTEX();

        void *msg = MessageBuffer_queueMessageUnsafe(KS_CS_MSG_WEAPON_SWAPPED,
            KS_CS_MSG_WEAPON_SWAPPED_SIZE);

        if (msg)
            *(char*)msg = (char)pd->weapon;
        else
        {
            DEBUG_PRINTF("Failed to queue a weapon swap message to the "
                "message buffer!");
        }
    }

    if (getMouseButtonState(key_config.dash))
        SET_BITFLAG_ON(kb_state, kb_state_t, KS_KEY_DASH);

    if (getMouseButtonState(key_config.shoot)
    && !game.mp_game_screen_data.is_menu_visible
    && sp_shootAsPlayerPawn(pawn))
    {
        int msg_type;
        int msg_size;

        switch (pd->weapon)
        {
            case ROCKET_LAUNCHER:
                msg_type = KS_CS_MSG_SHOT_ROCKET_LAUNCHER;
                msg_size = KS_CS_MSG_SHOT_ROCKET_LAUNCHER_SIZE;
                break;
            case SMG:
                msg_type = KS_CS_MSG_SHOT_SMG;
                msg_size = KS_CS_MSG_SHOT_SMG_SIZE;
                break;
            case RAILGUN:
                msg_type = KS_CS_MSG_SHOT_RAILGUN;
                msg_size = KS_CS_MSG_SHOT_RAILGUN_SIZE;
                break;
            case MACHETE:
                msg_type = KS_CS_MSG_HIT_MACHETE;
                msg_size = KS_CS_MSG_HIT_MACHETE_SIZE;
                break;
            default:
                ASSERT(0 && "Invalid weapon enum.");
                break;
        }

        LOCK_MSG_MUTEX();

        void *msg = MessageBuffer_queueMessageUnsafe(
            msg_type, msg_size);

        if (msg)
        {
            ByteBuffer_init(&byte_buffer, msg, msg_size);
            vec3 forward; Transform_getForwardVector(t, forward);

            switch (pd->weapon)
            {
                case ROCKET_LAUNCHER:
                    KS_CS_MSG_SHOT_ROCKET_LAUNCHER_serialize(&byte_buffer,
                        t->pos, forward);
                    break;
                case SMG:
                    KS_CS_MSG_SHOT_SMG_serialize(&byte_buffer, t->pos, forward);
                    break;
                case RAILGUN:
                    KS_CS_MSG_SHOT_RAILGUN_serialize(&byte_buffer,
                        t->pos, forward);
                    break;
                case MACHETE:
                    KS_CS_MSG_HIT_MACHETE_serialize(&byte_buffer,
                        t->pos, forward);
                    break;
                default:
                    ASSERT(0 && "Invalid weapon enum.");
                    break;
            }
        }
        else
        {
            DEBUG_PRINTF("Failed to queue a shoot/hit message "
                "to the message buffer!");
        }
    }

    UNLOCK_MSG_MUTEX();

    if (InputBuffer_queueMovementInput(&input_buffer, kb_state,
        t->rot[1]) != 0)
    {
        DEBUG_PRINTF("Failed to queue movement input!");
    }

    /*float tmp_rot0 = t->rot[0];
    float tmp_rot2 = t->rot[2];
    t->rot[0] = 0.f;
    t->rot[2] = 0.f;*/
    movePawnByKeyboardInput(pawn, kb_state);
    /*t->rot[0] = tmp_rot0;
    t->rot[2] = tmp_rot2;*/
}

void
mp_reconcileInputs(Entity *pawn, input_sequence_t latest_verified_sequence,
    vec3 latest_verified_position, vec3 latest_verified_velocity)
{
#   define SQ_SNAP_TRESHOLD 0.10f /* Was (21.5.17): 0.20f */

    Transform *t        = &Entity_getTransform(pawn)->transform;
    PhysicsComponent *p = Entity_getPhysicsComponent(pawn);

    input_sequence_t start  = latest_verified_sequence + 1;
    input_sequence_t end    = input_buffer.running_sequence;

    if (!IS_SEQUENCE_GREATER(end, start) ||
        ((end > start && end - start > max_input_replays) ||
         (end < start && end + (MAX_INPUT_BUFFER_ITEMS - start) > max_input_replays)))
    {
        DEBUG_PRINTF("%s: max replay treshold reached (%i) - "
            "snapping and reverting!\n", __func__, max_input_replays);
        InputBuffer_revertToSequence(&input_buffer, latest_verified_sequence,
            latest_verified_position, latest_verified_velocity);
        return;
    }

    vec3_copy(p->velocity, latest_verified_velocity);
    vec3_copy(t->pos, latest_verified_position);

    Scene3D_updatePhysicsForSingleEntity(&game.game_scene, pawn,
        game.game_scene.physics_step);
    Scene3D_updateStaticCollisionsForSingleEntity(&game.game_scene,
        pawn, game.game_scene.physics_step);

    if (start == end)
        return;

    int                 index;
    StoredClientInput   *inp;
    kb_state_t          kb_state;
    float               tmp_rot0 = t->rot[0];
    float               tmp_rot2 = t->rot[2];
    t->rot[0] = 0.f;
    t->rot[2] = 0.f;

    /* Copy the corrected position into the latest input */
    index = (int)latest_verified_sequence % MAX_SENT_INPUT_ITEMS;
    inp = &input_buffer.sent_items[index];
    vec3_copy(inp->position, latest_verified_position);

    vec3 diff;

    int num_iterations = 0;

    for (input_sequence_t seq = start; seq != end; ++seq)
        num_iterations++;

    if (num_iterations > max_input_replays)
    {
        InputBuffer_revertToSequence(&input_buffer, latest_verified_sequence,
            latest_verified_position, latest_verified_velocity);
        DEBUG_PRINTF("%s: num iterations over max_input_replays - reverting.\n",
            __func__);
        return;
    }

    int iteration = 0;

    for (input_sequence_t seq = start; seq != end; ++seq)
    {
        index = (int)seq % MAX_SENT_INPUT_ITEMS;
        inp = &input_buffer.sent_items[index];
        /*float multiplier = (float)iteration / (float)num_iterations;*/
        float sq_len;

        kb_state    = inp->input.data.movement.kb_state;
        t->rot[1]   = inp->input.data.movement.orientation;

        movePawnByKeyboardInput(pawn, kb_state);
        Scene3D_updatePhysicsForSingleEntity(&game.game_scene, pawn,
            game.game_scene.physics_step);
        Scene3D_updateStaticCollisionsForSingleEntity(&game.game_scene,
            pawn, game.game_scene.physics_step);

        /* Compute the difference between the old and new position */
        vec3_sub(diff, inp->position, t->pos);
        vec3_scale(diff, diff, _recon_diff_mul);
        sq_len = vec3_mul_inner(diff, diff);

        if (sq_len > SQ_SNAP_TRESHOLD)
        {
            if (sq_len < _recon_max_sq_diff)
                vec3_add(t->pos, t->pos, diff);
            else
            {
#if 0
                vec3_norm(diff, diff);
                vec3_scale(diff, diff, multiplier * sqrtf(_recon_max_sq_diff));
                vec3_add(t->pos, t->pos, diff);
#endif
            }
        }

        /* Copy the new results to the buffer */
        vec3_copy(inp->position, t->pos);
        vec3_copy(inp->velocity, p->velocity);

        iteration++;
    }

    t->rot[0] = tmp_rot0;
    t->rot[2] = tmp_rot2;

#   undef SQ_SNAP_TRESHOLD
}

void
mp_reconcileInputs2(Entity *pawn, input_sequence_t latest_verified_sequence,
    vec3 latest_verified_position, vec3 latest_verified_velocity)
{
    Transform *t        = &Entity_getTransform(pawn)->transform;
    PhysicsComponent *p = Entity_getPhysicsComponent(pawn);

    input_sequence_t start  = latest_verified_sequence + 1;
    input_sequence_t end    = input_buffer.running_sequence;

    if (!IS_SEQUENCE_GREATER(end, start))
    {
        DEBUG_PRINTF("%s: end was of lower sequence than start!\n", __func__);

#if 0
        if (start != end)
            InputBuffer_revertToSequence(&input_buffer, latest_verified_sequence,
                latest_verified_position, latest_verified_velocity);
#endif
        return;
    }

    float tmp_rot0 = t->rot[0];
    float tmp_rot2 = t->rot[2];
    t->rot[0] = 0.f;
    t->rot[2] = 0.f;

    vec3_copy(p->velocity, latest_verified_velocity);
    vec3_copy(t->pos, latest_verified_position);

    Scene3D_updatePhysicsForSingleEntity(&game.game_scene, pawn,
        game.game_scene.physics_step);
    Scene3D_updateStaticCollisionsForSingleEntity(&game.game_scene,
        pawn, game.game_scene.physics_step);

    int num_pot_runs = 0;

    for (input_sequence_t seq = start;
        seq != end;
        seq = (seq != 65535 ? seq + 1 : 0))
    {
        num_pot_runs++;

        if (num_pot_runs <= max_input_replays)
            continue;

        DEBUG_PRINTF("%s: too many potential runs. Sequence: %d. Snapping...\n",
            __func__, (int)latest_verified_sequence);

        /* Set a new starting position */
        input_sequence_t new_start;

        if (end > max_input_rollback)
            new_start = end - (max_input_rollback);
        else
            new_start = 65535 - ((max_input_rollback) - end);

        DEBUG_PRINTF("3 (old start %d, new start %d\n", (int)start, (int)new_start);

        start = new_start;

        int                 index   = (int)new_start % MAX_SENT_INPUT_ITEMS;
        StoredClientInput   *inp    = &input_buffer.sent_items[index];

        vec3_copy(p->velocity, inp->velocity);
        vec3_copy(t->pos, inp->position);
        t->rot[1] = inp->input.data.movement.orientation;

        movePawnByKeyboardInput(pawn,  inp->input.data.movement.kb_state);
        Scene3D_updatePhysicsForSingleEntity(&game.game_scene, pawn,
            game.game_scene.physics_step);
        Scene3D_updateStaticCollisionsForSingleEntity(&game.game_scene,
            pawn, game.game_scene.physics_step);

        /*InputBuffer_revertToSequence(&input_buffer, latest_verified_sequence,
            latest_verified_position, latest_verified_velocity);*/

        break;
    }

    int                 index;
    StoredClientInput   *inp;
    kb_state_t          kb_state;

    for (input_sequence_t seq = start;
        seq != end;
        seq = (seq != 65535 ? seq + 1 : 0))
    {
        index   = (int)seq % MAX_SENT_INPUT_ITEMS;
        inp     = &input_buffer.sent_items[index];

        if (!inp->used)
        {
            DEBUG_PRINTF("%s: input unused.\n", __func__);
            continue;
        }

        kb_state    = inp->input.data.movement.kb_state;
        t->rot[1]   = inp->input.data.movement.orientation;

        movePawnByKeyboardInput(pawn, kb_state);
        Scene3D_updatePhysicsForSingleEntity(&game.game_scene, pawn,
            game.game_scene.physics_step);
        Scene3D_updateStaticCollisionsForSingleEntity(&game.game_scene,
            pawn, game.game_scene.physics_step);
    }

    t->rot[0] = tmp_rot0;
    t->rot[2] = tmp_rot2;
}

Entity *
mp_createOtherPlayersPawn(Scene3D *scene, replication_id32_t rid,
    vec3 pos, vec3 vel, char char_type)
{
    Entity *e = Scene3D_createEntity(scene, pos[0], pos[1], pos[2]);
    if (!e) return 0;

    Entity_makeExistingReplicated(e, rid);

    shared_initPlayerPawn(e);

    ScriptComponent *sc     = Entity_attachScriptComponent(e);
    PlayerScriptData *sd    = ScriptComponent_allocateData(sc,
        sizeof(PlayerScriptData));
    shared_initBasePlayerScriptData(&sd->base);
    sd->character_type = char_type;

    if (!vec3_is_null(vel))
    {
        PhysicsComponent *pc = Entity_getPhysicsComponent(e);
        vec3_copy(pc->velocity, vel);
    }

    ModelComponent *mc      = Entity_attachModelComponent(e);
    RenderModel *rm         = ModelComponent_getDefaultRenderModel(mc);
    RenderModel *rm_weapon  = ModelComponent_attachRenderModel(mc, scene, 2);

    rm_weapon->model    = GET_INGAME_MODEL("rocket launcher");
    rm_weapon->material = GET_INGAME_MATERIAL("rocket launcher");
    rm_weapon->visible  = 1;
    rm_weapon->shader   = CEL_SHADER_OPAQUE;

    Animator *an = ModelComponent_attachNamedAnimator(mc, scene, rm,
        "character");
    an->speed = 0.8f;

    rm->shader                      = CEL_SHADER_OPAQUE;
    rm->outline_normal_offset       = 0.05f;
    rm->relative_transform.pos[1]   = -11.5f;
    rm->relative_transform.rot[1]   = (float)DEG_TO_RAD(260.0f); /* 270? */
    vec3_set(rm->relative_transform.scale, 0.2f, 0.2f, 0.2f);

    mp_setOtherPawnWeaponRenderModelTransform(rm_weapon, ROCKET_LAUNCHER);
    vec3_set(rm_weapon->relative_transform.scale, 0.15f, 0.15f, 0.15f);

    /* Based on char_type here we could play a different animation! */


    /* Set the state over range first, so that the setOtherPawnMovementState()
     * defaults to "standing" */
    sd->movement_state      = NUM_PAWN_MOVEMENT_STATES + 1;
    sd->base.is_hit         = 0;
    sd->base.is_hit_timer   = 0.0f;

    RenderModel *rm_muzzle = ModelComponent_attachRenderModel(mc, e->scene,
        PLAYER_RENDER_MODEL_ID_MUZZLEFIRE);
    rm_muzzle->material = GET_INGAME_MATERIAL("muzzlefire");
    rm_muzzle->shader = SHADER3D_BILLBOARD;
    rm_muzzle->mode = RENDER_MODEL_BILLBOARD;

    rm_muzzle->relative_transform.pos[0]    = -1.4f;
    rm_muzzle->relative_transform.pos[1]    = -0.3f;
    rm_muzzle->relative_transform.pos[2]    = -3.25f;
    rm_muzzle->relative_transform.scale[0]  = 0.5f;
    rm_muzzle->relative_transform.scale[1]  = 0.5f;
    rm_muzzle->relative_transform.scale[2]  = 0.5f;

    ModelComponent_attachAnimator(mc, e->scene, rm_muzzle,
        PLAYER_ANIMATOR_ID_MUZZLEFIRE);

    mp_setOtherPawnMovementStateAndAnimation(e, DEFAULT_PAWN_MOVEMENT_STATE);

    return e;
}

void
MPGameScreen_update(Screen *screen)
{
    (void)screen;
    double dt = getDelta();
    MPGameScreenData *scr = &game.mp_game_screen_data;

    GUI_beginUpdate();

    if (isKeyPressed(SDL_SCANCODE_ESCAPE) && !scr->is_menu_visible)
    {
        scr->is_menu_visible = 1;
        setRelativeMouseMode(0);
    }

    if (scr->is_menu_visible)
    {
        drawGameMenu(&scr->is_menu_visible);
        if (isKeyPressed(SDL_SCANCODE_N))
            scr->is_menu_visible = 0;
    }
    else if (isKeyPressed(key_config.show_stat_window))
    {
        int stat_menu_w = MIN(getWindowWidth()  - 450, 64);
        int stat_menu_h = MIN(getWindowHeight() - 430, 64);;

        GUI_setNextWindowStyle(&stat_window_style);
        GUI_beginWindow("stats",
            getWindowWidth() / 2 - stat_menu_w / 2 - 24,
            getWindowHeight() / 2 - stat_menu_h / 2,
            stat_menu_w, stat_menu_h);
        char buf[128];
        sprintf(buf, "Frags: %u\n", game.mp_data.num_frags);
        GUI_text_Scale(buf, 5, 5, 2.0f);

        GUI_endWindow();
    }

    if (scr->frag_text_timer > 0)
    {
        GUI_text_Scale("FRAG!", getWindowWidth() / 2 - 25,
            getWindowHeight() / 2 - 200, 4.0f);
        scr->frag_text_timer -= dt;
    }

    if (scr->powerup_text_timer > 0)
    {
        GUI_text_Scale("POWER-UP", getWindowWidth() / 4 - 30,
            getWindowHeight() / 4 - 20, 4.0f);
        scr->powerup_text_timer -= dt;
    }

    if (scr->served_text_timer > 0)
    {
        GUI_text_Scale("SERVED :DDD", getWindowWidth() / 4 - 30,
            getWindowHeight() / 4 - 20, 4.0f);
        scr->served_text_timer -= dt;
    }

    switch (game.multiplayer_state)
    {
        case MP_STATE_WAITING_FOR_GAME:
        {
            GUI_text_Scale("Waiting for info from server...",
                getWindowWidth() / 2 - 100, getWindowHeight() / 2 - 16, 2.0f);
        }
            break;
        case MP_STATE_LOADING:
        {
            game.multiplayer_state = MP_STATE_READY;
        }
            break;
        case MP_STATE_READY:
        {
            Entity *pawn = mp_getPlayerPawn();

            if (pawn)
            {
                Transform *t = &Entity_getTransform(pawn)->transform;

                ModelComponent *mc      = Entity_getModelComponent(pawn);
                PhysicsComponent *pc    = Entity_getPhysicsComponent(pawn);
                mc->visible = 1;
                RenderModel *rm = ModelComponent_getDefaultRenderModel(mc);

                rm->visible = 1;
                float cached_orientation = t->rot[1];

                if (game.mp_data.have_new_verified_input)
                /*|| reconciliation_func == mp_reconcileInputs)*/
                {
                    input_sequence_t sequence;
                    vec3 position, velocity;

                    Mutex_lock(&game.latest_verified_input_mutex);

                    sequence = game.mp_data.latest_verified_input_sequence;
                    vec3_copy(position, game.mp_data.latest_verified_position);
                    vec3_copy(velocity, game.mp_data.latest_verified_velocity);

                    game.mp_data.have_new_verified_input = 0;

                    Mutex_unlock(&game.latest_verified_input_mutex);

                    reconciliation_func(pawn, sequence, position, velocity);
                }

                Mutex_lock(&game.game_scene.entity_access_mutex);

                SimQueue_executeContents();

                t->rot[1] = cached_orientation;

                _mp_updateScene(pawn, t, pc, dt);

                /* Focus the camera to the view of the player */
                vec3 forward; Entity_getForwardVector(pawn, forward);
                vec3 target;
                vec3_copy(scr->camera.position, t->pos);
                vec3_add(target, scr->camera.position, forward);
                Camera_lookAt(&scr->camera, target);

                if (isKeyPressed(SDL_SCANCODE_DOWN))
                    graphics_config.draw_distance -= 50.0f * (float)dt;
                if (isKeyPressed(SDL_SCANCODE_UP))
                    graphics_config.draw_distance += 50.0f * (float)dt;
                if (isKeyPressed(SDL_SCANCODE_LEFT))
                    graphics_config.field_of_view -= 0.7f * (float)dt;
                if (isKeyPressed(SDL_SCANCODE_RIGHT))
                    graphics_config.field_of_view += 0.7f * (float)dt;

                /* Keys for changing the reconciliation function */
                if (isKeyPressed(SDL_SCANCODE_F1)
                && reconciliation_func != mp_reconcileInputs)
                {
                    reconciliation_func = mp_reconcileInputs;
                    DEBUG_PRINTF("Set reconciliation function to v1.\n");
                }

                if (isKeyPressed(SDL_SCANCODE_F2)
                && reconciliation_func != mp_reconcileInputs2)
                {
                    reconciliation_func = mp_reconcileInputs2;
                    DEBUG_PRINTF("Set reconciliation function to v2.\n");
                }

                if (isKeyPressed(SDL_SCANCODE_F3))
                    scr->show_network_text = 1;
                if (isKeyPressed(SDL_SCANCODE_F4))
                    scr->show_network_text = 0;

                if (isKeyPressed(SDL_SCANCODE_LSHIFT)
                && isKeyPressed(SDL_SCANCODE_R))
                {
                    const char *v1 = "Reconciliation func: v1 "
                        "(F1 or F2 to change)";
                    const char *v2 = "Reconciliation func: v2 "
                        "(F1 or F2 to change)";
                    if (reconciliation_func == mp_reconcileInputs)
                        GUI_text(v1, 5, 60);
                    else
                        GUI_text(v2, 5, 60);
                }

                scr->camera.fov         = graphics_config.field_of_view;
                scr->camera.far_clip    = graphics_config.draw_distance;
                renderScene3D(&game.game_scene, &scr->camera, 0);

                Mutex_unlock(&game.game_scene.entity_access_mutex);

                if (scr->show_network_text)
                {
                    char buf[128];

                    if (client.connection.have_unacked_messages)
                    {
                        sprintf(buf, "Latest acked input: %u",
                            (uint)game.mp_data.latest_verified_input_sequence);
                        GUI_text(buf, 0, 0);
                    }
                    else
                    {
                        GUI_text("no unacked messages", 0, 0);
                    }

                    sprintf(buf, "Position: [%f, %f, %f]\n",
                        t->pos[0], t->pos[1], t->pos[2]);
                    GUI_textLine(buf, 0, 14);

                    sprintf(buf, "Server thinks you're at: [%f, %f, %f]\n",
                        game.mp_data.latest_verified_position[0],
                        game.mp_data.latest_verified_position[1],
                        game.mp_data.latest_verified_position[2]);

                    GUI_textLine(buf, 0, 28);
                }
            }
            else
            {
                Scene3D_updateClientSimulation(&game.game_scene, &client, dt);
                GUI_text_Scale("Waiting for data from server...",
                    getWindowWidth() / 2 - 100, getWindowHeight() / 2 - 16,
                    2.0f);
            }
        }
            break;
    }

    renderTexture(permanent_assets.crosshair_texture,
        ((float)getWindowWidth() / 2) - 4.0f,
        ((float)getWindowHeight() / 2) - 4.0f);

   {
       Entity *pawn = mp_getPlayerPawn();
       ScriptComponent *sc = Entity_getScriptComponent(pawn);
       PlayerScriptData *pd = (PlayerScriptData*)sc->data;

       Texture *selected_weapon_texture;

       if (pd->base.is_hit)
       {
           float texture_width = (float)permanent_assets.hitscreen_texture->width;
           float texture_height = (float)permanent_assets.hitscreen_texture->height;
           SpriteBatch_drawSprite_Scale(&engine.renderer.sprite_batch, permanent_assets.hitscreen_texture,
               0.0f, 0.0f, 0, (float)getWindowWidth() / texture_width, (float)getWindowHeight() / texture_height);
           //renderTexture(permanent_assets.hitscreen_texture,
            //   0, 0);
       }

       switch (pd->weapon)
       {
       case ROCKET_LAUNCHER:   selected_weapon_texture = permanent_assets.rocket_selected_texture; break;
       case SMG:               selected_weapon_texture = permanent_assets.smg_selected_texture; break;
       case RAILGUN:           selected_weapon_texture = permanent_assets.railgun_selected_texture; break;
       case MACHETE:           selected_weapon_texture = permanent_assets.machete_selected_texture; break;
       default: {ASSERT(0); } break;
       }

       renderTexture(selected_weapon_texture,
           300.0f, (float)getWindowHeight() - 96.0f);

    }

    GUI_progressBar(game.mp_data.health, 100, 16, getWindowHeight() - 48, 54, 4,
        GUI_FILLDIRECTION_RIGHT, F_COLOR_WHITE);

    char buf[64];
    sprintf(buf, "%u ms", Client_getAverageLatency(&client));
    GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_LEFT);
    GUI_textLine(buf, 4, 0);

    sprintf(buf, "%f", engine.clock.fps);
    GUI_text(buf, 0, -16);


    GUI_endUpdate();

    if (client.status == CLIENT_STATUS_DISCONNECTED)
        setScreen(game.screens[MENU_SCREEN]);
}


void
MPGameScreen_open(Screen *screen)
{
    (void)screen;
    setRelativeMouseMode(1);
    game.mp_game_screen_data.is_menu_visible    = 0;
    game.mp_game_screen_data.frag_text_timer    = 0;
    game.mp_game_screen_data.powerup_text_timer = 0;
    game.mp_game_screen_data.served_text_timer  = 0;
    game.mp_game_screen_data.show_network_text  = 0;
    reconciliation_func                         = mp_reconcileInputs2;
}

void
MPGameScreen_close(Screen *screen)
{
    Client_disconnect(&client);
}

void
MPGameScreen_playFragText()
{
    game.mp_game_screen_data.frag_text_timer = FRAG_TEXT_DUR;
}

void
MPGameScreen_playPowerupText()
{
    game.mp_game_screen_data.powerup_text_timer = POWERUP_TEXT_DUR;
}

void
MPGameScreen_playServedText()
{
    game.mp_game_screen_data.served_text_timer = SERVED_TEXT_DUR;
}

int
createMPGameScreen()
{
    game.screens[MP_GAME_SCREEN] = createScreen(MPGameScreen_update,
        &game.mp_game_screen_data);

    if (!game.screens[MP_GAME_SCREEN])
        return 1;

    game.screens[MP_GAME_SCREEN]->onOpen    = MPGameScreen_open;
    game.screens[MP_GAME_SCREEN]->onClose   = MPGameScreen_close;

    Camera_init(&game.mp_game_screen_data.camera);

    return 0;
}

int
mp_initGameSceneForLevel1()
{
    Scene3D_clear(&game.game_scene);

    if (Scene3D_init(&game.game_scene, game.scene_memory,
        game.scene_memory_size, 2048, 1) != 0)
        return 1;

    Scene3D_setBackgroundColor(&game.game_scene, 0.0f, 0.0f, 1.0f, 0.0f);
    Scene3D_setAmbientColor(&game.game_scene, 1.0f, 1.0f, 1.0f, 0.5f);

    createMapEntity(&game.game_scene,
        GET_INGAME_MODEL("map"),
        GET_INGAME_MODEL("map nocol"),
        GET_INGAME_MATERIAL_ARRAY("level1"),
        GET_INGAME_MATERIAL_ARRAY("level2"),
        CEL_SHADER_TRANSPARENT, 0, 0, 0);

    if (shared_buildCollisionWorldForLevel1(&game.game_scene,
        GET_INGAME_MODEL("map")) != 0)
    {
        DEBUG_PRINTF("Failed to build level 1 collision world!\n");
        return 2;
    }

    shared_setDefaultScenePhysicsLimits(&game.game_scene);

    Scene3D_setSkyboxTexture(&game.game_scene,
        permanent_assets.tex_skybox_level1);
    Scene3D_setSkyboxScale(&game.game_scene, 1.0f, 1.0f, 1.0f);

    Entity *ambient_light = Scene3D_createEntity(&game.game_scene, 0.0f, 50.0f, 0.0f);

    DirectionalLightComponent *blue = Entity_attachDirectionalLightComponent(
        ambient_light);
    DirectionalLightComponent_setDiffuse(blue, 0.0f, 0.0f, 0.2f);
    DirectionalLightComponent_setSpecular(blue, 0.7f, 0.7f, 0.7f);
    DirectionalLightComponent_setDirection(blue, 0.0f, -0.5f, 0.5f, 1);
    blue->radius = 50.0f;

    Entity *sun = Scene3D_createEntity(&game.game_scene, 0.0f, 50.0f, 0.0f);

    DirectionalLightComponent *yellow = Entity_attachDirectionalLightComponent(sun);
    DirectionalLightComponent_setDiffuse(yellow, 0.3f, 0.3f, 0.3f);
    DirectionalLightComponent_setSpecular(yellow, 0.7f, 0.7f, 0.7f);
    DirectionalLightComponent_setDirection(yellow, 50.0f, -1.0f, 2.5f, 1);
    yellow->radius = 50.0f;

    PointLightComponent *p = Entity_attachPointLightComponent(sun);
    PointLightComponent_setDiffuse(p, 0.1f, 0.1f, 0.1f);
    PointLightComponent_setSpecular(p, 0.4f, 0.1f, 0.8f);
    PointLightComponent_setAmbient(p, 0.4f, 0.4f, 0.4f);
    p->radius = 150.0f;

    Scene3D_rotateSkybox(&game.game_scene, 0.0f, (float)DEG_TO_RAD(180), 0.0f);

    return 0;

}

void
mp_playerWeaponSwapNext(Entity *pawn)
{
    ScriptComponent *sc = Entity_getScriptComponent(pawn);
    PlayerScriptData *pd = (PlayerScriptData*)sc->data;

    enum KsWeaponType new_weapon;

    switch (pd->weapon)
    {
        case ROCKET_LAUNCHER:   new_weapon = SMG;               break;
        case SMG:               new_weapon = RAILGUN;           break;
        case RAILGUN:           new_weapon = ROCKET_LAUNCHER;           break;
        default: {ASSERT(0); } break;
    }

    setPlayerPawnWeaponLocal(pawn, new_weapon);
}

void
mp_playerWeaponSwapPrev(Entity *pawn)
{
    ScriptComponent *sc = Entity_getScriptComponent(pawn);
    PlayerScriptData *pd = (PlayerScriptData*)sc->data;

    enum KsWeaponType new_weapon;

    switch (pd->weapon)
    {
        case ROCKET_LAUNCHER:   new_weapon = RAILGUN;           break;
        case RAILGUN:           new_weapon = SMG;               break;
        case SMG:               new_weapon = ROCKET_LAUNCHER;   break;
        default: {ASSERT(0); } break;
    }

    setPlayerPawnWeaponLocal(pawn, new_weapon);
}

void
mp_shootAsPlayerPawn(Entity *pawn)
{
}

void
mp_shootAsOtherPawnRocketLauncher(Entity *pawn, vec3 position, vec3 direction)
{
    BulletType *bt = GET_BULLET_TYPE(ROCKET_LAUNCHER);

    vec3 spawn_pos;
    vec3_add(spawn_pos, position, direction);
    Entity *e = Scene3D_createEntity(pawn->scene, spawn_pos[0], spawn_pos[1], spawn_pos[2]);
    Transform *t = &Entity_getTransform(e)->transform;
    vec3 temp;
    Entity_setLifetime(e, 10.0f);
    DEBUG_PRINTF("set lifetime to : %f\n", 10.0f);

    vec3_norm(direction, direction);

    t->scale[0] = bt->scale;
    t->scale[1] = bt->scale;
    t->scale[2] = bt->scale;

    vec3_scale(temp, direction, -1.0f);
    t->rot[1] = -((float)atan2(temp[2], temp[0]));
    t->rot[2] = (float)atan2(temp[1], sqrtf((temp[0] * temp[0]) + \
        (temp[2] * temp[2])));

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
    pc->gravity_active = 0;
    pc->friction = 0.0f;
    PhysicsComponent_setActivity(pc, 1);
    vec3_copy(pc->velocity, direction);
    vec3_scale(pc->velocity, pc->velocity, bt->base.speed);

    ModelComponent *m = Entity_attachModelComponent(e);
    m->visible = 1;
    RenderModel *rm = ModelComponent_getDefaultRenderModel(m);
    rm->shader = CEL_SHADER_OPAQUE;
    rm->model = bt->model;
    rm->material = bt->material;

    vec3_copy(rm->relative_transform.rot, bt->relative_rotation);

    vec3 expl_dist_vec;
    vec3_sub(expl_dist_vec, game.mp_game_screen_data.camera.position, position);

    float dist_to_expl = sqrtf(expl_dist_vec[0] * expl_dist_vec[0]
        + expl_dist_vec[1] * expl_dist_vec[1]
        + expl_dist_vec[2] + expl_dist_vec[2]);

    if (dist_to_expl >= MAX_DISTANCE_TO_PLAY_SOUND)
        return;

    float perse = dist_to_expl / MAX_DISTANCE_TO_PLAY_SOUND;
    if (perse > 1.0f) perse = 1.0f;
    perse = 1.0f - perse;

    SoundEffect_setVolume(permanent_assets.se_fwump, (int)(128.0f * perse));

    SoundEffect_play(permanent_assets.se_fwump,
        SOUND_CHANNEL_PLAYER_WEAPON, 0);
}

void
mp_shootAsOtherPawnSMG(Entity *pawn, vec3 position, vec3 direction)
{
    vec3 forward;
    Transform *t = &Entity_getTransform(pawn)->transform;
    Transform_getForwardVector(t, forward);

    vec3 spawn_pos;
    vec3_add(spawn_pos, t->pos, forward);
    createBulletEntity(pawn->scene, SMG, spawn_pos,
        t->rot, forward);

    //Sound shit

    vec3 expl_dist_vec;
    vec3_sub(expl_dist_vec, position, game.mp_game_screen_data.camera.position);

    float dist_to_expl = sqrtf((expl_dist_vec[0] * expl_dist_vec[0])
        + (expl_dist_vec[1] * expl_dist_vec[1])
        + (expl_dist_vec[2] * expl_dist_vec[2]));

    if (dist_to_expl >= MAX_DISTANCE_TO_PLAY_SOUND)
    {
        return;
    }

    float perse = dist_to_expl / MAX_DISTANCE_TO_PLAY_SOUND;
    if (perse > 1.0f) perse = 1.0f;
    perse = 1.0f - perse;

    SoundEffect_setVolume(permanent_assets.se_smg, (int)(32.0f * perse));

    SoundEffect_play(permanent_assets.se_smg,
        SOUND_CHANNEL_PLAYER_WEAPON, 0);
}


void
mp_shootAsOtherPawnRailgun(Entity *pawn, vec3 position, vec3 direction)
{
    /*
    vec3 forward, hit_pos, triangle_normal, endpoint, random, temp;

    float dist_to_triangle, dist_to_hit;

    Transform *t = &Entity_getTransform(pawn)->transform;
    Transform_getForwardVector(t, forward);

    vec3_scale(temp, forward, 300.0f);
    vec3_add(endpoint, t->pos, temp);

    dist_to_triangle = Scene3D_castRayToStaticGeometryAndGetNormal(
        pawn->scene, t->pos, forward, 300.0f, hit_pos, &dist_to_hit,
        triangle_normal);

    DEBUG_PRINTF("hit_pos 0: %f", hit_pos[0]);
    DEBUG_PRINTF("hit_pos 1: %f", hit_pos[1]);
    DEBUG_PRINTF("hit_pos 2: %f", hit_pos[2]);

    for (int i = 0; i < 5; ++i)
    {
        random[0] = (float)(-10.0f + rand() % 20);
        random[1] = (float)(5.0f + rand() % 20);
        random[2] = (float)(-10.0f + rand() % 20);
        createEffect(pawn, getDefaultPlaneModel(),
            GET_INGAME_MATERIAL("railgun spark"), hit_pos, random, random,
            0.5f, 1.0f, 1, 1);
    }
    */
}

void
mp_createRocketExplosion(vec3 pos)
{
    Entity *explosion = Scene3D_createEntity(&game.game_scene,
        pos[0], pos[1], pos[2]);
    ModelComponent *m = Entity_attachModelComponent(explosion);
    ModelComponent_getDefaultRenderModel(m)->shader = CEL_SHADER_OPAQUE;
    ModelComponent_getDefaultRenderModel(m)->mode = RENDER_MODEL_NORMAL;
    Animator *a = ModelComponent_attachAnimator(m, &game.game_scene,
        ModelComponent_getDefaultRenderModel(m), 0);
    Animator_playAnimation(a, &ig_assets.explosion_animation, 0);

    m->visible = 1;

    PhysicsComponent *pc = Entity_attachPhysicsComponent(explosion);
    PhysicsComponent_setActivity(pc, 1);
    PhysicsComponent_setStaticity(pc, 1);
    PhysicsComponent_setCollisionCallback(pc, sp_explosionCallback);
    pc->gravity_active = 0;
    ScriptComponent *sc = Entity_attachScriptComponent(explosion);
    ScriptComponent_allocateData(sc, sizeof(float));
    *((float*)sc->data) = 0;
    ScriptComponent_setCallback(sc, mp_rocketExplosionScript);
    Entity_setLifetime(explosion, 0.2f);

    SphereCollider *sphereCollider = PhysicsComponent_attachSphereCollider(pc,
        explosion, 0);
    sphereCollider->radius = 2.0f;

    //vec3 cam_pos;
    //vec3_copy(cam_pos, game.mp_game_screen_data.camera.position);
    vec3 expl_dist_vec;
    vec3_sub(expl_dist_vec, game.mp_game_screen_data.camera.position, pos);

    float dist_to_expl = sqrtf(expl_dist_vec[0] * expl_dist_vec[0]
        + expl_dist_vec[1] * expl_dist_vec[1] 
        + expl_dist_vec[2] + expl_dist_vec[2]);

    if (dist_to_expl >= MAX_DISTANCE_TO_PLAY_SOUND)
        return;

    float perse = dist_to_expl / MAX_DISTANCE_TO_PLAY_SOUND;
    if (perse > 1.0f) perse = 1.0f;
    perse = 1.0f - perse;

    SoundEffect_setVolume(permanent_assets.se_explosion, (int)(64.0f * perse));

    SoundEffect_play(permanent_assets.se_explosion,
        SOUND_CHANNEL_ENEMY_EXPLOSION, 0);
}

void
mp_rocketExplosionScript(Entity *entity, void *scriptdata)
{
    float *timer = (float*)scriptdata;
    double delta = getDelta();
    *(timer) += (float)delta * 15.0f;
    Entity_setScale(entity, *(timer), *(timer), *(timer));
}

Entity *
mp_createItemEntity(Scene3D *scene, enum KsItemType type,
    float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(scene, x, y, z);
    if (!e) return 0;

    ModelComponent *mc  = Entity_attachModelComponent(e);
    RenderModel *rm     = ModelComponent_getDefaultRenderModel(mc);

    rm->shader  = CEL_SHADER_OPAQUE;
    rm->visible = 1;

    switch (type)
    {
        case ITEM_MEDKIT:
            Entity_setScale(e, 3.0f, 3.0f, 3.0f);
            rm->model = GET_INGAME_MODEL("medkit");
            rm->material = GET_INGAME_MATERIAL("medkit");
            rm->outline_normal_offset = 0.005f;
            Entity_setName(e, "medkit");
            break;
        case ITEM_ROCKET_AMMO:
            Entity_setScale(e, 0.5f, 0.5f, 0.5f);
            rm->model = GET_INGAME_MODEL("rocket ammo");
            rm->material = GET_INGAME_MATERIAL("rocket ammo");
            rm->outline_normal_offset = 0.02f;
            Entity_setName(e, "rocket ammo");
            break;
        case ITEM_SMG_AMMO:
            Entity_setScale(e, 0.5f, 0.5f, 0.5f);
            rm->model = GET_INGAME_MODEL("smg ammo");
            rm->material = GET_INGAME_MATERIAL("smg ammo");
            rm->outline_normal_offset = 0.03f;
            Entity_setName(e, "smg ammo");
            break;
        case ITEM_RAILGUN_AMMO:
            Entity_setScale(e, 0.25f, 0.25f, 0.25f);
            rm->model = GET_INGAME_MODEL("railgun ammo");
            rm->material = GET_INGAME_MATERIAL("railgun ammo");
            rm->outline_normal_offset = 0.05f;
            Entity_setName(e, "railgun ammo");
            break;
        default:
            rm->model = getDefaultBallModel();
            break;
    }

    ScriptComponent *s = Entity_attachScriptComponent(e);
    ScriptComponent_allocateData(s, sizeof(float));
    ScriptComponent_setCallback(s, sp_itemScript);

    return e;
}

static void
_mp_updateScene(Entity *pawn, Transform *t, PhysicsComponent *pc, double dt)
{
    Scene3D *s      = &game.game_scene;
    s->delta    = dt;

    /* Velocity, acceleration etc. calculations */
    bool32 is_first_frame = s->physics_timer == 0.0f;
    s->physics_timer += dt;


#if _CF_FIXED_PHYSICS_TIMESTEP
    if (s->physics_timer >= s->physics_step || is_first_frame)
    {
        float rot0;
        float rot2;

        int num_steps;

        if (!is_first_frame)
            num_steps = (int)(s->physics_timer / s->physics_step);
        else
            num_steps = 1;

        for (int i = 0; i < num_steps; ++i)
        {
            mp_updateAndBufferPlayerInputs(pawn, s->physics_step);

            rot0 = t->rot[0];
            rot2 = t->rot[2];

            t->rot[0] = 0.f;
            t->rot[2] = 0.f;

            Scene3D_updatePhysics(s, s->physics_step);
            s->physics_timer = s->physics_timer - s->physics_step;

            /* Collision detection:
             * - Begin with preliminary detection
             * - Do more precise testing and generate events and cache entries
             * - Handle the generated collision events: physics responses and
             *   callbacks
             * - Remove old cache entries that are no longer valid */
            Scene3D_updateCollisions(s);

            /* After simulation we also have to store the most recent
             * pawn state after the newest input */
            input_sequence_t sequence = input_buffer.running_sequence - 1;
            StoredClientInput *inp = &input_buffer.sent_items[
                sequence % MAX_SENT_INPUT_ITEMS];
            vec3_copy(inp->position, t->pos);
            vec3_copy(inp->velocity, pc->velocity);

            t->rot[0] = rot0;
            t->rot[2] = rot2;
        }
    }
#else
#   error Fixed physics time step is required by the client
#endif

    Scene3D_updateScripts(s);
    Scene3D_updateAnimators(s, dt);
    Scene3D_updateLightPositions(s, dt);
    Scene3D_updateModels(s);
    Scene3D_updateLifetimes(s, dt);

    /* Remove... removed entities */
    for (Entity **ep = s->entities.removables;
         ep < &s->entities.removables[s->entities.num_removables];
         ++ep)
    {
        Scene3D_removeEntityInner(s, *ep);
    }

    s->entities.num_removables = 0;
}
