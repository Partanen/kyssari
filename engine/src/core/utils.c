#include "utils.h"
#include "engine.h"

float F_COLOR_WHITE[4]    = {1.0f, 1.0f, 1.0f, 1.0f};
float F_COLOR_BLACK[4]    = { 0.0f, 0.0f, 0.0f, 1.0f };
float F_COLOR_SYS_BLUE[4] = {0.0392f, 0.2431f, 0.7020f, 1.0f};
Color BYTE_COLOR_WHITE  = { 255,255,255,255 };
vec3 DEFAULT_UP_VECTOR  = {0.0f, 1.0f, 0.0f};

char *
readTextFileToBuffer(const char *path)
{
    char        *buffer;
    FILE        *file;
    long int    num_chars;

    file = fopen(path, "rb");

    if (!file)
    {
        return 0;
    }

    /* Get file length */
    fseek(file, 0L, SEEK_END);
    num_chars = ftell(file);
    rewind(file);

    buffer = (char*)kmalloc((num_chars + 1));
    fread(buffer, sizeof(char), num_chars, file);
    buffer[num_chars] = 0;
    fclose(file);

    return buffer;
}

void
stripSymbolsFromString(char *str, const char *symbols, int num_symbols)
{
    if (str && symbols && num_symbols > 0)
    {
        int len = strlen(str);

        const char *s, *ls;
        ls = symbols + num_symbols;

        for (char *c = str; *c; ++c)
        {
            for (s = symbols; s < ls; ++s)
            {
                if (*c == *s)
                {
                    /* Why does strcpy not work here on gcc? */
                    memcpy(c, c + 1, len - (int)(c - str));
                    c--;
                    len--;
                    break;
                }
            }
        }
    }
}

int
loadConfigFile(Configuration *configuration, const char *path)
{
    FILE        *file;

    file = fopen(path, "r");

    if (!file)
    {
        return 1;
    }

    fscanf(file, path, "%s=%s", configuration->key, configuration->value);
   // DEBUG_PRINTF();

    fclose(file);

    return 0;
}

String
createString(const char *text)
{
    String str = {0};

    if (text)
    {
        uint len = (uint)strlen(text);

        if (len)
        {
            void *mem = DynamicMemoryBlock_malloc(&engine.dynamic_memory,
                (len + 1) * sizeof(char));

            if (mem)
            {
                str.c_str  = (char*)mem;
                str.len    = len;
                strcpy(str.c_str, text);
            }
        }
    }

    return str;
}

void
freeString(String *string)
{
    if (!string->c_str) return;

    DynamicMemoryBlock_free(&engine.dynamic_memory, (void*)string->c_str);
    string->c_str   = 0;
    string->len     = 0;
}

void
purgeTrailingSpacesFromStr(char *str)
{
    if (!str) return;

    int len, i;
    len = strlen(str);

    for (i = 0; i < len; ++i)
        if (str[i] != ' ' && str[i] != '\t') break;

    len = len - i;
    memmove(str, str + i, len);
    str[len] = '\0';

    for (i = len - 1; i >= 0; --i)
    {
        if (str[i] != ' ' && str[i] != '\t') break;
        str[i] = '\0';
    }
}

int
parseConfigFile(const char *path,
    void (*callback)(const char *opt, const char *val))
{
    if (!path)      return 1;
    if (!callback)  return 2;
    FILE *f = fopen(path, "r");
    if (!f)         return 3;

    char    buf[512];
    char    opt_str[256];
    char    val_str[256];
    int     i, j, k, found_eq;

    while (fgets(buf, 512, f))
    {
        found_eq    = 0;
        opt_str[0]  = 0;
        val_str[0]  = 0;

        for (i = 0; i < 255; ++i)
        {
            if (!buf[i] || buf[i] == '\n')
                break;

            if (buf[i] != '=')
                opt_str[i] = buf[i];
            else
            {
                opt_str[i++]    = '\0';
                found_eq        = 1;
                break;
            }
        }

        if (!found_eq) continue;

        for (j = 0; j < 255; ++j)
        {
            k = i + j;

            if (!buf[k] || buf[k] == '\n')
                {val_str[j] = '\0'; break;}

            val_str[j] = buf[k];
        }

        purgeTrailingSpacesFromStr(opt_str);
        purgeTrailingSpacesFromStr(val_str);

        callback(opt_str, val_str);
    }

    fclose(f);
    return 0;
}
