#version 330 core

#define NUM_MAX_DIRECTIONAL_LIGHTS 2
#define NUM_MAX_POINT_LIGHTS 4

struct Material
{
    sampler2D   diffuse;
    sampler2D   specular;
    float       shine;
    vec3        color;
};

layout (std140) uniform Shader3DSharedMatrices
{
    mat4 projection;
    mat4 look_at;
    vec3 camera_pos;
};

layout (std140) uniform Shader3DDirectionalLights
{
    vec3 dir_ambient[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_direction[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_diffuse[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_specular[NUM_MAX_DIRECTIONAL_LIGHTS];
    int num_directional_lights;
};

layout (std140) uniform Shader3DPointLights
{
    vec3 pnt_position[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_diffuse[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_specular[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_ambient[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_constant_linear_quadratic[NUM_MAX_POINT_LIGHTS];
    int num_point_lights;
};

layout (std140) uniform Shader3DSceneAmbientLighting
{
    vec4 scene_ambient_light;
};

uniform Material    material;

in vec3             frag_pos;
in vec2             tex_coord;
in vec3             normal;
out vec4            frag_color;

vec3 combined_ambient;
vec3 combined_diffuse;
vec3 combined_specular;

void
calculateDirectionalLight(vec3 p_ambient, vec3 p_direction,
    vec3 p_diffuse, vec3 p_specular, vec3 view_direction)
{
    /* Diffuse lighting */
    float diffuse_strength = max(dot(normal, normalize(-p_direction)), 0.0f);
    vec3 diffuse = diffuse_strength * p_diffuse;

    /* Specular lighting */
    vec3 reflect_dir        = reflect(-p_direction, normal);
    float specular_strength = clamp(pow(max(dot(view_direction, reflect_dir), 0.0f),
        material.shine), 0, 1);
    vec3 specular = p_specular * specular_strength;

    /* Ambient lighing */
    vec3 ambient = p_ambient;

    combined_diffuse    += diffuse;
    combined_specular   += specular;
    combined_ambient    += ambient;
}

void
calculatePointLight(vec3 p_position, vec3 p_diffuse, vec3 p_specular,
    vec3 p_ambient, vec3 p_constant_linear_quadratic, vec3 view_direction)
{
    vec3 light_direction    = normalize(p_position - frag_pos);
    vec3 reflect_direction  = reflect(-light_direction, normal);
    float distance          = length(p_position - frag_pos);
    float attenuation = clamp(1.0f / (p_constant_linear_quadratic.x +
        p_constant_linear_quadratic.y * distance +
        p_constant_linear_quadratic.z * distance * distance), 0, 1);

    /* Diffuse lighting */
    float diffuse_strength = max(dot(normal, -light_direction), 0.0f);
    vec3 diffuse = diffuse_strength * p_diffuse;
    
    /* Specular lighting */
    float specular_strength = clamp(pow(max(dot(view_direction, 
        reflect_direction), 0.0f), material.shine), 0, 1);
    vec3 specular = p_specular * specular_strength;

    /* Ambient lighting */
    vec3 ambient = p_ambient;

    combined_diffuse    += diffuse;
    combined_specular   += specular;
    combined_ambient    += ambient;
}

void main(void)
{
    combined_ambient    = vec3(0.0f, 0.0f, 0.0f);
    combined_diffuse    = vec3(0.0f, 0.0f, 0.0f);
    combined_specular   = vec3(0.0f, 0.0f, 0.0f);
    vec3 view_direction = normalize(camera_pos - frag_pos);

    /* Directional lights */
    for (int i = 0; i < num_directional_lights; ++i)
    {
        calculateDirectionalLight(dir_ambient[i],
            dir_direction[i], dir_diffuse[i], dir_specular[i], view_direction);
    }

    /* Point lights */
    for (int i = 0; i < num_point_lights; ++i)
    {
        calculatePointLight(pnt_position[i], pnt_diffuse[i],
            pnt_specular[i], pnt_ambient[i], pnt_constant_linear_quadratic[i],
            view_direction);
    }

    vec3 scene_ambient = vec3(
        scene_ambient_light.x,
        scene_ambient_light.y,
        scene_ambient_light.z) * scene_ambient_light.w;

    combined_ambient += scene_ambient;

    vec3 material_diff = material.color + vec3(texture(material.diffuse, tex_coord));
    vec3 material_spec = vec3(texture(material.specular, tex_coord));

    vec3 diff   = combined_diffuse  * material_diff;
    vec3 amb    = combined_ambient  * material_diff;
    vec3 spec   = combined_specular * material_spec;

    frag_color = vec4(diff + spec + amb, 1.0f);
}
