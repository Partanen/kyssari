/* A shader that discards fragments where alpha is below a given treshold */

#version 330 core

#define NUM_MAX_DIRECTIONAL_LIGHTS 2
#define NUM_MAX_POINT_LIGHTS 4

struct Material
{
    sampler2D   diffuse;
    sampler2D   specular;
    float       shine;
    vec3        color;
};

layout (std140) uniform Shader3DSharedMatrices
{
    mat4 projection;
    mat4 look_at;
    vec3 camera_pos;
};


uniform Material    material;

in vec3             frag_pos;
in vec2             tex_coord;
in vec3             normal;
out vec4            frag_color;

void main(void)
{
    if (texture(material.diffuse,  tex_coord).a <= 0.80f
    &&  texture(material.specular, tex_coord).a <= 0.80f)
        discard;

    frag_color = texture(material.diffuse, tex_coord);
}
