#define CLIENT_RETRY_CONNECT_TIME       500
#define CLIENT_FPS                      20
#define SERVER_FPS                      20
#define AUTH_CONFIRM_RESEND_TIME        1500
#define SERVER_CLIENT_DISCONNECTED      -1
#define DEFAULT_SERVER_CLIENT_TIMEOUT   2000
#define DEFAULT_SERVER_TIMEOUT          2000

#define DO_NETWORK_PRINTING 1
#define VERBOSE_NETWORK_PRINTING 0

#if DO_NETWORK_PRINTING
#   define NW_DEBUG_PRINTF(str, ...) \
        DEBUG_PRINTF(str, ##__VA_ARGS__)
#   define NW_PRINTF(str, ...) \
         PRINTF(str, ##__VA_ARGS__)
#   define NW_PUTS(str) \
        PUTS(str)
#else
#   define NW_DEBUG_PRINTF(str, ...)
#   define NW_PRINTF(str, ...)
#   define NW_PUTS(str)
#endif

#define FORMATTED_IP_ADDR(_addr_ptr) \
    (int)Address_getX((_addr_ptr)), \
    (int)Address_getY((_addr_ptr)), \
    (int)Address_getZ((_addr_ptr)), \
    (int)Address_getW((_addr_ptr))

enum ConnectionReceiveError
{
    CONNECTION_RECEIVE_ERROR_SOCKET         = -1,
    CONNECTION_RECEIVE_ERROR_UNKNOWN_SENDER = -2
};

static int
_Connection_init(Connection *c, DynamicMemoryBlock *allocator,
    void (*on_receive_packet_callback)(void*, int, void*),
    void *packet_receive_args,
    void (*on_packet_acked_callback)(Packet*,void*),
    void *packet_ack_args);

static int
_Connection_setAddress(Connection *connection, Address *address);

static inline void
_Connection_destroyMessage(Connection *c, uint16_t message_id, Message *m);

/* Handle incoming data from a known connetion */
static int
_Connection_handleValidReceivedData(Connection *c, char *buffer, int data_size);

/* Called every frame. Constructs a packet from queued messages and sends it -
 * if no messages are in queue, a packet will still be sent for keep-alive
 * purposes. */
static int
_Connection_sendPacket(Connection *c, Socket *s,
    char *buf, int buf_len,
    char *error_log, int log_len);

/* Returns the amount of bytes received */
static int
_Connection_receivePackets(Connection *c, Socket *socket,
    char *buffer, int buffer_size);

static void
_Connection_cleanOldPackets(Connection *c);

static void
_Connection_destroyPacketAndMessages(Connection *connection, Packet *packet,
    uint16_t sequence);

static inline void *
_Connection_queueMessage(Connection *c,
    message_t type,
    uint data_size); /* Not including sizeof(message_t) + sizeof(uint16_t) */

static void
_Client_update(Client *client, char *error_log, int log_len);

static thread_ret_t
_Client_main(void *args);

static inline int
_Client_tryCopyMessageToScene(Client *c, void *msg, int size);

static void
_Client_receivePacketCallback(void *packet, int size, void *receive_args);

static void
_Server_receivePackets(Server *s);

static void
_Server_update(Server *s, char *error_log, int log_len);

static thread_ret_t
_Server_main(void *args);

static inline ServerClient *
_Server_getClientByAddress(Server *s, Address *a);

static ServerClient *
_Server_reserveClient(Server *server, Address *address);

static void
_Server_freeClient(Server *s, ServerClient *sc);

static void
_Server_disconnectAndFreeClient(Server *s, ServerClient *sc);

static void
_ServerClient_receivePacketCallback(void *packet,
    int size, void *receive_args);

static int
_Connection_init(Connection *c, DynamicMemoryBlock *allocator,
    void (*on_receive_packet_callback)(void*, int, void*),
    void *packet_receive_args,
    void (*on_packet_acked_callback)(Packet*,void*),
    void *packet_ack_args)
{
    if (SequenceBuffer_MessagePtr_init(&c->message_sb_out,
            allocator, 1024) != 0)
        return 1;

    if (FIFOQueue_MessagePtr_init(&c->message_queue_out, allocator, 1024) != 0)
        return 2;

    if (SequenceBuffer_Packet_init(&c->packet_sb_out, allocator, 1024) != 0)
        return 3;

    if (SequenceBuffer_uint16_t_init(&c->packet_id_sb_in, allocator, 32) != 0)
        return 4;

    c->allocator                    = allocator;

    /* Parameters */
    c->keep_alive_frequency             = 500;
    c->message_resend_frequency         = 100;
    c->on_receive_packet_callback       = on_receive_packet_callback;
    c->packet_receive_args              = packet_receive_args;
    c->on_packet_acked_callback         = on_packet_acked_callback;
    c->packet_ack_args                  = packet_ack_args;
    c->time_last_received_valid_data    = getTimePassed();
    c->oldest_unacked_message           = 0;
    c->have_unacked_messages            = 0;

    memset(c->latency_cache, 0, sizeof(c->latency_cache));
    c->average_latency = 0;

    return 0;
}

static int
_Connection_setAddress(Connection *c, Address *a)
{
    if (!c)             return 1;
    if (!a)             return 2;
    if (!c->allocator)  return 3;

    c->address                  = *a;
    c->message_id_out           = 0;
    c->oldest_unacked_message   = 0;
    c->packet_id_out            = 0;
    c->oldest_unacked_packet    = 0;
    c->last_received_packet     = 0;
    c->last_sent_something      = 0;

    SequenceBuffer_MessagePtr_clear(&c->message_sb_out);
    FIFOQueue_MessagePtr_clear(&c->message_queue_out);
    SequenceBuffer_Packet_clear(&c->packet_sb_out);
    SequenceBuffer_uint16_t_clear(&c->packet_id_sb_in);

    return 0;
}

static void
_Client_update(Client *client, char *error_log, int log_len)
{
    Clock_tick(&client->clock);

    if (client->on_update_callback)
        client->on_update_callback(client);

    _Connection_cleanOldPackets(&client->connection);

#   define RECEIVE_SEND_AND_CHECK_TIMEOUT() \
    uint32_t time_since_reply = getTimePassed() - \
        client->connection.time_last_received_valid_data; \
 \
    if (time_since_reply >= client->server_timeout) \
    { \
        client->status = CLIENT_STATUS_DISCONNECTED; \
        NW_PUTS("Client: disconnected due to a timeout!\n"); \
        break; \
    } \
 \
    int received = _Connection_receivePackets(&client->connection, \
        &client->socket, (char*)client->buffer.memory, \
        (int)client->buffer.max_size); \
    (void)received; \
 \
    int sent = _Connection_sendPacket(&client->connection, \
        &client->socket, (char*)client->buffer.memory, \
        (int)client->buffer.max_size, \
        error_log, log_len); \
 \
    if (sent < 0) \
    { \
        NW_PRINTF("Client: failed to send a packet - sock error: %s\n", \
            error_log); \
    }

    switch (client->status)
    {
        case CLIENT_STATUS_DISCONNECTED:
        {
        }
            break;
        case CLIENT_STATUS_CONNECTING:
        {
            /* Re-request connection if haven't received confirmation yet */
            uint time_passed = getTimePassed();
            uint time_since_connection_request = getTimePassed() - \
                client->last_sent_connection_request;

            if (time_since_connection_request >= CLIENT_RETRY_CONNECT_TIME)
            {
                _Connection_queueMessage(&client->connection,
                    CS_MSG_CONNECTION_REQUEST,
                    CS_MSG_CONNECTION_REQUEST_SIZE);
                client->last_sent_connection_request = time_passed;
            }

            RECEIVE_SEND_AND_CHECK_TIMEOUT();
        }
            break;
        case CLIENT_STATUS_CONNECTED:
        {
            RECEIVE_SEND_AND_CHECK_TIMEOUT();
        }
            break;
        default:
            break;
    }
#   undef RECEIVE_SEND_AND_CHECK_TIMEOUT
}

static thread_ret_t
_Client_main(void *args)
{
    Client *c = (Client*)args;
    char error_log[512];

    while (c->running && c->status != CLIENT_STATUS_DISCONNECTED)
        _Client_update(c, error_log, 512);

    /* Return early if we don't need to send a disconnect notify */
    if (c->status != CLIENT_STATUS_CONNECTED
    &&  c->status != CLIENT_STATUS_CONNECTING)
        return 0;

    void *msg = _Connection_queueMessage(&c->connection,
        CS_MSG_DISCONNECT_NOTIFY, CS_MSG_DISCONNECT_NOTIFY_SIZE);

    if (msg)
    {
        NW_PUTS("Client: sending disconnect notify to server.");

        int sent = _Connection_sendPacket(&c->connection, &c->socket,
            (char*)c->buffer.memory, (int)c->buffer.max_size,
            error_log, 512);

        if (sent < 0)
        {
            NW_PRINTF("Client: failed to send packet containing a disconnect "
                "notify - sock error: %s.\n", error_log);
        }
    }
    else
    {
        NW_PRINTF("Client: failed to sent disconect notify to server.\n");
    }

    return 0;
}

static inline int
_Client_tryCopyMessageToScene(Client *c, void *msg, int size)
{
    if (!c->game.is_on || !c->game.scene) return 1;
    void *mem = SimpleMemoryBlock_malloc(&c->game.scene->message_buffer,
        (size_t)size);
    if (!mem) return 2;
    memcpy(mem, msg, (size_t)size);
    return 0;
}

static void
_Client_receivePacketCallback(void *packet, int size, void *receive_args)
{
    Client *client = (Client*)receive_args;

#   define CHECK_MESSAGE_SIZE(type) \
        if ((size - offset) < type##_SIZE) {offset = size; break;}

#   define CHECK_MESSAGE_MIN_SIZE(type) \
    if ((size - offset ) < type##_MIN_SIZE) {offset = size; break;}

#   define CHECK_VARIABLE_DATA_SIZE() \
    if (size - offset < variable_data_size) {offset = size; break;}

#   define BUFFER_COPY_OFFSET() \
        (data + offset - (int)MIN_MESSAGE_SIZE)

#   define MESSAGE_TYPE_FULL_SIZE(type) \
        (size_t)(type##_SIZE + MIN_MESSAGE_SIZE)

#   define MESSAGE_TYPE_FULL_VARIABLE_SIZE(type) \
        (size_t)(type##_MIN_SIZE + MIN_MESSAGE_SIZE + variable_data_size)


    if (client->game.is_on && client->game.scene)
    {
        Mutex_lock(&client->game.scene->message_buffer_mutex);
    }

    char        *data = (char*)packet;
    int         offset = PACKET_HEADER_SIZE;
    message_t   msg_type;
    uint16_t    msg_id;
    uint16_t    *msg_entry;
    int         user_msg_received;

    while (size - offset >= (int)MIN_MESSAGE_SIZE)
    {
        msg_type = *((message_t*)((char*)data + offset));
        offset += (int)sizeof(message_t);

        msg_id = *((uint16_t*)((char*)data + offset));
        offset += (int)sizeof(uint16_t);

        msg_entry = SequenceBuffer_uint16_t_getBySequence(
            &client->message_id_sb_in, msg_id);

        switch (msg_type)
        {
            case SC_MSG_ACCEPT_CONNECTION:
            {
                CHECK_MESSAGE_SIZE(SC_MSG_ACCEPT_CONNECTION);

                if (!msg_entry)
                {
                    client->status = CLIENT_STATUS_CONNECTED;
                    if (client->on_connection_accepted_callback)
                        client->on_connection_accepted_callback(client);
                    SequenceBuffer_uint16_t_insert(&client->message_id_sb_in,
                        msg_id);
                    NW_PUTS("Client: server accepted connection!");
                }

                offset += SC_MSG_ACCEPT_CONNECTION_SIZE;
            }
                break;
            case SC_MSG_REFUSE_CONNECTION:
            {
                CHECK_MESSAGE_SIZE(SC_MSG_REFUSE_CONNECTION);

                enum ConnectionRefuseReason reason =
                    (enum ConnectionRefuseReason)*(char*)(data + offset);

                if (client->on_connection_refused_callback)
                    client->on_connection_refused_callback(client, reason);

                client->status = CLIENT_STATUS_DISCONNECTED;

                NW_PRINTF("Client: server sent connection refuse packet, "
                    "reason: %i.\n", (int)reason);

                offset = size;
            }
                break;
            case SC_MSG_INFORM_OF_ENTITY:
            {
                CHECK_MESSAGE_SIZE(SC_MSG_INFORM_OF_ENTITY);

                if (!msg_entry)
                {
                    DEBUG_PRINTF("INFORM OF ENTITY RECEIVED!\n");
                    _Client_tryCopyMessageToScene(client, BUFFER_COPY_OFFSET(),
                        MESSAGE_TYPE_FULL_SIZE(SC_MSG_INFORM_OF_ENTITY));

                    SequenceBuffer_uint16_t_insert(&client->message_id_sb_in,
                        msg_id);
                }

                offset += SC_MSG_INFORM_OF_ENTITY_SIZE;
            }
                break;
            case SC_MSG_ENTITY_POSITION_UPDATE:
            {
                CHECK_MESSAGE_SIZE(SC_MSG_ENTITY_POSITION_UPDATE);
                if (!msg_entry)
                {
                    _Client_tryCopyMessageToScene(client, BUFFER_COPY_OFFSET(),
                        MESSAGE_TYPE_FULL_SIZE(SC_MSG_ENTITY_POSITION_UPDATE));

                    SequenceBuffer_uint16_t_insert(
                        &client->message_id_sb_in, msg_id);
                }
                offset += SC_MSG_ENTITY_POSITION_UPDATE_SIZE;
            }
                break;
            case SC_MSG_ENTITY_VELOCITY_UPDATE:
            {
                CHECK_MESSAGE_SIZE(SC_MSG_ENTITY_VELOCITY_UPDATE);
                if (!msg_entry)
                {
                    int r = _Client_tryCopyMessageToScene(client,
                        BUFFER_COPY_OFFSET(),
                        MESSAGE_TYPE_FULL_SIZE(SC_MSG_ENTITY_VELOCITY_UPDATE));
                    (void)r;

                    SequenceBuffer_uint16_t_insert(&client->message_id_sb_in,
                        msg_id);
                }
                offset += SC_MSG_ENTITY_VELOCITY_UPDATE_SIZE;
            }
                break;
            case SC_MSG_ENTITY_ROTATION_UPDATE:
            {
                CHECK_MESSAGE_SIZE(SC_MSG_ENTITY_ROTATION_UPDATE);
                if (!msg_entry)
                {
                    _Client_tryCopyMessageToScene(client,
                        BUFFER_COPY_OFFSET(),
                        MESSAGE_TYPE_FULL_SIZE(SC_MSG_ENTITY_ROTATION_UPDATE));
                    SequenceBuffer_uint16_t_insert(&client->message_id_sb_in,
                        msg_id);
                }
                offset += SC_MSG_ENTITY_ROTATION_UPDATE_SIZE;
            }
                break;
            default:
            {
                if (client->on_receive_message_callback)
                {
                    user_msg_received = client->on_receive_message_callback(
                        client, msg_type, msg_id, (void*)(data + offset),
                        size - (offset), msg_entry != 0);

                    if (user_msg_received >= 0)
                        offset += user_msg_received;
                    else
                        offset = size;
                }
                else
                {
                    NW_DEBUG_PRINTF("UNDEF MSG %i RECEIVED\n", (int)msg_type);
                    user_msg_received = -1;
                    offset = size;
                }

                if (!msg_entry && user_msg_received >= 0)
                {
                    SequenceBuffer_uint16_t_insert(&client->message_id_sb_in,
                        msg_id);
                }
            }
                break;
        }

        /* If message is valid, insert id into the received message sequence
         * buffer */
    }

    if (client->game.is_on && client->game.scene)
    {
        Mutex_unlock(&client->game.scene->message_buffer_mutex);
    }

#   undef CHECK_MESSAGE_SIZE
#   undef CHECK_MESSAGE_MIN_SIZE
#   undef BUFFER_COPY_OFFSET
#   undef MESSAGE_TYPE_FULL_SIZE
#   undef MESSAGE_TYPE_FULL_VARIABLE_SIZE
}

static void
_Connection_destroyPacketAndMessages(Connection *c, Packet *p,
    uint16_t sequence)
{
    Message **msg;

    for (uint16_t i = 0; i < p->num_messages; ++i)
    {
        msg = SequenceBuffer_MessagePtr_getBySequence(&c->message_sb_out,
            p->messages[i].id);

        if (msg)
        {
            /* Check if this was the oldest unacked message. If yes, we have
             * to find the next oldest one. */
            if ((*msg)->id == c->oldest_unacked_message)
            {
                uint16_t start;

                if ((*msg)->id != 65535)
                    start = (*msg)->id + 1;
                else
                    start = 0;

                bool32 have_unacked_messages = 0;
                Message **omsg;

                for (uint16_t i = start;
                     i != c->message_id_out;
                     i = (i != 65535 ? i + 1 : 0))
                {
                    omsg = SequenceBuffer_MessagePtr_getBySequence(
                        &c->message_sb_out, i);

                    if (omsg)
                    {
                        c->oldest_unacked_message = (*omsg)->id;
                        have_unacked_messages = 1;
                        break;
                    }
                }

                c->have_unacked_messages = have_unacked_messages;
            }

            _Connection_destroyMessage(c, p->messages[i].id, *msg);
        }
    }

    SequenceBuffer_Packet_eraseBySequence(&c->packet_sb_out, sequence);
}

int
Client_init(Client *client)
{
    if (client->initialized)
        return CLIENT_ERROR_ALREADY_INITIALIZED;

    client->status = CLIENT_STATUS_DISCONNECTED;

    if (MemoryPool_init(&client->memory_pool, MEGABYTES(16)) != 0)
        return CLIENT_ERROR_OUT_OF_MEMORY;

    if (DynamicMemoryBlock_init(&client->memory, &client->memory_pool,
            MEGABYTES(16)) != 0)
        return CLIENT_ERROR_OUT_OF_MEMORY;

    void *buf_mem = allocatePermanentMemory(1024);
    if (!buf_mem) return CLIENT_ERROR_OUT_OF_MEMORY;
    ByteBuffer_init(&client->buffer, buf_mem, 1024);
    Clock_init(&client->clock, CLIENT_FPS);

    /* Parameters */
    client->server_timeout = DEFAULT_SERVER_TIMEOUT;

    /* Callbacks */
    client->on_receive_message_callback     = 0;
    client->on_connection_refused_callback  = 0;
    client->on_packet_acked_callback        = 0;
    client->on_packet_acked_args            = 0;

    client->game.scene = 0;
    client->game.is_on = 0;

    client->initialized = 1;

    return 0;
}

int
Client_setReplicatedScene(Client *client, Scene3D *scene)
{
    if (!scene)                 return 1;
    if (!scene->is_multiplayer) return 2;

    client->game.scene = scene;
    client->game.is_on = 1;

    return 0;
}

void
Client_setFPS(Client *client, uint fps)
{
    ASSERT(client->initialized);
    client->clock.target_fps = fps;
}

void *
Client_queueMessage(Client *client, message_t type, int data_size)
{
    ASSERT(data_size >= 0);
    return _Connection_queueMessage(&client->connection, type, data_size);
}

int
Client_connect(Client *client, Address *server)
{
    if (client->status == CLIENT_STATUS_CONNECTING)
        return CLIENT_ERROR_ALREADY_CONNECTING;

    if (client->status == CLIENT_STATUS_CONNECTED)
        return CLIENT_ERROR_ALREADY_CONNECTED;

    if (_Connection_init(&client->connection, &client->memory,
        _Client_receivePacketCallback, client,
        client->on_packet_acked_callback,
        client->on_packet_acked_args) != 0)
    {
        NW_DEBUG_PRINTF("Client_connect(): failed to init "
            "connection.\n");
        return CLIENT_ERROR_OUT_OF_MEMORY;
    }

    if (SequenceBuffer_uint16_t_init(&client->message_id_sb_in,
        &client->memory, 512) != 0)
    {
        return CLIENT_ERROR_OUT_OF_MEMORY;
    }

    if (_Connection_setAddress(&client->connection, server) != 0)
    {
        DEBUG_PRINTF("Client_connect() failed: could not set address using "
            "_Connection_setAddress()!\n");
        return CLIENT_ERROR_INVALID_PARAMETER;
    }

    if (Socket_open(&client->socket) != 0)
        return CLIENT_ERROR_SENDER_OPEN;

    if (Socket_bind(&client->socket, KYS_CLIENT_PORT) != 0)
        return CLIENT_ERROR_LISTENER_BIND;

    if (Socket_setNonBlocking(&client->socket) != 0)
        return CLIENT_ERROR_SOCK_OPTIONS;

    client->last_sent_connection_request = 0;

    client->status  = CLIENT_STATUS_CONNECTING;
    client->running = 1;

    Thread_create(&client->thread, _Client_main, client);

    return 0;
}

int
Client_disconnect(Client *c)
{
    if (!c) return CLIENT_ERROR_INVALID_PARAMETER;

    int ret;

    if (c->status == CLIENT_STATUS_CONNECTED
    ||  c->status == CLIENT_STATUS_CONNECTING)
        ret = 0;
    else
        ret = CLIENT_ERROR_NOT_CONNECTED;

    if (c->running)
    {
        c->running = 0;
        Thread_join(&c->thread);
    }

    Socket_close(&c->socket);
    c->status = CLIENT_STATUS_DISCONNECTED;

    NW_PUTS("Client: disconnected!");

    return ret;
}

int
Server_init(Server *server, uint max_clients)
{
    if (!server)                return SERVER_ERROR_INVALID_PARAMETER;
    if (server->initialized)    return SERVER_ERROR_ALREADY_INITIALIZED;

    server->send_buffer_memory = allocatePermanentMemory(KYS_MTU * 2);
    if (!server->send_buffer_memory) return SERVER_ERROR_OUT_OF_MEMORY;

    ByteBuffer_init(&server->send_buffer, server->send_buffer_memory, KYS_MTU);
    server->recv_buffer = ((char*)server->send_buffer_memory) + KYS_MTU;
    server->recv_buffer_size = KYS_MTU;

    server->clients = allocatePermanentMemory(max_clients * sizeof(ServerClient));
    if (!server->clients)
        return SERVER_ERROR_OUT_OF_MEMORY;
    server->max_clients = max_clients;

    /* Callbacks */
    server->on_connection_request_callback  = 0;
    server->on_accept_client_callback       = 0;
    server->on_client_disconnected_callback = 0;
    server->on_update_callback              = 0;
    server->on_receive_message_callback     = 0;
    server->on_client_notified_disconnect   = 0;

    /* Default parameters */
    server->client_timeout = DEFAULT_SERVER_CLIENT_TIMEOUT;

    if (MemoryPool_init(&server->memory_pool, max_clients * MEGABYTES(8) + \
        MEGABYTES(1)) != 0)
        return SERVER_ERROR_OUT_OF_MEMORY;

    for (ServerClient *sc = server->clients;
         sc < &server->clients[max_clients];
         ++sc)
    {
        if (DynamicMemoryBlock_init(&sc->memory, &server->memory_pool,
            MEGABYTES(8)) != 0)
        {
            NW_DEBUG_PRINTF("Server_init(): failed to init a client's memory.\n");
            return SERVER_ERROR_OUT_OF_MEMORY;
        }
    }

    for (ServerGame *sg = server->games;
         sg < &server->games[MAX_SERVER_GAMES];
         ++sg)
    {
        sg->server = server;
    }

    Clock_init(&server->clock, SERVER_FPS);

    server->initialized = 1;
    return 0;
}

int
Server_run(Server *s)
{
    if (!s->initialized)
        return SERVER_ERROR_NOT_INITIALIZED;

    if (s->running)
        return SERVER_ERROR_ALREADY_RUNNING;

    /* Initialize the linked list of client slots */
    for (ServerClient *sc = s->clients;
         sc < &s->clients[s->max_clients];
         ++sc)
    {
        sc->next = sc + 1;
        sc->prev = sc - 1;
    }

    s->clients[0].prev                  = 0;
    s->clients[s->max_clients - 1].next = 0;
    s->free_clients                     = s->clients;
    s->active_clients                   = 0;
    s->num_bytes_received               = 0;

    /* Open and bind the server socket */
    if (Socket_open(&s->socket) != 0)
        return SERVER_ERROR_LISTENER_OPEN;

    if (Socket_bind(&s->socket, KYS_SERVER_PORT) != 0)
        return SERVER_ERROR_LISTENER_BIND;

    if (Socket_setNonBlocking(&s->socket) != 0)
        return SERVER_ERROR_SOCK_OPTIONS;

    s->running = 1;
    Thread_create(&s->thread, _Server_main, s);

    return 0;
}

int
Server_shutdown(Server *s)
{
    if (!s)                 return SERVER_ERROR_INVALID_PARAMETER;
    if (!s->initialized)    return SERVER_ERROR_NOT_INITIALIZED;
    if (!s->running)        return SERVER_ERROR_NOT_RUNNING;
    s->running = 0;
    /* TODO: At the end of the main function, tell all clients you're closing */
    Thread_join(&s->thread);
    Socket_close(&s->socket);
    return 0;
}

/* The int returned is a handle to the game and should be saved.
 * Returns -1 if unsuccessful. */
ServerGame *
Server_createGame(Server *server, Scene3D *scene, uint max_players)
{
    if (!server)                                        return 0;
    if (scene && !scene->is_multiplayer)                return 0;
    if (max_players <= 0 || max_players > MAX_CLIENTS)  return 0;
    if (server->num_games >= MAX_SERVER_GAMES)          return 0;

    int index = server->num_games;

    server->games[index].scene          = scene;
    server->games[index].max_players    = max_players;
    server->games[index].num_players    = 0;

    ++server->num_games;

    return &server->games[index];
}

void
ServerGame_setReplicatedScene(ServerGame *game, Scene3D *scene)
{
    if (!game) return;
    game->scene = scene;
}

int
ServerGame_attachClient(ServerGame *g, ServerClient *c)
{
    if (!g) return 1;
    if (!c) return 2;

    for (ServerGamePlayer *cp = g->players;
         cp < &g->players[g->num_players];
         ++cp)
    {
        if (cp->client == c)
        {
            return 3;
        }
    }

    g->players[g->num_players].client = c;
    c->is_in_game = 1;
    ++g->num_players;

    if (ServerGame_informPlayerOfAllEntities(g, c) != 0)
    {
        NW_PUTS("ServerGame_attachClient(): failed to inform new player "
            "of all entities!");
    }

    NW_DEBUG_PRINTF("Server: attached client to game - num players in game: "
        "%i.\n", g->num_players);

    return 0;
}

int
ServerGame_informPlayersOfEntity(ServerGame *game, Entity *entity)
{
    if (!Entity_isReplicated(entity)) return 1;

    Transform *t = &Entity_getTransform(entity)->transform;

    void *msg_data;

    ByteBuffer b;
    b.max_size = SC_MSG_INFORM_OF_ENTITY_SIZE;

    int i = 0;

    for (ServerGamePlayer *sgp = game->players;
         sgp < &game->players[game->num_players];
         ++sgp)
    {
        msg_data = Server_queueMessage(game->server, sgp->client,
            SC_MSG_INFORM_OF_ENTITY, SC_MSG_INFORM_OF_ENTITY_SIZE);

        if (msg_data)
        {
            b.memory = msg_data;
            b.offset = 0;
            SC_MSG_INFORM_OF_ENTITY_serialize(&b, entity->replication_id,
                entity->type, t->pos);
           ++i;
        }
        else
        {
            NW_PRINTF("Server: failed to allocate message to inform player "
                "%i.%i.%i.%i of an entity!",
                FORMATTED_IP_ADDR(&sgp->client->connection.address));
        }
    }

    NW_DEBUG_PRINTF("Server: informed %i clients of entity of replication id "
        "%u.\n", i, entity->replication_id);

    return 0;
}

int
ServerGame_informPlayerOfAllEntities(ServerGame *game, ServerClient *client)
{
    if (Scene3D_getNumReplicatedEntities(game->scene) <= 0) return 0;

    ReplicationTable *table = &game->scene->replication_table;

    ByteBuffer  buf;
    void        *msg;

    buf.max_size = SC_MSG_INFORM_OF_ENTITY_SIZE;

    for (ReplicationTableEntry *rte = table->reserved_entries;
         rte;
         rte = rte->reserved_next)
    {
        msg = _Connection_queueMessage(&client->connection,
            SC_MSG_INFORM_OF_ENTITY, SC_MSG_INFORM_OF_ENTITY_SIZE);

        if (!msg)
        {
            NW_PRINTF("Server: failed to inform client of some entities!\n");
            return 1;
        }

        buf.offset = 0;
        buf.memory = msg;

        SC_MSG_INFORM_OF_ENTITY_serialize(&buf, rte->entity->replication_id,
            rte->entity->type, Entity_getTransform(rte->entity)->transform.pos);

        DEBUG_PRINTF("Server: informed player %i.%i.%i.%i of entity of "
            "replication id %u!\n",
            FORMATTED_IP_ADDR(&client->connection.address),
            rte->entity->replication_id);
    }

    return 0;
}

void *
Server_queueMessage(Server *server,
    ServerClient *client,
    message_t type,
    int data_size)
{
    ASSERT(data_size >= 0);
    return _Connection_queueMessage(&client->connection, type, data_size);
}

void
Server_setFPS(Server *server, uint fps)
{
    ASSERT(server->initialized);
    server->clock.target_fps = fps;
}

static void
_Server_updateOngoingGames(Server *s)
{
    void        *msg;
    Transform   *t;
    ByteBuffer  write_buffer;

    for (ServerGame *sg = s->games; sg < &s->games[s->num_games]; ++sg)
    {
        if (!sg->scene) continue;

        Mutex_lock(&sg->scene->entity_access_mutex);

        /* Entity positions */
        for (ReplicationTableEntry *rte = sg->scene->replication_table.reserved_entries;
             rte; rte = rte->reserved_next)
        {
            if (!GET_BITFLAG(rte->flags, replication_bitmask_t,
                REPLICATION_FLAG_WAS_BROADCAST))
            {
                ServerGame_informPlayersOfEntity(sg, rte->entity);
                SET_BITFLAG_ON(rte->flags, replication_bitmask_t,
                    REPLICATION_FLAG_WAS_BROADCAST);
                NW_PUTS("Server: broadcast a new entity to all players!");
            }

            for (ServerGamePlayer *player = sg->players;
                 player < &sg->players[sg->num_players];
                 ++player)
            {
                t = &Entity_getTransform(rte->entity)->transform;

                /* Position */
                if (!GET_BITFLAG(rte->flags, replication_bitmask_t,
                    REPLICATION_FLAG_DONT_SEND_POSITION))
                {
                    msg = _Connection_queueMessage(
                        &player->client->connection,
                        SC_MSG_ENTITY_POSITION_UPDATE,
                        SC_MSG_ENTITY_POSITION_UPDATE_SIZE);

                    if (msg)
                    {
                        ByteBuffer_init(&write_buffer, msg,
                            SC_MSG_ENTITY_POSITION_UPDATE_SIZE);

                        SC_MSG_ENTITY_POSITION_UPDATE_serialize(&write_buffer,
                            rte->entity->replication_id, t->pos);
                    }
                }

                /* Rotation */
                if (!GET_BITFLAG(rte->flags, replication_bitmask_t,
                    REPLICATION_FLAG_DONT_SEND_ROTATION))
                {
                    msg = _Connection_queueMessage(&player->client->connection,
                        SC_MSG_ENTITY_ROTATION_UPDATE,
                        SC_MSG_ENTITY_ROTATION_UPDATE_SIZE);

                    if (msg)
                    {
                        ByteBuffer_init(&write_buffer, msg,
                            SC_MSG_ENTITY_ROTATION_UPDATE_SIZE);
                        SC_MSG_ENTITY_ROTATION_UPDATE_serialize(
                            &write_buffer, rte->entity->replication_id, t->rot);
                    }
                }

                /* Velocity */
                if (!GET_BITFLAG(rte->flags, replication_bitmask_t,
                    REPLICATION_FLAG_DONT_SEND_VELOCITY))
                {
                    PhysicsComponent *pc = Entity_getPhysicsComponent(
                        rte->entity);

                    if (pc)
                    {
                        msg = _Connection_queueMessage(
                            &player->client->connection,
                            SC_MSG_ENTITY_VELOCITY_UPDATE,
                            SC_MSG_ENTITY_VELOCITY_UPDATE_SIZE);

                        if (msg)
                        {
                            ByteBuffer_init(&write_buffer, msg,
                                SC_MSG_ENTITY_VELOCITY_UPDATE_SIZE);

                            SC_MSG_ENTITY_VELOCITY_UPDATE_serialize(
                                &write_buffer, rte->entity->replication_id,
                                pc->velocity);
                        }
                    }
                }
            }
        }

        Mutex_unlock(&sg->scene->entity_access_mutex);
    }
}

static void
_Server_update(Server *s, char *error_log, int log_len)
{
    Clock_tick(&s->clock);

    if (s->on_update_callback)
        s->on_update_callback(s);

    /* Update all game clients this server has to date */

    _Server_updateOngoingGames(s);
    _Server_receivePackets(s);

    bool32 client_timed_out;
    uint32_t time_since_reply;
    uint32_t time_passed = getTimePassed();

    /* Clean up all of our clients, then update them to date */
    ServerClient *c = s->active_clients;

    for (;c;)
    {
        client_timed_out = 0;

        _Connection_cleanOldPackets(&c->connection);

        /* Disconnect this client if it hasn't replied in a long enough time */
        time_since_reply = time_passed - \
            c->connection.time_last_received_valid_data;

        if (time_since_reply >= s->client_timeout)
        {
            _Connection_queueMessage(&c->connection,
                SC_MSG_TIMEOUT_DISCONNECT_NOTIFY,
                SC_MSG_TIMEOUT_DISCONNECT_NOTIFY_SIZE);
            /* This message is of size 0 */
            client_timed_out = 1;
        }

        /* Server: send packets to authed client */
        int sent = _Connection_sendPacket(&c->connection, &s->socket,
            s->send_buffer.memory, (int)s->send_buffer.max_size,
            error_log, 512);

        if (sent < 0)
        {
            sys_getSockErrorString(error_log, 512);
            NW_PRINTF("KysServer: error sending packet - %s\n",
                error_log);
        }
        else
            s->num_bytes_sent += (size_t)sent;

        if (client_timed_out)
        {
            ServerClient *next = c->next;
            NW_PRINTF("Server: disconnecting client %i.%i.%i.%i due to a "
                "timeout.\n", FORMATTED_IP_ADDR(&c->connection.address));
            _Server_disconnectAndFreeClient(s, c);
            c = next;
        }
        else
            c = c->next;
    }
}

static thread_ret_t
_Server_main(void *args)
{
    Server *s = (Server*)args;
    char error_log[512];

    NW_PUTS("Server: on!");

    while (s->running)
    {
        _Server_update(s, error_log, 512);
    }

    NW_PUTS("Server: shutting down.\n");

    return 0;
}

static void
_Server_receivePackets(Server *s)
{
    Address         addr;
    int             size;
    ServerClient    *c;

    do
    {
        size = Socket_receiveFrom(&s->socket,
            s->recv_buffer, (size_t)s->recv_buffer_size, &addr);

        if (size > 0)
            s->num_bytes_received += (size_t)size;

        if (size < (int)MIN_PACKET_SIZE)
            continue;

        c = _Server_getClientByAddress(s, &addr);

        if (!c)
        {
            int offset = (int)MIN_PACKET_SIZE;

            if (size - offset < CS_MSG_CONNECTION_REQUEST_SIZE)
            {
                NW_DEBUG_PRINTF("Server: Received an invalid package from "
                    "an unknown client.\n");
                continue;
            }

            message_t msg_id = *(message_t*)((char*)s->recv_buffer + offset);

            if (msg_id != CS_MSG_CONNECTION_REQUEST)
                continue;

            bool32 accept;

            if (s->on_connection_request_callback)
                accept = s->on_connection_request_callback(s, &addr);
            else
                accept = 1;

            if (accept)
            {
                c = _Server_reserveClient(s, &addr);

                if (!c)
                {
                    NW_PUTS("Server: received new connection, but maximum "
                        "client amount reached!\n");
                    continue;
                }

                _Connection_queueMessage(&c->connection,
                    SC_MSG_ACCEPT_CONNECTION, SC_MSG_ACCEPT_CONNECTION_SIZE);
                c->status = SERVER_CLIENT_AUTHED;

                NW_PRINTF("Server: Accepted connection %i.%i.%i.%i.\n",
                    FORMATTED_IP_ADDR(&c->connection.address));

                if (s->on_accept_client_callback)
                    s->on_accept_client_callback(s, c);
            }
            else
            {
                /* TODO: create a refused connection cache to not resend
                 * this every time on request to malicious users. */

                ByteBuffer buffer;
                ByteBuffer_init(&buffer, s->send_buffer_memory,
                    s->send_buffer.max_size);

                WRITE_PACKET_HEADER(&buffer, 0, 0, 0);

                SC_MSG_REFUSE_CONNECTION_serialize(&buffer,
                    CONNECTION_REFUSE_NO_REASON);

                int sent = Socket_send(&s->socket, &addr,
                    s->send_buffer_memory,
                    PACKET_HEADER_SIZE + SC_MSG_REFUSE_CONNECTION_SIZE);

                if (sent != -1)
                {
                    NW_PRINTF("Server: Sent a connection refused message to"
                        " client %i.%i.%i.%i.\n", FORMATTED_IP_ADDR(&addr));
                }
                else
                {
                    NW_PRINTF("Server: failed to send a connection refused "
                        "message to client %i.%i.%i.%i.\n",
                        FORMATTED_IP_ADDR(&addr));
                }

                continue;
            }
        }

        /* Client already exists */
        s->currently_handled_client = c;
        _Connection_handleValidReceivedData(&c->connection,
            s->recv_buffer, size);
    }
    while (size != -1);
}

static inline ServerClient *
_Server_getClientByAddress(Server *s, Address *a)
{
    for (ServerClient *sc = s->active_clients; sc; sc = sc->next)
    {
        if (a->address == sc->connection.address.address)
            return sc;
    }

    return 0;
}

static ServerClient *
_Server_reserveClient(Server *s, Address *address)
{
    ServerClient *c = s->free_clients;
    if (!c) return 0;

    /* Initialize client memory if haven't done so yet */
    DynamicMemoryBlock_clear(&c->memory);

    Address client_address  = *address;
    client_address.port     = KYS_CLIENT_PORT;

    /* Re-initialization here should be fine (?) because we cleared the
     * memory block above */
    if (_Connection_init(&c->connection, &c->memory,
        _ServerClient_receivePacketCallback, s, 0, 0) != 0)
    {
        NW_DEBUG_PRINTF("_Server_reserveClient(): failed to init "
            "connection.\n");
        return 0;
    }

    if (SequenceBuffer_uint16_t_init(&c->message_id_sb_in, &c->memory, 512) != 0)
    {
        DEBUG_PRINTF("_Server_reserveClient(): failed to init message_id_sb_in"
            " buffer\n");
        return 0;
    }

    ASSERT(c->memory.start);
    if (_Connection_setAddress(&c->connection, &client_address) != 0)
    {
        NW_DEBUG_PRINTF("_Server_reserveClient(): failed to set connection"
            " address!\n");
    }

    s->free_clients             = c->next;
    c->status                   = SERVER_CLIENT_UNAUTHED;
    c->last_replied_to          = 0;
    if (s->active_clients)
        s->active_clients->prev = c;
    c->next                     = s->active_clients;
    c->prev                     = 0;
    c->is_in_game               = 0;
    c->user_data                = 0;
    c->reserved                 = 1;
    s->active_clients           = c;

    s->num_active_clients++;

    return c;
}

static void
_Server_freeClient(Server *s, ServerClient *sc)
{
    if (sc->is_in_game)
    {
        /* Remove the client from any clients they were a part of */
        uint i;

        for (ServerGame *game = s->games;
             game < &s->games[s->num_games];
             ++game)
        {
            for (i = 0; i < game->num_players; ++i)
            {
                if (game->players[i].client == sc)
                {
                    if (game->num_players != 1)
                        game->players[i] = game->players[s->num_games - 1];

                    --game->num_players;
                    NW_PRINTF("Server: removed a client from game!\n");
                    break;
                }
            }
        }
    }

    if (sc->prev)
        sc->prev->next = sc->next;
    else
        s->active_clients = sc->next;

    if (sc->next)
        sc->next->prev = sc->prev;

    sc->next = s->free_clients;
    if (s->free_clients) s->free_clients->prev = sc;
    sc->prev = 0;
    s->free_clients = sc;

    sc->reserved = 0;

    s->num_active_clients -= 1;
}

static void
_Server_disconnectAndFreeClient(Server *s, ServerClient *sc)
{
    if (s->on_client_disconnected_callback)
        s->on_client_disconnected_callback(s, sc);
    _Server_freeClient(s, sc);
}

static void
_ServerClient_receivePacketCallback(void *packet, int size, void *receive_args)
{
#   define CHECK_MESSAGE_SIZE(type) \
        if ((size - offset) < type##_SIZE) {offset = size; break;}

#   define BUFFER_COPY_OFFSET() \
        (data + offset - (int)sizeof(message_t) - (int)sizeof(uint16_t))

#   define MESSAGE_TYPE_FULL_SIZE(type) \
        (size_t)(type##_SIZE + sizeof(message_t) + sizeof(uint16_t))

    Server *server              = (Server*)receive_args;
    ServerClient *server_client = server->currently_handled_client;

    char    *data   = (char*)packet;
    int     offset  = MIN_PACKET_SIZE;

    int         user_msg_received;
    message_t   msg_type;
    uint16_t    msg_id;
    uint16_t    *msg_entry;

    if (size - offset < MIN_MESSAGE_SIZE
    && server_client->status == SERVER_CLIENT_UNAUTHED)
    {
        NW_PUTS("Server: received a message too small from an unauthed "
            "connection. Disconnecting...");
        _Server_freeClient(server, server_client);
        return;
    }

    for (;size - offset >= MIN_MESSAGE_SIZE;)
    {
        msg_type = *(message_t*)(data + offset);
        offset += (int)sizeof(message_t);

        msg_id = *(uint16_t*)(data + offset);
        offset += (int)sizeof(uint16_t);

        msg_entry = SequenceBuffer_uint16_t_getBySequence(
            &server_client->message_id_sb_in, msg_id);

        if (server_client->status != SERVER_CLIENT_AUTHED
        && msg_type != CS_MSG_CONNECTION_REQUEST)
        {
            NW_PUTS("Server: Received a message that was not a connection request "
                "from an unauthed client.");
            _Server_freeClient(server, server_client);
            return;
        }

        switch (msg_type)
        {
            case CS_MSG_CONNECTION_REQUEST:
            {
                offset += CS_MSG_CONNECTION_REQUEST_SIZE;
            }
                break;
            case CS_MSG_DISCONNECT_NOTIFY:
            {
                char code = *(data + offset);
                if (server->on_client_notified_disconnect)
                {
                    server->on_client_notified_disconnect(server,
                        server_client, code);
                }
                NW_PRINTF("Server: client %i.%i.%i.%i requested disconnect - "
                    "removing!\n",
                    FORMATTED_IP_ADDR(&server_client->connection.address));
                _Server_disconnectAndFreeClient(server, server_client);
                offset = size;
            }
                break;
            default:
            {
                /* If the message wasn't of an inbuilt type, call the user
                 * callback (if provided) */

                /* TODO: Insert msg into the sequence buffer!!! */

                if (server->on_receive_message_callback)
                {
                    user_msg_received =
                        server->on_receive_message_callback(server,
                        server_client, msg_type, msg_id, (void*)(data + offset),
                        size - offset, msg_entry != 0);

                    if (user_msg_received >= 0)
                        offset += user_msg_received;
                    else
                        offset = size;
                }
                else
                {
                    offset = size;
                }

                if (!msg_entry && user_msg_received >= 0)
                {
                    SequenceBuffer_uint16_t_insert(
                        &server_client->message_id_sb_in, msg_id);
                }
            }
                break;
        }
    }

#   undef CHECK_MESSAGE_SIZE
#   undef CHECK_MESSAGE_MIN_SIZE
#   undef BUFFER_COPY_OFFSET
#   undef MESSAGE_TYPE_FULL_SIZE
}

/* Returns amount of bytes sent, or -1 on socket error */
static int
_Connection_sendPacket(Connection *c, Socket *s,
    char *buffer, int buf_len,
    char *error_log, int log_len)
{
    int ret = 0;
    bool32 have_messages = 0;

    /* TODO: send multiple packets in case we have more messages */

    for (uint16_t i = c->oldest_unacked_message;
        i != c->message_id_out;
        ++i)
    {
        if (SequenceBuffer_MessagePtr_exists(&c->message_sb_out, i))
        {
            have_messages = 1;
            break;
        }
    }

    uint32_t time                   = getTimePassed();
    uint32_t time_passed            = time - c->last_sent_something;
    bool32 create_keep_alive_packet = time_passed >= c->keep_alive_frequency;

    if (have_messages || create_keep_alive_packet)
    {
        /* Generate a packet */

        ret = (int)MIN_PACKET_SIZE;

        Packet *p = SequenceBuffer_Packet_insert(&c->packet_sb_out,
            c->packet_id_out);
        p->num_messages = 0;
        p->send_time    = getTimePassed();

        /* Generate ack bits to tell the receiver which of the last 32 packets
         * sent by them we have received */
        p->ack_bits = 0;

        uint32_t bit = 0;
        uint16_t start;

        if (c->last_received_packet >= 32)
            start = c->last_received_packet - 32;
        else
            start = 65535 - (32 - c->last_received_packet);

        for (uint16_t i = start; i != c->last_received_packet; ++i)
        {
            uint16_t *id = SequenceBuffer_uint16_t_getBySequence(
                &c->packet_id_sb_in, i);

            /* If packet is found, it has not been acked */
            if (id)
                SET_BITFLAG_ON(p->ack_bits, uint32_t, (1 << bit));

            ++bit;
        }

        /* Serialize the packet */
        ByteBuffer b;
        ByteBuffer_init(&b, buffer, (uint)buf_len);
        WRITE_PACKET_HEADER(&b, c->packet_id_out,
            c->last_received_packet, p->ack_bits);

        /* First, send any new messages we have, as those have the highest priority */
        Message *msg;
        uint32_t time_passed = getTimePassed();

        while (!FIFOQueue_MessagePtr_isEmpty(&c->message_queue_out) &&
               p->num_messages < 32)
        {
#ifdef _DEBUG
            if (c->message_queue_out.num < 0)
                NW_DEBUG_PRINTF("Message queue num was less than 0 (%i)???\n",
                    c->message_queue_out.num);
#endif
            msg = FIFOQueue_MessagePtr_pop(&c->message_queue_out);
            ASSERT(msg);

            if (!c->have_unacked_messages)
            {
                c->oldest_unacked_message   = msg->id;
                c->have_unacked_messages    = 1;
            }

            /* Remember the oldest message contained in this packet */
            if (p->num_messages == 0
            || IS_SEQUENCE_GREATER(p->oldest_contained_message, msg->id))
            {
                p->oldest_contained_message = msg->id;
            }

            ByteBuffer_write(&b, message_t, msg->type);
            ByteBuffer_write(&b, uint16_t, msg->id);

            if (ByteBuffer_writeBytes(&b, msg + 1, (uint)msg->size) != 0)
            {
                NW_DEBUG_PRINTF("Couldn't write bytes!\n");
                break;
            }

            msg->time_last_sent = time_passed;

            p->messages[p->num_messages].type   = msg->type;
            p->messages[p->num_messages].id     = msg->id;
            ++p->num_messages;

            /* TODO: ALSO CHECK PACKAGE MAX SIZE HERE !!! */
        }

        /* Pack in possible extra messages that need resending as they have
         * not been acked */
        Message **msg_ptr;

        if (b.max_size - b.offset >= MIN_MESSAGE_SIZE && p->num_messages < 32)
        {
            for (uint16_t i = c->oldest_unacked_message;
                 i != c->message_id_out;
                 i = (i != 65535 ? i + 1 : 0))
            {
                msg_ptr = SequenceBuffer_MessagePtr_getBySequence(
                    &c->message_sb_out, i);

                if (!msg_ptr) continue;

                msg = *msg_ptr;
                ASSERT(msg);

                /* If the message was already sent just now, continue */
                if (msg->time_last_sent == time_passed)
                {
                    continue;
                }

                if (!c->have_unacked_messages)
                {
                    c->oldest_unacked_message   = msg->id;
                    c->have_unacked_messages    = 1;
                }

                /* Remember the oldest message contained in this packet */
                if (p->num_messages == 0
                || IS_SEQUENCE_GREATER(p->oldest_contained_message, msg->id))
                {
                    p->oldest_contained_message = msg->id;
                }

                if ((msg->time_last_sent == 0
                || (time_passed - msg->time_last_sent >= c->message_resend_frequency))
                && (b.max_size - b.offset) >= MIN_MESSAGE_SIZE + msg->size)
                {
                    ByteBuffer_write(&b, message_t, msg->type);
                    ByteBuffer_write(&b, uint16_t, msg->id);

                    if (ByteBuffer_writeBytes(&b, msg + 1, (uint)msg->size) != 0)
                        break;

                    msg->time_last_sent = time_passed;
                }

                p->messages[p->num_messages].type   = msg->type;
                p->messages[p->num_messages].id     = msg->id;
                ++p->num_messages;

                if (p->num_messages >= 32)
                    break;

                /* TODO: ALSO CHECK PACKET MAX SIZE HERE (MTU) */
            }
        }

        /* Finally, actually send the serialized packet */
        if (Socket_send(s, &c->address, b.memory, b.offset) < 0)
        {
            if (error_log && log_len > 0)
            {
                sys_getSockErrorString(error_log, log_len);
                NW_DEBUG_PRINTF("_Connection_sendPacket(): Failed! Sock "
                    "error: %s.\n", error_log);
            }
            else
                NW_DEBUG_PRINTF("_Connection_sendPacket(): Failed!\n");

            ret = -1;
        }

        c->last_sent_something = time;

        ++c->packet_id_out;
    }

    return ret;
}

/* Do not call if the message is still in the message queue!\n" */
static inline void
_Connection_destroyMessage(Connection *c, uint16_t message_id, Message *m)
{
    DynamicMemoryBlock_unsafeFree(c->allocator, m);
    SequenceBuffer_MessagePtr_eraseBySequence(&c->message_sb_out, message_id);
}

static int
_Connection_handleValidReceivedData(Connection *c, char *buffer, int data_size)
{
    ByteBuffer b;
    ByteBuffer_init(&b, buffer, (uint)data_size);

    /* Ack any packets we should */
    uint16_t packet_id;
    uint16_t last_received_by_other;
    uint32_t ack_bits;

    ByteBuffer_read(&b, uint16_t, &packet_id);
    ByteBuffer_read(&b, uint16_t, &last_received_by_other);
    ByteBuffer_read(&b, uint32_t, &ack_bits);

    if (packet_id > c->last_received_packet)
        c->last_received_packet = packet_id;

    /* Insert the id of the packet into the in-coming sequence buffer */
    SequenceBuffer_uint16_t_insert(&c->packet_id_sb_in, packet_id);

#if VERBOSE_NETWORK_PRINTING
    NW_PRINTF("Size: %i \t-\tSequence: %i \t-\tlast_received_by_other: %i "
        "\t-\tack_bits: %u\n",
        data_size, (int)c->last_received_packet, (int)last_received_by_other,
        ack_bits);
#endif

    uint32_t time_passed = getTimePassed();

    /* Ack any possible packets we've sent to this connection. */

#   define ACK_AND_FREE_PACKET(_p, _sequence) \
    /* Calculate average latency */ \
    memcpy(c->latency_cache, c->latency_cache + 1, \
        (NUM_LATENCY_CACHE_ENTRIES - 1) * sizeof(uint32_t)); \
    c->latency_cache[0] = time_passed - p->send_time; \
    uint32_t accum = 0; \
    for (uint i = 0; i < NUM_LATENCY_CACHE_ENTRIES; ++i) \
        accum += c->latency_cache[i]; \
    c->average_latency = accum / NUM_LATENCY_CACHE_ENTRIES; \
    if (c->on_packet_acked_callback) \
        c->on_packet_acked_callback(p, c->packet_ack_args); \
    _Connection_destroyPacketAndMessages(c, _p, _sequence);

    Packet *p;

    p = SequenceBuffer_Packet_getBySequence(&c->packet_sb_out,
        last_received_by_other);

    if (p)
    {
        ACK_AND_FREE_PACKET(p, last_received_by_other);
    }

    uint16_t start, end;

    if (last_received_by_other >= 32)
        start = last_received_by_other - 32;
    else
        start = 65535 - (32 - last_received_by_other);

    end = last_received_by_other != 65535 ? last_received_by_other + 1 : 0;

    int bit = 0;
    bool32 was_acked;

    for (uint16_t i = start;
         i != end;
         i = (i != 65535 ? i + 1 : 0))
    {
        was_acked = GET_BITFLAG(ack_bits, uint32_t, (1 << bit));
        ++bit;

        if (!was_acked) continue;

        p = SequenceBuffer_Packet_getBySequence(
            &c->packet_sb_out, i);

        /* On packet acked... */
        if (p)
        {
            ACK_AND_FREE_PACKET(p, i);
        }
    }

    /* Handle packet here in one way or another, maybe via callback... */
    if (data_size > MIN_PACKET_SIZE && c->on_receive_packet_callback)
        c->on_receive_packet_callback(buffer, data_size, c->packet_receive_args);

    c->time_last_received_valid_data = getTimePassed();

    return 0;

#   undef ACK_AND_FREE_PACKET
}

static int
_Connection_receivePackets(Connection *c, Socket *socket,
    char *buffer, int buffer_size)
{
    Address sender_address;

    int total = 0;
    int received;

    do
    {
        received = Socket_receiveFrom(socket, buffer, (size_t)buffer_size,
            &sender_address);

        if (received >= (int)MIN_PACKET_SIZE)
        {
            if (sender_address.address == c->address.address)
                _Connection_handleValidReceivedData(c, buffer, received);

            total += received;
        }

    }
    while (received != -1);

    return received;
}

static void
_Connection_cleanOldPackets(Connection *c)
{
    uint16_t sequence;

    if (c->packet_id_out >= 33)
        sequence = c->packet_id_out - 33;
    else
        sequence = 65535 - (33 - c->packet_id_out);

    Packet *p = SequenceBuffer_Packet_getBySequence(&c->packet_sb_out, sequence);

    if (p)
    {
        _Connection_destroyPacketAndMessages(c, p, sequence);
        NW_DEBUG_PRINTF("_Connection_cleanOldPackets(): "
            "cleaned up an old packet.\n");
    }
}

/* Queue up a message to be sent to a client */

static inline void *
_Connection_queueMessage(Connection *c, message_t type, uint size)
{
    if (FIFOQueue_MessagePtr_isFull(&c->message_queue_out))
    {
        NW_PRINTF("Connection_queueMessage(): connection %i.%i.%i.%i's "
            "message queue is full!\n", FORMATTED_IP_ADDR(&c->address));
        return 0;
    }

    Message *msg = (Message*)DynamicMemoryBlock_unsafeMalloc(c->allocator,
        sizeof(Message) + size);

    if (!msg)
    {
        NW_DEBUG_PRINTF("Connection_queueMessage(): failed to allocate "
            "message.\n");
        return 0;
    }

    Message **msg_ptr = SequenceBuffer_MessagePtr_insert(&c->message_sb_out,
        c->message_id_out);

    if (!msg_ptr)
    {
        NW_DEBUG_PRINTF("Connection_queueMessage: failed to insert message to "
            "sequence buffer.\n");
        DynamicMemoryBlock_free(c->allocator, msg);
        return 0;
    }

    *msg_ptr = msg;

    FIFOQueue_MessagePtr_push(&c->message_queue_out, &msg);

    msg->type           = type;
    msg->id             = c->message_id_out;
    msg->size           = (int)size;
    msg->time_last_sent = 0;

    if (!c->have_unacked_messages)
    {
        c->have_unacked_messages = 1;
        c->oldest_unacked_message = msg->id;
    }

    /* For some reason, it just wasn't wrapping around otherwise wtf mang */
    if (c->message_id_out != 65535)
        ++c->message_id_out;
    else
        c->message_id_out = 0;

#if 0
    if (msg->size > 0)
        return msg + 1;
    else
        return 0;
#endif

    return msg + 1;
}
