#pragma once
#include "../compile_flags.h"
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include "../core/memory.h"
#include "../core/utils.h"
#include "spritebatch.h"
#include "camera.h"
#include "../core/gui.h"

#if _CF_ALLOW_IMMEDIATE_RENDER
    #include "immediate_render.h"
#endif

#define NUM_MAX_SHADER3DS               16
#define MAX_ENTITY_DIRECTIONAL_LIGHTS   2
#define MAX_ENTITY_POINT_LIGHTS         4
#define MAX_MATERIAL_DIFFUSE_TEXTURES   3
#define SKYBOX_SCALE_MULTIPLIER         30.0f

enum Shader3DUBOIndex
{
    SHADER3D_SHARED_MATRICES_INDEX = 0,
    SHADER3D_LIGHTS_INDEX,
    SHADER3D_POINT_LIGHTS_INDEX,
    SHADER3D_SCENE_AMBIENT_LIGHTING_INDEX
};

enum Shader3DType
{
    SHADER3D_TYPE_OPAQUE,
    SHADER3D_TYPE_TRANSPARENT,
    SHADER3D_TYPE_BLENDED,
    SHADER3D_TYPE_BILLBOARD,
    SHADER3D_TYPE_OPAQUE_OUTLINE
};

/* Default shaders are initialized in this order */
enum Shader3DIndex
{
    SHADER3D_OPAQUE_OPAQUE = 0,
    SHADER3D_OPAQUE_SKYBOX,
    SHADER3D_TRANSPARENT_TRANSPARENT,
    SHADER3D_OPAQUE_GOURAUD,
    SHADER3D_BILLBOARD
};

/* Forward declaration(s) */
typedef struct Window           Window;
typedef struct SpriteGroup      SpriteGroup;
typedef struct Scene3D          Scene3D;
typedef struct RenderModel      RenderModel;
typedef struct BoxCollider      BoxCollider;
typedef struct SphereCollider   SphereCollider;

/* Types defined here */
typedef struct Vertex                   Vertex;
typedef struct Shader3D                 Shader3D;
typedef struct Texture                  Texture;
typedef struct Material                 Material;
typedef struct MaterialArray            MaterialArray;
typedef struct BaseMesh                 BaseMesh;
typedef struct Model                    Model;
typedef struct ModelAnimationFrame      ModelAnimationFrame;
typedef struct ModelAnimation		    ModelAnimation;
typedef struct Renderer                 Renderer;
typedef GLint                           Viewport[4]; /* x, y, w, h */

enum RendererError
{
    RENDERER_ERROR_OUT_OF_MEMORY = 1,
    RENDERER_ERROR_GLEW_INIT,
    RENDERER_ERROR_ATTRIBUTE_NOT_FOUND,
    RENDERER_ERROR_IMMEDIATE_MODE_INIT
};

/* Not actually used anywhere, just describes the layout */
struct Vertex
{
    vec3 pos;
    vec2 uv;
    vec3 normal;
};

struct Texture
{
    GLuint  id;
    GLenum  type;
    GLenum  wrap_s, wrap_t;
    uint    width;
    uint    height;
};

struct Shader3D
{
    GLuint              id;
    enum Shader3DType   type;
    GLint               loc_model;
    GLint               loc_material_shine;
    GLint               loc_material_color; /* If there is no texture, this is used */

    bool32              support_directional_lights;
    bool32              support_point_lights;
};

#include "spritefont.h"

ARRAY_LIST_DECLARATION(Texture);

struct Material
{
    GLuint          diffuse;
    GLuint          specular;
    GLfloat         shine;
    vec3            color;
};

struct MaterialArray
{
    Material    **materials;
    uint        num_materials;
};

struct BaseMesh
{
    GLfloat     *vertices;
    GLuint      *indices;
    uint        num_vertices;
    uint        num_indices;
    GLuint      vbo;
    GLuint      vao;
    GLuint      ebo;
    bool32      initialised_statically;
    BaseMesh    *next;
};

struct Model
{
    BaseMesh    *meshes;
    uint        num_meshes;
};

struct ModelAnimationFrame
{
	Model	        *model;
    Material        *material;
    MaterialArray   *material_array;
	float	        relative_duration;
};

struct ModelAnimation
{
	float               duration;
    int                 num_frames;
	ModelAnimationFrame *frames;
};

struct Renderer
{
    SDL_GLContext                   context;
    SpriteBatch                     sprite_batch;
#if _CF_ALLOW_IMMEDIATE_RENDER
    ImmediateRenderState            immediate_state;
#endif
    Window                          *window;
    MemoryPool                      memory;
    DynamicMemoryBlock              dynamic_memory;
    bool32                          cleared_frame;

    GLuint                          ubo_shader3d_shared_matrices;
    GLuint                          ubo_shader3d_directional_lights;
    GLuint                          ubo_shader3d_point_lights;
    GLuint                          ubo_shader3d_scene_ambient_lighting;
    GLint                           outline_offset_index;

    /* Byte offsets of various uniform buffer members */
    GLuint                          ubo_os_projection;
    GLuint                          ubo_os_look_at;
    GLuint                          ubo_os_position;
    GLuint                          ubo_os_dir_ambient;
    GLuint                          ubo_os_dir_direction;
    GLuint                          ubo_os_dir_diffuse;
    GLuint                          ubo_os_dir_specular;
    GLuint                          ubo_os_dir_num;
    GLuint                          ubo_os_pnt_position;
    GLuint                          ubo_os_pnt_diffuse;
    GLuint                          ubo_os_pnt_specular;
    GLuint                          ubo_os_pnt_ambient;
    GLuint                          ubo_os_pnt_clq;
    GLuint                          ubo_os_pnt_num;
    GLuint                          ubo_os_outline;

    Viewport                        default_viewport;
    bool32                          use_default_viewport;
    GLfloat                         background_color[4];

    Shader3D                        *shader3ds;
    uint                            num_shader3ds;
    uint                            num_max_shader3ds;

    /* Meshes to be buffered up to the GPU */
    BaseMesh                        *base_mesh_buffer_queue;
    Mutex                           base_mesh_buffer_queue_mutex;

    Shader3D                         default_outline_shader;
};

/* Accessors to the global renderer's dynamic memory pool */
void *
renderMalloc(size_t size);

void
renderFree(void *target);

int
Shader3D_init(Shader3D *s,
    Renderer *renderer,
    enum Shader3DType type,
    const char *vertex_code,
    const char *fragment_code);

void
Texture_init(Texture *texture,
    GLenum texture_type, /* GL_TEXTURE_2D, GL_TEXTURE_CUBE_MAP, etc. */
    void *pixels,
    GLenum pixel_array_type, /* For example, GL_UNSIGNED_BYTE */
    GLenum pixel_format, /* for example, GL_RGBA */
    GLenum wrap_s, GLenum wrap_t,
    int w, int h);

void
Texture_initAsCubemap(Texture *texture,
    void *pixels_pos_x,
    void *pixels_neg_x,
    void *pixels_pos_y,
    void *pixels_neg_y,
    void *pixels_pos_z,
    void *pixels_neg_z,
    GLenum pixel_array_type,
    GLenum pixel_format,
    GLenum wrap_s, GLenum wrap_t,
    int individual_part_width,
    int individual_part_height);

/* Currently I guess this is pretty much the same as the one below... */
void
BaseMesh_initFromExistingAlloc(BaseMesh *mesh,
    GLfloat *vertices, uint num_vertices, /* Pre-allocated vertices */
    GLuint   *indices,  uint num_indices); /* Pre-allocated indices */

/* Init with a ready-made pointer to vertex data - this will simply set the correct
 * parameters to point to the right places for the mesh */
void
BaseMesh_initStatically(BaseMesh *mesh,
    GLfloat *vertices, uint num_vertices,
    GLuint   *indices,  uint num_indices);

static inline void
Model_init(Model *model, BaseMesh *meshes, uint num_meshes);

static inline uint
Model_getNumVertices(Model *model);

static inline uint
Model_getNumIndices(Model *model);

void
Model_computeAndGetMinMaxDimensions(Model *model, vec3 ret_min, vec3 ret_max);

void
BaseMesh_generateBuffer(BaseMesh *mesh);

static inline void
BaseMesh_destroyBuffer(BaseMesh *mesh);

void
generateTriangleMeshNormals(GLfloat *dest, GLfloat *src, uint num_vertices);

int
Material_init(Material *material, Texture *diffuse, Texture *specular,
    float shine, float r, float g, float b);

int
MaterialArray_init(MaterialArray *array, Material **materials,
    uint num_materials);

static inline Material
createMaterialFromTexture(Texture *texture);

/* Returns 0 on success */
int
Renderer_init(Renderer *renderer, Window *window,
    char *error_log, int log_len, bool32 vsync);

void
Renderer_renderScene3D(Renderer *renderer, Scene3D *scene, Camera *camera,
    Viewport viewport);

void
Renderer_setBackgroundColor(Renderer *renderer,
    float r, float g, float b, float a);

void
Renderer_handleQueuedWork(Renderer *renderer);

void
Renderer_swapBuffers(Renderer *renderer);

void
Renderer_renderSpriteGroup(Renderer *renderer, SpriteGroup *group);

void
Renderer_renderText(Renderer *renderer, SpriteFont *sprite_font, const char *text,
    float x, float y, float *color);

void
Renderer_renderGUI(Renderer *renderer, GUI *context);

void
Renderer_clearFrameIfNecessary(Renderer *renderer);

void
Renderer_onWindowResize(Renderer *renderer);

/* Pass in 0 to use full window as the viewport */
void
Renderer_setDefaultViewport(Renderer *renderer, Viewport viewport);

#if _CF_SCENE_DEBUG_RENDERING
void
Renderer_renderBoxCollider(Renderer *renderer, BoxCollider *box);

void
Renderer_renderStaticBoxCollider(Renderer *renderer, float pos_x, float pos_y, float pos_z, float dim_x, float dim_y, float dim_z, float rot_x, float rot_y, float rot_z);

void
Renderer_renderSphereCollider(Renderer *renderer, SphereCollider *sphere);
#else

static inline void
Renderer_renderBoxCollider(Renderer *renderer, BoxCollider *box){}

static inline void
Renderer_renderSphereCollider(Renderer *renderer, SphereCollider *sphere){}

#endif /* _CF_SCENE_DEBUG_RENDERING */

/* Returns shader index, -1 if none */
int
Renderer_createShader3D(Renderer *renderer,
    enum Shader3DType type,
    const char *vertex_code,
    const char *fragment_code);

void
Renderer_queueuBufferableBaseMesh(Renderer *renderer, BaseMesh *mesh);

static inline void
setUpVertexAttribPointers()
{
    /* Position */
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
        8 * sizeof(GLfloat), (const GLvoid*)0);
    glEnableVertexAttribArray(0);

    /* UV */
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
        8 * sizeof(GLfloat), (const GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    /* Normal */
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
        8 * sizeof(GLfloat), (const GLvoid*)(5 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
}

static inline void
BaseMesh_destroyBuffer(BaseMesh *mesh)
{
    glDeleteBuffers(1, &mesh->vbo);
    glDeleteBuffers(1, &mesh->ebo);
    glDeleteVertexArrays(1, &mesh->vao);
}

static inline Material
createMaterialFromTexture(Texture *texture)
{
    Material mat;
    Material_init(&mat, texture, texture, 0.0f, 0.0f, 0.0f, 0.0f);
    return mat;
}

static inline void
Model_init(Model *model, BaseMesh *meshes, uint num_meshes)
{
    model->meshes       = meshes;
    model->num_meshes   = num_meshes;
}

static inline uint
Model_getNumVertices(Model *model)
{
    uint num_verts = 0;
    for (BaseMesh *m = model->meshes;
         m < &model->meshes[model->num_meshes];
         ++m)
    {
        num_verts += m->num_vertices;
    }
    return num_verts;
}

static inline uint
Model_getNumIndices(Model *model)
{
    uint num_inds = 0;
    for (BaseMesh *m = model->meshes;
         m < &model->meshes[model->num_meshes];
         ++m)
    {
        num_inds += m->num_indices;
    }
    return num_inds;
}
