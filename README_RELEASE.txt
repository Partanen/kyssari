K�ristys
Copyright 2017, the Kyss�ri team
Contact: joelpartanen[at]tutanota.com

K�ristys is free software licensed under the GNU General Public License version
3. The source code of this program is available at
https://gitlab.com/Partanen/kyssari/. Binaries are available at
kuvis.itch.io/karistys.

The Kyss�ri team:
Kuvis       - engine programming
Lommi       - gameplay and engine programming
J�m�        - gameplay and engine programming
Pekka       - weapon, power-up, 2D art and other art
Lasse P.    - character and UI art
Lasse H.    - map design and exterior area models/props
Keyo        - sounds and music
