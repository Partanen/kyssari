#pragma once
#include "../../../engine/src/core/engine.h"
#include "../../../engine/src/core/scene3d.h"
#include "../../../engine/src/render/2d.h"
#include <math.h>
#include <time.h>

#include "../../shared/shared.h"
#include "menuscreen.h"
#include "spgamescreen.h"
#include "mpgamescreen.h"
#include "loadingscreen.h"

typedef struct BulletType           BulletType;
typedef struct WeaponType           WeaponType;
typedef struct PlayerScriptData     PlayerScriptData;
typedef struct StoredClientInput    StoredClientInput;
typedef struct InputBuffer          InputBuffer;
typedef struct StateUpdate          StateUpdate;
typedef struct StateUpdateBuffer    StateUpdateBuffer;
typedef struct SimQueue             SimQueue;
typedef struct ItemSpawn            ItemSpawn;
typedef struct Game                 Game;
typedef struct BulletScriptData     BulletScriptData;

#define MENU_SCENE_MEMORY_SIZE      4
#define LEVEL_SCENE_MEMORY_SIZE     MEGABYTES(190)
#define MAX_STATE_UPDATES           64
#define MAX_SENT_INPUT_ITEMS        256
#define MAX_MESSAGE_BUFFER_ITEMS    64
#define MAX_SIM_TASKS               128
#define DEFAULT_FOV                 DEG_TO_RAD(90.0f)//80.198563f
#define DEFAULT_VIEW_DISTANCE       300.0f
#define HITSCREEN_DURATION          2.0f

#define GET_INGAME_MATERIAL(_name) \
    AssetPool_getMaterial(&game.ingame_asset_pool, _name)

#define GET_INGAME_MATERIAL_ARRAY(_name) \
    AssetPool_getMaterialArray(&game.ingame_asset_pool, _name)

#define GET_INGAME_TEXTURE(_name) \
    AssetPool_getTexture(&game.ingame_asset_pool, _name)

#define GET_INGAME_MODEL(_name) \
    AssetPool_getModel(&game.ingame_asset_pool, _name)

#define LOAD_INGAME_TEXTURE(_name, _path) \
    AssetPool_loadTexture(&game.ingame_asset_pool, _name, _path)

#define LOAD_INGAME_MATERIAL(_name, _diff, _spec, _r, _g, _b, _shine) \
    AssetPool_loadMaterial(&game.ingame_asset_pool, _name, _diff, _spec, \
        _r, _g, _b, _shine)

#define LOAD_INGAME_MODEL(_name, _path) \
    AssetPool_loadModel(&game.ingame_asset_pool, _name, _path)

#define IS_CLIENT_ACTIVE() (client.status != CLIENT_STATUS_DISCONNECTED)

enum KsScreen
{
    MENU_SCREEN = 0,
    SP_GAME_SCREEN,
    MP_GAME_SCREEN,
    LOADING_SCREEN,

    NUM_SCREENS
};

enum MultiplayerState
{
    MP_STATE_WAITING_FOR_GAME = 0,
    MP_STATE_LOADING,
    MP_STATE_READY
};

enum PlayerRenderModelId
{
    PLAYER_RENDER_MODEL_ID_WEAPON = 0,
    PLAYER_RENDER_MODEL_ID_MUZZLEFIRE,
    PLAYER_RENDER_MODEL_ID_BODY
};

enum PlayerAnimatorId
{
    PLAYER_ANIMATOR_ID_WEAPON = 0,
    PLAYER_ANIMATOR_ID_MUZZLEFIRE
};

enum SoundChannel
{
    SOUND_CHANNEL_FOOTSTEPS,
    SOUND_CHANNEL_LANDING,
    SOUND_CHANNEL_PLAYER_WEAPON,
    SOUND_CHANNEL_SCREAM,
    SOUND_CHANNEL_ENEMY_EXPLOSION,
    SOUND_CHANNEL_PLAYER_EXPLOSION
};

/* This structure is useless */
struct BulletType
{
    BaseBulletType  base;

    float           scale;
    Model           *model;
    Material        *material;
    vec3            relative_rotation;

    void (*collisionCallBack)(
        PhysicsComponent *component_a,
        PhysicsComponent *component_b,
        Entity *entity_a, Entity *entity_b,
        enum ColliderType type_a, enum ColliderType type_b,
        enum CollisionEventType event_type,
        uint32_t collider_a_id, uint32_t collider_b_id);
};

struct BulletScriptData
{
    float   timer; // make this zero
    float   dist_to_hit;
    float   bullet_velocity;

    vec3    explosion_pos;
    vec3    last_position;
    vec3    player_pos;
};

struct WeaponType
{
    BaseWeaponType  base;

    Model           *model;
    Material        *material;
    BulletType      *bullet_type;
    char            material_name[128];
    char            model_name[128];
    float           rot_x, rot_y, rot_z,
                    pos_x, pos_y, pos_z;
    float           muzzlefire_duration;
    float           animation_duration;

    vec3            muzzlefire_relative_pos;
    vec3            muzzlefire_relative_scale;

    /* Singleplayer callback that will be called when the weapon is fired.
     * This will for example spawn the bullet entity and do other magic,
     * such as raycasting */
    void (*sp_on_shoot_callback)(enum KsWeaponType type, Entity *pawn);
};

struct PlayerScriptData
{
    BasePlayerScriptData        base;
    float                       muzzlefire_duration;
    float                       muzzlefire_timer;
    float                       animation_timer;

    /* On the client side, these are needed - the server stores them differently
     * per player, although its only relevant for animations for now. */
    enum KsPawnMovementState    movement_state;
    enum KsWeaponType           weapon;
    char                        character_type;
};

struct InputBuffer
{
    ClientInput         *new_items[MAX_INPUT_BUFFER_ITEMS];
    /* We store up to MAX_SENT_INPUT_ITEMS here for use during reconciliation */

    struct StoredClientInput
    {
        bool32      used;
        ClientInput input;
        vec3        position;
        vec3        velocity;
    }                   *sent_items;

    ubyte               num_items;
    int                 size_in_bytes;
    Mutex               mutex;
    kb_state_t          last_kb_state;
    float               last_orientation;
    input_sequence_t    running_sequence;
};

extern struct SimQueue
{
    Mutex   mutex;
    uint    num_tasks;

    struct
    {
        enum
        {
            SIM_REMOVE_ENTITY,
            SIM_SPAWN_ITEM,
            SIM_COLLIDE_ITEM,
            SIM_SWAP_OTHER_PAWN_WEAPON,
            SIM_EXPLODE_ROCKET,
            SIM_SNAP_PAWN_STATE
        } type;

        union
        {
            struct
            {
                Entity      *entity;
                uint32_t    id;
            } remove_entity;

            struct
            {
                int16_t type;
                int16_t index;
                vec3    pos;
            } spawn_item;

            struct
            {
                int16_t             item_index;
                int16_t             item_type;
                replication_id32_t  rid;
            } collide_item;

            struct
            {
                enum KsWeaponType   w;
                replication_id32_t  rid;
            } swap_other_pawn_weapon;

            struct
            {
                vec3    pos;
            } explode_rocket;

            struct
            {
                vec3 pos;
                vec3 vel;
            } snap_pawn_state;
        } data;
    } tasks[MAX_SIM_TASKS];
} sim_queue;

struct ItemSpawn
{
    enum KsItemType type;
    Entity          *entity;
};

struct Game
{
    enum MultiplayerState   multiplayer_state;
    enum KsMapId            loaded_map_id;
    enum KsScreen           screen_after_load;
    Screen                  *screens[NUM_SCREENS];
    MenuScreenData          menu_screen_data;
    SPGameScreenData        game_screen_data;
    MPGameScreenData        mp_game_screen_data;
    LoadingScreenData       loading_screen_data;
    AssetPool               ingame_asset_pool;
    Scene3D                 game_scene;
    void                    *scene_memory;
    size_t                  scene_memory_size;
    Thread                  load_thread;
    bool32                  loaded_startup_data;

    Mutex                   latest_verified_input_mutex;

    /* Keep this structure in such a shape it can safely be memset to 0
     * for reset purposes */
    struct MultiplayerData
    {
        Entity              *pawn;
        bool32              have_new_verified_input;
        input_sequence_t    latest_verified_input_sequence;
        vec3                latest_verified_position;
        vec3                latest_verified_velocity;
        int                 health;
        double              input_accumulator;
        ItemSpawn           item_spawns[MAX_ITEMS_PER_LEVEL];
        uint                num_frags;
    } mp_data;
};

extern struct IngameAssets
{
    ModelAnimation  stand_animation;
    ModelAnimation  jump_up_animation;
    ModelAnimation  jump_down_animation;
    ModelAnimation  dash_animation;
    ModelAnimation  dashback_animation;
    ModelAnimation  run_animation;
    ModelAnimation  strafe_left_animation;
    ModelAnimation  strafe_right_animation;

    ModelAnimation  stand_animation_anarchist;
    ModelAnimation  jump_up_animation_anarchist;
    ModelAnimation  jump_down_animation_anarchist;
    ModelAnimation  dash_animation_anarchist;
    ModelAnimation  dashback_animation_anarchist;
    ModelAnimation  run_animation_anarchist;
    ModelAnimation  strafe_left_animation_anarchist;
    ModelAnimation  strafe_right_animation_anarchist;

    ModelAnimation  stand_animation_native;
    ModelAnimation  jump_up_animation_native;
    ModelAnimation  jump_down_animation_native;
    ModelAnimation  dash_animation_native;
    ModelAnimation  dashback_animation_native;
    ModelAnimation  run_animation_native;
    ModelAnimation  strafe_left_animation_native;
    ModelAnimation  strafe_right_animation_native;

    ModelAnimation  stand_animation_officer;
    ModelAnimation  jump_up_animation_officer;
    ModelAnimation  jump_down_animation_officer;
    ModelAnimation  dash_animation_officer;
    ModelAnimation  dashback_animation_officer;
    ModelAnimation  run_animation_officer;
    ModelAnimation  strafe_left_animation_officer;
    ModelAnimation  strafe_right_animation_officer;

    /* Muzzles and explosions */
    ModelAnimation  rocket_muzzle_animation;
    ModelAnimation  smg_muzzle_animation;
    ModelAnimation  railgun_muzzle_animation;
    ModelAnimation  explosion_animation;
} ig_assets;

extern struct PermanentAssets
{
    SpriteFont  *sfont_roboto;

    Texture     *tex_skybox_level1;
    Texture     *tex_karistys_title;
    Texture     *tex_button_sheet1;
    Texture     *tex_button_sheet2;
    Texture     *crosshair_texture;
    Texture     *hitscreen_texture;
    Texture     *rocket_selected_texture;
    Texture     *smg_selected_texture;
    Texture     *railgun_selected_texture;
    Texture     *machete_selected_texture;

    SoundEffect *se_railgun;
    SoundEffect *se_steps;
    SoundEffect *se_death_scream1;
    SoundEffect *se_hit_scream1;
    SoundEffect *se_hit_scream2;
    SoundEffect *se_fwump;
    SoundEffect *se_explosion;
    SoundEffect *se_smg;

    int         clip_resume_n[4];
    int         clip_resume_h[4];
    int         clip_ragequit_n[4];
    int         clip_ragequit_h[4];

    int         clip_demo_n[4];
    int         clip_demo_h[4];
    int         clip_multiplayer_n[4];
    int         clip_multiplayer_h[4];
    int         clip_credits_n[4];
    int         clip_credits_h[4];
    int         clip_quit_n[4];
    int         clip_quit_h[4];
} permanent_assets;

extern struct KeyConfig
{
    int move_forward;
    int move_left;
    int move_right;
    int move_back;
    int toggle;
    int jump;
    int dash;
    int shoot;
    int weaponswap_next;
    int weaponswap_prev;
    int show_stat_window;
} key_config;

extern struct GraphicsConfig
{
    float   field_of_view;
    float   draw_distance;
} graphics_config;

extern struct GameData
{
    BulletType  bullet_types[NUM_BASE_WEAPON_TYPES];
    WeaponType  weapon_types[NUM_BASE_WEAPON_TYPES];
} game_data;

extern struct MessageBuffer
{
    struct MessageBufferItem
    {
        message_t       type;
        void            *data;
        int             size;
    } items[MAX_MESSAGE_BUFFER_ITEMS];

    uint                num_items;
    SimpleMemoryBlock   memory;
    Mutex               mutex;
} message_buffer;

#define GET_BULLET_TYPE(_enum) (&game_data.bullet_types[_enum])
#define GET_WEAPON_TYPE(_enum) (&game_data.weapon_types[_enum])

extern Game                 game;
extern Client               client;
extern InputBuffer          input_buffer;
extern StateUpdateBuffer    state_update_buffer;
extern int                  max_input_replays;
extern int                  max_input_rollback;
extern float                position_x_limit;
extern float                position_y_limit;
extern float                position_z_limit;
extern int                  network_tick_rate;

extern const char *uneducational_str;
extern const char *tips_of_the_day[];
extern const int NUM_TIPS_OF_THE_DAY;

extern int CEL_SHADER_OPAQUE;
extern int CEL_SHADER_TRANSPARENT;

extern GUIWindowStyle stat_window_style;

void
SimQueue_init();

static inline int
SimQueue_queueRemoveEntityTask(Entity *e);

static inline int
SimQueue_queueSpawnItemTask(int index, enum KsItemType type, vec3 pos);

static inline int
SimQueue_queueCollideItemTask(int index, enum KsItemType type,
    replication_id32_t rid);

static inline int
SimQueue_queueSwapOtherPawnWeaponTask(enum KsWeaponType type, replication_id32_t rid);

static inline int
SimQueue_queueRocketExplosion(vec3 pos);

void
SimQueue_executeContents();

void
SimQueue_init();

void
initGameData();

void
resetMultiplayerData();

int
initGame(int argc, char **argv);

int
loadPooledIngameAssets();

int
loadPooledIngameAssetsThreadedPortion();

int
loadPermanentAssets();

void
initMenuWindowStyle();

void
initStatWindowStyle();

int
parseServerAddressFromFile(Address *ret_addr, const char *path);

int
connectToServer();

void
sendInputBufferContents();

void
mp_setOtherPawnMovementStateAndAnimation(Entity *e, int new_state);

BulletType
createBulletType(BaseBulletType *base, Model *model, Material *material,
    float scale, vec3 relative_rotation);

WeaponType
createWeaponType(BaseWeaponType *base,
    Model *model, Material *material,
    BulletType *bullet_type, float animation_duration,
    float muzzlefire_duration,
    vec3 relative_position, vec3 relative_rotation,
    void (*sp_on__callback)(enum KsWeaponType type, Entity *pawn));

void
setPlayerPawnWeaponLocal(Entity *pawn, enum KsWeaponType weapon);

static inline void
setPlayerPawnVisualWeaponModel(RenderModel *rm, enum KsWeaponType weapon,
    PlayerScriptData *d);

Entity *
createMapEntity(Scene3D *scene, Model *model, Model *no_collision_model,
    MaterialArray *material_array, MaterialArray *nocol_material_array,
    int shader, float x, float y, float z);

/* TODO: get rid of this along with the bullet structure - its useless! */
Entity *
createBulletEntity(Scene3D *scene3d, enum KsWeaponType weapon,
    vec3 pos, vec3 rot, vec3 dir);

Entity *
createDirectionalLightEntity(Scene3D *scene, float x, float y, float z,
    float dir_x, float dir_y, float dir_z);

void
createEffect(Entity *entity, Model* model,
    Material *material, vec3 pos, vec3 velocity, vec3 rotation, float scale,
    float lifetime, bool32 gravity_active, bool32 is_billboard);

static inline void
InputBuffer_init(InputBuffer *buffer);

static inline void
InputBuffer_clear(InputBuffer *ib);

static inline int
InputBuffer_queueMovementInput(InputBuffer *ib, kb_state_t kbs,
    float orientation);

/* If the server falls too far behind on received input sequences,
 * this function can be called to reset the player's inputs back to the latest
 * acked one. Pass in the corrected pos and vel received from the server. */
static inline void
InputBuffer_revertToSequence(InputBuffer *ib, input_sequence_t seq,
    vec3 new_pos, vec3 new_vel);

int
MessageBuffer_init();

static inline void
MessageBuffer_clear();

void *
MessageBuffer_queueMessageUnsafe(message_t type, int size);

void
MessageBuffer_sendAllContents();

void
client_initPlayerPawn(Entity *e);

void
drawGameMenu(bool32 *is_menu_visible);

static inline Entity *
mp_getPlayerPawn();

static inline void
mp_setOtherPawnWeaponRenderModelTransform(RenderModel *rm, enum KsWeaponType type);

static inline void
setPlayerPawnVisualWeaponModel(RenderModel *rm, enum KsWeaponType weapon,
    PlayerScriptData *d)
{
    WeaponType *w   = GET_WEAPON_TYPE(d->weapon);
    rm->model       = w->model;
    rm->material    = w->material;

    rm->relative_transform.rot[0] = w->rot_x;
    rm->relative_transform.rot[1] = w->rot_y;
    rm->relative_transform.rot[2] = w->rot_z;

    rm->relative_transform.pos[0] = w->pos_x;
    rm->relative_transform.pos[1] = w->pos_y;
    rm->relative_transform.pos[2] = w->pos_z;

    if (d->base.f_toggled && d->weapon == SMG)
    {
        rm->relative_transform.rot[0] = 1.5f;
        rm->relative_transform.rot[1] = 4.7f;
        rm->relative_transform.pos[0] = -4.5f;
        rm->relative_transform.pos[1] = -2.3f;
        rm->relative_transform.pos[2] = -6.1f;
    }
}

static inline void
InputBuffer_init(InputBuffer *ib)
{
    Mutex_init(&ib->mutex);
    ib->sent_items = allocatePermanentMemory(
        MAX_SENT_INPUT_ITEMS * sizeof(struct StoredClientInput));
    for (int i = 0; i < MAX_SENT_INPUT_ITEMS; ++i)
        ib->sent_items[i].used = 0;
    ASSERT(ib->sent_items);
}

static inline void
InputBuffer_clear(InputBuffer *ib)
{
    ib->num_items               = 0;
    ib->size_in_bytes           = 0;
    ib->running_sequence        = 0;
    ib->last_kb_state           = 0;
    ib->last_orientation        = 0.0f;
    memset(ib->sent_items, 255, MAX_SENT_INPUT_ITEMS * sizeof(ClientInput));
    for (int i = 0; i < MAX_SENT_INPUT_ITEMS; ++i)
        ib->sent_items[i].used = 0;
}

static inline int
InputBuffer_queueMovementInput(InputBuffer *ib, kb_state_t kbs, float orientation)
{
    if (ib->num_items >= MAX_INPUT_BUFFER_ITEMS)
        return 1;

    Mutex_lock(&ib->mutex);

    /* Create the new input */
    int index = (int)ib->running_sequence % MAX_SENT_INPUT_ITEMS;
    StoredClientInput *inp                  = &ib->sent_items[index];
    inp->input.type                         = INPUT_MOVEMENT;
    inp->input.data.movement.kb_state       = kbs;
    inp->input.data.movement.orientation    = orientation;
    inp->input.sequence                     = ib->running_sequence;
    inp->used                               = 1;

    ib->new_items[ib->num_items] = &inp->input;

    ib->size_in_bytes += (int)(sizeof(kb_state_t) + sizeof(float));
    ib->last_kb_state           = kbs;
    ib->last_orientation        = orientation;

    ++ib->num_items;

    if (ib->running_sequence != 65535)
        ++ib->running_sequence;
    else
        ib->running_sequence = 0;

    Mutex_unlock(&ib->mutex);

    return 0;
}

static inline void
InputBuffer_revertToSequence(InputBuffer *ib, input_sequence_t seq,
    vec3 new_pos, vec3 new_vel)
{
    int index = (int)seq % MAX_SENT_INPUT_ITEMS;

    Mutex_lock(&ib->mutex);

    StoredClientInput *inp = &ib->sent_items[index];
    vec3_copy(inp->position, new_pos);
    vec3_copy(inp->velocity, new_vel);

    ib->last_kb_state       = inp->input.data.movement.kb_state;
    ib->last_orientation    = inp->input.data.movement.orientation;
    ib->size_in_bytes       = 0;
    ib->num_items           = 0;

    ib->running_sequence    = seq != 65535 ? seq + 1 : 0;

    Mutex_unlock(&ib->mutex);
}

static inline void
MessageBuffer_clear()
{
    message_buffer.num_items = 0;
    SimpleMemoryBlock_clear(&message_buffer.memory);
}

static inline int
SimQueue_queueRemoveEntityTask(Entity *e)
{
    if (sim_queue.num_tasks >= MAX_SIM_TASKS) return 1;
    Mutex_lock(&sim_queue.mutex);
    sim_queue.tasks[sim_queue.num_tasks].type = SIM_REMOVE_ENTITY;
    sim_queue.tasks[sim_queue.num_tasks].data.remove_entity.entity = e;
    sim_queue.tasks[sim_queue.num_tasks].data.remove_entity.id = e->id;
    ++sim_queue.num_tasks;
    Mutex_unlock(&sim_queue.mutex);
    return 0;
}

static inline int
SimQueue_queueSpawnItemTask(int index, enum KsItemType type, vec3 pos)
{
    if (sim_queue.num_tasks >= MAX_SIM_TASKS) return 1;

    Mutex_lock(&sim_queue.mutex);
    sim_queue.tasks[sim_queue.num_tasks].type = SIM_SPAWN_ITEM;
    sim_queue.tasks[sim_queue.num_tasks].data.spawn_item.type = (int16_t)type;
    sim_queue.tasks[sim_queue.num_tasks].data.spawn_item.index = (int16_t)index;
    vec3_copy(sim_queue.tasks[sim_queue.num_tasks].data.spawn_item.pos, pos);
    ++sim_queue.num_tasks;
    Mutex_unlock(&sim_queue.mutex);

    return 0;
}

static inline int
SimQueue_queueCollideItemTask(int index, enum KsItemType type,
    replication_id32_t rid)
{
    if (sim_queue.num_tasks >= MAX_SIM_TASKS) return 1;

    Mutex_lock(&sim_queue.mutex);
    sim_queue.tasks[sim_queue.num_tasks].type = SIM_COLLIDE_ITEM;
    sim_queue.tasks[sim_queue.num_tasks].data.collide_item.item_index =
        (int16_t)index;
    sim_queue.tasks[sim_queue.num_tasks].data.collide_item.item_type =
        (int16_t)type;
    sim_queue.tasks[sim_queue.num_tasks].data.collide_item.rid = rid;
    ++sim_queue.num_tasks;
    Mutex_unlock(&sim_queue.mutex);

    return 0;
}

static inline int
SimQueue_queueSwapOtherPawnWeaponTask(enum KsWeaponType type, replication_id32_t rid)
{
    if (sim_queue.num_tasks >= MAX_SIM_TASKS) return 1;

    Mutex_lock(&sim_queue.mutex);
    sim_queue.tasks[sim_queue.num_tasks].type = SIM_SWAP_OTHER_PAWN_WEAPON;
    sim_queue.tasks[sim_queue.num_tasks].data.swap_other_pawn_weapon.w = type;
    sim_queue.tasks[sim_queue.num_tasks].data.swap_other_pawn_weapon.rid = rid;

        ++sim_queue.num_tasks;
    Mutex_unlock(&sim_queue.mutex);

    return 0;
}

static inline int
SimQueue_queueRocketExplosion(vec3 pos)
{
    if (sim_queue.num_tasks >= MAX_SIM_TASKS) return 1;

    Mutex_lock(&sim_queue.mutex);

    sim_queue.tasks[sim_queue.num_tasks].type = SIM_EXPLODE_ROCKET;
    vec3_copy(sim_queue.tasks[sim_queue.num_tasks].data.explode_rocket.pos, pos);

    ++sim_queue.num_tasks;
    Mutex_unlock(&sim_queue.mutex);

    return 0;
}

static inline int
SimQueue_queueSnapPawnStateTask(vec3 pos, vec3 vel)
{
    if (sim_queue.num_tasks >= MAX_SIM_TASKS) return 1;

    Mutex_lock(&sim_queue.mutex);

    sim_queue.tasks[sim_queue.num_tasks].type = SIM_SNAP_PAWN_STATE;
    vec3_copy(sim_queue.tasks[sim_queue.num_tasks].data.snap_pawn_state.pos, pos);
    vec3_copy(sim_queue.tasks[sim_queue.num_tasks].data.snap_pawn_state.vel, vel);

    ++sim_queue.num_tasks;
    Mutex_unlock(&sim_queue.mutex);

    return 0;
}

static inline Entity *
mp_getPlayerPawn()
{
    return game.mp_data.pawn;
}

static inline void
mp_setOtherPawnWeaponRenderModelTransform(RenderModel *rm, enum KsWeaponType type)
{
    switch (type)
    {
    case ROCKET_LAUNCHER:
        rm->relative_transform.pos[0] = -4.0f;
        rm->relative_transform.pos[1] = -2.5f;
        rm->relative_transform.pos[2] = -5.5f;
        rm->relative_transform.rot[1] = (float)DEG_TO_RAD(280.0f); /* 270? */
        break;
    case SMG:
        rm->relative_transform.pos[0] = -3.0f;
        rm->relative_transform.pos[1] = -3.0f;
        rm->relative_transform.pos[2] = -4.5f;
        rm->relative_transform.rot[1] = (float)DEG_TO_RAD(280.0f); /* 270? */
        break;
    case RAILGUN:
        rm->relative_transform.pos[0] = -4.0f;
        rm->relative_transform.pos[1] = -3.0f;
        rm->relative_transform.pos[2] = -8.5f;
        rm->relative_transform.rot[1] = (float)DEG_TO_RAD(280.0f); /* 270? */
        break;
    case MACHETE:
        rm->relative_transform.pos[0] = -4.0f;
        rm->relative_transform.pos[1] = -2.5f;
        rm->relative_transform.pos[2] = -4.5f;
        rm->relative_transform.rot[1] = (float)DEG_TO_RAD(280.0f); /* 270? */
        break;
    default:
        break;
    }
}
