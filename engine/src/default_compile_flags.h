#pragma once

/* This file can be copied for use as a template. This directory must always
 * have a compile_flags.h file in it at compile time. */

/* If 0, an error in renderer intialization will cause the engine
 * initialization to fail, which may not be desirable when, for
 * example, debugging shaders. */
#define _CF_ALLOW_RENDERER_ERRORS 1

#define _CF_USE_DEFAULT_PHYSICS_COLLISION 1

#define _CF_ENTITY_STRING_NAMES 1

#define _CF_ALLOW_IMMEDIATE_RENDER 1

/* Allow drawing of debug shapes in 3D scenes.
 * This requires _CF_ALLOW_IMMEDIATE_RENDER! */
#define _CF_SCENE_DEBUG_RENDERING 1

#define _CF_ALLOW_TEXT_MODE 1

#define _CF_NETWORKING 1

#define _CF_FIXED_PHYSICS_TIMESTEP 1
