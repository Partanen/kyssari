#include "immediate_render.h"

#define NUM_INITIAL_IMMEDIATE_FLOATS 256

static const char *
im_vertex_code =
"   #version 330 core \n"
"   layout (location = 0) in vec3 pos; \n"
"   uniform mat4 projection; \n"
"   uniform mat4 model; \n"
"   void main(void) \n"
"   { \n"
"       gl_Position = projection * vec4(pos, 1.0f); \n "
"   } \n";

static const char *
im_fragment_code =
"   #version 330 core \n"
"   uniform vec4 c; \n"
"   out vec4 frag_color; \n"
"   void main(void) \n"
"   { \n"
"       frag_color = c; \n"
"   } \n";

int
ImmediateRenderState_init(ImmediateRenderState *s, Renderer *renderer,
    char *error_log, int log_len)
{
    s->renderer     = renderer;
    s->began        = 0;
    s->line_width   = 1.0f;

    s->vertices = DynamicMemoryBlock_malloc(&renderer->dynamic_memory,
        NUM_INITIAL_IMMEDIATE_FLOATS * sizeof(GLfloat));

    if (!s->vertices)
    {
        DEBUG_PRINTF("ImmediateRenderState_init(): failed to allocate vertices.\n");
        return 1;
    }

    s->max_floats = NUM_INITIAL_IMMEDIATE_FLOATS;

    glGenBuffers(1, &s->vbo);
    glGenVertexArrays(1, &s->vao);

    glBindVertexArray(s->vao);
    glBindBuffer(GL_ARRAY_BUFFER, s->vbo);
    glBufferData(GL_ARRAY_BUFFER, NUM_INITIAL_IMMEDIATE_FLOATS * sizeof(GLfloat),
        0, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
        3 * sizeof(GLfloat), (const GLvoid*)0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    GLuint vert = glCreateShader(GL_VERTEX_SHADER);
    GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(vert, 1, &im_vertex_code, 0);
    glShaderSource(frag, 1, &im_fragment_code, 0);
    glCompileShader(vert);
    glCompileShader(frag);

    GLint success;

    glGetShaderiv(vert, GL_COMPILE_STATUS, &success);
    if (success == GL_FALSE)
    {
        glGetShaderInfoLog(frag, log_len, 0, error_log);
        DEBUG_PRINTF("ImmediateRenderState_init(): failed to compile vertex shader.\n");
        return 2;
    }

    glGetShaderiv(frag, GL_COMPILE_STATUS, &success);
    if (success == GL_FALSE)
    {
        glGetShaderInfoLog(vert, log_len, 0, error_log);
        DEBUG_PRINTF("ImmediateRenderState_init(): failed to compile fragment shader.\n");
        return 3;
    }

    s->shader = glCreateProgram();
    glAttachShader(s->shader, vert);
    glAttachShader(s->shader, frag);
    glLinkProgram(s->shader);
    glDeleteShader(vert);
    glDeleteShader(frag);

    glGetProgramiv(s->shader, GL_LINK_STATUS, &success);

    if (success == GL_FALSE)
    {
        glGetProgramInfoLog(s->shader, log_len, 0, error_log);
        DEBUG_PRINTF("ImmediateRenderState_init(): failed to link shader.\n");
        return 4;
    }

    s->projection_location = glGetUniformLocation(s->shader, "projection");

    if (s->projection_location < 0)
    {
        DEBUG_PRINTF("ImmediateRenderState_init(): could not find matrix location.\n");
        return 5;
    }

    s->model_location = glGetUniformLocation(s->shader, "model");

    if (s->model_location < 0)
    {
        DEBUG_PRINTF("ImmediateRenderState_init(): could not find matrix location.\n");
    }

    s->color_location =  glGetUniformLocation(s->shader, "c");

    if (s->color_location < 0)
    {
        DEBUG_PRINTF("ImmediateRenderState_init(): could not find color location.\n");
        return 6;
    }


    ImmediateRenderState_color(s, 0.0f, 0.0f, 1.0f, 1.0f);

    mat4x4_identity(s->mat_model);

    return 0;
}

bool32
ImmediateRenderState_begin(ImmediateRenderState *state, GLenum mode)
{
    if (state->began) return 0;

    state->mode     = mode;
    state->began    = 1;

    return 1;
}

bool32
ImmediateRenderState_vertex3f(ImmediateRenderState *s,
    float x, float y, float z)
{
    if (s->num_floats + 3 > s->max_floats)
    {
        size_t new_size = (size_t)((float)s->num_floats * 1.1f);

        if (new_size < s->num_floats + 3)
            new_size = s->num_floats + 3;

        GLfloat *vertices = DynamicMemoryBlock_unsafeMalloc(
            &s->renderer->dynamic_memory, new_size * sizeof(GLfloat));

        if (!vertices)
        {
            DEBUG_PRINTF("Failed to allocate more space for immediate mode vertices.\n");
            return 0;
        }

        memcpy(vertices, s->vertices, s->num_floats * sizeof(GLfloat));
        DynamicMemoryBlock_unsafeFree(&s->renderer->dynamic_memory, s->vertices);
        s->vertices = vertices;

        s->max_floats = (uint)new_size;


        glBindVertexArray(s->vao);
        glBindBuffer(GL_ARRAY_BUFFER, s->vbo);
        glBufferData(GL_ARRAY_BUFFER, s->max_floats * sizeof(GLfloat),
            0, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
            3 * sizeof(GLfloat), (const GLvoid*)0);
        glEnableVertexAttribArray(0);
        glBindVertexArray(0);
    }

    s->vertices[s->num_floats + 0] = x;
    s->vertices[s->num_floats + 1] = y;
    s->vertices[s->num_floats + 2] = z;
    s->num_floats += 3;

    return 1;
}

bool32
ImmediateRenderState_end(ImmediateRenderState *s)
{
    if (!s->began) return 0;

    if (s->mode == GL_LINES || s->mode == GL_LINE_STRIP)
        glLineWidth(s->line_width);

    Renderer_clearFrameIfNecessary(s->renderer);

#if 0
    GLsizei num_indices;

    switch (s->mode)
    {
        case GL_LINES:
            num_indices = s->num_floats / 3;
            break;
        case GL_LINE_STRIP:
            /*num_indices = s->num_floats / 3 / 3 * 2;*/
            num_indices = (s->num_floats / 3) + 1;
            break;
        case GL_TRIANGLES:
            num_indices = s->num_floats / 3;
            break;
        default:
            num_indices = s->num_floats / 3;
    }
#endif

    /*  Render to screen */
    glUseProgram(s->shader);
    glUniform4f(s->color_location, s->color[0],
        s->color[1], s->color[2], s->color[3]);
    glBindVertexArray(s->vao);
    glBindBuffer(GL_ARRAY_BUFFER, s->vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, s->num_floats * sizeof(GLfloat),
        s->vertices);
    glDrawArrays(s->mode, 0, s->num_floats / 3);
    glBindVertexArray(0);
    glUseProgram(0);

    s->num_floats = 0;

    return 1;
}

void
ImmediateRenderState_ortho(ImmediateRenderState *state,
    float x, float y, float w, float h,
    float near_clip, float far_clip)
{
    state->matrix[0][0] = 2.0f / (GLfloat)(w - x);
    state->matrix[0][1] = 0.0f;
    state->matrix[0][2] = 0.0f;
    state->matrix[0][3] = 0.0f;

    state->matrix[1][0] = 0.0f;
    state->matrix[1][1] = 2.0f / (GLfloat)(y - h);
    state->matrix[1][2] = 0.0f;
    state->matrix[1][3] = 0.0f;

    state->matrix[2][0] = 0.0f;
    state->matrix[2][1] = 0.0f;
    state->matrix[2][2] = -1.0f;
    state->matrix[2][3] = 0.0f;

    state->matrix[3][0] = -(GLfloat)(w + x) / (GLfloat)(w - x);
    state->matrix[3][1] = -(GLfloat)(y + h) / (GLfloat)(y - h);
    state->matrix[3][2] = 0.0f;
    state->matrix[3][3] = 1.0f;
}

void
ImmediateRenderState_perspective(ImmediateRenderState *state,
    float field_of_view, float aspect,
    float near_clip, float far_clip)
{
    mat4x4_perspective(state->matrix, field_of_view,
        aspect, near_clip, far_clip);
}

void
ImmediateRenderState_lookAt(ImmediateRenderState *state,
    vec3 eye, vec3 target, vec3 up)
{
    mat4x4 m;
    mat4x4_look_at(m, eye, target, up);
    mat4x4_mul(state->matrix, state->matrix, m);
}

void
ImmediateRenderState_lookAtCamera(ImmediateRenderState *state,
    Camera *camera)
{
    mat4x4 *look_at = Camera_updateAndGetViewMatrix(camera);
    mat4x4_mul(state->matrix, state->matrix, *look_at);
}

void
ImmediateRenderState_updateMatrix(ImmediateRenderState *state)
{
    glUseProgram(state->shader);
    glUniformMatrix4fv(state->projection_location,
        1, GL_FALSE, &state->matrix[0][0]);
    glUseProgram(0);
}

