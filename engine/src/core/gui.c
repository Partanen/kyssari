#include "gui.h"
#include "engine.h"
#define NUM_INITIAL_GUI_WINDOWS 32
#define GUI_ROOT_WINDOW_STRING "_GUI_ROOT"

static GUI sgui = {0};
GUI * const gui = &sgui;
static GUIWindowStyle _default_window_style = {0};
static GUIButtonStyle _default_button_style = {0};
static GUIProgressBarStyle _default_progressbar_style = { 0 };
static uint32_t _gui_root_window_id;
GUIButtonStyle * const default_button_style = &_default_button_style;
GUIWindowStyle * const default_window_style = &_default_window_style;
GUIProgressBarStyle * const default_progressbar_style = &_default_progressbar_style;

#define SIZE_OF_GUI_VERTEX ((FLOATS_PER_SPRITE / 4) * sizeof(GLfloat))

static inline int
calculateGUITitleWidth(SpriteFont *font, const char *title)
{
    float width = 0;

    for (const char *c = title; *c; ++c)
    {
        if (*c == '\n' || (*c == '#' && *(c + 1) == '#'))
            break;
        width += font->characters[(int)*c].advance;
    }

    return (int)width;
}

static void
_GUI_resetNextButtonTextures()
{
    memset(&sgui.next_button_normal_texture,  0, sizeof(_GUIButtonTexture));
    memset(&sgui.next_button_hovered_texture, 0, sizeof(_GUIButtonTexture));
    memset(&sgui.next_button_pressed_texture, 0, sizeof(_GUIButtonTexture));
    sgui.next_button_normal_texture.scale[0] = 1.0f;
    sgui.next_button_normal_texture.scale[0] = 1.0f;
    sgui.next_button_textures_changed = 0;
}

static inline GUIButtonStyle *
GUI_getCurrentButtonStyle()
{
    if (sgui.next_button_style) return sgui.next_button_style;
    else if (sgui.button_style) return sgui.button_style;
    else                        return &_default_button_style;
}

static inline GUIProgressBarStyle *
GUI_getCurrentProgressBarStyle()
{
    return sgui.progressbar_style ? sgui.progressbar_style : &_default_progressbar_style;
}

static inline GUIWindowStyle *
GUI_getCurrentWindowStyle()
{
    if (sgui.next_window_style)
        return sgui.next_window_style;
    else
        return default_window_style;
}

static inline GUIWindow *
GUIWindow_getParent(GUIWindow *window)
{
    return window->parent != -1 ? &gui->windows[window->parent] : 0;
}

static inline GUIWindowStyle *
GUIWindow_getStyle(GUIWindow *win)
{
    return win->style ? win->style : default_window_style;
}

static inline SpriteFont *
GUIWindow_getFont(GUIWindow *window)
{
    if (window->style)
        if (window->style->font)
            return window->style->font;

    return getDefaultSpriteFont();
}

static inline SpriteFont *
GUIWindowStyle_getFont(GUIWindowStyle *style)
{
    if (style->font)
        return style->font;
    return getDefaultSpriteFont();
}

/* TODO: Take height in count with the y axis */
static inline void
calculateGUITextLinePosition(int *ret_x, int *ret_y,
    SpriteFont *font, const char *text, enum GUIOrigin origin,
    int given_x, int given_y,
    int parent_x, int parent_y, int parent_width, int parent_height)
{
    int w = calculateGUITitleWidth(font, text);

    switch (origin)
    {
        case GUI_ORIGIN_TOP_LEFT:
        {
            *ret_x = parent_x + given_x;
            *ret_y = parent_y + given_y;
        }
            break;
        case GUI_ORIGIN_TOP_RIGHT:
        {
            *ret_x = parent_x + parent_width - w - given_x;
            *ret_y = parent_y + given_y;
        }
            break;
        case GUI_ORIGIN_BOTTOM_LEFT:
        {
            *ret_x = parent_x + given_x;
            *ret_y = parent_y + parent_height - given_y;
        }
            break;
        case GUI_ORIGIN_BOTTOM_RIGHT:
        {
            *ret_x = parent_x + parent_width - w - given_x;
            *ret_y = parent_y + parent_height - given_y;
        }
            break;
    }
}

static inline void
calculateGUIQuadPositionByOrigin(int *ret_x, int *ret_y,
    int x, int y, int w, int h,
    int parent_x, int parent_y, int parent_w, int parent_h,
    enum GUIOrigin origin)
{
    switch (origin)
    {
        case GUI_ORIGIN_TOP_LEFT:
        {
            *ret_x = parent_x + x;
            *ret_y = parent_y + y;
        }
            break;
        case GUI_ORIGIN_TOP_RIGHT:
        {
            *ret_x = parent_x + parent_w - w - x;
            *ret_y = parent_y + y;
        }
            break;
        case GUI_ORIGIN_BOTTOM_LEFT:
        {
            *ret_x = parent_x + x;
            *ret_y = parent_y + parent_h - h - y;
        }
            break;
        case GUI_ORIGIN_BOTTOM_RIGHT:
        {
            *ret_x = parent_x + parent_w - w - x;
            *ret_y = parent_y + parent_h - h - y;
        }
            break;
    }
}

static void
GUIVertexBuffer_init(GUIVertexBuffer *buffer, uint num_vertices)
{
    buffer->vertices = kmalloc(num_vertices * SIZE_OF_GUI_VERTEX);
    ASSERT(buffer->vertices);
    buffer->capacity = num_vertices;
}

/* Grow capacity by at least 4 vertices */
static void
GUIVertexBuffer_growCapacity(GUIVertexBuffer *buffer)
{
    uint new_size = (uint)((float)buffer->capacity * 1.10f);
    if (new_size < buffer->num_vertices + 4)
        new_size = buffer->num_vertices + 4;

    GLfloat *new_verts = (GLfloat*)kmalloc(new_size * SIZE_OF_GUI_VERTEX);
    ASSERT(new_verts);
    memcpy(new_verts, buffer->vertices,
        buffer->num_vertices * SIZE_OF_GUI_VERTEX);
    kfree(buffer->vertices);
    buffer->vertices = new_verts;
    buffer->capacity = new_size;
    DEBUG_PRINTF("Increased the capacity of a GUIVertexBuffer to %u vertices.\n", new_size);
}

static void
GUIVertexBuffer_ensureCapacity(GUIVertexBuffer *buffer, uint capacity)
{
    if (buffer->capacity >= capacity)
        return;

    GLfloat *new_verts = (GLfloat*)kmalloc(capacity * SIZE_OF_GUI_VERTEX);
    ASSERT(new_verts);
    memcpy(new_verts, buffer->vertices,
        buffer->num_vertices * SIZE_OF_GUI_VERTEX);
    kfree(buffer->vertices);
    buffer->vertices = new_verts;
    buffer->capacity = capacity;
    DEBUG_PRINTF("Increased (ensured) the capacity of a GUIVertexBuffer to %u "
        "vertices.\n", capacity);
}

/* So that if we're printing out a longer string for the first time, we don't do a million
 * allocs during the print out, enlarging the buffer by a mere 5 % every time */
static inline void
GUIVertexBuffer_ensureCapacityForString(GUIVertexBuffer *buffer, const char *str)
{
    uint len = (uint)strlen(str);
    GUIVertexBuffer_ensureCapacity(buffer, buffer->capacity + (len * 4));
}

/* Returns the index of the command to the argument "index" */
static inline GUIDrawCommand *
GUICommandBuffer_pushDrawCommand(int *index)
{
    /* Increase size if necessary */
    if (sgui.command_buffer.capacity < sgui.command_buffer.num_commands + 1)
    {
        uint new_size = (uint)((float)sgui.command_buffer.capacity * 1.05f);
        if (new_size <= sgui.command_buffer.capacity + 1)
            new_size = sgui.command_buffer.capacity + 1;

        GUIDrawCommand *new_cmds = kmalloc(new_size * sizeof(GUIDrawCommand));
        ASSERT(new_cmds);
        memcpy(new_cmds, sgui.command_buffer.commands,
            sgui.command_buffer.num_commands * sizeof(GUIDrawCommand));
        kfree(sgui.command_buffer.commands);
        sgui.command_buffer.commands = new_cmds;
        sgui.command_buffer.capacity = new_size;
    }

    *index = sgui.command_buffer.num_commands;
    ++sgui.command_buffer.num_commands;
    sgui.command_buffer.commands[*index].next = -1;

    return &sgui.command_buffer.commands[*index];
}

static uint
GUIVertexBuffer_drawColorQuad(GUIVertexBuffer *buffer,
    int x, int y, int w, int h, float *color)
{
    uint index = buffer->num_vertices;

    /* Increase size if necessary */
    if (buffer->capacity < buffer->num_vertices + 4)
    {
        GUIVertexBuffer_growCapacity(buffer);
    }

    GLfloat *verts = &buffer->vertices[buffer->num_vertices * 8];

    /* Position */
    verts[ 0] = (GLfloat)x;
    verts[ 1] = (GLfloat)y;
    verts[ 8] = (GLfloat)(x + w);
    verts[ 9] = (GLfloat)y;
    verts[16] = (GLfloat)x;
    verts[17] = (GLfloat)(y + h);
    verts[24] = (GLfloat)(x + w);
    verts[25] = (GLfloat)(y + h);

    /* Color */
    verts[ 4] = (GLfloat)color[0];
    verts[ 5] = (GLfloat)color[1];
    verts[ 6] = (GLfloat)color[2];
    verts[ 7] = (GLfloat)color[3];
    verts[12] = (GLfloat)color[0];
    verts[13] = (GLfloat)color[1];
    verts[14] = (GLfloat)color[2];
    verts[15] = (GLfloat)color[3];
    verts[20] = (GLfloat)color[0];
    verts[21] = (GLfloat)color[1];
    verts[22] = (GLfloat)color[2];
    verts[23] = (GLfloat)color[3];
    verts[28] = (GLfloat)color[0];
    verts[29] = (GLfloat)color[1];
    verts[30] = (GLfloat)color[2];
    verts[31] = (GLfloat)color[3];

    buffer->num_vertices += 4;
    return index;
}

static inline uint
GUIVertexBuffer_drawTextureQuad(GUIVertexBuffer *buffer,
    Texture *texture,
    int x, int y,
    int clip_x, int clip_y, int clip_w, int clip_h,
    float *color,
    float scale_x, float scale_y)
{
    uint index = buffer->num_vertices;

    /* Increase size if necessary */
    if (buffer->capacity < buffer->num_vertices + 4)
    {
        GUIVertexBuffer_growCapacity(buffer);
    }

    GLfloat *verts = &buffer->vertices[buffer->num_vertices * 8];

    /* Position */
    verts[ 0] = (GLfloat)x;
    verts[ 1] = (GLfloat)y;
    verts[ 8] = (GLfloat)x + (GLfloat)clip_w * (GLfloat)scale_x;
    verts[ 9] = (GLfloat)y;
    verts[16] = (GLfloat)x;
    verts[17] = (GLfloat)y + (GLfloat)clip_h * (GLfloat)scale_y;
    verts[24] = (GLfloat)x + (GLfloat)clip_w * (GLfloat)scale_x;
    verts[25] = (GLfloat)y + (GLfloat)clip_h * (GLfloat)scale_y;

    /* Color */
    verts[ 4] = (GLfloat)color[0];
    verts[ 5] = (GLfloat)color[1];
    verts[ 6] = (GLfloat)color[2];
    verts[ 7] = (GLfloat)color[3];
    verts[12] = (GLfloat)color[0];
    verts[13] = (GLfloat)color[1];
    verts[14] = (GLfloat)color[2];
    verts[15] = (GLfloat)color[3];
    verts[20] = (GLfloat)color[0];
    verts[21] = (GLfloat)color[1];
    verts[22] = (GLfloat)color[2];
    verts[23] = (GLfloat)color[3];
    verts[28] = (GLfloat)color[0];
    verts[29] = (GLfloat)color[1];
    verts[30] = (GLfloat)color[2];
    verts[31] = (GLfloat)color[3];

    /* Clip */
    verts[ 2] = (GLfloat)clip_x / (GLfloat)texture->width;
    verts[ 3] = (GLfloat)clip_y / (GLfloat)texture->height;
    verts[10] = (GLfloat)(clip_x + clip_w) / (GLfloat)texture->width;
    verts[11] = (GLfloat)clip_y / (GLfloat)texture->height;
    verts[18] = (GLfloat)clip_x / (GLfloat)texture->width;
    verts[19] = (GLfloat)(clip_y + clip_h) / (GLfloat)texture->height;
    verts[26] = (GLfloat)(clip_x + clip_w) / (GLfloat)texture->width;
    verts[27] = (GLfloat)(clip_y + clip_h) / (GLfloat)texture->height;

    buffer->num_vertices += 4;
    return index;
}

static inline GUIDrawCommand *
GUIWindow_pushDrawCommand(GUIWindow *window)
{
    int *index_ptr = &window->draw_commands_index;

    /* Find the tail */
    while (*index_ptr != -1)
        index_ptr = &sgui.command_buffer.commands[*index_ptr].next;

    return GUICommandBuffer_pushDrawCommand(index_ptr);
}

static void
GUIWindow_drawColorQuad(GUIWindow *window,
    int x, int y, int w, int h, float *color)
{
    GUIDrawCommand *cmd = GUIWindow_pushDrawCommand(window);
    cmd->texture        = _BLANK_WHITE_TEXTURE->id;
    cmd->num_vertices   = 4;
    cmd->vertex_index   = GUIVertexBuffer_drawColorQuad(
        &window->vertex_buffer, x, y, w, h, color);
}

static inline void
GUIWindow_drawTexQuad(GUIWindow *window,
    Texture *texture,
    int x, int y, int w, int h,
    int clip_x, int clip_y, int clip_w, int clip_h,
    float *color, float scale_x, float scale_y)
{
    GUIDrawCommand *cmd = GUIWindow_pushDrawCommand(window);
    cmd->texture        = texture->id;
    cmd->num_vertices   = 4;
    cmd->vertex_index   = GUIVertexBuffer_drawTextureQuad(
        &window->vertex_buffer,
        texture, x, y, clip_x, clip_y, clip_w, clip_h, color,
        scale_x, scale_y);
}

#define GUI_DRAW_CHAR(vbuffer, font, schar, x, y, color, scale) \
{ \
    verts[ 0] = (GLfloat)(x + schar->min_x); \
    verts[ 1] = (GLfloat)y; \
    verts[ 8] = (GLfloat)(x + (schar->min_x + (GLfloat)schar->clip[2]) * scale); \
    verts[ 9] = (GLfloat)y; \
    verts[16] = (GLfloat)(x + schar->min_x); \
    verts[17] = (GLfloat)(y + schar->clip[3] * scale); \
    verts[24] = (GLfloat)(x + (schar->min_x + (GLfloat)schar->clip[2]) * scale); \
    verts[25] = (GLfloat)(y + (GLfloat)schar->clip[3] * scale); \
\
    verts[ 4] = (GLfloat)color[0]; \
    verts[ 5] = (GLfloat)color[1]; \
    verts[ 6] = (GLfloat)color[2]; \
    verts[ 7] = (GLfloat)color[3]; \
    verts[12] = (GLfloat)color[0]; \
    verts[13] = (GLfloat)color[1]; \
    verts[14] = (GLfloat)color[2]; \
    verts[15] = (GLfloat)color[3]; \
    verts[20] = (GLfloat)color[0]; \
    verts[21] = (GLfloat)color[1]; \
    verts[22] = (GLfloat)color[2]; \
    verts[23] = (GLfloat)color[3]; \
    verts[28] = (GLfloat)color[0]; \
    verts[29] = (GLfloat)color[1]; \
    verts[30] = (GLfloat)color[2]; \
    verts[31] = (GLfloat)color[3]; \
\
    verts[ 2] = (GLfloat)schar->clip[0] / (GLfloat)font->texture.width; \
    verts[ 3] = (GLfloat)schar->clip[1] / (GLfloat)font->texture.height; \
    verts[10] = (GLfloat)(schar->clip[0] + schar->clip[2]) / (GLfloat)font->texture.width; \
    verts[11] = (GLfloat)schar->clip[1] / (GLfloat)font->texture.height; \
    verts[18] = (GLfloat)schar->clip[0] / (GLfloat)font->texture.width; \
    verts[19] = (GLfloat)(schar->clip[1] + schar->clip[3]) / (GLfloat)font->texture.height; \
    verts[26] = (GLfloat)(schar->clip[0] + schar->clip[2]) / (GLfloat)font->texture.width; \
    verts[27] = (GLfloat)(schar->clip[1] + schar->clip[3]) / (GLfloat)font->texture.height; \
}

void
GUIWindow_drawTextLine(GUIWindow *window, int x, int y,
    SpriteFont *font, const char *text, float *color, float scale)
{
    GUIDrawCommand *cmd     = GUIWindow_pushDrawCommand(window);
    GUIVertexBuffer *buffer = &window->vertex_buffer;

    cmd->texture        = font->texture.id;
    cmd->vertex_index   = buffer->num_vertices;
    cmd->num_vertices   = 0;

    int             index;
    GLfloat         cur_x = (GLfloat)x;
    GLfloat         *verts;
    SpriteFontChar  *schar;

    for (const char *c = text; *c; ++c)
    {
        index = (int)(*c);

        if (*c == '\n')
        {
            break;
        }
        else
        {
            if (buffer->capacity < buffer->num_vertices + cmd->num_vertices + 4)
            {
                GUIVertexBuffer_ensureCapacityForString(buffer, c);
            }

            verts = &buffer->vertices[(buffer->num_vertices + cmd->num_vertices) * 8];
            schar = &font->characters[index];

            GUI_DRAW_CHAR(buffer, font, schar, cur_x, y, color, scale);

            cur_x += schar->advance * scale;
            cmd->num_vertices += 4;
        }
    }

    buffer->num_vertices += cmd->num_vertices;
}

void
GUIWindow_drawText(GUIWindow *window, int x, int y,
    int max_width, int max_height,
    SpriteFont *font, const char *text, float *color,
    float scale)
{
    GUIDrawCommand *cmd     = GUIWindow_pushDrawCommand(window);
    GUIVertexBuffer *buffer = &window->vertex_buffer;

    cmd->texture        = font->texture.id;
    cmd->vertex_index   = buffer->num_vertices;
    cmd->num_vertices   = 0;

    int             index;
    GLfloat         cur_x = (GLfloat)x;
    GLfloat         cur_y = (GLfloat)y;
    GLfloat         *verts;
    SpriteFontChar  *schar;

    for (const char *c = text; *c; ++c)
    {
        index = (int)(*c);

        if (*c == '\n')
        {
            cur_x = (GLfloat)x;
            cur_y += (GLfloat)font->height * scale;
        }
        else
        {
            if (buffer->capacity < buffer->num_vertices + cmd->num_vertices + 4)
            {
                GUIVertexBuffer_ensureCapacityForString(buffer, c);
            }

            verts = &buffer->vertices[(buffer->num_vertices + cmd->num_vertices) * 8];
            schar = &font->characters[index];

            if (cur_x + (GLfloat)schar->advance > (GLfloat)(x + max_width))
            {
                cur_x = (GLfloat)x;
                cur_y += (GLfloat)font->height * scale;
            }

            GUI_DRAW_CHAR(buffer, font, schar, cur_x, cur_y, color, scale);

            cur_x += (schar->advance * scale);
            cmd->num_vertices += 4;
        }
    }

    buffer->num_vertices += cmd->num_vertices;
}

static inline void
GUIWindow_drawTitleTextLine(GUIWindow *window, int x, int y,
    SpriteFont *font, const char *text, float *color, float scale)
{
    GUIDrawCommand *cmd     = GUIWindow_pushDrawCommand(window);
    GUIVertexBuffer *buffer = &window->vertex_buffer;

    cmd->texture        = font->texture.id;
    cmd->vertex_index   = buffer->num_vertices;
    cmd->num_vertices   = 0;

    int             index;
    GLfloat         cur_x = (GLfloat)x;
    GLfloat         *verts;
    SpriteFontChar  *schar;

    for (const char *c = text; *c; ++c)
    {
        index = (int)(*c);

        if (*c == '\n')
        {
            break;
        }
        else if (*c == '#' && *(c + 1) == '#')
        {
            break;
        }
        else
        {
            if (buffer->capacity < buffer->num_vertices + cmd->num_vertices + 4)
            {
                GUIVertexBuffer_growCapacity(buffer);
            }

            verts = &buffer->vertices[(buffer->num_vertices + cmd->num_vertices) * 8];
            schar = &font->characters[index];

            GUI_DRAW_CHAR(buffer, font, schar, cur_x, y, color, scale);

            cur_x += schar->advance * scale;
            cmd->num_vertices += 4;
        }
    }

    buffer->num_vertices += cmd->num_vertices;
}

static inline GUIWindow *
findGUIWindow(uint32_t id)
{
    for (GUIWindow *w = gui->windows;
         w < &gui->windows[gui->num_reserved_windows];
         ++w)
    {
        if (w->id == id)
            return w;
    }

    return 0;
}

static GUIWindow *
createGUIWindow(uint32_t id)
{
    /* Enlarge window array if necessary */
    if (gui->num_reserved_windows + 1 > gui->num_windows)
    {
        /* Enlarge by 10% */
        uint new_size = (uint)((float)gui->num_windows * 1.1f);

        if (new_size <= gui->num_reserved_windows)
            new_size = gui->num_reserved_windows + 1;

        GUIWindow *new_windows = (GUIWindow*)kmalloc(new_size * sizeof(GUIWindow));
        ushort *new_sorted_windows = (ushort*)kmalloc(new_size * sizeof(ushort));
        ASSERT(new_windows);
        ASSERT(new_sorted_windows);
        memcpy(new_windows, gui->windows, gui->num_reserved_windows * sizeof(GUIWindow));
        memcpy(new_sorted_windows, gui->sorted_windows, gui->num_sorted_windows * sizeof(ushort));
        kfree(gui->windows);
        kfree(gui->sorted_windows);
        gui->windows = new_windows;
        gui->sorted_windows = new_sorted_windows;
        gui->num_windows = new_size;
    }

    GUIWindow *win = &gui->windows[gui->num_reserved_windows];
    win->id = id;
    win->index = (ushort)gui->num_reserved_windows;
    win->last_made_active = sgui.frame_count; /* Make active upon creation */
    win->style = GUI_getCurrentWindowStyle();;
    GUIVertexBuffer_init(&win->vertex_buffer, 64);
    ++gui->num_reserved_windows;

    DEBUG_PRINTF("GUI: Created window %u\n", id);

    return win;
}

static inline GUIWindow *
findOrCreateGUIWindow(uint32_t id)
{
    GUIWindow *window = findGUIWindow(id);
    if (!window)
        window = createGUIWindow(id);
    return window;
}

int
GUI_init()
{
    gui->began_update = 0;
    gui->windows = kmalloc(NUM_INITIAL_GUI_WINDOWS * sizeof(GUIWindow));
    gui->sorted_windows = kmalloc(NUM_INITIAL_GUI_WINDOWS * sizeof(ushort));

    if (!gui->windows || !gui->sorted_windows)
    {
        DEBUG_PRINTF("GUI: Failed to allocate windows!\n");
        return 1;
    }

    gui->num_windows = NUM_INITIAL_GUI_WINDOWS;
    gui->num_reserved_windows = 0;

    /* Initialize the draw command buffer */
    sgui.command_buffer.commands = kmalloc(FLOATS_PER_SPRITE * 256 * sizeof(GUIDrawCommand));
    sgui.command_buffer.capacity = 256;

    if (!sgui.command_buffer.commands)
    {
        DEBUG_PRINTF("GUI: failed to allocate commands!\n");
        return 2;
    }

    /* Default window style */
    {
        setColorf(_default_window_style.color_background_normal,
            0.05f, 0.05f, 0.05f, 1.00f);
        setColorf(_default_window_style.color_background_active,
            0.02f, 0.02f, 0.02f, 1.00f);
        setColorf(_default_window_style.color_border_normal,
            0.14f, 0.14f, 0.29f, 1.00f);
        copyColorf(_default_window_style.color_border_active, F_COLOR_SYS_BLUE);
        copyColorf(_default_window_style.color_title, F_COLOR_WHITE);

        _default_window_style.border_width_top      = 15;
        _default_window_style.border_width_bottom   = 1;
        _default_window_style.border_width_left     = 1;
        _default_window_style.border_width_right    = 1;

        _default_window_style.show_title = 1;
        _default_window_style.title_origin = GUI_ORIGIN_TOP_LEFT;
        _default_window_style.title_position[0] = 1;
        _default_window_style.title_position[1] = 1;
    }

    /* Default button style */
    {
        setColorf(_default_button_style.color_background_normal,
            0.05f, 0.05f, 0.05f, 1.00f);
        setColorf(_default_button_style.color_background_hovered,
            0.05f, 0.05f, 0.05f, 1.00f);
        setColorf(_default_button_style.color_background_pressed,
            0.05f, 0.05f, 0.05f, 1.00f);

        setColorf(_default_button_style.color_border_normal,
            0.10f, 0.05f, 0.05f, 1.00f);
        setColorf(_default_button_style.color_border_hovered,
            0.50f, 0.05f, 0.05f, 1.00f);
        setColorf(_default_button_style.color_border_pressed,
            1.00f, 0.05f, 0.05f, 1.00f);

        _default_button_style.border_width_top      = 1;
        _default_button_style.border_width_bottom   = 1;
        _default_button_style.border_width_left     = 1;
        _default_button_style.border_width_right    = 1;

        _default_button_style.show_title = 1;
        _default_button_style.title_position[0] = 0;
        _default_button_style.title_position[1] = 0;
        _default_button_style.title_font = getDefaultSpriteFont();
        _default_button_style.title_scale = 1.0f;
    }

    /*default progressbar style*/
    {
        _default_progressbar_style.border_texture = loadTexture("assets/textures/hp_border.png");
        _default_progressbar_style.background_texture = loadTexture("assets/textures/hp_background_fill.png");
        _default_progressbar_style.fill_texture = loadTexture("assets/textures/hp_fill.png");
        _default_progressbar_style.font = getDefaultSpriteFont();
        _default_progressbar_style.text_option = GUI_BAR_SHOW_VALUE;
        _default_progressbar_style.show_background = 1;
        _default_progressbar_style.border_on_top = 0;
        _default_progressbar_style.show_border = 1;
        _default_progressbar_style.texture_scale = 0.5f;
    }

    _gui_root_window_id = hashFromString(GUI_ROOT_WINDOW_STRING);

    sgui.next_font          = 0;
    sgui.next_window_style  = 0;
    sgui.next_button_style  = 0;

    _GUI_resetNextButtonTextures();
    GUI_setDefaultOrigin(GUI_ORIGIN_TOP_LEFT);
    GUI_setDefaultTextScale(1.0f);

    return 0;
}

static inline void
tryMakeWindowHovered(GUIWindow *window)
{
    if (sgui.hovered_window_index != -1)
    {
        GUIWindow *w = &sgui.windows[sgui.hovered_window_index];
        GUIWindow *parent = &sgui.windows[sgui.current_window];

        if (parent->last_made_active >= w->last_made_active)
            sgui.hovered_window_index = window->index;
    }
    else
    {
        sgui.hovered_window_index = window->index;
    }
}

 /* Called before updating any of the rest of the UI state */
void
GUI_beginUpdate()
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(!sgui.began_update);

    sgui.began_update                   = 1;
    sgui.command_buffer.num_commands    = 0;
    sgui.num_sorted_windows             = 0;
    sgui.current_window                 = -1;
    sgui.next_active_window             = -1;
    sgui.next_pressed_button            = 0;
    sgui.next_hovered_button            = 0;
    sgui.hovered_window_index           = -1;

    if (sgui.next_button_textures_changed)
        _GUI_resetNextButtonTextures();

    sgui.frame_count += 1;

    GUI_beginEmptyWindow(GUI_ROOT_WINDOW_STRING,
        0, 0, (int)engine.window.width,
        (int)engine.window.height);
}

void
GUI_endUpdate()
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(gui->current_window != -1
        && sgui.windows[sgui.current_window].id == _gui_root_window_id);
    GUI_endWindow();

    ASSERT(gui->began_update);

    if (sgui.next_active_window != -1)
    {
        sgui.active_window = sgui.windows[sgui.next_active_window].id;
        sgui.windows[sgui.next_active_window].last_made_active = sgui.frame_count;
        DEBUG_PRINTF("GUI: Making window %u active.\n", sgui.active_window);
    }

    if (!isRelativeMouseMode())
    {
        if (mouseButtonDownThisFrame(MOUSE_BUTTON_LEFT))
        {
            uint32_t next_pressed_button = 0;

            /* Activate the next pressed button only if its parent
             * was the current active window */
            if (sgui.next_pressed_button)
            {
                GUIWindow *window = &sgui.windows[sgui.next_pressed_button_parent_index];

                if (window->id == sgui.windows[sgui.hovered_window_index].id)
                {
                    next_pressed_button = sgui.next_pressed_button;
                }
            }

            sgui.pressed_button = next_pressed_button;
        }
        else if (!getMouseButtonState(MOUSE_BUTTON_LEFT))
        {
            sgui.pressed_button = sgui.next_pressed_button;
        }
    }
    else
    {
        sgui.pressed_button = 0;
    }

    /* Make the next hovered button hovered only if a higher window was not hovered */
    if (sgui.next_hovered_button_parent_index == sgui.hovered_window_index)
        sgui.hovered_button = sgui.next_hovered_button;
    else
        sgui.hovered_button = 0;

    /* Sort gui windows */
    {
        bool32 swapped = 1;
        GUIWindow *w1, *w2;
        ushort tmp;

        while (swapped)
        {
            swapped = 0;

            for (ushort *index = sgui.sorted_windows;
                 index < &sgui.sorted_windows[sgui.num_sorted_windows - 1];
                 ++index)
            {
                w1 = &sgui.windows[*index];
                w2 = &sgui.windows[*(index + 1)];

                if (w1->last_made_active > w2->last_made_active)
                {
                    tmp = *index;
                    *index = *(index + 1);
                    *(index + 1) = tmp;
                    swapped = 1;
                }
            }
        }
    }

    sgui.began_update = 0;
}

static void
GUI_calcButtonPosition(int *ret_x, int *ret_y,
    GUIWindow *win, GUIWindowStyle *win_style,
    int bx, int by, int bw, int bh)
{
    switch (sgui.next_origin)
    {
        case GUI_ORIGIN_TOP_LEFT:
        {
            *ret_x = win->true_position[0] + bx + win_style->border_width_left;
            *ret_y = win->true_position[1] + by + win_style->border_width_top;
        }
            break;
        case GUI_ORIGIN_TOP_RIGHT:
        {
            *ret_x = win->true_position[0] + win->dimensions[0] - bw - bx;
            *ret_y = win->true_position[1] + by;
        }
            break;
        case GUI_ORIGIN_BOTTOM_LEFT:
        {
            *ret_x = win->true_position[0] + bx;
            *ret_y = win->true_position[1] + win->dimensions[1] - by;
        }
            break;
        case GUI_ORIGIN_BOTTOM_RIGHT:
        {
            *ret_x = win->true_position[0] + win->dimensions[0] - bw - bx;
            *ret_y = win->true_position[1] + win->dimensions[1] - by;
        }
            break;
    }
}

static bool32
GUI_updateButtonState(uint32_t id, GUIWindow *win,
    int true_x, int true_y, int w, int h)
{
    bool32 up_this_frame = 0;

    /* Test if the button is pressed */
    if (!isRelativeMouseMode()
    &&  TEST_POINT_IN_RECT(getMousePosition()[0],
            getMousePosition()[1], true_x, true_y, w, h))
    {
        if (!getMouseButtonState(MOUSE_BUTTON_LEFT))
        {
            if (mouseButtonUpThisFrame(MOUSE_BUTTON_LEFT)
            && sgui.pressed_button == id)
                up_this_frame = 1;

            if (sgui.next_hovered_button == 0 ||
                win->last_made_active >=
                sgui.windows[sgui.next_hovered_button_parent_index].last_made_active)
            {
                sgui.next_hovered_button = id;
                sgui.next_hovered_button_parent_index = win->index;
            }
        }
        else if (mouseButtonDownThisFrame(MOUSE_BUTTON_LEFT))
        {
            sgui.next_pressed_button = id;
            sgui.next_pressed_button_parent_index = win->index;
        }
    }

    return up_this_frame;
}

bool32
GUI_button(const char *label,
    int x, int y, int w, int h)
{
    if (KYS_IS_TEXT_MODE()) return 0;

    ASSERT(sgui.began_update && sgui.current_window != -1);
    uint32_t id = hashFromString(label);
    GUIWindow *win = &sgui.windows[sgui.current_window];
    GUIWindowStyle *win_style = GUIWindow_getStyle(win);

    int true_x, true_y;
    GUI_calcButtonPosition(&true_x, &true_y, win, win_style, x, y, w, h);

    bool32 up_this_frame = GUI_updateButtonState(id, win, true_x, true_y, w, h);

    /* Render */
    GUIButtonStyle *style = GUI_getCurrentButtonStyle();
    float *color_bg;
    float *color_border;

    enum
    {
        GUI_BUTTON_STATE_NORMAL,
        GUI_BUTTON_STATE_HOVERED,
        GUI_BUTTON_STATE_PRESSED
    } state;

    if (gui->pressed_button == id)      state = GUI_BUTTON_STATE_PRESSED;
    else if (gui->hovered_button == id) state = GUI_BUTTON_STATE_HOVERED;
    else                                state = GUI_BUTTON_STATE_NORMAL;

    _GUIButtonTexture *button_tex;

    switch (state)
    {
        case GUI_BUTTON_STATE_NORMAL:
        {
            if (!sgui.next_button_normal_texture.texture)
            {
                color_bg        = style->color_background_normal;
                color_border    = style->color_border_normal;
                button_tex = 0;
            }
            else
                button_tex = &sgui.next_button_normal_texture;
        }
            break;
        case GUI_BUTTON_STATE_HOVERED:
        {
            if (sgui.next_button_hovered_texture.texture)
                button_tex = &sgui.next_button_hovered_texture;
            else if (sgui.next_button_normal_texture.texture)
                button_tex = &sgui.next_button_normal_texture;
            else
            {
                color_bg = style->color_background_hovered;
                color_border = style->color_border_hovered;
                button_tex = 0;
            }
        }
            break;
        case GUI_BUTTON_STATE_PRESSED:
        {
            if (sgui.next_button_pressed_texture.texture)
            {
                button_tex = &sgui.next_button_pressed_texture;
            }
            else if (sgui.next_button_hovered_texture.texture)
            {
                button_tex = &sgui.next_button_hovered_texture;
            }
            else if (sgui.next_button_normal_texture.texture)
            {
                button_tex = &sgui.next_button_normal_texture;
            }
            else
            {
                color_bg = style->color_background_pressed;
                color_border = style->color_border_pressed;
                button_tex = 0;
            }
        }
            break;
    }

    /* Render without texture */
    if (!button_tex)
    {
        /* Render borders */
        if (color_border[3] > 0.0f)
            GUIWindow_drawColorQuad(win, true_x, true_y, w, h, color_border);

        /* Render background */
        if (color_bg[3] > 0.0f)
        {
            GUIWindow_drawColorQuad(win,
                true_x + style->border_width_left,
                true_y + style->border_width_top,
                w - style->border_width_left - style->border_width_right,
                h - style->border_width_top - style->border_width_bottom,
                color_bg);
        }
    }
    /* Render with texture */
    else
    {
        GUIWindow_drawTexQuad(win, button_tex->texture,
            true_x + button_tex->offset[0], true_y + button_tex->offset[1],
            button_tex->clip[2], button_tex->clip[3],
            button_tex->clip[0], button_tex->clip[1],
            button_tex->clip[2], button_tex->clip[3],
            F_COLOR_WHITE, button_tex->scale[0], button_tex->scale[1]);
    }

    /* Render title */
    if (style->show_title && style->title_scale > 0.0f)
    {
        int text_x, text_y;
        SpriteFont *font;

        font = !style->title_font ? getDefaultSpriteFont() : style->title_font;

        calculateGUITextLinePosition(&text_x, &text_y, font,
            label, style->title_origin,
            style->title_position[0],
            style->title_position[1],
            true_x, true_y, w, h);

        /*GUIWindow_drawTextLine(win, text_x, text_y,
            font, label, F_COLOR_WHITE, style->title_scale);
*/
        GUIWindow_drawTitleTextLine(win, text_x, text_y, font,
            label, F_COLOR_WHITE, style->title_scale);
    }

    sgui.next_origin        = 0;
    sgui.next_button_style  = sgui.button_style;

    if (sgui.next_button_textures_changed)
        _GUI_resetNextButtonTextures();

    return up_this_frame;
}

bool32
GUI_texButton(const char *label, Texture *tex, int *clip,
    float scale_x, float scale_y,
    int tex_offset_x, int tex_offset_y, /* From top left ignoring gui_origin */
    int x, int y, int w, int h)
{
    if (KYS_IS_TEXT_MODE()) return 0;

    ASSERT(sgui.began_update && sgui.current_window != -1);
    uint32_t id = hashFromString(label);
    GUIWindow *win = &sgui.windows[sgui.current_window];
    GUIWindowStyle *win_style = GUIWindow_getStyle(win);

    int true_x, true_y;
    GUI_calcButtonPosition(&true_x, &true_y, win, win_style, x, y, w, h);

    bool32 up_this_frame = GUI_updateButtonState(id, win, true_x, true_y, w, h);

    static int backup_clip[4] = {0};
    int *true_clip;

    if (clip)
        true_clip = clip;
    else
    {
        backup_clip[2] = tex->width;
        backup_clip[3] = tex->width;
        true_clip = backup_clip;
    }

    /* Render */
    if (tex)
    {
        GUIWindow_drawTexQuad(win, tex,
            true_x + tex_offset_x, true_y + tex_offset_y,
            true_clip[2], true_clip[3],
            true_clip[0], true_clip[1], true_clip[2], true_clip[3],
            F_COLOR_WHITE, scale_x, scale_y);
    }

    /* Render title */
    GUIButtonStyle *style = GUI_getCurrentButtonStyle();

    if (style->show_title && style->title_scale > 0.0f)
    {
        int text_x, text_y;
        SpriteFont *font;

        font = !style->title_font ? getDefaultSpriteFont() : style->title_font;

        calculateGUITextLinePosition(&text_x, &text_y, font,
            label, style->title_origin,
            x + win_style->border_width_left + style->title_position[0],
            y + win_style->border_width_top  + style->title_position[1],
            win->true_position[0], win->true_position[1], w, h);

        GUIWindow_drawTextLine(win, text_x, text_y,
            font, label, F_COLOR_WHITE, style->title_scale);
    }

    sgui.next_origin        = 0;
    sgui.next_button_style  = sgui.button_style;

    return up_this_frame;
}

void
GUI_progressBar(uint current_value, uint max_value, int x, int y, int fill_offset_x,
    int fill_offset_y, int fill_direction, float *text_color)
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(sgui.began_update && sgui.current_window != -1);
    GUIWindow *win = &sgui.windows[sgui.current_window];
    GUIProgressBarStyle *style = GUI_getCurrentProgressBarStyle();

    int quad_x, quad_y;
    char value_str[128];
    char max_value_str[128];

    calculateGUIQuadPositionByOrigin(&quad_x, &quad_y,
        x, y, style->background_texture->width, style->background_texture->height,
        win->true_position[0], win->true_position[1],
        win->dimensions[0], win->dimensions[1],
        sgui.next_origin);

    if (!style->border_on_top)
    {
        if (style->show_border && style->border_texture != 0)
            GUIWindow_drawTexQuad(win,
                style->border_texture, quad_x, quad_y,
                0, 0, style->border_texture->width, style->border_texture->height,
                style->border_texture->width, style->border_texture->height,
                F_COLOR_WHITE, style->texture_scale, style->texture_scale);
    }

    if(style->show_background && style->background_texture != 0)
        GUIWindow_drawTexQuad(win,
            style->background_texture, quad_x + fill_offset_x, quad_y + fill_offset_y,
            0, 0, style->background_texture->width, style->background_texture->height,
            style->background_texture->width, style->background_texture->height,
            F_COLOR_WHITE, style->texture_scale, style->texture_scale);

    if (style->fill_texture == 0)
        return;

    switch (fill_direction)
    {
    case GUI_FILLDIRECTION_RIGHT:
    {
        GUIWindow_drawTexQuad(win,
            style->fill_texture, quad_x + fill_offset_x, quad_y + fill_offset_y,
            0, 0, style->fill_texture->width, style->fill_texture->height,
            style->fill_texture->width - (int)(style->fill_texture->width * ((max_value - current_value) * 0.01f)),
            style->fill_texture->height,
            F_COLOR_WHITE, style->texture_scale, style->texture_scale);
    }
    break;
    case GUI_FILLDIRECTION_LEFT:
    {
        GUIWindow_drawTexQuad(win,
            style->fill_texture, quad_x + fill_offset_x + (int)(style->fill_texture->width * style->texture_scale), quad_y + fill_offset_y,
            0, 0, style->fill_texture->width, style->fill_texture->height,
            (int)(style->fill_texture->width * ((current_value) * 0.01f)) * -1,
            style->fill_texture->height,
            F_COLOR_WHITE, style->texture_scale, style->texture_scale);
    }
    break;
    case GUI_FILLDIRECTION_TOP:
    {
        GUIWindow_drawTexQuad(win,
            style->fill_texture, quad_x + fill_offset_x, quad_y + fill_offset_y,
            0, 0, style->fill_texture->width, style->fill_texture->height,
            style->fill_texture->width,
            style->fill_texture->height - (int)(style->fill_texture->height * ((max_value - current_value) * 0.01f)),
            F_COLOR_WHITE, style->texture_scale, style->texture_scale);
    }
    break;
    case GUI_FILLDIRECTION_BOTTOM:
    {
        GUIWindow_drawTexQuad(win,
            style->fill_texture,
            quad_x + fill_offset_x, quad_y + fill_offset_y + (int)(style->fill_texture->height * style->texture_scale),
            0, 0, style->fill_texture->width, style->fill_texture->height,
            style->fill_texture->width, (int)(style->fill_texture->height * ((current_value) * 0.01f)) * -1,
            F_COLOR_WHITE, style->texture_scale, style->texture_scale);
    }
    break;
    }

    if (style->border_on_top)
    {
        if (style->show_border && style->border_texture != 0)
            GUIWindow_drawTexQuad(win,
                style->border_texture, quad_x, quad_y,
                0, 0, style->border_texture->width, style->border_texture->height,
                style->border_texture->width, style->border_texture->height,
                F_COLOR_WHITE, style->texture_scale, style->texture_scale);
    }

    if (style->text_option == GUI_BAR_SHOW_VALUE)
    {
        sprintf(value_str, "%d", (int)current_value);

        GUIWindow_drawTextLine(win, quad_x + fill_offset_x + (int)(style->background_texture->width * style->texture_scale) / 2,
            quad_y + fill_offset_y - (int)style->font->height / 2 + (int)(style->background_texture->height * style->texture_scale) / 2,
            style->font, value_str, text_color, 1.0f);
    }

    else if (style->text_option == GUI_BAR_SHOW_VALUE_WITH_MAX)
    {
        sprintf(value_str, "%d", (int)current_value);
        sprintf(max_value_str, "/ %d", (int)max_value);

        GUIWindow_drawTextLine(win, quad_x + fill_offset_x - strlen(value_str) + (int)(style->background_texture->width * style->texture_scale) / 2 - strlen(value_str) * 5,
            quad_y + fill_offset_y + (int)style->font->height / 2 + (int)(style->background_texture->height * style->texture_scale) / 2,
            style->font, value_str, text_color, 1.0f);

        GUIWindow_drawTextLine(win, quad_x + fill_offset_x + ((int)(style->background_texture->width * style->texture_scale) / 2) + strlen(value_str),
            quad_y + fill_offset_y + (int)style->font->height / 2 + (int)(style->background_texture->height * style->texture_scale) / 2,
            style->font, max_value_str, text_color, 1.0f);
    }
}

void
GUI_beginWindow(const char *title,
    int x, int y, int w, int h)
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(sgui.began_update);

    uint32_t id = hashFromString(title);
    GUIWindow *win = findOrCreateGUIWindow(id);
    win->draw_commands_index = -1;
    win->vertex_buffer.num_vertices = 0;
    win->parent = gui->current_window;
    win->style = GUI_getCurrentWindowStyle();
    sgui.current_window = (int)win->index;
    gui->sorted_windows[gui->num_sorted_windows] = win->index;
    ++gui->num_sorted_windows;

    win->relative_position[0] = x;
    win->relative_position[1] = y;
    win->dimensions[0] = w;
    win->dimensions[1] = h;

    GUIWindow *parent = GUIWindow_getParent(win);

    if (parent)
    {
        win->true_position[0] = parent->true_position[0] + x;
        win->true_position[1] = parent->true_position[1] + y;
    }
    else
    {
        win->true_position[0] = x;
        win->true_position[1] = y;
    }

    if (!isRelativeMouseMode())
    {
        if (TEST_POINT_IN_RECT(engine.input.mouse_pos[0], engine.input.mouse_pos[1],
            win->true_position[0], win->true_position[1], w, h))
        {
            if (mouseButtonDownThisFrame(MOUSE_BUTTON_LEFT))
            {
                /* Remember when the window was made active for layring purposes */
                if (sgui.next_active_window == -1
                || sgui.windows[sgui.next_active_window].last_made_active <= win->last_made_active)
                {
                    gui->next_active_window = win->index;
                }
            }
            else
            {

            }

            tryMakeWindowHovered(win);
        }
    }

    /* Rendering */
    GUIWindowStyle *style = GUIWindow_getStyle(win);
    float *color_border;
    float *color_bg;

    if (id != gui->active_window)
    {
        color_border    = style->color_border_normal;
        color_bg        = style->color_background_normal;
    }
    else
    {
        color_border    = style->color_border_active;
        color_bg        = style->color_background_active;
    }

    /* Render borders */
    if (color_border[3] > 0.0f)
    {
        GUIWindow_drawColorQuad(win, win->true_position[0],
            win->true_position[1], w, h, color_border);
    }

    /* Render background */
    if (color_bg[3] > 0.0f)
    {
        GUIWindow_drawColorQuad(win,
            win->true_position[0] + style->border_width_left,
            win->true_position[1] + style->border_width_top,
            w - style->border_width_left - style->border_width_right,
            h - style->border_width_top - style->border_width_bottom,
            color_bg);
    }

    /* Render title */
    if (style->show_title)
    {
        int x, y;

        switch (style->title_origin)
        {
            case GUI_ORIGIN_TOP_LEFT:
            {
                x = win->true_position[0] + style->border_width_left + style->title_position[0];
                y = win->true_position[1] + style->title_position[1];
            }
                break;
            case GUI_ORIGIN_TOP_RIGHT:
            {
                int text_width = calculateGUITitleWidth(getDefaultSpriteFont(), title);
                x = win->true_position[0] + style->border_width_left + win->dimensions[0] - style->border_width_right - text_width;
                y = win->true_position[1] + style->title_position[1];
            }
                break;
            case GUI_ORIGIN_BOTTOM_LEFT:
            {
                x = win->true_position[0] + style->border_width_left + style->title_position[0];
                y = win->true_position[1] + win->dimensions[1] - (int)getDefaultSpriteFont()->height;
            }
                break;
            case GUI_ORIGIN_BOTTOM_RIGHT:
            {
                int text_width = calculateGUITitleWidth(getDefaultSpriteFont(), title);
                x = win->true_position[0] + style->border_width_left + win->dimensions[0] - style->border_width_right - text_width;
                y = win->true_position[1] + win->dimensions[1] - (int)getDefaultSpriteFont()->height;
            }
                break;
        }

        x += style->title_position[0];
        y += style->title_position[1];

        GUIWindow_drawTitleTextLine(win, x, y, getDefaultSpriteFont(),
            title, style->color_title, 1.0f);
    }
}

void
GUI_beginEmptyWindow(const char *title,
    int x, int y, int w, int h)
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(sgui.began_update);

    uint32_t id = hashFromString(title);
    GUIWindow *win = findOrCreateGUIWindow(id);
    win->draw_commands_index = -1;
    win->vertex_buffer.num_vertices = 0;
    win->parent = gui->current_window;
    sgui.current_window = (int)win->index;
    gui->sorted_windows[gui->num_sorted_windows] = win->index;
    ++gui->num_sorted_windows;

    win->relative_position[0] = x;
    win->relative_position[1] = y;
    win->dimensions[0] = w;
    win->dimensions[1] = h;

    GUIWindow *parent = GUIWindow_getParent(win);

    if (parent)
    {
        win->true_position[0] = parent->true_position[0] + x;
        win->true_position[1] = parent->true_position[1] + y;
    }
    else
    {
        win->true_position[0] = x;
        win->true_position[1] = y;
    }


    if (TEST_POINT_IN_RECT(engine.input.mouse_pos[0], engine.input.mouse_pos[1],
        win->true_position[0], win->true_position[1], w, h))
    {
        tryMakeWindowHovered(win);
    }
}

void
GUI_textLine(const char *text, int x, int y)
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(sgui.began_update && sgui.current_window != -1);

    GUIWindow *w            = &sgui.windows[sgui.current_window];
    GUIWindowStyle *style   = GUIWindow_getStyle(w);
    SpriteFont *font        = sgui.next_font ? sgui.next_font : GUIWindowStyle_getFont(style);

    int text_x, text_y;

    /* FIXME: the x and y passed as "given_x" and "given_y" here are wrong! */
    calculateGUITextLinePosition(&text_x, &text_y, font,
        text, sgui.next_origin,
        x + style->border_width_left,
        y + style->border_width_top,
        w->true_position[0], w->true_position[1],
        w->dimensions[0], w->dimensions[1]);

    GUIWindow_drawTextLine(w,
        text_x, text_y,
        font, text, F_COLOR_WHITE, gui->default_text_scale);

    sgui.next_font      = 0;
    sgui.next_origin    = sgui.default_origin;
}

void
GUI_text(const char *text, int x, int y)
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(sgui.began_update && sgui.current_window != -1);

    GUIWindow *w = &sgui.windows[sgui.current_window];
    GUIWindowStyle *style = GUIWindow_getStyle(w);

    SpriteFont *font = sgui.next_font ? sgui.next_font : GUIWindowStyle_getFont(style);

    GUIWindow_drawText(w,
        w->true_position[0] + style->border_width_left + x,
        w->true_position[1] + style->border_width_top  + y,
        w->dimensions[0] - style->border_width_left - style->border_width_right - x,
        w->dimensions[1] - style->border_width_top - style->border_width_bottom - y,
        font, text, F_COLOR_WHITE,
        1.0f);

    sgui.next_font = 0;
    sgui.next_origin = sgui.default_origin;
}

void
GUI_text_Scale(const char *text, int x, int y, float scale)
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(sgui.current_window != -1);

    GUIWindow *w = &sgui.windows[sgui.current_window];
    GUIWindowStyle *style = GUIWindow_getStyle(w);

    SpriteFont *font = sgui.next_font ? sgui.next_font : GUIWindowStyle_getFont(style);

    GUIWindow_drawText(w,
        w->true_position[0] + style->border_width_left + x,
        w->true_position[1] + style->border_width_top  + y,
        w->dimensions[0] - style->border_width_left - style->border_width_right - x,
        w->dimensions[1] - style->border_width_top - style->border_width_bottom - y,
        font, text, F_COLOR_WHITE,
        scale);

    sgui.next_font = 0;
    sgui.next_origin = sgui.default_origin;
}

void
GUI_texture(Texture *texture, int x, int y)
{
    if (KYS_IS_TEXT_MODE()) return;
    if (!texture) return;

    ASSERT(sgui.began_update && sgui.current_window != -1);
    GUIWindow *win = &sgui.windows[sgui.current_window];

    int quad_x, quad_y;

    calculateGUIQuadPositionByOrigin(&quad_x, &quad_y,
        x, y, (int)texture->width, (int)texture->height,
        win->true_position[0], win->true_position[1],
        win->dimensions[0], win->dimensions[1],
        sgui.next_origin);

    GUIWindow_drawTexQuad(win,
        texture, quad_x, quad_y,
        (int)texture->width, (int)texture->height,
        0, 0, (int)texture->width, (int)texture->height,
        F_COLOR_WHITE, 1.0f, 1.0f);

    sgui.next_origin = sgui.default_origin;
}

void
GUI_texture_Clip(Texture *texture, int x, int y,
    int cx, int cy, int cw, int ch)
{
    if (KYS_IS_TEXT_MODE()) return;
    if (!texture) return;

    ASSERT(sgui.began_update && sgui.current_window != -1);
    GUIWindow *win = &sgui.windows[sgui.current_window];

    int quad_x, quad_y;

    calculateGUIQuadPositionByOrigin(&quad_x, &quad_y,
        x, y, (int)texture->width, (int)texture->height,
        win->true_position[0], win->true_position[1],
        win->dimensions[0], win->dimensions[1],
        sgui.next_origin);

    GUIWindow_drawTexQuad(win,
        texture, quad_x, quad_y,
        (int)texture->width, (int)texture->height,
        cx, cy, cw, ch, F_COLOR_WHITE, 1.0f, 1.0f);

    sgui.next_origin = sgui.default_origin;
}

void
GUI_texture_Scale(Texture *texture, int x, int y, float sx, float sy)
{
    if (KYS_IS_TEXT_MODE()) return;
    if (!texture) return;

    ASSERT(sgui.began_update && sgui.current_window != -1);
    GUIWindow *win = &sgui.windows[sgui.current_window];

    int quad_x, quad_y;

    calculateGUIQuadPositionByOrigin(&quad_x, &quad_y,
        x, y, (int)texture->width, (int)texture->height,
        win->true_position[0], win->true_position[1],
        win->dimensions[0], win->dimensions[1],
        sgui.next_origin);

    GUIWindow_drawTexQuad(win,
        texture, quad_x, quad_y,
        (int)texture->width, (int)texture->height,
        0, 0, texture->width, texture->height, F_COLOR_WHITE, sx, sy);

    sgui.next_origin = sgui.default_origin;
}

void
GUI_texture_ClipScale(Texture *texture, int x, int y,
    int cx, int cy, int cw, int ch,
    float scale_x, float scale_y)
{
    if (KYS_IS_TEXT_MODE()) return;
    if (!texture) return;

    ASSERT(sgui.current_window != -1);
    GUIWindow *win = &sgui.windows[sgui.current_window];

    int quad_x, quad_y;

    calculateGUIQuadPositionByOrigin(&quad_x, &quad_y,
        x, y,
        (int)((float)texture->width  * scale_x),
        (int)((float)texture->height * scale_y),
        win->true_position[0], win->true_position[1],
        win->dimensions[0], win->dimensions[1],
        sgui.next_origin);

    GUIWindow_drawTexQuad(win,
        texture, quad_x, quad_y,
        (int)texture->width, (int)texture->height,
        cx, cy, cw, ch, F_COLOR_WHITE,
        scale_x, scale_y);

    sgui.next_origin = sgui.default_origin;
}

void
GUI_endWindow()
{
    if (KYS_IS_TEXT_MODE()) return;

    ASSERT(sgui.began_update && sgui.current_window != -1);
    GUI_setNextWindowStyle(0);
    sgui.current_window = sgui.windows[sgui.current_window].parent;
}

#define SET_BUTTON_TEX_PARMS(_bt) \
    _bt.texture = tex; \
 \
    if (!tex) return; \
 \
    if (clip) \
    { \
        for (int i = 0; i < 4; ++i) \
            _bt.clip[i] = clip[i]; \
    } \
    else \
    { \
        _bt.clip[0] = 0; \
        _bt.clip[1] = 0; \
        _bt.clip[2] = tex->width; \
        _bt.clip[3] = tex->height; \
    } \
 \
    _bt.offset[0] = offset_x; \
    _bt.offset[1] = offset_y; \
 \
    _bt.scale[0] = scale_x; \
    _bt.scale[1] = scale_y; \
 \
    gui->next_button_textures_changed = 1;

void
GUI_setNextNormalButtonTextureParams(Texture *tex, int *clip,
    int offset_x, int offset_y, float scale_x, float scale_y)
{
    SET_BUTTON_TEX_PARMS(gui->next_button_normal_texture);
}

void
GUI_setNextHoveredButtonTextureParams(Texture *tex, int *clip,
    int offset_x, int offset_y, float scale_x, float scale_y)
{
    SET_BUTTON_TEX_PARMS(gui->next_button_hovered_texture);
}

void
GUI_setNextPressedButtonTextureParams(Texture *tex, int *clip,
    int offset_x, int offset_y, float scale_x, float scale_y)
{
    SET_BUTTON_TEX_PARMS(gui->next_button_pressed_texture);
}

#undef SET_BUTTON_TEX_PARMS
