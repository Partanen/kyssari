#include "../../engine/src/core/engine.h"
#include <math.h>

static Server server = {0};
static Client client = {0};

static double spawn_timer = 0;
static bool32 timer_ticking = 0;

typedef struct ScreenData ScreenData;
typedef struct PlayerData PlayerData;
typedef struct BulletData BulletData;

#define TRIANGLE_A {5.0f,  0.0f, 0.0f}
#define TRIANGLE_B {0.0f,  5.0f, 0.0f}
#define TRIANGLE_C {-5.0f,  0.0f, 0.0f}

static vec3 ta = TRIANGLE_A;
static vec3 tb = TRIANGLE_B;
static vec3 tc = TRIANGLE_C;

static bool32 COLLIDING;
static int code;

static int test_shader_index;
static ServerGame *server_game = 0;
static bool32 simulating_scene = 0;
static Thread server_simulation_thread;

void
tryInitServer();

void
tryInitClient();

void
onClientAcceptedCallback(Server *s, ServerClient *sc);

void
onServerUpdateCallback(Server *server);

int
seekFileToPositionAfterWord(FILE *file, const char *word);

uint32_t
parseServerIPFromConnectDotConf();

void
updateAndRender(Screen *screen);

Entity *
createOwl(float x, float y, float z);

Entity *
createCube(float x, float y, float z, float scale);

Entity *
createBall(float x, float y, float z, float scale);

Entity *
createPlayer(float x, float y, float z);

Entity *
createBillboard(float x, float y, float z);

void
createEffect(Entity *entity, int amount, Model* model,
    Material *material, vec3 velocity, float scale, float lifetime);

void
bulletCallback(PhysicsComponent *component_a, PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b, enum ColliderType collider_a_type,
    enum ColliderType collider_b_type, enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id);

void
explosionCallback(PhysicsComponent *comp_a, PhysicsComponent *comp_b, Entity *ea,
    Entity *eb, enum ColliderType col_a_type, enum ColliderType col_b_type,
    enum CollisionEventType type, uint32_t a_id, uint32_t b_id);

void
testScript(Entity *e, void *data);

void
testiscript(Entity *entity, void *scriptdata);

void
trianglescript(Entity *entity, void *scriptdata);

void
beginServerSceneSimulation();

void
endServerSceneSimulation();

static thread_ret_t
simulateServerScene(void *);

void
replicatedEntityCreationCallback(Entity *e);

Plane
computePlane(vec3 a, vec3 b, vec3 c);

int
raycasttest(vec3 start, vec3 end, Plane *plane, float *t, vec3 q);

//int TestRaySphere(vec3 p, vec3 d, Sphere3D *s);

struct ScreenData
{
    AssetPool   assetpool;
    Scene3D     scene3d;
    Camera      camera;
    BaseMesh    *mesh1;
    Entity      *player;
    Entity      *entity2;
    Texture     *texture;
    Texture     *tex_grass;
    Texture     *tex_muzzlefire;
    Texture     *hp_border;
    Texture     *hp_fill;
    Material    mat_grass;
    Material    mat;
    Material    mat2;
    Entity      *entities[1024];
    Font        *font_munro;
    SpriteFont  *sfont_roboto;
    Texture     *tex_skybox;
    BaseMesh    *player_mesh;
    Model       *map_model;

    Entity      *cube_entity;
    Entity      *ground_cube;
    Entity      *physics_cube_a;
	Entity		*physics_cube_b;
    Entity      *raytest_cube;
    Entity      *billboard;
    Plane       plane;

    bool32      camera_follow_on;

    Entity      *light_entity;

    AudioSystem audiosystem;
    SoundEffect *soundeffect_test;
    Music       *music_test;

    Socket      socket;

    Model       *testmodel;

    Texture     *cursor;

    int         bar_value;
} data;

struct PlayerData
{
    int hp;
};

struct BulletData
{
    vec3    startPosition;
    vec3    forward;
    float   velocity;
    bool32  printed;
    float   timer;
    float   out;
};

bool32 active   = 1;

void
tryInitServer()
{
    if (kys_initNetworking() != 0)
    {
        DEBUG_PRINTF("Failed to init networking!\n");
        return;
    }

    int r = Server_init(&server, 15);

    if (r != 0 && r != SERVER_ERROR_ALREADY_INITIALIZED)
    {
        DEBUG_PRINTF("Failed to init server.\n");
        return;
    }

    if (r != SERVER_ERROR_ALREADY_INITIALIZED)
    {
        /* Set callbacks */
        server.on_update_callback = onServerUpdateCallback;
        server.on_accept_client_callback = onClientAcceptedCallback;
        server_game = Server_createGame(&server, &data.scene3d, 2);
    }

    if (!server_game)
    {
        DEBUG_PRINTF("Failed to create server game!\n");
        return;
    }

    if (Server_run(&server) != 0)
        DEBUG_PRINTF("Failed to start listening using server.\n");
    else
        DEBUG_PRINTF("Started server!\n");
}

void
tryInitClient()
{
    if (!client.initialized && !server.initialized)
    {
        uint32_t connection_address = parseServerIPFromConnectDotConf();

        Address server_addr;
        /*Address_init(&server_addr, 195, 148, 70, 121, KYS_SERVER_PORT);*/
        /*Address_init(&server_addr, 188, 238, 53, 128, KYS_SERVER_PORT);*/
        /*Address_init(&server_addr, 127, 0, 0, 1, KYS_SERVER_PORT);*/
        Address_initFromUint32(&server_addr, connection_address, KYS_SERVER_PORT);

        DEBUG_PRINTF("tryInitClient: parsed server address is: "
            "%u (%i.%i.%i.%i)\n",
            connection_address,
            (int)Address_getX(&server_addr),
            (int)Address_getY(&server_addr),
            (int)Address_getZ(&server_addr),
            (int)Address_getW(&server_addr));

        /*Address_init(&server_addr, 127, 0, 0, 1, KYS_SERVER_PORT);*/

        if (kys_initNetworking() != 0)
            DEBUG_PRINTF("Failed to init networking!\n");
        else if (Client_init(&client) != 0)
            DEBUG_PRINTF("Failed to init client.\n");
        else if (Client_setReplicatedScene(&client, &data.scene3d) != 0)
            DEBUG_PRINTF("Failed to set replicated scene for client\n");
        else if (Client_connect(&client, &server_addr) != 0)
            DEBUG_PRINTF("Failed to connect client.\n");
    }
}

/*static bool32 spawned_entity = 0;*/
static replication_id32_t id;

void
onClientAcceptedCallback(Server *s, ServerClient *sc)
{
    if (ServerGame_attachClient(server_game, sc) != 0)
    {
        DEBUG_PRINTF("FAILED TO ATTACH CLIENT TO GAME.\n");
    }

    Mutex_lock(&data.scene3d.entity_access_mutex);

    Entity *e = Scene3D_createEntity(&data.scene3d, 1.0f, 1.0f, 0.0f);
    if (e)
    {
        Entity_makeReplicated(e);

        replicatedEntityCreationCallback(e);

#if 0
        if (ServerGame_informPlayersOfEntity(server_game, e) != 0)
            DEBUG_PRINTF("Failed to inform players of entity!\n");
#endif

        id = e->replication_id;

        DEBUG_PRINTF("SPAWNED ENTITY\n");
    }

    if (ServerGame_informPlayerOfAllEntities(server_game, sc) != 0)
    {
        DEBUG_PRINTF("FAILED TO INFORM ALL PLAYERS OF ALL ENTITIES\n");
    }

    Mutex_unlock(&data.scene3d.entity_access_mutex);
}

void
onServerUpdateCallback(Server *server)
{
    Mutex_lock(&data.scene3d.entity_access_mutex);

    for (ReplicationTableEntry *rte =
            data.scene3d.replication_table.reserved_entries;
         rte;
         rte = rte->reserved_next)
    {
        if (rte->entity)
        {
            PhysicsComponent *p = Entity_getPhysicsComponent(rte->entity);

            if (p)
                p->velocity[0] = 0.5f;
        }
    }

    Mutex_unlock(&data.scene3d.entity_access_mutex);
}

int
seekFileToPositionAfterWord(FILE *file, const char *word)
{
    int len = strlen(word);
    if (len <= 0) return -1;

    rewind(file);

    char c;
    int num_correct = 0;

    while (num_correct != len)
    {
        c = getc(file);

        if (c == EOF)
            break;
        else if (c == word[num_correct])
            ++num_correct;
    }

    return num_correct == len ? 0 : 1;
}

uint32_t
parseServerIPFromConnectDotConf()
{
    FILE *file = fopen("connect.conf", "r");
    char hostname[21];
    hostname[20] = '\0';

    const char *localhost = "127.0.0.1";

    if (file)
    {
        char buf[128];
        int len;
        int res = seekFileToPositionAfterWord(file, "hostname = ");

        if (res == 0)
        {
            buf[0] = '\0';
            fgets(buf, 128, file);
            len = strlen(buf);

            if (len <= 0)
                strcpy(hostname, localhost);
            else
            {
                if (buf[len-1] == '\n') buf[len-1] = '\0';
                strcpy(hostname, buf);
            }
        }
        else
            strcpy(hostname, localhost);

        fclose(file);
    }
    else
        strcpy(hostname, localhost);

    uint32_t ret;

    struct sockaddr_in in_address = {0};
    int pres = inet_pton(AF_INET, hostname, &in_address.sin_addr);

    if (pres == 1)
        ret = (uint32_t)in_address.sin_addr.s_addr;
    else
        ret = 0;


    return ret;
}

void
updateAndRender(Screen *screen)
{
    ScreenData *data = (ScreenData*)screen->screen_data;

    static vec3 point_a;
    static vec3 point_b;

    #define CAMERA_SPEED 10.0f

    if (isKeyPressed(SDL_SCANCODE_F1))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
        {
            SphereCollider *sc = PhysicsComponent_getSphereCollider(
                Entity_getPhysicsComponent(data->physics_cube_a), "sphere");
            if (!sc)
                DEBUG_PRINTF("AAARGH SC VIRHE:\n");
            SET_BITFLAG_ON(sc->flags, char, COLLIDER_FLAG_IS_INACTIVE);
        }
        else
            data->scene3d.render_collision_octree = 1;
    }

    if (isKeyPressed(SDL_SCANCODE_F2))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
        {
            SphereCollider *sc = PhysicsComponent_getSphereCollider(
                Entity_getPhysicsComponent(data->physics_cube_a), "sphere");

            SET_BITFLAG_OFF(sc->flags, char, COLLIDER_FLAG_IS_INACTIVE);
        }
        else
            data->scene3d.render_collision_octree = 0;
    }

    if (isKeyPressed(SDL_SCANCODE_F3))
    {
        data->scene3d.render_collision_geometry = 1;
        data->scene3d.render_dynamic_colliders  = 1;
    }

    if (isKeyPressed(SDL_SCANCODE_F4))
    {
        data->scene3d.render_collision_geometry = 0;
        data->scene3d.render_dynamic_colliders  = 0;
    }

    if (isKeyPressed(SDL_SCANCODE_W))
    {
        Camera_moveForward(&data->camera, CAMERA_SPEED * (float)getDelta());
    }

    if (isKeyPressed(SDL_SCANCODE_S))
    {
        Camera_moveBack(&data->camera, CAMERA_SPEED * (float)getDelta());
    }

    if (isKeyPressed(SDL_SCANCODE_A))
    {
        if (!data->camera_follow_on)
            Camera_strafeLeft(&data->camera, CAMERA_SPEED * (float)getDelta());
    }

    if (isKeyPressed(SDL_SCANCODE_D))
    {
        if (!data->camera_follow_on)
            Camera_strafeRight(&data->camera, CAMERA_SPEED * (float)getDelta());
    }

    if (isKeyPressed(SDL_SCANCODE_M))
        setRelativeMouseMode(0);

    if (isKeyPressed(SDL_SCANCODE_N))
        setRelativeMouseMode(1);

    if (isKeyPressed(SDL_SCANCODE_1))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
        {
            if (Server_shutdown(&server) != 0)
                DEBUG_PRINTF("Failed to shutdown server.\n");
            else
                DEBUG_PRINTF("Server successfully shut down.");
        }
        else
        {
            tryInitServer();
        }
    }

    if (isKeyPressed(SDL_SCANCODE_2))
    {
        tryInitClient();
    }

    if (isKeyPressed(SDL_SCANCODE_3))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.pos[1] += 0.1f;
        DEBUG_PRINTF("transform.pos[1] = %f\n", mc_a->render_models->relative_transform.pos[1]);
    }
    if (isKeyPressed(SDL_SCANCODE_4))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.pos[1] -= 0.1f;
        DEBUG_PRINTF("transform.pos[1] = %f\n", mc_a->render_models->relative_transform.pos[1]);
    }

    if (isKeyPressed(SDL_SCANCODE_5))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.pos[2] += 0.1f;
        DEBUG_PRINTF("transform.pos[2] = %f\n", mc_a->render_models->relative_transform.pos[2]);
    }
    if (isKeyPressed(SDL_SCANCODE_6))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.pos[2] -= 0.1f;
        DEBUG_PRINTF("transform.pos[2] = %f\n", mc_a->render_models->relative_transform.pos[2]);
    }
    if (isKeyPressed(SDL_SCANCODE_7))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.rot[0] += 0.1f;
        DEBUG_PRINTF("transform.rot[0] = %f\n", mc_a->render_models->relative_transform.rot[0]);
    }
    if (isKeyPressed(SDL_SCANCODE_U))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.rot[0] -= 0.1f;
        DEBUG_PRINTF("transform.rot[0] = %f\n", mc_a->render_models->relative_transform.rot[0]);
    }
    if (isKeyPressed(SDL_SCANCODE_8))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.rot[1] += 0.1f;
        DEBUG_PRINTF("transform.rot[1] = %f\n", mc_a->render_models->relative_transform.rot[1]);
    }
    if (isKeyPressed(SDL_SCANCODE_I))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.rot[1] -= 0.1f;
        DEBUG_PRINTF("transform.rot[1] = %f\n", mc_a->render_models->relative_transform.rot[1]);
    }
    if (isKeyPressed(SDL_SCANCODE_9))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.rot[2] += 0.1f;
        DEBUG_PRINTF("transform.rot[2] = %f\n", mc_a->render_models->relative_transform.rot[2]);
    }
    if (isKeyPressed(SDL_SCANCODE_O))
    {
        ModelComponent *mc_a = Entity_getModelComponent(data->physics_cube_a);
        mc_a->render_models->relative_transform.rot[2] -= 0.1f;
        DEBUG_PRINTF("transform.rot[2] = %f\n", mc_a->render_models->relative_transform.rot[2]);
    }

    if (isKeyPressed(SDL_SCANCODE_UP))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
            ta[1] += (float)getDelta();
        else if (isKeyPressed(SDL_SCANCODE_LCTRL))
            tb[1] += (float)getDelta();
        else
            tc[1] += (float)getDelta();
    }

    if (isKeyPressed(SDL_SCANCODE_DOWN))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
            ta[1] -= (float)getDelta();
        else if (isKeyPressed(SDL_SCANCODE_LCTRL))
            tb[1] -= (float)getDelta();
        else
            tc[1] -= (float)getDelta();
    }

    if (isKeyPressed(SDL_SCANCODE_LEFT))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
            ta[0] -= (float)getDelta();
        else if (isKeyPressed(SDL_SCANCODE_LCTRL))
            tb[0] -= (float)getDelta();
        else
            tc[0] -= (float)getDelta();
    }

    if (isKeyPressed(SDL_SCANCODE_RIGHT))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
            ta[0] += (float)getDelta();
        else if (isKeyPressed(SDL_SCANCODE_LCTRL))
            tb[0] += (float)getDelta();
        else
            tc[0] += (float)getDelta();
    }

    if (isKeyPressed(SDL_SCANCODE_Y))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
            ta[2] -= (float)getDelta();
        else if (isKeyPressed(SDL_SCANCODE_LCTRL))
            tb[2] -= (float)getDelta();
        else
            tc[2] -= (float)getDelta();
    }

    if (isKeyPressed(SDL_SCANCODE_U))
    {
        if (isKeyPressed(SDL_SCANCODE_LSHIFT))
            ta[2] += (float)getDelta();
        else if (isKeyPressed(SDL_SCANCODE_LCTRL))
            tb[2] += (float)getDelta();
        else
            tc[2] += (float)getDelta();
    }

    static bool32 can_shoot = 1;
    static double shoot_timer;

    if (!can_shoot)
    {
        shoot_timer -= (float)getDelta();
        if (shoot_timer <= 0)
            can_shoot = 1;
    }

    if (getMouseButtonState(MOUSE_BUTTON_LEFT) && can_shoot)
    {
        vec3 forward;
        Entity_getForwardVector(data->physics_cube_a, forward);
        Transform *t    = &Entity_getTransform(data->physics_cube_a)->transform;
        //Entity *e       = createCube(t->pos[0] + forward[0] * 3.0f,
        //    t->pos[1] + forward[1] * 3.0f,
        //    t->pos[2] + forward[2] * 3.0f, 0.1f);
        //Entity_setRotation(e, t->rot[0], t->rot[1], t->rot[2]);
        Entity *e = createBall(t->pos[0] + forward[0] * 2, t->pos[1] + forward[1] * 2, t->pos[2] + forward[2] * 2, 0.1f);

        if (!e)
        {
            DEBUG_PRINTF("EI luotu entityja");
        }

        Entity_setName(e, "bullet");
        Entity_setLifetime(e, 5);

        PhysicsComponent *c = Entity_getPhysicsComponent(e);
        
        vec3_copy(c->velocity, forward);
        vec3_norm(c->velocity, c->velocity);
        vec3_scale(c->velocity, c->velocity, 10.0f);
        PhysicsComponent_setCollisionCallback(c, bulletCallback);
        c->friction = 0.0f;
        c->max_velocity = 3000.0f;
        
        PhysicsComponent_setStaticity(c, 1);
        can_shoot = 0;
        shoot_timer = 0.2f;
        float raylength = 20.0f;
        vec3 rayend;
        vec3 temppi;
        vec3 dummy;
        float out;
        vec3_scale(temppi, forward, raylength);
        vec3_add(rayend, Entity_getTransform(data->physics_cube_a)->transform.pos, temppi);
        //BoxCollider_makeSimpleTrigger(PhysicsComponent_getBoxCollider(c, "box"));
        if (Scene3D_castRayToStaticGeometry(data->physics_cube_a->scene, Entity_getTransform(data->physics_cube_a)->transform.pos, forward, raylength, dummy, &out))
        {
            DEBUG_PRINTF("ASDSADFASFSADFASFAFADFDAFSDAFSDAFSDAFSDAFASD FA \n");
            ScriptComponent *sc = Entity_getScriptComponent(e);
            ((BulletData*)sc->data)->velocity = 10.0f;
            ((BulletData*)sc->data)->printed = 0;
            //((BulletData*)sc->data)->timer = 0.0f;
            ((BulletData*)sc->data)->out = out;
            ScriptComponent_setCallback(sc, trianglescript);
        }
        vec3 random;
        random[0] = -2.0f + rand() % 4;
        random[1] = 3.0f;
        random[2] = -2.0f + rand() % 4;
        createEffect(data->physics_cube_a, 1, getDefaultPlaneModel(), AssetPool_getMaterial(&data->assetpool, "muzzlefire"), random, 0.4f, 2.0f);


    }

    if (isKeyPressed(SDL_SCANCODE_F))
    {
        if (active)
        {
            Scene3D_removeEntity(&data->scene3d, data->entity2);
            active = 0;
        }
    }

    if (isKeyPressed(SDL_SCANCODE_G))
    {
        if (!active)
        {
            data->entity2 = createOwl(-1.0f, 0.0f, 1.0f);
            active = 1;
        }
    }
   
    
    
    /*Entity_lookAt(data->physics_cube_b, data->camera.position);*/

    if (isKeyPressed(SDL_SCANCODE_LEFT))
    {
        Transform *t = &Entity_getTransform(data->physics_cube_b)->transform;
        Entity_setRotation(data->physics_cube_b,
            t->rot[0],
            t->rot[1] + (float)getDelta() * 2.0f,
            t->rot[2]);


    }
    if (isKeyPressed(SDL_SCANCODE_UP))
    {
        Transform *ray = &Entity_getTransform(data->raytest_cube)->transform;
        Entity_setRotation(data->raytest_cube, ray->rot[0], ray->rot[1] , ray->rot[2] + (float)getDelta() * 2.0f);
    }

    if (isKeyPressed(SDL_SCANCODE_RIGHT))
    {
        if (!isKeyPressed(SDL_SCANCODE_LSHIFT))
        {
            Transform *t = &Entity_getTransform(data->physics_cube_b)->transform;
            Entity_setRotation(data->physics_cube_b,
                t->rot[0],
                t->rot[1] - (float)getDelta() * 2.0f, t->rot[2]);
        }
        else
        {
        }
    }

    if (mouseMovedThisFrame() && isRelativeMouseMode())   //cuben py�ritys hiirell�??
    {
        Transform *t = &Entity_getTransform(data->physics_cube_a)->transform;

        float deg1 = (float)RAD_TO_DEG(t->rot[1]);
        float deg2 = (float)RAD_TO_DEG(t->rot[2]);

        deg1 = deg1 - (float)getMouseRelativeMovement()[0] * (float)getDelta();
        deg2 = deg2 + (float)getMouseRelativeMovement()[1] * (float)getDelta();

        if (deg1 > (float)RAD_TO_DEG(M_PI) * 2.0f)
            deg1 = deg1 - (float)RAD_TO_DEG(M_PI) * 2.0f;
        else if (deg1 < 0.0f)
            deg1 = (float)RAD_TO_DEG(M_PI) * 2.0f + deg1;

        deg2 = CLAMP(deg2, (float)RAD_TO_DEG(-((float)M_PI / 2.0f)) + 0.001f,
            (float)RAD_TO_DEG((float)M_PI / 2.0f) - 0.001f);
        Entity_setRotation(data->physics_cube_a, t->rot[0],
            (float)DEG_TO_RAD(deg1), (float)DEG_TO_RAD(deg2));
    }

    //cuben py�ritys kameraan p�in??
    //Transform *t = &Entity_getTransform(data->raytest_cube)->transform;
    //mat3x3 test;
    //mat3x3_identity(test);
    //vec3 direction;
    //vec3_sub(direction, t->pos, data->camera.position);
    //mat3x3_rotate(test, test, 1.0f, 0.0f, 0.0f, direction[0]);
    //mat3x3_rotate(test, test, 0.0f, 1.0f, 0.0f, direction[1]);
    //mat3x3_rotate(test, test, 0.0f, 0.0f, 1.0f, direction[2]);
    //vec3_copy(t->rot[0], test[0]);
    //vec3_copy(t->rot[1], test[1]);
    //vec3_copy(t->rot[2], test[2]);

    //player movement?
    if (data->camera_follow_on)
    {
        vec3 forward;

        Entity_getForwardVector(data->physics_cube_a, forward);
        PhysicsComponent *pc = Entity_getPhysicsComponent(data->physics_cube_a);

        vec3 strafe;
        vec3_mul_cross(strafe, forward, DEFAULT_UP_VECTOR);
        //vec3_norm(strafe, strafe);

        if (isKeyPressed(SDL_SCANCODE_W))
        {
            pc->velocity[0] = forward[0] * 10.0f;
            pc->velocity[2] = forward[2] * 10.0f;
        }

        if (isKeyPressed(SDL_SCANCODE_S))
        {
            pc->velocity[0] = -forward[0] * 10.0f;
            pc->velocity[2] = -forward[2] * 10.0f;
        }

        if (isKeyPressed(SDL_SCANCODE_A))
        {
            pc->velocity[0] = -strafe[0] * 10.0f;
            pc->velocity[2] = -strafe[2] * 10.0f;
        }

        if (isKeyPressed(SDL_SCANCODE_D))
        {
            pc->velocity[0] = strafe[0] * 10.0f;
            pc->velocity[2] = strafe[2] * 10.0f;
        }

        if (isKeyPressed(SDL_SCANCODE_A) && isKeyPressed(SDL_SCANCODE_W))
        {
            pc->velocity[0] = (-strafe[0] + forward[0]) * 10.0f;
            pc->velocity[2] = (-strafe[2] + forward[2]) * 10.0f;
        }

        if (isKeyPressed(SDL_SCANCODE_D) && isKeyPressed(SDL_SCANCODE_W))
        {
            pc->velocity[0] = (strafe[0] + forward[0]) * 10.0f;
            pc->velocity[2] = (strafe[2] + forward[2]) * 10.0f;
        }

        if (isKeyPressed(SDL_SCANCODE_A) && isKeyPressed(SDL_SCANCODE_S))
        {
            pc->velocity[0] = (-strafe[0] + -forward[0]) * 10.0f;
            pc->velocity[2] = (-strafe[2] + -forward[2]) * 10.0f;
        }

        if (isKeyPressed(SDL_SCANCODE_D) && isKeyPressed(SDL_SCANCODE_S))
        {
            pc->velocity[0] = (strafe[0] + -forward[0]) * 10.0f;
            pc->velocity[2] = (strafe[2] + -forward[2]) * 10.0f;
        }

        if (!isKeyPressed(SDL_SCANCODE_W) && !isKeyPressed(SDL_SCANCODE_S) && !isKeyPressed(SDL_SCANCODE_A) && !isKeyPressed(SDL_SCANCODE_D))
        {
            pc->acceleration[0] = 0;
            pc->acceleration[2] = 0;
        }

        if (isKeyPressed(SDL_SCANCODE_SPACE))
        {
            pc->velocity[1] = 20.0f;
        }
    }

    if (isKeyPressed(SDL_SCANCODE_UP))
    {
        if (!isKeyPressed(SDL_SCANCODE_LSHIFT))
        {
            vec3 forward;

            Entity_getForwardVector(data->physics_cube_b, forward);
            PhysicsComponent *pc = Entity_getPhysicsComponent(data->physics_cube_b);

            pc->acceleration[0] = forward[0] * 5;
            pc->acceleration[1] = forward[1] * 5;
            pc->acceleration[2] = forward[2] * 5;

            pc->gravity_active = 1;
        }
        else
        {
            /*Transform *t = &Entity_getTransform(data->physics_cube_a)->transform;
            Entity_setRotation(data->physics_ball_a,
                t->rot[0], t->rot[1], t->rot[2] + (float)getDelta() * 2.0f);*/
        }
    }
    else
    {
    }

	if (isKeyPressed(SDL_SCANCODE_DOWN))
	{
        if (!isKeyPressed(SDL_SCANCODE_LSHIFT))
        {
            vec3 forward;

            Entity_getForwardVector(data->physics_cube_b, forward);
            PhysicsComponent *pc = Entity_getPhysicsComponent(data->physics_cube_b);
            pc->velocity[0] = forward[0];
            pc->velocity[1] = forward[1];
            pc->velocity[2] = forward[2];
        }
        else
        {
            Transform *t = &Entity_getTransform(data->physics_cube_a)->transform;
            Entity_setRotation(data->physics_cube_a,
                t->rot[0], t->rot[1], t->rot[2] - (float)getDelta() * 2.0f);
        }
	}

    if (isKeyPressed(SDL_SCANCODE_SPACE))
    {
        data->bar_value -= 1;
        if (data->bar_value <= 0)
            data->bar_value = 100;

        if (!isKeyPressed(SDL_SCANCODE_LSHIFT))
            Camera_moveUp(&data->camera, CAMERA_SPEED * (float)getDelta());
        else
            Camera_moveDown(&data->camera, CAMERA_SPEED * (float)getDelta());
    }

    if (isKeyPressed(SDL_SCANCODE_K))
        data->camera_follow_on = 1;

    if (isKeyPressed(SDL_SCANCODE_J))
        data->camera_follow_on = 0;

    if (!data->camera_follow_on && mouseMovedThisFrame() && isRelativeMouseMode())
    {
        Camera_turn(&data->camera,
             (float)getMouseRelativeMovement()[0] * (float)getDelta(),
            -(float)getMouseRelativeMovement()[1] * (float)getDelta(),
            0.05f);
    }

    /* GUI */
    {
        GUI_beginUpdate();


        GUI_progressBar(data->bar_value, 100, 300, 200, 104, 24,
            GUI_FILLDIRECTION_RIGHT, F_COLOR_BLACK);

        if (GUI_button("ebin button", 500, 500, 48, 48))
            DEBUG_PRINTF("pressed!\n");

        char buf[128];
        sprintf(buf, "%f", engine.clock.fps);

        GUI_beginWindow("window##win1", 50, 150, 128, 128);
            if (GUI_button("button", 5, 5, 40, 40))
                DEBUG_PRINTF("pressed button 'button'\n");
            GUI_textLine("ebin :DDDDDDDDDDDDDDDDDDDDD", 50, 50);
            GUI_beginWindow("window333##win2", 129, 30, 64, 64);
            GUI_endWindow();
        GUI_endWindow();

        GUI_beginWindow("window##win3", 75, 175, 128, 128);
            GUI_text(buf, 2, 2);
            GUI_texture_ClipScale(data->texture, 10, 25, 0, 0, 48, 48,
                0.5f, 0.5f);
            {
                Transform *t = &Entity_getTransform(data->physics_cube_a)->transform;
                sprintf(buf, "rot: %f, %f, %f\n", t->rot[0], t->rot[1], t->rot[2]);
                GUI_text(buf, 2, 96);
            }
        GUI_endWindow();

        if (COLLIDING)
        {
            GUI_textLine("Colliding!", 5, getWindowHeight() - 20 - 5);
        }

        sprintf(buf, "%i", code);
        GUI_textLine(buf, 5, getWindowHeight() - 20 - 5 - 20);

        GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_RIGHT);
        if (server.running)
        {
            sprintf(buf, "server running, num clients: %u\n",
                server.num_active_clients);
            GUI_textLine(buf, 5, 5);
        }
        else
            GUI_textLine("server down", 5, 5);

        {
            const char *client_text;

            switch (client.status)
            {
                case CLIENT_STATUS_DISCONNECTED:
                    client_text = "disconnected";
                    break;
                case CLIENT_STATUS_CONNECTING:
                    client_text = "connecting";
                    break;
                case CLIENT_STATUS_CONNECTED:
                    client_text = "connected";
                    break;
            }

            GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_LEFT);
            GUI_textLine("client status:", 5, 34);
            GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_LEFT);
            GUI_textLine(client_text, 5, 25);
        }

        GUI_endUpdate();
    }

    if (server.initialized)
        Scene3D_updateServerSimulation(&data->scene3d, &server, getDelta());
    else if (client.initialized)
        Scene3D_updateClientSimulation(&data->scene3d, &client, getDelta());
    else
        Scene3D_updateSimulation(&data->scene3d, getDelta());

    /* Triangle collision test */
    {
        SphereCollider *s = Entity_getPhysicsComponent(data->physics_cube_a)->sphere_colliders;

        vec3 n = {0.5f, 0.5f, 0.5f};
        vec3 pt;
        COLLIDING = testSphere3DTriangle(&s->data, ta, tb, tc, pt);
    }

    /* Camera follow */
    if (data->camera_follow_on)
    {
        Transform *t = &Entity_getTransform(data->physics_cube_a)->transform;

        vec3 forward;
        Entity_getForwardVector(data->physics_cube_a, forward);

        data->camera.position[0] = t->pos[0];
        data->camera.position[1] = t->pos[1];
        data->camera.position[2] = t->pos[2];

        vec3 temp;

        vec3_add(temp, data->camera.position, forward);
        Camera_lookAt(&data->camera, temp);

        vec3_copy(data->scene3d.camera_position, t->rot);
    }
    /* Render */
    if (!KYS_IS_TEXT_MODE())
    {
        /*Viewport v = {0, 0, , 600};*/
        renderScene3D(&data->scene3d, &data->camera, 0);

        renderText("PRESS K TO ALLOW CAMERA FOLLOW, J TO DISABLE",
            getDefaultSpriteFont(), 2, 2, 0);

        renderTexture(data->texture, 0, 0);
        renderTexture(data->cursor, ((float)getWindowWidth() / 2) - 4.0f, ((float)getWindowHeight() / 2) - 4.0f);

        imr_identity();
        imr_perspective(data->camera.fov,
            (float)getWindowWidth() / (float)getWindowHeight(),
            data->camera.near_clip, data->camera.far_clip);
        imr_lookAtCamera(&data->camera);
        imr_updateMatrix();

        imr_lineWidth(2.5f);
        imr_color(0.2f, 0.2f, 0.9f, 1.0f);

        Renderer_renderStaticBoxCollider(&engine.renderer, 5.0f, 5.0f, 5.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
        imr_begin(GL_LINE_STRIP);
            imr_vertex3f(ta[0], ta[1], ta[2]);
            imr_vertex3f(tb[0], tb[1], tb[2]);
            imr_vertex3f(tc[0], tc[1], tc[2]);
            imr_vertex3f(ta[0], ta[1], ta[2]);
        imr_end();

        imr_begin(GL_LINES);
            imr_vertex3f(point_a[0], point_a[1], point_a[2]);
            imr_vertex3f(point_b[0], point_b[1], point_b[2]);
        imr_end();
    }
}

void
updateAndRenderTextMode(Screen *screen)
{
#if _CF_ALLOW_TEXT_MODE
    clear();

    printw("Kyssari playground text control mode\n\n"
    "Commands:\n"
    "r: begin simulation\n"
    "s: start server\n"
    "q: quit");

    const char *str_true    = "true";
    const char *str_false   = "false";

    printw(
        "\n\nScene simulation is: %s\n"
        "Server on: %s\n"
        "Server has clients: %s\n"
        "Bytes received: %lu\n",
        simulating_scene ? str_true : str_false,
        server.running ? str_true : str_false,
        server.active_clients ? str_true : str_false,
        server.num_bytes_received);

    int ci = 0;
    for (ServerClient *c = server.active_clients; c; c = c->next)
    {
        printw("Last packet received from client %i.%i.%i.%i: %u\n",
            Address_getX(&c->connection.address),
            Address_getY(&c->connection.address),
            Address_getZ(&c->connection.address),
            Address_getW(&c->connection.address),
            (uint)server.active_clients->connection.last_received_packet);
    }

    refresh();
    int ch = getch();

    switch (ch)
    {
        case 'q':
        {
            endServerSceneSimulation();
            kys_beginShutdown();
        }
            break;
        case 'r': /* Begin simulation */
            beginServerSceneSimulation();
            break;
        case 's': /* Start server */
            tryInitServer();
            break;
    }
#endif
}

void
bulletCallback(PhysicsComponent *component_a, PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b,
    enum ColliderType collider_a_type,
    enum ColliderType collider_b_type,
    enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id)
{
    //if (type == COLLISION_ENTER)
    //    DEBUG_PRINTF("LAALA\n");
    //else if (type == COLLISION_END)
    //    DEBUG_PRINTF("ALAAL\n");
    //return;
    if (type == COLLISION_ENTER)
    {
        vec3 pos1;
        vec3_copy(pos1, Entity_getTransform(entity_a)->transform.pos);
        //vec3 pos2;
        //vec3_copy(pos2, Entity_getTransform(entity_b)->transform.pos);
        //vec3 direction;
        //vec3_sub(direction, pos2, pos1);
        //PhysicsComponent_applyImpactForce(component_b, direction, 50.0f);
        
        Scene3D_removeEntity(entity_a->scene, entity_a);
        Entity *e = createBall(pos1[0], pos1[1], pos1[2], 0.1f);
        PhysicsComponent *pc = Entity_getPhysicsComponent(e);
        /*ScriptComponent *sc = Entity_attachScriptComponent(e);*/
        ScriptComponent *sc = Entity_getScriptComponent(e);
        ScriptComponent_setCallback(sc, testiscript);
        Entity_setLifetime(e, 0.5f);
        PhysicsComponent_setCollisionCallback(pc, explosionCallback);

        //SphereCollider *sc = PhysicsComponent_getSphereCollider(pc, "bullet");
        //sc->radius = 10.0f;
        //Scene3D_removeEntity(entity_b->scene, entity_b);
    }
}

void
explosionCallback(PhysicsComponent *comp_a, PhysicsComponent *comp_b, Entity *ea,
    Entity *eb, enum ColliderType col_a_type, enum ColliderType col_b_type,
    enum CollisionEventType type, uint32_t a_id, uint32_t b_id)
{
    if (type == COLLISION_ENTER)
    {
        vec3 pos1;
        vec3_copy(pos1, Entity_getTransform(ea)->transform.pos);
        vec3 pos2;
        vec3_copy(pos2, Entity_getTransform(eb)->transform.pos);
        vec3 direction;
        vec3_sub(direction, pos2, pos1);
        PhysicsComponent_applyImpactForce(comp_b, direction, 50.0f);
    }
}

void
testiscript(Entity *entity, void *scriptdata)
{
    //float *timer = (float*)scriptdata;
    BulletData *bd = (BulletData*)scriptdata;
    double delta = getDelta();
    float maxsize = 5;
    bd->timer += (float)delta * 10.0f;
    PhysicsComponent *pc = Entity_getPhysicsComponent(entity);
    SphereCollider *sphere = PhysicsComponent_getSphereCollider(pc, "bullet");
    if (bd->timer > maxsize)
        bd->timer = maxsize;
    Entity_setScale(entity, bd->timer, bd->timer, bd->timer);
}

void
trianglescript(Entity *entity, void *scriptdata)
{
    BulletData *bd = (BulletData*)scriptdata;
    double delta = getDelta();
    float time;
    float timer;
    float lenght = 20.0f;
    vec3 pos;
    vec3 dummyvec;

    //vec3 forward;
    //Entity_getForwardVector(data.physics_cube_a, forward);
    PhysicsComponent *pc = Entity_getPhysicsComponent(entity);


        time = (bd->out - 2.0f) / bd->velocity;
        bd->timer += (float)delta;
        if (bd->timer >= time)
        {
            Scene3D_removeEntity(entity->scene, entity);
            vec3_copy(pos, Entity_getTransform(entity)->transform.pos);
            DEBUG_PRINTF("RAJAHDYKSEN POSITIO: %f. %f. %f \n", pos[0], pos[1], pos[2]);
            Entity *e = createBall(pos[0], pos[1], pos[2], 0.1f);

            ScriptComponent *sc = Entity_getScriptComponent(e);
            ScriptComponent_setCallback(sc, testiscript);
            Entity_setLifetime(e, 0.5f);
            //PhysicsComponent_setCollisionCallback(pc, explosionCallback);
        }
    //}
}

void
beginServerSceneSimulation()
{
    if (simulating_scene) return;
    simulating_scene = 1;
    Thread_create(&server_simulation_thread, simulateServerScene, 0);
}

void
endServerSceneSimulation()
{
    if (!simulating_scene) return;
    simulating_scene = 0;
    Thread_join(&server_simulation_thread);
}

static thread_ret_t
simulateServerScene(void *args)
{
    Clock local_clock;
    Clock_init(&local_clock, 60);

    while (simulating_scene)
    {
        Clock_tick(&local_clock);
        Scene3D_updateServerSimulation(&data.scene3d, &server,
            local_clock.delta);
    }

    return 0;
}

Entity *
createOwl(float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(&data.scene3d, x, y, z);
    Entity_setRotation(e, 0, 0, 0);
    Entity_attachPhysicsComponent(e);

    return e;
}

Entity *
createBillboard(float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(&data.scene3d, x, y, z);
    Entity_setRotation(e, 0, 0, 0);

    {
        ModelComponent *m = Entity_attachModelComponent(e);
        m->visible = 1;
        RenderModel *rm = ModelComponent_attachRenderModel(m, &data.scene3d, 1);
        rm->model = getDefaultPlaneModel();
        rm->relative_transform.pos[0] = 1.0f;
        rm->relative_transform.pos[1] = 1.0f;
        rm->relative_transform.pos[2] = 1.0f;
        rm->material = AssetPool_getMaterial(&data.assetpool, "muzzlefire");
        rm->shader = SHADER3D_BILLBOARD;
        rm->mode = RENDER_MODEL_BILLBOARD;
    }

    return e;
}

void
replicatedEntityCreationCallback(Entity *e)
{
    ModelComponent *mc = Entity_attachModelComponent(e);

    if (mc)
    {
        RenderModel *rm = ModelComponent_getDefaultRenderModel(mc);
        rm->model       = getDefaultBallModel();
    }

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);

    if (!pc)
    {
        DEBUG_PRINTF("replicatedEntityCreationCallback(): failed to attack "
            "physics component!\n");
    }

    DEBUG_PRINTF("replicateEntityCreationCallback(): done!\n");
}

Entity *
createCube(float x, float y, float z, float scale)
{
    Entity *e = Scene3D_createEntity(&data.scene3d, x, y, z);
    Transform *t = &Entity_getTransform(e)->transform;

    t->scale[0] = scale;
    t->scale[1] = scale;
    t->scale[2] = scale;

    {
        PhysicsComponent *collider = Entity_attachPhysicsComponent(e);
        BoxCollider *box = PhysicsComponent_attachBoxCollider(collider, e, "box");
        /*SET_BITFLAG_ON(box->flags, char, COLLIDER_FLAG_IGNORE_STATIC_GEOMETRY);*/
    }

    {
        ModelComponent *m = Entity_attachModelComponent(e);
        m->visible = 1;
    }

    Entity_setName(e, "cube\n");

    return e;
}

Entity *
createBall(float x, float y, float z, float scale)
{
    Entity *e = Scene3D_createEntity(&data.scene3d, x, y, z);
    Transform *t = &Entity_getTransform(e)->transform;
    t->scale[0] = scale;
    t->scale[1] = scale;
    t->scale[2] = scale;

    {
        PhysicsComponent *collider = Entity_attachPhysicsComponent(e);
        //SphereCollider *sphere = PhysicsComponent_attachSphereCollider(collider, e, "asd");
        collider->gravity_active = 0;
        //SphereCollider_makeSimpleTrigger(PhysicsComponent_getSphereCollider(collider, "asd"));
        //SET_BITFLAG_ON(sphere->flags, char, COLLIDER_FLAG_IGNORE_STATIC_GEOMETRY);
    }
    {
        ModelComponent *m = Entity_attachModelComponent(e);
        RenderModel *rm = ModelComponent_getDefaultRenderModel(m);
        rm->model = getDefaultBallModel();
        rm->material = AssetPool_getMaterial(&data.assetpool, "red material");
        rm->shader = SHADER3D_OPAQUE_OPAQUE;
        vec3_set(rm->relative_transform.scale, 0.5f, 0.5f, 0.5f);
        //ModelComponent_setMesh(m, getDefaultBallMesh());
        //m->visible = 1;
    }
    Entity_setName(e, "bullet\n");
    {
        ScriptComponent *sc = Entity_attachScriptComponent(e);
        ScriptComponent_allocateData(sc, sizeof(BulletData));
        //vec3_copy(((BulletData*)sc->data)->startPosition, Entity_getTransform(data.physics_cube_a)->transform.pos);
        //Entity_getForwardVector(data.physics_cube_a, ((BulletData*)sc->data)->forward);
        //((BulletData*)sc->data)->velocity = 10.0f;
        //((BulletData*)sc->data)->printed = 0;
        //((BulletData*)sc->data)->timer = 0.0f;
        //ScriptComponent_setCallback(sc, trianglescript);
    }
    return e;

}

Entity *
createPlayer(float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(&data.scene3d, x, y, z);

    PointLightComponent *l = Entity_attachPointLightComponent(e);
    PointLightComponent_setDiffuse(l, 0.8f, 0.1f, 0.4f);
    PointLightComponent_setSpecular(l, 0.4f, 0.1f, 0.4f);
    l->radius = 3.0f;

    ModelComponent *m = Entity_attachModelComponent(e);
    m->visible = 1;

    return e;
}

void
createEffect(Entity *entity, int amount, Model* model,
    Material *material, vec3 velocity, float scale, float lifetime)
{
    EntityTransform *et = Entity_getTransform(entity);

    for (int i = 0; i < amount; ++i)
    {
        Entity *e = Scene3D_createEntity(&data.scene3d, et->transform.pos[0], et->transform.pos[1], et->transform.pos[2]);

        ModelComponent *m = Entity_attachModelComponent(e);
        RenderModel *rm = ModelComponent_attachRenderModel(m, &data.scene3d, 1);
        rm->model = model;
        rm->material = material;
        vec3_set(rm->relative_transform.scale, scale, scale, scale);
        rm->shader = SHADER3D_BILLBOARD;
        //rm->mode = RENDER_MODEL_BILLBOARD;
        
        PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
        pc->gravity_active = 1;
        vec3_copy(pc->velocity, velocity);
        pc->friction = 0.5f;

        Entity_setLifetime(e, lifetime);
    }
}

Entity *
createDirectionalLight(float x, float y, float z, float dir_x, float dir_y, float dir_z)
{
    Entity *e = Scene3D_createEntity(&data.scene3d, x, y, z);

    DirectionalLightComponent *l = Entity_attachDirectionalLightComponent(e);

    DirectionalLightComponent_setDiffuse(l, 0.8f, 0.8f, 0.8f);
    DirectionalLightComponent_setSpecular(l, 0.5f, 0.5f, 0.5f);
    DirectionalLightComponent_setDirection(l, dir_x, dir_y, dir_z, 1);
    l->radius = 50.0f;

    ModelComponent *m = Entity_attachModelComponent(e);
    m->visible = 1;

    EntityTransform *et = Entity_getTransform(e);
    et->transform.scale[0] = 1.0f;
    et->transform.scale[1] = 1.0f;
    et->transform.scale[2] = 1.0f;

    return e;
}

Entity *
createPhysicsBall(float x, float y, float z, float scale)
{
    Entity *e = Scene3D_createEntity(&data.scene3d, x, y, z);

    ModelComponent *m = Entity_attachModelComponent(e);
    RenderModel *rm = ModelComponent_getDefaultRenderModel(m);
    rm->model = getDefaultBallModel();
    rm->material = AssetPool_getMaterial(&data.assetpool, "red material");
    rm->shader = SHADER3D_OPAQUE_OPAQUE;
    vec3_set(rm->relative_transform.scale, 0.5f, 0.5f, 0.5f);

    PhysicsComponent *p = Entity_attachPhysicsComponent(e);
    SphereCollider *sc = PhysicsComponent_attachSphereCollider(p, e, 0);
    SET_BITFLAG_ON(sc->flags, char, COLLIDER_FLAG_IGNORE_STATIC_GEOMETRY);

    Entity_setPosition(e, x, y, z);
    Entity_setScale(e, scale, scale, scale);

    return e;
}

int main(int argc, char **argv)
{
    {
        int error = kys_init(argc, argv, "Playground", MEGABYTES(32),
            MEGABYTES(64), INIT_FLAG_RESIZABLE_WINDOW, 1024, 768);

        if (error != 0)
        {
            return error;
        }
    }
    char *vertexcode = readTextFileToBuffer("shaders/testi.vert");
    char *fragcode = readTextFileToBuffer("shaders/testi.frag");
    test_shader_index = Renderer_createShader3D(&engine.renderer,
        SHADER3D_TYPE_OPAQUE_OUTLINE, vertexcode, fragcode);
    kfree(vertexcode);
    kfree(fragcode);

    /*ASSETPOOL INIT AND USAGE*/
    AssetPool_init(&data.assetpool, 4, 4, 8, 4, 4, 4, 4, 4, MEGABYTES(8));

    AssetPool_loadTexture(&data.assetpool, "matlock", "assets/matlock.png");
    AssetPool_loadTexture(&data.assetpool, "muzzlefire", "assets/textures/muzzlefire.png");
    //AssetPool_loadTexture(&data.assetpool, "border", "assets/textures/hp_border.png");
    //AssetPool_loadTexture(&data.assetpool, "fill", "assets/textures/hp_fill.png");
    data.texture = AssetPool_getTexture(&data.assetpool, "matlock");
    data.tex_muzzlefire = AssetPool_getTexture(&data.assetpool, "muzzlefire");
    data.cursor = loadTexture("assets/textures/cursor.png");


    //AssetPool_loadBaseMesh(&data.assetpool, "owldude", "assets/rocket_launcher.obj", &engine.assets);
    data.mesh1 = //AssetPool_getBaseMesh(&data.assetpool, "owldude");

    Assets_loadBaseMesh(&engine.assets, "assets/railgun.obj");

    data.testmodel = Assets_loadModel(&engine.assets, "assets/railgun.obj");

    AssetPool_loadMaterial(&data.assetpool, "blue material", 0, 0, 0.5f,
        0.2f, 0.2f, 0.8f);

    AssetPool_loadMaterial(&data.assetpool, "muzzlefire",
        data.tex_muzzlefire, data.tex_muzzlefire, 0.0f, 0.0f, 0.0f, 0.0f);

    AssetPool_loadMaterial(&data.assetpool, "red material", 0, 0, 0.5f,
        0.8f, 0.1f, 0.1f);

    Material *muh_mats[3] =
    {
        AssetPool_getMaterial(&data.assetpool, "blue material"),
        AssetPool_getMaterial(&data.assetpool, "muzzelfier"),
        AssetPool_getMaterial(&data.assetpool, "red material")
    };

    AssetPool_loadMaterialArray(&data.assetpool, "jee", muh_mats, 3);

    {
        Texture *t = AssetPool_loadTexture(&data.assetpool, "rocket launcher",
            "assets/textures/rocket_launcher_texture.png");
        AssetPool_loadMaterial(&data.assetpool, "rocket launcher", t, t,
            0.5f, 0.5f, 0.5f, 0.5f);
    }

    data.player_mesh = loadBaseMesh("assets/player_model.obj");

    data.bar_value = 100;

    setRelativeMouseMode(1);
    Screen *screen = createScreen(updateAndRender, &data);
    screen->updateAndRenderTextMode = updateAndRenderTextMode;

    /*AUDIO*/
    /*AssetPool_loadSoundEffect(&data.assetpool, "snd_test", "assets/soundeffects/HealSpace.wav");
    data.soundeffect_test = AssetPool_getSoundEffect(&data.assetpool, "snd_test");
    SoundEffect_play(data.soundeffect_test, 0);

    data.music_test = Assets_loadMusic(&engine.assets, "assets/music/BossSpaceIntro.wav");
    AssetPool_loadMusic(&data.assetpool, "music_test", "assets/music/BossSpaceIntro.wav");
    data.music_test = AssetPool_getMusic(&data.assetpool, "music_test");
    Music_play(data.music_test, -1); */

    /* Custom init stuff goes here */
    Camera_init(&data.camera);

    setRenderBackgroundColor(0.0f, 0.0f, 0.0f, 1.0f);

    /* Scene stuff */
    void *scene_memory = allocatePermanentMemory(MEGABYTES(16));

    if (!scene_memory || Scene3D_init(&data.scene3d, scene_memory,
        MEGABYTES(16), 2048, 1) != 0)
    {
        DEBUG_PRINTF("Scene_init() failure.\n");
        return 1;
    }
    else
    {
        data.scene3d.on_replicated_entity_creation_callback =
            replicatedEntityCreationCallback;
    }

    /* Static colliders */
    {
#if 0
        StaticBoxCollider *box = Scene3D_createStaticBoxCollider(&data.scene3d,
            3.0f, 0.0f, 2.0f);
        StaticBoxCollider_setModel(box, &data.scene3d, data.testmodel);
        StaticBoxCollider_setMaterial(box, &data.scene3d,
            AssetPool_getMaterial(&data.assetpool, "blue material"));
#endif
    }


    /*Viewport vp = {500, 200, 800, 600};
    Scene3D_setRenderViewport(&data.scene3d, vp);*/
    data.scene3d.physics_limits.gravity_direction[0] = 0.0f;
    data.scene3d.physics_limits.gravity_direction[1] = -1.0f;
    data.scene3d.physics_limits.gravity_direction[2] = 0.0f;
    data.scene3d.physics_limits.gravity_strength = 20.0f;
    data.scene3d.physics_limits.max_y_value = 150.0f;
    data.scene3d.physics_limits.min_y_value = 0.0f;

    data.font_munro = loadFont("assets/fonts/munro/munro.ttf", 10);

    AssetPool_loadSpriteFont(&data.assetpool, "roboto", "assets/fonts/Roboto-Regular.ttf", 10);
    data.sfont_roboto = AssetPool_getSpriteFont(&data.assetpool, "roboto");

    data.tex_grass = loadTexture("assets/grass.png");

    data.billboard = createBillboard(2.0f, 2.0f, 2.0f);

    data.player = createOwl(0.0f, 5.0f, 1.0f);
    data.entity2 = createOwl(-1.0f, 5.0f, 1.0f);

    {
#if 0
        data.cube_entity = createCube(0.0f, 5.0f, 0.0f, 2.0f);

        ModelComponent_setShader(Entity_getModelComponent(data.cube_entity),
            SHADER3D_OPAQUE_GOURAUD);

        PointLightComponent *l = Entity_attachPointLightComponent(data.cube_entity);
        PointLightComponent_setDiffuse(l, 0.4f, 0.1f, 0.8f);
        PointLightComponent_setSpecular(l, 0.4f, 0.1f, 0.8f);
        PointLightComponent_setAmbient(l, 0.6f, 0.6f, 0.6f);

#endif
    }

    /* This is the "player" character right now */
    {
        data.physics_cube_a = createCube(1.0f, 6.0f, -2.0f, 1.0f);
        PhysicsComponent *pc_a = Entity_getPhysicsComponent(data.physics_cube_a);
        ModelComponent *mc_a = Entity_getModelComponent(data.physics_cube_a);
        RenderModel *rm = ModelComponent_getDefaultRenderModel(mc_a);
        rm->model = data.testmodel;
        rm->material = AssetPool_getMaterial(&data.assetpool, "rocket launcher");
        rm->relative_transform.pos[0] = -7.8f;
        rm->relative_transform.pos[1] = -4.4f;
        rm->relative_transform.pos[2] = -5.0f;
        rm->relative_transform.rot[1] = -1.5f;
        rm->shader = test_shader_index;
        vec3_set(rm->relative_transform.scale, 0.1f, 0.1f, 0.1f);
        PhysicsComponent_setActivity(pc_a, 1);
        pc_a->friction = 5.0f;
        pc_a->max_velocity = 5.0f;
        pc_a->elasticity = 0.0f;

        RenderModel *bill = ModelComponent_attachRenderModel(mc_a, &data.scene3d, 1);
        bill->model = getDefaultPlaneModel();
        bill->material = AssetPool_getMaterial(&data.assetpool, "muzzlefire");
        bill->shader = SHADER3D_BILLBOARD;
        bill->mode = RENDER_MODEL_BILLBOARD;
        bill->relative_transform.pos[0] = -1.0f;
        bill->relative_transform.pos[1] = 0.5f;
        bill->relative_transform.pos[2] = 0.5f;
        bill->relative_transform.scale[0] = 0.5f;
        bill->relative_transform.scale[1] = 0.5f;
        bill->relative_transform.scale[2] = 0.5f;


        SET_BITFLAG_ON(PhysicsComponent_getBoxCollider(pc_a, "box")->flags,
            char, COLLIDER_FLAG_IS_INACTIVE);

        Entity_setName(data.physics_cube_a, "player");

        SphereCollider *sphere = PhysicsComponent_attachSphereColliderByName(pc_a,
            data.physics_cube_a, "sphere");
        sphere->radius = 2.0f;
        vec3_set(sphere->position, 0.0f, 0.0f, 0.0f);
        /*SET_BITFLAG_ON(sphere->flags, char, COLLIDER_FLAG_IGNORE_STATIC_GEOMETRY);*/
        SET_BITFLAG_ON(PhysicsComponent_getBoxCollider(pc_a, "box")->flags, char,
            COLLIDER_FLAG_IGNORE_STATIC_GEOMETRY);
    }

    {
        data.physics_cube_b = createCube(3.0f, 6.0f, 1.0f, 1.0f);
        PhysicsComponent *pc_b = Entity_getPhysicsComponent(data.physics_cube_b);
        PhysicsComponent_setActivity(pc_b, 1);
        pc_b->friction = 5.0f;
        pc_b->max_velocity = 5.0f;
        pc_b->elasticity = 0.0f;
        //DEBUG_PRINTF("%f\n", PhysicsComponent_getBoxCollider(pc_b, "box")->dimensions[0]);
        RenderModel *rm = ModelComponent_getDefaultRenderModel(
            Entity_getModelComponent(data.physics_cube_b));
        rm->shader = test_shader_index;
        
        rm->model = getDefaultCubeModel();
    }

    {
        data.raytest_cube = createCube(15.0f, 2.0f, 15.0f, 1.0f);
        PhysicsComponent *pc_test = Entity_getPhysicsComponent(data.raytest_cube);
        PhysicsComponent_setStaticity(pc_test, 0);
        pc_test->friction = 10.0f;
        pc_test->gravity_active = 0;
    }

    data.ground_cube = createCube(0.0f, data.scene3d.physics_limits.min_y_value, 0.0f, 1.0f);
    Entity_setScale(data.ground_cube, 4.0f, 1.0f, 4.0f);
    PhysicsComponent *pc_ground = Entity_getPhysicsComponent(data.ground_cube);
    PhysicsComponent_setStaticity(pc_ground, 1);
    pc_ground->friction = 0.0f;

    for (int i = 0; i < 2; ++i)
    {
        data.entities[i] = createOwl(-2.0f - 0.1f * (float)i, 0.0f, 1.0f);
    }

    Scene3D_setAmbientColor(&data.scene3d, 1.0f, 1.0f, 1.0f, 1.0f);
    Scene3D_setBackgroundColor(&data.scene3d, 0.2f, 0.0f, 0.0f, 0.4f);

    data.tex_skybox = loadCubemapTexture(
        "assets/textures/corona_x_pos.png", "assets/textures/corona_x_neg.png",
        "assets/textures/corona_y_pos.png", "assets/textures/corona_y_neg.png",
        "assets/textures/corona_z_pos.png", "assets/textures/corona_z_neg.png");

    Material_init(&data.mat, data.texture, data.texture, 0.0f, 0.4f, 0.4f, 0.4f);
    Material_init(&data.mat2, 0, 0, 0.7f, 0.2f, 0.2f, 0.8f);
    Material_init(&data.mat_grass, data.tex_grass, data.tex_grass, 0.0f, 0.0f, 0.0f, 0.0f);

    data.light_entity = createDirectionalLight(1.0f, 2.0f, 0.0f, 5.0f, -3.0f, 2.5f);
    createDirectionalLight(-1.0f, 2.0f, 0.0f, -5.0f, 3.0f, -2.5f);
    vec3 a = { 1.0f, 2.0f, 0.0f };
    vec3 b = { -1.0f, 2.0f, 0.0f };
    vec3 c = { 1.0f, 5.0f, 0.0f };
    data.plane = computePlane(a, b, c);

    createPlayer(0.0f, 0.0f, 0.0f);

    createPhysicsBall(8.0f, 1.0f, 0.0f, 1.0f);

    {
#if 0
        data.physics_ball_a = createPhysicsBall(2.0f, 2.0f, 3.0f, 1.0f);
        PhysicsComponent *pc_b = Entity_getPhysicsComponent(data.physics_ball_a);
        PhysicsComponent_setActivity(pc_b, 1);
        pc_b->friction      = 1.0f;
        pc_b->max_velocity  = 5.0f;
        pc_b->elasticity    = 0.0f;
#endif
    }

    /* Map entity */
    {
        data.map_model = loadModel("assets/models/level1.obj");

        ASSERT(data.map_model);

        Entity *e = Scene3D_createEntity(&data.scene3d, 0, 0, 0);
        ASSERT(e);

        Entity_setName(e, "MAP");

        ModelComponent *m = Entity_attachModelComponent(e);
        ASSERT(m);

        m->render_models->model = data.map_model;
        m->visible = 1;
    }

    if (data.map_model)
    {
        vec3 offset = {0.0f, 0.0f, 0.0f};
        Scene3D_buildCollisionWorldFromModel(&data.scene3d, data.map_model,
            offset, 5);

        /* static boxcollider*/
        Scene3D_addStaticBoxCollidersToWorld(&data.scene3d, 5.0f, 5.0f, 5.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
        
    }
    else
        DEBUG_PRINTF("Could not find model for collision world!\n");

#if _CF_ALLOW_TEXT_MODE
    timeout(5);
#endif

    setVsync(1);
	kys_run(argc, argv, screen);

    {
        int dc = Client_disconnect(&client);
        DEBUG_PRINTF("Client_disconnect() returned: %i.\n", dc);
    }

    return 0;
}

void
testScript(Entity *e, void *data)
{
    /*DEBUG_PRINTF("PLAYER SCRIPT UPDATING\n");*/
}

int
raycasttest(vec3 a, vec3 b, Plane *plane, float *t, vec3 q)
{
    vec3 ab;
    vec3_sub(ab, b, a);
    float dotna = vec3_mul_inner(plane->normal, a);
    float dotnab = vec3_mul_inner(plane->normal, ab);

    *t = (plane->dot - dotna) / dotnab;

    if (*t >= 0.0f && *t <= 1.0f)
    {
        vec3 multi;
        vec3_scale(multi, ab, *t);
        vec3_add(q, a, multi);
        DEBUG_PRINTF("JEE OSUIT PLANEEEN\n");
        return 1;
    }

    return 0;
}

Plane
computePlane(vec3 a, vec3 b, vec3 c)
{
    Plane p;
    vec3 cross;
    vec3 norm;
    vec3 ba;
    vec3 ca;
    vec3_sub(ba, b, a);
    vec3_sub(ca, c, a);
    vec3_mul_cross(cross, ba, ca);
    vec3_norm(norm, cross);
    vec3_copy(p.normal, norm);
    p.dot = vec3_mul_inner(p.normal, a);

    return p;
}
