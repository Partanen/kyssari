#version 330 core

struct Material
{
    sampler2D   diffuse;
    sampler2D   specular;
    float       shine;
    vec3        color;
};

layout (std140) uniform Shader3DSharedMatrices
{
    mat4 projection;
    mat4 look_at;
    vec3 camera_pos;
};


in vec2 tex_coord;

in vec3 out_normal;

out vec4 frag_color;

uniform Material    material;

void main(void)
{
	frag_color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
}
