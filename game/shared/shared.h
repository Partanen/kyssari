#define INGAME_ASSETPOOL_MEMORY_SIZE MEGABYTES(8)

#define KS_VERSION_STR          "0.6.6"
#define PLAYER_COLLIDER_ID      98
#define PLAYER_FOOT_COLLIDER_ID 99
#define PLAYER_MOVEMENT_SPEED   20.0f
#define PLAYER_DASH_SPEED       30.0f
#define PLAYER_JUMP_SPEED       20.0f
#define MAX_INPUT_BUFFER_ITEMS  64
#define ROCKET_RAY_LENGTH       300
#define SMG_RAY_LENGTH          100
#define RAILGUN_RAY_LENGTH      300
#define MAX_HEALTH              100
#define MAX_ITEMS_PER_LEVEL     64
#define MAX_DISTANCE_TO_PLAY_SOUND 60.0f

#define COOLDOWN_ROCKET_LAUNCHER    0.2f
#define COOLDOWN_SMG                0.1f
#define COOLDOWN_RAILGUN            1.0f
#define COOLDOWN_MACHETE            0.3f

typedef uint16_t                    input_sequence_t;
typedef char                        kb_state_t;
typedef struct BaseBulletType       BaseBulletType;
typedef struct BaseWeaponType       BaseWeaponType;
typedef struct BasePlayerScriptData BasePlayerScriptData;
typedef struct ClientInput          ClientInput;

#define LEVEL_1_COLLISION_MAP_FILE_PATH \
    "assets/models/sps_level1_map_stairsexcluded.obj"
#define MAX_INPUT_REPLAYS 16
#define MAX_INPUT_ROLLBACK 4

#define MEDKIT_RADIUS       0.5f
#define ROCKET_AMMO_RADIUS  3.0f
#define SMG_AMMO_RADIUS     3.0f
#define RAILGUN_AMMO_RADIUS 3.0f

/* This type is not actually defined here, but individually in the
 * server and client files. Both share the "base" member of type
 * BasePlayerScriptData */
typedef struct PlayerScriptData PlayerScriptData;

enum ClientInputType
{
    INPUT_MOVEMENT = 1,
};

enum KsEntityType
{
    ENTITY_TYPE_PLAYER_PAWN = 1
};

enum KsItemType
{
    ITEM_MEDKIT = 0,
    ITEM_ROCKET_AMMO,
    ITEM_SMG_AMMO,
    ITEM_RAILGUN_AMMO,

    NUM_ITEM_TYPES
};

enum KsWeaponType
{
    ROCKET_LAUNCHER = 0,
    SMG,
    RAILGUN,
    MACHETE,

    NUM_BASE_WEAPON_TYPES
};

#define DEFAULT_WEAPON ROCKET_LAUNCHER

enum KsCharacterType
{
    CHARACTER_TYPE_ANARCHIST = 0,
    CHARACTER_TYPE_NATIVE,
    CHARACTER_TYPE_PIRATE,
    CHARACTER_TYPE_OFFICER,

    NUM_CHARACTER_TYPES
};

enum KsPawnMovementState
{
    PAWN_MOVEMENT_STATE_STAND,
    PAWN_MOVEMENT_STATE_FORWARD,
    PAWN_MOVEMENT_STATE_BACKWARD,
    PAWN_MOVEMENT_STATE_LEFT,
    PAWN_MOVEMENT_STATE_RIGHT,
    PAWN_MOVEMENT_STATE_JUMP_UP,
    PAWN_MOVEMENT_STATE_JUMP_DOWN,
    PAWN_MOVEMENT_STATE_DASH,
    PAWN_MOVEMENT_STATE_DASH_BACK,

    NUM_PAWN_MOVEMENT_STATES
};

#define DEFAULT_PAWN_MOVEMENT_STATE PAWN_MOVEMENT_STATE_STAND

enum KsKeyBit
{
    KS_KEY_FORWARD          = (1 << 0),
    KS_KEY_LEFT             = (1 << 1),
    KS_KEY_RIGHT            = (1 << 2),
    KS_KEY_BACK             = (1 << 3),
    KS_KEY_JUMP             = (1 << 4),
    KS_KEY_DASH             = (1 << 5)
};

enum KsMapId
{
    KS_MAP_NONE = 0,
    KS_MAP_1
};

struct BaseBulletType
{
    int         type;
    int         damage;
    float       speed;
    float       lifetime;
};

struct BaseWeaponType
{
    float           fire_rate;
    const char      *name;
};

struct BasePlayerScriptData
{
    int                         hp;
    float                       movement_speed;
    bool32                      is_dashing, is_jumping, jump_pressed, dash_pressed,
                                weaponswap_pressed, is_grounded, f_pressed,
                                f_toggled, melee_attacking, can_shoot, is_hit;
    float                       is_hit_timer;
    float                       shoot_timer;
};

struct ClientInput
{
    ubyte               type;
    input_sequence_t    sequence;

    union ClientInputData
    {
        struct ClientInputKeyboardStateData
        {
            kb_state_t  kb_state;
            float       orientation;
        } movement;
    } data;
};

extern struct SharedGameData
{
    BaseBulletType  base_bullet_types[NUM_BASE_WEAPON_TYPES];
    BaseWeaponType  base_weapon_types[NUM_BASE_WEAPON_TYPES];
} shared_game_data;

extern uint _num_ignore_player_collider_ids;
extern uint32_t _ignore_player_collider_ids[2];

#define GET_BASE_BULLET_TYPE(_enumerator) \
    (&shared_game_data.base_bullet_types[_enumerator])
#define GET_BASE_WEAPON_TYPE(_enumerator) \
    (&shared_game_data.base_weapon_types[_enumerator])
#define GET_KEY_STATE_BIT(_state, _bit) GET_BITFLAG(_state, kb_state_t, _bit)

int
shared_buildCollisionWorldForLevel1(Scene3D *scene, Model *map_model);

void
shared_initPlayerPawn(Entity *e);

void
shared_initSharedGameData();

static inline void
shared_initBasePlayerScriptData(BasePlayerScriptData *data);

void
shared_setDefaultScenePhysicsLimits(Scene3D *scene);

BaseBulletType
shared_createBaseBulletType(int enumerator, int damage, float speed,
    float lifetime);

BaseWeaponType
shared_createBaseWeaponType(const char *name, float firerate);

/* Shared functions to apply inputs to pawns so that their velocity changes,
 * but nothing else is affected.
 * TODO: Combine and fetch physics component only once. */

int
movePawnByKeyboardInput(Entity *pawn, kb_state_t kb_state);

static inline void
shared_initBasePlayerScriptData(BasePlayerScriptData *d)
{
    d->hp                       = 100;
    d->is_dashing               = 0;
    d->is_jumping               = 0;
    d->jump_pressed             = 0;
    d->is_grounded              = 0;
    d->f_pressed                = 0;
    d->f_toggled                = 0;
    d->movement_speed           = 20.0f;
    d->melee_attacking          = 0;
    d->can_shoot                = 1;
    d->shoot_timer              = 0.0f;
}
