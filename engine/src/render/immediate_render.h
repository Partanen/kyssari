#pragma once

/* Forward declaration(s) */
typedef struct Renderer Renderer;

typedef struct ImmediateRenderState ImmediateRenderState;

struct ImmediateRenderState
{
    GLuint      shader;
    GLuint      vbo;
    GLuint      vao;
    GLint       projection_location;
    GLint       color_location;
    GLint       model_location;
    GLfloat     *vertices;
    uint        num_floats;
    uint        max_floats;
    GLenum      mode;
    bool32      began;
    Renderer    *renderer;
    mat4x4      matrix;
    mat4x4      mat_model;
    GLfloat     line_width;
    GLfloat     color[4];
};

int
ImmediateRenderState_init(ImmediateRenderState *state, Renderer *renderer,
    char *log, int log_len);

bool32
ImmediateRenderState_begin(ImmediateRenderState *state, GLenum mode);

bool32
ImmediateRenderState_end(ImmediateRenderState *state);

bool32
ImmediateRenderState_vertex3f(ImmediateRenderState *state,
    float x, float y, float z);

static inline void
ImmediateRenderState_identity(ImmediateRenderState *state);

void
ImmediateRenderState_perspective(ImmediateRenderState *state,
    float field_of_view, float aspect,
    float near_clip, float far_clip);

void
ImmediateRenderState_lookAt(ImmediateRenderState *state,
    vec3 eye, vec3 target, vec3 up);

void
ImmediateRenderState_lookAtCamera(ImmediateRenderState *state,
    Camera *camera);

void
ImmediateRenderState_ortho(ImmediateRenderState *state,
    float x, float y, float w, float h, float n, float f);

void
ImmediateRenderState_updateMatrix(ImmediateRenderState *state);

static inline void
ImmediateRenderState_color(ImmediateRenderState *s, float r, float g, float b, float a);

static inline void
ImmediateRenderState_lineWidth(ImmediateRenderState *s, float w);






static inline void
ImmediateRenderState_identity(ImmediateRenderState *state)
{
    mat4x4_identity(state->matrix);
}

static inline void
ImmediateRenderState_color(ImmediateRenderState *s, float r, float g, float b, float a)
{
    setColorf(s->color, r, g, b, a);
}

static inline void
ImmediateRenderState_lineWidth(ImmediateRenderState *s, float w)
{
    s->line_width = w;
}
