#include "camera.h"

static inline void
Camera_reCalculateFrontVector(Camera *camera)
{
    //camera->front[0] = -cosf(camera->yaw) * sinf(camera->pitch) * sinf(camera->roll) - sinf(camera->yaw) * cosf(camera->roll);
    //camera->front[2] = -sinf(camera->yaw) * sinf(camera->pitch) * sinf(camera->roll) + cosf(camera->yaw) * cosf(camera->roll);
    //camera->front[1] = cosf(camera->pitch) * sinf(camera->roll);
    camera->front[0] = (float)(cos(camera->yaw) * cos(camera->pitch));
    camera->front[1] = (float)sin(camera->pitch);
    camera->front[2] = (float)(sin(camera->yaw) * cos(camera->pitch));
}

void
Camera_init(Camera *camera)
{
    vec3_set(camera->position, 0.0f, 0.0f, 3.0f);
    vec3_set(camera->up,       0.0f, 1.0f, 0.0f);
    camera->fov    = 70.0f; /* The var is actually in radians now so why the hell
                               is this still in degrees? Anyway, it looks fine
                               so lets leave it alone for now. */
    camera->pitch  = 0.0f;
    camera->yaw    = CLAMP(DEG_TO_RAD(-90.0f), 0, (float)M_PI * 2.0f);
    camera->roll   = 0.0f;
    camera->near_clip = 0.1f;
    camera->far_clip = 300.0f;
    Camera_reCalculateFrontVector(camera);
    vec3_norm(camera->front, camera->front);
}

void
Camera_moveForward(Camera *camera, float speed)
{
    if (speed <= 0.0f) return;
    vec3 v;
    vec3_scale(v, camera->front, speed);
    vec3_add(camera->position, camera->position, v);
}

void
Camera_moveBack(Camera *camera, float speed)
{
    if (speed <= 0.0f) return;

    vec3 v;
    vec3_scale(v, camera->front, -speed);
    vec3_add(camera->position, camera->position, v);
}

void
Camera_moveUp(Camera *camera, float speed)
{
    if (speed <= 0) return;
    vec3 v;
    vec3_scale(v, camera->up, speed);
    vec3_add(camera->position, camera->position, v);
}

void
Camera_moveDown(Camera *camera, float speed)
{
    if (speed <= 0) return;
    vec3 v;
    vec3_scale(v, camera->up, -speed);
    vec3_add(camera->position, camera->position, v);
}

void
Camera_strafeLeft(Camera *camera, float speed)
{
    if (speed <= 0.0f) return;

    vec3 cross;
    vec3_mul_cross(cross, camera->front, camera->up);
    vec3_norm(cross, cross);
    vec3_scale(cross, cross, speed);
    vec3_sub(camera->position, camera->position, cross);
}

void
Camera_strafeRight(Camera *camera, float speed)
{
    if (speed <= 0.0f) return;

    vec3 cross;
    vec3_mul_cross(cross, camera->front, camera->up);
    vec3_norm(cross, cross);
    vec3_scale(cross, cross, -speed);
    vec3_sub(camera->position, camera->position, cross);
}

void
Camera_turn(Camera *camera, float x, float y, float speed)
{
    if (speed <= 0.0f) return;

    float yaw = camera->yaw + speed * x;

    if (yaw > (float)M_PI * 2.0f)
        yaw = yaw - (float)M_PI * 2.0f;
    else if (yaw < 0.0f)
        yaw = (float)M_PI * 2.0f + yaw;

    camera->yaw   = yaw;
    camera->pitch = CLAMP(camera->pitch + speed * y, -((float)M_PI / 2.0f) + 0.001f, (float)M_PI / 2.0f - 0.001f);
    camera->roll = 0.0f;
    Camera_reCalculateFrontVector(camera);
    vec3_norm(camera->front, camera->front);
}

void
Camera_lookAt(Camera *camera, vec3 target)
{
    vec3 direction;
    vec3_sub(direction, target, camera->position);
    vec3_norm(direction, direction);

    camera->yaw     = (float)atan2(direction[2], direction[0]);
    camera->pitch   = (float)atan2(direction[1],
        sqrtf((direction[0] * direction[0]) + (direction[2] * direction[2])));

    if (camera->yaw > (float)M_PI * 2.0f)
        camera->yaw = camera->yaw - (float)M_PI * 2.0f;
    else if (camera->yaw < 0.0f)
        camera->yaw = (float)M_PI * 2.0f + camera->yaw;
    camera->roll = 0.0f;
    camera->pitch = CLAMP(camera->pitch, -((float)M_PI / 2.0f) + 0.001f, (float)M_PI / 2.0f - 0.001f);

    Camera_reCalculateFrontVector(camera);
    vec3_norm(camera->front, camera->front);
}

mat4x4 *
Camera_updateAndGetViewMatrix(Camera *camera)
{
    vec3 pos_plus_front;
    vec3_add(pos_plus_front, camera->position, camera->front);
    mat4x4_identity(camera->mat_look_at);
    mat4x4_look_at(camera->mat_look_at,
        camera->position, pos_plus_front, camera->up);

    return &camera->mat_look_at;
}

void
Camera_setRotation(Camera *camera, float yaw, float pitch, float roll)
{
    if (yaw > (float)M_PI * 2.0f)
        yaw = yaw - (float)M_PI * 2.0f;
    else if (yaw < 0.0f)
        yaw = (float)M_PI * 2.0f + yaw;

    camera->yaw   = yaw;
    camera->pitch = CLAMP(pitch, -((float)M_PI / 2.0f) + 0.001f, (float)M_PI / 2.0f - 0.001f);
    camera->roll = 0.0f;

    Camera_reCalculateFrontVector(camera);
    vec3_norm(camera->front, camera->front);
}
