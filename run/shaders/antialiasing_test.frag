#version 330 core

struct Material
{
    sampler2D   diffuse;
    sampler2D   specular;
    float       shine;
    vec3        color;
};

layout (std140) uniform Shader3DSharedMatrices
{
    mat4 projection;
    mat4 look_at;
    vec3 camera_pos;
};


in vec2 tex_coord;

in vec3 out_normal;

out vec4 frag_color;

uniform Material    material;

float width = 1024.0f;
float height = 1024.0f;

const vec3 luma = vec3(0.299f, 0.587f, 0.114f);
const float FXAA_SUBPIX_SHIFT = 1.0f/4.0f;


vec2 rcpFrame = vec2(1.0f/width, 1.0f/height);

vec4 posPos = vec4(tex_coord, tex_coord -(rcpFrame * (0.5f + FXAA_SUBPIX_SHIFT)));

vec3 FxaaPixelShader(vec4 posPos, sampler2D diffuse, vec2 rcpFrame)
{
	#define FXAA_REDUCE_MIN (1.0f / 128.0f)
	#define FXAA_REDUCE_MUL (1.0f / 8.0f)
	#define FXAA_SPAN_MAX 8.0f
	
	vec3 rgbNW = vec3(textureLod(diffuse, posPos.zw, 0.0f));
	vec3 rgbNE = vec3(textureLod(diffuse, posPos.zw + vec2(1.0f, 0.0f)*rcpFrame, 0.0f));
	vec3 rgbSW = vec3(textureLod(diffuse, posPos.zw + vec2(0.0f, 1.0f)*rcpFrame, 0.0f));
	vec3 rgbSE = vec3(textureLod(diffuse, posPos.zw + vec2(1.0f, 1.0f)*rcpFrame, 0.0f));
	vec3 rgbM = vec3(textureLod(diffuse, posPos.xy, 0.0f));
	
	vec3 luma = vec3(0.299f, 0.587f, 0.114f);
	float lumaNW = dot(rgbNW, luma);
	float lumaNE = dot(rgbNE, luma);
	float lumaSW = dot(rgbSW, luma);
	float lumaSE = dot(rgbSE, luma);
	float lumaM = dot(rgbM, luma);
	
	float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
	float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));
	
	vec2 dir = vec2(-(lumaNW + lumaNE) - (lumaSW + lumaSE), (lumaNW + lumaSW) - (lumaNE + lumaSE));
	
	float dirReduce = max((lumaNW + lumaNE + lumaSW + lumaSE) * (0.25f * FXAA_REDUCE_MUL), FXAA_REDUCE_MIN);
	float rcpDirMin = 1.0f/(min(abs(dir.x), abs(dir.y)) + dirReduce);
	dir = min(vec2(FXAA_SPAN_MAX, FXAA_SPAN_MAX), max(vec2(-FXAA_SPAN_MAX, -FXAA_SPAN_MAX), dir * rcpDirMin)) * rcpFrame.xy;
	
	vec3 rgbA = (1.0f / 2.0f) * (textureLod(diffuse, posPos.xy + dir * (1.0f/3.0f - 0.5f), 0.0f).xyz + textureLod(diffuse, posPos.xy + dir * (2.0f/3.0f - 0.5f), 0.0f).xyz);
	
	vec3 rgbB = rgbA * (1.0f/2.0f) + (1.0f / 4.0f) * (textureLod(diffuse, posPos.xy + dir * (0.0f/3.0f - 0.5f),0.0f).xyz + textureLod(diffuse, posPos.xy + dir *(3.0f/3.0f - 0.5f),0.0f).xyz);
	float lumaB = dot(rgbB, luma);
	if((lumaB < lumaMin) || (lumaB > lumaMax)) return rgbA;
	return rgbB;
}

vec4 PostFX(sampler2D diffuse, vec2 uv)
{
	vec4 c = vec4(0.0f);
	vec2 rpcFrame = vec2(1.0f/width, 1.0f/height);
	c.xyz = FxaaPixelShader(posPos, diffuse, rcpFrame);
	c.w = 1.0f;
	return c;
}

void main(void)
{
	vec2 uv = tex_coord;
	frag_color = PostFX(material.diffuse, uv);
	//frag_color = vec4(0.0f, 0.0f, 0.0f, 1.0f);

}