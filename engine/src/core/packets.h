#pragma once

typedef unsigned char message_t;

/* Just a representation of what a packet header looks like -
 * not actually used anywhere */
struct PacketHeader
{
    uint16_t    sequence;
    uint16_t    last_received;
    uint32_t    received_packet_flags;
};

#define PACKET_HEADER_SIZE (sizeof(uint16_t) * 2 + sizeof(uint32_t))
#define MIN_PACKET_SIZE (PACKET_HEADER_SIZE)
#define MIN_MESSAGE_SIZE (sizeof(message_t) + sizeof(uint16_t))

/* Does NOT contain the size of the packet id or the size of the packet seq */
#define MSG_SIZE_DECL(_name, _size) \
    static const int _name##_SIZE = _size;

#define MSG_MIN_SIZE_DECL(_name, _size) \
    static const int _name##_MIN_SIZE = _size;

#define WRITE_PACKET_HEADER(buffer, sequence, last_received, received_flags) \
{ \
    ByteBuffer_write(buffer, uint16_t, sequence); \
    ByteBuffer_write(buffer, uint16_t, last_received); \
    ByteBuffer_write(buffer, uint32_t, received_flags); \
}

/* Message types */
enum InbuiltServerToClientMessageType
{
    SC_MSG_ACCEPT_CONNECTION = 1,
    SC_MSG_REFUSE_CONNECTION,
    SC_MSG_TIMEOUT_DISCONNECT_NOTIFY,
    SC_MSG_INFORM_OF_ENTITY,
    SC_MSG_INFORM_OF_ENTITY_REMOVAL,
    SC_MSG_ENTITY_POSITION_UPDATE,
    SC_MSG_ENTITY_VELOCITY_UPDATE,
    SC_MSG_ENTITY_ROTATION_UPDATE,

    NUM_SERVER_TO_CLIENT_MESSAGE_TYPES
};

#define FIRST_USER_SERVER_TO_CLIENT_MESSAGE_ID \
    NUM_SERVER_TO_CLIENT_MESSAGE_TYPES

enum InbuiltClientToServerMessageType
{
    CS_MSG_CONNECTION_REQUEST = 1,
    CS_MSG_DISCONNECT_NOTIFY,

    NUM_CLIENT_TO_SERVER_MESSAGE_TYPES
};

#define FIRST_USER_CLIENT_TO_SERVER_MESSAGE_ID \
    NUM_CLIENT_TO_SERVER_MESSAGE_TYPES

enum InbuiltAnyMessageType
{
#   if (NUM_SERVER_TO_CLIENT_MESSAGE_TYPES > NUM_CLIENT_TO_SERVER_MESSAGE_TYPES)
    ANY_MSG_DUMMY = NUM_CLIENT_TO_SERVER_MESSAGE_TYPES,
#   else
    ANY_MSG_DUMMY = NUM_SERVER_TO_CLIENT_MESSAGE_TYPES,
#   endif

    NUM_ANY_MESSAGE_TYPES
};

enum ConnectionRefuseReason
{
    CONNECTION_REFUSE_TOO_MANY_CONNECTIONS,
    CONNECTION_REFUSE_NO_REASON
};

MSG_SIZE_DECL(SC_MSG_ACCEPT_CONNECTION, 0);

MSG_SIZE_DECL(SC_MSG_REFUSE_CONNECTION, 1);

MSG_SIZE_DECL(SC_MSG_TIMEOUT_DISCONNECT_NOTIFY, 0);

MSG_SIZE_DECL(SC_MSG_INFORM_OF_ENTITY, (int)sizeof(replication_id32_t) + \
    (int)sizeof(entity_type16_t) + (int)sizeof(float) * 3);

MSG_SIZE_DECL(SC_MSG_INFORM_OF_ENTITY_REMOVAL, (int)sizeof(replication_id32_t));

MSG_SIZE_DECL(SC_MSG_ENTITY_POSITION_UPDATE, (int)sizeof(replication_id32_t) + \
    sizeof(vec3));

MSG_SIZE_DECL(SC_MSG_ENTITY_VELOCITY_UPDATE, (int)sizeof(replication_id32_t) + \
    sizeof(vec3));

MSG_SIZE_DECL(SC_MSG_ENTITY_ROTATION_UPDATE, (int)sizeof(replication_id32_t) + \
    sizeof(vec3));

MSG_SIZE_DECL(CS_MSG_CONNECTION_REQUEST, 0);

MSG_SIZE_DECL(CS_MSG_DISCONNECT_NOTIFY, 1);

/*#############################
 *# CLIENT TO SERVER MESSAGES #
 *#############################*/

static inline void
CS_MSG_DISCONNECT_NOTIFY_serialize(ByteBuffer *buf, char code)
{
    ByteBuffer_write(buf, char, code);
}

/*#############################
 *# SERVER TO CLIENT MESSAGES #
 *#############################*/

static inline void
SC_MSG_REFUSE_CONNECTION_serialize(ByteBuffer *buf,
    enum ConnectionRefuseReason reason)
{
    ByteBuffer_write(buf, char, (char)reason);
}

static inline void
SC_MSG_INFORM_OF_ENTITY_serialize(ByteBuffer *buf,
    replication_id32_t replication_id,
    entity_type16_t e_type, /* like the band :DDDD */
    vec3 pos)
{
    ByteBuffer_write(buf, replication_id32_t, replication_id);
    ByteBuffer_write(buf, entity_type16_t, e_type);
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
}

static inline void
SC_MSG_ENTITY_POSITION_UPDATE_serialize(ByteBuffer *buf,
    replication_id32_t replication_id,
    vec3 position)
{
    ByteBuffer_write(buf, replication_id32_t, replication_id);
    ByteBuffer_writeBytes(buf, position, sizeof(vec3));
}

static inline void
SC_MSG_ENTITY_VELOCITY_UPDATE_serialize(ByteBuffer *buf,
    replication_id32_t replication_id,
    vec3 velocity)
{
    ByteBuffer_write(buf, replication_id32_t, replication_id);
    ByteBuffer_writeBytes(buf, velocity, sizeof(vec3));
}

static inline void
SC_MSG_ENTITY_ROTATION_UPDATE_serialize(ByteBuffer *buf,
    replication_id32_t replication_id, vec3 rotation)
{
    ByteBuffer_write(buf, replication_id32_t, replication_id);
    ByteBuffer_writeBytes(buf, rotation, sizeof(vec3));
}
