#pragma once
#include "types.h"

#ifdef _WANGBLOWS
    #include "sys_win32.h"
#elif defined(_UNIX)
    #include "sys_unix.h"
#endif

typedef struct Address      Address;
typedef struct Socket       Socket;
typedef struct ByteBuffer   ByteBuffer;

#define KYS_SERVER_PORT_STR     "44344"
#define KYS_SERVER_PORT         44344
#define KYS_CLIENT_PORT_STR     "44345"
#define KYS_CLIENT_PORT         44345

enum SocketError
{
    SOCK_ERROR_BAD_PORT = 1,
    SOCK_ERROR_CREATE,
    SOCK_ERROR_BIND,
    SOCK_ERROR_NOT_OPEN,
    SOCK_ERROR_SETTING_OPTIONS,
    SOCK_ERROR_LISTEN
};

struct Address
{
    uint32_t            address;    /* Host order */
    ushort              port;       /* Host order */
    struct sockaddr_in  addr_in;    /* Network order */
};

/* UDP only */
struct Socket
{
    sys_socket_t    fd;
    bool32          open;
    ushort          port;
};

struct ByteBuffer
{
    char *memory;
    uint max_size;
    uint offset;
};

void
Address_init(Address *addr,
    ubyte x, ubyte y, ubyte z, ubyte w,
    ushort port);

void
Address_initFromUint32(Address *addr, uint32_t xyzw, ushort port);

Address
createAddress(ubyte x, ubyte y, ubyte z, ubyte w, ushort port);

static inline ubyte
Address_getX(Address *addr);

static inline ubyte
Address_getY(Address *addr);

static inline ubyte
Address_getZ(Address *addr);

static inline ubyte
Address_getW(Address *addr);

/* Closes the socket if it was already open,
 * then opens it again */
int
Socket_open(Socket *socket);

int
Socket_bind(Socket *socket, ushort port);

void
Socket_close(Socket *socket);

static inline int
Socket_send(Socket *socket,
    Address *address,
    void *data, uint len);

static inline int
Socket_listen(Socket *socket, int backlog);

/* Listen to incoming packets and store the sender
 * address to parameter Address. */
int
Socket_receiveFrom(Socket *socket,
    char *buf, size_t buf_size,
    Address *from);

int
Socket_setNonBlocking(Socket *sock);

static inline int
Socket_listen(Socket *socket, int backlog);

static inline void
ByteBuffer_init(ByteBuffer *buf, char *memory, uint size);


static inline void *
ByteBuffer_alloc(ByteBuffer *buffer, uint size);

#define ByteBuffer_clear(buf_ptr) ((buf_ptr)->offset = 0)

#define ByteBuffer_write(buf_ptr, type, val) \
{ \
    ASSERT((buf_ptr)->offset + sizeof(type) <= (buf_ptr)->max_size); \
    type *val_dest = (type*)((buf_ptr)->memory + (buf_ptr)->offset); \
    *val_dest = val; \
    (buf_ptr)->offset += sizeof(type); \
}

#define ByteBuffer_read(buf_ptr, type, ret_ptr) \
{ \
    ASSERT((buf_ptr)->offset + sizeof(type) <= (buf_ptr)->max_size); \
    type *value_ptr = (type*)((buf_ptr)->memory + (buf_ptr)->offset); \
    *(ret_ptr) = *value_ptr; \
    (buf_ptr)->offset += sizeof(type); \
}

/* Returns 0 on success, 1 otherwise */
static inline int
ByteBuffer_readBytes(ByteBuffer *buffer, void *target, uint num_bytes);

/* Returns 0 on success */
static inline int
ByteBuffer_writeBytes(ByteBuffer *buffer, void *bytes, uint num_bytes);

static inline ubyte
Address_getX(Address *addr)
{
    return *((char*)(&addr->address) + 3);
}

static inline ubyte
Address_getY(Address *addr)
{
    return *((char*)(&addr->address) + 2);
}

static inline ubyte
Address_getZ(Address *addr)
{
    return *((char*)(&addr->address) + 1);
}

static inline ubyte
Address_getW(Address *addr)
{
    return *(char*)(&addr->address);
}

static inline int
Socket_send(Socket *sock,
    Address *address,
    void *data, uint len)
{
    return sendto(sock->fd, data, len,
        0, (struct sockaddr*)&address->addr_in,
        sizeof(address->addr_in));
}

static inline int
Socket_listen(Socket *socket, int backlog)
{
    return sys_listenSocket(socket->fd, backlog) == -1;
}

static inline void
ByteBuffer_init(ByteBuffer *buf, char *memory, uint size)
{
    ASSERT(memory);
    buf->offset     = 0;
    buf->max_size   = size;
    buf->memory     = memory;
}

static inline void *
ByteBuffer_alloc(ByteBuffer *buffer, uint size)
{
    if (size > buffer->max_size - buffer->offset) return 0;
    void *ret = buffer->memory + buffer->offset;
    ++buffer->offset;
    return ret;
}

static inline int
ByteBuffer_readBytes(ByteBuffer *buffer, void *target, uint num_bytes)
{
    if (buffer->max_size - buffer->offset >= num_bytes)
    {
        memcpy(target, buffer->memory + buffer->offset, num_bytes);
        buffer->offset += num_bytes;
        return 0;
    }

    return 1;
}

static inline int
ByteBuffer_writeBytes(ByteBuffer *buffer, void *bytes, uint num_bytes)
{
    if (buffer->max_size - buffer->offset >= num_bytes)
    {
        memcpy(buffer->memory + buffer->offset, bytes, num_bytes);
        buffer->offset += num_bytes;
        return 0;
    }
    return 1;
}
