.PHONY: all build-dirs sqlite3-debug

.default: ship

prep: build-dirs sqlite3-debug

all:
	nmake build-dirs
	nmake sqlite3-debug
	nmake prep
	nmake ship

build-dirs:
	IF NOT EXIST "engine/build" mkdir "engine/build"
	IF NOT EXIST "engine/intermediate" mkdir "engine/intermediate"
	IF NOT EXIST "game/client/build" mkdir "game/client/build"
	IF NOT EXIST "game/client/intermediate" mkdir "game/client/intermediate"
	IF NOT EXIST "game/server/build" mkdir "game/server/build"
	IF NOT EXIST "game/server/intermediate" mkdir "game/server/intermediate"
	IF NOT EXIST "playground/build" mkdir "playground/build"
	IF NOT EXIST "playground/intermediate" mkdir "playground/intermediate"
	IF NOT EXIST "engine/src/compile_flags.h" XCOPY \
		engine\src\default_compile_flags.h engine\src\compile_flags.h

sqlite3-debug:
	$(CC) /D_DEBUG /c /wd4142 /W3 /EHsc /Zi /Od /Tc \
	engine\src\lib\sqlite3\sqlite3.c /Fobin\windows\sqlite3.obj

ship:
	cd game\server && nmake ship
