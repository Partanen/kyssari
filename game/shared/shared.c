struct SharedGameData shared_game_data = {0};

uint _num_ignore_player_collider_ids = 2;
uint32_t _ignore_player_collider_ids[2] =
{
    PLAYER_COLLIDER_ID,
    PLAYER_FOOT_COLLIDER_ID
};

static void
playerFeetCollisionCallback(PhysicsComponent *component,
    Entity *entity,
    enum ColliderType collider, uint32_t collider_id,
    vec3 normal, enum CollisionEventType event_type);

int
shared_buildCollisionWorldForLevel1(Scene3D *scene, Model *map_model)
{
    vec3 offset = {0.0f, 0.0f, 0.0f};
    if (Scene3D_buildCollisionWorldFromModel(scene, map_model, offset, 6, 32) != 0)
        return 1;

    Scene3D_addStaticBoxCollidersToWorld(scene,
        35.82f, 5.2f, 11.6f, 5.0f, 0.5f, 1.5f, 0.0f, 0.4f, 0.695f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        41.04f, 9.5f, 4.39f, 3.0f, 0.5f, 1.5f, 0.0f, 0.4f, 0.71f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        52.11f, 9.45f, -36.48f, 3.5f, 0.5f, 1.5f, 0.0f, 1.68f, 0.56f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        50.09f, 13.4f, -43.69f, 5.0f, 0.5f, 1.5f, 0.0f, 1.97f, 0.44f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        46.08f, 16.55f, -59.806f, 3.0f, 0.5f, 1.5f, 0.0f, 1.64f, 0.29f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        -61.089f, 21.11f, -51.279f, 3.0f, 0.5f, 1.0f, 0.0f, 2.28f, 0.64f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        -64.34f, 23.2f, -51.64f, 2.0f, 0.5f, 1.0f, 0.0f, 3.86f, 0.58f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.31f, 24.49f, -21.66f, 7.0f, 0.5f, 1.5f, 0.0f, 1.62f, 0.62f);

    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 30.4f, -31.18f, 2.5f, 1.5f, 2.5f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 33.4f, -31.18f, 2.5f, 1.5f, 2.5f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 36.4f, -31.18f, 2.5f, 1.5f, 2.5f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 39.4f, -31.18f, 2.25f, 1.5f, 2.25f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 42.4f, -31.18f, 2.25f, 1.5f, 2.25f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 45.4f, -31.18f, 2.25f, 1.5f, 2.25f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 48.4f, -31.18f, 2.0f, 1.5f, 2.0f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 51.4f, -31.18f, 2.0f, 1.5f, 2.0f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 54.4f, -31.18f, 2.0f, 1.5f, 2.0f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 57.4f, -31.18f, 2.0f, 1.5f, 2.0f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 60.4f, -31.18f, 2.0f, 1.5f, 2.0f, 0.0f, 0.0f, 0.0f);
    Scene3D_addStaticBoxCollidersToWorld(scene,
        0.0f, 63.4f, -31.18f, 2.0f, 1.5f, 2.0f, 0.0f, 0.0f, 0.0f);

    return 0;
}

void
shared_initPlayerPawn(Entity *e)
{
    e->type = ENTITY_TYPE_PLAYER_PAWN;

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
    PhysicsComponent_setActivity(pc, 1);
    PhysicsComponent_setStaticCollisionCallback(pc,
        playerFeetCollisionCallback);

    pc->friction        = 10.0f;
    pc->max_velocity    = 5.0f;
    pc->elasticity      = 0.0f;
    pc->gravity_active  = 1;

    SphereCollider *body = PhysicsComponent_attachSphereCollider(pc,e,
        PLAYER_COLLIDER_ID);
    body->radius = 1.0f;
    vec3_set(body->position, 0.0f, 0.0f, 0.0f);
    SphereCollider_setAxisLock(body, 1, 1, 1);

    SphereCollider *feet = PhysicsComponent_attachSphereCollider(pc, e,
        PLAYER_FOOT_COLLIDER_ID);
    feet->radius = 1.0f;
    vec3_set(feet->position, 0.0f, 1.5f, 0.0f);
    SphereCollider_setAxisLock(feet, 1, 1, 1);
}

void
shared_initSharedGameData()
{
    /* Bullets */
    shared_game_data.base_bullet_types[ROCKET_LAUNCHER] =
        shared_createBaseBulletType(ROCKET_LAUNCHER, 5, 60.0f, 2.0f);
    shared_game_data.base_bullet_types[SMG] =
        shared_createBaseBulletType(SMG, 1, 100.0f, 0.2f);
    shared_game_data.base_bullet_types[RAILGUN] =
        shared_createBaseBulletType(RAILGUN, 2, 100.0f, 0.2f);
    shared_game_data.base_bullet_types[MACHETE] =
        shared_createBaseBulletType(MACHETE, 100, 40.0f, 0.2f);

    /* Weapons */
    shared_game_data.base_weapon_types[ROCKET_LAUNCHER] =
        shared_createBaseWeaponType("Rocket Launcher", 0.6f);
    shared_game_data.base_weapon_types[SMG] =
        shared_createBaseWeaponType("SMG", 0.1f);
    shared_game_data.base_weapon_types[RAILGUN] =
        shared_createBaseWeaponType("Railgun", 1.0f);
    shared_game_data.base_weapon_types[MACHETE] =
        shared_createBaseWeaponType("Puukko", 0.5f);
}

void
shared_setDefaultScenePhysicsLimits(Scene3D *scene)
{
    scene->physics_limits.gravity_direction[0]  = 0.0f;
    scene->physics_limits.gravity_direction[1]  = -1.0f;
    scene->physics_limits.gravity_direction[2]  = 0.0f;
    scene->physics_limits.gravity_strength      = 50.0f;
    scene->physics_limits.max_y_value           = 150.0f;
    scene->physics_limits.min_y_value           = 0.0f;

    scene->physics_step = 0.016f;
}

BaseBulletType
shared_createBaseBulletType(int enumerator, int damage, float speed,
    float lifetime)
{
    BaseBulletType b;

    b.type              = enumerator;
    b.damage            = damage;
    b.speed             = speed;
    b.lifetime          = lifetime;

    return b;
}

BaseWeaponType
shared_createBaseWeaponType(const char *name, float firerate)
{
    BaseWeaponType w;

    w.fire_rate         = firerate;
    w.name              = name;

    return w;
}

int
movePawnByKeyboardInput(Entity *pawn, kb_state_t kb_state)
{
    PhysicsComponent *pc = Entity_getPhysicsComponent(pawn);
    if (!pc) return 0;
    ScriptComponent *sc = Entity_getScriptComponent(pawn);
    PlayerScriptData *pd = (PlayerScriptData*)sc->data;

    vec3 forward; Entity_getForwardVector(pawn, forward);
    vec3 right; vec3_mul_cross(right, forward, DEFAULT_UP_VECTOR);

    int next_movement_state;

    if (!GET_KEY_STATE_BIT(kb_state, KS_KEY_FORWARD)
    &&  !GET_KEY_STATE_BIT(kb_state, KS_KEY_LEFT)
    &&  !GET_KEY_STATE_BIT(kb_state, KS_KEY_RIGHT)
    &&  !GET_KEY_STATE_BIT(kb_state, KS_KEY_BACK))
    {
        pc->acceleration[0] = 0.0f;
        pc->acceleration[2] = 0.0f;
        pd->base.movement_speed = PLAYER_MOVEMENT_SPEED;

        next_movement_state = PAWN_MOVEMENT_STATE_STAND;
    }

    if (GET_KEY_STATE_BIT(kb_state, KS_KEY_FORWARD))
    {
        pc->velocity[0] = forward[0] * pd->base.movement_speed;
        pc->velocity[2] = forward[2] * pd->base.movement_speed;

        next_movement_state = PAWN_MOVEMENT_STATE_FORWARD;

        if (GET_KEY_STATE_BIT(kb_state, KS_KEY_LEFT)
        && GET_KEY_STATE_BIT(kb_state, KS_KEY_FORWARD))
        {
            pc->velocity[0] = (-right[0] + forward[0]) * (pd->base.movement_speed / 2);
            pc->velocity[2] = (-right[2] + forward[2]) * (pd->base.movement_speed / 2);
        }

        if (GET_KEY_STATE_BIT(kb_state, KS_KEY_RIGHT)
        && GET_KEY_STATE_BIT(kb_state, KS_KEY_FORWARD))
        {
            pc->velocity[0] = (right[0] + forward[0]) * (pd->base.movement_speed / 2);
            pc->velocity[2] = (right[2] + forward[2]) * (pd->base.movement_speed / 2);
        }
    }

    if (GET_KEY_STATE_BIT(kb_state, KS_KEY_LEFT)
    && !GET_KEY_STATE_BIT(kb_state, KS_KEY_FORWARD)
    && !GET_KEY_STATE_BIT(kb_state, KS_KEY_BACK))
    {
        pc->velocity[0] = -right[0] * pd->base.movement_speed;
        pc->velocity[2] = -right[2] * pd->base.movement_speed;

        next_movement_state = PAWN_MOVEMENT_STATE_LEFT;
    }

    if (GET_KEY_STATE_BIT(kb_state, KS_KEY_RIGHT)
    && !GET_KEY_STATE_BIT(kb_state, KS_KEY_FORWARD)
    && !GET_KEY_STATE_BIT(kb_state, KS_KEY_BACK))
    {
        pc->velocity[0] = right[0] * pd->base.movement_speed;
        pc->velocity[2] = right[2] * pd->base.movement_speed;

        next_movement_state = PAWN_MOVEMENT_STATE_RIGHT;
    }

    if (GET_KEY_STATE_BIT(kb_state, KS_KEY_BACK))
    {
        pc->velocity[0] = -forward[0] * pd->base.movement_speed;
        pc->velocity[2] = -forward[2] * pd->base.movement_speed;

        next_movement_state = PAWN_MOVEMENT_STATE_BACKWARD;

        if (GET_KEY_STATE_BIT(kb_state, KS_KEY_LEFT)
        &&  GET_KEY_STATE_BIT(kb_state, KS_KEY_BACK))
        {
            pc->velocity[0] = (-right[0] + -forward[0]) * (pd->base.movement_speed / 2);
            pc->velocity[2] = (-right[2] + -forward[2]) * (pd->base.movement_speed / 2);
        }

        if (GET_KEY_STATE_BIT(kb_state, KS_KEY_RIGHT)
        &&  GET_KEY_STATE_BIT(kb_state, KS_KEY_BACK))
        {
            pc->velocity[0] = (right[0] + -forward[0]) * (pd->base.movement_speed / 2);
            pc->velocity[2] = (right[2] + -forward[2]) * (pd->base.movement_speed / 2);
        }
    }

    if (GET_KEY_STATE_BIT(kb_state, KS_KEY_JUMP))
    {
        if (!pd->base.jump_pressed)
        {
            if (pd->base.is_grounded)
            {
                pc->velocity[1]         = 20.0f;
                pd->base.is_grounded    = 0;
                pd->base.is_jumping     = 1;

                next_movement_state = PAWN_MOVEMENT_STATE_JUMP_UP;
            }
        }
    }

    if (GET_KEY_STATE_BIT(kb_state, KS_KEY_DASH))
    {
        if (pd->base.is_grounded)
        {
            pc->velocity[1]         = 10.f;
            pd->base.is_grounded    = 0;
            pd->base.movement_speed = PLAYER_DASH_SPEED;
            pd->base.is_dashing     = 1;

            next_movement_state = PAWN_MOVEMENT_STATE_DASH;
        }
    }

    if (pc->velocity[1] < -5.0f)
        next_movement_state = PAWN_MOVEMENT_STATE_JUMP_DOWN;

    if (pd->base.is_jumping)
    {
        if(pc->velocity[1] >= 0.0f)
            next_movement_state = PAWN_MOVEMENT_STATE_JUMP_UP;
        else if (pc->velocity[1] < 0.0f)
            next_movement_state = PAWN_MOVEMENT_STATE_JUMP_DOWN;
    }

    if (pd->base.is_dashing)
    {
        if (GET_KEY_STATE_BIT(kb_state, KS_KEY_BACK))
            next_movement_state = PAWN_MOVEMENT_STATE_DASH_BACK;
        else
            next_movement_state = PAWN_MOVEMENT_STATE_DASH;
    }

    return next_movement_state;
}

/* Why's this in shared? */
void
turnPlayerEntity(Entity *pawn, int16_t x, int16_t y)
{
    Transform *t = &Entity_getTransform(pawn)->transform;

    float deg1 = (float)RAD_TO_DEG(t->rot[1]);
    float deg2 = (float)RAD_TO_DEG(t->rot[2]);

    deg1 = deg1 - (float)x * (float)getDelta();
    deg2 = deg2 + (float)y * (float)getDelta();

    if (deg1 > (float)RAD_TO_DEG(M_PI) * 2.0f)
        deg1 = deg1 - (float)RAD_TO_DEG(M_PI) * 2.0f;
    else if (deg1 < 0.0f)
        deg1 = (float)RAD_TO_DEG(M_PI) * 2.0f + deg1;

    deg2 = CLAMP(deg2, (float)RAD_TO_DEG(-((float)M_PI / 2.0f)) + 1.000f,
        (float)RAD_TO_DEG((float)M_PI / 2.0f) - 0.100f);

    Entity_setRotation(pawn, t->rot[0],
        (float)DEG_TO_RAD(deg1), (float)DEG_TO_RAD(deg2));
}

static void
playerFeetCollisionCallback(PhysicsComponent *component,
    Entity *entity,
    enum ColliderType collider, uint32_t collider_id,
    vec3 normal, enum CollisionEventType event_type)
{
    ScriptComponent *sc = Entity_getScriptComponent(entity);
    BasePlayerScriptData *pd = (BasePlayerScriptData*)sc->data;

    if (collider_id == PLAYER_FOOT_COLLIDER_ID && event_type == COLLISION_ENTER)
    {
        pd->is_jumping = 0;
        pd->is_grounded = 1;
        if(pd->is_dashing)
        {
            pd->is_dashing = 0;
            pd->movement_speed = PLAYER_MOVEMENT_SPEED;
        }
    }
    if (collider_id == PLAYER_FOOT_COLLIDER_ID && event_type == COLLISION_END)
    {
        pd->is_grounded = 0;
    }
}
