int
createLoadingScreen()
{
    LoadingScreenData *data = &game.loading_screen_data;

    game.screens[LOADING_SCREEN] = createScreen(LoadingScreen_update, data);

    if (!game.screens[LOADING_SCREEN])
        return 1;

    game.screens[LOADING_SCREEN]->onOpen    = LoadingScreen_open;
    game.screens[LOADING_SCREEN]->onClose   = LoadingScreen_close;

    int num_load_errors = 0;

    data->tex_kys_load_anim = loadTexture("assets/textures/loading_screen/"
        "kyssari_load_animation.png");
    if (!game.loading_screen_data.tex_kys_load_anim) ++num_load_errors;

    for (int i = 0; i < 8; ++i)
    {
        data->load_anim_clips[i][0] = (i % 4) * 256;
        data->load_anim_clips[i][1] = (i / 4) * 256;
        data->load_anim_clips[i][2] = 256;
        data->load_anim_clips[i][3] = 256;
    }

    data->tex_i_want_to_believe = loadTexture("assets/textures/loading_screen/"
        "i_want_to_believe.png");
    if (!data->tex_i_want_to_believe) ++num_load_errors;

    data->tex_dark_layer = loadTexture("assets/textures/loading_screen/"
        "dark_layer.png");
    if (!data->tex_dark_layer) ++num_load_errors;

    data->tex_bar = loadTexture("assets/textures/loading_screen/loadbar.png");
    if (!data->tex_bar) ++num_load_errors;

    if (num_load_errors > 0)
    {
        DEBUG_PRINTF("Failed to load some loading screen assets (%i)!\n",
            num_load_errors);
    }

    return 0;
}

void
LoadingScreen_open(Screen *screen)
{
    LoadingScreenData *data = &game.loading_screen_data;

    data->tip_of_the_day = rand() % NUM_TIPS_OF_THE_DAY;

    setRenderBackgroundColor(1.0f, 1.0f, 1.0f, 1.0f);

    data->percent_complete          = 0;
    data->load_anim_timer           = 0.0f;
    data->load_anim_current_clip    = 0;
    data->time_passed = 0;

    int r = loadPooledIngameAssetsThreadedPortion();
    ASSERT(r == 0);
}

void
LoadingScreen_close(Screen *screen)
{
}

static float
_fitBigRectToScreen(int width, int height)
{
    float ww = (float)getWindowWidth();
    float wh = (float)getWindowHeight();

    float w = (float)width;
    float h = (float)height;

    float wd = ww / w;
    float hd = wh / h;

    return wd > hd ? wd : hd;
}

void
LoadingScreen_update(Screen *screen)
{
    (void)screen;

    double dt = getDelta();
    LoadingScreenData *data = &game.loading_screen_data;

    data->time_passed += dt;

    if (game.loaded_startup_data)
    {
        data->percent_complete = (int)((float)data->time_passed / \
            (float)NONINIT_LOADING_SCREEN_MIN_DUR * 100.f);
    }

    GUI_beginUpdate();

    int win_mid_x = (int)getWindowWidth()  / 2;
    int win_mid_y = (int)getWindowHeight() / 2;
    int half_w, half_h;
    float scale;

    /* Background */

    scale = _fitBigRectToScreen(data->tex_i_want_to_believe->width,
        data->tex_dark_layer->height);

    half_w = (int)(data->tex_i_want_to_believe->width  * scale)  / 2;
    half_h = (int)(data->tex_i_want_to_believe->height * scale) / 2;

    GUI_texture_Scale(data->tex_i_want_to_believe, win_mid_x - half_w,
        win_mid_y - half_h, scale, scale);

    /* Dark layer */
    scale = _fitBigRectToScreen(data->tex_dark_layer->width,
        data->tex_dark_layer->height);

    half_w = (int)((float)data->tex_dark_layer->width  * scale) / 2;
    half_h = (int)((float)data->tex_dark_layer->height * scale) / 2;

    GUI_texture_Scale(data->tex_dark_layer, win_mid_x - half_w,
        win_mid_y - half_h, scale, scale);

    /* The loading animation */
    data->load_anim_timer += dt;

    while (data->load_anim_timer >= LOAD_ANIM_FRAME_LEN)
    {
        data->load_anim_timer -= LOAD_ANIM_FRAME_LEN;
        ++data->load_anim_current_clip;

        if (data->load_anim_current_clip >= 8)
            data->load_anim_current_clip = 0;
    }

    int *la_clip = data->load_anim_clips[data->load_anim_current_clip];

#   define BAR_BORDER_OFFSET 10

    int x_sub = getWindowWidth() > data->tex_bar->width ? 126 : \
        BAR_BORDER_OFFSET - 80;

    GUI_texture_ClipScale(data->tex_kys_load_anim,
        getWindowWidth() - 256 - x_sub, getWindowHeight() - 256 - 28,
        la_clip[0], la_clip[1], la_clip[2], la_clip[3],
        0.75f, 0.75f);

    /* Progress bar */

    float sx;

    if (getWindowWidth() > data->tex_bar->width)
        sx = 1.0f;
    else
    {
        sx = (float)(getWindowWidth() - (BAR_BORDER_OFFSET * 2)) / \
            (float)data->tex_bar->width;
    }

    int progress_bar_y = getWindowHeight() - 103 + data->tex_bar->height + 10;

    GUI_texture_ClipScale(data->tex_bar,
        win_mid_x - (int)((float)data->tex_bar->width * sx) / 2,
        progress_bar_y,
        0, 0,
        (int)((float)data->tex_bar->width * ((float)data->percent_complete / 100.0f)),
        data->tex_bar->height,
        sx, 1.0f);

    GUI_text_Scale("Tip of the day:",
        BAR_BORDER_OFFSET + 80, win_mid_y - 32, 2.0f);
    GUI_text_Scale(tips_of_the_day[data->tip_of_the_day],
        BAR_BORDER_OFFSET + 80, win_mid_y - 12, 2.0f);

    GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_RIGHT);
    GUI_textLine(uneducational_str, 0, 13);

    GUI_endUpdate();

    if (!game.loaded_startup_data)
    {

        if (data->percent_complete >= 100
        && data->time_passed >= INITIAL_LOADING_SCREEN_MIN_DUR)
        {
            Thread_join(&game.load_thread);
            loadPermanentAssets();
            loadPooledIngameAssets();
            shared_initSharedGameData();
            initGameData();

            int r;
            r = createMenuScreen(); ASSERT(!r);
            r = createGameScreen(); ASSERT(!r);
            r = createMPGameScreen(); ASSERT(!r);
            setScreen(game.screens[game.screen_after_load]);
            game.loaded_startup_data = 1;
        }
    }
    else if (data->time_passed >= NONINIT_LOADING_SCREEN_MIN_DUR)
        setScreen(game.screens[game.screen_after_load]);

#   undef BAR_BORDER_OFFSET
}
