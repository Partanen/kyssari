#include "spritebatch.h"
#include "renderer.h"
#include "spritemacro.h"

static Texture _blank_white_texture = {0};
Texture *_BLANK_WHITE_TEXTURE = &_blank_white_texture;

#define BATCH_SIZE 2048

/* Macro corner ordering:
 * 1. top left
 * 2. top right
 * 3. bottom left
 * 4. bottom right */

/* Vertex data order:
 * [0] = vertex1 x
 * [1] = vertex1 y
 * [2] = vertex1 tex_x
 * [3] = vertex1_tex_y
 * [4] = vertex1_r
 * [5] = vertex1_g
 * [6] = vertex1_b
 * [7] = vertex1_a
 *
 * [8] = vertex2 x
 * [9] = vertex2 y
 * [10] = vertex2 tex_x
 * [11] = vertex2_tex_y
 * [12] = vertex2_r
 * [13] = vertex2_g
 * [14] = vertex2_b
 * [15] = vertex2_a
 *
 * [16] = vertex3 x
 * [17] = vertex3 y
 * [18] = vertex3 tex_x
 * [19] = vertex3_tex_y
 * [20] = vertex3_r
 * [21] = vertex3_g
 * [22] = vertex3_b
 * [23] = vertex3_a
 *
 * [24] = vertex4 x
 * [25] = vertex4 y
 * [26] = vertex4 tex_x
 * [27] = vertex4_tex_y
 * [28] = vertex4_r
 * [29] = vertex4_g
 * [30] = vertex4_b
 * [31] = vertex4_a */

int
SpriteBatch_init(SpriteBatch *batch, GLchar *error_log, GLint log_len)
{
    batch->tex_swap_count = 0;
    batch->sprite_count   = 0;
    batch->scale_x = 1.0f;
    batch->scale_y = 1.0f;

    batch->vertex_buffer    = (GLfloat*)renderMalloc(BATCH_SIZE * FLOATS_PER_SPRITE * sizeof(GLfloat));
    batch->texture_buffer   = (TextureBufferItem*)renderMalloc(BATCH_SIZE * sizeof(TextureBufferItem));
    batch->buffer_size      = BATCH_SIZE;

    const char *vertex_code =
        "#version 330 core\n"
        "layout (location = 0) in vec2 pos;\n"
        "layout (location = 1) in vec2 in_tex_coord;\n"
        "layout (location = 2) in vec4 in_color_mod;\n"
        "out vec2 tex_coord;\n"
        "out vec4 color_mod;\n"
        "uniform mat4 p;\n"
        "uniform vec2 scale;\n"
        "void main(void)\n"
        "{\n"
        "   gl_Position = p * vec4(scale.x * pos.x, scale.y * pos.y, 0.0, 1.0);\n"
        "   tex_coord = in_tex_coord;\n"
        "   color_mod = in_color_mod;\n"
        "}";

    const char *fragment_code =
        "#version 330 core\n"
        "in vec2 tex_coord;\n"
        "in vec4 color_mod;\n"
        "uniform sampler2D tex;\n"
        "out vec4 frag_color;\n"
        "void main(void)\n"
        "{\n"
        "   frag_color = color_mod * texture(tex, tex_coord);\n"
        "}";

    GLuint vtx_shader = glCreateShader(GL_VERTEX_SHADER);
    GLuint frg_shader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vtx_shader, 1, &vertex_code, 0);
    glShaderSource(frg_shader, 1, &fragment_code, 0);
    glCompileShader(vtx_shader);
    glCompileShader(frg_shader);

    GLint success;

    glGetShaderiv(vtx_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        if (error_log)
            glGetShaderInfoLog(vtx_shader, log_len, NULL, error_log);
        glDeleteShader(vtx_shader);
        glDeleteShader(frg_shader);
        return 2;
    }

    glGetShaderiv(frg_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        if (error_log)
            glGetShaderInfoLog(frg_shader, log_len, NULL, error_log);
        glDeleteShader(vtx_shader);
        glDeleteShader(frg_shader);
        return 3;
    }

    GLuint program = glCreateProgram();
    glAttachShader(program, vtx_shader);
    glAttachShader(program, frg_shader);
    glLinkProgram(program);
    glDeleteShader(vtx_shader);
    glDeleteShader(frg_shader);

    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success)
    {
        if (error_log)
            glGetProgramInfoLog(program, log_len, NULL, error_log);
        glDeleteProgram(program);
        return 4;
    }

    batch->program = program;

    batch->proj_loc    = glGetUniformLocation(batch->program, "p");
    batch->scale_loc   = glGetUniformLocation(batch->program, "scale");

    if (batch->proj_loc < 0)
        return 5;

    if (batch->scale_loc < 0)
        return 6;

    glUseProgram(batch->program);
    glGenVertexArrays(1, &batch->vao);
    glGenBuffers(1, &batch->vbo);
    glGenBuffers(1, &batch->ebo);

    glBindVertexArray(batch->vao);
    glBindBuffer(GL_ARRAY_BUFFER, batch->vbo);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
        8 * sizeof(GLfloat), (const GLvoid*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
        8 * sizeof(GLfloat), (const GLvoid*)(2 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE,
        8 * sizeof(GLfloat), (const GLvoid*)(4 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBufferData(GL_ARRAY_BUFFER,
        batch->buffer_size * FLOATS_PER_SPRITE * sizeof(GLfloat),
        batch->vertex_buffer, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch->ebo);
    GLuint indices[6] = {0, 1, 2, 2, 3, 1};
    GLuint *index_data = (GLuint*)renderMalloc(BATCH_SIZE * 6 * sizeof(GLuint));
    for (int i = 0; i < BATCH_SIZE; ++i)
        for (int j = 0; j < 6; ++j)
            index_data[i * 6 + j] = indices[j] + i * 4;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 BATCH_SIZE * 6 * sizeof(GLuint),
                 index_data,
                 GL_DYNAMIC_DRAW);
    renderFree(index_data);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);

    /* Create the blank texture */
    if (_BLANK_WHITE_TEXTURE->id == 0)
    {
        GLubyte blank_pixels[16];

        for (GLubyte *b = blank_pixels; b < &blank_pixels[16]; ++b)
            *b = 255;

        Texture_init(_BLANK_WHITE_TEXTURE,
            GL_TEXTURE_2D, blank_pixels,
            GL_RGBA, GL_UNSIGNED_BYTE,
            GL_REPEAT, GL_REPEAT, 2, 2);
    }

    return 0;
};

void
SpriteBatch_allocateMore(SpriteBatch *batch)
{
    DEBUG_PRINTF("SpriteBatch: allocating more space.\n");

    unsigned int new_size = (unsigned int)((float)batch->buffer_size * 1.05f);
    if (new_size <= batch->buffer_size)
        new_size = batch->buffer_size + 1;

    void *vertex_buf  = renderMalloc(new_size * FLOATS_PER_SPRITE* sizeof(GLfloat));
    void *tex_buf     = renderMalloc(new_size * FLOATS_PER_SPRITE * sizeof(TextureBufferItem));

    memcpy(vertex_buf, batch->vertex_buffer,
        batch->buffer_size * FLOATS_PER_SPRITE * sizeof(GLfloat));
    memcpy(tex_buf, batch->texture_buffer,
        batch->buffer_size * sizeof(TextureBufferItem));

    renderFree(batch->vertex_buffer);
    renderFree(batch->texture_buffer);

    batch->vertex_buffer    = (GLfloat*)vertex_buf;
    batch->texture_buffer   = (TextureBufferItem*)tex_buf;

    batch->buffer_size = new_size;
}

void
SpriteBatch_dispose(SpriteBatch *batch)
{
    renderFree(batch->vertex_buffer);
    renderFree(batch->texture_buffer);
    glDeleteBuffers(1, &batch->vbo);
    glDeleteProgram(batch->program);
}

void
SpriteBatch_flush(SpriteBatch *batch, int *vp)
{
    if (batch->sprite_count < 1)
        return;

    glDisable(GL_DEPTH_TEST);
    glUseProgram(batch->program);

    /* Set up the projection matrix by screen dimensions */
    GLint viewport[4];

    if (vp)
    {
        viewport[0] = vp[0];
        viewport[1] = vp[1];
        viewport[2] = vp[2];
        viewport[3] = vp[3];
    }
    else
    {
        glGetIntegerv(GL_VIEWPORT, viewport);
    }

    GLfloat projection[4][4] =
    {
        {2.0f / (GLfloat)(viewport[2] - viewport[0]), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (GLfloat)(viewport[1] - viewport[3]), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {- (GLfloat)(viewport[2] + viewport[0]) / (GLfloat)(viewport[2] - viewport[0]),
         - (GLfloat)(viewport[1] + viewport[3]) / (GLfloat)(viewport[1] - viewport[3]),
         0.0f, 1.0f}
    };

    glUniformMatrix4fv(batch->proj_loc, 1, GL_FALSE, &projection[0][0]);

    glUniform2f(batch->scale_loc, batch->scale_x, batch->scale_y);

    /* Buffer data */
    glBindVertexArray(batch->vao);
    glBindBuffer(GL_ARRAY_BUFFER, batch->vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch->ebo);
    glActiveTexture(GL_TEXTURE0);

    batch->proj_loc    = glGetUniformLocation(batch->program, "p");
    batch->scale_loc   = glGetUniformLocation(batch->program, "scale");

    unsigned int num_batches = batch->sprite_count / BATCH_SIZE + 1;
    unsigned int sprites_in_batch;
    unsigned int batch_sprites_drawn;
    unsigned int tex_buf_offset = 0;
    unsigned int repeats;
    GLuint last_texture = batch->texture_buffer[0].id;
    GLuint cur_texture;
    glBindTexture(GL_TEXTURE_2D, last_texture);

    for (unsigned int batch_num = 0; batch_num < num_batches; ++batch_num)
    {
        sprites_in_batch = batch->sprite_count - batch_num * BATCH_SIZE;
        if (sprites_in_batch > BATCH_SIZE) sprites_in_batch = BATCH_SIZE;

        batch_sprites_drawn = 0;

        glBufferSubData(GL_ARRAY_BUFFER,                                                    /* Target */
                        0,                                                                  /* Offset */
                        sprites_in_batch * FLOATS_PER_SPRITE * sizeof(GLfloat),             /* Size */
                        &batch->vertex_buffer[batch_num * BATCH_SIZE * FLOATS_PER_SPRITE]); /* Data */

        while (batch_sprites_drawn < sprites_in_batch)
        {
            /* Bind the correct texture */
            cur_texture = batch->texture_buffer[tex_buf_offset].id;

            if (cur_texture != last_texture)
            {
                glBindTexture(GL_TEXTURE_2D, batch->texture_buffer[tex_buf_offset].id);
                last_texture = cur_texture;
            }

            /* Draw the amount of repeats suggested by the texture buffer item */
            repeats = batch->texture_buffer[tex_buf_offset].repeats + 1;

            if (batch_sprites_drawn + repeats > sprites_in_batch)
            {
                repeats = sprites_in_batch - batch_sprites_drawn;
                batch->texture_buffer[tex_buf_offset].repeats -= repeats;
            }
            else
            {
                ++tex_buf_offset;
            }

            glDrawElements(GL_TRIANGLES,    /* mode */
                           repeats * 6,		/* count */
                           GL_UNSIGNED_INT,	/* type */
                           (GLvoid*)(batch_sprites_drawn * 6 * sizeof(GLuint))); /* indices*/
            batch_sprites_drawn += repeats;
        }
    }

    batch->tex_swap_count = 0;
    batch->sprite_count   = 0;

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);
}

void
SpriteBatch_flushMultipleBuffers(SpriteBatch *batch, SpriteBufferList *buffers, int *vp)
{
    glDisable(GL_DEPTH_TEST);
    glUseProgram(batch->program);

    /* Set up the projection matrix by screen dimensions */
    GLint viewport[4];

    if (vp)
    {
        viewport[0] = vp[0];
        viewport[1] = vp[1];
        viewport[2] = vp[2];
        viewport[3] = vp[3];
    }
    else
    {
        glGetIntegerv(GL_VIEWPORT, viewport);
    }

    GLfloat projection[4][4] =
    {
        {2.0f / (GLfloat)(viewport[2] - viewport[0]), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (GLfloat)(viewport[1] - viewport[3]), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {- (GLfloat)(viewport[2] + viewport[0]) / (GLfloat)(viewport[2] - viewport[0]),
         - (GLfloat)(viewport[1] + viewport[3]) / (GLfloat)(viewport[1] - viewport[3]),
         0.0f, 1.0f}
    };

    glUniformMatrix4fv(batch->proj_loc, 1, GL_FALSE, &projection[0][0]);

    glUniform2f(batch->scale_loc, batch->scale_x, batch->scale_y);

    /* Buffer data */
    glBindVertexArray(batch->vao);
    glBindBuffer(GL_ARRAY_BUFFER, batch->vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch->ebo);
    glActiveTexture(GL_TEXTURE0);

    batch->proj_loc    = glGetUniformLocation(batch->program, "p");
    batch->scale_loc   = glGetUniformLocation(batch->program, "scale");

    unsigned int num_batches;
    unsigned int sprites_in_batch;
    unsigned int batch_sprites_drawn;
    unsigned int tex_buf_offset;
    unsigned int repeats;
    GLuint last_texture = 0;
    GLuint cur_texture;

    SpriteBuffer *buffer;

    for (SpriteBufferList *buf_item = buffers;
         buf_item;
         buf_item = buf_item->next)
    {
        buffer = &buf_item->buffer;

        if (buffer->sprite_count <= 0)
            continue;

        if (buffer->texture_buffer[0].id != last_texture)
        {
            last_texture = buffer->texture_buffer[0].id;
            glBindTexture(GL_TEXTURE_2D, last_texture);
        }

        num_batches = buffer->sprite_count / BATCH_SIZE + 1;
        tex_buf_offset = 0;

        for (unsigned int batch_num = 0; batch_num < num_batches; ++batch_num)
        {
            sprites_in_batch = buffer->sprite_count - batch_num * BATCH_SIZE;
            if (sprites_in_batch > BATCH_SIZE) sprites_in_batch = BATCH_SIZE;

            batch_sprites_drawn = 0;

            glBufferSubData(GL_ARRAY_BUFFER,                                                    /* Target */
                            0,                                                                  /* Offset */
                            sprites_in_batch * FLOATS_PER_SPRITE * sizeof(GLfloat),             /* Size */
                            &buffer->vertex_buffer[batch_num * BATCH_SIZE * FLOATS_PER_SPRITE]); /* Data */

            while (batch_sprites_drawn < sprites_in_batch)
            {
                /* Bind the correct texture */
                cur_texture = buffer->texture_buffer[tex_buf_offset].id;

                if (cur_texture != last_texture)
                {
                    glBindTexture(GL_TEXTURE_2D, buffer->texture_buffer[tex_buf_offset].id);
                    last_texture = cur_texture;
                }

                /* Draw the amount of repeats suggested by the texture buffer item */
                repeats = buffer->texture_buffer[tex_buf_offset].repeats + 1;

                if (batch_sprites_drawn + repeats > sprites_in_batch)
                {
                    repeats = sprites_in_batch - batch_sprites_drawn;
                    batch->texture_buffer[tex_buf_offset].repeats -= repeats;
                }
                else
                {
                    ++tex_buf_offset;
                }

                glDrawElements(GL_TRIANGLES,    /* mode */
                               repeats * 6,		/* count */
                               GL_UNSIGNED_INT,	/* type */
                               (GLvoid*)(batch_sprites_drawn * 6 * sizeof(GLuint))); /* indices*/
                batch_sprites_drawn += repeats;
            }
        }
    }


    batch->tex_swap_count = 0;
    batch->sprite_count   = 0;

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);
}

void
SpriteBatch_drawSprite(SpriteBatch *batch, Texture *texture,
                       float x, float y, unsigned int *clip)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION(batch, x, y, clip);
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS(batch, texture, clip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(batch, texture, x, y);
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(batch);
    }

    SET_SPRITE_DEFAULT_COLOR(batch);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawSprite_Flip(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, SpriteFlip flip)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION(batch, x, y, clip);
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS(batch, texture, clip, flip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(batch, texture, x, y);
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS(batch, flip);
    }

    SET_SPRITE_DEFAULT_COLOR(batch);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawSprite_Scale(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float scale_x, float scale_y)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION(batch, x, y, clip, scale_x, scale_y);
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS(batch, texture, clip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION(batch, texture, x, y, scale_x, scale_y);
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(batch);
    }

    SET_SPRITE_DEFAULT_COLOR(batch);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawSprite_Rotate(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float angle)
{

    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    GLfloat origin_x;
    GLfloat origin_y;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION(batch, x, y, clip);
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS(batch, texture, clip);
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(batch, texture, x, y);
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(batch);
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)texture->width/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE(batch, origin_x, origin_y);
    SET_SPRITE_DEFAULT_COLOR(batch);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}



void
SpriteBatch_drawSprite_Color(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float *color)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION(batch, x, y, clip);
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS(batch, texture, clip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(batch, texture, x, y);
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(batch);
    }

    SET_SPRITE_MOD_COLOR(batch, color);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawSprite_FlipScale(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip,SpriteFlip flip, float scale_x, float scale_y)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION(batch, x, y, clip, scale_x, scale_y);
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS(batch, texture, clip, flip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION(batch, texture, x, y, scale_x, scale_y);
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS(batch, flip);
    }

    SET_SPRITE_DEFAULT_COLOR(batch);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawSprite_FlipRotate(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, SpriteFlip flip, float angle)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    GLfloat origin_x;
    GLfloat origin_y;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION(batch, x, y, clip);
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS(batch, texture, clip, flip);
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(batch, texture, x, y);
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS(batch, flip);
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)texture->width/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE(batch, origin_x, origin_y);
    SET_SPRITE_DEFAULT_COLOR(batch);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawSprite_FlipColor(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, SpriteFlip flip, float *color)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION(batch, x, y, clip);
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS(batch, texture, clip, flip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(batch, texture, x, y);
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS(batch, flip);
    }

    SET_SPRITE_MOD_COLOR(batch, color);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawSprite_ScaleRotate(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float scale_x, float scale_y, float angle)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    GLfloat origin_x;
    GLfloat origin_y;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION(batch, x, y, clip, scale_x, scale_y);
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS(batch, texture, clip);
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION(batch, texture, x, y, scale_x, scale_y);
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(batch);
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)texture->width/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE(batch, origin_x, origin_y);
    SET_SPRITE_DEFAULT_COLOR(batch);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawSprite_ScaleColor(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float scale_x, float scale_y,
    float *color)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION(batch, x, y, clip, scale_x, scale_y);
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS(batch, texture, clip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION(batch, texture, x, y, scale_x, scale_y);
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(batch);
    }

    SET_SPRITE_MOD_COLOR(batch, color);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

inline void
SpriteBatch_drawSprite_FlipScaleRotate(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip,
    SpriteFlip flip, float scale_x, float scale_y, float angle)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    GLfloat origin_x;
    GLfloat origin_y;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION(batch, x, y, clip, scale_x, scale_y);
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS(batch, texture, clip, flip);
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION(batch, texture, x, y, scale_x, scale_y);
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS(batch, flip);
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)texture->width/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE(batch, origin_x, origin_y);
    SET_SPRITE_DEFAULT_COLOR(batch);
    SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture);
}

void
SpriteBatch_drawRect(SpriteBatch *batch,
    float x, float y, float w, float h,
    float *color)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    batch->vertex_buffer[index +  0] = (GLfloat)x;
    batch->vertex_buffer[index +  1] = (GLfloat)y;
    batch->vertex_buffer[index +  8] = (GLfloat)(x + w);
    batch->vertex_buffer[index +  9] = (GLfloat)y;
    batch->vertex_buffer[index + 16] = (GLfloat)x;
    batch->vertex_buffer[index + 17] = (GLfloat)(y + h);
    batch->vertex_buffer[index + 24] = (GLfloat)(x + w);
    batch->vertex_buffer[index + 25] = (GLfloat)(y + h);

    SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(batch);

    if (color)
    {
        SET_SPRITE_MOD_COLOR(batch, color);
    }
    else
    {
        SET_SPRITE_DEFAULT_COLOR(batch);
    }

    SPRITE_DRAW_FUNC_LOWER_HALF(batch, _BLANK_WHITE_TEXTURE);
}
