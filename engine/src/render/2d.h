#include "../core/utils.h"
#include "renderer.h"

#define SPRITE_GROUP_SUB_ARRAY_SIZE 256

typedef struct Sprite       Sprite;
typedef struct SpriteData   SpriteData;
typedef struct SpriteGroup  SpriteGroup;

struct Sprite
{
    uint32_t    index;
    uint32_t    data_index;
    SpriteGroup *group;
    Sprite      *prev, *next;
};

ARRAY_LIST_DECLARATION(Sprite);

struct SpriteData
{
    uint32_t    sprite_index;
    Texture     *texture;
    vec2        pos;
    SpriteFlip  flip;
    vec2        scale;
    float       rot;
    vec4        color;
    short       layer;
    bool32      visible;
};

VECTOR_DECLARATION(SpriteData);

struct SpriteGroup
{
    ArrayList_Sprite    sprites;
    Vector_SpriteData   sprite_data;
    Sprite              *free;
    Sprite              *reserved;
    bool32              need_sort;
    GLint               viewport[4];
    bool32              use_viewport_override;
    GLfloat             background_color[4];
};

int
SpriteGroup_init(SpriteGroup *group, size_t num_initial_sprites,
    DynamicMemoryBlock *block);

/* Passing 0 as the viewport disables viewport override */
void
SpriteGroup_setViewport(SpriteGroup *group, Viewport viewport);

Sprite *
SpriteGroup_reserve(SpriteGroup *group);

void
Sprite_free(Sprite *sprite);

static inline SpriteData *
Sprite_getData(Sprite *sprite);

static inline void
Sprite_setTexture(Sprite *sprite, Texture *texture);

static inline void
Sprite_setVisibility(Sprite *sprite, bool32 visible);

static inline void
Sprite_setLayer(Sprite *sprite, short layer);

static inline short
Sprite_getLayer(Sprite *sprite);

static inline void
Sprite_setPosition(Sprite *sprite, float x, float y);

static inline void
Sprite_setScale(Sprite *sprite, float x, float y);

static inline void
Sprite_setColor(Sprite *sprite, float r, float b, float g, float alpha);

static inline void
Sprite_setAlpha(Sprite *sprite, float alpha);

static inline float *
Sprite_getPosition(Sprite *sprite);

static inline float *
Sprite_getScale(Sprite *sprite);

static inline float *
Sprite_getColor(Sprite *sprite);

static inline float
Sprite_getAlpha(Sprite *sprite);

static inline SpriteData *
Sprite_getData(Sprite *sprite)
{
    return &sprite->group->sprite_data.items[sprite->data_index];
}

static inline void
Sprite_setVisibility(Sprite *sprite, bool32 visible)
{
    SpriteData *data = Sprite_getData(sprite);
    data->visible = visible;
}

static inline void
Sprite_setTexture(Sprite *sprite, Texture *texture)
{
    SpriteData *data = Sprite_getData(sprite);
    data->texture = texture;
}

static inline void
Sprite_setLayer(Sprite *sprite, short layer)
{
    SpriteData *data = Sprite_getData(sprite);
    data->layer = layer;
    sprite->group->need_sort = 1;
}

static inline short
Sprite_getLayer(Sprite *sprite)
{
    SpriteData *data = Sprite_getData(sprite);
    return data->layer;
}

static inline void
Sprite_setPosition(Sprite *sprite, float x, float y)
{
    SpriteData *data = Sprite_getData(sprite);
    data->pos[0] = x;
    data->pos[1] = y;
}

static inline void
Sprite_setScale(Sprite *sprite, float x, float y)
{
    SpriteData *data = Sprite_getData(sprite);
    data->scale[0] = x;
    data->scale[1] = y;
}

static inline void
Sprite_setColor(Sprite *sprite, float r, float g, float b, float a)
{
    SpriteData *data = Sprite_getData(sprite);
    data->color[0] = r;
    data->color[1] = g;
    data->color[2] = b;
    data->color[3] = a;
}

static inline void
Sprite_setAlpha(Sprite *sprite, float alpha)
{
    SpriteData *data = Sprite_getData(sprite);
    data->color[3] = alpha;
}

static inline float *
Sprite_getPosition(Sprite *sprite)
{
    SpriteData *data = Sprite_getData(sprite);
    return data->pos;
}

static inline float *
Sprite_getScale(Sprite *sprite)
{
    SpriteData *data = Sprite_getData(sprite);
    return data->scale;
}

static inline float *
Sprite_getColor(Sprite *sprite)
{
    SpriteData *data = Sprite_getData(sprite);
    return data->color;
}

static inline float
Sprite_getAlpha(Sprite *sprite)
{
    SpriteData *data = Sprite_getData(sprite);
    return data->color[3];
}
