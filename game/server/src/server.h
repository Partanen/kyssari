#pragma once
#include "../../../engine/src/core/engine.h"
#include "../../shared/shared.h"
#include <limits.h>
#include <time.h>

#define PERMANENT_MEMORY_AMOUNT MEGABYTES(256)
#define DYNAMIC_MEMORY_AMOUNT   MEGABYTES(64)
#define MAX_PLAYERS_PER_GAME    4
#define GAME_SCENE_MEMORY_SIZE  MEGABYTES(190)
#define MAX_ENTITIES_LEVEL_1    2048
#define MAX_OUTPUT_BUFFER_ITEMS 256
#define MAX_SIM_TASKS           128
#define SERVER_TICK_RATE        128
#define SPAWN_POINT_RE_USE_TIME 5000 /* Milliseconds */

#define MAX_SPAWN_POINTS_PER_LEVEL  32
#define NUM_SPAWN_POINTS_LEVEL1     5
#define NUM_ITEMS_LEVEL1            4

#if NUM_SPAWN_POINTS_LEVEL1 > MAX_SPAWN_POINTS_PER_LEVEL
#   pragma error Too many spawn points specified.
#endif

#if NUM_ITEMS_LEVEL1 > MAX_ITEMS_PER_LEVEL
#   pragma error Too many item spawns specified
#endif

#define LOAD_INGAME_TEXTURE(_name, _path) \
    AssetPool_loadTexture(&ks_ingame_assets, _name, _path)

#define GET_INGAME_TEXTURE(_name) \
    AssetPool_getTexture(&ks_ingame_assets, _name)

#define LOAD_INGAME_MODEL(_name, _path) \
    AssetPool_loadModel(&ks_ingame_assets, _name, _path)

#define GET_INGAME_MODEL(_name) \
    AssetPool_getModel(&ks_ingame_assets, _name)

#define LOAD_INGAME_MATERIAL(_name, _diff, _spec, _r, _g, _b, _shine) \
    AssetPool_loadMaterial(&ks_ingame_assets, _name, _diff, _spec, \
        _r, _g, _b, _shine)

#define GET_INGAME_MATERIAL(_name) \
    AssetPool_getMaterial(&ks_ingame_assets, _name)

typedef struct MainMenu             MainMenu;
typedef struct InputExecutionBuffer InputExecutionBuffer;
typedef struct WeaponType           WeaponType;
typedef struct ItemSpawnScriptData  ItemSpawnScriptData;
typedef struct PlayerSnapshot       PlayerSnapshot;
typedef struct PlayerSnapshotBuffer PlayerSnapshotBuffer;
typedef struct Player               Player;
typedef struct SimQueue             SimQueue;
typedef struct OutputBuffer         OutputBuffer;
typedef struct Game                 Game;
typedef struct BulletScriptData     BulletScriptData;

enum PlayerState
{
    PLAYER_STATE_LOADING = 0,
    PLAYER_STATE_LOADED
};

struct MainMenu
{
    const char *title_str;

    struct MainMenuTextModeData
    {
        WINDOW      *network_win;
        WINDOW      *help_win;
        bool32      show_help_win;
        int         term_width, term_height;
    } text_mode;
};

struct InputExecutionBuffer
{
    ClientInput         items[MAX_INPUT_BUFFER_ITEMS];
    int                 first;
    int                 num;
    input_sequence_t    latest_applied_input;
    Mutex               mutex;
};

struct PlayerSnapshot
{
    uint32_t    timestamp;
    vec3        pos;
};

struct PlayerSnapshotBuffer
{
    PlayerSnapshot  items[32];
    uint            num;
};

struct Player
{
    bool32                  reserved;
    uint32_t                id;
    ServerClient            *client;
    enum PlayerState        state;
    Entity                  *pawn;
    InputExecutionBuffer    input_buffer;
    kb_state_t              kb_state;
    float                   orientation;
    int                     num_frags;

    /* Manipulated on the networking thread */
    double                  shoot_cooldown_timer;

    Mutex                   latest_input_mutex;
    input_sequence_t        latest_verified_input;
    vec3                    latest_verified_position;
    vec3                    latest_verified_velocity;
    bool32                  have_new_verified_state;
    int                     health;

    /* Following 3 not verified to the client itself,
     * but sent to other players */
    enum KsWeaponType       latest_weapon;
    int                     latest_movement_state;
    float                   latest_orientation;
    char                    character_type;
};

struct OutputBuffer
{
    struct OutputItem
    {
        ServerClient    *client;
        message_t       type;
        void            *data;
        int             size;
    } items[MAX_OUTPUT_BUFFER_ITEMS];

    uint                num_items;
    SimpleMemoryBlock   memory;
    Mutex               mutex;
};

struct PlayerScriptData
{
    BasePlayerScriptData base;
};

typedef struct
{
    float               timer;
    replication_id32_t  shooter_rid;
} ExplosionScriptData;

struct WeaponType
{
    BaseWeaponType base;
};

struct ItemSpawnScriptData
{
    #define ITEM_IS_SPAWNED 0
    int         index;
    uint32_t    cooldown;
    uint32_t    next_spawn_time; /* If 0, item is spawned */
};

struct SimQueue
{
    Mutex   mutex;
    uint    num_tasks;

    struct
    {
        enum
        {
            SIM_REMOVE_ENTITY,
            SIM_SHOOT_ROCKET,
            SIM_SHOOT_SMG,
            SIM_SHOOT_RAILGUN
        } type;

        union
        {
            struct
            {
                Entity      *entity;
                uint32_t    id;
            } remove_entity;

            struct
            {
                replication_id32_t  shooter_rid;
                vec3                pos;
                vec3                dir;
            } shoot_something;
        } data;
    } tasks[MAX_SIM_TASKS];
};

typedef struct KsItemSpawnDefinition
{
    enum KsItemType type;
    uint32_t        cooldown;
    vec3            pos;
} KsItemSpawnDefinition;

extern struct GameData
{
    vec3 spawn_points_level1[NUM_SPAWN_POINTS_LEVEL1];
    KsItemSpawnDefinition items_level1[NUM_ITEMS_LEVEL1];
} game_data;

struct Game
{
    bool32                  running;
    uint32_t                running_player_id;
    enum KsMapId            map_id;
    Scene3D                 scene;
    Player                  *active_players[MAX_PLAYERS_PER_GAME];
    Player                  players[MAX_PLAYERS_PER_GAME];
    PlayerSnapshotBuffer    player_snapshots[MAX_PLAYERS_PER_GAME];
    uint                    num_players;
    void                    *scene_memory;
    size_t                  scene_memory_size;
    Clock                   simulation_clock;
    Thread                  simulation_thread;
    Mutex                   player_access_mutex;
    SimQueue                sim_queue;
    OutputBuffer            output_buffer;

    uint32_t                spawn_point_timers[MAX_SPAWN_POINTS_PER_LEVEL];
    Entity                  *item_spawns[MAX_ITEMS_PER_LEVEL];
};

struct BulletScriptData
{
    float               timer; // make this zero
    float               dist_to_hit;
    float               bullet_velocity;
    replication_id32_t  shooter_rid;
    vec3                explosion_pos;
    vec3                last_position;
};

#define GET_PLAYER_INDEX(_g_ptr, _p_ptr) \
    ((int)(((char*)(_p_ptr) - (char*)(_g_ptr)->players)) / sizeof(Player))

#define GET_PLAYER_SNAPSHOTS(_g_ptr, _p_ptr) \
    (&((_g_ptr)->player_snapshots[GET_PLAYER_INDEX((_g_ptr), (_p_ptr))]))

extern MainMenu     main_menu;
extern Server       ks_server;
extern ServerGame   *ks_server_game;
extern Screen       *ks_main_menu_screen;
extern Game         ks_game;
extern AssetPool    ks_ingame_assets;
extern WeaponType   weapon_types[NUM_BASE_WEAPON_TYPES];
extern int          input_diff_tolerance;
extern int          input_diff_rollback;

extern int          network_tick_rate;

#define GET_WEAPON_TYPE(_name)(&weapon_types[_name])

/* Load game data, initialize default scene, etc. */
int
ks_init();

void
ks_quit();

/* Begin accepting connections to the lobby */
int
ks_beginNetworking();

int
ks_quitNetworking();

void
ks_loadIngameAssets();

void
InputExecutionBuffer_init(InputExecutionBuffer *ieb);

static inline void
InputExecutionBuffer_clear(InputExecutionBuffer *ieb);

static inline void
InputExecutionBuffer_queueMovementInputUnsafe(InputExecutionBuffer *ieb,
    input_sequence_t sequence, kb_state_t kb_state, float orientation);

void
SimQueue_init(SimQueue *q);

static inline void
SimQueue_queueRemoveEntityTask(SimQueue *q, Entity *e);

static inline void
SimQueue_queueShootRocketTask(SimQueue *q, replication_id32_t shooter_rid,
    vec3 pos, vec3 dir);

static inline void
SimQueue_queueShootSMGTask(SimQueue *q, replication_id32_t shooter_rid,
    vec3 pos, vec3 dir);

static inline void
SimQueue_queueShootRailgunTask(SimQueue *q, replication_id32_t shooter_rid,
    vec3 pos, vec3 dir);

void
SimQueue_executeContents(SimQueue *q, Game *game);

void
SimQueue_init(SimQueue *q);

int
OutputBuffer_init(OutputBuffer *ob);

void *
OutputBuffer_queueMessageUnsafe(OutputBuffer *ob, ServerClient *client,
    message_t type, int size);

void
OutputBuffer_sendAllContents(OutputBuffer *ob);

void
GameData_init();

int
Game_init(Game *game);

int
Game_start(Game *game);

void
Game_stop(Game *game);

int
Game_attachPlayer(Game *game, ServerClient *client);

int
Game_removePlayer(Game *game, Player *p);

void
Game_getRandomSpawnPoint(Game *game, vec3 ret);

/* Note: when run on the main thread, must have claimed a lock of the
 * player access mutex of the game */
void
Game_killPlayersPawn(Game *game, Player *p, replication_id32_t killer);

bool32
Game_checkPlayerShotPositionValidity(Game *g, Player *player, vec3 pos);

int
initSceneForLevel1(Scene3D *scene, void *memory, size_t memory_size);

int
initMainMenu();

void
updateAndRenderMainMenu(Screen *screen);

void
updateAndRenderMainMenuTextMode(Screen *screen);

void
server_initPlayerPawn(Entity *e);

void
server_shootRocket(replication_id32_t shooter, vec3 player_pos, vec3 forward,
    Game *game);

void
server_shootSMG(replication_id32_t rid, vec3 player_pos, vec3 forward, Game *game);

void
server_shootRailgun(replication_id32_t rid, vec3 player_pos, vec3 forward, Game *game);

void
server_rocketSkyBulletScript(Entity *entity, void *scriptdata);

void
server_rocketStaticColliderBulletScript(Entity *entity, void *scriptdata);

void
server_medkitScript(Entity *e, void *data_ptr);

void
server_rocketAmmoScript(Entity *e, void *data_ptr);

void
server_SMGAmmoScript(Entity *e, void *data_ptr);

void
server_railgunAmmoScript(Entity *e, void *data_ptr);

void
server_medkitCollisionCallback(PhysicsComponent *pca, PhysicsComponent *pcb,
    Entity *ea, Entity *eb,
    enum ColliderType cta, enum ColliderType ctb,
    enum CollisionEventType event_type,
    uint32_t collider_a, uint32_t collider_b);

void
server_rocketAmmoCollisionCallback(PhysicsComponent *pca, PhysicsComponent *pcb,
    Entity *ea, Entity *eb,
    enum ColliderType cta, enum ColliderType ctb,
    enum CollisionEventType event_type,
    uint32_t collider_a, uint32_t collider_b);

void
server_SMGCollisionCallback(PhysicsComponent *pca, PhysicsComponent *pcb,
    Entity *ea, Entity *eb,
    enum ColliderType cta, enum ColliderType ctb,
    enum CollisionEventType event_type,
    uint32_t collider_a, uint32_t collider_b);

void
server_railgunCollisionCallback(PhysicsComponent *pca, PhysicsComponent *pcb,
    Entity *ea, Entity *eb,
    enum ColliderType cta, enum ColliderType ctb,
    enum CollisionEventType event_type,
    uint32_t collider_a, uint32_t collider_b);

Entity *
server_createMedkitEntity(Scene3D *scene, int index, uint32_t respawn_time,
    float x, float y, float z);

Entity *
server_createRocketAmmoEntity(Scene3D *scene, int index, uint32_t respawn_time,
    float x, float y, float z);

Entity *
server_createSMGAmmoEntity(Scene3D *scene, int index, uint32_t respawn_time,
    float x, float y, float z);

Entity *
server_createRailgunAmmoEntity(Scene3D *scene, int index, uint32_t respawn_time,
    float x, float y, float z);

/* Note: player access mutex must be locked before calling. Called from the
 * simulation thread. */
int
Game_informPlayersOfRocketShot(Game *game, Player *shooter, vec3 pos, vec3 dir);

int
Game_informPlayersOfSMGShot(Game *game, Player *shooter, vec3 pos, vec3 dir);

/* Note: player access mutex must be locked before calling. Called from the
 * simulation thread. */
int
Game_informPlayersOfRocketExplosion(Game *game, replication_id32_t shooter,
    vec3 pos);

/* Does not lock any mutexes, returns the new health amount */
int
Game_dealDamageToPlayer(Game *game, Player *p, replication_id32_t killer,
    int damage);

void
server_rocketExplosionScript(Entity *entity, void *scriptdata);

void
server_createRocketExplosionEntity(Scene3D *scene, vec3 pos,
    replication_id32_t shooter_rid);

static inline void
InputExecutionBuffer_clear(InputExecutionBuffer *ieb)
{
    Mutex_lock(&ieb->mutex);
    ieb->first                  = 0;
    ieb->num                    = 0;
    ieb->latest_applied_input   = USHRT_MAX;
    Mutex_unlock(&ieb->mutex);
}

static inline void
InputExecutionBuffer_queueMovementInputUnsafe(InputExecutionBuffer *ieb,
    input_sequence_t sequence, kb_state_t kb_state, float orientation)
{
    if (ieb->num >= MAX_INPUT_BUFFER_ITEMS)
    {
        DEBUG_PRINTF("InputExecutionBuffer full!\n");
        return;
    }

    int index = (ieb->first + ieb->num) % (int)MAX_INPUT_BUFFER_ITEMS;

    ClientInput *inp                = &ieb->items[index];
    inp->sequence                   = sequence;
    inp->type                       = INPUT_MOVEMENT;
    inp->data.movement.kb_state     = kb_state;
    inp->data.movement.orientation  = orientation;

    ++ieb->num;
}

static inline void
SimQueue_queueRemoveEntityTask(SimQueue *q, Entity *e)
{
    if (q->num_tasks >= MAX_SIM_TASKS) return;
    Mutex_lock(&q->mutex);
    q->tasks[q->num_tasks].type                         = SIM_REMOVE_ENTITY;
    q->tasks[q->num_tasks].data.remove_entity.entity    = e;
    q->tasks[q->num_tasks].data.remove_entity.id        = e->id;
    ++q->num_tasks;
    Mutex_unlock(&q->mutex);
}

static inline void
SimQueue_queueShootRocketTask(SimQueue *q, replication_id32_t shooter_rid,
    vec3 pos, vec3 dir)
{
    if (q->num_tasks >= MAX_SIM_TASKS) return;

    Mutex_lock(&q->mutex);
    q->tasks[q->num_tasks].type = SIM_SHOOT_ROCKET;
    q->tasks[q->num_tasks].data.shoot_something.shooter_rid = shooter_rid;
    vec3_copy(q->tasks[q->num_tasks].data.shoot_something.pos, pos);
    vec3_copy(q->tasks[q->num_tasks].data.shoot_something.dir, dir);
    ++q->num_tasks;
    Mutex_unlock(&q->mutex);
}

static inline void
SimQueue_queueShootSMGTask(SimQueue *q, replication_id32_t shooter_rid,
    vec3 pos, vec3 dir)
{
    if (q->num_tasks >= MAX_SIM_TASKS) return;

    Mutex_lock(&q->mutex);

    q->tasks[q->num_tasks].type = SIM_SHOOT_SMG;
    q->tasks[q->num_tasks].data.shoot_something.shooter_rid = shooter_rid;
    vec3_copy(q->tasks[q->num_tasks].data.shoot_something.pos, pos);
    vec3_copy(q->tasks[q->num_tasks].data.shoot_something.dir, dir);
    ++q->num_tasks;

    Mutex_unlock(&q->mutex);
}

static inline void
SimQueue_queueShootRailgunTask(SimQueue *q, replication_id32_t shooter_rid,
    vec3 pos, vec3 dir)
{
    if (q->num_tasks >= MAX_SIM_TASKS) return;

    Mutex_lock(&q->mutex);

    q->tasks[q->num_tasks].type = SIM_SHOOT_RAILGUN;
    q->tasks[q->num_tasks].data.shoot_something.shooter_rid = shooter_rid;
    vec3_copy(q->tasks[q->num_tasks].data.shoot_something.pos, pos);
    vec3_copy(q->tasks[q->num_tasks].data.shoot_something.dir, dir);
    ++q->num_tasks;

    Mutex_unlock(&q->mutex);
}
