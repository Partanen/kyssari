#include "scene3d.h"
#include "collision.c"
#include "../core/engine.h"
#include "packets.h"

#define DEFAULT_PHYSICS_TIMESTEP (1.0f / 60.0f);

static int
_ReplicationTable_init(ReplicationTable *t, SimpleMemoryBlock *m,
    uint num_entities);

static void
_ReplicationTable_insert(ReplicationTable *table, Entity *entity);

static void
_ReplicationTable_erase(ReplicationTable *table, Entity *entity);

#define COMPONENT_POOL_FUNC_DECLARATION(component_name, component_index, component_bit) \
    static inline void \
    component_name##Pool_init(component_name##Pool *pool, void *memory, size_t num_max) \
    { \
        pool->components = (component_name*)memory; \
        pool->num_max = num_max; \
        pool->num_reserved = 0; \
    }; \
    \
    static inline component_name * \
    component_name##Pool_reserve(component_name##Pool *pool, \
        Entity *entity) \
    { \
        component_name *component = &pool->components[pool->num_reserved]; \
        ++pool->num_reserved; \
        component->entity_index = entity->index; \
        entity->components[component_index] = pool->num_reserved - 1; \
        entity->component_flags |= component_bit; \
        return &pool->components[pool->num_reserved - 1]; \
    }; \
    static inline void \
    component_name##Pool_free(component_name##Pool *pool, \
        component_name *component, Scene3D *scene) \
    { \
        if (component != &pool->components[pool->num_reserved - 1]) \
        { \
            /* Take the array's last component and swap it into the removed component's place.
             * Tell the last component's owning entity the new position of it's component */ \
            component_name *oc  = &pool->components[pool->num_reserved - 1]; \
            Entity *oe          = &scene->entities.entities[oc->entity_index]; \
            oe->components[component_index] = (uint32_t)(((size_t)component - \
                (size_t)pool->components) / sizeof(component_name)); \
            *component = *oc; \
        } \
        --pool->num_reserved; \
    };

COMPONENT_POOL_FUNC_DECLARATION(PhysicsComponent,
    PHYSICS_COMPONENT_INDEX,
    PHYSICS_COMPONENT_BIT);

COMPONENT_POOL_FUNC_DECLARATION(ModelComponent,
    MODEL_COMPONENT_INDEX,
    MODEL_COMPONENT_BIT);

COMPONENT_POOL_FUNC_DECLARATION(DirectionalLightComponent,
    DIRECTIONAL_LIGHT_COMPONENT_INDEX,
    DIRECTIONAL_LIGHT_COMPONENT_BIT);

COMPONENT_POOL_FUNC_DECLARATION(PointLightComponent,
    POINT_LIGHT_COMPONENT_INDEX,
    POINT_LIGHT_COMPONENT_BIT);

COMPONENT_POOL_FUNC_DECLARATION(ScriptComponent,
    SCRIPT_COMPONENT_INDEX,
    SCRIPT_COMPONENT_BIT);

#if _CF_USE_DEFAULT_PHYSICS_COLLISION
void (*handleEntityPhysicsCollision)(CollisionEvent *event) = Entity_collisionPhysics;
#else
void (*handleEntityPhysicsCollision)(CollisionEvent *event) = 0;
#endif

/* Internal */

CollisionWorldNode *
CollisionWorld_buildTree(CollisionWorld *w, CollisionWorldNode *parent,
    uint *num_nodes, vec3 center, float dimensions, int reached_depth)
{
    if (reached_depth < 0) return 0;

    ASSERT(*num_nodes < w->max_nodes);

    CollisionWorldNode *node = &w->nodes[*num_nodes];
    *num_nodes = (*num_nodes) + 1;

    vec3_copy(node->center, center);
    node->dimensions            = dimensions;
    node->parent                = parent;
    node->triangles             = 0;
    node->static_box_colliders  = 0;
    node->flags                 = 0;

#ifdef _DEBUG
    node->depth = (char)reached_depth;
#endif

    vec3 child_center; /* ebin :D */
    float half_width = dimensions * 0.5f;

    for (uint i = 0; i < 8; ++i)
    {
        child_center[0] = center[0] + (((i & 1) ? half_width : -half_width));
        child_center[1] = center[1] + (((i & 2) ? half_width : -half_width));
        child_center[2] = center[2] + (((i & 4) ? half_width : -half_width));

        node->children[i] = CollisionWorld_buildTree(w, node, num_nodes,
            child_center, half_width, reached_depth - 1);
    }

    return node;
}

static int
CollisionWorld_init(CollisionWorld *w,
    SimpleMemoryBlock *mem,
    uint tree_depth,
    float root_half_width,
    uint num_triangles, uint num_static_box_colliders)
{
    uint max_nodes  = 1;
    uint num_leaves = 1;

    for (int i = 0; i < (int)tree_depth; ++i)
    {
        num_leaves = 8 * num_leaves;
        max_nodes += num_leaves;
    }

    w->nodes = SimpleMemoryBlock_malloc(mem,
        max_nodes * sizeof(CollisionWorldNode));
    if (!w->nodes)
    {
        DEBUG_PRINTF("CollisionWorld_init(): failed to allocate nodes. "
            "Free space: %lu, required: %lu\n",
            SimpleMemoryBlock_getFreeSpace(mem),
            max_nodes * sizeof(CollisionWorldNode));
        return 1;
    }

    w->triangles = SimpleMemoryBlock_malloc(mem,
        (num_triangles + 1) * sizeof(CollisionTriangle));
    if (!w->triangles)
    {
        DEBUG_PRINTF("CollisionWorld_init(): failed to allocate triangles.\n");
        return 2;
    }

    w->static_box_colliders = SimpleMemoryBlock_malloc(mem,
        num_static_box_colliders * sizeof(StaticBoxCollider));
    if (!w->static_box_colliders)
    {
        DEBUG_PRINTF("CollisionWorld_init(): failed to allocate static box colliders.\n");
        return 3;
    }

    vec3 root_center    = {0.0f, 0.0f, 0.0f};
    uint num_nodes      = 0;

    w->max_static_box_colliders = num_static_box_colliders + 1;
    w->max_triangles = num_triangles + 1;
    w->num_triangles = 0;
    w->max_nodes     = max_nodes;

    w->tree = CollisionWorld_buildTree(w, 0, &num_nodes, root_center,
        root_half_width, (int)tree_depth);

    return 0;
}

/* Another one from Ericson's book */
static CollisionWorldNode *
CollisionWorld_getNodeByObjectRadius(vec3 center,
    CollisionWorldNode *tree, float radius)
{
    if (!tree->children[0])
        return tree;

    int index       = 0;
    bool32 straddle = 0;

    for (int i = 0; i < 3; ++i)
    {
        float dt = center[i] - tree->center[i];

        if (ABS(dt) <= radius)
        {
            straddle = 1;
            break;
        }

        if (dt > 0.0f) index |= (1 << i);
    }

    if (!straddle)
        return CollisionWorld_getNodeByObjectRadius(center, tree->children[index], radius);
    else
        return tree;
}

static int
CollisionWorld_insertStaticBoxColliders(CollisionWorld *w, float pos_x, float pos_y, float pos_z,
    float dimension_x, float dimension_y, float dimension_z, float rotation_x, float rotation_y, float rotation_z)
{

    if (w->num_static_box_colliders + 1 > w->max_static_box_colliders)
    {
        DEBUG_PRINTF("num static box colliders %u - max static box colliders %u\n",
            w->num_static_box_colliders + 1, w->max_static_box_colliders);
        ASSERT(0);
        return 1;
    }

    OBB3D obb;
    obb.center[0] = pos_x;
    obb.center[1] = pos_y;
    obb.center[2] = pos_z;
    obb.dimensions[0] = dimension_x;
    obb.dimensions[1] = dimension_y;
    obb.dimensions[2] = dimension_z;
    OBB3D_setRotation(&obb, rotation_x, rotation_y, rotation_z);
    

    AABB3D obb_aabb = OBB3D_getAABB3D(&obb);
    AABB3D node_aabb;

    CollisionWorldNode *node = w->tree;
    CollisionWorldNode *child;
    bool32 contains;

    while (1)
    {
        if (!node->children[0]) break;
        contains = 0;

        for (int i = 0; i < 8; ++i)
        {
            child = node->children[i];
            
            CollisionWorldNode_getAABB3D(child, &node_aabb);

            if (AABB3D_containsAABB3D(&node_aabb, &obb_aabb))
            {
                contains = 1;
                node = child;
                break;
            }
        }

        if (!contains) break;
    }

    /* Mark the ancestors of this node to have static colliders */
    if (node->parent && !GET_BITFLAG(node->parent->flags, char,
        CWNODE_CHILDREN_HAVE_STATIC_BOX_COLLIDERS))
    {
        for (CollisionWorldNode *parent = node->parent; 
            parent;
            parent = parent->parent)
        {
            SET_BITFLAG_ON(parent->flags, char, CWNODE_CHILDREN_HAVE_STATIC_BOX_COLLIDERS);
        }
    }

    StaticBoxCollider *sbc      = &w->static_box_colliders[w->num_static_box_colliders++];
    sbc->data                   = obb;
    sbc->next                   = node->static_box_colliders;
    node->static_box_colliders  = sbc;

    return 0;
}

static void
CollisionWorld_insertTriangle(CollisionWorld *w, Triangle *triangle)
{
    if (w->num_triangles + 1 > w->max_triangles)
    {
        DEBUG_PRINTF("num_triangles + 1: %u - max_triangles: %u\n",
            w->num_triangles + 1, w->max_triangles);
        ASSERT(0);
    }

    AABB3D tri_aabb = Triangle_getAABB3D(triangle);
    AABB3D node_aabb;

    CollisionWorldNode *node = w->tree;
    CollisionWorldNode *child;
    bool32 contains;

    while (1)
    {
        if (!node->children[0]) break;

        contains = 0;

        for (int i = 0; i < 8; ++i)
        {
            child = node->children[i];

            CollisionWorldNode_getAABB3D(child, &node_aabb);

            if (AABB3D_containsAABB3D(&node_aabb, &tri_aabb))
            {
                contains = 1;
                node = child;
                break;
            }
        }

        if (!contains) break;
    }

    /* Mark the ancestors of this node to have triangles */
    if (node->parent && !GET_BITFLAG(node->parent->flags, char,
        CWNODE_CHILDREN_HAVE_TRIANGLES))
    {
        for (CollisionWorldNode *parent = node->parent;
             parent;
             parent = parent->parent)
        {
            SET_BITFLAG_ON(parent->flags, char, CWNODE_CHILDREN_HAVE_TRIANGLES);
        }
    }

    CollisionTriangle *t    = &w->triangles[w->num_triangles++];
    t->data                 = *triangle;
    t->next                 = node->triangles;
    node->triangles         = t;
}

#if 0
static void
computeTriangleDistributionPerDepth(CollisionWorldNode *tree, uint *tri_amounts)
{
    for (CollisionTriangle *t = tree->triangles; t; t = t->next)
        ++tri_amounts[(int)tree->depth];

    if (tree->children[0])
    {
        for (CollisionWorldNode **ch = tree->children;
             ch < &tree->children[8];
             ++ch)
        {
            computeTriangleDistributionPerDepth(*ch, tri_amounts);
        }
    }
}
#endif

static int
CollisionWorld_initFromModel(CollisionWorld *world, Model *model,
    SimpleMemoryBlock *mem, vec3 offset, uint tree_depth, uint max_num_static_box_colliders)
{
    uint num_triangles = Model_getNumIndices(model) / 3 + 1;

    float root_half_width = 0.0f;

    vec3 min_dimensions, max_dimensions;
    Model_computeAndGetMinMaxDimensions(model, min_dimensions, max_dimensions);

    for (int i = 0; i < 3; ++i)
    {
        if (max_dimensions[i] > root_half_width)
            root_half_width = max_dimensions[i];
    }

    root_half_width *= 1.05f;

    int ret = CollisionWorld_init(world, mem, tree_depth,
        root_half_width, num_triangles, max_num_static_box_colliders);

    if (ret == 0)
    {
        DEBUG_PRINTF("Initialized CollisionWorld from a model. Number of "
            "triangles: %u, world half width: %f.\n", num_triangles,
            root_half_width);
    }
    else
    {
        DEBUG_PRINTF("Failed to initialize CollisionWorld from a model.\n");
        return ret;
    }

    GLfloat *v1, *v2, *v3;
    Triangle tri;

    for (BaseMesh *m = model->meshes;
         m < &model->meshes[model->num_meshes];
         ++m)
    {
        for (uint i = 0; i < m->num_indices; i += 3)
        {
            if (i + 0 < m->num_indices)
                v1 = &m->vertices[m->indices[(i + 0)] * 8];
            if (i + 1 < m->num_indices)
                v2 = &m->vertices[m->indices[(i + 1)] * 8];
            if (i + 2 < m->num_indices)
                v3 = &m->vertices[m->indices[(i + 2)] * 8];

            tri.points[0][0] = v1[0] + offset[0];
            tri.points[0][1] = v1[1] + offset[1];
            tri.points[0][2] = v1[2] + offset[2];

            tri.points[1][0] = v2[0] + offset[0];
            tri.points[1][1] = v2[1] + offset[1];
            tri.points[1][2] = v2[2] + offset[2];

            tri.points[2][0] = v3[0] + offset[0];
            tri.points[2][1] = v3[1] + offset[1];
            tri.points[2][2] = v3[2] + offset[2];

            vec3_copy(tri.normal, v1 + 5);

            CollisionWorld_insertTriangle(world, &tri);
        }
    }

#if 0
#ifdef _DEBUG
    uint tri_amounts[tree_depth];
    memset(tri_amounts, 0, tree_depth * sizeof(uint));
    computeTriangleDistributionPerDepth(world->tree, tri_amounts);

    for (uint i = 0; i < tree_depth; ++i)
        DEBUG_PRINTF("Triangles at depth %u: %u.\n", i, tri_amounts[i]);
    DEBUG_PRINTF("Note that lower = deeper in the tree\n");
#endif
#endif

    return 0;
}

static int
_ReplicationTable_init(ReplicationTable *t, SimpleMemoryBlock *m,
    uint num_entities)
{
    void *memory = SimpleMemoryBlock_malloc(m,
        num_entities * (sizeof(ReplicationTableEntry) + \
            sizeof(ReplicationTableEntry*)));

    if (!memory) return 1;

    t->all_entries  = (ReplicationTableEntry*)memory;
    t->lookup_table = (ReplicationTableEntry**)((char*)memory + \
        (num_entities * sizeof(ReplicationTableEntry)));
    t->size = num_entities;

    for (ReplicationTableEntry *e = t->all_entries;
         e < &t->all_entries[num_entities];
         ++e)
    {
        e->lookup_prev     = e - 1;
        e->lookup_next     = e + 1;
        e->entity   = 0;
    }

    t->all_entries->lookup_prev                     = 0;
    t->all_entries[num_entities - 1].lookup_next    = 0;
    t->free_entries                                 = t->all_entries;
    t->reserved_entries                             = 0;
    t->num_reserved                                 = 0;

    memset(t->lookup_table, 0, num_entities * sizeof(ReplicationTableEntry*));

    return 0;
}

static void
_ReplicationTable_insert(ReplicationTable *table, Entity *entity)
{
#if _CF_NETWORKING
    ASSERT(entity->replication_id != ENTITY_NOT_REPLICATED);
    ASSERT(table->free_entries);

    uint index = entity->replication_id % table->size;
    /* TODO: CHECK IF ENTITY ALREADY EXISTS!!! */

    /* Reserve new entry */
    ReplicationTableEntry *entry = table->free_entries;
    if (entry->lookup_next) entry->lookup_next->lookup_prev = 0;
    table->free_entries = entry->lookup_next;

    if (table->lookup_table[index])
        table->lookup_table[index]->lookup_prev = entry;

    entry->lookup_next = table->lookup_table[index];
    entry->lookup_prev             = 0;

    /* Add into list of reserveds */
    if (table->reserved_entries)
        table->reserved_entries->reserved_prev = entry;
    entry->reserved_next = table->reserved_entries;
    entry->reserved_prev = 0;
    table->reserved_entries = entry;

    entry->entity   = entity;
    entry->flags    = 0;

    DEBUG_PRINTF("ReplicationTable_insert(): inserted %u.\n",
        entity->replication_id);

    table->lookup_table[index] = entry;
    ++table->num_reserved;
#endif
}

static void
_ReplicationTable_erase(ReplicationTable *table, Entity *entity)
{
    DEBUG_PRINTF("ReplicationTable_erase(): erasing %u.\n",
        entity->replication_id);
#if _CF_NETWORKING
    uint index = entity->replication_id % table->size;

    for (ReplicationTableEntry **p = &table->lookup_table[index];
         *p;
         p = &(*p)->lookup_next)
    {
        if ((*p)->entity == entity)
        {
            ReplicationTableEntry *entry = *p;

            /* Remove from lookup list */
            if (entry->lookup_next) entry->lookup_next->lookup_prev = entry->lookup_prev;

            if (entry->lookup_prev)
                entry->lookup_prev->lookup_next = entry->lookup_next;
            else
                table->lookup_table[index] = entry->lookup_next;

            /* Remove from reserved list */
            if (entry->reserved_next)
                entry->reserved_next->reserved_prev = entry->reserved_prev;

            if (entry->reserved_prev)
                entry->reserved_prev->reserved_next = entry->reserved_next;
            else
                table->reserved_entries = entry->reserved_next;

            entry->entity = 0;

            --table->num_reserved;

            return;
        }
    }

    ASSERT(0 && "ReplicationTable_erase() failure.\n");
#endif
}

int
Scene3D_buildCollisionWorld(Scene3D *scene)
{
    int r = CollisionWorld_init(&scene->collision_world, &scene->memory,
        2, 500.0f, 16, 16);
    if (r != 0) DEBUG_PRINTF("Scene3D_buildCollisionWorld(): Failed to build "
        "collision world.\n");
    return r;
}

static inline void
pushCollisionEnterEvent(Scene3D *scene,
    PhysicsComponent *component_a, PhysicsComponent *component_b,
    uint32_t collider_a, enum ColliderType collider_a_type,
    uint32_t collider_b, enum ColliderType collider_b_type,
    vec3 normal, uint32_t normal_owner, float penetration)
{
    EntityPool *pool = &scene->entities;

    if (pool->num_collision_events >= pool->num_max_collision_events)
    {
        DEBUG_PRINTF("pushCollisionEnterEvent(): "
            "Collision event maximum amount reached!\n");
        return;
    }

    CollisionEvent *event = &pool->collision_events[pool->num_collision_events];

    event->component_a      = component_a;
    event->component_b      = component_b;
    event->collider_a       = collider_a;
    event->collider_b       = collider_b;
    event->collider_a_type  = collider_a_type;
    event->collider_b_type  = collider_b_type;
    event->normal_owner     = normal_owner;
    vec3_copy(event->normal, normal);
    event->penetration      = penetration;
    event->type             = COLLISION_ENTER;

    ++pool->num_collision_events;
}

static inline void
pushCollisionEndEvent(Scene3D *scene,
    PhysicsComponent *component_a, PhysicsComponent *component_b,
    uint32_t collider_a, enum ColliderType collider_a_type,
    uint32_t collider_b, enum ColliderType collider_b_type)
{
    EntityPool *pool = &scene->entities;

    if (pool->num_collision_events >= pool->num_max_collision_events)
    {
        DEBUG_PRINTF("pushCollisionEndEvent(): "
            "Collision event maximum amount reached!\n");
        return;
    }

    CollisionEvent *event = &pool->collision_events[pool->num_collision_events];

    event->component_a      = component_a;
    event->component_b      = component_b;
    event->collider_a       = collider_a;
    event->collider_b       = collider_b;
    event->collider_a_type  = collider_a_type;
    event->collider_b_type  = collider_b_type;
    event->type             = COLLISION_END;

    ++pool->num_collision_events;
}

static inline void
PhysicsComponent_updateColliderTransforms(PhysicsComponent *p, EntityTransform *et)
{

    bool32 created_rot_matrix = 0;
    mat3x3 tm;

	float	collider_x_rot,
			collider_y_rot,
			collider_z_rot;

    /* TODO: should probably take in count the entity's scale here, too */

    #define CALC_ROT_MATRIX(collider_relative_pos, tm, et, rotated) \
    { \
        mat3x3_identity(tm); \
        if(!GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_X_BIT)) \
            mat3x3_rotate(tm, tm, -1.0f, 0.0f, 0.0f, et->transform.rot[0]); \
        if(!GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_Y_BIT)) \
            mat3x3_rotate(tm, tm, 0.0f, 0.0f, -1.0f, et->transform.rot[2]); \
        if(!GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_Z_BIT)) \
            mat3x3_rotate(tm, tm, 0.0f, -1.0f, 0.0f, et->transform.rot[1]); \
        rotated = 1; \
    }


    for (BoxCollider *box = p->box_colliders; box; box = box->next)
    {


        /* Scale */
        box->data.dimensions[0] = et->transform.scale[0] * box->dimensions[0];
        box->data.dimensions[1] = et->transform.scale[1] * box->dimensions[1];
        box->data.dimensions[2] = et->transform.scale[2] * box->dimensions[2];

		if (GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_X_BIT))
			collider_x_rot = et->transform.rot[0];
		else
            collider_x_rot = 0.0f;
		if (GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_Y_BIT))
			collider_y_rot = et->transform.rot[1];
		else
            collider_y_rot = 0.0f;

		if (GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_Z_BIT))
            collider_z_rot = et->transform.rot[2];
		else
            collider_z_rot = 0.0f;

        if (!vec3_is_null(box->rotation) || !vec3_is_null(box->position))
        {
            /* Rotate the relative position of the box */
            if (!created_rot_matrix)
                CALC_ROT_MATRIX(box->position, tm, et, created_rot_matrix);

            mat3x3 boxmt;
            mat3x3_identity(boxmt);
            if (!GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_X_BIT))
                mat3x3_rotate(boxmt, boxmt, 1.0f, 0.0f, 0.0f, box->rotation[0]);
            if(!GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_Y_BIT))
                mat3x3_rotate(boxmt, boxmt, 0.0f, 1.0f, 0.0f, box->rotation[1]);
            if(!GET_BITFLAG(box->flags, char, COLLIDER_AXIS_LOCK_Z_BIT))
                mat3x3_rotate(boxmt, boxmt, 0.0f, 0.0f, 1.0f, box->rotation[2]);

            mat3x3_mul(box->data.rotation, boxmt, tm);

            vec3 v;
            vec3_mul_mat3x3(v, box->position, tm);
            vec3_sub(box->data.center, et->transform.pos, v);
        }
        else
        {
            mat3x3_identity(box->data.rotation);
            mat3x3_rotate(box->data.rotation, box->data.rotation, 1.0f, 0.0f, 0.0f, -collider_x_rot);
            mat3x3_rotate(box->data.rotation, box->data.rotation, 0.0f, 0.0f, 1.0f, -collider_z_rot);
            mat3x3_rotate(box->data.rotation, box->data.rotation, 0.0f, 1.0f, 0.0f, -collider_y_rot);


            vec3_copy(box->data.center, et->transform.pos);
        }
    }

    for (SphereCollider *sp = p->sphere_colliders; sp; sp = sp->next)
    {
        if (!vec3_is_null(sp->position))
        {
            /* Rotate the sphere's relative position */
            mat3x3_identity(tm);

            if (!GET_BITFLAG(sp->flags, char, COLLIDER_AXIS_LOCK_X_BIT))
                mat3x3_rotate(tm, tm, -1.0f, 0.0f, 0.0f, et->transform.rot[0]);
            if (!GET_BITFLAG(sp->flags, char, COLLIDER_AXIS_LOCK_Z_BIT))
                mat3x3_rotate(tm, tm, 0.0f, 0.0f, -1.0f, et->transform.rot[2]);
            if (!GET_BITFLAG(sp->flags, char, COLLIDER_AXIS_LOCK_Y_BIT))
                mat3x3_rotate(tm, tm, 0.0f, -1.0f, 0.0f, et->transform.rot[1]);
            //if (!created_rot_matrix)
                //CALC_ROT_MATRIX(box->position, tm, et, created_rot_matrix);

            vec3 v;
            vec3_mul_mat3x3(v, sp->position, tm);
            vec3_sub(sp->data.center, et->transform.pos, v); /* Should be add */
        }
        else
        {
            vec3_copy(sp->data.center, et->transform.pos);
        }

        float avg = (et->transform.scale[0] + et->transform.scale[1] + \
            et->transform.scale[2]) / 3.0f;
        sp->data.radius = sp->radius * avg;
    }

    #undef CALC_ROT_MATRIX
}

static int
CollisionCache_init(CollisionCache *cache,
    SimpleMemoryBlock *mem, uint num_entities)
{
    cache->pool = (CollisionCacheEntry*)SimpleMemoryBlock_malloc(mem,
        num_entities * 4 * sizeof(CollisionCacheEntry));
    cache->table = SimpleMemoryBlock_malloc(mem,
        num_entities * sizeof(CollisionCacheEntry*));
    cache->resolvable_pairs = SimpleMemoryBlock_malloc(mem,
        num_entities * 4 * sizeof(ResolvableColliderPair));
    cache->capacity = num_entities * 4;
    cache->num_max_resolvable_pairs = num_entities * 4;
    cache->num_resolvable_pairs = 0;

    if (!cache->pool || !cache->table || !cache->resolvable_pairs)
    {
        DEBUG_PRINTF("CollisionCache_init: out of memory.");
        return 1;
    }

    for (CollisionCacheEntry *entry = cache->pool;
         entry < &cache->pool[cache->capacity - 1];
         ++entry)
    {
        entry->next = entry + 1;
    }

    cache->pool[cache->capacity - 1].next = 0;
    cache->free = cache->pool;

    memset(cache->table, 0, num_entities * sizeof(CollisionCacheEntry*));

    return 0;
}

static inline void
CollisionCache_pushEntry(CollisionCache *cache,
    uint32_t entity_a_index, uint32_t entity_b_index,
    uint32_t collider_a, enum ColliderType collider_a_type,
    uint32_t collider_b, enum ColliderType collider_b_type)
{
    if (cache->free)
    {
        CollisionCacheEntry *entry = cache->free;
        cache->free = entry->next;

        /* Always make the lower index be a */
        if (entity_a_index < entity_b_index)
        {
            entry->collider_a       = collider_a;
            entry->collider_b       = collider_b;
            entry->collider_a_type  = collider_a_type;
            entry->collider_b_type  = collider_b_type;
            entry->entity_a_index   = entity_a_index;
            entry->entity_b_index   = entity_b_index;
        }
        else
        {
            entry->collider_a       = collider_b;
            entry->collider_b       = collider_a;
            entry->collider_a_type  = collider_b_type;
            entry->collider_b_type  = collider_a_type;
            entry->entity_a_index   = entity_b_index;
            entry->entity_b_index   = entity_a_index;
        }

        entry->timestamp = (uint32_t)getCurrentFrameNum();

        CollisionCacheEntry **tail = &cache->table[entry->entity_a_index];
        while (*tail) tail = &(*tail)->next;

        *tail = entry;
        entry->next = 0;
    }
    else
    {
        DEBUG_PRINTF("Warning: ran out of free collision cache slots!\n");
    }
}

static inline CollisionCacheEntry *
CollisionCache_getEntry(CollisionCache *cache,
    uint32_t entity_a_index, uint32_t entity_b_index,
    uint32_t collider_a, uint8_t collider_a_type,
    uint32_t collider_b, uint8_t collider_b_type)
{
    ASSERT(entity_a_index != entity_b_index);

    CollisionCacheEntry **entry_p;
    CollisionCacheEntry *e;

    uint32_t lower_index,    higher_index;
    uint32_t lower_collider, higher_collider;
    uint8_t  lower_type,     higher_type;

    if (entity_a_index < entity_b_index)
    {
        lower_index    = entity_a_index;
        lower_collider = collider_a;
        lower_type     = collider_a_type;

        higher_index   = entity_b_index;
        higher_collider= collider_b;
        higher_type    = collider_b_type;
    }
    else
    {
        lower_index    = entity_b_index;
        lower_collider = collider_b;
        lower_type     = collider_b_type;

        higher_index   = entity_a_index;
        higher_collider= collider_a;
        higher_type    = collider_a_type;
    }

    for (entry_p = &cache->table[lower_index];
         *entry_p;
         entry_p = &(*entry_p)->next)
    {
        e = *entry_p;

        /* First index is always lower */
        if (e->entity_a_index   == lower_index
        &&  e->entity_b_index   == higher_index
        &&  e->collider_a       == lower_collider
        &&  e->collider_b       == higher_collider
        &&  e->collider_a_type  == lower_type
        &&  e->collider_b_type  == higher_type)
        {
            return e;
        }
    }

    return 0;
}

static inline void
CollisionCache_pushResolvablePair(CollisionCache *cache,
    PhysicsComponent *component_a, PhysicsComponent *component_b,
    uint32_t collider_a, uint8_t collider_a_type,
    uint32_t collider_b, uint8_t collider_b_type)
{
    if (cache->num_resolvable_pairs >= cache->num_max_resolvable_pairs)
    {
        DEBUG_PRINTF("CollisionCache_pushResolvablePair(): too many pairs!\n");
        return;
    }

    ResolvableColliderPair *pair = &cache->resolvable_pairs[cache->num_resolvable_pairs++];
    pair->component_a       = component_a;
    pair->component_b       = component_b;
    pair->collider_a        = collider_a;
    pair->collider_b        = collider_b;
    pair->collider_a_type   = collider_a_type;
    pair->collider_b_type   = collider_b_type;
}


/* External */

void
Entity_setPosition(Entity *entity, float x, float y, float z)
{
    EntityTransform *et = Entity_getTransform(entity);
    et->transform.pos[0] = x;
    et->transform.pos[1] = y;
    et->transform.pos[2] = z;
}

void
Entity_setRotation(Entity *entity, float x, float y, float z)
{
    EntityTransform *et = Entity_getTransform(entity);
    et->transform.rot[0] = x;
    et->transform.rot[1] = y;// RADIAN_CLAMPF(y);
    et->transform.rot[2] = z;// RADIAN_CLAMPF(z);

    if (et->transform.rot[0] > (float)M_PI * 2.0f)
        et->transform.rot[0] = et->transform.rot[1] - (float)M_PI * 2.0f;
    else if (et->transform.rot[0] < 0.0f)
        et->transform.rot[0] = (float)M_PI * 2.0f + et->transform.rot[0];

    if (et->transform.rot[1] > (float)M_PI * 2.0f)
        et->transform.rot[1] = et->transform.rot[1] - (float)M_PI * 2.0f;
    else if (et->transform.rot[1] < 0.0f)
        et->transform.rot[1] = (float)M_PI * 2.0f + et->transform.rot[1];

    if (et->transform.rot[2] > (float)M_PI * 2.0f)
        et->transform.rot[2] = et->transform.rot[2] - (float)M_PI * 2.0f;
    else if (et->transform.rot[0] < 0.0f)
        et->transform.rot[2] = (float)M_PI * 2.0f + et->transform.rot[2];
}

void
Entity_lookAt(Entity *entity, vec3 position)
{
    Transform *t = &Entity_getTransform(entity)->transform;
    vec3 dir;
    vec3_sub(dir, position, t->pos);
    vec3_norm(dir, dir);

    t->rot[1] = -((float)atan2(dir[2], dir[0]));
    /*t->rot[2] = -(CLAMP(t->rot[1], -((float)M_PI / 2.0f) + 0.001f, (float)M_PI / 2.0f - 0.001f));*/
    t->rot[2] = (float)atan2(dir[1], sqrtf((dir[0] * dir[0]) + (dir[2] * dir[2])));
}

void
Entity_strafe(Entity *entity, float speed)
{
    EntityTransform *et = Entity_getTransform(entity);

    vec3 fwd;
    Transform_getForwardVector(&et->transform, fwd);

    vec3 c;
    vec3_mul_cross(c, fwd, DEFAULT_UP_VECTOR);
    vec3_norm(c, c);
    vec3_scale(c, c, speed);
    vec3_sub(et->transform.pos, et->transform.pos, c);
}

void
EntityPool_init(EntityPool *pool, Scene3D *scene, uint num_entities,
    SimpleMemoryBlock *memory)
{
    ASSERT(!pool->entities);
    ASSERT(num_entities > 0);
    ASSERT(memory);

    pool->entities = (Entity*)SimpleMemoryBlock_malloc(memory,
        num_entities * sizeof(Entity));
    pool->transforms = (EntityTransform*)SimpleMemoryBlock_malloc(memory,
        num_entities * sizeof(EntityTransform));

    pool->lifetimes = (EntityLifetime*)SimpleMemoryBlock_malloc(memory, 
        num_entities * sizeof(EntityLifetime));
    pool->num_lifetimes = 0;

    pool->num_max_entities  = num_entities;
    pool->num_reserved      = 0;

    uint index = 0;

    for (Entity *e = pool->entities;
        e < &pool->entities[num_entities];
        ++e)
    {
        e->index = index++;
        e->next  = e + 1;
        e->scene = scene;
        e->flags = 0;
    }

    pool->entities[num_entities - 1].next = 0;

    pool->free      = pool->entities;
    pool->reserved  = 0;

    /* A list of entities to be removed */
    pool->removables = SimpleMemoryBlock_malloc(memory,
        num_entities * sizeof(Entity*));
    pool->num_removables = 0;

    /* Physics components */
    PhysicsComponentPool_init(&pool->physics,
        SimpleMemoryBlock_malloc(memory,
            num_entities * sizeof(PhysicsComponent)),
        num_entities);

    ModelComponentPool_init(&pool->models,
        SimpleMemoryBlock_malloc(memory,
            num_entities * sizeof(ModelComponent)),
        num_entities);

    DirectionalLightComponentPool_init(&pool->lights,
        SimpleMemoryBlock_malloc(memory,
            num_entities * sizeof(DirectionalLightComponent)),
        num_entities);

    PointLightComponentPool_init(&pool->point_lights,
        SimpleMemoryBlock_malloc(memory,
            num_entities * sizeof(PointLightComponent)),
        num_entities);

    ScriptComponentPool_init(&pool->scripts,
        SimpleMemoryBlock_malloc(memory,
            num_entities * sizeof(ScriptComponent)),
        num_entities);

    /* Event buffers */

    /* Collision events */
    {
        void *collision_events = SimpleMemoryBlock_malloc(memory,
            num_entities * sizeof(CollisionEvent));

        ASSERT(collision_events);

        pool->collision_events = (CollisionEvent*)collision_events;
        pool->num_collision_events = 0;
        pool->num_max_collision_events = num_entities;
    }

    /* Collision entries */
    int cache_ret = CollisionCache_init(&pool->collision_cache,
        memory, num_entities);
    ASSERT(cache_ret == 0);
}

Entity *
EntityPool_reserve(EntityPool *pool, uint id)
{
    if (!pool->free)
        return 0;

    Entity *e       = pool->free;
    pool->free      = e->next;

    Entity **p = &pool->reserved;
    while (*p && (*p)->index < e->index)
        p = &(*p)->next;

    e->next = *p;
    *p = e;

    e->id = id;
    e->component_flags = 0;
    memset(&e->components, 0, NUM_MAX_COMPONENTS * sizeof(uint32_t));

    /* Assign the entity a transform */
    EntityTransform *t  = &pool->transforms[pool->num_reserved];
    t->entity_index     = e->index;
    e->transform_index  = pool->num_reserved;

    ++pool->num_reserved;

    return e;
}

void
EntityPool_free(EntityPool *pool, Entity *entity)
{
    Entity **p;

    /* Find the entity previous to this one */
    /* Note: maybe we should just do a prev variable for the struct... */
    for (p = &pool->reserved;
        *p != entity && *p;
        p = &(*p)->next) {}

    ASSERT(*p == entity);
    *p = entity->next;

    /* Append to free list */
    p = &pool->free;

    while (*p && (*p)->index < entity->index)
        p = &(*p)->next;

    entity->next = (*p);
    *p = entity;

    --pool->num_reserved;
}

#define COMPONENT_RESERVE_WARNING_STR(component_type, entity) \
    "Warning: failed to reserve " #component_type \
    "for entity %s, id %u!\n", Entity_getName(entity), entity->id

PhysicsComponent *
Entity_attachPhysicsComponent(Entity *entity)
{
    ASSERT(!Entity_hasComponent(entity, PHYSICS_COMPONENT_BIT));

    PhysicsComponent *p = PhysicsComponentPool_reserve(
        &entity->scene->entities.physics, entity);

    if (!p)
    {
        DEBUG_PRINTF(COMPONENT_RESERVE_WARNING_STR(PhysicsComponent, entity));
        return p;
    }

    p->flags = 0;
    PhysicsComponent_setActivity(p, 1);
    PhysicsComponent_setStaticity(p, 0);

    p->gravity_active = 1;
    p->friction     = 1.0f;
    p->max_velocity = 10.0f;
    p->elasticity   = 0.0f;

    p->velocity[0] = 0.0f;
    p->velocity[1] = 0.0f;
    p->velocity[2] = 0.0f;

    p->acceleration[0] = 0.0f;
    p->acceleration[1] = 0.0f;
    p->acceleration[2] = 0.0f;

    p->box_colliders    = 0;
    p->sphere_colliders = 0;

    p->transform_index = entity->transform_index;

    p->collisionCallback = 0;
    p->staticCollisionCallback = 0;

    return p;
}

ModelComponent *
Entity_attachModelComponent(Entity *entity)
{
    ASSERT(!Entity_hasComponent(entity, MODEL_COMPONENT_BIT));

    ModelComponent *m = ModelComponentPool_reserve(
        &entity->scene->entities.models, entity);

    if (!m)
    {
        DEBUG_PRINTF(COMPONENT_RESERVE_WARNING_STR(ModelComponent, entity));
        return 0;
    }

    m->render_models = RenderModelPool_reserve(&entity->scene->render_model_pool);

    if (!m->render_models)
    {
        DEBUG_PRINTF("Warning: failed to add render model to model component!");
        return 0;
    }

    m->render_models->visible       = 1;
    m->transform_index              = entity->transform_index;
    m->visible                      = 1;
    m->render_models->parent_next   = 0;
    m->animators                    = 0;

    return m;
}

DirectionalLightComponent *
Entity_attachDirectionalLightComponent(Entity *entity)
{
    ASSERT(!Entity_hasComponent(entity, DIRECTIONAL_LIGHT_COMPONENT_BIT));

    DirectionalLightComponent *l = DirectionalLightComponentPool_reserve(
        &entity->scene->entities.lights, entity);

    if (!l)
    {
        DEBUG_PRINTF(COMPONENT_RESERVE_WARNING_STR(DirectionalLightComponent, entity));
        return l;
    }

    l->transform_index = entity->transform_index;

    vec3_set(l->diffuse,    0.0f, 0.0f, 0.0f);
    vec3_set(l->specular,   0.0f, 0.0f, 0.0f);
    vec3_set(l->ambient,    0.0f, 0.0f, 0.0f);
    l->radius = 1.0f;
    l->active = 1;

    return l;
}

PointLightComponent *
Entity_attachPointLightComponent(Entity *entity)
{
    ASSERT(!Entity_hasComponent(entity, POINT_LIGHT_COMPONENT_BIT));

    PointLightComponent *l = PointLightComponentPool_reserve(
        &entity->scene->entities.point_lights, entity);

    if (!l)
    {
        DEBUG_PRINTF(COMPONENT_RESERVE_WARNING_STR(PointLightComponent, entity));
        return l;
    }

    l->transform_index = entity->transform_index;

    vec3_set(l->offset,     0.0f, 0.0f, 0.0f);
    vec3_set(l->diffuse,    0.0f, 0.0f, 0.0f);
    vec3_set(l->specular,   0.0f, 0.0f, 0.0f);
    vec3_set(l->ambient,    0.0f, 0.0f, 0.0f);

    l->constant_attenuation     = 1.0f;
    l->linear_attenuation       = 0.09f;
    l->quadratic_attenuation    = 0.032f;

    l->radius                   = 1.0f;
    l->active                   = 1;

    return l;
}

ScriptComponent *
Entity_attachScriptComponent(Entity *entity)
{
    ASSERT(!Entity_hasComponent(entity, SCRIPT_COMPONENT_BIT));

    ScriptComponent *s = ScriptComponentPool_reserve(
        &entity->scene->entities.scripts, entity);

    if (!s)
    {
        DEBUG_PRINTF(COMPONENT_RESERVE_WARNING_STR(ScriptComponent, entity));
        return s;
    }

    s->data         = 0;
    s->callback     = 0;

    return s;
}

void
Entity_collisionPhysics(CollisionEvent *event)
{
	if ((!PhysicsComponent_isStatic(event->component_a) &&!PhysicsComponent_isStatic(event->component_a))
        && (PhysicsComponent_isActive(event->component_b) && PhysicsComponent_isActive(event->component_a)))
	{
		if (event->normal[0] > 0.0f)
        {
            event->component_a->velocity[0] = ((-event->component_a->velocity[0] + \
                event->component_b->velocity[0]) * event->component_a->elasticity);
        }
		if (event->normal[1] > 0.0f)
        {
            event->component_a->velocity[1] = ((-event->component_a->velocity[1] + \
                event->component_b->velocity[1]) * event->component_a->elasticity);
        }
		if (event->normal[2] > 0.0f)
        {
            event->component_a->velocity[2] = ((-event->component_a->velocity[2] + \
                event->component_b->velocity[2]) * event->component_a->elasticity);
        }
	}
}

void
Entity_makeReplicated(Entity *entity)
{
#if _CF_NETWORKING
    ASSERT(entity->replication_id == ENTITY_NOT_REPLICATED);
    ASSERT(entity->scene->is_multiplayer);
    entity->replication_id = entity->scene->running_replication_id++;
    _ReplicationTable_insert(&entity->scene->replication_table, entity);
#endif
}

void
Entity_makeExistingReplicated(Entity *entity, replication_id32_t replication_id)
{
    ASSERT(entity->replication_id == ENTITY_NOT_REPLICATED);
    ASSERT(entity->scene->is_multiplayer);
    entity->replication_id = replication_id;
    _ReplicationTable_insert(&entity->scene->replication_table, entity);
}

int
Scene3D_init(Scene3D *scene,
    void *memory, size_t memory_size,
    uint num_entities,
    bool32 is_multiplayer)
{
    ASSERT(!scene->initialized);

    if (!memory || !memory_size)
        return SCENE3D_ERROR_OUT_OF_MEMORY;

#if !_CF_NETWORKING
    if (is_multiplayer)
        return SCENE3D_ERROR_NETWORKING_DISABLED;
#endif

    SimpleMemoryBlock_init(&scene->memory, memory, memory_size);
    EntityPool_init(&scene->entities, scene, num_entities, &scene->memory);
    scene->use_viewport_override = 0;

    scene->physics_limits.axis_y_min_val    = 0.0f;
    scene->physics_limits.min_y_value = 0.0f;
    scene->physics_limits.max_y_value = FLT_MAX;
    scene->physics_limits.gravity_strength  = 10.0f;
    scene->physics_limits.gravity_direction[0] = 0.0f;
    scene->physics_limits.gravity_direction[1] = -1.0f;
    scene->physics_limits.gravity_direction[2] = 0.0f;
    mat4x4_identity(scene->skybox_rotation);

    scene->have_skybox = 0;
    Scene3D_setSkyboxScale(scene, 1.0f, 1.0f, 1.0f);
    Scene3D_setAmbientColor(scene, 1.0f, 1.0f, 1.0f, 1.0f);

    /* Collision data containers */
    {
        void *pool_mem = SimpleMemoryBlock_malloc(&scene->memory,
            num_entities * sizeof(BoxCollider));

        if (!pool_mem)
        {
            DEBUG_PRINTF("Scene3D: failed to pre-allocate box colliders.\n");
            return SCENE3D_ERROR_OUT_OF_MEMORY;
        }

        BoxColliderPool_init(&scene->box_collider_pool,
            pool_mem, num_entities);

        pool_mem = SimpleMemoryBlock_malloc(&scene->memory,
            num_entities * sizeof(SphereCollider));

        if (!pool_mem)
        {
            DEBUG_PRINTF("Scene3D: failed to pre-allocate sphere colliders.\n");
            return SCENE3D_ERROR_OUT_OF_MEMORY;
        }

        SphereColliderPool_init(&scene->sphere_collider_pool,
            pool_mem, num_entities);
    }

    /*Animators*/
    {
        Animator *animator_mem = SimpleMemoryBlock_malloc(&scene->memory,
            num_entities * 2 * sizeof(Animator));

        if (!animator_mem)
        {
            DEBUG_PRINTF("Scene3D: failed to pre-allocate animators.\n");
            return SCENE3D_ERROR_OUT_OF_MEMORY;
        }
        AnimatorPool_init(&scene->animator_pool, animator_mem, num_entities * 2);
    }

    /* Render models */
    {
        void *mesh_mem = SimpleMemoryBlock_malloc(&scene->memory,
            num_entities * 2 * sizeof(RenderModel));

        if (!mesh_mem)
        {
            DEBUG_PRINTF("Scene3D: failed to pre-allocate render meshes.\n");
            return SCENE3D_ERROR_OUT_OF_MEMORY;
        }

        RenderModelPool_init(&scene->render_model_pool, mesh_mem, num_entities * 2);
    }

    memset(&scene->collision_world, 0, sizeof(CollisionWorld));

    scene->render_collision_octree      = 0;
    scene->render_collision_geometry    = 0;
    scene->render_dynamic_colliders     = 0;

    /* Work ranges for multithreading */
    scene->work_ranges = SimpleMemoryBlock_malloc(&scene->memory,
        engine.thread_pool.num_threads * sizeof(SceneWorkRange));

    if (!scene->work_ranges)
    {
        DEBUG_PRINTF("Scene3D_init(): failed to preallocate work ranges.\n");
        return SCENE3D_ERROR_OUT_OF_MEMORY;
    }


    scene->max_work_ranges  = engine.thread_pool.num_threads;
    scene->physics_step     = DEFAULT_PHYSICS_TIMESTEP;
    scene->physics_timer    = 0.0f;

#if _CF_NETWORKING
    if (is_multiplayer)
    {
        if (_ReplicationTable_init(&scene->replication_table, &scene->memory,
            num_entities) != 0)
        {
            return SCENE3D_ERROR_OUT_OF_MEMORY;
        }

        void *msg_mem = SimpleMemoryBlock_malloc(&scene->memory,
            KYS_MTU * 2);

        if (!msg_mem)
        {
            DEBUG_PRINTF("Scene3D_init(): failed to preallocate message "
                "memory.\n");
            return SCENE3D_ERROR_OUT_OF_MEMORY;
        }

        SimpleMemoryBlock_init(&scene->message_buffer, msg_mem, KYS_MTU * 2);

        Mutex_init(&scene->message_buffer_mutex);
        Mutex_init(&scene->entity_access_mutex);
    }

    scene->is_multiplayer           = is_multiplayer;
    scene->running_replication_id   = 0;

    /* Replication callbacks */
    scene->on_replicated_entity_creation_callback   = 0;
#endif

    scene->initialized = 1;

    return 0;
}

void
Scene3D_clear(Scene3D *scene)
{
    if (!scene->initialized) return;

    ScriptComponent *sc;
    for (uint i = 0; i < scene->entities.scripts.num_reserved; ++i)
    {
        sc = &scene->entities.scripts.components[i];

        if (sc->data)
            kfree(sc->data);
    }

    memset(scene, 0, sizeof(Scene3D));

    scene->initialized = 0;
}

int
Scene3D_buildCollisionWorldFromModel(Scene3D *scene, Model *model, vec3 offset, 
    uint tree_depth, uint max_num_static_box_colliders)
{
    return CollisionWorld_initFromModel(&scene->collision_world, model,
        &scene->memory, offset, tree_depth, max_num_static_box_colliders);
}

int
Scene3D_addStaticBoxCollidersToWorld(Scene3D *scene, float pos_x, float pos_y, float pos_z, 
    float dimension_x, float dimension_y, float dimension_z, float rotation_x, float rotation_y, float rotation_z)
{
    return CollisionWorld_insertStaticBoxColliders(&scene->collision_world, pos_x, pos_y, pos_z, 
        dimension_x, dimension_y, dimension_z, rotation_x, rotation_y, rotation_z);
}

static void
testRayWithNodeAndChildren(CollisionWorldNode *node, vec3 origin, vec3 direction, float length, 
    vec3 ret_hitpoint, float *distance_from_origin)
{
    float dist_to_triangle, dist_to_static_box_collider;
    vec3 hitpoint_to_triangle, hitpoint_to_static_box_collider;
    for (CollisionTriangle *t = node->triangles; t; t = t->next)
    {
        if (testTriangle3DRay(t->data.points[0], t->data.points[1], t->data.points[2], origin, direction, hitpoint_to_triangle, &dist_to_triangle))
        {
            if (*distance_from_origin > dist_to_triangle)
            {
                *distance_from_origin = dist_to_triangle;
                vec3_copy(ret_hitpoint, hitpoint_to_triangle);
            }
        }
    }
    for (StaticBoxCollider *sbc = node->static_box_colliders; sbc; sbc = sbc->next)
    {
        if (OBB3DRay(&sbc->data, origin, direction, length, hitpoint_to_static_box_collider, &dist_to_static_box_collider))
        {
            if (*distance_from_origin > dist_to_static_box_collider)
            {
                *distance_from_origin = dist_to_static_box_collider;
                vec3_copy(ret_hitpoint, hitpoint_to_static_box_collider);
            }
        }
    }

    if (!node->children[0])
    {
        return;
    }

    for (CollisionWorldNode **cn = node->children; cn < &node->children[8]; ++cn)
    {
        testRayWithNodeAndChildren(*cn, origin, direction, length, ret_hitpoint, distance_from_origin);
    }
}

static void
testRayWithNodeAndParents(CollisionWorldNode *node, vec3 origin, vec3 direction, float length, vec3 ret_hitpoint, float *distance_from_origin)
{
    float dist_to_triangle, dist_to_static_box_collider;
    vec3 hitpoint_to_triangle, hitpoint_to_static_box_collider;

    for (CollisionTriangle *t = node->triangles; t; t = t->next)
    {
        if (testTriangle3DRay(t->data.points[0], t->data.points[1], t->data.points[2], origin, direction, hitpoint_to_triangle, &dist_to_triangle))
        {
            if (*distance_from_origin > dist_to_triangle)
            {
                *distance_from_origin = dist_to_triangle;
                vec3_copy(ret_hitpoint, hitpoint_to_triangle);
            }
        }
    }
    for (StaticBoxCollider *sbc = node->static_box_colliders; sbc; sbc = sbc->next)
    {
        if (OBB3DRay(&sbc->data, origin, direction, length, hitpoint_to_static_box_collider, &dist_to_static_box_collider))
        {
            if (*distance_from_origin > dist_to_static_box_collider)
            {
                *distance_from_origin = dist_to_static_box_collider;
                vec3_copy(ret_hitpoint, hitpoint_to_static_box_collider);
            }
        }
    }

    if (node->parent)
    {
        testRayWithNodeAndParents(node->parent, origin, direction, length, ret_hitpoint, distance_from_origin);
    }
}


float
Scene3D_castRayToStaticGeometry(Scene3D *scene, vec3 origin, vec3 direction, float length, vec3 ret_hitpoint, float *distance_from_origin)
{
    CollisionWorldNode *node;
    *distance_from_origin = FLT_MAX;
    float radius = (length * 0.5f);
    vec3 halfpoint;
    vec3 temp;
    vec3_scale(temp, direction, radius);
    vec3_add(halfpoint, origin, temp);

    node = CollisionWorld_getNodeByObjectRadius(halfpoint, scene->collision_world.tree, radius);

    testRayWithNodeAndChildren(node, origin, direction, length, ret_hitpoint, distance_from_origin);

    if (node->parent)
    {
        testRayWithNodeAndParents(node->parent, origin, direction, length, ret_hitpoint, distance_from_origin);
    }

    if (*distance_from_origin <= length)
        return *distance_from_origin;

    else
        return 0;

}

static void
testRayWithNodeAndChildrenAndGetNormal(CollisionWorldNode *node, vec3 origin, vec3 direction, float length, vec3 ret_hitpoint, float *distance_from_origin, vec3 normal)
{
    float dist_to_triangle, dist_to_static_box_collider;
    vec3 hitpoint_to_triangle, hitpoint_to_static_box_collider;
    for (CollisionTriangle *t = node->triangles; t; t = t->next)
    {
        if (testTriangle3DRay(t->data.points[0], t->data.points[1], t->data.points[2], origin, direction, hitpoint_to_triangle, &dist_to_triangle))
        {
            if (*distance_from_origin > dist_to_triangle)
            {
                *distance_from_origin = dist_to_triangle;
                vec3_copy(ret_hitpoint, hitpoint_to_triangle);
                vec3_copy(normal, t->data.normal);
            }
        }
    }

    for (StaticBoxCollider *sbc = node->static_box_colliders; sbc; sbc = sbc->next)
    {
        if (OBB3DRay(&sbc->data, origin, direction, length, hitpoint_to_static_box_collider, &dist_to_static_box_collider))
        {
            if (*distance_from_origin > dist_to_static_box_collider)
            {
                *distance_from_origin = dist_to_static_box_collider;
                vec3_copy(ret_hitpoint, hitpoint_to_static_box_collider);
            }
        }
    }

    if (!node->children[0])
    {
        return;
    }

    for (CollisionWorldNode **cn = node->children; cn < &node->children[8]; ++cn)
    {
        testRayWithNodeAndChildrenAndGetNormal(*cn, origin, direction, length, ret_hitpoint, distance_from_origin, normal);
    }
}

static void
testRayWithNodeAndParentsAndGetNormal(CollisionWorldNode *node, vec3 origin, vec3 direction, float length, vec3 ret_hitpoint, float *distance_from_origin, vec3 normal)
{
    float dist_to_triangle, dist_to_static_box_collider;
    vec3 hitpoint_to_triangle, hitpoint_to_static_box_collider;
    for (CollisionTriangle *t = node->triangles; t; t = t->next)
    {
        if (testTriangle3DRay(t->data.points[0], t->data.points[1], t->data.points[2], origin, direction, hitpoint_to_triangle, &dist_to_triangle))
        {
            if (*distance_from_origin > dist_to_triangle)
            {
                *distance_from_origin = dist_to_triangle;
                vec3_copy(ret_hitpoint, hitpoint_to_triangle);
                vec3_copy(normal, t->data.normal);
            }
        }
    }

    for (StaticBoxCollider *sbc = node->static_box_colliders; sbc; sbc = sbc->next)
    {
        if (OBB3DRay(&sbc->data, origin, direction, length, hitpoint_to_static_box_collider, &dist_to_static_box_collider))
        {
            if (*distance_from_origin > dist_to_static_box_collider)
            {
                *distance_from_origin = dist_to_static_box_collider;
                vec3_copy(ret_hitpoint, hitpoint_to_static_box_collider);
            }
        }
    }

    if (node->parent)
    {
        testRayWithNodeAndParentsAndGetNormal(node->parent, origin, direction, length, ret_hitpoint, distance_from_origin, normal);
    }
}

float
Scene3D_castRayToStaticGeometryAndGetNormal(Scene3D *scene, vec3 origin, vec3 direction, float length, vec3 ret_hitpoint, float *distance_from_origin, vec3 normal)
{
    CollisionWorldNode *node;
    *distance_from_origin = FLT_MAX;
    float radius = (length * 0.5f);
    vec3 halfpoint;
    vec3 temp;
    vec3_scale(temp, direction, radius);
    vec3_add(halfpoint, origin, temp);

    node = CollisionWorld_getNodeByObjectRadius(halfpoint, scene->collision_world.tree, radius);

    testRayWithNodeAndChildrenAndGetNormal(node, origin, direction, length, ret_hitpoint, distance_from_origin, normal);

    if (node->parent)
    {
        testRayWithNodeAndParentsAndGetNormal(node->parent, origin, direction, length, ret_hitpoint, distance_from_origin, normal);
    }

    if (*distance_from_origin <= length)
        return *distance_from_origin;

    else
        return 0;

}

SphereCollider *
Scene3D_castRayToSphereColliders(Scene3D *scene, vec3 origin, vec3 endpoint, float *distance_from_origin, uint32_t *ignored_ids, uint num_ignored_ids)
{
    float dist;
    SphereCollider *ret_sc = 0;
    for (PhysicsComponent *pc = scene->entities.physics.components;
        pc < &scene->entities.physics.components[scene->entities.physics.num_reserved];
        ++pc)
    {
        for (SphereCollider *sc = pc->sphere_colliders; sc; sc = sc->next)
        {
            bool32 ignored = 0;
            for (uint i = 0; i < num_ignored_ids; ++i)
            {
                if (ignored_ids[i] == sc->id)
                {
                    ignored = 1;
                    break;
                }
            }
            if (!ignored && testSphere3DRayWithEndpoint(&sc->data, origin, endpoint, &dist))
            {
                if (!ret_sc || *distance_from_origin > dist)
                {
                    *distance_from_origin = dist;
                    ret_sc = sc;
                }
            }
        }
    }
    return ret_sc;
}

void
Scene3D_setRenderViewport(Scene3D *scene, Viewport viewport)
{
    if (viewport)
    {
        scene->render_viewport[0] = viewport[0];
        scene->render_viewport[1] = viewport[1];
        scene->render_viewport[2] = viewport[2];
        scene->render_viewport[3] = viewport[3];
        scene->use_viewport_override = 1;
    }
    else
    {
        scene->use_viewport_override = 0;
    }
}

void
Scene3D_removeEntityInner(Scene3D *scene, Entity *entity)
{
    ASSERT(entity);

    /*DEBUG_PRINTF("Removing entity - name: %s, id: %u\n",
        Entity_getName(entity), entity->id); */

    /* Swap transforms */

    EntityPool *pool = &scene->entities;

    if (pool->num_reserved > 1
    &&  entity->transform_index < pool->num_reserved - 1)
    {
        EntityTransform *transform  = &pool->transforms[entity->transform_index];
        EntityTransform *last_pos   = &pool->transforms[pool->num_reserved - 1];

        Entity *other_entity = &pool->entities[last_pos->entity_index];

        other_entity->transform_index = entity->transform_index;
        transform->entity_index = other_entity->index;
        transform->transform = last_pos->transform;

        if (Entity_hasComponent(other_entity, PHYSICS_COMPONENT_BIT))
        {
            PhysicsComponent *p = Entity_getPhysicsComponent(other_entity);
            p->transform_index  = entity->transform_index;
        }

        if (Entity_hasComponent(other_entity, DIRECTIONAL_LIGHT_COMPONENT_BIT))
        {
            DirectionalLightComponent *dl =
                Entity_getDirectionalLightComponent(other_entity);
            dl->transform_index = entity->transform_index;
        }

        if (Entity_hasComponent(other_entity, POINT_LIGHT_COMPONENT_BIT))
        {
            PointLightComponent *pl =
                Entity_getPointLightComponent(other_entity);
            pl->transform_index = entity->transform_index;
        }

        if (Entity_hasComponent(other_entity, MODEL_COMPONENT_BIT))
        {
            ModelComponent *m = Entity_getModelComponent(other_entity);
            m->transform_index = entity->transform_index;
        }
    }

    /* Free the components to their pools */

    if (Entity_hasComponent(entity, PHYSICS_COMPONENT_BIT))
    {
        PhysicsComponent *p = Entity_getPhysicsComponent(entity);

        /* Remove colliders */
        {
            BoxCollider *current = p->box_colliders;
            BoxCollider *next;

            while (current)
            {
                next = current->next;
                BoxColliderPool_free(&entity->scene->box_collider_pool,
                    current);
                current = next;
            }
        }

        {
            SphereCollider *current = p->sphere_colliders;
            SphereCollider *next;

            while (current)
            {
                next = current->next;
                SphereColliderPool_free(&entity->scene->sphere_collider_pool,
                    current);
                current = next;
            }
        }

        PhysicsComponentPool_free(&entity->scene->entities.physics,
            Entity_getPhysicsComponent(entity), scene);
    }

    if (Entity_hasComponent(entity, MODEL_COMPONENT_BIT))
    {
        ModelComponent *model = Entity_getModelComponent(entity);

        for (RenderModel *rm = model->render_models; rm; rm = rm->parent_next)
            RenderModelPool_free(&scene->render_model_pool, rm);

        for (Animator *a = model->animators; a; a = a->parent_next)
            AnimatorPool_free(&scene->animator_pool, a);

        ModelComponentPool_free(&scene->entities.models,
            model, scene);
    }

    if (Entity_hasComponent(entity, DIRECTIONAL_LIGHT_COMPONENT_BIT))
    {
        DirectionalLightComponentPool_free(&entity->scene->entities.lights,
            Entity_getDirectionalLightComponent(entity), scene);
    }

    if (Entity_hasComponent(entity, POINT_LIGHT_COMPONENT_BIT))
    {
        PointLightComponentPool_free(&entity->scene->entities.point_lights,
            Entity_getPointLightComponent(entity), scene);
    }

    if (Entity_hasComponent(entity, SCRIPT_COMPONENT_BIT))
    {
        ScriptComponent *s = Entity_getScriptComponent(entity);
        if (s->data != 0)
        {
            kfree(s->data);
            s->data = 0;
        }
        ScriptComponentPool_free(&entity->scene->entities.scripts, s, scene);
    }

    if (entity->replication_id != ENTITY_NOT_REPLICATED)
        _ReplicationTable_erase(&scene->replication_table, entity);

    EntityPool_free(&scene->entities, entity);
}

void
Scene3D_removeEntity(Scene3D *scene, Entity *entity)
{
    ASSERT(entity);

    if (!GET_BITFLAG(entity->flags, char, ENTITY_ALIVE))
    {
        DEBUG_PRINTF("Warning: attempted to remove entity %s, id %u, "
            "but already removed!\n", Entity_getName(entity), entity->id);
        return;
    }

    SET_BITFLAG_OFF(entity->flags, char, ENTITY_ALIVE);
    scene->entities.removables[scene->entities.num_removables] = entity;
    ++scene->entities.num_removables;
}

void
Scene3D_setSkyboxTexture(Scene3D *scene, Texture *texture)
{
    if (texture)
    {
        scene->skybox_texture = texture;
        scene->have_skybox = 1;
    }
    else
    {
        scene->have_skybox = 0;
    }
}

void
Scene3D_setSkyboxScale(Scene3D *scene, float scale_x, float scale_y, float scale_z)
{
    scene->skybox_scale[0] = scale_x;
    scene->skybox_scale[1] = scale_y;
    scene->skybox_scale[2] = scale_z;
}

void
Scene3D_updateSimulation(Scene3D *scene, double dt)
{
    scene->delta = dt;

    /* Velocity, acceleration etc. calculations */
    bool32 is_first_frame = scene->physics_timer == 0.0f;
    scene->physics_timer += dt;

#if _CF_FIXED_PHYSICS_TIMESTEP
    if (scene->physics_timer >= scene->physics_step || is_first_frame)
    {
        int num_steps;

        if (!is_first_frame)
            num_steps = (int)(scene->physics_timer / scene->physics_step);
        else
            num_steps = 1;

        for (int i = 0; i < num_steps; ++i)
        {
            Scene3D_updatePhysics(scene, scene->physics_step);
            scene->physics_timer = scene->physics_timer - scene->physics_step;

            /* Collision detection:
             * - Begin with preliminary detection
             * - Do more precise testing and generate events and cache entries
             * - Handle the generated collision events: physics responses and
             *   callbacks
             * - Remove old cache entries that are no longer valid */
            Scene3D_updateCollisions(scene);
        }
    }
#else
    Scene3D_updatePhysics(scene, dt);
    Scene3D_updateCollisions(scene);
#endif

    Scene3D_updateScripts(scene);
    Scene3D_updateAnimators(scene, dt);
    Scene3D_updateLightPositions(scene, dt);
    Scene3D_updateModels(scene);
    Scene3D_updateLifetimes(scene, dt);

    /* Remove... removed entities */
    for (Entity **ep = scene->entities.removables;
         ep < &scene->entities.removables[scene->entities.num_removables];
         ++ep)
    {
        Scene3D_removeEntityInner(scene, *ep);
    }

    scene->entities.num_removables = 0;
}

void
Scene3D_updateServerSimulation(Scene3D *scene, Server *server, double dt)
{
    Mutex_lock(&scene->entity_access_mutex);
    Scene3D_updateSimulation(scene, dt);
    Mutex_unlock(&scene->entity_access_mutex);
}

void
Scene3D_updateClientSimulation(Scene3D *scene, Client *client, double dt)
{
    ASSERT(scene->is_multiplayer);

    Mutex_lock(&scene->message_buffer_mutex);

    int num_bytes = SimpleMemoryBlock_getAllocatedSize(
        &scene->message_buffer);

    message_t   msg_type;
    uint16_t    msg_id;
    char        *data = (char*)scene->message_buffer.start;
    int         offset = 0;

    while (num_bytes - offset >= (int)MIN_MESSAGE_SIZE)
    {
        msg_type = *(message_t*)(data + offset);
        offset += (int)sizeof(message_t);

        msg_id = *(uint16_t*)(data + offset);
        offset += (int)sizeof(uint16_t);

        switch (msg_type)
        {
            case SC_MSG_INFORM_OF_ENTITY:
            {
                replication_id32_t replication_id =
                    *(replication_id32_t*)(data + offset);

                if (ReplicationTable_get(&scene->replication_table,
                    replication_id))
                {
                    DEBUG_PRINTF("Server requested replicating an entity,"
                        "but id is already in use!\n");
                    offset += SC_MSG_INFORM_OF_ENTITY_SIZE;
                    break;
                }

                entity_type16_t entity_type = *(entity_type16_t*)(data + \
                    offset + sizeof(replication_id32_t));

                vec3 pos;
                vec3_copy(pos, (float*)(data + offset + \
                    sizeof(replication_id32_t) + sizeof(entity_type16_t)));

                Entity *e = Scene3D_createEntity(scene, pos[0], pos[1], pos[2]);

                if (!e)
                {
                    DEBUG_PRINTF("Scene3D_updateClientSimulation(): attempted "
                        "to create replicated entity, but failed!\n");
                }
                else
                {
                    e->type = entity_type;
                    Entity_makeExistingReplicated(e, replication_id);
                    if (scene->on_replicated_entity_creation_callback)
                        scene->on_replicated_entity_creation_callback(e);
                    DEBUG_PRINTF("Scene3D_updateClientSimulation(): created "
                        "new replicated entity based on server request!\n");
                }

                offset += SC_MSG_INFORM_OF_ENTITY_SIZE;
            }
                break;
            case SC_MSG_INFORM_OF_ENTITY_REMOVAL:
            {
                replication_id32_t replication_id =
                    *(replication_id32_t*)(data + offset);

                Entity *e = ReplicationTable_get(&scene->replication_table,
                    replication_id);

                if (e)
                {
                    Scene3D_removeEntity(scene, e);
                    DEBUG_PRINTF("Scene3D_updateClientSimulation(): removed "
                        "an entity on upon server request.\n");
                }

                offset += SC_MSG_INFORM_OF_ENTITY_REMOVAL_SIZE;
            };
            case SC_MSG_ENTITY_POSITION_UPDATE:
            {
                replication_id32_t replication_id =
                    *(replication_id32_t*)(data + offset);

                ReplicationTableEntry *entry = ReplicationTable_getEntry(
                    &scene->replication_table, replication_id);

                if (entry && IS_SEQUENCE_GREATER(msg_id,
                    entry->latest_position_update))
                {
                    Entity *e       = entry->entity;
                    Transform *t    = &Entity_getTransform(e)->transform;

                    float *server_pos = (float*)((char*)data + offset +
                        sizeof(replication_id));

                    PhysicsComponent *pc = Entity_getPhysicsComponent(e);
                    if (pc)
                    {
                        double dt = (double)Client_getAverageLatency(client);
                        dt /= 1000.0f;
                        dt *= 0.5f;
                        vec3 v; vec3_scale(v, pc->velocity, (float)dt);
                        vec3_scale(v, v, 0.5f);
                        vec3_add(server_pos, server_pos, v);
                    }

#if 0
                    vec3 diff; vec3_sub(diff, t->pos, server_pos);
                    float len = vec3_len(diff);

                    if (len >= 2.0f) ;
#endif
                    vec3_copy(t->pos, server_pos);
                }

                offset += SC_MSG_ENTITY_POSITION_UPDATE_SIZE;
            }
                break;
            case SC_MSG_ENTITY_VELOCITY_UPDATE:
            {
                replication_id32_t replication_id =
                    *(replication_id32_t*)(data + offset);

                ReplicationTableEntry *entry = ReplicationTable_getEntry(
                    &scene->replication_table, replication_id);

                if (entry && IS_SEQUENCE_GREATER(msg_id,
                    entry->latest_velocity_update))
                {
                    PhysicsComponent *pc = Entity_getPhysicsComponent(
                        entry->entity);

                    if (pc)
                    {
                        vec3_copy(pc->velocity, (float*)((char*)data + offset +
                            sizeof(replication_id)));
                    }
                }

                offset += SC_MSG_ENTITY_VELOCITY_UPDATE_SIZE;
            }
                break;
            case SC_MSG_ENTITY_ROTATION_UPDATE:
            {
                replication_id32_t replication_id =
                    *(replication_id32_t*)(data + offset);

                Entity *e = ReplicationTable_get(&scene->replication_table,
                    replication_id);

                if (!e)
                {
                    DEBUG_PRINTF("SC_MSG_ENTITY_ROTATION_UPDATE: no entity "
                        "found!\n");
                }
                else
                {
                    Transform *t = &Entity_getTransform(e)->transform;
                    vec3_copy(t->rot, (float*)(data + offset + \
                        (int)sizeof(replication_id32_t)));
                }

                offset += SC_MSG_ENTITY_ROTATION_UPDATE_SIZE;
            }
                break;
            default:
            {
                DEBUG_PRINTF("Scene3D_updateClientSimulation: unknown message "
                    "type %u!\n", (uint)msg_type);
                offset = num_bytes;
            }
                break;
        }
    }

    SimpleMemoryBlock_clear(&scene->message_buffer);

    Mutex_unlock(&scene->message_buffer_mutex);

    Scene3D_updateSimulation(scene, dt);
}

Entity *
Scene3D_createEntity(Scene3D *scene, float x, float y, float z)
{
    Entity *e = EntityPool_reserve(&scene->entities, ++scene->running_entity_id);

    if (e)
    {
        ASSERT(!GET_BITFLAG(e->flags, char, ENTITY_ALIVE));
        EntityTransform *et = Entity_getTransform(e);
        vec3_set(et->transform.pos, x, y, z);
        vec3_set(et->transform.scale, 1.f, 1.f, 1.f);
        vec3_set(et->transform.rot, 0.f, 0.f, 0.f);
        SET_BITFLAG_ON(e->flags, char, ENTITY_ALIVE);

        e->type = 0;
#if _CF_ENTITY_STRING_NAMES
        e->name[0] = '\0';
#endif

#if _CF_NETWORKING
        e->replication_id = ENTITY_NOT_REPLICATED;
#endif

        return e;
    }
    else
    {
        DEBUG_PRINTF("Warning: Scene3D_createEntity() failed (out of slots?)\n");
        return 0;
    }
}

Entity *
Scene3D_createReplicatedServerEntity(Scene3D *scene, entity_type16_t type,
    float x, float y, float z)
{
    if (!scene->is_multiplayer) return 0;

    Entity *e = Scene3D_createEntity(scene, x, y, z);
    if (!e) return 0;

    e->type = type;
    Entity_makeReplicated(e);

    return e;
}

void
Scene3D_updateAnimators(Scene3D *scene, double dt)
{
    /* Update AnimatorPool */

    for (Animator *a = scene->animator_pool.reserved; a; a = a->pool_next)
    {
        a->time_passed += (float)(dt * (double)a->speed);

        if (a->model_animation != 0)
        {
            if (a->time_passed >= (float)(a->model_animation->duration))
            {
                if (a->current_model_frame == a->model_animation->num_frames - 1)
                {
                    if (a->loop)
                    {
                        a->current_model_frame = 0;
                        a->time_passed = 0.0f;
                        a->render_model->model = a->model_animation->frames[a->current_model_frame].model;
                        a->render_model->material = a->model_animation->frames[a->current_model_frame].material;
                        a->render_model->material_array = a->model_animation->frames[a->current_model_frame].material_array;
                    }
                }
                else
                {
                    ++a->current_model_frame;
                    a->time_passed = 0.0f;
                    a->render_model->model = a->model_animation->frames[a->current_model_frame].model;
                    a->render_model->material = a->model_animation->frames[a->current_model_frame].material;
                    a->render_model->material_array = a->model_animation->frames[a->current_model_frame].material_array;
                }
            }
        }
	}
}

void
Scene3D_updateLifetimes(Scene3D *scene, double dt)
{
    EntityLifetime *lt;

    for (int i = 0; i < (int)scene->entities.num_lifetimes; ++i)
    {
        lt = &scene->entities.lifetimes[i];
        lt->timeleft -= dt;

        if (lt->timeleft <= 0)
        {
            if (lt->id == lt->entity->id)
                Scene3D_removeEntity(scene, lt->entity);

            *lt = *(&scene->entities.lifetimes[scene->entities.num_lifetimes - 1]);
            ASSERT(scene->entities.num_lifetimes != 0);
            scene->entities.num_lifetimes -= 1;
            i -= 1;
        }
    }
}

static void
updateModelsForEntityRange(void *args)
{
    SceneWorkRange *range_struct = (SceneWorkRange*)args;
    Scene3D *scene = range_struct->scene;
    EntityTransform *et;
    vec3 camera_position;
    vec3_copy(camera_position, scene->camera_position);
    mat4x4 mat_tmp;
    struct ModelLightCount light_count;
    vec3 light_pos;

    for (ModelComponent *m = &scene->entities.models.components[range_struct->begin];
         m < &scene->entities.models.components[range_struct->end];
         ++m)
    {
        /* Note: may need to do transforms despite not being visible */
        if (!m->visible)
            continue;

        et = &scene->entities.transforms[m->transform_index];

        /* Update lights affecting this component */

        light_count.directional = 0;
        light_count.point       = 0;
        DirectionalLightComponent *directional_lights[MAX_ENTITY_DIRECTIONAL_LIGHTS];
        PointLightComponent *point_lights[MAX_ENTITY_POINT_LIGHTS];

        for (DirectionalLightComponent *l = scene->entities.lights.components;
             l < &scene->entities.lights.components[
                scene->entities.lights.num_reserved];
             ++l)
        {
            if (!l->active) continue;
            vec3_sub(light_pos, et->transform.pos, l->position);

            if (vec3_mul_inner(light_pos, light_pos) <= POWER_OF_2(l->radius))
            {
                directional_lights[light_count.directional] = l;
                ++light_count.directional;

                if (light_count.directional >= MAX_ENTITY_DIRECTIONAL_LIGHTS)
                    break;
            }
        }

        for (PointLightComponent *l = scene->entities.point_lights.components;
             l < &scene->entities.point_lights.components[
                scene->entities.point_lights.num_reserved];
             ++l)
        {
            if (!l->active) continue;

            vec3_sub(light_pos, et->transform.pos, l->cached_position);

            if (vec3_mul_inner(light_pos, light_pos) <= POWER_OF_2(l->radius))
            {
                point_lights[light_count.point] = l;
                ++light_count.point;

                if (light_count.point >= MAX_ENTITY_POINT_LIGHTS)
                    break;
            }
        }

        /* Update render models */
        for (RenderModel *rm = m->render_models; rm; rm = rm->parent_next)
        {
            if (!rm->visible) continue;

            mat4x4_identity(rm->mat_model);

            if (!(rm->mode == RENDER_MODEL_BILLBOARD))
            {
                mat4x4_translate_in_place(rm->mat_model,
                    et->transform.pos[0],
                    et->transform.pos[1],
                    et->transform.pos[2]);
            }
            else
            {
                mat4x4 tmp;
                mat4x4_identity(tmp);
                mat4x4_rotate(tmp, tmp, -1.0f, 0.0f, 0.0f, et->transform.rot[0]);
                mat4x4_rotate(tmp, tmp, 0.0f, 0.0f, -1.0f, et->transform.rot[2]);
                mat4x4_rotate(tmp, tmp, 0.0f, -1.0f, 0.0f, et->transform.rot[1]);


                vec4 rel =
                {
                    rm->relative_transform.pos[0],
                    rm->relative_transform.pos[1],
                    rm->relative_transform.pos[2],
                    1.0f
                };

                vec4 v;
                vec4_mul_mat4x4(v, rel, tmp);
                vec4 pos;
                vec4_add(pos, et->transform.pos, v);
                mat4x4_translate_in_place(rm->mat_model, pos[0], pos[1], pos[2]);
            }



            /* Rotation */
            if (!(rm->mode == RENDER_MODEL_BILLBOARD))
            {
                mat4x4_identity(mat_tmp);
                mat4x4_rotate(mat_tmp, mat_tmp, 1.0f, 0.0f, 0.0f, et->transform.rot[0]);
                mat4x4_rotate(mat_tmp, mat_tmp, 0.0f, 1.0f, 0.0f, et->transform.rot[1]);
                mat4x4_rotate(mat_tmp, mat_tmp, 0.0f, 0.0f, 1.0f, et->transform.rot[2]);
                mat4x4_mul(rm->mat_model, rm->mat_model, mat_tmp);
            }

            /* Scaling */ // Scaalaus siiretty ennen rotatiota t�h�n.
            mat4x4_scale_aniso(rm->mat_model,
                rm->mat_model,
                et->transform.scale[0] * rm->relative_transform.scale[0],
                et->transform.scale[1] * rm->relative_transform.scale[1],
                et->transform.scale[2] * rm->relative_transform.scale[2]);

            /* Relative transform */
            if (!(rm->mode == RENDER_MODEL_BILLBOARD))
            {
                mat4x4_translate_in_place(rm->mat_model,
                    rm->relative_transform.pos[0],
                    rm->relative_transform.pos[1],
                    rm->relative_transform.pos[2]);
                mat4x4_identity(mat_tmp);
                mat4x4_rotate(mat_tmp, mat_tmp, 1.0f, 0.0f, 0.0f, rm->relative_transform.rot[0]);
                mat4x4_rotate(mat_tmp, mat_tmp, 0.0f, 0.0f, 1.0f, rm->relative_transform.rot[2]);
                mat4x4_rotate(mat_tmp, mat_tmp, 0.0f, 1.0f, 0.0f, rm->relative_transform.rot[1]);

                mat4x4_mul(rm->mat_model, rm->mat_model, mat_tmp);
            }

            /* Copy light data to the render model */
            memcpy(rm->directional_lights, directional_lights,
                MAX_ENTITY_DIRECTIONAL_LIGHTS * sizeof(DirectionalLightComponent*));
            memcpy(rm->point_lights, point_lights,
                MAX_ENTITY_POINT_LIGHTS * sizeof(PointLightComponent*));
            rm->light_count = light_count;
        }
    }
}

void
Scene3D_updateModels(Scene3D *scene)
{
    if (scene->entities.models.num_reserved > 4)
    {
        uint num_tests          = scene->entities.models.num_reserved;
        uint num_threads        = MAX(engine.thread_pool.num_threads,
                                        scene->entities.models.num_reserved);
        uint tests_per_thread   = num_tests / num_threads;
        uint last_addition      = num_tests % num_threads;

        ASSERT(scene->max_work_ranges >= num_threads);

        for (uint i = 0; i < num_threads; ++i)
        {
            scene->work_ranges[i].scene = scene;
            scene->work_ranges[i].begin = i * tests_per_thread;
            scene->work_ranges[i].end   = scene->work_ranges[i].begin + tests_per_thread;
            if (i == num_threads - 1) scene->work_ranges[i].end += last_addition;
            createThreadedJob(updateModelsForEntityRange, &scene->work_ranges[i]);
        }

        while (ThreadPool_haveUnfinishedWork(&engine.thread_pool))
            ThreadPool_doWork(&engine.thread_pool);
    }
    else if (scene->entities.models.num_reserved > 0)
    {
        scene->work_ranges[0].scene = scene;
        scene->work_ranges[0].begin = 0;
        scene->work_ranges[0].end   = scene->entities.models.num_reserved;
        updateModelsForEntityRange(&scene->work_ranges[0]);
    }
}

void
Scene3D_updateLightPositions(Scene3D *scene, double dt)
{
    /* Update lights separatenly since there's likely much less of them
     * compared to any other components */
    {
        EntityTransform *et;

        for (DirectionalLightComponent *l = scene->entities.lights.components;
             l < &scene->entities.lights.components[
                scene->entities.lights.num_reserved];
             ++l)
        {
            et = &scene->entities.transforms[l->transform_index];
            vec3_add(l->position, et->transform.pos, l->offset);
        }

        for (PointLightComponent *l = scene->entities.point_lights.components;
             l < &scene->entities.point_lights.components[
                scene->entities.point_lights.num_reserved];
             ++l)
        {
            et = &scene->entities.transforms[l->transform_index];
            vec3_add(l->cached_position, et->transform.pos, l->offset);
        }
    }
}

void
updatePhysicsForEntityRange(void *args)
{
    SceneWorkRange *range_struct    = (SceneWorkRange*)args;
    Scene3D *scene                  = range_struct->scene;
    PhysicsComponentPool *pool      = &range_struct->scene->entities.physics;
    double dt                       = range_struct->dt;

    EntityTransform *et;
    vec3 friction_dir;

    /* Velocity and other transformation calculations */
    for (PhysicsComponent *p = &pool->components[range_struct->begin];
        p < &pool->components[range_struct->end];
        ++p)
    {
        if (PhysicsComponent_isActive(p))
        {
            /* Move the entity */
            {
                et = &scene->entities.transforms[p->transform_index];

                et->transform.pos[0] = et->transform.pos[0] + p->velocity[0] * (float)dt;
                et->transform.pos[1] = et->transform.pos[1] + p->velocity[1] * (float)dt;
                et->transform.pos[2] = et->transform.pos[2] + p->velocity[2] * (float)dt;

                if (et->transform.pos[1] > scene->physics_limits.max_y_value)
                {
                    et->transform.pos[1] = scene->physics_limits.max_y_value;
                }

                if (et->transform.pos[1] <= scene->physics_limits.min_y_value)
                {
                    et->transform.pos[1] = scene->physics_limits.min_y_value;
                    p->velocity[1] = 0;
                }

                PhysicsComponent_updateColliderTransforms(p, et);
            }

            if (p->gravity_active)
            {
                if (scene->physics_limits.gravity_direction[0] != 0.0f)
                    p->velocity[0] += scene->physics_limits.gravity_direction[0] * \
                        scene->physics_limits.gravity_strength * (float)dt;
                if (scene->physics_limits.gravity_direction[1] != 0.0f)
                    p->velocity[1] += scene->physics_limits.gravity_direction[1] * \
                         scene->physics_limits.gravity_strength * (float)dt;
                if (scene->physics_limits.gravity_direction[2] != 0.0f)
                    p->velocity[2] += scene->physics_limits.gravity_direction[2] * \
                        scene->physics_limits.gravity_strength * (float)dt;
            }

            vec3_copy(friction_dir, p->velocity);

            p->velocity[0] += p->acceleration[0] * (float)dt;
            p->velocity[1] += p->acceleration[1] * (float)dt;
            p->velocity[2] += p->acceleration[2] * (float)dt;

            p->velocity[0] += (-friction_dir[0] * p->friction) * (float)dt;
            if (!p->gravity_active)
                p->velocity[1] += (-friction_dir[1] * p->friction) * (float)dt;
            p->velocity[2] += (-friction_dir[2] * p->friction) * (float)dt;

            if (p->velocity[0] <= FRICTION_STOPPING_LIMIT && p->velocity[0] \
                >= -FRICTION_STOPPING_LIMIT)
                p->velocity[0] = 0.0f;
            if (p->velocity[1] <= FRICTION_STOPPING_LIMIT && p->velocity[1] \
                >= -FRICTION_STOPPING_LIMIT)
                p->velocity[1] = 0.0f;
            if (p->velocity[2] <= FRICTION_STOPPING_LIMIT && p->velocity[2] \
                >= -FRICTION_STOPPING_LIMIT)
                p->velocity[2] = 0.0f;
        }
    }
}

void
Scene3D_updatePhysics(Scene3D *scene, double dt)
{
    if (scene->entities.physics.num_reserved <= 0) return;

    if (scene->entities.physics.num_reserved > 1)
    {
        uint num_tests          = scene->entities.physics.num_reserved;
        uint num_threads        = MAX(engine.thread_pool.num_threads,
                                        scene->entities.physics.num_reserved);
        uint tests_per_thread   = num_tests / num_threads;
        uint last_addition      = num_tests % num_threads;

        ASSERT(scene->max_work_ranges >= num_threads);

        for (uint i = 0; i < num_threads; ++i)
        {
            scene->work_ranges[i].scene = scene;
            scene->work_ranges[i].begin = i * tests_per_thread;
            scene->work_ranges[i].end   = scene->work_ranges[i].begin + tests_per_thread;
            scene->work_ranges[i].dt    = dt;
            if (i == num_threads - 1) scene->work_ranges[i].end += last_addition;
            createThreadedJob(updatePhysicsForEntityRange, &scene->work_ranges[i]);
        }

        while (ThreadPool_haveUnfinishedWork(&engine.thread_pool))
            ThreadPool_doWork(&engine.thread_pool);
    }
    else if (scene->entities.physics.num_reserved == 1)
    {
        scene->work_ranges[0].scene = scene;
        scene->work_ranges[0].begin = 0;
        scene->work_ranges[0].end   = 1;
        scene->work_ranges[0].dt    = dt;
        updatePhysicsForEntityRange(&scene->work_ranges[0]);
    }
}

#define MOVE_SINGLE_COLLIDING_ENTITY(et, normal, penetration) \
    et->transform.pos[0] = et->transform.pos[0] - normal[0] * penetration; \
    et->transform.pos[1] = et->transform.pos[1] - normal[1] * penetration; \
    et->transform.pos[2] = et->transform.pos[2] - normal[2] * penetration;

#define REDUCE_VELOCITY_BY_NORMAL(pc, normal) \
		if(PhysicsComponent_isActive(pc)) \
		{ \
            if ((pc->velocity[0] < 0.0f && normal[0] < 0.0f) \
            || (pc->velocity[0] > 0.0f && normal[0] > 0.0f)) \
                pc->velocity[0] = 0.0f; \
            if ((pc->velocity[1] < 0.0f && normal[1] < 0.0f) \
            || (pc->velocity[1] > 0.0f && normal[1] > 0.0f)) \
                pc->velocity[1] = 0.0f; \
            if ((pc->velocity[2] < 0.0f && normal[2] < 0.0f) \
            || (pc->velocity[2] > 0.0f && normal[2] > 0.0f)) \
                pc->velocity[2] = 0.0f; \
            if ((pc->acceleration[0] < 0.0f && normal[0] < 0.0f) \
            || (pc->acceleration[0] > 0.0f && normal[0] > 0.0f)) \
                pc->acceleration[0] = 0.0f; \
            if ((pc->acceleration[1] < 0.0f && normal[1] < 0.0f) \
            || (pc->acceleration[1] > 0.0f && normal[1] > 0.0f)) \
                pc->acceleration[1] = 0.0f; \
            if ((pc->acceleration[2] < 0.0f && normal[2] < 0.0f) \
            || (pc->acceleration[2] > 0.0f && normal[2] > 0.0f)) \
                pc->acceleration[2] = 0.0f; \
		}

#define REDUCE_PHYSICS_COMPONENT_MOVEMENT_BY_NORMAL(p, normal) \
{ \
    if ((p->velocity[0] < 0.0f && normal[0] > 0.0f) \
    ||  (p->velocity[0] > 0.0f && normal[0] < 0.0f)) \
    { \
        p->velocity[0]      = 0.0f; \
        p->acceleration[0]  = 0.0f; \
    } \
    if ((p->velocity[1] < 0.0f && normal[1] > 0.0f) \
    ||  (p->velocity[1] > 0.0f && normal[1] < 0.0f)) \
    { \
        p->velocity[1]      = 0.0f; \
        p->acceleration[1]  = 0.0f; \
    } \
    if ((p->velocity[2] < 0.0f && normal[2] > 0.0f) \
    ||  (p->velocity[2] > 0.0f && normal[2] < 0.0f)) \
    { \
        p->velocity[2]      = 0.0f; \
        p->acceleration[2]  = 0.0f; \
    } \
}

#define TRY_CALL_STATIC_COLLISION_CALLBACK(component, e, \
    collider_type, collider, \
    normal, scene) \
{ \
    if (component->staticCollisionCallback \
    && !(*collider_collided) \
    && !GET_BITFLAG(collider->flags, char, \
        COLLIDER_FLAG_WAS_COLLIDING_WITH_STATIC_GEOMETRY)) \
    { \
        if (!(*e)) *e = &scene->entities.entities[component->entity_index]; \
        component->staticCollisionCallback(component, *e, collider_type, \
            collider->id, normal, COLLISION_ENTER); \
    } \
}

static void
testBoxColliderWithNodeAndChildren(BoxCollider *b, AABB3D *b_aabb, CollisionWorldNode *n,
    PhysicsComponent *p, EntityTransform **et, Entity **e, Scene3D *scene,
    bool32 *collider_collided)
{
    for (CollisionTriangle *t = n->triangles; t; t = t->next)
    {
        if (testOBB3DTriangle(&b->data, t->data.points[0], t->data.points[1],
            t->data.points[2], t->data.normal, t->data.normal))
        {
            REDUCE_PHYSICS_COMPONENT_MOVEMENT_BY_NORMAL(p, t->data.normal);

            float penetration = computeOBB3DTrianglePenetration(&b->data, &t->data);

            if (!(*et)) *et = &scene->entities.transforms[p->transform_index];

            MOVE_SINGLE_COLLIDING_ENTITY((*et), t->data.normal, -penetration * 0.025f);
            PhysicsComponent_updateColliderTransforms(p, *et);

            TRY_CALL_STATIC_COLLISION_CALLBACK(p, e, COLLIDER_TYPE_OBB, b,
                t->data.normal, scene);

            *collider_collided = 1;
        }
    }

    if (!n->children[0])
        return;

    AABB3D node_aabb;

    for (CollisionWorldNode **cn = n->children; cn < &n->children[8]; ++cn)
    {
        if ((*cn)->triangles
        || GET_BITFLAG((*cn)->flags, char, CWNODE_CHILDREN_HAVE_TRIANGLES))
        {
            CollisionWorldNode_getAABB3D(*cn, &node_aabb);

            if (!testAABB3DAABB3D(b_aabb, &node_aabb))
                continue;

            testBoxColliderWithNodeAndChildren(b, b_aabb, *cn, p, et, e, scene,
                collider_collided);
        }
    }
}

static void
testBoxColliderWithNodeAndParents(BoxCollider *b, CollisionWorldNode *n,
    PhysicsComponent *p, EntityTransform **et, Entity **e, Scene3D *scene,
    bool32 *collider_collided)
{
    for (CollisionTriangle *t = n->triangles; t; t = t->next)
    {
        if (testOBB3DTriangle(&b->data, t->data.points[0], t->data.points[1],
            t->data.points[2], t->data.normal, t->data.normal))
        {
            REDUCE_PHYSICS_COMPONENT_MOVEMENT_BY_NORMAL(p, t->data.normal);

            float penetration = computeOBB3DTrianglePenetration(&b->data, &t->data);

            if (!(*et)) *et = &scene->entities.transforms[p->transform_index];

            MOVE_SINGLE_COLLIDING_ENTITY((*et), t->data.normal, -penetration * 0.025f);
            PhysicsComponent_updateColliderTransforms(p, *et);

            TRY_CALL_STATIC_COLLISION_CALLBACK(p, e, COLLIDER_TYPE_OBB, b,
                t->data.normal, scene);

            *collider_collided = 1;
        }
    }

    if (n->parent)
        testBoxColliderWithNodeAndParents(b, n->parent, p, et, e, scene,
            collider_collided);
}

static void
testSphereColliderWithNodeAndChildren(SphereCollider *s, AABB3D *s_aabb,
    CollisionWorldNode *n, PhysicsComponent *p, EntityTransform **et,
    Entity **e, Scene3D *scene, bool32 *collider_collided)
{
    vec3 point;
    float penetration;
    vec3 normal;

    for (CollisionTriangle *t = n->triangles; t; t = t->next)
    {
        if (testSphere3DTriangleAndGetData(&s->data, t->data.points[0],
            t->data.points[1], t->data.points[2], point, &penetration))
        {
            REDUCE_PHYSICS_COMPONENT_MOVEMENT_BY_NORMAL(p, t->data.normal);

            if (!(*et)) *et = &scene->entities.transforms[p->transform_index];

            MOVE_SINGLE_COLLIDING_ENTITY((*et), t->data.normal, penetration);
            PhysicsComponent_updateColliderTransforms(p, *et);

            TRY_CALL_STATIC_COLLISION_CALLBACK(p, e, COLLIDER_TYPE_SPHERE,
                s, t->data.normal, scene);

            *collider_collided = 1;
        }
    }

    for (StaticBoxCollider *sbc = n->static_box_colliders; sbc; sbc = sbc->next)
    {
        if (testSphere3DOBB3DAndGetData(&s->data, &sbc->data, point, normal, &penetration))
        {
            REDUCE_PHYSICS_COMPONENT_MOVEMENT_BY_NORMAL(p, normal);

            if (!(*et)) *et = &scene->entities.transforms[p->transform_index];

            MOVE_SINGLE_COLLIDING_ENTITY((*et), normal, penetration);
            REDUCE_VELOCITY_BY_NORMAL(p, normal);
            PhysicsComponent_updateColliderTransforms(p, *et);

            TRY_CALL_STATIC_COLLISION_CALLBACK(p, e, COLLIDER_TYPE_SPHERE, s, normal, scene);

            *collider_collided = 1;
        }
    }

    if (!n->children[0])
    {
        return;
    }

    AABB3D node_aabb;

    for (CollisionWorldNode **cn = n->children; cn < &n->children[8]; ++cn)
    {
        if ((*cn)->triangles
        || GET_BITFLAG((*cn)->flags, char, CWNODE_CHILDREN_HAVE_TRIANGLES))
        {
            CollisionWorldNode_getAABB3D(*cn, &node_aabb);

            if (!testAABB3DAABB3D(&node_aabb, s_aabb))
                continue;

            testSphereColliderWithNodeAndChildren(s, s_aabb, *cn, p,et, e,
                scene, collider_collided);
        }

        if((*cn)->static_box_colliders
            || GET_BITFLAG((*cn)->flags, char, CWNODE_CHILDREN_HAVE_STATIC_BOX_COLLIDERS))
        {
            CollisionWorldNode_getAABB3D(*cn, &node_aabb);

            if (!testAABB3DAABB3D(&node_aabb, s_aabb))
                continue;

            testSphereColliderWithNodeAndChildren(s, s_aabb, *cn, p, et, e,
                scene, collider_collided);
        }
    }
}

static void
testSphereColliderWithNodeAndParents(SphereCollider *s, CollisionWorldNode *n,
    PhysicsComponent *p, EntityTransform **et, Entity **e, Scene3D *scene,
    bool32 *collider_collided)
{
    vec3 point;
    vec3 normal;
    float penetration;

    for (CollisionTriangle *t = n->triangles; t; t = t->next)
    {
        if (testSphere3DTriangleAndGetData(&s->data, t->data.points[0],
            t->data.points[1], t->data.points[2], point, &penetration))
        {
            REDUCE_PHYSICS_COMPONENT_MOVEMENT_BY_NORMAL(p, t->data.normal);

            if (!(*et)) *et = &scene->entities.transforms[p->transform_index];

            MOVE_SINGLE_COLLIDING_ENTITY((*et), t->data.normal, penetration);
            PhysicsComponent_updateColliderTransforms(p, *et);

            TRY_CALL_STATIC_COLLISION_CALLBACK(p, e, COLLIDER_TYPE_SPHERE,
                s, t->data.normal, scene);

            *collider_collided = 1;
        }
    }

    for (StaticBoxCollider *sbc = n->static_box_colliders; sbc; sbc = sbc->next)
    {
        if (testSphere3DOBB3DAndGetData(&s->data, &sbc->data, point, normal, &penetration))
        {
            REDUCE_PHYSICS_COMPONENT_MOVEMENT_BY_NORMAL(p, normal);

            if (!(*et)) *et = &scene->entities.transforms[p->transform_index];

            MOVE_SINGLE_COLLIDING_ENTITY((*et), normal, penetration);
            REDUCE_VELOCITY_BY_NORMAL(p, normal);
            PhysicsComponent_updateColliderTransforms(p, *et);

            TRY_CALL_STATIC_COLLISION_CALLBACK(p, e, COLLIDER_TYPE_SPHERE, s, normal, scene);

            *collider_collided = 1;
        }
    }

    if (n->parent)
    {
        testSphereColliderWithNodeAndParents(s, n->parent, p, et, e, scene,
            collider_collided);
    }
}

#define ON_COLLIDER_NOT_COLLIDED(collider, component, collider_type) \
{ \
    if (component->staticCollisionCallback) \
    { \
        if (!e) e = &scene->entities.entities[component->entity_index]; \
        component->staticCollisionCallback(component, e, \
            collider_type, collider->id, 0, COLLISION_END); \
    } \
    SET_BITFLAG_OFF(collider->flags, char, \
        COLLIDER_FLAG_WAS_COLLIDING_WITH_STATIC_GEOMETRY); \
}

void
doStaticCollisionForEntityRange(void *range_struct_ptr)
{
    SceneWorkRange *range_struct    = (SceneWorkRange*)range_struct_ptr;
    Scene3D *scene                  = range_struct->scene;
    uint begin                      = range_struct->begin;
    uint end                        = range_struct->end;

    CollisionWorldNode  *node;
    EntityTransform     *et;
    Entity              *e;
    bool32              collider_collided;

    for (PhysicsComponent *p = &scene->entities.physics.components[begin];
         p < &scene->entities.physics.components[end];
         ++p)
    {
        if (PhysicsComponent_isStatic(p)) continue;

        et  = 0;
        e   = 0;

        float radius;
        AABB3D b_aabb;

        for (BoxCollider *b = p->box_colliders; b; b = b->next)
        {
            if (GET_BITFLAG(b->flags, char, COLLIDER_FLAG_IS_INACTIVE))
                continue;

            if (GET_BITFLAG(b->flags, char, COLLIDER_FLAG_IGNORE_STATIC_GEOMETRY))
                continue;

            collider_collided = 0;

            radius = OBB3D_getRadius(&b->data);
            computeSphereAABB(&b_aabb, b->data.center, radius);

            node = CollisionWorld_getNodeByObjectRadius(b->data.center,
                scene->collision_world.tree, OBB3D_getRadius(&b->data));

            testBoxColliderWithNodeAndChildren(b, &b_aabb, node, p, &et, &e,
                scene, &collider_collided);

            if (node->parent)
            {
                testBoxColliderWithNodeAndParents(b, node->parent, p, &et, &e,
                    scene, &collider_collided);
            }

            /* Flag the collider correctly for collision callbacks */
            if (!collider_collided)
            {
                ON_COLLIDER_NOT_COLLIDED(b, p, COLLIDER_TYPE_OBB);
            }
            else
            {
                SET_BITFLAG_ON(b->flags, char,
                    COLLIDER_FLAG_WAS_COLLIDING_WITH_STATIC_GEOMETRY);
            }
        }

        AABB3D s_aabb;

        for (SphereCollider *s = p->sphere_colliders; s; s = s->next)
        {
            if (GET_BITFLAG(s->flags, char, COLLIDER_FLAG_IS_INACTIVE))
                continue;

            if (GET_BITFLAG(s->flags, char, COLLIDER_FLAG_IGNORE_STATIC_GEOMETRY))
                continue;

            collider_collided = 0;

            computeSphereAABB(&s_aabb, s->data.center, s->data.radius);

            node = CollisionWorld_getNodeByObjectRadius(s->data.center,
                scene->collision_world.tree, s->data.radius);

            testSphereColliderWithNodeAndChildren(s, &s_aabb, node, p, &et, &e,
                scene, &collider_collided);

            if (node->parent)
            {
                testSphereColliderWithNodeAndParents(s, node->parent, p,
                    &et, &e, scene, &collider_collided);
            }

            /* Flag the collider correctly for collision callbacks */
            if (!collider_collided)
            {
                ON_COLLIDER_NOT_COLLIDED(s, p, COLLIDER_TYPE_SPHERE);
            }
            else
            {
                SET_BITFLAG_ON(s->flags, char,
                    COLLIDER_FLAG_WAS_COLLIDING_WITH_STATIC_GEOMETRY);
            }
        }
    }
}

static void
Scene3D_doStaticCollision(Scene3D *scene)
{
    if (!scene->collision_world.tree) return;

    uint num_tests          = scene->entities.physics.num_reserved;
    if (num_tests < 1) {return;}

    uint num_threads        = MAX(engine.thread_pool.num_threads,
                                    scene->entities.physics.num_reserved);
    uint tests_per_thread   = num_tests / num_threads;
    uint last_addition      = num_tests % num_threads;

    ASSERT(scene->max_work_ranges >= num_threads);

    for (uint i = 0; i < num_threads; ++i)
    {
        scene->work_ranges[i].scene = scene;
        scene->work_ranges[i].begin = i * tests_per_thread;
        scene->work_ranges[i].end   = scene->work_ranges[i].begin + tests_per_thread;
        if (i == num_threads - 1) scene->work_ranges[i].end += last_addition;
        createThreadedJob(doStaticCollisionForEntityRange, &scene->work_ranges[i]);
    }

    while (ThreadPool_haveUnfinishedWork(&engine.thread_pool))
        ThreadPool_doWork(&engine.thread_pool);
}

void
Scene3D_updateCollisions(Scene3D *scene)
{
    /* Preliminary collision detection after
     * updating all the physics components */

#define IS_TRIGGER_ONLY(collider) \
    (GET_BITFLAG(collider->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY))

/* TODO: DO MORE EXTENSIVE TESTING FOR EVENT GENERATING STUFF HERE ALREADY !!! */

#define PUSH_TRIGGER_EVENT_AND_CACHE_ENTRY(component_a, component_b, \
    collider_a, collider_a_type, \
    collider_b, collider_b_type) \
{ \
    CollisionCacheEntry *entry = CollisionCache_getEntry( \
        &scene->entities.collision_cache, \
        component_a->entity_index, component_b->entity_index, \
        collider_a->id, collider_a_type, \
        collider_b->id, collider_b_type); \
 \
    if (!entry) \
    { \
        static vec3 dummy_vec = {0.0f, 0.0f, 0.0f}; \
 \
        CollisionCache_pushEntry( \
            &scene->entities.collision_cache, \
            component_a->entity_index, \
            component_b->entity_index, \
            collider_a->id, collider_a_type, \
            collider_b->id, collider_b_type); \
 \
        pushCollisionEnterEvent(scene, \
            component_a, component_b, \
            collider_a->id, collider_a_type, \
            collider_b->id, collider_b_type, \
            dummy_vec, collider_a->id, 0); \
    } \
    else \
    {\
        entry->timestamp = (uint32_t)getCurrentFrameNum(); \
    }\
}

    PhysicsComponentPool *pool = &scene->entities.physics;

    for (PhysicsComponent *p = pool->components;
        p < &pool->components[pool->num_reserved];
        ++p)
    {
        for (PhysicsComponent *p2 = pool->components;
             p2 < &pool->components[pool->num_reserved];
             ++p2)
        {
            if (p == p2 || p->entity_index > p2->entity_index)
                continue;

            if (PhysicsComponent_isStatic(p) && PhysicsComponent_isStatic(p2))
                continue;

            /* Box colliders */
            for (BoxCollider *b1 = p->box_colliders; b1; b1 = b1->next)
            {
                if (GET_BITFLAG(b1->flags, char, COLLIDER_FLAG_IS_INACTIVE))
                    continue;

                /* Preliminary box - box */
                for (BoxCollider *b2 = p2->box_colliders; b2; b2 = b2->next)
                {
                    if (GET_BITFLAG(b2->flags, char, COLLIDER_FLAG_IS_INACTIVE))
                        continue;

                    if (testOBB3DOBB3D(&b2->data, &b1->data))
                    {
                        if (!IS_TRIGGER_ONLY(b1) && !IS_TRIGGER_ONLY(b2))
                        {
                            CollisionCache_pushResolvablePair(
                                &scene->entities.collision_cache,
                                p, p2,
                                b1->id, COLLIDER_TYPE_OBB,
                                b2->id, COLLIDER_TYPE_OBB);
                        }
                        else
                        {
                            /* Preliminary test for OBBs is good enough for us
                             * to know the result is true */
                            PUSH_TRIGGER_EVENT_AND_CACHE_ENTRY(p, p2,
                                b1, COLLIDER_TYPE_OBB, b2, COLLIDER_TYPE_OBB);
                        }
                    }
                }

                /* Preliminary box-sphere */
                for (SphereCollider *s_b = p2->sphere_colliders;
                     s_b;
                     s_b = s_b->next)
                {
                    if (GET_BITFLAG(s_b->flags, char, COLLIDER_FLAG_IS_INACTIVE))
                        continue;

                    if (testSphere3DOBB3DCheap(&s_b->data, &b1->data))
                    {
                        if (!IS_TRIGGER_ONLY(b1) && !IS_TRIGGER_ONLY(s_b))
                        {
                            CollisionCache_pushResolvablePair(&scene->entities.collision_cache,
                                p, p2,
                                b1->id,  COLLIDER_TYPE_OBB,
                                s_b->id, COLLIDER_TYPE_SPHERE);
                        }
                        else
                        {
                            PUSH_TRIGGER_EVENT_AND_CACHE_ENTRY(p, p2,
                                b1,  COLLIDER_TYPE_OBB,
                                s_b, COLLIDER_TYPE_SPHERE);
                        }
                    }
                }
            }

            /* Sphere colliders */
            for (SphereCollider *s_a = p->sphere_colliders; s_a; s_a = s_a->next)
            {
                if (GET_BITFLAG(s_a->flags, char, COLLIDER_FLAG_IS_INACTIVE))
                    continue;

                /* Preliminary sphere-sphere */
                for (SphereCollider *s_b = p2->sphere_colliders; s_b; s_b = s_b->next)
                {
                    if (GET_BITFLAG(s_b->flags, char, COLLIDER_FLAG_IS_INACTIVE))
                        continue;

                    if (testSphere3DSphere3DCheap(&s_a->data, &s_b->data))
                    {
                        if (!IS_TRIGGER_ONLY(s_a) && !IS_TRIGGER_ONLY(s_b))
                        {
                            CollisionCache_pushResolvablePair(
                                &scene->entities.collision_cache,
                                p, p2,
                                s_a->id, COLLIDER_TYPE_SPHERE,
                                s_b->id, COLLIDER_TYPE_SPHERE);
                        }
                        else
                        {
                            PUSH_TRIGGER_EVENT_AND_CACHE_ENTRY(p, p2,
                                s_a, COLLIDER_TYPE_SPHERE,
                                s_b, COLLIDER_TYPE_SPHERE);
                        }
                    }
                }

                /* Preliminary sphere-box */
                for (BoxCollider *box_b = p2->box_colliders; box_b; box_b = box_b->next)
                {
                    if (GET_BITFLAG(box_b->flags, char, COLLIDER_FLAG_IS_INACTIVE))
                        continue;

                    if (testSphere3DOBB3DCheap(&s_a->data, &box_b->data))
                    {
                        if (!IS_TRIGGER_ONLY(s_a) && !IS_TRIGGER_ONLY(box_b))
                        {
                            CollisionCache_pushResolvablePair(
                                &scene->entities.collision_cache,
                                p, p2,
                                s_a->id,   COLLIDER_TYPE_SPHERE,
                                box_b->id, COLLIDER_TYPE_OBB);
                        }
                        else
                        {
                            PUSH_TRIGGER_EVENT_AND_CACHE_ENTRY(p, p2,
                                s_a,   COLLIDER_TYPE_SPHERE,
                                box_b, COLLIDER_TYPE_OBB);
                        }
                    }
                }
            }
        }
    }

#undef IS_TRIGGER_ONLY
#undef PUSH_TRIGGER_EVENT_AND_CACHE_ENTRY

    /* After the preliminary phase, do actual collision detection */

    /* Resolve separation of physics collisions */
    uint *num_resolvable_pairs = &scene->entities.collision_cache.num_resolvable_pairs;

    if (*num_resolvable_pairs > 0)
    {
        int num_rounds = 0;
        bool32 have_movables = 1;
        PhysicsComponent *component_a, *component_b;
        uint32_t collider_a, collider_b;
        uint8_t collider_a_type, collider_b_type;
        bool32 move_both;

        while (have_movables && num_rounds < 3)
        {
            num_rounds++;
            have_movables = 0;

            for (ResolvableColliderPair *pair = scene->entities.collision_cache.resolvable_pairs;
                 pair < &scene->entities.collision_cache.resolvable_pairs[*num_resolvable_pairs];
                 ++pair)
            {
                if (PhysicsComponent_isStatic(pair->component_a))
                {
                    component_a     = pair->component_b;
                    collider_a      = pair->collider_b;
                    collider_a_type = pair->collider_b_type;

                    component_b     = pair->component_a;
                    collider_b      = pair->collider_a;
                    collider_b_type = pair->collider_a_type;

                    move_both = 0;
                }
                else if (PhysicsComponent_isStatic(pair->component_b))
                {
                    component_a     = pair->component_a;
                    collider_a      = pair->collider_a;
                    collider_a_type = pair->collider_a_type;

                    component_b     = pair->component_b;
                    collider_b      = pair->collider_b;
                    collider_b_type = pair->collider_b_type;

                    move_both = 0;
                }
                else
                {
                    component_a     = pair->component_a;
                    collider_a      = pair->collider_a;
                    collider_a_type = pair->collider_a_type;

                    component_b     = pair->component_b;
                    collider_b      = pair->collider_b;
                    collider_b_type = pair->collider_b_type;

                    move_both = 1;
                }

/* MOVE_SINGLE_COLLIDING_ENTITY defined higher above */

#define MOVE_BOTH_COLLIDING_ENTITIES(et_a, et_b, normal, penetration) \
        et_a->transform.pos[0] = et_a->transform.pos[0] - (normal[0] * penetration) * 0.5f; \
        et_a->transform.pos[1] = et_a->transform.pos[1] - (normal[1] * penetration) * 0.5f; \
        et_a->transform.pos[2] = et_a->transform.pos[2] - (normal[2] * penetration) * 0.5f; \
\
        et_b->transform.pos[0] = et_b->transform.pos[0] + (normal[0] * penetration) * 0.5f; \
        et_b->transform.pos[1] = et_b->transform.pos[1] + (normal[1] * penetration) * 0.5f; \
        et_b->transform.pos[2] = et_b->transform.pos[2] + (normal[2] * penetration) * 0.5f;


#define PUSH_CACHE_ENTRY_AND_ENTER_EVENT_IF_NOT_EXISTS(component_a, component_b, \
    collider_a, collider_a_type, \
    collider_b, collider_b_type, \
    normal, penetration, normal_owner) \
{ \
    CollisionCacheEntry *entry = CollisionCache_getEntry( \
        &scene->entities.collision_cache, \
        component_a->entity_index, \
        component_b->entity_index, \
        collider_a->id, collider_a_type, \
        collider_b->id, collider_b_type); \
 \
    if (!entry) \
    { \
        CollisionCache_pushEntry( \
            &scene->entities.collision_cache, \
            component_a->entity_index, \
            component_b->entity_index, \
            collider_a->id, collider_a_type, \
            collider_b->id, collider_b_type); \
 \
        pushCollisionEnterEvent(scene, \
            component_a, component_b, \
            collider_a->id, collider_a_type, \
            collider_b->id, collider_b_type, \
            normal, normal_owner->id, penetration); \
    } \
    else \
        entry->timestamp = (uint32_t)getCurrentFrameNum(); \
}

                switch (collider_a_type)
                {
                    case COLLIDER_TYPE_OBB:
                    {
                        switch (collider_b_type)
                        {
                            /* Resolve OBB - OBB */
                            case COLLIDER_TYPE_OBB:
                            {
                                BoxCollider *box_a =
                                    PhysicsComponent_getBoxColliderByHash(
                                        component_a, collider_a);
                                BoxCollider *box_b =
                                    PhysicsComponent_getBoxColliderByHash(
                                        component_b, collider_b);
                                if (!box_a || !box_b) break;

                                int     collision_type;
                                vec3    normal;
                                float   penetration;

                                /* Other's box colliders */
                                collision_type = testOBB3DOBB3DAndGetData(&box_b->data,
                                    &box_a->data, normal, &penetration);

                                if (collision_type)
                                    have_movables = 1;
                                else
                                    break;

                                if (!move_both)
                                {
									REDUCE_VELOCITY_BY_NORMAL(component_a, normal);
                                    EntityTransform *et =
                                        &scene->entities.transforms[
                                            component_a->transform_index];
                                    MOVE_SINGLE_COLLIDING_ENTITY(et, normal,penetration);
                                    PhysicsComponent_updateColliderTransforms(
                                        component_a, et);
                                }
                                else
                                {
                                    EntityTransform *et_a =
                                        &scene->entities.transforms[
                                            component_a->transform_index];
                                    EntityTransform *et_b =
                                        &scene->entities.transforms[
                                            component_b->transform_index];
									REDUCE_VELOCITY_BY_NORMAL(component_a, normal);
									REDUCE_VELOCITY_BY_NORMAL(component_b, normal);
                                    MOVE_BOTH_COLLIDING_ENTITIES(et_a, et_b,
                                        normal, penetration);
                                    PhysicsComponent_updateColliderTransforms(
                                        component_a, et_a);
                                    PhysicsComponent_updateColliderTransforms(
                                        component_b, et_b);
                                }

                                PUSH_CACHE_ENTRY_AND_ENTER_EVENT_IF_NOT_EXISTS(
                                    component_a, component_b,
                                    box_a, COLLIDER_TYPE_OBB,
                                    box_b, COLLIDER_TYPE_OBB,
                                    normal, penetration, box_b);
                            }
                                break;
                            /* Resolve OBB-sphere */
                            case COLLIDER_TYPE_SPHERE:
                            {
                                BoxCollider *box_a =
                                    PhysicsComponent_getBoxColliderByHash(
                                        component_a, collider_a);

                                SphereCollider *sp_b =
                                    PhysicsComponent_getSphereColliderByHash(
                                        component_b, collider_b);

                                vec3 point, normal;
                                float penetration;

                                if (!testSphere3DOBB3DAndGetData(&sp_b->data,
                                        &box_a->data, point, normal, &penetration))
                                    break;

                                EntityTransform *et =
                                    &scene->entities.transforms[
                                        component_b->transform_index];

                                /* TODO: MAKE MOVE BOTH ENTITIES */
                                MOVE_SINGLE_COLLIDING_ENTITY(et, normal, penetration);
                                REDUCE_VELOCITY_BY_NORMAL(component_b, normal);
                                PhysicsComponent_updateColliderTransforms(component_b, et);

                                PUSH_CACHE_ENTRY_AND_ENTER_EVENT_IF_NOT_EXISTS(
                                    component_a, component_b,
                                    box_a, COLLIDER_TYPE_OBB,
                                    sp_b,  COLLIDER_TYPE_SPHERE,
                                    normal, penetration, box_a);
                            }
                                break;
                        }
                    }
                        break;
                    case COLLIDER_TYPE_SPHERE:
                    {
                        switch (collider_b_type)
                        {
                            /* Resolve sphere-sphere */
                            case COLLIDER_TYPE_SPHERE:
                            {
                                SphereCollider *sp_a =
                                    PhysicsComponent_getSphereColliderByHash(
                                    component_a, collider_a);
                                SphereCollider *sp_b =
                                    PhysicsComponent_getSphereColliderByHash(
                                    component_b, collider_b);

                                vec3 direction_from_a;
                                float penetration;

                                if (testSphere3DSphere3DAndGetData(&sp_a->data,
                                        &sp_b->data, direction_from_a,
                                        &penetration))
                                {
                                    if (!move_both)
                                    {
                                        EntityTransform *et =
                                            &scene->entities.transforms[
                                                component_a->transform_index];
                                        MOVE_SINGLE_COLLIDING_ENTITY(et,
                                            direction_from_a, penetration);
                                        PhysicsComponent_updateColliderTransforms(
                                            component_a, et);
                                    }
                                    else
                                    {
                                        EntityTransform *et_a =
                                            &scene->entities.transforms[
                                                component_a->transform_index];
                                        EntityTransform *et_b =
                                            &scene->entities.transforms[
                                                component_b->transform_index];
                                        MOVE_BOTH_COLLIDING_ENTITIES(
                                            et_a, et_b, direction_from_a,
                                            penetration);
                                        PhysicsComponent_updateColliderTransforms(
                                            component_a, et_a);
                                        PhysicsComponent_updateColliderTransforms(
                                            component_b, et_b);
                                    }

                                    PUSH_CACHE_ENTRY_AND_ENTER_EVENT_IF_NOT_EXISTS(
                                        component_a, component_b,
                                        sp_a, COLLIDER_TYPE_SPHERE,
                                        sp_b, COLLIDER_TYPE_SPHERE,
                                        direction_from_a, penetration, sp_a);
                                }
                                else
                                    break;
                            }
                                break;
                            /* Resolve sphere-OBB */
                            case COLLIDER_TYPE_OBB:
                            {
                                SphereCollider *sp_a =
                                    PhysicsComponent_getSphereColliderByHash(
                                        component_a, collider_a);
                                BoxCollider *box_b =
                                    PhysicsComponent_getBoxColliderByHash(
                                        component_b, collider_b);

                                vec3 point, normal;
                                float penetration;

                                if (!testSphere3DOBB3DAndGetData(&sp_a->data,
                                    &box_b->data, point, normal, &penetration))
                                    break;

                                EntityTransform *et =
                                    &scene->entities.transforms[
                                        component_a->transform_index];

                                MOVE_SINGLE_COLLIDING_ENTITY(et, normal, penetration);
                                REDUCE_VELOCITY_BY_NORMAL(component_a, normal);
                                PhysicsComponent_updateColliderTransforms(component_a, et);

                                PUSH_CACHE_ENTRY_AND_ENTER_EVENT_IF_NOT_EXISTS(
                                    component_a, component_b,
                                    sp_a,  COLLIDER_TYPE_SPHERE,
                                    box_b, COLLIDER_TYPE_OBB,
                                    normal, penetration, box_b);
                            }
                                break;
                        }
                    }
                        break;
                }
            }
        }

        *num_resolvable_pairs = 0;
    }

#undef MOVE_SINGLE_COLLIDING_ENTITY
#undef MOVE_BOTH_COLLIDING_ENTITIES
#undef REDUCE_VELOCITY_BY_NORMAL
#undef PUSH_CACHE_ENTRY_AND_ENTER_EVENT_IF_NOT_EXISTS

    /* Remove old enties from the collision cache and generate end events.
     * Clear any old and no-longer valid cache entries. */
    for (PhysicsComponent *p = pool->components;
         p < &pool->components[pool->num_reserved];
         ++p)
    {
        CollisionCacheEntry **entry, *e;
        entry = &scene->entities.collision_cache.table[p->entity_index];
        uint32_t current_frame = (uint32_t)getCurrentFrameNum();

        while (*entry)
        {
            e = *entry;

            /* Remove if the entry was not updated this frame */
            if (e->timestamp != current_frame)
            {
                pushCollisionEndEvent(scene,
                    Entity_getPhysicsComponent(
                        &scene->entities.entities[p->entity_index]),
                    Entity_getPhysicsComponent(
                        &scene->entities.entities[e->entity_b_index]),
                    0, 0, 0, 0); /* TODO: push proper data */

                *entry = e->next; /* Removal */
                e->next = scene->entities.collision_cache.free;
                scene->entities.collision_cache.free = e;
            }
            else
                entry = &(*entry)->next;
        }
    }

    /* Handle events */
    {
        CollisionEvent *event;

        for (uint i = 0; i < scene->entities.num_collision_events; ++i)
        {
            event = &scene->entities.collision_events[i];

			if (event->type == COLLISION_ENTER)
				handleEntityPhysicsCollision(event);

            if (event->component_a->collisionCallback != 0)
            {
                event->component_a->collisionCallback(event->component_a, event->component_b,
                    EntityPool_getEntityByIndex(
                        &scene->entities, event->component_a->entity_index),
                    EntityPool_getEntityByIndex(
                        &scene->entities, event->component_b->entity_index),
                    event->collider_a_type, event->collider_b_type,
                    event->type,
                    event->collider_a, event->collider_b);
            }

            if (event->component_b->collisionCallback != 0)
            {
                event->component_b->collisionCallback(event->component_b, event->component_a,
                    EntityPool_getEntityByIndex(&scene->entities,
                        event->component_b->entity_index),
                    EntityPool_getEntityByIndex(&scene->entities,
                        event->component_a->entity_index),
                    event->collider_b_type, event->collider_a_type, event->type,
                    event->collider_b, event->collider_a);
            }
        }

        scene->entities.num_collision_events = 0;
    }

    Scene3D_doStaticCollision(scene);
}

void
Scene3D_updateScripts(Scene3D *scene)
{
    ScriptComponent *script;

    for (uint i = 0; i < scene->entities.scripts.num_reserved; ++i)
    {
        script = &scene->entities.scripts.components[i];

        if (script->callback != 0)
        {
            script->callback(&scene->entities.entities[script->entity_index],
                script->data);
        }
    }
}

int
AnimatorPool_init(AnimatorPool *animatorpool, Animator *memory, uint capacity)
{
    animatorpool->capacity = capacity;
    animatorpool->animators = memory;

    for (Animator *animator = animatorpool->animators;
        animator < &animatorpool->animators[capacity];
        ++animator)
    {
        animator->pool_prev = animator - 1;
        animator->pool_next = animator + 1;
        animator->model_animation = 0;
    }

    animatorpool->animators[0].pool_prev = 0;
    animatorpool->animators[capacity - 1].pool_next = 0;
    animatorpool->free = animatorpool->animators;
    animatorpool->reserved = 0;

    return 0;
}

void
Animator_playAnimation(Animator *animator, ModelAnimation *animation, bool32 loop)
{
    animator->model_animation = animation;
    animator->current_model_frame = 0;
    animator->loop = loop;
    animator->time_passed = 0.0f;
    animator->render_model->model = animation->frames[0].model;
    animator->render_model->material = animation->frames[0].material;
    animator->render_model->material_array = animation->frames[0].material_array;
}

void
PhysicsComponent_applyForce(PhysicsComponent *component,
    vec3 direction, float strength)
{
    vec3_add(component->velocity, component->velocity, direction);
    vec3_norm(component->velocity, component->velocity);
    float speed = vec3_len(component->velocity);
    speed += strength;
    vec3_scale(component->velocity, component->velocity, speed);
}

RenderModel *
ModelComponent_attachRenderModel(ModelComponent *component,
    Scene3D *scene, uint32_t id)
{
    RenderModel *rm = RenderModelPool_reserve(&scene->render_model_pool);
    if (!rm) return rm;

    RenderModel_giveId(rm, id);

    RenderModel **tail = &component->render_models;
    while (*tail) tail = &(*tail)->parent_next;

    *tail = rm;
    rm->parent_next = 0;
    rm->visible     = 1;

    return rm;
}

RenderModel *
ModelComponent_attachNamedRenderModel(ModelComponent *component,
    Scene3D *scene, const char *name)
{
    RenderModel *rm = RenderModelPool_reserve(&scene->render_model_pool);
    if (!rm) return rm;

    RenderModel_giveName(rm, name);

    RenderModel **tail = &component->render_models;
    while (*tail) tail = &(*tail)->parent_next;

    *tail = rm;
    rm->parent_next = 0;

    return rm;
}

Animator *
ModelComponent_attachAnimator(ModelComponent *component,
    Scene3D *scene, RenderModel *rendermodel, uint32_t id)
{
    Animator *a = AnimatorPool_reserve(&scene->animator_pool);
    if (!a)
    {
        DEBUG_PRINTF("Could not reserve animator from animatorpool!\n");
        return a;
    }
    Animator_giveId(a, id);

    Animator **tail = &component->animators;
    while (*tail) tail = &(*tail)->parent_next;

    *tail = a;
    a->parent_next = 0;
    a->render_model = rendermodel;
    a->speed = 1.0f;

    return a;
}

Animator *
ModelComponent_attachNamedAnimator(ModelComponent *component,
    Scene3D *scene, RenderModel *rendermodel, const char *name)
{
    Animator *a = AnimatorPool_reserve(&scene->animator_pool);
    if (!a) return a;

    Animator_giveName(a, name);

    Animator **tail = &component->animators;
    while (*tail) tail = &(*tail)->parent_next;

    *tail = a;
    a->parent_next = 0;
    a->render_model = rendermodel;
    a->speed = 1.0f;

    return a;
}

void
ScriptComponent_setCallback(ScriptComponent *script,
    void(*callback)(Entity *entity, void *data))
{
    script->callback = callback;
}

void *
ScriptComponent_allocateData(ScriptComponent *script, size_t size)
{
    ASSERT(!script->data);
    script->data = kmalloc(size);
    return script->data;
}

BoxCollider *
PhysicsComponent_attachBoxCollider(PhysicsComponent *component,
    Entity *entity,
    const char *identifier)
{
    BoxCollider **tail = &component->box_colliders;

    while (*tail)
    {
        tail = &(*tail)->next;
    }

    *tail = BoxColliderPool_reserve(
        &entity->scene->box_collider_pool,
        hashFromString(identifier));

    ASSERT(*tail);

    vec3_set((*tail)->dimensions, 0.5f, 0.5f, 0.5f);
    vec3_set((*tail)->position, 0.0f, 0.0f, 0.0f);
    vec3_set((*tail)->rotation, 0.0f, 0.0f, 0.0f);

	(*tail)->flags = 0;

    return *tail;
}

BoxCollider *
PhysicsComponent_getBoxCollider(PhysicsComponent *component,
    const char *identifier)
{
    uint32_t id = hashFromString(identifier);
    BoxCollider *tail = component->box_colliders;

    while (tail && tail->id != id)
        tail = tail->next;

    return tail;
}

BoxCollider *
PhysicsComponent_getBoxColliderByHash(PhysicsComponent *component,
    uint32_t hash)
{
    BoxCollider *tail = component->box_colliders;

    while (tail && tail->id != hash)
        tail = tail->next;

    return tail;
}

SphereCollider *
PhysicsComponent_attachSphereCollider(PhysicsComponent *component,
    Entity *entity,
    uint32_t identifier)
{
    SphereCollider **tail = &component->sphere_colliders;
    while (*tail) tail = &(*tail)->next;

    *tail = SphereColliderPool_reserve(
        &entity->scene->sphere_collider_pool,
        identifier);

    if (!(*tail))
    {
        DEBUG_PRINTF("Warning: Attempted to attach a sphere collider "
            "to entity %u, but ran out of them!\n", entity->id);
        return 0;
    }

    (*tail)->radius     = 0.5f;
	(*tail)->flags      = 0;

    (*tail)->entity_index = entity->index;

    return *tail;
}

SphereCollider *
PhysicsComponent_attachSphereColliderByName(PhysicsComponent *component,
    Entity *entity,
    const char *identifier)
{
    SphereCollider **tail = &component->sphere_colliders;
    while (*tail) tail = &(*tail)->next;

    *tail = SphereColliderPool_reserve(
        &entity->scene->sphere_collider_pool,
        hashFromString(identifier));

    if (!(*tail))
    {
        DEBUG_PRINTF("Warning: Attempted to attach a sphere collider "
            "to entity %u, but ran out of them!\n", entity->id);
        return 0;
    }

    (*tail)->radius = 0.5f;
    (*tail)->flags = 0;

    return *tail;
}

SphereCollider *
PhysicsComponent_getSphereCollider(PhysicsComponent *component,
    const char *identifier)
{
    uint32_t id = hashFromString(identifier);
    SphereCollider *tail = component->sphere_colliders;

    while (tail && tail->id != id)
        tail = tail->next;

    return tail;
}

SphereCollider *
PhysicsComponent_getSphereColliderByHash(PhysicsComponent *component,
    uint32_t hash)
{
    SphereCollider *tail = component->sphere_colliders;

    while (tail && tail->id != hash)
        tail = tail->next;

    return tail;
}

void
PhysicsComponent_applyImpactForce(PhysicsComponent *component, vec3 direction,
    float force)
{
    float v;
    for (int i = 0; i < 3; ++i)
    {
        v = direction[i] * force;
        component->velocity[i] = v;

#if 0
        if (v < 0)
        {
            min = -component->max_velocity;
            max = 0.f;
        }
        else
        {
            min = 0.f;
            max = component->max_velocity;
        }

        v = CLAMP(v, min, max);

        if (ABS(v) > ABS(component->velocity[i]))
            component->velocity[i] = v;
#endif
    }
}

void
RenderModelPool_init(RenderModelPool *pool, void *memory, uint num_max_models)
{
    pool->models    = (RenderModel*)memory;
    pool->capacity  = num_max_models;

    for (RenderModel *model = pool->models;
         model < &pool->models[num_max_models];
         ++model)
    {
        model->pool_prev = model - 1;
        model->pool_next = model + 1;
        model->model = 0;
        model->visible = 1;
        model->shader = SHADER3D_OPAQUE_GOURAUD;
    }

    pool->models[0].pool_prev = 0;
    pool->models[num_max_models - 1].pool_next = 0;
    pool->free = pool->models;
    pool->reserved = 0;
}
