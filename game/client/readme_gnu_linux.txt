Karistys

This GNU/Linux build only been tested using Manjaro (running kernel version
4.9.0).

Karistys is free software licenced under GPL version 3. The code is available
at gitlab.com/Partanen/kyssari. Binary downloads are available at
kuvis.itch.io/karistys.

Required libraries for running the application:
For the client, install SDL2, SDL2_image and SDL2_mixer using your package
manager of choice. For example, using pacman:
'pacman -S sdl2 sdl2-image sdl2-mixer'
For the server, the ncurses library is required, but it comes pre-installed
with most systems.

How to play:

Client:
	run ./karistys in the game's root directory.

Server:
	run ./karistys_server for a 'graphical' version, or './karistys-server -g'
	for the ncurses-based text mode version.

Using the server:
To play multiplayer, the karistys_server program must be running on one
machine. This machine can also be running a single client at the same time. Once the server program is running, press 'r' (in both, graphical or
curses mode) in the server program's window to enable networking.
Make sure the machine's firewall is correctly configured. Karistys uses the
ports 44344 and 44345 (UDP).
The server can hold up to 4 players at a time.

Connecting to a server on the client:
Karistys finds the IP address of the server it attempts to connect to from a
file called connect.conf that should reside in the same directory as the
binary. If it does not exist, you can create it.
The format of the file is as follows (without the quotation marks):
'hostname = 127.0.0.1' 
A '#' character marks a commented-out (ignored) line.
To finally connect to the server, press the "multiplayer" button the main
menu.

Single player:
The game includes a single player "demo" for trying out physics, movement and
weapons.

Client controls and keybinds:
Movement: 		WASD, mouse
Jump: 			space
Dash: 			right mouse button
Switch weapon: 	e
