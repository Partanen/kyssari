#include "memory.h"

static uint32_t _running_block_id = 0;

#define MIN_ALLOC_SIZE 4096

static inline void *
DynamicMemoryBlock_getEnd(DynamicMemoryBlock *block)
{
    return (void*)((char*)block->start + block->total_size);
}

static inline size_t
MemoryBlockHeader_getFreeSpace(MemoryBlockHeader *header, DynamicMemoryBlock *block)
{
    size_t end = header->next ? (size_t)(char*)header->next : \
        (size_t)(char*)DynamicMemoryBlock_getEnd(block);
    return end - (size_t)((char*)(header + 1) + header->size);
}

int
MemoryPool_init(MemoryPool *pool, size_t size)
{
    ASSERT(pool->start == 0);

    if (size < MIN_ALLOC_SIZE)
    {
        size = MIN_ALLOC_SIZE;
    }
    else
    {
        size_t remainder = size % MIN_ALLOC_SIZE;
        size = size + remainder;
    }

    pool->start     = sys_allocateMemory(size);
    if (!pool->start) return 1;

    pool->offset    = pool->start;

    if (pool->start)
    {
        pool->total_size = size;
        Mutex_init(&pool->mutex);
        return 0;
    }

    return 2;
}

void *
MemoryPool_allocateBlock(MemoryPool *pool, size_t size)
{
    void *ret;

    Mutex_lock(&pool->mutex);

    if (size <= ((size_t)(char*)pool->start + pool->total_size) - (size_t)(char*)pool->offset)
    {
        ret = pool->offset;
        pool->offset = (void*)((char*)pool->offset + size);
    }
    else
    {
        ret = 0;
    }

    Mutex_unlock(&pool->mutex);

    return ret;
};


int
DynamicMemoryBlock_init(DynamicMemoryBlock *block, MemoryPool *pool, size_t size)
{
    ASSERT(!block->start);

    block->start = MemoryPool_allocateBlock(pool, size);

    if (!block->start)
    {
        return 1;
    }

    block->total_size   = size;
    block->headers      = 0;

    Mutex_init(&block->mutex);
    memset(block->start, 0, size);

    block->id = _running_block_id++;

    return 0;
}

void
DynamicMemoryBlock_clear(DynamicMemoryBlock *block)
{
    if (!block) return;
    block->headers = 0;
}

static inline void *
DynamicMemoryBlock_mallocInner(DynamicMemoryBlock *block, size_t size)
{
    if (size == 0) return 0;

    size_t tot_size = size + sizeof(MemoryBlockHeader);
    if (tot_size > block->total_size) return 0;

    void *ret = 0;
    MemoryBlockHeader *header;
    size_t free_size;

    /* Other blocks already exist */
    if (block->headers)
    {
        /* There's free space at the beginning of the block */
        if (block->headers != (MemoryBlockHeader*)block->start
        && (size_t)((char*)block->headers - (char*)block->start) >= tot_size)
        {
            header                  = (MemoryBlockHeader*)block->start;
            header->size            = size;
            block->headers->prev    = header;
            header->next            = block->headers;
            header->prev            = 0;
            block->headers          = header;
            ret = (void*)(header + 1);

            #ifdef _DEBUG
            header->is_in_use = 1;
            #endif
        }
        /* Find a block that has free space at the end of it, right before
         * the next block (or alternatively, the last block */
        else
        {
            for (header = block->headers;
                 header;
                 header = header->next)
            {
                free_size = MemoryBlockHeader_getFreeSpace(header, block);
                if (free_size < tot_size) continue;

                /* Create a new header after the space of this header */
                MemoryBlockHeader *new_header =
                    (MemoryBlockHeader*)((char*)(header + 1) + header->size);

                new_header->size = size;
                new_header->prev = header;
                new_header->next = header->next;

                if (header->next) header->next->prev = new_header;
                header->next = new_header;

                ret = (void*)(new_header + 1);

                #ifdef _DEBUG
                new_header->is_in_use = 1;
                #endif

                break;
            }
        }
    }
    /* This is the first block we have */
    else
    {
        header = (MemoryBlockHeader*)block->start;
        header->next    = 0;
        header->prev    = 0;
        header->size    = size;
        block->headers  = header;
        ret = (void*)(header + 1);

        #ifdef _DEBUG
        header->is_in_use = 1;
        #endif
    }

    return ret;
}

void *
DynamicMemoryBlock_malloc(DynamicMemoryBlock *block, size_t size)
{
    ASSERT(block->start != 0);

    Mutex_lock(&block->mutex);
    void *ret = DynamicMemoryBlock_mallocInner(block, size);
    Mutex_unlock(&block->mutex);

    return ret;
}

void *
DynamicMemoryBlock_unsafeMalloc(DynamicMemoryBlock *block, size_t size)
{
    return DynamicMemoryBlock_mallocInner(block, size);
}

static inline void
DynamicMemoryBlock_freeInner(DynamicMemoryBlock *block, void *target)
{
    MemoryBlockHeader *p = ((MemoryBlockHeader*)target) - 1;

#if _DEBUG
    ASSERT(p->is_in_use && "Attempted to free an already freed block of "
        "memory!\n");
#endif

    if (p->next) p->next->prev = p->prev;

    if (p->prev)
        p->prev->next = p->next;
    else
        block->headers = p->next;

#ifdef _DEBUG
    p->is_in_use = 0;
#endif
}

void
DynamicMemoryBlock_free(DynamicMemoryBlock *block, void *target)
{
    if (!target || !block) return;
    ASSERT(target >= (void*)((char*)block->start + sizeof(MemoryBlockHeader)));
    ASSERT((void*)target < (void*)((char*)block->start + block->total_size));

    Mutex_lock(&block->mutex);
    DynamicMemoryBlock_freeInner(block, target);
    Mutex_unlock(&block->mutex);
}

void
DynamicMemoryBlock_unsafeFree(DynamicMemoryBlock *block, void *target)
{
    if (!target || !block) return;
    ASSERT(target >= (void*)((char*)block->start + sizeof(MemoryBlockHeader)));
    ASSERT((void*)target < (void*)((char*)block->start + block->total_size));
    DynamicMemoryBlock_freeInner(block, target);
}

static inline void *
DynamicMemoryBlock_reallocInner(DynamicMemoryBlock *block, void *target, size_t size)
{
    if (!target || !block) return 0;

    if (size == 0)
    {
        DynamicMemoryBlock_free(block, target);
        return 0;
    }

    if (!target)
        return DynamicMemoryBlock_mallocInner(block, size);

    MemoryBlockHeader *h = (MemoryBlockHeader*)target - 1;

    if (size < h->size)
    {
        h->size = size;
        return target;
    }

    size_t free_size = MemoryBlockHeader_getFreeSpace(h, block);

    if (h->size + free_size >= size)
    {
        h->size = free_size;
        return target;
    }

    void *new_target = DynamicMemoryBlock_mallocInner(block, size);

    if (!new_target)
        return 0;

    memcpy(new_target, target, h->size);
    DynamicMemoryBlock_freeInner(block, target);

    return new_target;
}

void *
DynamicMemoryBlock_realloc(DynamicMemoryBlock *block, void *target, size_t size)
{
    void *ret;
    Mutex_lock(&block->mutex);
    ret = DynamicMemoryBlock_reallocInner(block, target, size);
    Mutex_unlock(&block->mutex);
    return ret;
}

void *
DynamicMemoryBlock_unsafeRealloc(DynamicMemoryBlock *block, void *target,
    size_t size)
{
    return DynamicMemoryBlock_reallocInner(block, target, size);
}

void
SimpleMemoryBlock_init(SimpleMemoryBlock *block, void *start, size_t total_size)
{
    block->start        = start;
    block->offset       = start;
    block->total_size   = total_size;
}

void *
SimpleMemoryBlock_malloc(SimpleMemoryBlock *block, size_t size)
{
    char *block_end = (char*)block->start + block->total_size;
    size_t mem_left = (size_t)block_end - (size_t)(char*)block->offset;

    if (mem_left < size)
        return 0;

    void *ret = block->offset;
    block->offset = (void*)((char*)block->offset + size);

    return ret;
}

size_t
SimpleMemoryBlock_getFreeSpace(SimpleMemoryBlock *block)
{
    return block->total_size - (size_t)((char*)block->offset - (char*)block->start);
}
