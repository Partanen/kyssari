#include "audio.h"
#include "engine.h"

int
AudioSystem_init(AudioSystem *audiosystem)
{
    audiosystem->isInitialized = 0;

    /*Bitwise parameters:
    MIX_INIT_FAC, MIX_INIT_MOD,
    MIX_INIT_MP3, MIC_INIT_OGG*/
    if (Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG) == -1)
    {
        DEBUG_PRINTF("Mix_Init() failure: %s\n", Mix_GetError());
        return AUDIO_ERROR_MIX_INIT;
    }

    if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, AUDIO_CHANNEL_STEREO, AUDIO_OPEN_CHUNKSIZE) == -1)
    {
        DEBUG_PRINTF("Mix_OpenAudio() failure: %s\n", Mix_GetError());
        return AUDIO_ERROR_OPEN_AUDIO;
    }

    audiosystem->isInitialized = 1;

    return 0;
}

void
Music_play(Music *musicfile, int loops) //Loops: -1 = infinite, otherwise play loop times
{
    if(engine.audio_system.isInitialized)
        Mix_PlayMusic(musicfile->music, loops);
}

void
Music_pause()
{
    if (!engine.audio_system.isInitialized) return;
    Mix_PauseMusic();
}

void
Music_stop()
{
    if (!engine.audio_system.isInitialized) return;
    Mix_HaltMusic();
}

void
Music_resume()
{
    if (!engine.audio_system.isInitialized) return;
    Mix_ResumeMusic();
}

int
SoundEffect_play(SoundEffect *soundeffect, int channel, int loops)
{
    if (!engine.audio_system.isInitialized) return -1;
    if (!soundeffect) return -1;
    return Mix_PlayChannel(channel, soundeffect->chunk, loops);
}

void
SoundEffect_setVolume(SoundEffect *effect, char volume)
{
    if (!engine.audio_system.isInitialized) return;
    if (effect) Mix_VolumeChunk(effect->chunk, volume);
}

bool32
isSoundEffectChannelPlaying(int channel)
{
    if (!engine.audio_system.isInitialized) return 0;
    return Mix_Playing(channel);
}
