#version 330 core

struct Material
{
    sampler2D   diffuse;
    sampler2D   specular;
    float       shine;
    vec3        color;
};

layout (std140) uniform Shader3DSceneAmbientLighting
{
    vec4 scene_ambient_light;
};

in vec2 tex_coord;
in vec3 combined_diffuse;
in vec3 combined_ambient;
out vec4 frag_color;

uniform Material    material;

void main(void)
{
    vec3 diff = combined_diffuse * (material.color + vec3(texture(material.diffuse, tex_coord)));
    vec3 amb = combined_ambient * vec3(texture(material.diffuse, tex_coord));

    frag_color = vec4((diff + amb), 1.0f);
}
