#include "engine.h"
#include "assets.c"
#include "../lib/sqlite3/sqlite3.h"
#include "network.c"

int _using_curses = 0;

static int
_kys_runDesktopMode(int argc, char **argv, Screen *first_screen);

static int
_kys_runTextMode(int argc, char **argv, Screen *first_screen);

Engine engine;
static bool32 initialized_networking = 0;

ARRAY_LIST_DECLARATION(Screen);
static ArrayList_Screen screens;

/* Internals */

thread_ret_t
_ThreadPool_main(void *args);

static int
Engine_readConfig()
{
#if 0
    sqlite3 *db;

    if (sqlite3_open_v2("config.sql", &db,
        SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE, 0) != SQLITE_OK)
    {
        DEBUG_PRINTF("Failed to open / create configuration file!\n");
        return 1;
    }

    if (!db)
    {
        DEBUG_PRINTF("sqlite3_open_v2() was successful, but failed to "
            "allocate enough memory for reading the config file!\n");
        return 2;
    }

    int r;

    char *error_msg;

    r = sqlite3_exec(db, "", 0, 0, &error_msg);

    sqlite3_close(db);
#endif

    return 0;
}

/* Externals */

void
Window_handleEvent(Window *window, SDL_Event *event)
{
	switch (event->window.event)
	{
        case SDL_WINDOWEVENT_SIZE_CHANGED:
        {
            window->width   = event->window.data1;
            window->height  = event->window.data2;
        }
            break;
		case SDL_WINDOWEVENT_MOVED:     break;
        case SDL_WINDOWEVENT_RESIZED:   break;
        case SDL_WINDOWEVENT_MINIMIZED: break;
        case SDL_WINDOWEVENT_MAXIMIZED: break;
		case SDL_WINDOWEVENT_RESTORED:  break;
		case SDL_WINDOWEVENT_CLOSE:     break;
	}
}

int
kys_init(int argc, char **argv, const char *title,
    size_t permanent_memory, size_t dynamic_memory,
    int flags, int window_width, int window_height)
{
    engine.graphics_mode = KYS_GRAPHICS_DESKTOP;

    /* Parse args */
    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "-g") == 0 || strcmp(argv[i], "--no-gui") == 0)
        {
#if _CF_ALLOW_TEXT_MODE
            DEBUG_PRINTF("Attempting to start in no-gui mode.\n");
            engine.graphics_mode = KYS_GRAPHICS_TEXT;
#else
            DEBUG_PRINTF("Attempted to start in text mode, but Kyssari was "
                "not built with text mode support!\n");
#endif
        }
        else
            DEBUG_PRINTF("Invalid parameter: %s.\n", argv[i]);
    }

#if _CF_ALLOW_TEXT_MODE
    if (GET_BITFLAG(flags, int, INIT_FLAG_TEXT_MODE))
        engine.graphics_mode = KYS_GRAPHICS_TEXT;
#endif

#ifdef _UNIX
#if 0
    if (!dlopen("../bin/linux/libassimp.so.3.3.1", RTLD_LAZY))
    {
        DEBUG_PRINTF("Failed to load shared library (Assimp).\n");
        return KYS_ERROR_LIB_LOAD;
    }
#endif
#endif

    if (Engine_readConfig() != 0)
        DEBUG_PRINTF("Failed to read engine config.\n");

    if (MemoryPool_init(&engine.memory, permanent_memory + dynamic_memory) != 0)
    {
        DEBUG_PRINTF("MemoryPool_init() failure.\n");
        return KYS_ERROR_SYS_MEM_ALLOC_FAILURE;
    }
    else
    {
        DEBUG_PRINTF("Allocated %lu bytes of initial memory.\n", engine.memory.total_size);
    }

    if (DynamicMemoryBlock_init(&engine.dynamic_memory, &engine.memory,
            dynamic_memory) != 0)
    {
        DEBUG_PRINTF("DynamicMemoryBlock_init() failure (engine dynamic memory area)\n");
        return KYS_ERROR_DYNAMIC_ALLOC_FAILURE;
    }
    else
    {
        DEBUG_PRINTF("Distributed %lu bytes for dynamic storage (block id: %u).\n",
            engine.dynamic_memory.total_size, engine.dynamic_memory.id);
    }

    if (DynamicMemoryBlock_init(&engine.permanent_memory,
            &engine.memory, permanent_memory) != 0)
    {
        DEBUG_PRINTF("DynamicMemoryBlock_init() failure (engine permanent "
            "memory area)\n");
        return KYS_ERROR_DYNAMIC_ALLOC_FAILURE;
    }
    else
    {
        DEBUG_PRINTF("Distributed %lu bytes for permanent storage (block id: %u).\n",
            engine.permanent_memory.total_size, engine.permanent_memory.id);
    }

    int sdl_init_flags = 0;

    if (!KYS_IS_TEXT_MODE())
    {
        sdl_init_flags |= SDL_INIT_VIDEO;
        sdl_init_flags |= SDL_INIT_AUDIO;
    }

    if (SDL_Init(sdl_init_flags) != 0)
    {
        DEBUG_PRINTF("SDL_Init() failure: %s\n", SDL_GetError());
        return KYS_ERROR_SDL_INIT;
    }

    if (!KYS_IS_TEXT_MODE())
    {
        int img_init_flags = IMG_INIT_PNG | IMG_INIT_JPG;

    if (IMG_Init(img_init_flags) != img_init_flags)
        {
            DEBUG_PRINTF("IMG_Init() failure: %s\n", IMG_GetError());
            return KYS_ERROR_IMG_INIT;
        }
    }

    if (TTF_Init() != 0)
    {
        DEBUG_PRINTF("TTF_Init(): %s\n", TTF_GetError());
        return KYS_ERROR_TTF_INIT;
    }

    int window_flags = SDL_WINDOW_OPENGL;

    if ((flags & INIT_FLAG_RESIZABLE_WINDOW) == INIT_FLAG_RESIZABLE_WINDOW)
    {
        window_flags = window_flags | SDL_WINDOW_RESIZABLE;
    }

    if ((flags & INIT_FLAG_FULLSCREEN) == INIT_FLAG_FULLSCREEN)
    {
        window_flags = window_flags | SDL_WINDOW_FULLSCREEN;
    }

    if ((flags & INIT_FLAG_FULLSCREEN_DESKTOP) == INIT_FLAG_FULLSCREEN_DESKTOP)
    {
        window_flags = window_flags | SDL_WINDOW_FULLSCREEN_DESKTOP;
    }

    if ((flags & INIT_FLAG_WINDOW_MAXIMIZED) == INIT_FLAG_WINDOW_MAXIMIZED)
    {
        window_flags = window_flags | SDL_WINDOW_MAXIMIZED;
    }

	Clock_init(&engine.clock, KYS_IS_TEXT_MODE() ? 60 : 0);

    int num_cpus    = sys_getNumCPUs();
    int num_threads = num_cpus / 2 - 1;
    if (num_threads < 1) num_threads = 1;
    else if (num_threads > 4) num_threads = 4;

    DEBUG_PRINTF("CPUs: %i - spawning %i threads...\n", num_cpus, num_threads);

    if (ThreadPool_initAndRun(&engine.thread_pool, num_threads) != 0)
    {
        DEBUG_PRINTF("ThreadPool_initAndRun() failed!\n");
        return KYS_ERROR_THREADS;
    }

    if (!KYS_IS_TEXT_MODE())
    {
        engine.window.sdl_window = SDL_CreateWindow(title,
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            window_width, window_height,
            window_flags);

        engine.window.width     = window_width;
        engine.window.height    = window_height;

        engine.input.keyboard_state = SDL_GetKeyboardState(0);
        engine.input.mouse_button_state = 0;
        engine.input.last_mouse_button_state = 0;
        engine.input.mouse_sensitivity = 8;
        engine.input.sensitivity_timer = 0;

        if (AudioSystem_init(&engine.audio_system) != 0)
        {
            DEBUG_PRINTF("AudioSystem_init() Error\n");
        }

        GLchar error_log[1024];
        int render_res = Renderer_init(&engine.renderer, &engine.window,
            error_log, 1024, 1);
        if (render_res != 0)
        {
            DEBUG_PRINTF("There were errors initializing the renderer: %s", error_log);
#if _CF_ALLOW_RENDERER_ERRORS
#else
            if (render_res == RENDERER_ERROR_OUT_OF_MEMORY
            ||  render_res == RENDERER_ERROR_GLEW_INIT)
                return KYS_ERROR_RENDERER_INIT;
#endif
        }
    }

    if (Assets_init(&engine.assets) != 0)
    {
        DEBUG_PRINTF("Warning: Failed to allocate requested amount of memory for assets.\n");
    }

    if (!KYS_IS_TEXT_MODE())
    {
        if (GUI_init() != 0)
        {
            DEBUG_PRINTF("Warning: failed to initialize immediate mode GUI.\n");
        }
    }

    ArrayList_Screen_init(&screens, 8, 1, &engine.permanent_memory);

#if _CF_ALLOW_TEXT_MODE
    if (KYS_IS_TEXT_MODE())
    {
        engine.clock.target_fps = 30;
        initscr();
        keypad(stdscr, 1);
        curs_set(0);
        _using_curses = 1;
    }
#endif

    return 0;
}

static int
_kys_runDesktopMode(int argc, char **argv, Screen *first_screen)
{
    SDL_Event event;

    while (engine.running)
    {
        if (engine.next_screen)
        {
            if (engine.next_screen != engine.current_screen)
            {
                if (engine.current_screen->onClose)
                    engine.current_screen->onClose(engine.current_screen);

                engine.current_screen = engine.next_screen;

                if (engine.current_screen->onOpen)
                    engine.current_screen->onOpen(engine.current_screen);
            }

            engine.next_screen = 0;
        }

        Clock_tick(&engine.clock);

        bool32 mouse_moved  = 0;
        int mouse_rel_x     = 0;
        int mouse_rel_y     = 0;

        engine.input.sensitivity_timer += engine.clock.delta;

        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_WINDOWEVENT:
                {
                    Window_handleEvent(&engine.window, &event);

                    if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
                    {
                        Renderer_onWindowResize(&engine.renderer);
                    }
                }
                    break;
                case SDL_QUIT:
                {
                    kys_beginShutdown();
                }
                    break;
                case SDL_MOUSEMOTION:
                {
                    while (engine.input.sensitivity_timer > MOUSE_UPDATE_STEP)
                    {
                        mouse_rel_x = event.motion.xrel;
                        mouse_rel_y = event.motion.yrel;
                        engine.input.mouse_last_pos[0] = engine.input.mouse_pos[0];
                        engine.input.mouse_last_pos[1] = engine.input.mouse_pos[1];
                        engine.input.mouse_pos[0] = event.motion.x;
                        engine.input.mouse_pos[1] = event.motion.y;
                        mouse_moved = 1;
                        engine.input.sensitivity_timer -= MOUSE_UPDATE_STEP;
                    }
                }
                    break;
            }
        }

        if (engine.running)
        {
            engine.input.mouse_moved_this_frame = mouse_moved;
            engine.input.mouse_relative_movement[0] = mouse_rel_x * engine.input.mouse_sensitivity;
            engine.input.mouse_relative_movement[1] = mouse_rel_y * engine.input.mouse_sensitivity;
            engine.input.last_mouse_button_state = engine.input.mouse_button_state;
            engine.input.mouse_button_state = SDL_GetMouseState(0, 0);

            Renderer_handleQueuedWork(&engine.renderer);

            if (engine.current_screen && engine.current_screen->updateAndRender)
                engine.current_screen->updateAndRender(engine.current_screen);

            Renderer_renderGUI(&engine.renderer, gui);
            Renderer_swapBuffers(&engine.renderer);
        }
    }

    return 0;
}

static int
_kys_runTextMode(int argc, char **argv, Screen *first_screen)
{
#if _CF_ALLOW_TEXT_MODE

    PUTS("Starting Kyssari in text mode.");

    while (engine.running)
    {
        if (engine.next_screen)
        {
            if (engine.next_screen != engine.current_screen)
            {
                if (engine.current_screen->onCloseTextMode)
                    engine.current_screen->onCloseTextMode(engine.current_screen);

                engine.current_screen = engine.next_screen;

                if (engine.current_screen->onOpenTextMode)
                    engine.current_screen->onOpenTextMode(engine.current_screen);
            }

            engine.next_screen = 0;
        }

        Clock_tick(&engine.clock);

        if (engine.current_screen
        && engine.current_screen->updateAndRenderTextMode)
        {
            engine.current_screen->updateAndRenderTextMode(
                engine.current_screen);
        }
    }

    endwin();

    return 0;
#else
    return 1;
#endif
}

int
kys_run(int argc, char **argv, Screen *first_screen)
{
    engine.next_screen = 0;
    engine.current_screen = first_screen;

    if (first_screen->onOpen)
    {
        first_screen->onOpen(first_screen);
    }

	engine.running = 1;

    int ret;

    if (!KYS_IS_TEXT_MODE())
    {
        ret = _kys_runDesktopMode(argc, argv, first_screen);
    }
    else
    {
        ret = _kys_runTextMode(argc, argv, first_screen);
    }

    ThreadPool_shutdown(&engine.thread_pool);

	return ret;
}

void
kys_beginShutdown()
{
	engine.running = 0;
}

int
kys_initNetworking()
{
    if (!initialized_networking)
    {
        if (sys_initSockAPI() != 0) return 1;
        initialized_networking = 1;
        return 0;
    }
    return 0;
}

Screen *
createScreen (void (*updateAndRender)(Screen *screen),
	void *screen_data)
{
    Screen *screen = ArrayList_Screen_pushEmpty(&screens);

	screen->updateAndRender         = updateAndRender;
    screen->onOpen                  = 0;
    screen->onClose                 = 0;

    screen->updateAndRenderTextMode = 0;
    screen->onOpenTextMode          = 0;
    screen->onCloseTextMode         = 0;

	screen->screen_data     = screen_data;

	return screen;
}

void
setScreen(Screen *screen)
{
    ASSERT(screen);
    engine.next_screen = screen;
}

void
Clock_init(Clock *clock, uint fps_limit)
{
	clock->delta		= 0;
    clock->last_tick    = 0;
	clock->fps			= 0;
	clock->frame_num	= 0;
    clock->time_passed  = 0;
	clock->target_fps	= fps_limit;
}

int
ThreadPool_initAndRun(ThreadPool *pool, uint num_threads)
{
    if (num_threads > NUM_MAX_THREADS) return 1;

    for (ThreadJob *j = pool->jobs;
         j < &pool->jobs[NUM_JOBS - 1];
         ++j)
    {
        j->next = j + 1;
    }

    pool->jobs[NUM_JOBS - 1].next   = 0;
    pool->waiting                   = 0;
    pool->free                      = pool->jobs;
    pool->num_threads               = num_threads;
    pool->running                   = 1;
    pool->num_incomplete            = 0;
    pool->num_threads_started       = 0;

    Mutex_init(&pool->mutex);
    ConditionVariable_init(&pool->cvar);

    for (Thread *th = pool->threads;
         th < &pool->threads[num_threads];
         ++th)
    {
        if (Thread_create(th, _ThreadPool_main, pool) != 0)
        {
            DEBUG_PRINTF("!!! %s\n", strerror(errno));
            pool->running = 0;
            return 2;
        }
    }

    return 0;
}

void
ThreadPool_shutdown(ThreadPool *pool)
{
    if (!pool->running) return;
    pool->running = 0;

    ConditionVariable_notifyAll(&pool->cvar);

    for (uint i = 0; i < pool->num_threads; ++i)
        Thread_join(&pool->threads[i]);

    Mutex_destroy(&pool->mutex);
    ConditionVariable_destroy(&pool->cvar);
}

bool32
ThreadPool_haveUnfinishedWork(ThreadPool *pool)
{
    bool32 r = sys_interlockedCompareExchangeInt32(&pool->num_incomplete, 0, 0);
    return r > 0 ? 1 : 0;
}

thread_ret_t
_ThreadPool_main(void *pool_ptr)
{
    ThreadPool *pool = (ThreadPool*)pool_ptr;
    sys_interlockedIncrementInt32(&pool->num_threads_started); /* ID */

    while (pool->running)
    {
        Mutex_lock(&pool->mutex);

        ThreadJob *job;

        while (!pool->waiting && pool->running)
            ConditionVariable_wait(&pool->cvar, &pool->mutex);

        {
            if (!pool->waiting)
            {
                Mutex_unlock(&pool->mutex);
                break;
            }

            job = pool->waiting;

            pool->waiting = job->next;

            void (*f)(void*)    = job->func;
            void *a             = job->args;

            job->next   = pool->free;
            pool->free  = job;

            Mutex_unlock(&pool->mutex);

            f(a);

            sys_interlockedDecrementInt32(&pool->num_incomplete);
        }
    }

    return 0;
}

int
ThreadPool_addWork(ThreadPool *pool, void (*func), void *args)
{
    Mutex_lock(&pool->mutex);

    ThreadJob *j = pool->free;
    if (!j) {Mutex_unlock(&pool->mutex); return 1;};

    j->func = func;
    j->args = args;

    pool->free = j->next;

    ThreadJob **tail = &pool->waiting;
    while (*tail) tail = &(*tail)->next;

    *tail = j;
    j->next = 0;

    sys_interlockedIncrementInt32(&pool->num_incomplete);

    Mutex_unlock(&pool->mutex);
    ConditionVariable_notifyOne(&pool->cvar);

    return 0;
}

void
ThreadPool_doWork(ThreadPool *pool)
{
    Mutex_lock(&pool->mutex);

    if (pool->waiting)
    {
        ThreadJob *job = pool->waiting;

        pool->waiting = job->next;

        void (*f)(void*)    = job->func;
        void *a             = job->args;

        job->next   = pool->free;
        pool->free  = job;

        Mutex_unlock(&pool->mutex);

        f(a);

        sys_interlockedDecrementInt32(&pool->num_incomplete);

        return;
    }

    Mutex_unlock(&pool->mutex);
}

void
Clock_tick(Clock *clock)
{
    /* We just save this for some other stuff */
    clock->time_passed      = SDL_GetTicks();

    uint64_t tick_time      = SDL_GetPerformanceCounter();
    uint64_t perf_freq      = SDL_GetPerformanceFrequency();

    if (clock->last_tick != 0.0f)
        clock->delta = (double)(tick_time - clock->last_tick) / (double)perf_freq;
    else
        clock->delta = 0.016f;

    clock->last_tick = tick_time;

    if (clock->delta > 0.0f)
        clock->fps = 1.0f / clock->delta;

    if (clock->target_fps != 0)
    {
        double target_time = 1.0f / (double)clock->target_fps * 1000.0f;
        uint64_t frame_dur_so_far = tick_time - clock->last_tick;
        double subs = target_time - (double)frame_dur_so_far;

        if (subs > 0.0f)
            sys_sleepMilliseconds((uint32_t)(target_time - frame_dur_so_far));
    }

	++clock->frame_num;
}
