#version 330 core

#define NUM_MAX_DIRECTIONAL_LIGHTS 2
#define NUM_MAX_POINT_LIGHTS 4

struct Material
{
    sampler2D   diffuse;
    sampler2D   specular;
    float       shine;
    vec3        color;
};

layout (std140) uniform Shader3DSharedMatrices
{
    mat4 projection;
    mat4 look_at;
    vec3 camera_pos;
};

uniform mat4        model;
uniform Material    material;
uniform float       outline_offset;

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 in_tex_coord;
layout (location = 2) in vec3 in_normal;

out vec3 frag_pos;
out vec2 tex_coord;

out vec3 out_normal;


vec3 normal;


void main(void)
{
    frag_pos = vec3(model * vec4(pos, 1.0f));
    tex_coord = in_tex_coord;

	vec3 scaledpos = vec3(pos + in_normal * outline_offset);

    normal = normalize(vec3(inverse(model) * vec4(in_normal, 0.0f)));
    vec3 view_direction = normalize(camera_pos - frag_pos);

    gl_Position = projection * look_at * model * vec4(scaledpos, 1.0f);
}
