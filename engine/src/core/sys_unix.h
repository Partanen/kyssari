/* Currently this is only compliant with GCC on GNU/Linux. Some functions may
 * not be POSIX compliant at all */

#pragma once
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <signal.h>
#include <assert.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <dlfcn.h>
#include <semaphore.h>
#include <string.h>

extern int _using_curses;

#ifdef _DEBUG

    #define SET_BREAKPOINT() raise(SIGINT)

    #define DEBUG_PRINTF(str, ...)\
        (!_using_curses ? fprintf(stdout, str, ##__VA_ARGS__) : (void)0)

#else
    #define SET_BREAKPOINT()
    #define DEBUG_PRINTF(str, ...)
#endif

#define PRINTF(str, ...) \
    (!_using_curses ? printf(str, ##__VA_ARGS__) : (void)0)


#define PUTS(str) \
    (!_using_curses ? puts(str) : (void)0);

#define ASSERT(expression) assert(expression)
#define SYS_MEMORY_BARRIER() __sync_synchronize()
#define thread_ret_t void *

typedef pthread_mutex_t Mutex;
typedef pthread_t       Thread;
typedef pthread_cond_t  ConditionVariable;
typedef sem_t           Semaphore;
typedef int             sys_socket_t;

static inline int
Thread_create(Thread *thread, thread_ret_t (*func)(void *), void *args);

static inline void
Thread_join(Thread *thread);

static inline void
Thread_detach(Thread *thread);

static inline void
Thread_kill(Thread *thread);

static inline void
Mutex_init(Mutex *mutex);

static inline void
Mutex_destroy(Mutex *mutex);

static inline void
Mutex_lock(Mutex *mutex);

static inline void
Mutex_unlock(Mutex *mutex);

static inline void
ConditionVariable_init(ConditionVariable *cond_var);

static inline void
ConditionVariable_destroy(ConditionVariable *cv);

static inline int
ConditionVariable_wait(ConditionVariable *cond_var, Mutex *mutex);

static inline void
ConditionVariable_notifyOne(ConditionVariable *cvar);

static inline void
ConditionVariable_notifyAll(ConditionVariable *cvar);

static inline void
Semaphore_init(Semaphore *sem, int initial_count);

static inline void
Semaphore_wait(Semaphore *sem);

static inline void
Semaphore_release(Semaphore *sem);

static inline void
sys_sleepMilliseconds(unsigned int ms);

static inline void *
sys_allocateMemory(size_t size);

static inline int
sys_getSockErrorString(char *buf, int buf_len);

static inline int
sys_initSockAPI();

static inline int
sys_closeSocket(sys_socket_t sock);

static inline int
sys_listenSocket(sys_socket_t sock, int backlog);

static inline int
sys_setSocketNonBlocking(sys_socket_t sock);

static inline int
sys_getNumCPUs();



static inline int
Thread_create(Thread *thread, thread_ret_t (*func)(void *), void *args)
{
    return pthread_create(thread, 0, func, args) == 0 ? 0 : 1;
}

static inline void
Thread_join(Thread *thread)
{
    pthread_join(*thread, 0);
}

static inline void
Thread_detach(Thread *thread)
{
    pthread_detach(*thread);
}

static inline void
Thread_kill(Thread *thread)
{
    pthread_kill(*thread, SIGKILL);
}

static inline void
Mutex_init(Mutex *mutex)
{
    pthread_mutex_init(mutex, 0);
}

static inline void
Mutex_destroy(Mutex *mutex)
{
    pthread_mutex_destroy(mutex);
}

static inline void
Mutex_lock(Mutex *mutex)
{
    pthread_mutex_lock(mutex);
}

static inline void
Mutex_unlock(Mutex *mutex)
{
    pthread_mutex_unlock(mutex);
}

static inline void
ConditionVariable_init(ConditionVariable *cv)
{
    pthread_cond_init(cv, 0);
}

static inline void
ConditionVariable_destroy(ConditionVariable *cv)
{
    pthread_cond_destroy(cv);
}

static inline int
ConditionVariable_wait(ConditionVariable *cv, Mutex *m)
{
    return pthread_cond_wait(cv, m) == 0 ? 0 : 1;
}

static inline void
ConditionVariable_notifyOne(ConditionVariable *cv)
{
    pthread_cond_signal(cv);
}

static inline void
ConditionVariable_notifyAll(ConditionVariable *cv)
{
    pthread_cond_broadcast(cv);
}

static inline void
Semaphore_init(Semaphore *sem, int initial_count)
{
    sem_init(sem, 0, (unsigned int)initial_count);
}

static inline void
Semaphore_wait(Semaphore *sem)
{
    sem_wait(sem);
}

static inline void
Semaphore_release(Semaphore *sem)
{
    sem_post(sem);
}

static inline int32_t
sys_interlockedCompareExchangeInt32(
    int32_t volatile *target,
    int32_t exchange,
    int32_t comparand)
{
    return __sync_val_compare_and_swap(target, comparand, exchange);
}

static inline int32_t
sys_interlockedIncrementInt32(int32_t volatile *target)
{
    return __sync_add_and_fetch(target, 1);
}

static inline int32_t
sys_interlockedDecrementInt32(int32_t volatile *target)
{
    return __sync_sub_and_fetch(target, 1);
}

static inline void
sys_sleepMilliseconds(unsigned int ms)
{
    usleep(ms * 1000);
}

static inline void *
sys_allocateMemory(size_t size)
{
    return mmap(0, size, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, 0, 0);
}

static inline int
sys_getSockErrorString(char *buf, int buf_len)
{
    if (buf_len <= 0) return 0;
    const char *s = strerror(errno);
    if (s)
        strncpy(buf, s, buf_len);
    else
        buf[0] = '\0';
    return strlen(buf);
}

static inline int
sys_initSockAPI()
{
    return 0;
}

static inline int
sys_closeSocket(sys_socket_t sock)
{
    return close(sock);
}

static inline int
sys_listenSocket(sys_socket_t sock, int backlog)
{
    return listen(sock, backlog);
}

static inline int
sys_setSocketNonBlocking(sys_socket_t sock)
{
    int flags = fcntl(sock, F_GETFL, 0);
    return fcntl(sock, F_SETFL, flags | O_NONBLOCK) != -1 ? 0 : 1;
}

static inline int
sys_getNumCPUs()
{
    return sysconf(_SC_NPROCESSORS_ONLN);
}
