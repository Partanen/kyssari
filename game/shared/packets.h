enum ServerToClientMessage
{
    KS_SC_MSG_ATTACHED_TO_GAME = FIRST_USER_SERVER_TO_CLIENT_MESSAGE_ID,

    /* Tell the player what their pawn's ID is */
    KS_SC_MSG_OWN_PAWN_INFORM,

    /* Tell the client that an entity of a given replication id is another
     * player */
    KS_SC_MSG_OTHERS_PAWN_INFORM,
    KS_SC_MSG_OTHERS_PAWN_REMOVED,
    KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE,
    KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE,
    KS_SC_MSG_OTHER_PAWN_SWAPPED_WEAPON,
    KS_SC_MSG_SNAP_OWN_PAWN_STATE,

    /* Shots */
    KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER,
    KS_SC_MSG_OTHER_PAWN_SHOT_SMG,
    KS_SC_MSG_OTHER_PAWN_SHOT_RAILGUN,
    KS_SC_MSG_OTHER_PAWN_HIT_MACHETE,

    KS_SC_MSG_OWN_HEALTH_INFORM,
    KS_SC_MSG_PAWN_DEATH_INFORM,
    KS_SC_MSG_ITEM_SPAWNED,
    KS_SC_MSG_FRAG_COUNT,
    KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN,
    KS_SC_MSG_FRAG_INFORM,
    KS_SC_MSG_OTHERS_ROCKET_EXPLOSION,

    NUM_KS_SERVER_TO_CLIENT_MESSAGE_TYPES,
};

#if NUM_KS_SERVER_TO_CLIENT_MESSAGE_TYPES > 255
#   error TOO MANY SERVER TO CLIENT MESSAGE TYPES
#endif

enum ClientToServerMessage
{
    KS_CS_MSG_FINISHED_LOADING_LEVEL = FIRST_USER_CLIENT_TO_SERVER_MESSAGE_ID,
    KS_CS_MSG_INPUT_BUFFER_CONTENTS,
    KS_CS_MSG_WEAPON_SWAPPED,

    /* Shots */
    KS_CS_MSG_SHOT_ROCKET_LAUNCHER,
    KS_CS_MSG_SHOT_SMG,
    KS_CS_MSG_SHOT_RAILGUN,
    KS_CS_MSG_HIT_MACHETE,

    NUM_KS_CLIENT_TO_SERVER_MESSAGE_TYPES
};

#if NUM_KS_SERVER_TO_CLIENT_MESSAGE_TYPES > 255
#   error TOO MANY CLIENT TO SERVER MESSAGE TYPES
#endif

/* Server to client */
MSG_SIZE_DECL(KS_SC_MSG_ATTACHED_TO_GAME, 1); /* Contains the map id */

MSG_SIZE_DECL(KS_SC_MSG_OWN_PAWN_INFORM,
    sizeof(replication_id32_t) +    /* Pawn id */
    sizeof(char) +                  /* Character type */
    sizeof(vec3));                  /* Position */

MSG_SIZE_DECL(KS_SC_MSG_OTHERS_PAWN_INFORM,
    sizeof(replication_id32_t) +
    sizeof(char) +  /* Character type */
    sizeof(vec3) +  /* Position */
    sizeof(vec3) +  /* Velocity */
    sizeof(ubyte)); /* Orientation */

MSG_SIZE_DECL(KS_SC_MSG_OTHERS_PAWN_REMOVED,
    sizeof(replication_id32_t));

MSG_SIZE_DECL(KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE,
    sizeof(vec3) + sizeof(vec3) + sizeof(input_sequence_t));

MSG_SIZE_DECL(KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE,
    sizeof(replication_id32_t) +
    sizeof(vec3) +  /* Position */
    sizeof(vec3) +  /* Velocity */
    sizeof(ubyte) + /* Orientation */
    sizeof(char));  /* Weapon and animation */

MSG_SIZE_DECL(KS_SC_MSG_OTHER_PAWN_SWAPPED_WEAPON,
    sizeof(replication_id32_t) + 1);

MSG_SIZE_DECL(KS_SC_MSG_SNAP_OWN_PAWN_STATE,
    sizeof(vec3) + sizeof(vec3)); /* pos, velocity */

/* Other pawn shot message layout:
 * replication id, position, direction */
MSG_SIZE_DECL(KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER,
    sizeof(replication_id32_t) + sizeof(vec3) * 2);
MSG_SIZE_DECL(KS_SC_MSG_OTHER_PAWN_SHOT_SMG,
    sizeof(replication_id32_t) + sizeof(vec3) * 2);
MSG_SIZE_DECL(KS_SC_MSG_OTHER_PAWN_SHOT_RAILGUN,
    sizeof(replication_id32_t) + sizeof(vec3) * 2);
MSG_SIZE_DECL(KS_SC_MSG_OTHER_PAWN_HIT_MACHETE,
    sizeof(replication_id32_t) + sizeof(vec3) * 2);

MSG_SIZE_DECL(KS_SC_MSG_OWN_HEALTH_INFORM, 1);

MSG_SIZE_DECL(KS_SC_MSG_PAWN_DEATH_INFORM,
    sizeof(replication_id32_t));

MSG_SIZE_DECL(KS_SC_MSG_ITEM_SPAWNED,
    sizeof(char) +  /* Index */
    sizeof(char) +  /* Type */
    sizeof(vec3));  /* Position */

MSG_SIZE_DECL(KS_SC_MSG_FRAG_COUNT, sizeof(uint));

MSG_SIZE_DECL(KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN,
    1 +                             /* Item index */
    1 +                             /* Item type */
    sizeof(replication_id32_t));    /* Pawn id */

MSG_SIZE_DECL(KS_SC_MSG_FRAG_INFORM,
    sizeof(replication_id32_t) + /* Fragger */
    sizeof(replication_id32_t)); /* Fraggee */

MSG_SIZE_DECL(KS_SC_MSG_OTHERS_ROCKET_EXPLOSION,
    sizeof(vec3)); /* Position */

/* Client to server */
MSG_MIN_SIZE_DECL(KS_CS_MSG_INPUT_BUFFER_CONTENTS, 1);
MSG_SIZE_DECL(KS_CS_MSG_WEAPON_SWAPPED, 1);

MSG_SIZE_DECL(KS_CS_MSG_SHOT_ROCKET_LAUNCHER,   sizeof(vec3) * 2);
MSG_SIZE_DECL(KS_CS_MSG_SHOT_SMG,               sizeof(vec3) * 2);
MSG_SIZE_DECL(KS_CS_MSG_SHOT_RAILGUN,           sizeof(vec3) * 2);
MSG_SIZE_DECL(KS_CS_MSG_HIT_MACHETE,            sizeof(vec3) * 2);


/* Server to client */

static inline void
KS_SC_MSG_OWN_PAWN_INFORM_serialize(ByteBuffer *buf,
    replication_id32_t replication_id, char character_type, vec3 position)
{
    ByteBuffer_write(buf, replication_id32_t, replication_id);
    ByteBuffer_write(buf, char, character_type);
    ASSERT(ByteBuffer_writeBytes(buf, position, sizeof(vec3)) == 0);
}

static inline void
KS_SC_MSG_OTHERS_PAWN_INFORM_serialize(ByteBuffer *buf,
    replication_id32_t replication_id,
    char character_type, vec3 position, vec3 velocity, ubyte orientation)
{
    ByteBuffer_write(buf, replication_id32_t, replication_id);
    ByteBuffer_write(buf, char, character_type);
    ASSERT(ByteBuffer_writeBytes(buf, position, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, velocity, sizeof(vec3)) == 0);
    ByteBuffer_write(buf, ubyte, orientation);
}

static inline void
KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE_serialize(ByteBuffer *buf,
    vec3 pos, vec3 vel, input_sequence_t latest_applied_input)
{
    ByteBuffer_writeBytes(buf, pos, sizeof(vec3));
    ByteBuffer_writeBytes(buf, vel, sizeof(vec3));
    ByteBuffer_write(buf, input_sequence_t, latest_applied_input);
}

static inline void
KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE_serialize(ByteBuffer *buf,
    replication_id32_t rid,
    vec3 position,
    vec3 velocity,
    ubyte orientation,
    int weapon,
    int movement_state)
{
    ByteBuffer_write(buf, replication_id32_t, rid);
    ASSERT(ByteBuffer_writeBytes(buf, position, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, velocity, sizeof(vec3)) == 0);
    ByteBuffer_write(buf, ubyte, orientation);

    /* I don't know how the C standard defines alignment here but maybe
     * using a union will be safer(?) */
    union
    {
        struct WaInner
        {
            int weapon:3;
            int movement_state:5;
        } data;
    } wa_byte;

    wa_byte.data.weapon         = weapon;
    wa_byte.data.movement_state = movement_state;

    ByteBuffer_write(buf, char, *(char*)&wa_byte.data);
}

static inline void
KS_SC_MSG_SNAP_OWN_PAWN_STATE_serialize(ByteBuffer *buf, vec3 pos, vec3 vel)
{
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, vel, sizeof(vec3)) == 0);
}

static inline void
KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER_serialize(ByteBuffer *buf,
    replication_id32_t rid, vec3 pos, vec3 dir)
{
    ByteBuffer_write(buf, replication_id32_t, rid);
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, dir, sizeof(vec3)) == 0);
}

static inline void
KS_SC_MSG_OTHER_PAWN_SHOT_SMG_serialize(ByteBuffer *buf,
    replication_id32_t rid, vec3 pos, vec3 dir)
{
    ByteBuffer_write(buf, replication_id32_t, rid);
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, dir, sizeof(vec3)) == 0);
}

static inline void
KS_SC_MSG_OTHER_PAWN_SHOT_RAILGUN_serialize(ByteBuffer *buf,
    replication_id32_t rid, vec3 pos, vec3 dir)
{
    ByteBuffer_write(buf, replication_id32_t, rid);
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, dir, sizeof(vec3)) == 0);
}

static inline void
KS_SC_MSG_OTHER_PAWN_HIT_MACHETE_serialize(ByteBuffer *buf,
    replication_id32_t rid, vec3 pos, vec3 dir)
{
    ByteBuffer_write(buf, replication_id32_t, rid);
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, dir, sizeof(vec3)) == 0);
}

static inline void
KS_SC_MSG_ITEM_SPAWNED_serialize(ByteBuffer *buf, int index, int type, vec3 pos)
{
    ByteBuffer_write(buf, char, (char)index);
    ByteBuffer_write(buf, char, (char)type);
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
}

static inline void
KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN_serialize(ByteBuffer *buf, int item_index,
    int item_type, replication_id32_t rid)
{
    ByteBuffer_write(buf, char, (char)item_index);
    ByteBuffer_write(buf, char, (char)item_type);
    ByteBuffer_write(buf, replication_id32_t, rid);
}

/* Client to server */

static inline void
KS_CS_MSG_SHOT_ROCKET_LAUNCHER_serialize(ByteBuffer *buf, vec3 pos, vec3 dir)
{
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, dir, sizeof(vec3)) == 0);
}

static inline void
KS_CS_MSG_SHOT_SMG_serialize(ByteBuffer *buf, vec3 pos, vec3 dir)
{
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, dir, sizeof(vec3)) == 0);
}

static inline void
KS_CS_MSG_SHOT_RAILGUN_serialize(ByteBuffer *buf, vec3 pos, vec3 dir)
{
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, dir, sizeof(vec3)) == 0);
}

static inline void
KS_CS_MSG_HIT_MACHETE_serialize(ByteBuffer *buf, vec3 pos, vec3 dir)
{
    ASSERT(ByteBuffer_writeBytes(buf, pos, sizeof(vec3)) == 0);
    ASSERT(ByteBuffer_writeBytes(buf, dir, sizeof(vec3)) == 0);
}
