#pragma once
#include "utils.h"
#include "audio.h"

#define NUM_INITIAL_TEXTURE_ASSET_SLOTS 256
#define NUM_INITIAL_MESH_ASSET_SLOTS 128
#define NUM_INITIAL_FONT_ASSET_SLOTS 8
#define NUM_INITIAL_SOUNDEFFECT_ASSET_SLOTS 4
#define NUM_INITIAL_MUSIC_ASSET_SLOTS 4
#define NUM_INITIAL_MODEL_ASSET_SLOTS 32
#define MAX_MODEL_VERTICES 4096
#define ASSETPOOL_MEMORY_BUFFER 60

/* Forward declarations */
typedef struct Texture          Texture;
typedef struct SpriteFont       SpriteFont;

/* Types defined here */
typedef struct AssetPool        AssetPool;
typedef struct Assets           Assets;
typedef TTF_Font *              FontPtr;

VECTOR_DECLARATION(BaseMesh);
ARRAY_LIST_DECLARATION(FontPtr);
ARRAY_LIST_DECLARATION(SoundEffect);
ARRAY_LIST_DECLARATION(Music);
ARRAY_LIST_DECLARATION(Model);
HASH_TABLE_DECLARATION(SpriteFont);
HASH_TABLE_DECLARATION(Texture);
HASH_TABLE_DECLARATION(BaseMesh);
HASH_TABLE_DECLARATION(Model);
HASH_TABLE_DECLARATION(Material);
HASH_TABLE_DECLARATION(MaterialArray);
HASH_TABLE_DECLARATION(SoundEffect);
HASH_TABLE_DECLARATION(Music);

struct AssetPool
{
    SimpleMemoryBlock       memory;

    HashTable_SpriteFont    spritefonts;
    HashTable_BaseMesh      basemeshes;
    HashTable_Texture       textures;
    HashTable_Material      materials;
    HashTable_MaterialArray material_arrays;
    HashTable_SoundEffect   soundeffects;
    HashTable_Music         musics;
    HashTable_Model         models;
};

struct Assets
{
    ArrayList_Model         models;
    ArrayList_Texture       textures;
    ArrayList_SoundEffect   soundeffects;
    ArrayList_Music         musictracks;
    Vector_BaseMesh         base_meshes;

    vec3                    *mesh_load_pos;
    vec2                    *mesh_load_uv;
    vec3                    *mesh_load_norm;
    GLfloat                 *vertex_buffer;
    Mutex                   mesh_mutex;

    ArrayList_FontPtr   fonts;

    /* Basic shapes */
    BaseMesh            mesh_cube;
    BaseMesh            mesh_triangle;
    BaseMesh            *mesh_ball;
    BaseMesh            mesh_plane;
    Model               model_cube;
    Model               *model_ball;
    Model               model_triangle;
    Model               model_plane;

    SpriteFont          default_sprite_font;
    SpriteFont          *sprite_fonts;
    uint                num_sprite_fonts;
    uint                num_max_sprite_fonts;
};

static inline SpriteFont *
AssetPool_getSpriteFont(AssetPool *assetpool, const char *name);

static inline BaseMesh *
AssetPool_getBaseMesh(AssetPool *assetpool, const char *name);

static inline Model *
AssetPool_getModel(AssetPool *assetpool, const char *name);

static inline Texture *
AssetPool_getTexture(AssetPool *assetpool, const char *name);

static inline Material *
AssetPool_getMaterial(AssetPool *assetpool, const char *name);

static inline MaterialArray *
AssetPool_getMaterialArray(AssetPool *assetpool, const char *name);

static inline SoundEffect *
AssetPool_getSoundEffect(AssetPool *assetpool, const char *name);

static inline Music *
AssetPool_getMusic(AssetPool *assetpool, const char *name);

int
AssetPool_init(AssetPool *assetpool, uint32_t num_spritefonts,
    uint32_t num_basemeshes,    uint32_t num_textures,
    uint32_t num_materials,     uint32_t num_material_arrays,
    uint32_t num_soundeffects,  uint32_t num_musics,
    uint32_t num_models,
    size_t additional_memory);

SpriteFont *
AssetPool_loadSpriteFont(AssetPool *assetpool, const char *name, const char *path, uint size);

BaseMesh *
AssetPool_loadBaseMesh(AssetPool *assetpool, const char *name, const char *path, Assets *assets);

Model *
AssetPool_loadModel(AssetPool *assetpool, const char *name, const char *path);

Texture *
AssetPool_loadTexture(AssetPool *assetpool, const char *name, const char *path);

Material *
AssetPool_loadMaterial(AssetPool *assetpool, const char *name,
    Texture *diffuse_textures, Texture *specular,
    float shine, float r, float g, float b);

MaterialArray *
AssetPool_loadMaterialArray(AssetPool *assetpool, const char *name,
    Material **materials, uint num_materials);

SoundEffect *
AssetPool_loadSoundEffect(AssetPool *assetpool, const char *name, const char *path);

Music *
AssetPool_loadMusic(AssetPool *assetpool, const char *name, const char *path);

void
AssetPool_clear(AssetPool *assetpool);

int
Assets_init(Assets *assets);

Texture *
Assets_loadTexture(Assets *assets, const char *path);

Texture *
Assets_loadCubemapTexture(Assets *assets,
    const char *path_pos_x, const char *path_neg_x,
    const char *path_pos_y, const char *path_neg_y,
    const char *path_pos_z, const char *path_neg_z);

Texture *
Assets_createTextureFromText(Assets *assets,
    Font *font, Color *color,
    const char *text, int wrap_width);

Texture *
Assets_updateTextureFromText(Assets *assets,
    Texture *texture,
    Font * font, Color *color,
    const char *text, int wrap_width);

/* Load a single mesh from a file */
BaseMesh *
Assets_loadBaseMesh(Assets *assets, const char *path);

/* Load a model from a file. A model may include multiple meshes */
Model *
Assets_loadModel(Assets *assets, const char *path);

Font *
Assets_loadFont(Assets *assets, const char *path, int size);

SpriteFont *
Assets_loadSpriteFont(Assets *assets, const char *path,
    uint size, Color color);

SoundEffect *
Assets_loadSoundEffect(Assets *assets, const char *path);

Music *
Assets_loadMusic(Assets *assets, const char *path);

static inline SpriteFont *
AssetPool_getSpriteFont(AssetPool *assetpool, const char *name)
{
    return HashTable_SpriteFont_get(&assetpool->spritefonts, name);
}

static inline BaseMesh *
AssetPool_getBaseMesh(AssetPool *assetpool, const char *name)
{
    return HashTable_BaseMesh_get(&assetpool->basemeshes, name);
}

static inline Model *
AssetPool_getModel(AssetPool *assetpool, const char *name)
{
    return HashTable_Model_get(&assetpool->models, name);
}

static inline Texture *
AssetPool_getTexture(AssetPool *assetpool, const char *name)
{
    return HashTable_Texture_get(&assetpool->textures, name);
}

static inline Material *
AssetPool_getMaterial(AssetPool *assetpool, const char *name)
{
    return HashTable_Material_get(&assetpool->materials, name);
}

static inline MaterialArray *
AssetPool_getMaterialArray(AssetPool *assetpool, const char *name)
{
    return HashTable_MaterialArray_get(&assetpool->material_arrays, name);
}

static inline SoundEffect *
AssetPool_getSoundEffect(AssetPool *assetpool, const char *name)
{
    return HashTable_SoundEffect_get(&assetpool->soundeffects, name);
}

static inline Music *
AssetPool_getMusic(AssetPool *assetpool, const char *name)
{
    return HashTable_Music_get(&assetpool->musics, name);
}
