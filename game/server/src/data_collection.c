#include "../../../engine/src/lib/sqlite3/sqlite3.h"

static struct Database
{
    sqlite3 *db;
} ks_db = {0};

static int
db_init();

static int
db_close();

static int
db_init()
{
    if (sqlite3_open_v2("ks_database.sql", &ks_db.db,
        SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE, 0) != SQLITE_OK)
    {
        return 1;
    }

    if (!ks_db.db)
        return 2;

    char *err_msg;
    if (sqlite3_exec(ks_db.db, "CREATE TABLE ACCEPTED_USERS("
        "IP INT INTEGER PRIMARY KEY, "
        "IS_BANNED INT)", 0, 0, &err_msg) != 0)
    {
        PUTS(err_msg);
        sqlite3_free(err_msg);
        return 3;
    }

    return 0;
}

static int
db_close()
{
    sqlite3_close(ks_db.db);
    return 0;
}

static int
db_createBanEntry(Address *address, bool32 banned)
{
    char cmd[512];
    sprintf(cmd, "INSERT INTO ACCEPTED_USERS(IP, IS_BANNED) "
        "VALUES (%i, %i)",
        (int)address->address, banned);

    char *err_msg;
    if (sqlite3_exec(ks_db.db, cmd, 0, 0, &err_msg) != 0)
    {
        PUTS(err_msg);
        sqlite3_free(err_msg);
        return 1;
    }

    return 0;
}

typedef struct CharBuf
{
    char    *mem;
    int     len;
} CharBuf;

static int
callback(void *args, int num_cols, char **col_strs,
    char **column_names)
{
    CharBuf *buf = (CharBuf*)args;

    if (num_cols)
        strcpy(buf->mem, col_strs[1]);
    else
        buf->mem[0] = '\0';

    return 0;
}

/* Return -1 on error */
static int
db_isAddressBanned(Address *addr)
{
    char buf_mem[512];
    CharBuf buf = {buf_mem, 512};

    char cmd[512];
    sprintf(cmd, "SELECT * FROM ACCEPTED_USERS;");

    char *err_msg;
    if (sqlite3_exec(ks_db.db, cmd, callback, &buf, &err_msg) != 0)
    {
        PUTS(err_msg);
        sqlite3_free(err_msg);
        return -1;
    }

    PUTS(buf_mem);

    return 0;
}
