#pragma once
#include "types.h"
#include "../render/spritebatch.h"

#ifdef _WANGBLOWS
    #include "sys_win32.h"
#elif defined(_UNIX)
    #include "sys_unix.h"
#endif

/* Forward declaration(s) */
typedef struct SpriteFont           SpriteFont;

typedef struct GUIButtonStyle       GUIButtonStyle;
typedef struct GUIWindowStyle       GUIWindowStyle;
typedef struct GUIProgressBarStyle  GUIProgressBarStyle;
typedef struct GUIDrawCommand       GUIDrawCommand;
typedef struct GUIDrawCommandBuffer GUIDrawCommandBuffer;
typedef struct GUIVertexBuffer      GUIVertexBuffer;
typedef struct GUIWindow            GUIWindow;
typedef struct GUI                  GUI;

/* Global GUI instance */
extern GUI * const gui;

/* Default GUI styles - modify at will */
extern GUIProgressBarStyle * const default_progressbar_style;
extern GUIButtonStyle * const default_button_style;
extern GUIWindowStyle * const default_window_style;

enum GUIOrigin
{
    GUI_ORIGIN_TOP_LEFT = 0,
    GUI_ORIGIN_TOP_RIGHT,
    GUI_ORIGIN_BOTTOM_LEFT,
    GUI_ORIGIN_BOTTOM_RIGHT
};

enum GUIBarFillDirection
{
    GUI_FILLDIRECTION_RIGHT = 0,
    GUI_FILLDIRECTION_LEFT,
    GUI_FILLDIRECTION_TOP,
    GUI_FILLDIRECTION_BOTTOM
};

enum GUIBarTextOptions
{
    GUI_BAR_DONT_SHOW_VALUE = 0,
    GUI_BAR_SHOW_VALUE,
    GUI_BAR_SHOW_VALUE_WITH_MAX
};

struct GUIWindowStyle
{
    float               color_background_normal[4];
    float               color_background_active[4];
    float               color_border_normal[4];
    float               color_border_active[4];

    int                 border_width_top;
    int                 border_width_bottom;
    int                 border_width_left;
    int                 border_width_right;

    bool32              show_title;
    int                 title_position[2];
    enum GUIOrigin      title_origin;
    float               color_title[4];
    SpriteFont          *title_font;
    SpriteFont          *font;
};

struct GUIButtonStyle
{
    float               color_background_normal[4];
    float               color_background_hovered[4];
    float               color_background_pressed[4];
    float               color_border_normal[4];
    float               color_border_hovered[4];
    float               color_border_pressed[4];

    int                 border_width_top;
    int                 border_width_bottom;
    int                 border_width_left;
    int                 border_width_right;

    bool32              show_title;
    int                 title_position[2];
    enum GUIOrigin      title_origin;
    SpriteFont          *title_font;
    float               title_scale;
};

struct GUIProgressBarStyle
{
    SpriteFont              *font;
    Texture                 *border_texture;
    Texture                 *background_texture;
    Texture                 *fill_texture;
    float                   text_color;
    float                   texture_scale;
    bool32                  show_border;
    bool32                  show_background;
    bool32                  border_on_top;
    enum GUIBarTextOptions  text_option;
};

struct GUIDrawCommand
{
    GLuint  texture;
    uint    vertex_index;
    uint    num_vertices;
    int     next;
};

struct GUIVertexBuffer
{
    GLfloat             *vertices;
    uint                num_vertices;
    uint                capacity;
};

struct GUIWindow
{
    uint32_t            id;
    int                 parent;
    uint                last_made_active;
    GUIWindowStyle      *style;
    ushort              index;
    int                 relative_position[2];
    int                 true_position[2];
    int                 dimensions[2];
    int                 draw_commands_index;
    GUIVertexBuffer     vertex_buffer;
};

struct GUIDrawCommandBuffer
{

    GUIDrawCommand  *commands;
    uint            num_commands;
    uint            capacity;
};

typedef struct
{
    Texture *texture;
    bool32  have_clip;
    int     clip[4];
    int     offset[2];
    float   scale[2];
} _GUIButtonTexture;

struct GUI
{
    bool32                  began_update;
    GUIWindow               *windows;
    ushort                  *sorted_windows;
    uint                    num_sorted_windows;
    uint                    num_windows;
    uint                    num_reserved_windows;
    GUIDrawCommandBuffer    command_buffer;
    int                     current_window;
    uint32_t                active_window;
    int                     next_active_window;
    uint                    frame_count;
    enum GUIOrigin          next_origin;
    enum GUIOrigin          default_origin;
    float                   default_text_scale;
    SpriteFont              *next_font;
    GUIButtonStyle          *button_style;
    GUIProgressBarStyle     *progressbar_style;
    uint32_t                pressed_button;
    uint32_t                hovered_button;
    uint32_t                next_pressed_button;
    int                     next_pressed_button_parent_index;
    uint32_t                next_hovered_button;
    int                     next_hovered_button_parent_index;
    int                     hovered_window_index;
    GUIWindowStyle          *next_window_style;
    GUIButtonStyle          *next_button_style;

    bool32                  next_button_textures_changed;
    _GUIButtonTexture       next_button_normal_texture;
    _GUIButtonTexture       next_button_hovered_texture;
    _GUIButtonTexture       next_button_pressed_texture;
};

int
GUI_init();

 /* Called before updating any of the rest of the UI state.
  * Calling beginUpdate signifies the beginning of a new frame - don't
  * call it more than once per frame! */
void
GUI_beginUpdate();

void
GUI_endUpdate();

void
GUI_endAllWindows();

/* Returns 1 if the button came up this frame */
bool32
GUI_button(const char *label,
    int x, int y, int w, int h);

bool32
GUI_texButton(const char *label,
    Texture *tex, int *clip,
    float tex_scale_x, float tex_scale_y,
    int tex_offset_x, int tex_offset_y, /* From top left ignoring gui_origin */
    int x, int y, int w, int h);

void
GUI_progressBar(uint current_value, uint max_value, int x, int y,
    int fill_offset_x, int fill_offset_y, int fill_direction,
    float *text_color);

void
GUI_beginWindow(const char *title,
    int x, int y, int w, int h);

/* Begin an invisible window for a new parent position.
 * End with GUI_endWindow() */
void
GUI_beginEmptyWindow(const char *title,
    int x, int y, int w, int h);

void
GUI_endWindow();

void
GUI_textLine(const char *text, int x, int y);

void
GUI_text(const char *text, int x, int y);

void
GUI_text_Scale(const char *text, int x, int y, float scale);

void
GUI_texture(Texture *texture, int x, int y);

void
GUI_texture_Clip(Texture *texture, int x, int y,
    int cx, int cy, int cw, int ch);

void
GUI_texture_Scale(Texture *texture, int x, int y,
    float scale_x, float scale_y);

void
GUI_texture_ClipScale(Texture *texture, int x, int y,
    int cx, int cy, int cw, int ch,
    float scale_x, float scale_y);

/* Pass in 0 to reset to default style */
static inline void
GUI_setNextWindowStyle(GUIWindowStyle *style);

static inline void
GUI_setNextButtonStyle(GUIButtonStyle *style);

static inline void
GUI_setNextOrigin(enum GUIOrigin origin);

static inline void
GUI_setDefaultTextScale(float scale);

static inline void
GUI_setDefaultOrigin(enum GUIOrigin origin);

void
GUI_setNextNormalButtonTextureParams(Texture *tex, int *clip,
    int offset_x, int offset_y, float scale_x, float scale_y);

void
GUI_setNextHoveredButtonTextureParams(Texture *tex, int *clip,
    int offset_x, int offset_y, float scale_x, float scale_y);

void
GUI_setNextPressedButtonTextureParams(Texture *tex, int *clip,
    int offset_x, int offset_y, float scale_x, float scale_y);


static inline void
GUI_setNextOrigin(enum GUIOrigin origin)
{
    gui->next_origin = origin;
}

static inline void
GUI_setDefaultTextScale(float scale)
{
    gui->default_text_scale = scale;
}

static inline void
GUI_setDefaultOrigin(enum GUIOrigin origin)
{
    gui->default_origin = origin;
}

static inline void
GUI_setNextFont(SpriteFont *font)
{
    ASSERT(gui->began_update);
    gui->next_font = font;
}

static inline void
GUI_setNextWindowStyle(GUIWindowStyle *style)
{
    ASSERT(gui->began_update);
    gui->next_window_style = style;
}

static inline void
GUI_setNextButtonStyle(GUIButtonStyle *style)
{
    ASSERT(gui->began_update);
    gui->next_button_style = style;
}

