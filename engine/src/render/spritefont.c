

int
SpriteFont_init(SpriteFont *font,
    const char *ttf_font_path,
    uint font_size,
    Color *color)
{
    if (KYS_IS_TEXT_MODE()) return 0;

    Font *f = TTF_OpenFont(ttf_font_path, (int)font_size);
    if (!font)
    {
        DEBUG_PRINTF("SpriteFont_init(): OpenFont() failure. TTF_Error: %s\n",
            TTF_GetError());
        return 1;
    }

    char chars[256];
    for (int c = 0; c < 255; ++c)
        chars[c] = (char)(c+1);
    chars[255] = '\0';

    SDL_Surface *s = TTF_RenderText_Blended(f, chars, *color);

    if (!s)
    {
        DEBUG_PRINTF("SpriteFont_init(): surface creation failure. TTF_Error: %s\n",
            TTF_GetError());
        TTF_CloseFont(f);
        return 2;
    }

    glGenTextures(1, &font->texture.id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, font->texture.id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
        s->w, s->h,
        0, GL_RGBA, GL_UNSIGNED_BYTE,
        s->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
    font->texture.width  = s->w;
    font->texture.height = s->h;

    int ret;
    int minx, maxx, miny, maxy, advance;
    int current_advance = 0;
    for (int i = 0; i < 256; ++i)
    {
        ret = TTF_GlyphMetrics(f, (Uint16)i, &minx, &maxx, &miny, &maxy, &advance);

        if (ret)
        {
            font->characters[i].clip[0] = 0;
            font->characters[i].clip[1] = 0;
            font->characters[i].clip[2] = 0;
            font->characters[i].clip[3] = 0;
            font->characters[i].advance = 0.0f;
            continue;
        }

        font->characters[i].clip[0] = (uint)(current_advance + minx);
        font->characters[i].clip[1] = 0;
        font->characters[i].clip[2] = (uint)(maxx - minx);
        font->characters[i].clip[3] = (uint)(s->h);
        font->characters[i].min_x   = (float)minx;
        font->characters[i].advance = (float)advance;

        current_advance += advance;
    }

    font->height = (float)TTF_FontHeight(f);

    SDL_FreeSurface(s);
    TTF_CloseFont(f);

    return 0;
}
