#pragma once
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <assert.h>
#include <limits.h>
#include <stdint.h>

#if _MSC_VER == 1800
	#define inline __inline
#endif

extern int _using_curses;

#ifdef _DEBUG

    #include <stdio.h>
    #define DEBUG_PRINTF(str, ...)\
        (!_using_curses ? fprintf(stdout, str, ##__VA_ARGS__) : (void)0)

    #define SET_BREAKPOINT() DebugBreak()

#else
    #define DEBUG_PRINTF(str, ...)
    #define SET_BREAKPOINT()
#endif

#define PRINTF(str, ...) \
    (!_using_curses ? printf(str, ##__VA_ARGS__) : (void)0)

#define PUTS(str) \
    (!_using_curses ? puts(str) : (void)0)

#define ASSERT(expression) assert(expression)
#define SYS_MEMORY_BARRIER() MemoryBarrier()
#define SYS_READ_BARRIER() _ReadBarrier(); _mm_mfence()
#define SYS_WRITE_BARRIER() _WriteBarrier()

#define thread_ret_t DWORD WINAPI

typedef struct Thread               Thread;
typedef CRITICAL_SECTION            Mutex;
typedef CONDITION_VARIABLE          ConditionVariable;
typedef SOCKET                      sys_socket_t;
typedef HANDLE                      Semaphore;

static inline int
Thread_create(Thread *thread, LPTHREAD_START_ROUTINE func, void *args);

static inline void
Thread_join(Thread *thread);

static inline void
Thread_detach(Thread *thread);

static inline void
Thread_kill(Thread *thread);

static inline void
Mutex_init(Mutex *mutex);

static inline void
Mutex_destroy(Mutex *mutex);

static inline void
Mutex_lock(Mutex *mutex);

static inline void
Mutex_unlock(Mutex *mutex);

static inline void
ConditionVariable_init(ConditionVariable *cv);

static inline void
ConditionVariable_destroy(ConditionVariable *cv);

static inline int
ConditionVariable_wait(ConditionVariable *cv, Mutex *m);

static inline void
ConditionVariable_notifyOne(ConditionVariable *cv);

static inline void
ConditionVariable_notifyAll(ConditionVariable *cv);

static inline void
Semaphore_init(Semaphore *sem, int initial_count);

static inline void
Semaphore_wait(Semaphore *sem);

static inline void
Semaphore_release(Semaphore *sem);

static inline int32_t
sys_interlockedExchangeInt32(volatile int32_t *target, int32_t val);

static inline int32_t
sys_interlockedIncrementInt32(int32_t volatile *target);

static inline int32_t
sys_interlockedDecrementInt32(int32_t volatile *target);

static inline int32_t
sys_interlockedCompareExchangeInt32(volatile *target,
    int32_t exchange, int32_t comparand);

static inline void
sys_sleepMilliseconds(unsigned int ms);

static inline void *
sys_allocateMemory(size_t size);

static inline int
sys_getSockErrorString(char *buf, int buf_len);

static inline int
sys_initSockAPI();

static inline int
sys_closeSocket(sys_socket_t sock);

static inline int
sys_listenSocket(sys_socket_t sock, int backlog);

static inline int
sys_setSocketNonBlocking(sys_socket_t sock);

static inline int
sys_getNumCPUs();

struct Thread
{
    HANDLE  handle;
    DWORD   id;
};

typedef int32_t SpinLock;

struct ConditionVariable
{
    CRITICAL_SECTION    *critical_section;
    CONDITION_VARIABLE  native_handle;
};

static inline int
Thread_create(Thread *thread, LPTHREAD_START_ROUTINE func, void *args)
{
    thread->handle = CreateThread(0, 0, func, args, 0, &thread->id);
    return thread->handle == NULL;
}

static inline void
Thread_join(Thread *thread)
{
    WaitForSingleObject(thread->handle, INFINITE);
}

static inline void
Thread_detach(Thread *thread)
{
    CloseHandle(thread->handle);
}

static inline void
Thread_kill(Thread *thread)
{
    TerminateThread(thread->handle, THREAD_TERMINATE);
}

static inline void
Mutex_init(Mutex *mutex)
{
    InitializeCriticalSection(mutex);
}

static inline void
Mutex_destroy(Mutex *mutex)
{
    DeleteCriticalSection(mutex);
}

static inline void
Mutex_lock(Mutex *mutex)
{
    EnterCriticalSection(mutex);
}

static inline void
Mutex_unlock(Mutex *mutex)
{
    LeaveCriticalSection(mutex);
}

static inline void
ConditionVariable_init(ConditionVariable *cv)
{
    InitializeConditionVariable(cv);
}

static inline void
ConditionVariable_destroy(ConditionVariable *cv)
{
    /* Win32 has no DeleteConditionVariable function */
}

static inline int
ConditionVariable_wait(ConditionVariable *cv, Mutex *m)
{
    if (!SleepConditionVariableCS(cv, m, INFINITE)) return 1;
    return 0;
}

static inline void
ConditionVariable_notifyOne(ConditionVariable *cv)
{
    WakeConditionVariable(cv);
}

static inline void
ConditionVariable_notifyAll(ConditionVariable *cv)
{
    WakeAllConditionVariable(cv);
}

static inline void
Semaphore_init(Semaphore *sem, int initial_count)
{
    *sem = CreateSemaphoreEx(0,
        (LONG)initial_count,
        (LONG)INT_MAX, 0, 0,
        SEMAPHORE_ALL_ACCESS);
}

static inline void
Semaphore_wait(Semaphore *sem)
{
    WaitForSingleObject(*sem, INFINITE);
}

static inline void
Semaphore_release(Semaphore *sem)
{
    ReleaseSemaphore(*sem, 1, 0);
}

static inline int32_t
sys_interlockedExchangeInt32(int32_t volatile *target, int32_t val)
{
    return InterlockedExchange(target, val);
}

/* If target is equal to comparand, exchange is stored in place of target */
static inline int32_t
sys_interlockedCompareExchangeInt32(
    int32_t volatile *target,
    int32_t exchange,
    int32_t comparand)
{
    return InterlockedCompareExchange(target, exchange, comparand);
}

sys_interlockedIncrementInt32(int32_t volatile *target)
{
    return InterlockedIncrement(target);
}

static inline int32_t
sys_interlockedDecrementInt32(int32_t volatile *target)
{
    return InterlockedDecrement(target);
}

static inline void
sys_sleepMilliseconds(unsigned int ms)
{
    Sleep(ms);
}

static inline void *
sys_allocateMemory(size_t size)
{
    return VirtualAlloc(0, size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
}

static inline int
sys_getSockErrorString(char *buf, int buf_len)
{
    wchar_t *str = 0;

    FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        0,
        WSAGetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPWSTR)&str, 0, 0);
    wcstombs(buf, str, buf_len);
    LocalFree(str);

    return strlen(buf);
}

static inline int
sys_initSockAPI()
{
    WSADATA wsa_data;
    return WSAStartup(MAKEWORD(2, 2), &wsa_data);
}

static inline int
sys_closeSocket(sys_socket_t sock)
{
    return closesocket(sock);
}

static inline int
sys_listenSocket(sys_socket_t sock, int backlog)
{
    return listen(sock, backlog);
}

static inline int
sys_setSocketNonBlocking(sys_socket_t sock)
{
    u_long mode = 1;
    return ioctlsocket(sock, FIONBIO, &mode) == 0 ? 0 : 1;
}

static inline int
sys_getNumCPUs()
{
    SYSTEM_INFO info;
    GetSystemInfo(&info);
    return info.dwNumberOfProcessors;
}
