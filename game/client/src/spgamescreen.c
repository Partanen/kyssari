Entity *
sp_createEnemy(Scene3D *scene3d, SPGameScreenData *screendata, Model *enemymodel,
    Material *enemymaterial, float x, float y, float z)
{
    float scale = 1.0f;
    Entity *e = Scene3D_createEntity(scene3d, x, y, z);
    Entity_setName(e, "enemy");

    ModelComponent *m = Entity_attachModelComponent(e);
    RenderModel *rm = ModelComponent_getDefaultRenderModel(m);

    rm->model                       = enemymodel;
    rm->shader                      = CEL_SHADER_OPAQUE;
    rm->material                    = enemymaterial;
    rm->outline_normal_offset       = 0.05f;
    rm->relative_transform.pos[1]   = -5.0f;
    rm->relative_transform.rot[1]   = 20.0f;
    rm->relative_transform.scale[0] = 0.2f;
    rm->relative_transform.scale[1] = 0.2f;
    rm->relative_transform.scale[2] = 0.2f;

    m->visible = 1;

    Transform *t = &Entity_getTransform(e)->transform;

    t->scale[0] = scale;
    t->scale[1] = scale;
    t->scale[2] = scale;

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
    PhysicsComponent_setActivity(pc, 1);

    pc->friction        = 2.0f;
    pc->max_velocity    = 5.0f;
    pc->elasticity      = 0.0f;
    pc->gravity_active  = 1;

    //animations
    {
        Animator *a = ModelComponent_attachNamedAnimator(m, scene3d,
            ModelComponent_getDefaultRenderModel(m), "player");
        Animator_playAnimation(a, &ig_assets.run_animation, 1);
    }

    {
        SphereCollider *sphereCollider = PhysicsComponent_attachSphereCollider(
            pc, e, 3);
        sphereCollider->radius = 1.0f;
    }

    ScriptComponent *s = Entity_attachScriptComponent(e);
    ScriptComponent_allocateData(s, sizeof(EnemyData));
    ScriptComponent_setCallback(s, sp_enemyScript);

    ((EnemyData*)s->data)->hp       = 3;
    ((EnemyData*)s->data)->speed    = 2;

    return e;
}

void
createExplosion(Scene3D *scene3d, float x, float y, float z, vec3 pos)
{
    Entity *explosion = Scene3D_createEntity(scene3d, x, y, z);
    ModelComponent *m = Entity_attachModelComponent(explosion);
    m->render_models->shader = CEL_SHADER_OPAQUE;
    m->render_models->mode = RENDER_MODEL_NORMAL;
    Animator *a = ModelComponent_attachAnimator(m, scene3d,
        ModelComponent_getDefaultRenderModel(m), 0);
    ASSERT(a);
    Animator_playAnimation(a, &ig_assets.explosion_animation, 0);

    m->visible = 1;

    PhysicsComponent *pc = Entity_attachPhysicsComponent(explosion);
    PhysicsComponent_setActivity(pc, 1);
    PhysicsComponent_setStaticity(pc, 1);
    pc->gravity_active = 0;
    ScriptComponent *sc = Entity_attachScriptComponent(explosion);
    ScriptComponent_allocateData(sc, sizeof(float));
    *((float*)sc->data) = 0;
    ScriptComponent_setCallback(sc, sp_explosionScript);
    Entity_setLifetime(explosion, 0.2f);
    PhysicsComponent_setCollisionCallback(pc, sp_explosionCallback);
    SphereCollider *sphereCollider = PhysicsComponent_attachSphereCollider(pc, explosion, 0);
    sphereCollider->radius = 2.0f;

    vec3 posi = { x, y, z };

    vec3 expl_dist_vec;
    vec3_sub(expl_dist_vec, pos, posi);

    float dist_to_expl = sqrtf(expl_dist_vec[0]* expl_dist_vec[0] 
        + expl_dist_vec[1] * expl_dist_vec[1] 
        + expl_dist_vec[2] * expl_dist_vec[2]);

    if (dist_to_expl >= MAX_DISTANCE_TO_PLAY_SOUND)
    {
        return;
    }

    float perse = dist_to_expl / MAX_DISTANCE_TO_PLAY_SOUND;
    if (perse > 1.0f) perse = 1.0f;
    perse = 1.0f - perse;

    SoundEffect_setVolume(permanent_assets.se_explosion, (int)(64.0f * perse));

    SoundEffect_play(permanent_assets.se_explosion,
        SOUND_CHANNEL_PLAYER_EXPLOSION, 0);
}

Entity *
createCube(Scene3D *scene3d, float x, float y, float z, float scale)
{
    Entity *e = Scene3D_createEntity(scene3d, x, y, z);
    Transform *t = &Entity_getTransform(e)->transform;

    t->scale[0] = scale;
    t->scale[1] = scale;
    t->scale[2] = scale;

    Entity_attachPhysicsComponent(e);
    PhysicsComponent *pc = Entity_getPhysicsComponent(e);
    pc->gravity_active = 0;

    {
        //PhysicsComponent_attachBoxCollider(pc, e, "box");
        ////box->dimensions[0] = scale / 2.0f;
        ////box->dimensions[1] = scale / 2.0f;
        ////box->dimensions[2] = scale / 2.0f;
    }

    {
        ModelComponent *m = Entity_attachModelComponent(e);
        m->render_models->model = getDefaultCubeModel();
        m->visible = 1;
        m->render_models->material = GET_INGAME_MATERIAL("explosion");
    }

    return e;
}

Entity *
sp_createItem(Scene3D *scene3d,SPGameScreenData *screendata,
    float x, float y, float z, float scale, float radius,
    enum KsItemType item_id)
{
    Entity *e = Scene3D_createEntity(scene3d, x, y, z);
    Transform *t = &Entity_getTransform(e)->transform;

    t->scale[0] = scale;
    t->scale[1] = scale;
    t->scale[2] = scale;

    Entity_attachPhysicsComponent(e);

    PhysicsComponent *pc = Entity_getPhysicsComponent(e);
    PhysicsComponent_setActivity(pc, 1);
    pc->gravity_active = 0;

    PhysicsComponent_setCollisionCallback(pc, sp_itemCollisionCallback);
    SphereCollider *sphere = PhysicsComponent_attachSphereCollider(pc, e, 0);
    sphere->radius = radius;
    vec3_set(sphere->position, 0.0f, 0.0f, 0.0f);

    SET_BITFLAG_ON(sphere->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY);

    ModelComponent *m = Entity_attachModelComponent(e);
    m->render_models->shader = CEL_SHADER_OPAQUE;

    switch (item_id)
    {
        case ITEM_MEDKIT:
            m->render_models->model = GET_INGAME_MODEL("medkit");
            m->render_models->material = GET_INGAME_MATERIAL("medkit");
            m->render_models->outline_normal_offset = 0.005f;
            Entity_setName(e, "medkit");
            break;
        case ITEM_ROCKET_AMMO:
            m->render_models->model = GET_INGAME_MODEL("rocket ammo");
            m->render_models->material = GET_INGAME_MATERIAL("rocket ammo");
            m->render_models->outline_normal_offset = 0.02f;
            Entity_setName(e, "rocket ammo");
            break;
        case ITEM_SMG_AMMO:
            m->render_models->model = GET_INGAME_MODEL("smg ammo");
            m->render_models->material = GET_INGAME_MATERIAL("smg ammo");
            m->render_models->outline_normal_offset = 0.03f;
            Entity_setName(e, "smg ammo");
            break;
        case ITEM_RAILGUN_AMMO:
            m->render_models->model = GET_INGAME_MODEL("railgun ammo");
            m->render_models->material = GET_INGAME_MATERIAL("railgun ammo");
            m->render_models->outline_normal_offset = 0.05f;
            Entity_setName(e, "railgun ammo");
            break;
        default:
            m->render_models->model = getDefaultBallModel();
            break;
    }

    m->visible = 1;

    ScriptComponent *s = Entity_attachScriptComponent(e);
    ScriptComponent_allocateData(s, sizeof(float));
    ScriptComponent_setCallback(s, sp_itemScript);

    return e;
}

int
createGameScreen()
{
    game.screens[SP_GAME_SCREEN] = createScreen(GameScreen_update, &game.game_screen_data);

    if (!game.screens[SP_GAME_SCREEN])
        return 1;

    game.screens[SP_GAME_SCREEN]->onOpen    = GameScreen_open;
    game.screens[SP_GAME_SCREEN]->onClose   = GameScreen_close;

    SPGameScreenData *data = (SPGameScreenData*)game.screens[SP_GAME_SCREEN]->screen_data;

    /* Loading assets */

    data->p1_score = 1;
    data->p2_score = 2;
    Camera_init(&data->camera);
    data->camera_follow_on = 1;

    return 0;
}

void
GameScreen_update(Screen *screen)
{
    SPGameScreenData *data = (SPGameScreenData*)game.screens[SP_GAME_SCREEN]->screen_data;
#define CAMERA_SPEED 20.0f

    if (isKeyPressed(SDL_SCANCODE_ESCAPE) && !data->is_menu_visible)
    {
        data->is_menu_visible = 1;
        setRelativeMouseMode(0);
    }

    vec3 forward;
    Entity_getNonNormalizedForwardVector(data->player, forward);
    forward[1] = 0;
    vec3_norm(forward, forward);

    PhysicsComponent *pc = Entity_getPhysicsComponent(data->player);
    vec3 strafe; vec3_mul_cross(strafe, forward, DEFAULT_UP_VECTOR);

    ScriptComponent *sc     = Entity_getScriptComponent(data->player);
    PlayerScriptData *pd    = (PlayerScriptData*)sc->data;

    ModelComponent *mc      = Entity_getModelComponent(data->player);
    RenderModel *weapon_rm  = ModelComponent_getRenderModelById(mc,
        PLAYER_RENDER_MODEL_ID_WEAPON);

    if (isKeyPressed(SDL_SCANCODE_F3))

        Scene3D_setDebugCollisionGeometryRendering(&game.game_scene, 1);
    if (isKeyPressed(SDL_SCANCODE_F4))
        Scene3D_setDebugCollisionGeometryRendering(&game.game_scene, 0);

    if (!isKeyPressed(SDL_SCANCODE_W) && !isKeyPressed(SDL_SCANCODE_S)
    && !isKeyPressed(SDL_SCANCODE_A)  && !isKeyPressed(SDL_SCANCODE_D))
    {
        pc->acceleration[0] = 0.0f;
        pc->acceleration[2] = 0.0f;
        pd->base.movement_speed = 20.0f;
    }

#   define TRY_PLAY_WALK_SOUND() \
    if (pd->base.is_grounded \
    && !isSoundEffectChannelPlaying(SOUND_CHANNEL_FOOTSTEPS)) \
    { \
        SoundEffect_play(permanent_assets.se_steps, \
            SOUND_CHANNEL_FOOTSTEPS, 0); \
    } \

    if (isKeyPressed(SDL_SCANCODE_W))
    {
        if (!data->camera_follow_on)
            Camera_moveForward(&data->camera, CAMERA_SPEED *(float)getDelta());
        else
        {
            TRY_PLAY_WALK_SOUND();

            pc->velocity[0] = forward[0] * pd->base.movement_speed;
            pc->velocity[2] = forward[2] * pd->base.movement_speed;

            if (isKeyPressed(SDL_SCANCODE_A) && isKeyPressed(SDL_SCANCODE_W))
            {
                pc->velocity[0] = (-strafe[0] + forward[0]) * (pd->base.movement_speed / 2);
                pc->velocity[2] = (-strafe[2] + forward[2]) * (pd->base.movement_speed / 2);
            }

            if (isKeyPressed(SDL_SCANCODE_D) && isKeyPressed(SDL_SCANCODE_W))
            {
                pc->velocity[0] = (strafe[0] + forward[0]) * (pd->base.movement_speed / 2);
                pc->velocity[2] = (strafe[2] + forward[2]) * (pd->base.movement_speed / 2);
            }
        }
    }

    if (isKeyPressed(SDL_SCANCODE_S))
    {
        if (!data->camera_follow_on)
            Camera_moveBack(&data->camera, CAMERA_SPEED * (float)getDelta());
        else
        {
            TRY_PLAY_WALK_SOUND();

            pc->velocity[0] = -forward[0] * pd->base.movement_speed;
            pc->velocity[2] = -forward[2] * pd->base.movement_speed;

            if (isKeyPressed(SDL_SCANCODE_A) && isKeyPressed(SDL_SCANCODE_S))
            {
                pc->velocity[0] = (-strafe[0] + -forward[0]) * (pd->base.movement_speed / 2);
                pc->velocity[2] = (-strafe[2] + -forward[2]) * (pd->base.movement_speed / 2);
            }

            if (isKeyPressed(SDL_SCANCODE_D) && isKeyPressed(SDL_SCANCODE_S))
            {
                pc->velocity[0] = (strafe[0] + -forward[0]) * (pd->base.movement_speed / 2);
                pc->velocity[2] = (strafe[2] + -forward[2]) * (pd->base.movement_speed / 2);
            }
        }
    }

    if (isKeyPressed(SDL_SCANCODE_A) && !isKeyPressed(SDL_SCANCODE_W)
    && !isKeyPressed(SDL_SCANCODE_S))
    {
        if (!data->camera_follow_on)
            Camera_strafeLeft(&data->camera, CAMERA_SPEED * (float)getDelta());
        else
        {
            TRY_PLAY_WALK_SOUND();
            pc->velocity[0] = -strafe[0] * pd->base.movement_speed;
            pc->velocity[2] = -strafe[2] * pd->base.movement_speed;
        }
    }

    if (isKeyPressed(SDL_SCANCODE_D) && !isKeyPressed(SDL_SCANCODE_W)
    && !isKeyPressed(SDL_SCANCODE_S))
    {
        if (!data->camera_follow_on)
            Camera_strafeRight(&data->camera, CAMERA_SPEED * (float)getDelta());
        else
        {
            TRY_PLAY_WALK_SOUND();
            pc->velocity[0] = strafe[0] * pd->base.movement_speed;
            pc->velocity[2] = strafe[2] * pd->base.movement_speed;
        }
    }

    if (isKeyPressed(SDL_SCANCODE_E))
    {
        enum KsWeaponType new_weapon;

        switch (pd->weapon)
        {
            case ROCKET_LAUNCHER:   new_weapon = SMG;               break;
            case SMG:               new_weapon = RAILGUN;           break;
            case RAILGUN:           new_weapon = MACHETE;           break;
            case MACHETE:           new_weapon = ROCKET_LAUNCHER;   break;
            default: {ASSERT(0);} break;
        }

        if (!pd->base.weaponswap_pressed && pd->animation_timer <= 0.0f)
        {
            setPlayerPawnWeaponLocal(data->player, new_weapon);
            pd->base.weaponswap_pressed = 1;
        }
    }
    else if (!isKeyPressed(SDL_SCANCODE_E) && !isKeyPressed(SDL_SCANCODE_Q))
        pd->base.weaponswap_pressed = 0;

    if (isKeyPressed(SDL_SCANCODE_Q))
    {
        enum KsWeaponType new_weapon;

        switch (pd->weapon)
        {
            case ROCKET_LAUNCHER:   new_weapon = MACHETE;           break;
            case SMG:               new_weapon = ROCKET_LAUNCHER;   break;
            case RAILGUN:           new_weapon = SMG;               break;
            case MACHETE:           new_weapon = RAILGUN;           break;
            default: {ASSERT(0); } break;
        }

        if (!pd->base.weaponswap_pressed && pd->animation_timer <= 0.0f)
        {
            setPlayerPawnWeaponLocal(data->player, new_weapon);
            pd->base.weaponswap_pressed = 1;
        }
    }
    else if (!isKeyPressed(SDL_SCANCODE_Q) && !isKeyPressed(SDL_SCANCODE_E))
        pd->base.weaponswap_pressed = 0;

    if (isKeyPressed(SDL_SCANCODE_F) && !pd->base.f_pressed)
    {
        pd->base.f_pressed = 1;

        if (!pd->base.f_toggled)
            pd->base.f_toggled = 1;
        else
            pd->base.f_toggled = 0;

        /* For SMG, the visual needs to be updated */
        if (pd->weapon == SMG)
            setPlayerPawnVisualWeaponModel(weapon_rm, SMG, pd);
    }
    else if (!isKeyPressed(SDL_SCANCODE_F))
    {
        pd->base.f_pressed = 0;
    }

#   undef TRY_PLAY_WALK_SOUND

    if (isKeyPressed(SDL_SCANCODE_SPACE))
    {
        if (!pd->base.jump_pressed)
        {
            if (!data->camera_follow_on)
                Camera_moveUp(&data->camera, CAMERA_SPEED * (float)getDelta());
            else
            {
                if (pd->base.is_grounded)
                {
                    pc->velocity[1] = 20.0f;
                    pd->base.is_jumping = 1;
                    pd->base.is_grounded = 0;
                }
            }
        }
    }

    if (isKeyPressed(SDL_SCANCODE_X))
    {
        Camera_moveDown(&data->camera, CAMERA_SPEED * (float)getDelta());
    }

    if (isKeyPressed(SDL_SCANCODE_Y))
    {
        if (!isKeyPressed(SDL_SCANCODE_LSHIFT))
            data->show_position_text = 1;
        else
            data->show_position_text = 0;
    }

    if (!data->camera_follow_on && mouseMovedThisFrame() && isRelativeMouseMode())
    {
        Camera_turn(&data->camera,
            (float)getMouseRelativeMovement()[0] * (float)getDelta(),
            -(float)getMouseRelativeMovement()[1] * (float)getDelta(),
            0.1f);
    }

    if (isKeyPressed(SDL_SCANCODE_K))
        data->camera_follow_on = 1;
    if (isKeyPressed(SDL_SCANCODE_J))
        data->camera_follow_on = 0;

    //Player rotation with mouse
    if (data->camera_follow_on && mouseMovedThisFrame() && isRelativeMouseMode())
    {
        turnPlayerEntity(data->player, (int16_t)getMouseRelativeMovement()[0],
            (int16_t)getMouseRelativeMovement()[1]);
    }

    if (getMouseButtonState(MOUSE_BUTTON_LEFT) && !data->is_menu_visible)
        sp_shootAsPlayerPawn(data->player);

    if (getMouseButtonState(MOUSE_BUTTON_RIGHT))
    {
        if (pd->base.is_grounded)
        {
            pc->velocity[1]         = 10;
            pd->base.is_grounded    = 0;
            pd->base.movement_speed = 30.0f;
            pd->base.is_dashing     = 1;
        }
    }

    Scene3D_updateSimulation(&game.game_scene, getDelta());

    //camera follow
    if (data->camera_follow_on)
    {
        Transform *t = &Entity_getTransform(data->player)->transform;

        vec3 forward;
        Entity_getForwardVector(data->player, forward);

        data->camera.position[0] = t->pos[0];
        data->camera.position[1] = t->pos[1];
        data->camera.position[2] = t->pos[2];

        vec3 temp;

        vec3_add(temp, data->camera.position, forward);
        Camera_lookAt(&data->camera, temp);
    }

    Transform *t = &Entity_getTransform(data->player)->transform;
    vec3_copy(game.game_scene.camera_position, t->pos);

    double dt = getDelta();

    if (isKeyPressed(SDL_SCANCODE_DOWN))
        graphics_config.draw_distance -= 50.0f * (float)dt;
    if (isKeyPressed(SDL_SCANCODE_UP))
        graphics_config.draw_distance += 50.0f * (float)dt;
    if (isKeyPressed(SDL_SCANCODE_LEFT))
        graphics_config.field_of_view -= 0.7f * (float)dt;
    if (isKeyPressed(SDL_SCANCODE_RIGHT))
        graphics_config.field_of_view += 0.7f * (float)dt;

    float mul;

    if (isKeyPressed(SDL_SCANCODE_LSHIFT))
        mul = -1.0f;
    else
        mul = 1.0f;

    if (isKeyPressed(SDL_SCANCODE_7))
    {
        ModelComponent *mc = Entity_getModelComponent(data->player);
        RenderModel *rm = ModelComponent_getRenderModelById(mc,
            PLAYER_RENDER_MODEL_ID_WEAPON);
        rm->relative_transform.pos[0] += 0.05f * mul;
    }

    if (isKeyPressed(SDL_SCANCODE_8))
    {
        ModelComponent *mc = Entity_getModelComponent(data->player);
        RenderModel *rm = ModelComponent_getRenderModelById(mc,
            PLAYER_RENDER_MODEL_ID_WEAPON);
        rm->relative_transform.pos[1] += 0.05f * mul;
    }

    if (isKeyPressed(SDL_SCANCODE_9))
    {
        ModelComponent *mc = Entity_getModelComponent(data->player);
        RenderModel *rm = ModelComponent_getRenderModelById(mc,
            PLAYER_RENDER_MODEL_ID_WEAPON);
        rm->relative_transform.pos[2] += 0.05f * mul;
    }

    //render
    data->camera.fov        = graphics_config.field_of_view;
    data->camera.far_clip   = graphics_config.draw_distance;
    renderScene3D(&game.game_scene, &data->camera, 0);

    renderTexture(permanent_assets.crosshair_texture,
        ((float)getWindowWidth() / 2) - 4.0f,
        ((float)getWindowHeight() / 2) - 4.0f);

    if (pd->base.is_hit)
    {
        renderTexture(permanent_assets.hitscreen_texture,
            0, 0);
    }

    {
        Texture *selected_weapon_texture;

        switch (pd->weapon)
        {
        case ROCKET_LAUNCHER:   selected_weapon_texture = permanent_assets.rocket_selected_texture; break;
        case SMG:               selected_weapon_texture = permanent_assets.smg_selected_texture; break;
        case RAILGUN:           selected_weapon_texture = permanent_assets.railgun_selected_texture; break;
        case MACHETE:           selected_weapon_texture = permanent_assets.machete_selected_texture; break;
        default: {ASSERT(0); } break;
        }

        renderTexture(selected_weapon_texture,
            300.0f, (float)getWindowHeight() - 96.0f);
    }

    GUI_beginUpdate();

    if (data->is_menu_visible)
        drawGameMenu(&data->is_menu_visible);

    GUI_progressBar(pd->base.hp, 100, 16, getWindowHeight() - 48, 54, 4,
        GUI_FILLDIRECTION_RIGHT, F_COLOR_WHITE);

    /* FPS text */
    char buf[128];
    sprintf(buf, "%f", engine.clock.fps);
    GUI_text(buf, 0, -16);

    data->show_position_text = 1;
    if (data->show_position_text)
    {
        sprintf(buf, "%f, %f, %f", t->pos[0], t->pos[1], t->pos[2]);
        GUI_text(buf, 0, 0);
    }

    if (!data->camera_follow_on)
        GUI_text("Camera-free-look on - press 'k' to disable ('j' to enable)",
            20, 20);

    GUI_endUpdate();
}

void
GameScreen_open(Screen *screen)
{
    int r = sp_initGameSceneForLevel1();
    ASSERT(!r);
    SPGameScreenData *data = (SPGameScreenData*)screen->screen_data;
    data->player = Scene3D_createEntity(&game.game_scene, 0.0f, 10.0f, 0.0);
    client_initPlayerPawn(data->player);
    data->show_position_text    = 0;
    data->is_menu_visible       = 0;
    data->camera_follow_on      = 1;
}

void
GameScreen_close(Screen *screen)
{
    SPGameScreenData *data = (SPGameScreenData*)screen->screen_data;
    Scene3D_removeEntity(&game.game_scene, data->player);
}

void
moveEntity(Entity *entity, float speed)
{
    EntityTransform *et = Entity_getTransform(entity);
    vec3 forward;
    Transform_getNonNormalizedForwardVector(&et->transform, forward);

    PhysicsComponent *pc = Entity_getPhysicsComponent(entity);
    pc->velocity[0] = forward[0] * speed;
    pc->velocity[2] = forward[2] * speed;
}

int
sp_initGameSceneForLevel1()
{
    Scene3D_clear(&game.game_scene);

    if (Scene3D_init(&game.game_scene, game.scene_memory,
        game.scene_memory_size, 2048, 1) != 0)
        return 1;

    Scene3D_setBackgroundColor(&game.game_scene, 0.0f, 0.0f, 1.0f, 0.0f);
    Scene3D_setAmbientColor(&game.game_scene, 1.0f, 1.0f, 1.0f, 0.5f);

#if 0
    game.game_scene.on_replicated_entity_creation_callback =
        onReplicatedEntityCreationCallback;
#endif

    createMapEntity(&game.game_scene,
        GET_INGAME_MODEL("map"),
        GET_INGAME_MODEL("map nocol"),
        GET_INGAME_MATERIAL_ARRAY("level1"),
        GET_INGAME_MATERIAL_ARRAY("level2"),
        CEL_SHADER_TRANSPARENT, 0, 0, 0);

    if (shared_buildCollisionWorldForLevel1(&game.game_scene,
        GET_INGAME_MODEL("map")) != 0)
    {
        DEBUG_PRINTF("Failed to build level 1 collision world!\n");
        return 2;
    }

    shared_setDefaultScenePhysicsLimits(&game.game_scene);

    vec3 enemy_pos = {0.f, 35.f, 0.f};
    vec3 enemy_vel = {0.f, 0.f, 0.f};

    for (int i = 0; i < 10; ++i)
    {
        enemy_pos[0] = (float)(-50 + rand() % 100);
        enemy_pos[2] = (float)(-50 + rand() % 100);
        sp_createOtherPlayersPawn(&game.game_scene, enemy_pos, enemy_vel,
            rand() % NUM_CHARACTER_TYPES);
    }

    sp_createItem(&game.game_scene, &game.game_screen_data, 15.0f, 5.0f, 2.0f,
        4.0f, 0.5f, ITEM_MEDKIT);
    sp_createItem(&game.game_scene, &game.game_screen_data, 20.0f, 5.0f, -2.0f,
        0.5f, 3.0f, ITEM_ROCKET_AMMO);
    sp_createItem(&game.game_scene, &game.game_screen_data, 21.0f, 5.0f, -5.0f,
        0.5f, 3.0f, ITEM_SMG_AMMO);
    sp_createItem(&game.game_scene, &game.game_screen_data, 23.0f, 5.0f, -10.0f,
        0.25f, 3.0f, ITEM_RAILGUN_AMMO);

    Scene3D_setSkyboxTexture(&game.game_scene,
        permanent_assets.tex_skybox_level1);
    Scene3D_setSkyboxScale(&game.game_scene, 1.0f, 1.0f, 1.0f);

    Entity *ambient_light = Scene3D_createEntity(&game.game_scene, 0.0f, 50.0f,
        0.0f);

    DirectionalLightComponent *blue = Entity_attachDirectionalLightComponent(
        ambient_light);
    DirectionalLightComponent_setDiffuse(blue, 0.0f, 0.0f, 0.2f);
    DirectionalLightComponent_setSpecular(blue, 0.7f, 0.7f, 0.7f);
    DirectionalLightComponent_setDirection(blue, 0.0f, -0.5f, 0.5f, 1);
    blue->radius = 50.0f;

    Entity *sun = Scene3D_createEntity(&game.game_scene, 0.0f, 50.0f, 0.0f);

    DirectionalLightComponent *yellow = Entity_attachDirectionalLightComponent(
        sun);
    DirectionalLightComponent_setDiffuse(yellow, 0.3f, 0.3f, 0.3f);
    DirectionalLightComponent_setSpecular(yellow, 0.7f, 0.7f, 0.7f);
    DirectionalLightComponent_setDirection(yellow, 50.0f, -1.0f, 2.5f, 1);
    yellow->radius = 50.0f;

    PointLightComponent *p = Entity_attachPointLightComponent(sun);
    PointLightComponent_setDiffuse(p, 0.1f, 0.1f, 0.1f);
    PointLightComponent_setSpecular(p, 0.4f, 0.1f, 0.8f);
    PointLightComponent_setAmbient(p, 0.4f, 0.4f, 0.4f);
    p->radius = 150.0f;

    Scene3D_rotateSkybox(&game.game_scene, 0.0f, (float)DEG_TO_RAD(180), 0.0f);

    return 0;
}

void
sp_enemyTakeDamage(Entity *entity, int amount)
{
    ScriptComponent *sc = Entity_getScriptComponent(entity);
    EnemyData *ed = (EnemyData*)sc->data;

    ed->hp -= amount;

    EntityTransform *et = Entity_getTransform(entity);

    if (ed->hp <= 0)
    {
        vec3 random;
        for (int i = 0; i < 3; ++i)
        {
            random[0] = (float)(-5.0f + rand() % 10);
            random[1] = 10.0f;
            random[2] = (float)(-5.0f + rand() % 10);
            createEffect(entity, getDefaultPlaneModel(),
                GET_INGAME_MATERIAL("muzzlefire"), et->transform.pos, random, et->transform.rot,
                1.0f, 1.5f, 1, 1);
        }
        Scene3D_removeEntity(entity->scene, entity);
    }
}

bool32
sp_shootAsPlayerPawn(Entity *pawn)
{
    ScriptComponent *sc     = Entity_getScriptComponent(pawn);  ASSERT(sc);
    PlayerScriptData *pd    = (PlayerScriptData*)sc->data;      ASSERT(pd);

    if (!pd->base.can_shoot) return 0;

    WeaponType *w = GET_WEAPON_TYPE(pd->weapon);
    pd->base.shoot_timer        = w->base.fire_rate;
    pd->base.can_shoot          = 0;
    pd->animation_timer         = w->animation_duration;

    ModelComponent *m = Entity_getModelComponent(pawn);
    ASSERT(m);
    Animator *muzzle_an = ModelComponent_getAnimatorById(m,
        PLAYER_ANIMATOR_ID_MUZZLEFIRE);
    ASSERT(muzzle_an);
    RenderModel *muzzle_rm = ModelComponent_getRenderModelById(m,
        PLAYER_RENDER_MODEL_ID_MUZZLEFIRE);
    ASSERT(muzzle_rm);

    /* Fire up the muzzle fire animation */
    muzzle_rm->visible = 1;
    pd->muzzlefire_duration = w->muzzlefire_duration;
    muzzle_rm->relative_transform.pos[0] = w->muzzlefire_relative_pos[0];
    muzzle_rm->relative_transform.pos[1] = w->muzzlefire_relative_pos[1];
    muzzle_rm->relative_transform.pos[2] = w->muzzlefire_relative_pos[2];
    muzzle_rm->relative_transform.scale[0] = w->muzzlefire_relative_scale[0];
    muzzle_rm->relative_transform.scale[1] = w->muzzlefire_relative_scale[1];
    muzzle_rm->relative_transform.scale[2] = w->muzzlefire_relative_scale[2];

    muzzle_rm->relative_transform.pos[0] = -1.4f;
    muzzle_rm->relative_transform.pos[1] = -0.3f;
    muzzle_rm->relative_transform.pos[2] = -0.5f;
    muzzle_rm->relative_transform.scale[0] = 0.75f;
    muzzle_rm->relative_transform.scale[1] = 0.75f;
    muzzle_rm->relative_transform.scale[2] = 0.75f;

    if (w->sp_on_shoot_callback)
        w->sp_on_shoot_callback(pd->weapon, pawn);

    return 1;
}

void
sp_playerScript(Entity *e, void *data)
{
    float dt                = (float)e->scene->delta;
    PlayerScriptData *d     = (PlayerScriptData*)data;
    ModelComponent *m       = Entity_getModelComponent(e);
    RenderModel *muzzle_rm  = ModelComponent_getRenderModelById(m,
        PLAYER_RENDER_MODEL_ID_MUZZLEFIRE);
    RenderModel *weapon_rm  = ModelComponent_getRenderModelById(m,
        PLAYER_RENDER_MODEL_ID_WEAPON);

    if (muzzle_rm->visible)
    {
        d->muzzlefire_duration -= dt;
        if (d->muzzlefire_duration <= 0.0f)
            muzzle_rm->visible = 0;
    }

    if (!d->base.can_shoot)
    {
        d->base.shoot_timer -= (float)e->scene->delta;

        if (d->base.shoot_timer <= 0)
            d->base.can_shoot = 1;
    }

    if (d->animation_timer >= 0)
    {
        d->animation_timer -= dt;

        switch (d->weapon)
        {
            case ROCKET_LAUNCHER:
                weapon_rm->relative_transform.pos[0] -= 15.0f * dt;
                weapon_rm->relative_transform.rot[2] +=  0.9f * dt;
                break;
            case SMG:
                weapon_rm->relative_transform.pos[0] -=  5.0f * dt;
                weapon_rm->relative_transform.rot[2] -=  0.1f * dt;
                break;
            case RAILGUN:
                weapon_rm->relative_transform.pos[0] -= 6.0f * dt;
                weapon_rm->relative_transform.rot[2] +=  0.65f * dt;
                break;
            default:
                break;
        }

        if (d->animation_timer <= 0)
            setPlayerPawnVisualWeaponModel(weapon_rm, d->weapon, d);
    }

    if (d->base.is_hit)
    {
        d->base.is_hit_timer -= dt;

        if (d->base.is_hit_timer <= 0)
            d->base.is_hit = 0;
    }

    if (d->base.melee_attacking)
    {
        weapon_rm->relative_transform.rot[2] += 15.0f * dt;

        if (weapon_rm->relative_transform.rot[2] > 3.0f)
        {
            setPlayerPawnVisualWeaponModel(weapon_rm, d->weapon, d);
            d->base.melee_attacking = 0;
        }
    }
}

void
sp_onShootCallback_RocketLauncher(enum KsWeaponType weapon_type, Entity *pawn)
{
    vec3 forward;

    Transform *t = &Entity_getTransform(pawn)->transform;
    Transform_getForwardVector(t, forward);

    vec3 spawn_pos;
    vec3_add(spawn_pos, t->pos, forward);

    Entity *e = createBulletEntity(pawn->scene, weapon_type,
        spawn_pos, t->rot, forward);

    ScriptComponent *sc = Entity_getScriptComponent(e);
    vec3_copy(((BulletScriptData*)sc->data)->last_position, t->pos);
    vec3_copy(((BulletScriptData*)sc->data)->player_pos, t->pos);
    /* TODO: add scripts to bullet types */
    ScriptComponent_setCallback(sc, sp_dynamicObjectBulletRay);

    const float ray_len = 300.0f;

    float dist_to_triangle = Scene3D_castRayToStaticGeometry(pawn->scene,
        t->pos, forward, ray_len,
        ((BulletScriptData*)sc->data)->explosion_pos,
        &((BulletScriptData*)sc->data)->dist_to_hit);

    if (dist_to_triangle != 0)
        ScriptComponent_setCallback(sc, sp_rocketBulletScript);

    ModelComponent *pawn_mc = Entity_getModelComponent(pawn);
    RenderModel *weapon_rm = ModelComponent_getRenderModelById(pawn_mc,
        PLAYER_RENDER_MODEL_ID_WEAPON);
    weapon_rm->relative_transform.pos[0] += 200.0f * 0.016f;
    weapon_rm->relative_transform.rot[2] -= 10.0f * 0.016f;

    ModelComponent *m = Entity_getModelComponent(pawn);
    ASSERT(m);
    Animator *muzzle_an = ModelComponent_getAnimatorById(m,
        PLAYER_ANIMATOR_ID_MUZZLEFIRE);
    ASSERT(muzzle_an);
    RenderModel *muzzle_rm = ModelComponent_getRenderModelById(m,
        PLAYER_RENDER_MODEL_ID_MUZZLEFIRE);
    ASSERT(muzzle_rm);
    Animator_playAnimation(muzzle_an, &ig_assets.rocket_muzzle_animation, 0);

    SoundEffect_play(permanent_assets.se_fwump,
        SOUND_CHANNEL_PLAYER_WEAPON, 0);
}

void
sp_onShootCallback_SMG(enum KsWeaponType weapon_type, Entity *pawn)
{

    SoundEffect_play(permanent_assets.se_smg,
        SOUND_CHANNEL_PLAYER_WEAPON, 0);

    vec3 forward, triangle_normal, random, hit_pos, endpoint, temp;
    float dist_to_triangle, dist_to_hit;
    float ray_len = 100.0f;
    Transform *t = &Entity_getTransform(pawn)->transform;
    Transform_getForwardVector(t, forward);

    vec3 spawn_pos;
    vec3_add (spawn_pos, t->pos, forward);
    createBulletEntity(pawn->scene, weapon_type, spawn_pos,
        t->rot, forward);
    vec3_scale(temp, forward, ray_len);
    vec3_add(endpoint, t->pos, temp);

    dist_to_triangle = Scene3D_castRayToStaticGeometryAndGetNormal(
        &game.game_scene, t->pos, forward, ray_len, hit_pos, &dist_to_hit,
        triangle_normal);

    SphereCollider *sphere = Scene3D_castRayToSphereColliders(
        pawn->scene, endpoint, t->pos, &dist_to_hit,
        _ignore_player_collider_ids, _num_ignore_player_collider_ids);

    if (sphere != 0
    && (dist_to_triangle >= dist_to_hit || dist_to_triangle == 0))
    {
        Entity *oe = Scene3D_getEntityByIndex(pawn->scene, sphere->entity_index);
        vec3_copy(hit_pos, Entity_getTransform(oe)->transform.pos);
        sp_enemyTakeDamage(oe, 20);
    }

    vec3_set(random, 0.0f, 0.0f, 0.0f);
    vec3_set(triangle_normal, triangle_normal[0],
        triangle_normal[1] - (float)DEG_TO_RAD(270), triangle_normal[2]);

    createEffect(pawn, getDefaultPlaneModel(),
        GET_INGAME_MATERIAL("bullethole"), hit_pos, random,
        triangle_normal, 0.2f, 1.0f, 0, 0);

    ModelComponent *pawn_mc = Entity_getModelComponent(pawn);
    RenderModel *weapon_rm = ModelComponent_getRenderModelById(pawn_mc,
        PLAYER_RENDER_MODEL_ID_WEAPON);
    weapon_rm->relative_transform.pos[0] += 30.0f * 0.016f;

    ModelComponent *m = Entity_getModelComponent(pawn);
    ASSERT(m);
    Animator *muzzle_an = ModelComponent_getAnimatorById(m,
        PLAYER_ANIMATOR_ID_MUZZLEFIRE);
    ASSERT(muzzle_an);
    RenderModel *muzzle_rm = ModelComponent_getRenderModelById(m,
        PLAYER_RENDER_MODEL_ID_MUZZLEFIRE);
    ASSERT(muzzle_rm);
    Animator_playAnimation(muzzle_an, &ig_assets.smg_muzzle_animation, 0);
}

void
sp_onShootCallback_Railgun(enum KsWeaponType weapon_type, Entity *pawn)
{
    vec3 forward, hit_pos, triangle_normal, endpoint, random, temp;
    float dist_to_triangle, dist_to_hit;

    const float ray_len = 300.0f;

    Transform *t = &Entity_getTransform(pawn)->transform;
    Transform_getForwardVector(t, forward);

    vec3 spawn_pos;
    vec3_add (spawn_pos, t->pos, forward);

    vec3_scale(temp, forward, ray_len);
    vec3_add(endpoint, t->pos, temp);

    dist_to_triangle = Scene3D_castRayToStaticGeometryAndGetNormal(
        pawn->scene, t->pos, forward, ray_len, hit_pos, &dist_to_hit,
        triangle_normal);

    SphereCollider *sphere = Scene3D_castRayToSphereColliders(pawn->scene,
        endpoint, t->pos, &dist_to_hit, _ignore_player_collider_ids,
        _num_ignore_player_collider_ids);

    if (sphere != 0
    && (dist_to_triangle >= dist_to_hit || dist_to_triangle == 0))
    {
        Entity *oe = Scene3D_getEntityByIndex(pawn->scene,
            sphere->entity_index);
        vec3_copy(hit_pos, Entity_getTransform(oe)->transform.pos);
        sp_enemyTakeDamage(oe, 100);
    }

    for (int i = 0; i < 5; ++i)
    {
        random[0] = (float)(-10.0f + rand() % 20);
        random[1] = (float)(5.0f + rand() % 20);
        random[2] = (float)(-10.0f + rand() % 20);
        createEffect(pawn, getDefaultPlaneModel(),
            GET_INGAME_MATERIAL("railgun spark"), hit_pos, random, random,
            0.5f, 1.0f, 1, 1);
    }

    ModelComponent *pawn_mc = Entity_getModelComponent(pawn);
    RenderModel *weapon_rm = ModelComponent_getRenderModelById(pawn_mc,
        PLAYER_RENDER_MODEL_ID_WEAPON);

    weapon_rm->relative_transform.pos[0] += 360.0f * 0.016f;
    weapon_rm->relative_transform.rot[2] -= 40.0f * 0.016f;

    ModelComponent *m = Entity_getModelComponent(pawn);
    ASSERT(m);
    Animator *muzzle_an = ModelComponent_getAnimatorById(m,
        PLAYER_ANIMATOR_ID_MUZZLEFIRE);
    ASSERT(muzzle_an);
    RenderModel *muzzle_rm = ModelComponent_getRenderModelById(m,
        PLAYER_RENDER_MODEL_ID_MUZZLEFIRE);
    ASSERT(muzzle_rm);
    Animator_playAnimation(muzzle_an, &ig_assets.railgun_muzzle_animation, 0);

    SoundEffect_play(permanent_assets.se_railgun,
        SOUND_CHANNEL_PLAYER_WEAPON, 0);
}

void
sp_onShootCallback_Machete(enum KsWeaponType weapon_type, Entity *pawn)
{
    vec3 forward;
    Transform *t = &Entity_getTransform(pawn)->transform;
    Transform_getForwardVector(t, forward);
    ScriptComponent *pawn_sc = Entity_getScriptComponent(pawn);
    ((PlayerScriptData*)pawn_sc->data)->base.melee_attacking = 1;
    SoundEffect_play(permanent_assets.se_railgun, SOUND_CHANNEL_SCREAM, 0);
}

void
sp_enemyScript(Entity *e, void *data)
{
    EnemyData *d = (EnemyData*)data;
    d->travel_distance += 1;
    if (d->travel_distance > 200)
    {
        if (d->speed > 0.0f)
            d->speed = -2.0f;
        else
            d->speed = 2.0f;
        d->travel_distance = 0;
    }

    moveEntity(e, d->speed);
}

void
sp_explosionScript(Entity *entity, void *data)
{
    float *timer = (float*)data;
    double delta = getDelta();
    *(timer) += (float)delta * 15.0f;
    Entity_setScale(entity, *(timer), *(timer), *(timer));
}

void
sp_itemScript(Entity *entity, void *data)
{
    EntityTransform *et = Entity_getTransform(entity);
    et->transform.rot[1] += (float)getDelta();
}

void
sp_rocketBulletScript(Entity *entity, void *scriptdata)
{
    BulletScriptData *bd = (BulletScriptData*)scriptdata;
    double delta = getDelta();
    float time;
    EntityTransform *et = Entity_getTransform(entity);
    time = bd->dist_to_hit / bd->bullet_velocity;
    bd->timer += (float)delta;

    if (bd->timer >= time)
    {
        createExplosion(entity->scene, bd->explosion_pos[0],
            bd->explosion_pos[1], bd->explosion_pos[2], bd->player_pos);
        Scene3D_removeEntity(entity->scene, entity);
    }
    if (!vec3_equals(bd->last_position, et->transform.pos))
    {
        if (Scene3D_castRayToSphereColliders(entity->scene, et->transform.pos,
            bd->last_position, &bd->dist_to_hit, _ignore_player_collider_ids,
            _num_ignore_player_collider_ids))
        {
            Scene3D_removeEntity(entity->scene, entity);
            createExplosion(entity->scene, bd->last_position[0],
                bd->last_position[1], bd->last_position[2], bd->player_pos);
            return;
        }
        vec3_copy(bd->last_position, et->transform.pos);
    }
}

void
sp_dynamicObjectBulletRay(Entity *entity, void *scripdata)
{
    BulletScriptData *bd = (BulletScriptData*)scripdata;
    EntityTransform *et = Entity_getTransform(entity);
    double delta = getDelta();
    bd->timer += (float)delta;

    if (!vec3_equals(bd->last_position, et->transform.pos))
    {
        if (Scene3D_castRayToSphereColliders(entity->scene,
            bd->last_position, et->transform.pos, &bd->dist_to_hit,
            _ignore_player_collider_ids, _num_ignore_player_collider_ids))
        {
            createExplosion(entity->scene, et->transform.pos[0],
                et->transform.pos[1], et->transform.pos[2], bd->player_pos);
            Scene3D_removeEntity(entity->scene, entity);

        }
    }
    vec3_copy(bd->last_position, et->transform.pos);
}

void
sp_removeCallback(PhysicsComponent *component_a, PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b,
    enum ColliderType collider_a_type,
    enum ColliderType collider_b_type,
    enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id)
{
    if (type == COLLISION_ENTER)
    {
        Scene3D_removeEntity(entity_a->scene, entity_a);
    }
}

void
sp_itemCollisionCallback(PhysicsComponent *component_a,
    PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b,
    enum ColliderType collider_a_type,
    enum ColliderType collider_b_type,
    enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id)
{
    if (type == COLLISION_ENTER)
    {
        ScriptComponent *sc     = Entity_getScriptComponent(entity_b);
        PlayerScriptData *pd    = (PlayerScriptData*)sc->data;
        EntityTransform *et     = Entity_getTransform(entity_a);

        if (strcmp(Entity_getName(entity_a), "medkit") == 0)
        {
            pd->base.hp = CLAMP(pd->base.hp + 50, 0, 100);
        }

        vec3 random;
        for (int i = 0; i < 3; ++i)
        {
            random[0] = (float)(-5.0f + rand() % 10);
            random[1] = 10.0f;
            random[2] = (float)(-5.0f + rand() % 10);
            createEffect(entity_a, getDefaultPlaneModel(),
                GET_INGAME_MATERIAL("muzzlefire"), et->transform.pos, random, et->transform.rot,
                1.0f, 1.5f, 1, 1);
        }
        Scene3D_removeEntity(entity_a->scene, entity_a);
    }
}

void
sp_explosionCallback(PhysicsComponent *component_a,
    PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b, enum ColliderType collider_a_type,
    enum ColliderType collider_b_type, enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id)
{
    if (type == COLLISION_ENTER)
    {
        vec3 pos1;
        vec3_copy(pos1, Entity_getTransform(entity_a)->transform.pos);
        vec3 pos2;
        vec3_copy(pos2, Entity_getTransform(entity_b)->transform.pos);
        vec3 direction;
        vec3_sub(direction, pos2, pos1);
        PhysicsComponent_applyImpactForce(component_b, direction, 5.0f);

        if (strcmp(Entity_getName(entity_b), "enemy") == 0) /* Beautiful */
        {
            EntityTransform *et = Entity_getTransform(entity_b);

            vec3 random;
            for (int i = 0; i < 3; ++i)
            {
                random[0] = (float)(-5.0f + rand() % 10);
                random[1] = 10.0f;
                random[2] = (float)(-5.0f + rand() % 10);
                createEffect(entity_b, getDefaultPlaneModel(),
                    GET_INGAME_MATERIAL("muzzlefire"), et->transform.pos,
                    random, et->transform.rot, 1.0f, 1.5f, 1 , 1);
            }

            ScriptComponent *sc = Entity_getScriptComponent(entity_b);

            if (sc)
            {
                PlayerScriptData *data  = sc->data;
                data->base.hp -= 50;
                if (data->base.hp <= 0)
                    Scene3D_removeEntity(entity_b->scene, entity_b);
            }
        }
    }
}

Entity *
sp_createOtherPlayersPawn(Scene3D *scene, vec3 pos, vec3 vel, char char_type)
{
    Entity *e = Scene3D_createEntity(scene, pos[0], pos[1], pos[2]);
    if (!e) return 0;

    Entity_setName(e, "enemy");
    shared_initPlayerPawn(e);

    Transform *t = &Entity_getTransform(e)->transform;
    t->rot[1] = (float)(rand() % 7);

    ScriptComponent *sc     = Entity_attachScriptComponent(e);
    PlayerScriptData *sd    = ScriptComponent_allocateData(sc,
        sizeof(PlayerScriptData));
    shared_initBasePlayerScriptData(&sd->base);
    sd->character_type = char_type;
    sd->base.hp = 500;

    PhysicsComponent *pc = Entity_getPhysicsComponent(e);
    vec3_copy(pc->velocity, vel);

    SphereCollider *body = PhysicsComponent_getSphereColliderByHash(pc,
        PLAYER_COLLIDER_ID);
    if (body) body->id = 0;

    SphereCollider *feet = PhysicsComponent_getSphereColliderByHash(pc,
        PLAYER_FOOT_COLLIDER_ID);
    if (feet) feet->id = 0;

    ModelComponent *mc      = Entity_attachModelComponent(e);
    RenderModel *rm         = ModelComponent_getDefaultRenderModel(mc);
    RenderModel *rm_weapon  = ModelComponent_attachRenderModel(mc, scene, 2);

    rm_weapon->model    = GET_INGAME_MODEL("rocket launcher");
    rm_weapon->material = GET_INGAME_MATERIAL("rocket launcher");
    rm_weapon->visible  = 1;
    rm_weapon->shader   = CEL_SHADER_OPAQUE;

    Animator *an = ModelComponent_attachNamedAnimator(mc, scene, rm,
        "character");
    an->speed = 0.8f;

    rm->shader                      = CEL_SHADER_OPAQUE;
    rm->outline_normal_offset       = 0.05f;
    rm->relative_transform.pos[1]   = -11.5f;
    rm->relative_transform.rot[1]   = (float)DEG_TO_RAD(260.0f); /* 270? */
    vec3_set(rm->relative_transform.scale, 0.2f, 0.2f, 0.2f);

    mp_setOtherPawnWeaponRenderModelTransform(rm_weapon, ROCKET_LAUNCHER);
    vec3_set(rm_weapon->relative_transform.scale, 0.15f, 0.15f, 0.15f);

    /* Based on char_type here we could play a different animation! */

    /* Set the state over range first, so that the setOtherPawnMovementState()
     * defaults to "standing" */
    sd->movement_state      = NUM_PAWN_MOVEMENT_STATES + 1;
    sd->base.is_hit         = 0;
    sd->base.is_hit_timer   = 0.0f;

    RenderModel *rm_muzzle = ModelComponent_attachRenderModel(mc, e->scene,
        PLAYER_RENDER_MODEL_ID_MUZZLEFIRE);
    rm_muzzle->material = GET_INGAME_MATERIAL("muzzlefire");
    rm_muzzle->shader = SHADER3D_BILLBOARD;
    rm_muzzle->mode = RENDER_MODEL_BILLBOARD;

    rm_muzzle->relative_transform.pos[0]    = -1.4f;
    rm_muzzle->relative_transform.pos[1]    = -0.3f;
    rm_muzzle->relative_transform.pos[2]    = -3.25f;
    rm_muzzle->relative_transform.scale[0]  = 0.5f;
    rm_muzzle->relative_transform.scale[1]  = 0.5f;
    rm_muzzle->relative_transform.scale[2]  = 0.5f;

    ModelComponent_attachAnimator(mc, e->scene, rm_muzzle,
        PLAYER_ANIMATOR_ID_MUZZLEFIRE);
    mp_setOtherPawnMovementStateAndAnimation(e, DEFAULT_PAWN_MOVEMENT_STATE);

    return e;
}
