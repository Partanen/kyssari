#pragma once

#include "utils.h"
#include <SDL2/SDL_mixer.h> //local include needs SDL_sdlinc.h to work

#define AUDIO_OPEN_CHUNKSIZE 4096

enum AudioError
{
    AUDIO_ERROR_MIX_INIT = 1,
    AUDIO_ERROR_OPEN_AUDIO
};

enum AudioChannels
{
    AUDIO_CHANNEL_MONO = 1,
    AUDIO_CHANNEL_STEREO
};

typedef struct AudioSystem  AudioSystem;
typedef struct SoundEffect  SoundEffect;
typedef struct Music        Music;

int
AudioSystem_init(AudioSystem *audiosystem);

int
SoundEffect_play(SoundEffect *soundeffect, int channel, int loops);

void
SoundEffect_setVolume(SoundEffect *effect, char volume);

bool32
isSoundEffectChannelPlaying(int channel);

void
Music_play(Music *musicfile, int loops);

void
Music_pause();

void
Music_stop();

void
Music_resume();

struct Music
{
    Mix_Music *music;
};

struct SoundEffect
{
    Mix_Chunk *chunk;
};

struct AudioSystem
{
    int audiochannel;

    bool32 isInitialized;
};