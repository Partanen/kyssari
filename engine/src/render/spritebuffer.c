#include "spritemacro.h"

static inline void
SpriteBuffer_swapWithBatch(SpriteBuffer *buffer, SpriteBatch *batch)
{
    uint sprite_count           = batch->sprite_count;
    uint tex_swap_count         = batch->tex_swap_count;
    GLfloat *vertex_buffer      = batch->vertex_buffer;
    TextureBufferItem *tex_buf  = batch->texture_buffer;

    batch->sprite_count         = buffer->sprite_count;
    batch->tex_swap_count       = buffer->tex_swap_count;
    batch->vertex_buffer        = buffer->vertex_buffer;
    batch->texture_buffer       = buffer->texture_buffer;

    buffer->sprite_count        = sprite_count;
    buffer->tex_swap_count      = tex_swap_count;
    buffer->vertex_buffer       = vertex_buffer;
    buffer->texture_buffer      = tex_buf;
}

static inline void
SpriteBuffer_bufferSprite(SpriteBuffer *buffer,
    Texture *texture,
    float x, float y, unsigned int *clip)
{
    /* Do some sort of assertion here to check if more slots
     * exist? */

    unsigned int index = buffer->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION(buffer, x, y, clip);
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS(buffer, texture, clip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(buffer, texture, x, y);
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(buffer);
    }

    SET_SPRITE_DEFAULT_COLOR(buffer);
    SPRITE_DRAW_FUNC_LOWER_HALF(buffer, texture);
}

static inline void
SpriteBuffer_bufferSprite_Color(SpriteBuffer *buffer, Texture *texture,
    float x, float y, unsigned int *clip, float *color)
{
    /* Do some sort of assertion here to check if more slots
     * exist? */

    unsigned int index = buffer->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION(buffer, x, y, clip);
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS(buffer, texture, clip);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(buffer, texture, x, y);
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(buffer);
    }

    SET_SPRITE_MOD_COLOR(buffer, color);
    SPRITE_DRAW_FUNC_LOWER_HALF(buffer, texture);
}

static inline void
SpriteBuffer_bufferRect(SpriteBuffer *buffer,
    float x, float y, float w, float h,
    float *color)
{
    /* Do some sort of assertion here to check if more slots
     * exist? */

    unsigned int index = buffer->sprite_count * FLOATS_PER_SPRITE;

    buffer->vertex_buffer[index +  0] = (GLfloat)x;
    buffer->vertex_buffer[index +  1] = (GLfloat)y;
    buffer->vertex_buffer[index +  8] = (GLfloat)(x + w);
    buffer->vertex_buffer[index +  9] = (GLfloat)y;
    buffer->vertex_buffer[index + 16] = (GLfloat)x;
    buffer->vertex_buffer[index + 17] = (GLfloat)(y + h);
    buffer->vertex_buffer[index + 24] = (GLfloat)(x + w);
    buffer->vertex_buffer[index + 25] = (GLfloat)(y + h);

    SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(buffer);

    if (color)
    {
        SET_SPRITE_MOD_COLOR(buffer, color);
    }
    else
    {
        SET_SPRITE_DEFAULT_COLOR(buffer);
    }

    SPRITE_DRAW_FUNC_LOWER_HALF(buffer, _BLANK_WHITE_TEXTURE);
}
