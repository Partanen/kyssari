#pragma once
#include <stdint.h>
#include <SDL2/SDL_ttf.h>

#define _USE_MATH_DEFINES

#ifdef _WANGBLOWS
    #pragma warning(disable : 4244)
    #pragma warning(disable : 4005)
    #include "../lib/linmath/linmath.h"
    #pragma warning(default : 4244)
    #pragma warning(default : 4005)
#else
    #include "../lib/linmath/linmath.h"
#endif

typedef unsigned int        uint;
typedef long unsigned int   long_uint;
typedef int                 bool32;
typedef char                bool8;
typedef char                byte;
typedef unsigned char       ubyte;
typedef unsigned short      ushort;
typedef TTF_Font            Font;
typedef SDL_Color           Color;

typedef struct String       String;
typedef struct Transform    Transform;

static inline void
Transform_getForwardVector(Transform *transform, vec3 ret);

static inline void
Transform_getNonNormalizedForwardVector(Transform *transform, vec3 ret);

static inline void
Transform_getRightVector(Transform *transform, vec3 ret);

static inline void
Transform_getUpVector(Transform *transform, vec3 ret);

static inline void
Transform_getModelMatrix(Transform transform, mat4x4 ret);

struct Transform
{
    vec3    pos;
    vec3    rot;
    vec3    scale;
};

struct String
{
    char    *c_str;
    uint    len;
};

static inline void
Transform_getForwardVector(Transform *transform, vec3 ret)
{
    ret[0] = -(cosf(transform->rot[1]) * cosf(transform->rot[2]));
    ret[1] = -(sinf(transform->rot[2]));
    ret[2] = sinf(transform->rot[1]) * cosf(transform->rot[2]);

    vec3_norm(ret, ret);
}

static inline void
Transform_getNonNormalizedForwardVector(Transform *transform, vec3 ret)
{
    ret[0] = -(cosf(transform->rot[1]) * cosf(transform->rot[2]));
    ret[1] = -(sinf(transform->rot[2]));
    ret[2] = sinf(transform->rot[1]) * cosf(transform->rot[2]);
}

static inline void
Transform_getRightVector(Transform *transform, vec3 ret)
{
    
}

static inline void
Transform_getUpVector(Transform *transform, vec3 ret)
{
}

static inline void
Transform_getModelMatrix(Transform transform, mat4x4 ret)
{
    mat4x4 mat_tmp;
    mat4x4_identity(ret);
    mat4x4_translate_in_place(ret, transform.pos[0],
        transform.pos[1], transform.pos[2]);
    mat4x4_identity(mat_tmp);
    mat4x4_rotate_X(mat_tmp, mat_tmp, transform.rot[0]);
    mat4x4_rotate_Y(mat_tmp, mat_tmp, transform.rot[1]);
    mat4x4_rotate_Z(mat_tmp, mat_tmp, transform.rot[2]);
    mat4x4_mul(ret, ret, mat_tmp);
    mat4x4_scale_aniso(ret, ret, transform.scale[0],
        transform.scale[1], transform.scale[2]);
}
