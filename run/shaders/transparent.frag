/* A shader that discards fragments where alpha is below a given treshold */

#version 330 core

#define NUM_MAX_DIRECTIONAL_LIGHTS 2
#define NUM_MAX_POINT_LIGHTS 4

struct Material
{
    sampler2D   diffuse;
    sampler2D   specular;
    float       shine;
    vec3        color;
};

layout (std140) uniform Shader3DSharedMatrices
{
    mat4 projection;
    mat4 look_at;
    vec3 camera_pos;
};

layout (std140) uniform Shader3DDirectionalLights
{
    vec3 dir_ambient[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_direction[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_diffuse[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_specular[NUM_MAX_DIRECTIONAL_LIGHTS];
    int num_directional_lights;
};

layout (std140) uniform Shader3DPointLights
{
    vec3 pnt_position[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_diffuse[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_specular[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_ambient[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_constant_linear_quadratic[NUM_MAX_POINT_LIGHTS];
    int num_point_lights;
};

layout (std140) uniform Shader3DSceneAmbientLighting
{
    vec4 scene_ambient_light;
};

uniform Material    material;

in vec3             frag_pos;
in vec2             tex_coord;
in vec3             normal;
out vec4            frag_color;

vec3
calculateDirectionalLight(vec3 d_ambient, vec3 d_direction,
    vec3 d_diffuse, vec3 d_specular, vec3 view_direction)
{
    /* Diffuse lighting */
    float diffuse_strength = max(dot(normal, normalize(-d_direction)), 0.0f);
    vec3 diffuse = diffuse_strength * d_diffuse * (material.color + vec3(texture(material.diffuse, tex_coord)));

    /* Specular lighting */
    vec3 reflect_dir        = reflect(-d_direction, normal);
    float specular_strength = clamp(pow(max(dot(view_direction, reflect_dir), 0.0f), material.shine), 0, 1);
    vec3 specular = d_specular * specular_strength * vec3(texture(material.specular, tex_coord));

    /* Ambient lighing */
    vec3 ambient = d_ambient * vec3(texture(material.diffuse, tex_coord));

    return diffuse + specular + ambient;
}

vec3
calculatePointLight(vec3 p_position, vec3 p_diffuse, vec3 p_specular,
    vec3 p_ambient, vec3 p_constant_linear_quadratic, vec3 view_direction)
{
    vec3 light_direction    = normalize(p_position - frag_pos);
    vec3 reflect_direction  = reflect(-light_direction, normal);
    float distance          = length(p_position - frag_pos);
    float attenuation = clamp(1.0f / (p_constant_linear_quadratic.x +
                                p_constant_linear_quadratic.y * distance +
                                p_constant_linear_quadratic.z * distance * distance), 0.0f, 1.0f);

    /* Diffuse lighting */
    float diffuse_strength = max(dot(normal, -light_direction), 0.0f);
    vec3 diffuse = diffuse_strength * p_diffuse * (material.color + vec3(texture(material.diffuse, tex_coord)));
    
    /* Specular lighting */
    float specular_strength = clamp(pow(max(dot(view_direction, reflect_direction), 0.0f), material.shine), 0.0f, 1.0f);
    vec3 specular = p_specular * specular_strength * (material.color + vec3(texture(material.specular, tex_coord)));

    /* Ambient lighting */
    vec3 ambient = p_ambient * vec3(texture(material.diffuse, tex_coord));

    return attenuation * (diffuse + specular + ambient);
}

void main(void)
{
    if (texture(material.diffuse,  tex_coord).a <= 0.1f
    &&  texture(material.specular, tex_coord).a <= 0.1f)
        discard;

    vec3 combined_color     = vec3(0.0f, 0.0f, 0.0f);
    vec3 view_direction     = normalize(camera_pos - frag_pos);

    /* Directional lights */
#if 0
    for (int i = 0; i < NUM_MAX_DIRECTIONAL_LIGHTS; ++i)
        combined_color += calculateDirectionalLight(directional_lights[i], view_direction);

    for (int i = 0; i < num_point_lights; ++i)
        combined_color += calculatePointLight(point_lights[i], view_direction);
#endif

    vec3 scene_ambient = vec3(scene_ambient_light.x,
        scene_ambient_light.y,
        scene_ambient_light.z) * scene_ambient_light.w;

    frag_color = vec4(scene_ambient + combined_color, 1.0f);
}
