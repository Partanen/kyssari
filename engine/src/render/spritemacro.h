/* Separated into it's own file because for example the UI's sprite buffer also uses them */

#define SPRITE_DRAW_FUNC_LOWER_HALF(batch, texture)\
    if (batch->sprite_count > 0)\
    {\
        if (batch->texture_buffer[batch->tex_swap_count].id == texture->id)\
            ++batch->texture_buffer[batch->tex_swap_count].repeats;\
        else\
        {\
            ++batch->tex_swap_count;\
            batch->texture_buffer[batch->tex_swap_count].id = texture->id;\
            batch->texture_buffer[batch->tex_swap_count].repeats = 0;\
        }\
    }\
    else\
    {\
        batch->texture_buffer[0].id = texture->id;\
        batch->texture_buffer[0].repeats = 0;\
    }\
    ++batch->sprite_count;

#define SET_CLIPPED_SPRITE_DEFAULT_POSITION(batch, x, y, clip)\
    batch->vertex_buffer[index +  0] = (GLfloat)x;\
    batch->vertex_buffer[index +  1] = (GLfloat)y;\
    batch->vertex_buffer[index +  8] = (GLfloat)(x + clip[2]);\
    batch->vertex_buffer[index +  9] = (GLfloat)y;\
    batch->vertex_buffer[index + 16] = (GLfloat)x;\
    batch->vertex_buffer[index + 17] = (GLfloat)(y + clip[3]);\
    batch->vertex_buffer[index + 24] = (GLfloat)(x + clip[2]);\
    batch->vertex_buffer[index + 25] = (GLfloat)(y + clip[3]);

#define SET_UNCLIPPED_SPRITE_DEFAULT_POSITION(batch, texture, x, y)\
    batch->vertex_buffer[index +  0] = (GLfloat)x;\
    batch->vertex_buffer[index +  1] = (GLfloat)y;\
    batch->vertex_buffer[index +  8] = (GLfloat)(x + texture->width);\
    batch->vertex_buffer[index +  9] = (GLfloat)y;\
    batch->vertex_buffer[index + 16] = (GLfloat)x;\
    batch->vertex_buffer[index + 17] = (GLfloat)(y + texture->height);\
    batch->vertex_buffer[index + 24] = (GLfloat)(x + texture->width);\
    batch->vertex_buffer[index + 25] = (GLfloat)(y + texture->height);

#define SET_UNCLIPPED_SPRITE_SCALED_POSITION(batch, texture, x, y, scale_x, scale_y)\
    batch->vertex_buffer[index +  0] = (GLfloat)x;\
    batch->vertex_buffer[index +  1] = (GLfloat)y;\
    batch->vertex_buffer[index +  8] = (GLfloat)(x + texture->width * scale_x);\
    batch->vertex_buffer[index +  9] = (GLfloat)y;\
    batch->vertex_buffer[index + 16] = (GLfloat)x;\
    batch->vertex_buffer[index + 17] = (GLfloat)(y + texture->height * scale_y);\
    batch->vertex_buffer[index + 24] = (GLfloat)(x + texture->width * scale_x);\
    batch->vertex_buffer[index + 25] = (GLfloat)(y + texture->height * scale_y);

#define SET_CLIPPED_SPRITE_SCALED_POSITION(batch, x, y, clip, scale_x, scale_y)\
    batch->vertex_buffer[index +  0] = (GLfloat)x;\
    batch->vertex_buffer[index +  1] = (GLfloat)y;\
    batch->vertex_buffer[index +  8] = (GLfloat)(x + clip[2] * scale_x);\
    batch->vertex_buffer[index +  9] = (GLfloat)y;\
    batch->vertex_buffer[index + 16] = (GLfloat)x;\
    batch->vertex_buffer[index + 17] = (GLfloat)(y + clip[3] * scale_y);\
    batch->vertex_buffer[index + 24] = (GLfloat)(x + clip[2] * scale_x);\
    batch->vertex_buffer[index + 25] = (GLfloat)(y + clip[3] * scale_y);

#define SET_CLIPPED_SPRITE_DEFAULT_CLIPS(batch, texture, clip)\
    batch->vertex_buffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
    batch->vertex_buffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
    batch->vertex_buffer[index + 10] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
    batch->vertex_buffer[index + 11] = (GLfloat)clip[1] / (GLfloat)texture->height;\
    batch->vertex_buffer[index + 18] = (GLfloat)clip[0] / (GLfloat)texture->width;\
    batch->vertex_buffer[index + 19] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
    batch->vertex_buffer[index + 26] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
    batch->vertex_buffer[index + 27] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\

#define SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS(batch)\
    batch->vertex_buffer[index +  2] = 0.0f;\
    batch->vertex_buffer[index +  3] = 0.0f;\
    batch->vertex_buffer[index + 10] = 1.0f;\
    batch->vertex_buffer[index + 11] = 0.0f;\
    batch->vertex_buffer[index + 18] = 0.0f;\
    batch->vertex_buffer[index + 19] = 1.0f;\
    batch->vertex_buffer[index + 26] = 1.0f;\
    batch->vertex_buffer[index + 27] = 1.0f;\

#define SET_CLIPPED_SPRITE_FLIPPED_CLIPS(batch, texture, clip, flip)\
    switch (flip)\
    {\
        case SPRITE_FLIP_NONE:\
        {\
            batch->vertex_buffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 10] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 11] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 18] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 19] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 26] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 27] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_HORIZONTAL:\
        {\
            batch->vertex_buffer[index +  2] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 10] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 11] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 18] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 19] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 26] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 27] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_VERTICAL:\
        {\
            batch->vertex_buffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  3] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 10] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 11] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 18] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 19] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 26] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 27] = (GLfloat)clip[1] / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_BOTH:\
        {\
            batch->vertex_buffer[index +  2] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  3] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 10] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 11] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 18] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 19] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 26] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 27] = (GLfloat)clip[1] / (GLfloat)texture->height;\
        }\
            break;\
    }

#define SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS(batch, flip)\
        switch (flip)\
        {\
            case SPRITE_FLIP_NONE:\
            {\
                batch->vertex_buffer[index +  2] = 0.0f;\
                batch->vertex_buffer[index +  3] = 0.0f;\
                batch->vertex_buffer[index + 10] = 1.0f;\
                batch->vertex_buffer[index + 11] = 0.0f;\
                batch->vertex_buffer[index + 18] = 0.0f;\
                batch->vertex_buffer[index + 19] = 1.0f;\
                batch->vertex_buffer[index + 26] = 1.0f;\
                batch->vertex_buffer[index + 27] = 1.0f;\
            }\
                break;\
            case SPRITE_FLIP_HORIZONTAL:\
            {\
                batch->vertex_buffer[index +  2] = 1.0f;\
                batch->vertex_buffer[index +  3] = 0.0f;\
                batch->vertex_buffer[index + 10] = 0.0f;\
                batch->vertex_buffer[index + 11] = 0.0f;\
                batch->vertex_buffer[index + 18] = 1.0f;\
                batch->vertex_buffer[index + 19] = 1.0f;\
                batch->vertex_buffer[index + 26] = 0.0f;\
                batch->vertex_buffer[index + 27] = 1.0f;\
            }\
                break;\
            case SPRITE_FLIP_VERTICAL:\
            {\
                batch->vertex_buffer[index +  2] = 0.0f;\
                batch->vertex_buffer[index +  3] = 1.0f;\
                batch->vertex_buffer[index + 10] = 1.0f;\
                batch->vertex_buffer[index + 11] = 1.0f;\
                batch->vertex_buffer[index + 18] = 0.0f;\
                batch->vertex_buffer[index + 19] = 0.0f;\
                batch->vertex_buffer[index + 26] = 1.0f;\
                batch->vertex_buffer[index + 27] = 0.0f;\
            }\
                break;\
            case SPRITE_FLIP_BOTH:\
            {\
                batch->vertex_buffer[index +  2] = 1.0f;\
                batch->vertex_buffer[index +  3] = 1.0f;\
                batch->vertex_buffer[index + 10] = 0.0f;\
                batch->vertex_buffer[index + 11] = 1.0f;\
                batch->vertex_buffer[index + 18] = 1.0f;\
                batch->vertex_buffer[index + 19] = 0.0f;\
                batch->vertex_buffer[index + 26] = 0.0f;\
                batch->vertex_buffer[index + 27] = 0.0f;\
            }\
                break;\
        }

#define ROTATE_SPRITE(batch, origin_x, origin_y)\
    float trans_x, trans_y, rot_x, rot_y;\
    \
    for (int i = 0; i < 4; ++i)\
    {\
        trans_x = batch->vertex_buffer[index + (i * 8) + 0] - origin_x;\
        trans_y = batch->vertex_buffer[index + (i * 8) + 1] - origin_y;\
        \
        rot_x = trans_x * (float)cos(angle) - trans_y * (float)sin(angle);\
        rot_y = trans_x * (float)sin(angle) + trans_y * (float)cos(angle);\
        \
        batch->vertex_buffer[index + (i * 8) + 0] = rot_x + origin_x;\
        batch->vertex_buffer[index + (i * 8) + 1] = rot_y + origin_y;\
    }

#define SET_SPRITE_DEFAULT_COLOR(batch)\
    batch->vertex_buffer[index + 4] = 1.0f;\
    batch->vertex_buffer[index + 5] = 1.0f;\
    batch->vertex_buffer[index + 6] = 1.0f;\
    batch->vertex_buffer[index + 7] = 1.0f;\
    batch->vertex_buffer[index + 12] = 1.0f;\
    batch->vertex_buffer[index + 13] = 1.0f;\
    batch->vertex_buffer[index + 14] = 1.0f;\
    batch->vertex_buffer[index + 15] = 1.0f;\
    batch->vertex_buffer[index + 20] = 1.0f;\
    batch->vertex_buffer[index + 21] = 1.0f;\
    batch->vertex_buffer[index + 22] = 1.0f;\
    batch->vertex_buffer[index + 23] = 1.0f;\
    batch->vertex_buffer[index + 28] = 1.0f;\
    batch->vertex_buffer[index + 29] = 1.0f;\
    batch->vertex_buffer[index + 30] = 1.0f;\
    batch->vertex_buffer[index + 31] = 1.0f;

#define SET_SPRITE_MOD_COLOR(batch, color)\
    batch->vertex_buffer[index + 4] = color[0];\
    batch->vertex_buffer[index + 5] = color[1];\
    batch->vertex_buffer[index + 6] = color[2];\
    batch->vertex_buffer[index + 7] = color[3];\
    batch->vertex_buffer[index + 12] = color[0];\
    batch->vertex_buffer[index + 13] = color[1];\
    batch->vertex_buffer[index + 14] = color[2];\
    batch->vertex_buffer[index + 15] = color[3];\
    batch->vertex_buffer[index + 20] = color[0];\
    batch->vertex_buffer[index + 21] = color[1];\
    batch->vertex_buffer[index + 22] = color[2];\
    batch->vertex_buffer[index + 23] = color[3];\
    batch->vertex_buffer[index + 28] = color[0];\
    batch->vertex_buffer[index + 29] = color[1];\
    batch->vertex_buffer[index + 30] = color[2];\
    batch->vertex_buffer[index + 31] = color[3];
