typedef struct MPGameScreenData MPGameScreenData;

#define FRAG_TEXT_DUR       2.0f
#define POWERUP_TEXT_DUR    1.7f
#define SERVED_TEXT_DUR     1.7f

struct MPGameScreenData
{
    Camera  camera;
    bool32  is_menu_visible;
    bool32  is_stat_win_visible;
    bool32  show_network_text;

    double  frag_text_timer;
    double  powerup_text_timer;
    double  served_text_timer;
};

int
createMPGameScreen();

void
MPGameScreen_update(Screen *screen);

void
MPGameScreen_open(Screen *screen);

void
MPGameScreen_close(Screen *screen);

void
MPGameScreen_playFragText();

void
MPGameScreen_playPowerupText();

void
MPGameScreen_playServedText();

int
mp_initGameSceneForLevel1();

void
mp_updateAndBufferPlayerInputs(Entity *pawn, double dt);

void
mp_replayInputsFromLatestVerified(Entity *pawn);

void
mp_playerWeaponSwapNext(Entity *pawn);

void
mp_playerWeaponSwapPrev(Entity *pawn);

void
mp_reconcileInputs(Entity *pawn, input_sequence_t latest_verified_sequence,
    vec3 latest_verified_position, vec3 latest_verified_velocity);

void
mp_reconcileInputs2(Entity *pawn, input_sequence_t latest_verified_sequence,
    vec3 latest_verified_position, vec3 latest_verified_velocity);

void
mp_shootAsPlayerPawn(Entity *pawn);

void
mp_shootAsOtherPawnRocketLauncher(Entity *pawn, vec3 position, vec3 direction);

void
mp_shootAsOtherPawnSMG(Entity *pawn, vec3 position, vec3 direction);

void
mp_shootAsOtherPawnRailgun(Entity *pawn, vec3 position, vec3 direction);

void
mp_createRocketExplosion(vec3 pos);

void
mp_rocketExplosionScript(Entity *entity, void *scriptdata);

Entity *
mp_createOtherPlayersPawn(Scene3D *scene, replication_id32_t rid,
    vec3 pos, vec3 vel, char char_type);

Entity *
mp_createItemEntity(Scene3D *scene, enum KsItemType type,
    float x, float y, float z);
