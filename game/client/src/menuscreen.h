#pragma once

#define WINDOW_HEIGHT 768
#define WINDOW_WIDTH 1024

/*forwards declaration*/
typedef struct Game Game;

typedef struct MenuScreenData   MenuScreenData;

struct MenuScreenData
{
    Scene3D         scene3d;
    Camera          camera;
    Entity          *light_entity;
    Texture         *tex_test;
	int				credits_toggle;
    GUIButtonStyle  menubutton_style;
    char *          menu_text_file;
};

int
createMenuScreen();

void
MenuScreen_update(Screen *screen);

void
MenuScreen_open(Screen *screen);

void
MenuScreen_drawUI(MenuScreenData *screen);

int
initGameSceneForSinglePlayer();

int
initGameSceneForMultiPlayer();
