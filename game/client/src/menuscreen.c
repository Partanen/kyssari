#include "menuscreen.h"

int
createMenuScreen()
{
    game.screens[MENU_SCREEN] = createScreen(MenuScreen_update, &game.menu_screen_data);
    if (!game.screens[MENU_SCREEN])
        return 1;

    game.screens[MENU_SCREEN]->onOpen = MenuScreen_open;

    MenuScreenData *data = (MenuScreenData *)game.screens[MENU_SCREEN]->screen_data;

    setRelativeMouseMode(0);
    Camera_init(&data->camera);
    Camera_turn(&data->camera, -1.0f, 0.7f, 0.0f);
    setRenderBackgroundColor(1.0f, 1.0f, 0.0f, 1.0f);

    /*SCENE INIT */
    void *scene_memory = allocatePermanentMemory(MEGABYTES(MENU_SCENE_MEMORY_SIZE));

    if (!scene_memory)
    {
        DEBUG_PRINTF("Could not allocate enough memory for scene.\n");
        return 1;
    }

    if (Scene3D_init(&data->scene3d, scene_memory,
        MEGABYTES(MENU_SCENE_MEMORY_SIZE), 4, 1) != 0)
    {
        DEBUG_PRINTF("Scene_init() failure.\n");
        return 2;
    }

    /* Menu button style */
    data->menubutton_style.title_scale = 2.0f;
    data->menubutton_style.title_font = getDefaultSpriteFont();//permanent_assets.sfont_roboto;
    setColorf(data->menubutton_style.color_background_normal,
        0.05f, 0.05f, 0.05f, 1.00f);
    setColorf(data->menubutton_style.color_background_hovered,
        0.05f, 0.05f, 0.05f, 1.00f);
    setColorf(data->menubutton_style.color_background_pressed,
        0.05f, 0.05f, 0.05f, 1.00f);
    setColorf(data->menubutton_style.color_border_normal,
        0.635f, 0.287f, 0.067f, 1.00f);
    setColorf(data->menubutton_style.color_border_hovered,
        1.00f, 0.85f, 0.80f, 1.00f);
    setColorf(data->menubutton_style.color_border_pressed,
        0.952f, 0.357f, 0.0f, 1.00f);
    data->menubutton_style.border_width_top     = 2;
    data->menubutton_style.border_width_bottom  = 2;
    data->menubutton_style.border_width_left    = 2;
    data->menubutton_style.border_width_right   = 2;
    data->menubutton_style.show_title           = 1;
    data->menubutton_style.title_position[0]    = 50;
    data->menubutton_style.title_position[1]    = 20;
    gui->button_style = &data->menubutton_style;

    data->credits_toggle = 0;

    Scene3D_setAmbientColor(&data->scene3d, 1.0f, 1.0f, 1.0f, 0.4f);
    Scene3D_setBackgroundColor(&data->scene3d, 0.2f, 0.2f, 0.6f, 1.0f);
    Scene3D_setSkyboxTexture(&data->scene3d,
        permanent_assets.tex_skybox_level1);

    data->light_entity = createDirectionalLightEntity(&data->scene3d, 1.0f,
        2.0f, 0.0f, 5.0f, -3.0f, 2.5f);

    return 0;
}

void
MenuScreen_update(Screen *screen)
{
    if (client.status == CLIENT_STATUS_CONNECTED)
    {
        game.screen_after_load = MP_GAME_SCREEN;
        setScreen(game.screens[LOADING_SCREEN]);
    }

    MenuScreenData *data = (MenuScreenData*)screen->screen_data;
    Scene3D_updateSimulation(&data->scene3d, getDelta());

    /* Render */
    data->camera.fov = 70.0f;
    renderScene3D(&data->scene3d, &data->camera, 0);
    Camera_turn(&data->camera, -2.0f, 0.0f, 0.1f * (float)getDelta());
    MenuScreen_drawUI(data);

    if (isKeyPressed(SDL_SCANCODE_ESCAPE))
        kys_beginShutdown();
}

void
MenuScreen_open(Screen *screen)
{
    setRelativeMouseMode(0);
    if (game.menu_screen_data.menu_text_file)
        kfree(game.menu_screen_data.menu_text_file);
    game.menu_screen_data.menu_text_file = readTextFileToBuffer("menu.txt");
}

void
MenuScreen_drawUI(MenuScreenData *screen)
{
    /* MENUBUTTONS */
    GUI_beginUpdate();

    GUI_setNextOrigin(GUI_ORIGIN_TOP_LEFT);
    const int menu_win_h = 540;
    GUI_beginWindow("menu win", 50, getWindowHeight() / 2 - menu_win_h / 2,
        530, menu_win_h);
    GUI_text_Scale(game.menu_screen_data.menu_text_file, 4, 4, 2.f);
    GUI_endWindow();

    #define pa permanent_assets

    static const int menu_w = 450;
    static const int menu_h = 360;

    GUI_beginEmptyWindow("menu", getWindowWidth() / 2 - menu_w / 2,
        getWindowHeight() - menu_h - 90, menu_w, menu_h);

    const float bscale = 0.75f;

    GUI_setNextNormalButtonTextureParams(pa.tex_button_sheet2,
        pa.clip_demo_n, 0, 0, bscale, bscale);

    GUI_setNextHoveredButtonTextureParams(pa.tex_button_sheet2,
        pa.clip_demo_h, 0, 0, bscale, bscale);

    GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_LEFT);

    if (GUI_button("##demo",
        menu_w / 2 - (int)((float)pa.clip_demo_n[2] * bscale) / 2,
        305,
        (int)((float)pa.clip_demo_n[2] * bscale),
        (int)((float)pa.clip_demo_n[3] * bscale)) && !IS_CLIENT_ACTIVE())
    {
        setRelativeMouseMode(1);
        game.screen_after_load = SP_GAME_SCREEN;
        setScreen(game.screens[LOADING_SCREEN]);
    }

    GUI_setNextNormalButtonTextureParams(permanent_assets.tex_button_sheet2,
        pa.clip_multiplayer_n, 0, 0, bscale, bscale);

    GUI_setNextHoveredButtonTextureParams(pa.tex_button_sheet2,
        pa.clip_multiplayer_h, 0, 0, bscale, bscale);

    GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_LEFT);

    if (GUI_button("##multiplayer",
        menu_w / 2 - (int)((float)pa.clip_multiplayer_n[2] * bscale) / 2,
        205,
        (int)((float)pa.clip_multiplayer_n[2] * bscale),
        (int)((float)pa.clip_multiplayer_n[3] * bscale)))
    {
        if (game.loaded_map_id != KS_MAP_1)
            mp_initGameSceneForLevel1();
        connectToServer(&game.game_screen_data);
    }

    GUI_setNextNormalButtonTextureParams(permanent_assets.tex_button_sheet2,
        pa.clip_quit_n, 0, 0, bscale, bscale);

    GUI_setNextHoveredButtonTextureParams(pa.tex_button_sheet2,
        pa.clip_quit_h, 0, 0, bscale, bscale);

    GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_LEFT);

    if (GUI_button("##quit",
        menu_w / 2 - (int)((float)pa.clip_quit_n[2] * bscale) / 2,
        95,
        (int)((float)pa.clip_quit_n[2] * bscale),
        (int)((float)pa.clip_quit_n[3] * bscale)))
    {
        kys_beginShutdown();
    }
    GUI_endWindow();
#       undef pa
    /*
    int button_y = 256;
    const int button_w = 256;
    #define BUTTON_Y() (getWindowHeight() - 200 - (button_y -= 76))

    if (GUI_button("     START DEMO",
        (getWindowWidth() / 2) - button_w / 2,
        BUTTON_Y(), button_w, 64)
    &&  !IS_CLIENT_ACTIVE())
    {
        setRelativeMouseMode(1);
        setScreen(game.screens[SP_GAME_SCREEN]);
    }

    if (GUI_button(" MULTIPLAYER GAME",
        (getWindowWidth() / 2) - button_w / 2,
        BUTTON_Y(), 256, 64))
    {
        if (game.loaded_map_id != KS_MAP_1)
            mp_initGameSceneForLevel1();

        connectToServer(&game.game_screen_data);
    }

	if (GUI_button("        CREDITS",
        (getWindowWidth() / 2) - button_w / 2,
        BUTTON_Y(), button_w, 64))
	{
		if (screen->credits_toggle)
			screen->credits_toggle = 0;
		else
			screen->credits_toggle = 1;
	}

    if (GUI_button("          QUIT",
        (getWindowWidth() / 2) - button_w / 2,
        BUTTON_Y(), button_w, 64))
    {
        kys_beginShutdown();
    }

    #undef BUTTON_Y
    */
    /* FPS COUNTER */
    char buf[128];
    sprintf(buf, "%f", engine.clock.fps);
    GUI_text(buf, 0, -16);

    sprintf(buf, "Karistys v. %s", KS_VERSION_STR);
    GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_LEFT);
    GUI_textLine(buf, 1, 13);

	GUI_texture(permanent_assets.tex_karistys_title,
        (getWindowWidth() / 2) - permanent_assets.tex_karistys_title->width / 2,
        16);

    /* UPDATES WINDOW */
    if (screen->credits_toggle)
    {
        GUI_beginWindow("", getWindowWidth() - 310,
            getWindowHeight() - 410, 300, 400);
        GUI_textLine("Credits:", 4, 20);
        GUI_textLine("Created by the Kyssari team (2017):"
         "",
         4, 32);
        GUI_endWindow();
    }

    if (client.status != CLIENT_STATUS_DISCONNECTED)
    {
        const int con_win_d[2] = {256, 128};
        GUI_beginWindow("connection",
            getWindowWidth() / 2 - con_win_d[0] / 2,
            getWindowHeight() / 2 - con_win_d[1] / 2,
            con_win_d[0], con_win_d[1]);


        static const char *str_connecting   = "connecting...";
        static const char *str_disconnected = "disconnected";
        static const char *str_connected    = "connected";

        const char *str;

        switch (client.status)
        {
            case CLIENT_STATUS_DISCONNECTED: str = str_disconnected; break;
            case CLIENT_STATUS_CONNECTING:   str = str_connecting;   break;
            case CLIENT_STATUS_CONNECTED:    str = str_connected;    break;
        }

        GUI_text_Scale(str, 79, 52, 2.0f);

        GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_LEFT);

        Address *saddr = &client.connection.address;

        sprintf(buf, "%i.%i.%i.%i",
            Address_getX(saddr), Address_getY(saddr),
            Address_getZ(saddr), Address_getW(saddr));
        GUI_textLine(buf, 4, 13);

        GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_RIGHT);
        GUI_textLine(KYS_SERVER_PORT_STR, 4, 13);

        GUI_endWindow();
    }

    GUI_setNextOrigin(GUI_ORIGIN_BOTTOM_RIGHT);
    GUI_textLine(uneducational_str, 0, 13);

    GUI_endUpdate();
}
