#include "renderer.h"
#include "spritebatch.c"
#include "spritebuffer.c"
#include "../core/engine.h"
#include "2d.h"
#include "../core/scene3d.h"

#include "camera.c"
#include "spritefont.c"

#if _CF_ALLOW_IMMEDIATE_RENDER
    #include "immediate_render.c"
#endif

#define SHARED_MAT_BUF_SIZE (3 * sizeof(mat4x4))
#define DIR_LIGHT_BUF_SIZE \
    (4 * sizeof(vec4) * MAX_ENTITY_DIRECTIONAL_LIGHTS + sizeof(vec4))
#define POINT_LIGHT_BUF_SIZE \
    (5 * sizeof(vec4) * MAX_ENTITY_POINT_LIGHTS + sizeof(vec4))
#define AMB_LIGHT_BUF_SIZE (sizeof(vec4))

static inline void
printGLErrors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        DEBUG_PRINTF("GL errors found! Printing them out...\n");
        DEBUG_PRINTF("%i\n", error);

        for (error = glGetError(); error != GL_NO_ERROR; error = glGetError())
        {
            DEBUG_PRINTF("%i\n", error);
        }

        DEBUG_PRINTF("Printed all errors!\n");
    }
}

static inline void
Renderer_setGLViewportAndScissor(Renderer *renderer, GLint x, GLint y, GLint w, GLint h)
{
    glViewport(x, renderer->window->height - h - y, w, h);
    glScissor (x, renderer->window->height - h - y, w, h);
}

static inline void
Renderer_flushIfNecessary(Renderer *renderer)
{
    if (renderer->sprite_batch.sprite_count > 0)
    {
        Renderer_setGLViewportAndScissor(renderer,
            renderer->default_viewport[0],
            renderer->default_viewport[1],
            renderer->default_viewport[2],
            renderer->default_viewport[3]);
        SpriteBatch_flush(&renderer->sprite_batch, 0);
    }
}

static inline void
Renderer_bindShader3D(Renderer *renderer, int shader)
{
    glUseProgram(renderer->shader3ds[shader].id);
}

static inline void
bindMaterial(Material *material, Shader3D *shader)
{
    if (material)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, material->diffuse);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, material->specular);
        glUniform1f(shader->loc_material_shine, material->shine);

        if (!material->diffuse && !material->specular
        && shader->loc_material_color >= 0)
        {
            glUniform3f(shader->loc_material_color, material->color[0],
                material->color[1], material->color[2]);
        }
        else
        {
            glUniform3f(shader->loc_material_color, 0.0f, 0.0f, 0.0f);
        }
    }
    else
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1f(shader->loc_material_shine, 0.0f);
        glUniform3f(shader->loc_material_color, 0.5f, 0.5f, 0.5f);
    }
}

int
Shader3D_init(Shader3D *s,
    Renderer *renderer,
    enum Shader3DType type,
    const char *vtx_code,
    const char *frg_code)
{
    char error_log[1024];

    s->id = glCreateProgram();

    GLuint vtx_shader = glCreateShader(GL_VERTEX_SHADER);
    GLuint frg_shader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vtx_shader, 1, &vtx_code, 0);
    glShaderSource(frg_shader, 1, &frg_code, 0);
    glCompileShader(vtx_shader);
    glCompileShader(frg_shader);

    GLint success;

    glGetShaderiv(vtx_shader, GL_COMPILE_STATUS, &success);

    if (success == GL_FALSE)
    {
        glGetShaderInfoLog(vtx_shader, 1024, 0, error_log);
        DEBUG_PRINTF("Failed to compile vertex shader. Error log:\n%s\n",
            error_log);
        glDeleteShader(vtx_shader);
        glDeleteShader(frg_shader);
        return 1;
    }

    glGetShaderiv(frg_shader, GL_COMPILE_STATUS, &success);

    if (success == GL_FALSE)
    {
        glGetShaderInfoLog(frg_shader, 1024, 0, error_log);
        DEBUG_PRINTF("Failed to compile fragment shader. Error log:\n%s\n",
            error_log);
        glDeleteShader(vtx_shader);
        glDeleteShader(frg_shader);
        return 2;
    }

    GLuint program = glCreateProgram();
    glAttachShader(program, vtx_shader);
    glAttachShader(program, frg_shader);
    glLinkProgram(program);
    glDeleteShader(vtx_shader);
    glDeleteShader(frg_shader);

    glGetProgramiv(program, GL_LINK_STATUS, &success);

    if (success == GL_FALSE)
    {
        glGetProgramInfoLog(program, 1024, 0, error_log);
        DEBUG_PRINTF("Failed to link shader program. Error log:\n%s\n",
            error_log);
        glDeleteProgram(program);
        return 3;
    }

    s->id   = program;
    s->type = type;

    /* Shared matrices */
    GLuint shader3d_shared_matrices_index = glGetUniformBlockIndex(s->id,
        "Shader3DSharedMatrices");

    if (shader3d_shared_matrices_index == GL_INVALID_INDEX)
    {
        DEBUG_PRINTF("Shader3D_init(): Received bad index from "
            "glGetUniformBlockIndex() for shared matrices.\n");
        glDeleteProgram(program);
        return 4;
    }

    glUniformBlockBinding(s->id, shader3d_shared_matrices_index,
        SHADER3D_SHARED_MATRICES_INDEX);
    glBindBufferBase(GL_UNIFORM_BUFFER, SHADER3D_SHARED_MATRICES_INDEX,
        renderer->ubo_shader3d_shared_matrices);

    /* Model matrix */
    s->loc_model = glGetUniformLocation(s->id, "model");

    if (s->loc_model < 0)
    {
        DEBUG_PRINTF("Shader3D_init(): could not find model matrix "
            "uniform in shader.\n");
        glDeleteProgram(program);
        return 5;
    }

    /* Directional lighting */
    GLuint shader3d_lights_index = glGetUniformBlockIndex(s->id,
        "Shader3DDirectionalLights");

    if (shader3d_lights_index == GL_INVALID_INDEX)
        s->support_directional_lights = 0;
    else
    {
        glUniformBlockBinding(s->id, shader3d_lights_index,
            SHADER3D_LIGHTS_INDEX);
        glBindBufferBase(GL_UNIFORM_BUFFER, SHADER3D_LIGHTS_INDEX,
            renderer->ubo_shader3d_directional_lights);
        s->support_directional_lights = 1;
    }

    /* Point lighting */
    GLuint shader3d_point_lights_index = glGetUniformBlockIndex(s->id,
        "Shader3DPointLights");

    if (shader3d_lights_index == GL_INVALID_INDEX)
        s->support_point_lights = 0;
    else
    {
        glUniformBlockBinding(s->id, shader3d_point_lights_index,
            SHADER3D_POINT_LIGHTS_INDEX);
        glBindBufferBase(GL_UNIFORM_BUFFER, SHADER3D_POINT_LIGHTS_INDEX,
            renderer->ubo_shader3d_point_lights);
        s->support_point_lights = 1;
    }

    /* Scene ambient lighting */
    GLuint shader3d_ambient_lighting_index = glGetUniformBlockIndex(s->id,
        "Shader3DSceneAmbientLighting");

    if (shader3d_ambient_lighting_index != GL_INVALID_INDEX)
    {
        glUniformBlockBinding(s->id, shader3d_ambient_lighting_index,
            SHADER3D_SCENE_AMBIENT_LIGHTING_INDEX);
        glBindBufferBase(GL_UNIFORM_BUFFER, SHADER3D_SCENE_AMBIENT_LIGHTING_INDEX,
            renderer->ubo_shader3d_scene_ambient_lighting);
    }

    /* Materials - may not be supported */
    s->loc_material_shine = glGetUniformLocation(s->id, "material.shine");
    s->loc_material_color = glGetUniformLocation(s->id, "material.color");

    return 0;
}

void
BaseMesh_initFromExistingAlloc(BaseMesh *mesh,
    GLfloat *vertices, uint num_vertices,
    GLuint  *indices,  uint num_indices)
{
    ASSERT(vertices && num_vertices > 0 && indices && indices > 0);

    mesh->num_vertices  = num_vertices;
    mesh->num_indices   = num_indices;
    mesh->vbo           = 0;
    mesh->vao           = 0;
    mesh->vertices      = vertices;
    mesh->indices       = indices;
	mesh->initialised_statically = 0;
}

void
BaseMesh_initStatically(BaseMesh *mesh,
    GLfloat *vertices,  uint num_vertices,
    GLuint  *indices,   uint num_indices)
{
    ASSERT(vertices && num_vertices > 0 && indices && num_indices > 0);

    mesh->num_vertices  = num_vertices;
    mesh->num_indices   = num_indices;
    mesh->vertices      = vertices;
    mesh->indices       = indices;
    mesh->vbo           = 0;
    mesh->vao           = 0;
	mesh->initialised_statically = 1;
}

void
BaseMesh_generateBuffer(BaseMesh *mesh)
{
    ASSERT(mesh->vertices && mesh->indices
        && mesh->num_vertices && mesh->num_indices);
    ASSERT(!mesh->vao);

    glGenVertexArrays(1, &mesh->vao);
    glGenBuffers(1, &mesh->vbo);
    glGenBuffers(1, &mesh->ebo);

    glBindVertexArray(mesh->vao);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo);
    glBufferData(GL_ARRAY_BUFFER, mesh->num_vertices * sizeof(Vertex),
        mesh->vertices, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->num_indices * sizeof(GLint),
        mesh->indices, GL_STATIC_DRAW);

    setUpVertexAttribPointers();

    glBindVertexArray(0);
}

void
generateTriangleMeshNormals(GLfloat *dest, GLfloat *src, uint num_vertices)
{
    vec3 *v1, *v2, *v3;
    vec3 u, v, cross;
    GLfloat *write_pos;

    unsigned int num_triangles = num_vertices / 3;

    for (unsigned int i = 0; i < num_triangles; ++i)
    {
        v1 = (vec3*)(&src[i * 15     ]);
        v2 = (vec3*)(&src[i * 15 +  5]);
        v3 = (vec3*)(&src[i * 15 + 10]);

        vec3_sub(u, *v2, *v1);
        vec3_sub(v, *v3, *v1);

        cross[0] = (u[1] * v[2]) - (u[2] * v[1]);
        cross[1] = (u[2] * v[0]) - (u[0] * v[2]);
        cross[2] = (u[0] * v[1]) - (u[1] * v[0]);

        write_pos = dest + i * 3 * 8 + 5;

        for (GLfloat *d = write_pos;
            d < write_pos + 3 * 8;
            d += 8)
        {
            d[0] = cross[0];
            d[1] = cross[1];
            d[2] = cross[2];
        }
    }
}

int
Material_init(Material *material, Texture *diffuse, Texture *specular,
    float shine, float r, float g, float b)
{
    material->diffuse   = diffuse  ? diffuse->id  : 0;
    material->specular  = specular ? specular->id : 0;
    material->shine     = shine;
    material->color[0]  = r;
    material->color[1]  = g;
    material->color[2]  = b;

    return 0;
}

int
MaterialArray_init(MaterialArray *array, Material **materials,
    uint num_materials)
{
    if (!array)         return 1;
    if (!materials)     return 2;
    if (!num_materials) return 3;
    array->materials        = materials;
    array->num_materials    = num_materials;
    return 0;
}

void *
renderMalloc(size_t size)
{
    return DynamicMemoryBlock_malloc(&engine.renderer.dynamic_memory, size);
}

void
renderFree(void *target)
{
    DynamicMemoryBlock_free(&engine.renderer.dynamic_memory, target);
}

void
Texture_init(Texture *texture, GLenum texture_type,
    void *pixels, GLenum pixel_format,
	GLenum pixel_array_type,
	GLenum wrap_s, GLenum wrap_t,
    int w, int h)
{
    if (KYS_IS_TEXT_MODE()) return;

	glGenTextures(1, &texture->id);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(texture_type, texture->id);

	glTexParameteri(texture_type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    /* Note: used to be GL_NEAREST! which is should be used for sprites */
	glTexParameteri(texture_type, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(texture_type, GL_TEXTURE_WRAP_S, wrap_s);
	glTexParameteri(texture_type, GL_TEXTURE_WRAP_T, wrap_t);

	glTexImage2D(texture_type, 0, pixel_format, w, h,
		0, pixel_format, pixel_array_type,
		pixels);

	glBindTexture(texture_type, 0);

	texture->width  = w;
	texture->height = h;
}

void
Texture_initAsCubemap(Texture *texture,
    void *pixels_pos_x,
    void *pixels_neg_x,
    void *pixels_pos_y,
    void *pixels_neg_y,
    void *pixels_pos_z,
    void *pixels_neg_z,
    GLenum pixel_array_type,
    GLenum pixel_format,
    GLenum wrap_s, GLenum wrap_t,
    int individual_part_w, int individual_part_h)
{
    if (KYS_IS_TEXT_MODE()) return;

	glGenTextures(1, &texture->id);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture->id);

    void *pixel_arrays[6] = {pixels_pos_x, pixels_neg_x,
        pixels_pos_y, pixels_neg_y, pixels_pos_z, pixels_neg_z};

    static GLenum type_array[6] = {GL_TEXTURE_CUBE_MAP_POSITIVE_X,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_X, GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_Z};

    for (int i = 0; i < 6; ++i)
    {
        glTexImage2D(type_array[i], 0, pixel_format, individual_part_w,
            individual_part_h, 0,
            pixel_format, pixel_array_type,
            pixel_arrays[i]);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE); /* TMP */

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    texture->width = individual_part_w;
    texture->height = individual_part_h;
    texture->type = GL_TEXTURE_CUBE_MAP;
    texture->wrap_s = wrap_s;
    texture->wrap_t = wrap_t;
}

int
Renderer_init(Renderer *renderer, Window *window, char *error_log, int log_len,
    bool32 vsync)
{
    /* Initialize OpenGL */
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

        renderer->context = SDL_GL_CreateContext(window->sdl_window);

        glewExperimental    = GL_TRUE;
        GLenum glew_result  = glewInit();

        if (glew_result != GLEW_OK)
        {
            DEBUG_PRINTF("Renderer_init(): glewInit() failure. Error code: %i, "
                "string: %s\n", glew_result, glewGetErrorString(glew_result));
            return RENDERER_ERROR_GLEW_INIT;
        }

        SDL_GL_SetSwapInterval(vsync);
        glEnable(GL_BLEND);
        glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
        glEnable(GL_MULTISAMPLE);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_SCISSOR_TEST);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glEnable(GL_LINE_SMOOTH);
        glLineWidth(0.5f);
        glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    }

    renderer->window = window;

    if (MemoryPool_init(&renderer->memory, MEGABYTES(64)) != 0)
        return RENDERER_ERROR_OUT_OF_MEMORY;

    renderer->shader3ds = MemoryPool_allocateBlock(&renderer->memory,
        NUM_MAX_SHADER3DS * sizeof(Shader3D));

    if (!renderer->shader3ds)
        return RENDERER_ERROR_OUT_OF_MEMORY;

    renderer->num_shader3ds = 0;
    renderer->num_max_shader3ds = NUM_MAX_SHADER3DS;

    if (DynamicMemoryBlock_init(&renderer->dynamic_memory, &renderer->memory,
            MEGABYTES(16) - NUM_MAX_SHADER3DS * sizeof(Shader3D)) != 0)
    {
        DEBUG_PRINTF("Renderer_init(): failed to allocate enough memory at "
            "initialization.\n");
        return RENDERER_ERROR_OUT_OF_MEMORY;
    }

    DEBUG_PRINTF("Renderer: allocated %lu initial memory.\n",
        (size_t)MEGABYTES(32));

    int num_errors = 0;

    /* Generate Shader3D shared uniform buffer objects */
    glGenBuffers(1, &renderer->ubo_shader3d_shared_matrices);
    glGenBuffers(1, &renderer->ubo_shader3d_directional_lights);
    glGenBuffers(1, &renderer->ubo_shader3d_point_lights);
    glGenBuffers(1, &renderer->ubo_shader3d_scene_ambient_lighting);

    glBindBuffer(GL_UNIFORM_BUFFER, renderer->ubo_shader3d_shared_matrices);
    glBufferData(GL_UNIFORM_BUFFER, SHARED_MAT_BUF_SIZE, 0, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_UNIFORM_BUFFER, renderer->ubo_shader3d_directional_lights);
    glBufferData(GL_UNIFORM_BUFFER, DIR_LIGHT_BUF_SIZE, 0, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_UNIFORM_BUFFER, renderer->ubo_shader3d_point_lights);
    glBufferData(GL_UNIFORM_BUFFER, POINT_LIGHT_BUF_SIZE, 0, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_UNIFORM_BUFFER, renderer->ubo_shader3d_scene_ambient_lighting);
    glBufferData(GL_UNIFORM_BUFFER, AMB_LIGHT_BUF_SIZE, 0, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    /* Init default shaders */
    char *vtx_code;
    char *frg_code;

    /* OPAQUE_OPAQUE */
    {
        vtx_code = readTextFileToBuffer("shaders/opaque.vert");
        frg_code = readTextFileToBuffer("shaders/opaque.frag");

        int shader_opaque = Renderer_createShader3D(renderer,
            SHADER3D_TYPE_OPAQUE, vtx_code, frg_code);

        if (shader_opaque != SHADER3D_OPAQUE_OPAQUE)
        {
            DEBUG_PRINTF("Renderer error: wrong index assigned to default opaque "
                "shader - assigned index: %i - wanted index: %i\n", shader_opaque,
                SHADER3D_OPAQUE_OPAQUE);
        }

        kfree((void*)vtx_code);
        kfree((void*)frg_code);
    }

    /* OPAQUE_SKYBOX */
    {
        vtx_code = readTextFileToBuffer("shaders/opaque_skybox.vert");
        frg_code = readTextFileToBuffer("shaders/opaque_skybox.frag");

        int shader_opaque_tex_only = Renderer_createShader3D(renderer,
            SHADER3D_TYPE_OPAQUE, vtx_code, frg_code);

        if (shader_opaque_tex_only != SHADER3D_OPAQUE_SKYBOX)
        {
            DEBUG_PRINTF("Renderer error: wrong index assigned to default opaque"
                " skybox shader - assigned index: %i - wanted index: %i\n",
                shader_opaque_tex_only, SHADER3D_OPAQUE_SKYBOX);
        }

        kfree((void*)vtx_code);
        kfree((void*)frg_code);
    }

    /* TRANSPARENT_TRANSPARENT */
    {
        vtx_code = readTextFileToBuffer("shaders/opaque.vert");
        frg_code = readTextFileToBuffer("shaders/transparent.frag");

        int index = Renderer_createShader3D(renderer,
            SHADER3D_TYPE_TRANSPARENT, vtx_code, frg_code);

        if (index != SHADER3D_TRANSPARENT_TRANSPARENT)
        {
            DEBUG_PRINTF("Renderer error: wrong index assigned to default transparent"
                " shader - assigned index: %i - wanted index: %i\n",
                index, SHADER3D_TRANSPARENT_TRANSPARENT);
        }

        kfree((void*)vtx_code);
        kfree((void*)frg_code);
    }

    {
        vtx_code = readTextFileToBuffer("shaders/opaque_gouraud.vert");
        frg_code = readTextFileToBuffer("shaders/opaque_gouraud.frag");

        int index = Renderer_createShader3D(renderer,
            SHADER3D_TYPE_OPAQUE, vtx_code, frg_code);

        if (index != SHADER3D_OPAQUE_GOURAUD)
        {
            DEBUG_PRINTF("Renderer error: wrong index assigned to default"
                " opaque gouraud shader - assigned index: %i - wanted index: %i\n",
                index, SHADER3D_OPAQUE_GOURAUD);
        }

        kfree((void*)vtx_code);
        kfree((void*)frg_code);
    }

    /* BILLBOARD */
    {
        vtx_code = readTextFileToBuffer("shaders/opaque.vert");
        frg_code = readTextFileToBuffer("shaders/billboard.frag");

        int index = Renderer_createShader3D(renderer,
            SHADER3D_TYPE_BILLBOARD, vtx_code, frg_code);

        if (index != SHADER3D_BILLBOARD)
        {
            DEBUG_PRINTF("Renderer error: wrong index assigned to default billboard"
                " shader - assigned index: %i - wanted index: %i\n",
                index, SHADER3D_BILLBOARD);
        }

        kfree((void*)vtx_code);
        kfree((void*)frg_code);
    }

    /* OUTLINE */
    {
        vtx_code = readTextFileToBuffer("shaders/default_outline.vert");
        frg_code = readTextFileToBuffer("shaders/default_outline.frag");

        int ret = Shader3D_init(&renderer->default_outline_shader,
            renderer, SHADER3D_TYPE_OPAQUE_OUTLINE, vtx_code, frg_code);

        if (ret != 0)
        {
            DEBUG_PRINTF("Renderer error: failed loading outline shader\n");
        }
        else
        {
            renderer->outline_offset_index = glGetUniformLocation(
                renderer->default_outline_shader.id, "outline_offset");

            if (renderer->outline_offset_index < 0)
            {
                DEBUG_PRINTF("Shader3D_init(): warning: no uniform named outline_"
                    "offset found in default opaque outline shader.\n");
            }
        }

        kfree((void*)vtx_code);
        kfree((void*)frg_code);
    }

    /* Find out uniform member offsets */
    {
        GLuint prog = renderer->shader3ds[SHADER3D_OPAQUE_GOURAUD].id;

        const char  *names[6] = {"projection", "look_at", "camera_pos"};
        GLuint      indices[6];

        /* Going to assume the opaque gouraud shader was successfully
         * compiled. */
        glGetUniformIndices(prog, 3, names, indices);

        GLint offsets[6];
        glGetActiveUniformsiv(prog, 3, indices, GL_UNIFORM_OFFSET, offsets);

        renderer->ubo_os_projection = offsets[0];
        renderer->ubo_os_look_at    = offsets[1];
        renderer->ubo_os_position   = offsets[2];

        for (int i = 0; i < 3; ++i)
            DEBUG_PRINTF("Mat UBO offset[%d]: %u\n", i, offsets[i]);

        names[0] = "dir_ambient";
        names[1] = "dir_direction";
        names[2] = "dir_diffuse";
        names[3] = "dir_specular";
        names[4] = "num_directional_lights";

        glGetUniformIndices(prog, 5, names, indices);
        glGetActiveUniformsiv(prog, 5, indices, GL_UNIFORM_OFFSET, offsets);

        renderer->ubo_os_dir_ambient    = offsets[0];
        renderer->ubo_os_dir_direction  = offsets[1];
        renderer->ubo_os_dir_diffuse    = offsets[2];
        if (offsets[3] < DIR_LIGHT_BUF_SIZE)
            renderer->ubo_os_dir_specular   = offsets[3];
        else
        {
            renderer->ubo_os_dir_specular =
                MAX_ENTITY_DIRECTIONAL_LIGHTS * sizeof(vec4) * 3;
            DEBUG_PRINTF("ubo_os_dir_specular: %u\n",
                renderer->ubo_os_dir_specular);
        }
        if (offsets[4] < DIR_LIGHT_BUF_SIZE)
            renderer->ubo_os_dir_num = offsets[4];
        else
        {
            renderer->ubo_os_dir_num =
                MAX_ENTITY_DIRECTIONAL_LIGHTS * sizeof(vec4) * 4;
            DEBUG_PRINTF("ubo_os_dir_num: %u\n",
                renderer->ubo_os_dir_num);
        }

        for (int i = 0; i < 5; ++i)
            DEBUG_PRINTF("Dir UBO offset[%d]: %u\n", i, offsets[i]);

        names[0] = "pnt_position";
        names[1] = "pnt_diffuse";
        names[2] = "pnt_specular";
        names[3] = "pnt_ambient";
        names[4] = "pnt_constant_linear_quadratic";
        names[5] = "num_point_lights";

        glGetUniformIndices(prog, 6, names, indices);
        glGetActiveUniformsiv(prog, 6, indices, GL_UNIFORM_OFFSET, offsets);

        renderer->ubo_os_pnt_position   = offsets[0];
        renderer->ubo_os_pnt_diffuse    = offsets[1];
        renderer->ubo_os_pnt_specular   = offsets[2];
        if (offsets[3] < POINT_LIGHT_BUF_SIZE)
            renderer->ubo_os_pnt_ambient    = offsets[3];
        else
        {
            renderer->ubo_os_pnt_ambient =
                MAX_ENTITY_POINT_LIGHTS * sizeof(vec4) * 3;
            DEBUG_PRINTF("ubo_os_pnt_ambient: %u\n",
                renderer->ubo_os_pnt_ambient);
        }
        if (offsets[4] < POINT_LIGHT_BUF_SIZE)
            renderer->ubo_os_pnt_clq = offsets[4];
        else
        {
            renderer->ubo_os_pnt_clq =
                MAX_ENTITY_POINT_LIGHTS * sizeof(vec4) * 3;
            DEBUG_PRINTF("ubo_os_pnt_clq: %u\n", renderer->ubo_os_pnt_clq);
        }
        if (offsets[5] < POINT_LIGHT_BUF_SIZE)
            renderer->ubo_os_pnt_num = offsets[5];
        else
        {
            renderer->ubo_os_pnt_num =
                MAX_ENTITY_POINT_LIGHTS * sizeof(vec4) * 4;
            DEBUG_PRINTF("ubo_os_pnt_num: %u\n", renderer->ubo_os_pnt_num);
        }

        for (int i = 0; i < 5; ++i)
            DEBUG_PRINTF("Point UBO offset[%d]: %u\n", i, offsets[i]);
    }

    renderer->background_color[0] = 0.0f;
    renderer->background_color[1] = 0.0f;
    renderer->background_color[2] = 0.0f;
    renderer->background_color[3] = 1.0f;

    if (SpriteBatch_init(&renderer->sprite_batch, error_log, log_len) != 0)
    {
        ++num_errors;
    }

    renderer->cleared_frame = 0;
    renderer->use_default_viewport = 0;
    renderer->default_viewport[0] = 0;
    renderer->default_viewport[1] = 0;
    renderer->default_viewport[2] = window->width;
    renderer->default_viewport[3] = window->height;

    renderer->base_mesh_buffer_queue = 0;

#if _CF_ALLOW_IMMEDIATE_RENDER
    if (ImmediateRenderState_init(&renderer->immediate_state,
        renderer, error_log, log_len) != 0)
    {
        DEBUG_PRINTF("Renderer_init(): failed to initialize immediate render "
            "state.\n");
        return RENDERER_ERROR_IMMEDIATE_MODE_INIT;
    }
#endif

    Mutex_init(&renderer->base_mesh_buffer_queue_mutex);

    return num_errors;
}

static inline void
copyDirectionalLightListToBuffer(Renderer *r, DirectionalLightComponent **list,
    int num_lights, char *buf)
{
    GLfloat *amb = (GLfloat*)(buf + r->ubo_os_dir_ambient);
    GLfloat *dir = (GLfloat*)(buf + r->ubo_os_dir_direction);
    GLfloat *dif = (GLfloat*)(buf + r->ubo_os_dir_diffuse);
    GLfloat *spc = (GLfloat*)(buf + r->ubo_os_dir_specular);
    GLint   *num = (GLint*)(buf + r->ubo_os_dir_num);

    int os;

    for (int i = 0; i < num_lights; ++i)
    {
        os = i * 4;
        vec3_copy(amb + os, list[i]->ambient);
        vec3_copy(dir + os, list[i]->direction);
        vec3_copy(dif + os, list[i]->diffuse);
        vec3_copy(spc + os, list[i]->specular);
    }

    *num = (GLint)num_lights;
}

static inline void
copyPointLightListToBuffer(Renderer *r, PointLightComponent **list,
    int num_lights, char *buf)
{
    GLfloat *pos = (GLfloat*)(buf + r->ubo_os_pnt_position);
    GLfloat *dif = (GLfloat*)(buf + r->ubo_os_pnt_diffuse);
    GLfloat *spc = (GLfloat*)(buf + r->ubo_os_pnt_specular);
    GLfloat *amb = (GLfloat*)(buf + r->ubo_os_pnt_ambient);
    GLfloat *clq = (GLfloat*)(buf + r->ubo_os_pnt_clq);
    GLint   *num = (GLint*)(buf + r->ubo_os_pnt_num);

    int os;

    for (int i = 0; i < num_lights; ++i)
    {
        os = i * 4;
        vec3_copy(pos + os, list[i]->cached_position);
        vec3_copy(dif + os, list[i]->diffuse);
        vec3_copy(spc + os, list[i]->specular);
        vec3_copy(amb + os, list[i]->ambient);
        vec3_set (clq + os, list[i]->constant_attenuation,
            list[i]->linear_attenuation, list[i]->quadratic_attenuation);
    }

    *num = (GLint)num_lights;
}

#if _CF_SCENE_DEBUG_RENDERING

static void
Renderer_renderCollisionWorldNode(Renderer *renderer, CollisionWorldNode *node)
{
#define DRAW_OCT_POINT(a, b, c) \
    ImmediateRenderState_vertex3f(&renderer->immediate_state, \
        node->center[0] + (a), \
        node->center[1] + (b), \
        node->center[2] + (c));

    DRAW_OCT_POINT(-node->dimensions,   -node->dimensions,  -node->dimensions);
    DRAW_OCT_POINT(node->dimensions,    -node->dimensions,  -node->dimensions);
    DRAW_OCT_POINT(node->dimensions,    -node->dimensions,  node->dimensions);
    DRAW_OCT_POINT(-node->dimensions,   -node->dimensions,  node->dimensions);
    DRAW_OCT_POINT(-node->dimensions,   -node->dimensions,  -node->dimensions);
    /* 1. pillar */
    DRAW_OCT_POINT(-node->dimensions,   node->dimensions,   -node->dimensions);
    /* 2. pillar */
    DRAW_OCT_POINT(node->dimensions,    node->dimensions,   -node->dimensions);
    /* 3. pillar */
    DRAW_OCT_POINT(node->dimensions,    node->dimensions,   node->dimensions);
    /* 4. pillar */
    DRAW_OCT_POINT(-node->dimensions,   node->dimensions,   node->dimensions);
    DRAW_OCT_POINT(node->dimensions,    node->dimensions,   -node->dimensions);
    DRAW_OCT_POINT(node->dimensions,    node->dimensions,   node->dimensions);
    DRAW_OCT_POINT(-node->dimensions,   node->dimensions,   node->dimensions);
    DRAW_OCT_POINT(-node->dimensions,   node->dimensions,   -node->dimensions);

#undef DRAW_OCT_POINT

    if (!node->children[0])
        return;

    for (int i = 0; i < 8; ++i)
        Renderer_renderCollisionWorldNode(renderer, node->children[i]);
}

static void
Renderer_renderSceneCollisionOctree(Renderer *renderer, Scene3D *scene,
    Camera *camera, float viewport_width, float viewport_height)
{
    ImmediateRenderState_identity(&renderer->immediate_state);
    ImmediateRenderState_perspective(&renderer->immediate_state, camera->fov,
        viewport_width / viewport_height,
        camera->near_clip, camera->far_clip);
    ImmediateRenderState_lookAtCamera(&renderer->immediate_state, camera);
    ImmediateRenderState_updateMatrix(&renderer->immediate_state);

    ImmediateRenderState_lineWidth(&renderer->immediate_state, 1.5f);
    ImmediateRenderState_color(&renderer->immediate_state,
        0.6f, 0.7f, 0.1f, 1.0f);

    ImmediateRenderState_begin(&renderer->immediate_state, GL_LINE_STRIP);
    Renderer_renderCollisionWorldNode(renderer, scene->collision_world.tree);
    ImmediateRenderState_end(&renderer->immediate_state);
}

static void
Renderer_renderSceneCollisionGeometry(Renderer *renderer, Scene3D *scene,
    Camera *camera,  float viewport_width, float viewport_height)
{
    ImmediateRenderState_identity(&renderer->immediate_state);
    ImmediateRenderState_perspective(&renderer->immediate_state, camera->fov,
        viewport_width / viewport_height,
        camera->near_clip, camera->far_clip);
    ImmediateRenderState_lookAtCamera(&renderer->immediate_state, camera);
    ImmediateRenderState_updateMatrix(&renderer->immediate_state);

    ImmediateRenderState_lineWidth(&renderer->immediate_state, 1.5f);
    ImmediateRenderState_color(&renderer->immediate_state,
        0.1f, 0.1f, 0.7f, 1.0f);


    for (CollisionTriangle *t = scene->collision_world.triangles;
         t < &scene->collision_world.triangles[scene->collision_world.num_triangles];
         ++t)
    {
        ImmediateRenderState_begin(&renderer->immediate_state, GL_LINE_STRIP);

        ImmediateRenderState_vertex3f(&renderer->immediate_state,
            t->data.points[0][0], t->data.points[0][1], t->data.points[0][2]);

        ImmediateRenderState_vertex3f(&renderer->immediate_state,
            t->data.points[1][0], t->data.points[1][1], t->data.points[1][2]);

        ImmediateRenderState_vertex3f(&renderer->immediate_state,
            t->data.points[2][0], t->data.points[2][1], t->data.points[2][2]);

        ImmediateRenderState_vertex3f(&renderer->immediate_state,
            t->data.points[0][0], t->data.points[0][1], t->data.points[0][2]);

        ImmediateRenderState_end(&renderer->immediate_state);
    }
}

void
Renderer_renderBoxCollider(Renderer *renderer, BoxCollider *box)
{
    if (!GET_BITFLAG(box->flags, char, COLLIDER_FLAG_IS_INACTIVE))
        ImmediateRenderState_color(&renderer->immediate_state, 0.0f, 0.0f, 1.0f, 1.0f);
    else if (GET_BITFLAG(box->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY))
        ImmediateRenderState_color(&renderer->immediate_state, 0.8f, 0.1f, 0.1f, 1.0f);
    else
        ImmediateRenderState_color(&renderer->immediate_state, 0.0f, 0.0f, 1.0f, 1.0f);

    vec3 v;
    ImmediateRenderState_begin(&renderer->immediate_state, GL_LINES);

    #define DRAW_BOX_POINT(a, b, c) \
        v[0] = a; v[1] = b; v[2] = c; \
        vec3_mul_mat3x3(v, v, box->data.rotation); \
        vec3_add(v, v, box->data.center); \
        imr_vertex3f(v[0], v[1], v[2]);

    DRAW_BOX_POINT(box->data.dimensions[0], box->data.dimensions[1], box->data.dimensions[2]);
    DRAW_BOX_POINT(box->data.dimensions[0], box->data.dimensions[1], -box->data.dimensions[2]);
    DRAW_BOX_POINT(box->data.dimensions[0], -box->data.dimensions[1], -box->data.dimensions[2]);
    DRAW_BOX_POINT(box->data.dimensions[0], -box->data.dimensions[1], box->data.dimensions[2]);
    DRAW_BOX_POINT(box->data.dimensions[0], box->data.dimensions[1], box->data.dimensions[2]);
    DRAW_BOX_POINT(-box->data.dimensions[0], box->data.dimensions[1], box->data.dimensions[2]);
    DRAW_BOX_POINT(-box->data.dimensions[0], -box->data.dimensions[1], box->data.dimensions[2]);
    DRAW_BOX_POINT(box->data.dimensions[0], -box->data.dimensions[1], box->data.dimensions[2]);
    DRAW_BOX_POINT(box->data.dimensions[0], box->data.dimensions[1], -box->data.dimensions[2]);
    DRAW_BOX_POINT(-box->data.dimensions[0], box->data.dimensions[1], -box->data.dimensions[2]);
    DRAW_BOX_POINT(-box->data.dimensions[0], -box->data.dimensions[1], -box->data.dimensions[2]);
    DRAW_BOX_POINT(box->data.dimensions[0], -box->data.dimensions[1], -box->data.dimensions[2]);
    DRAW_BOX_POINT(-box->data.dimensions[0], box->data.dimensions[1], -box->data.dimensions[2]);
    DRAW_BOX_POINT(-box->data.dimensions[0], box->data.dimensions[1], box->data.dimensions[2]);
    DRAW_BOX_POINT(-box->data.dimensions[0], -box->data.dimensions[1], box->data.dimensions[2]);
    DRAW_BOX_POINT(-box->data.dimensions[0], -box->data.dimensions[1], -box->data.dimensions[2]);

    ImmediateRenderState_end(&renderer->immediate_state);

#undef DRAW_BOX_POINT
}

void
Renderer_renderStaticBoxCollider(Renderer *renderer, float pos_x, float pos_y, float pos_z, float dim_x, float dim_y, float dim_z, float rot_x, float rot_y, float rot_z)
{
    ImmediateRenderState_color(&renderer->immediate_state, 0.0f, 0.0f, 1.0f, 1.0f);
    OBB3D obb;
    obb.center[0] = pos_x;
    obb.center[1] = pos_y;
    obb.center[2] = pos_z;
    obb.dimensions[0] = dim_x;
    obb.dimensions[1] = dim_y;
    obb.dimensions[2] = dim_z;
    OBB3D_setRotation(&obb, rot_x, rot_y, rot_z);
    vec3 v;
    ImmediateRenderState_begin(&renderer->immediate_state, GL_LINE_STRIP);

#define DRAW_BOX_POINT(a, b, c) \
        v[0] = a; v[1] = b; v[2] = c; \
        vec3_mul_mat3x3(v, v, obb.rotation); \
        vec3_add(v, v, obb.center); \
        imr_vertex3f(v[0], v[1], v[2]);

        DRAW_BOX_POINT(obb.dimensions[0], obb.dimensions[1], obb.dimensions[2]);
        DRAW_BOX_POINT(obb.dimensions[0], obb.dimensions[1], -obb.dimensions[2]);
        DRAW_BOX_POINT(obb.dimensions[0], -obb.dimensions[1], -obb.dimensions[2]);
        DRAW_BOX_POINT(obb.dimensions[0], -obb.dimensions[1], obb.dimensions[2]);
        DRAW_BOX_POINT(obb.dimensions[0], obb.dimensions[1], obb.dimensions[2]);
        DRAW_BOX_POINT(-obb.dimensions[0], obb.dimensions[1], obb.dimensions[2]);
        DRAW_BOX_POINT(-obb.dimensions[0], -obb.dimensions[1], obb.dimensions[2]);
        DRAW_BOX_POINT(obb.dimensions[0], -obb.dimensions[1], obb.dimensions[2]);
        DRAW_BOX_POINT(obb.dimensions[0], obb.dimensions[1], -obb.dimensions[2]);
        DRAW_BOX_POINT(-obb.dimensions[0], obb.dimensions[1], -obb.dimensions[2]);
        DRAW_BOX_POINT(-obb.dimensions[0], -obb.dimensions[1], -obb.dimensions[2]);
        DRAW_BOX_POINT(obb.dimensions[0], -obb.dimensions[1], -obb.dimensions[2]);
        DRAW_BOX_POINT(-obb.dimensions[0], obb.dimensions[1], -obb.dimensions[2]);
        DRAW_BOX_POINT(-obb.dimensions[0], obb.dimensions[1], obb.dimensions[2]);
        DRAW_BOX_POINT(-obb.dimensions[0], -obb.dimensions[1], obb.dimensions[2]);
        DRAW_BOX_POINT(-obb.dimensions[0], -obb.dimensions[1], -obb.dimensions[2]);

    ImmediateRenderState_end(&renderer->immediate_state);

#undef DRAW_BOX_POINT
}

void
Renderer_renderSphereCollider(Renderer *renderer, SphereCollider *sphere)
{
    if (!GET_BITFLAG(sphere->flags, char, COLLIDER_FLAG_IS_INACTIVE))
        ImmediateRenderState_color(&renderer->immediate_state, 0.0f, 0.0f, 1.0f, 1.0f);
    else if (GET_BITFLAG(sphere->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY))
        ImmediateRenderState_color(&renderer->immediate_state, 0.8f, 0.1f, 0.1f, 1.0f);
    else
        ImmediateRenderState_color(&renderer->immediate_state, 0.0f, 0.0f, 1.0f, 1.0f);

    ImmediateRenderState_begin(&renderer->immediate_state, GL_LINES);

    double theta = 0;
    double phi = 0;

    vec3 temp;

    double delta_theta = M_PI / 12;
    double delta_phi = 2 * M_PI / 10;

    for (int i = 0; i < 10; i++)
    {
        theta += delta_theta;

        for (int j = 0; j < 10; j++)
        {
            phi += delta_phi;
            temp[0] = (float)(sin(theta) * cos(phi));
            temp[1] = (float)(sin(theta) * sin(phi));
            temp[2] = (float)(cos(theta));
            vec3_scale(temp, temp, sphere->radius);
            vec3_add(temp, temp, sphere->data.center);

            ImmediateRenderState_vertex3f(&renderer->immediate_state,
                temp[0], temp[1], temp[2]);
        }
    }

    ImmediateRenderState_end(&renderer->immediate_state);
}

#endif

void
Renderer_renderScene3D(Renderer *renderer, Scene3D *scene,
    Camera *camera, Viewport viewport)
{
    Renderer_clearFrameIfNecessary(renderer);
    Renderer_flushIfNecessary(renderer);
    glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glLineWidth(2.0f);
    GLfloat vp_width_f, vp_height_f; /* Used for the projection matrix */

    if (viewport)
    {
        vp_width_f  = (GLfloat)viewport[2];
        vp_height_f = (GLfloat)viewport[3];
        Renderer_setGLViewportAndScissor(renderer,
            viewport[0], viewport[1], viewport[2], viewport[3]);
    }
    else
    {
        vp_width_f  = (GLfloat)renderer->window->width;
        vp_height_f = (GLfloat)renderer->window->height;
        Renderer_setGLViewportAndScissor(renderer, 0, 0,
            renderer->window->width, renderer->window->height);
    }

    Renderer_bindShader3D(renderer, SHADER3D_OPAQUE_OPAQUE);

    glClearColor(scene->bg_color[0], scene->bg_color[1],
        scene->bg_color[2], scene->bg_color[3]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    mat4x4 *camera_look_at;

    /* Set up the projection matrix */
    {
        mat4x4 mtx_projection;
        mat4x4_identity(mtx_projection);
        mat4x4_perspective(mtx_projection,
            camera->fov,
            vp_width_f / vp_height_f,
            camera->near_clip, camera->far_clip);

        camera_look_at = Camera_updateAndGetViewMatrix(camera);

        /* Send the projection and look-at to the gpu */
        glBindBuffer(GL_UNIFORM_BUFFER, renderer->ubo_shader3d_shared_matrices);

        char ubo_buf[SHARED_MAT_BUF_SIZE]; /* Lets assume it won't overflow */

        memcpy(ubo_buf + renderer->ubo_os_projection, mtx_projection,
            sizeof(mat4x4));

        memcpy(ubo_buf + renderer->ubo_os_look_at, (*camera_look_at),
            sizeof(mat4x4));

        vec3_set((float*)(ubo_buf + renderer->ubo_os_position),
            (GLfloat)camera->position[0],
            (GLfloat)camera->position[1],
            (GLfloat)camera->position[2]);

        glBufferSubData(GL_UNIFORM_BUFFER, 0, SHARED_MAT_BUF_SIZE, ubo_buf);
    }

    /* Ambient lighting */
    {
        glBindBuffer(GL_UNIFORM_BUFFER, renderer->ubo_shader3d_scene_ambient_lighting);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(vec4), scene->ambient_light_color);
    }

    GLuint bound_vao    = 0;
    Material *bound_mat = (Material*)&bound_vao; /* Just point anywhere except 0 muh main mang */
    int bound_shader    = -1;

    char dir_light_buffer[DIR_LIGHT_BUF_SIZE];
    char point_light_buffer[POINT_LIGHT_BUF_SIZE];

    mat3x3 billboard_tmp, billboard_temp;
    mat4x4 billboard_rotate;
    mat4x4_identity(billboard_rotate);

    MaterialArray *material_array = 0;

    Material *array[1];
    MaterialArray tmp_material_array;
    tmp_material_array.materials     = array;
    tmp_material_array.num_materials = 1;

    GLfloat outline_normal_offset = FLT_MAX;

    uint material_iterator;

    /* Render the skybox */
    if (scene->have_skybox)
    {
        glCullFace(GL_FRONT);
        glDepthMask(GL_FALSE);
        bound_shader = SHADER3D_OPAQUE_SKYBOX;
        Renderer_bindShader3D(renderer, SHADER3D_OPAQUE_SKYBOX);

        BaseMesh *cube_mesh = getDefaultCubeMesh();

        if (cube_mesh->vao != bound_vao || bound_vao == 0)
        {
            bound_vao = cube_mesh->vao;
            glBindVertexArray(bound_vao);
        }

        mat4x4 mat_model;
        mat4x4 mat_tmp;
        mat4x4_identity(mat_tmp);
        mat4x4_identity(mat_model);

        mat4x4_translate_in_place(mat_model, camera->position[0],
            camera->position[1], camera->position[2]);

        mat4x4_scale_aniso(mat_model, mat_model,
            scene->skybox_scale[0] * SKYBOX_BASE_SCALE_MULTIPLIER,
            scene->skybox_scale[1] * SKYBOX_BASE_SCALE_MULTIPLIER,
            scene->skybox_scale[2] * SKYBOX_BASE_SCALE_MULTIPLIER);

        mat4x4_mul(mat_model, mat_model, scene->skybox_rotation);

        glUniformMatrix4fv(renderer->shader3ds[SHADER3D_OPAQUE_SKYBOX].loc_model,
            1, GL_FALSE, &mat_model[0][0]);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(scene->skybox_texture->type, scene->skybox_texture->id);
        glDrawElements(GL_TRIANGLES, cube_mesh->num_indices, GL_UNSIGNED_INT, 0);
        glDepthMask(GL_TRUE);
    }

    /* Render entities */

    for (RenderModel *m = scene->render_model_pool.reserved;
         m;
         m = m->pool_next)
    {
        if (!m->visible)    continue;
        if (!m->model)      continue;

        material_iterator = 0;

        if (m->shader != bound_shader)
        {
            bound_shader = m->shader;
            Renderer_bindShader3D(renderer, bound_shader);
        }

        if (m->material_array)
            material_array = m->material_array;
        else
        {
            material_array = &tmp_material_array;
            tmp_material_array.materials[0] = m->material;
        }

        /* Directional lights */
        {
            copyDirectionalLightListToBuffer(renderer, m->directional_lights,
                (int)m->light_count.directional, dir_light_buffer);
            glBindBuffer(GL_UNIFORM_BUFFER, renderer->ubo_shader3d_directional_lights);
            glBufferSubData(GL_UNIFORM_BUFFER, 0, DIR_LIGHT_BUF_SIZE,
                dir_light_buffer);
        }

        /* Point lights */
        {
            copyPointLightListToBuffer(renderer, m->point_lights,
                (int)m->light_count.point, point_light_buffer);
            glBindBuffer(GL_UNIFORM_BUFFER, renderer->ubo_shader3d_point_lights);
            glBufferSubData(GL_UNIFORM_BUFFER, 0, POINT_LIGHT_BUF_SIZE,
                point_light_buffer);
        }

        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        for (BaseMesh *mesh = m->model->meshes;
             mesh < &m->model->meshes[m->model->num_meshes];
             ++mesh)
        {
            if (material_array->materials[material_iterator] != bound_mat)
            {
                bound_mat = material_array->materials[material_iterator];
                bindMaterial(bound_mat, &renderer->shader3ds[bound_shader]);
            }

            if (mesh->vao != bound_vao)
            {
                bound_vao = mesh->vao;
                glBindVertexArray(mesh->vao);
            }
            // billboard rotate
            if (m->mode == RENDER_MODEL_BILLBOARD)
            {
                mat4x4_identity(billboard_rotate);
                vec3_copy(billboard_temp[0], camera->mat_look_at[0]);
                vec3_copy(billboard_temp[1], camera->mat_look_at[1]);
                vec3_copy(billboard_temp[2], camera->mat_look_at[2]);
                mat3x3_transpose(billboard_tmp, billboard_temp);
                vec3_copy(billboard_rotate[0], billboard_tmp[0]);
                vec3_copy(billboard_rotate[1], billboard_tmp[1]);
                vec3_copy(billboard_rotate[2], billboard_tmp[2]);

                mat4x4_mul(m->mat_model, m->mat_model, billboard_rotate);
            }

            glUniformMatrix4fv(renderer->shader3ds[bound_shader].loc_model,
                1, GL_FALSE, &m->mat_model[0][0]);

            /*Outline rendering*/

            if (renderer->shader3ds[bound_shader].type == SHADER3D_TYPE_OPAQUE_OUTLINE)
            {
                glEnable(GL_CULL_FACE);
                glCullFace(GL_FRONT);

                glUseProgram(renderer->default_outline_shader.id);

                /* Pls change string to id here */
                glUniformMatrix4fv(renderer->default_outline_shader.loc_model,
                    1, GL_FALSE, &m->mat_model[0][0]);

                if (m->outline_normal_offset != outline_normal_offset)
                {
                    glUniform1f(renderer->outline_offset_index,
                        m->outline_normal_offset);
                    outline_normal_offset = m->outline_normal_offset;
                }

                glDrawElements(GL_TRIANGLES, mesh->num_indices, GL_UNSIGNED_INT, 0);

                glUseProgram(renderer->shader3ds[bound_shader].id);
                glCullFace(GL_BACK);
                glDisable(GL_CULL_FACE);
            }

            glDrawElements(GL_TRIANGLES, mesh->num_indices, GL_UNSIGNED_INT, 0);

            if (material_iterator < material_array->num_materials - 1)
                ++material_iterator;
        }
    }

    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);
    glBindTexture(GL_TEXTURE_2D, 0);

#if _CF_SCENE_DEBUG_RENDERING
    if (scene->render_collision_octree)
        Renderer_renderSceneCollisionOctree(renderer, scene, camera,
            vp_width_f, vp_height_f);

    if (scene->render_collision_geometry)
        Renderer_renderSceneCollisionGeometry(renderer, scene, camera,
            vp_width_f, vp_height_f);

    if (scene->render_dynamic_colliders)
    {
        for (PhysicsComponent *p = scene->entities.physics.components;
             p < &scene->entities.physics.components[scene->entities.physics.num_reserved];
             ++p)
        {
            for (BoxCollider *b = p->box_colliders; b; b = b->next)
                Renderer_renderBoxCollider(renderer, b);

            for (SphereCollider *s = p->sphere_colliders; s; s = s->next)
                Renderer_renderSphereCollider(renderer, s);
        }
    }
#endif
}

void
Renderer_setBackgroundColor(Renderer *renderer,
    float r, float g, float b, float a)
{
    renderer->background_color[0] = r;
    renderer->background_color[1] = g;
    renderer->background_color[2] = b;
    renderer->background_color[3] = a;
}

void
Renderer_clearFrameIfNecessary(Renderer *renderer)
{
    if (!renderer->cleared_frame)
    {
        glScissor(0, 0, renderer->window->width, renderer->window->height);

        glClearColor(renderer->background_color[0],
            renderer->background_color[1],
            renderer->background_color[2],
            renderer->background_color[3]);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        renderer->cleared_frame = 1;
    }
}

void
Renderer_onWindowResize(Renderer *renderer)
{
    if (!renderer->use_default_viewport)
    {
        renderer->default_viewport[0] = 0;
        renderer->default_viewport[1] = 0;
        renderer->default_viewport[2] = renderer->window->width;
        renderer->default_viewport[3] = renderer->window->height;
    }
}

void
Renderer_setDefaultViewport(Renderer *renderer, Viewport viewport)
{
    Renderer_flushIfNecessary(renderer);

    if (viewport)
    {
        renderer->default_viewport[0] = viewport[0];
        renderer->default_viewport[1] = viewport[1];
        renderer->default_viewport[2] = viewport[2];
        renderer->default_viewport[3] = viewport[3];
        renderer->use_default_viewport = 1;
    }
    else
    {
        renderer->default_viewport[0] = 0;
        renderer->default_viewport[1] = 0;
        renderer->default_viewport[2] = renderer->window->width;
        renderer->default_viewport[3] = renderer->window->height;
        renderer->use_default_viewport = 0;
    }
}

int
Renderer_createShader3D(Renderer *renderer,
    enum Shader3DType type,
    const char *vertex_code,
    const char *fragment_code)
{
    if (renderer->num_shader3ds >= renderer->num_max_shader3ds)
    {
        DEBUG_PRINTF("Warning: ran out of on shader3ds slots!\n");
        return -1;
    }

    int ret = Shader3D_init(&renderer->shader3ds[renderer->num_shader3ds],
        renderer, type, vertex_code, fragment_code);

    if (ret != 0)
    {
        DEBUG_PRINTF("Renderer_createShader3D: Shader3D_init() returned non-zero.\n");
        return -1;
    }

    return renderer->num_shader3ds++;
}

void
Renderer_handleQueuedWork(Renderer *renderer)
{
    Mutex_lock(&renderer->base_mesh_buffer_queue_mutex);

    /* Buffer new meshes */
    for (BaseMesh *m = renderer->base_mesh_buffer_queue; m; m = m->next)
        if (!m->vao)
            BaseMesh_generateBuffer(m);

    renderer->base_mesh_buffer_queue = 0;

    Mutex_unlock(&renderer->base_mesh_buffer_queue_mutex);
}

void
Renderer_swapBuffers(Renderer *renderer)
{
    Renderer_clearFrameIfNecessary(renderer);
    Renderer_flushIfNecessary(renderer);
    SDL_GL_SwapWindow(renderer->window->sdl_window);
    renderer->cleared_frame = 0;
}

void
Renderer_renderSpriteGroup(Renderer *renderer, SpriteGroup *group)
{
    Renderer_clearFrameIfNecessary(renderer);
    Renderer_flushIfNecessary(renderer);

    size_t num_sprites = group->sprite_data.num_items;

    if (group->need_sort)
    {
        bool32 swapped = 1;

        while (swapped)
        {
            swapped = 0;

            SpriteData temp_sprite_data;
            Sprite *sprite1, *sprite2;
            uint32_t temp_index;

            for (SpriteData *sprite_data = group->sprite_data.items;
                sprite_data < &group->sprite_data.items[group->sprite_data.num_items - 1];
                ++sprite_data)
            {
                if (sprite_data->layer > (sprite_data + 1)->layer)
                {
                    temp_sprite_data = *sprite_data;
                    *sprite_data = *(sprite_data + 1);
                    *(sprite_data +1) = temp_sprite_data;

                    sprite1 = ArrayList_Sprite_at(&group->sprites,
                        sprite_data->sprite_index);
                    sprite2 = ArrayList_Sprite_at(&group->sprites,
                        (sprite_data + 1)->sprite_index);

                    temp_index          = sprite1->data_index;
                    sprite1->data_index = sprite2->data_index;
                    sprite2->data_index = temp_index;

                    swapped = 1;
                }
            }
        }
        group->need_sort = 0;
    }

    for (SpriteData *data = group->sprite_data.items;
        data < &group->sprite_data.items[num_sprites];
        ++data)
    {
        if (data->visible)
        {
            SpriteBatch_drawSprite_FlipScaleRotate(&engine.renderer.sprite_batch,
                data->texture,
                data->pos[0], data->pos[1],
                0, /* clip */
                data->flip,
                data->scale[0], data->scale[1],
                data->rot);
        }
    }

    GLint viewport[4];

    if (group->use_viewport_override)
    {
        viewport[0] = group->viewport[0];
        viewport[1] = renderer->window->height - group->viewport[3] - group->viewport[1];
        viewport[2] = group->viewport[2];
        viewport[3] = group->viewport[3];
    }
    else
    {
        viewport[0] = 0;
        viewport[1] = 0;
        viewport[2] = renderer->window->width;
        viewport[3] = renderer->window->height;
    }


    Renderer_setGLViewportAndScissor(renderer,
        viewport[0], viewport[1], viewport[2], viewport[3]);

    if (group->background_color[3] > 0.0f)
    {
        glClearColor(group->background_color[0], group->background_color[1],
            group->background_color[2], group->background_color[3]);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    if (group->use_viewport_override)
    {
        GLint batch_viewport[] = {0, 0, group->viewport[2], group->viewport[3]};
        SpriteBatch_flush(&engine.renderer.sprite_batch, batch_viewport);
    }
    else
    {
        SpriteBatch_flush(&engine.renderer.sprite_batch, viewport);
    }
}

void
Renderer_renderText(Renderer *renderer, SpriteFont *sprite_font,
    const char *text,
    float x, float y,
    float *color)
{
    int index;
    float cur_x = x;

    static float white_color[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    float *actual_color = color ? color : white_color;

    for (const char *c = text; *c; ++c)
    {
        index = (int)(*c);

        if (*c != '\n')
        {
            SpriteBatch_drawSprite_Color(&renderer->sprite_batch, &sprite_font->texture,
                cur_x + sprite_font->characters[index].min_x, y,
                sprite_font->characters[index].clip,
                actual_color);

            cur_x += sprite_font->characters[index].advance;
        }
        else
        {
            y += sprite_font->height;
            cur_x = x;
        }
    }
}

void
Renderer_renderGUI(Renderer *renderer, GUI *context)
{
    if (context->num_sorted_windows <= 0)
        return;

    Renderer_clearFrameIfNecessary(renderer);
    Renderer_flushIfNecessary(renderer);
    Renderer_setGLViewportAndScissor(renderer, 0, 0, renderer->window->width,
        renderer->window->height);

    SpriteBatch *batch = &renderer->sprite_batch;

    glDisable(GL_DEPTH_TEST);
    glUseProgram(batch->program);

    {
        GLint *viewport = renderer->default_viewport;

        GLfloat projection[4][4] =
        {
            {2.0f / (GLfloat)(viewport[2] - viewport[0]), 0.0f, 0.0f, 0.0f},
            {0.0f, 2.0f / (GLfloat)(viewport[1] - viewport[3]), 0.0f, 0.0f},
            {0.0f, 0.0f, -1.0f, 0.0f},
            {- (GLfloat)(viewport[2] + viewport[0]) / (GLfloat)(viewport[2] - viewport[0]),
             - (GLfloat)(viewport[1] + viewport[3]) / (GLfloat)(viewport[1] - viewport[3]),
             0.0f, 1.0f}
        };

        glUniformMatrix4fv(batch->proj_loc, 1, GL_FALSE, &projection[0][0]);
    }

    glUniform2f(batch->scale_loc, 1.0f, 1.0f);
    glBindVertexArray(batch->vao);
    glBindBuffer(GL_ARRAY_BUFFER, batch->vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch->ebo);
    glActiveTexture(GL_TEXTURE0);

    /* Render */
    GUIWindow       *w;
    int             command_index;
    GUIDrawCommand  *cmd;

    GLuint  bound_texture   = 0;
    uint    buffer_offset   = 0;
    uint    num_vertices    = 0;

    glBindTexture(GL_TEXTURE_2D, bound_texture);

    for (ushort *index = context->sorted_windows;
         index < &context->sorted_windows[context->num_sorted_windows];
         ++index)
    {
        w = &context->windows[*index];
        command_index = w->draw_commands_index;

        /* TODO: make this not assume the buffer includes less than 2048 quads */
        glBufferSubData(GL_ARRAY_BUFFER,
            0,
            w->vertex_buffer.num_vertices * (FLOATS_PER_SPRITE / 4) * sizeof(GLfloat),
            &w->vertex_buffer.vertices[0]);

        buffer_offset   = 0;
        num_vertices    = 0;

        while (command_index != -1)
        {
            cmd = &context->command_buffer.commands[command_index];

            /* If the texture remained the same and we're still working
             * on the same buffer, just increase the amount of verts drawn */
            if (cmd->texture == bound_texture
            && num_vertices + cmd->num_vertices <= 2048 / 4)
            {
                num_vertices += cmd->num_vertices;
            }
            else
            {
                if (num_vertices > 0)
                {
                    glDrawElements(GL_TRIANGLES,
                       num_vertices / 4 * 6,
                       GL_UNSIGNED_INT,
                       (GLvoid*)(buffer_offset / 4 * 6 * sizeof(GLuint)));
                }

                bound_texture   = cmd->texture;
                buffer_offset   = cmd->vertex_index;
                num_vertices    = cmd->num_vertices;
                glBindTexture(GL_TEXTURE_2D, bound_texture);
            }

            command_index = cmd->next;
        }

        /* Flush remaining */
        if (num_vertices > 0)
        {
            glDrawElements(GL_TRIANGLES,
               num_vertices / 4 * 6,
               GL_UNSIGNED_INT,
               (GLvoid*)(buffer_offset / 4 * 6 * sizeof(GLuint)));
        }
    }

    context->num_sorted_windows = 0;
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);
}

void
Renderer_queueuBufferableBaseMesh(Renderer *renderer, BaseMesh *mesh)
{
    if (KYS_IS_TEXT_MODE()) return;
    if (mesh->vao) return;

    Mutex_lock(&renderer->base_mesh_buffer_queue_mutex);
    mesh->next = renderer->base_mesh_buffer_queue;
    renderer->base_mesh_buffer_queue = mesh;
    Mutex_unlock(&renderer->base_mesh_buffer_queue_mutex);
}

void
Model_computeAndGetMinMaxDimensions(Model *model, vec3 ret_min, vec3 ret_max)
{
    if (!model->meshes || !model->meshes->vertices)
    {
        vec3_set(ret_min, 0.0f, 0.0f, 0.0f);
        vec3_set(ret_max, 0.0f, 0.0f, 0.0f);
        return;
    }

    vec3_copy(ret_min, model->meshes->vertices);
    vec3_copy(ret_max, model->meshes->vertices);

    float *vtx;

    for (BaseMesh *m = model->meshes;
         m < &model->meshes[model->num_meshes];
         ++m)
    {
        for (uint i = 0; i < m->num_vertices; ++i)
        {
            vtx = &m->vertices[i * 8];

            if (vtx[0] < ret_min[0]) ret_min[0] = vtx[0];
            if (vtx[1] < ret_min[1]) ret_min[1] = vtx[1];
            if (vtx[2] < ret_min[2]) ret_min[2] = vtx[2];

            if (vtx[0] > ret_max[0]) ret_max[0] = vtx[0];
            if (vtx[1] > ret_max[1]) ret_max[1] = vtx[1];
            if (vtx[2] > ret_max[2]) ret_max[2] = vtx[2];
        }
    }
}
