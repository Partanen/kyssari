#pragma once
#include "../compile_flags.h"
#include "../render/renderer.h"
#include "collision.h"

/* Forward declaration(s) */
typedef struct Server Server;
typedef struct Client Client;

/* Types defined here */
typedef struct EntityTransform              EntityTransform;
typedef struct Entity                       Entity;
typedef struct Particle                     Particle;
typedef struct BoxCollider                  BoxCollider;
typedef struct SphereCollider               SphereCollider;
typedef struct RenderModel                  RenderModel;
typedef struct RenderModelPool              RenderModelPool;
typedef struct Animator                     Animator;
typedef struct AnimatorPool                 AnimatorPool;
typedef struct PhysicsComponent             PhysicsComponent;
typedef struct ModelComponent               ModelComponent;
typedef struct DirectionalLightComponent    DirectionalLightComponent;
typedef struct PointLightComponent          PointLightComponent;
typedef struct ScriptComponent              ScriptComponent;
typedef struct CollisionEvent               CollisionEvent;
typedef struct ResolvableColliderPair       ResolvableColliderPair;
typedef struct CollisionCacheEntry          CollisionCacheEntry;
typedef struct CollisionCache               CollisionCache;
typedef struct CollisionTriangle            CollisionTriangle;
typedef struct CollisionWorldNode           CollisionWorldNode;
typedef struct CollisionWorld               CollisionWorld;
typedef struct EntityPool                   EntityPool;
typedef struct ReplicationTableEntry        ReplicationTableEntry;
typedef struct ReplicationTable             ReplicationTable;
typedef struct NewReplicatedEntityData      NewReplicatedEntityData;
typedef struct ScenePhysicsLimits           ScenePhysicsLimits;
typedef struct SceneWorkRange               SceneWorkRange;
typedef struct Scene3D                      Scene3D;
typedef struct EntityLifetime               EntityLifetime;
typedef struct StaticBoxCollider            StaticBoxCollider;

typedef uint16_t    entity_type16_t;
typedef uint32_t    replication_id32_t;
typedef char        replication_bitmask_t;

/* Physics collision handler - set to point to a custom function
 * if desired, or set the correct compilation flag to ignore */
extern void (*handleEntityPhysicsCollision)(CollisionEvent *event);

#define SKYBOX_BASE_SCALE_MULTIPLIER 40.0f
#define FRICTION_STOPPING_LIMIT 0.01f
#define NUM_COLLISION_ENTRIES_PER_ENTITY 4
#define GRAVITY_MAX 0.5f
#define MAX_ENTITY_NAME_LEN 12
#define ENTITY_NOT_REPLICATED 0xFFFFFFFF

#define SIZE_OF_ENTITY (sizeof(Entity) +\
    sizeof(EntityTransform) +\
    sizeof(PhysicsComponent) +\
    sizeof(ModelComponent) +\
    sizeof(DirectionalLightComponent) +\
    sizeof(PointLightComponent) +\
    sizeof(ScriptComponent))

enum Scene3DError
{
    SCENE3D_ERROR_OUT_OF_MEMORY = 1,
    SCENE3D_ERROR_NETWORKING_DISABLED
};

enum ComponentBit
{
    PHYSICS_COMPONENT_BIT               = (1 << 0),
    MODEL_COMPONENT_BIT                 = (1 << 1),
    DIRECTIONAL_LIGHT_COMPONENT_BIT     = (1 << 2),
    POINT_LIGHT_COMPONENT_BIT           = (1 << 3),
    SCRIPT_COMPONENT_BIT                = (1 << 4)
};

enum ComponentIndex
{
    PHYSICS_COMPONENT_INDEX             = 0,
    MODEL_COMPONENT_INDEX               = 1,
    DIRECTIONAL_LIGHT_COMPONENT_INDEX   = 2,
    POINT_LIGHT_COMPONENT_INDEX         = 3,
    SCRIPT_COMPONENT_INDEX              = 4,

    NUM_MAX_COMPONENTS
};

enum ColliderAxisLock
{
	COLLIDER_AXIS_LOCK_X_BIT = (1 << 0),
	COLLIDER_AXIS_LOCK_Y_BIT = (1 << 1),
	COLLIDER_AXIS_LOCK_Z_BIT = (1 << 2)
};

enum ColliderFlag
{
    /* No physics collision and no proper data for collision normals - simply
     * generates collision enter and exit events and calls the PhysiscCompomponent's
     * callback. */
    COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY                = (1 << 3),
    COLLIDER_FLAG_IGNORE_STATIC_GEOMETRY                = (1 << 4),
    COLLIDER_FLAG_IS_INACTIVE                           = (1 << 5),
    COLLIDER_FLAG_WAS_COLLIDING_WITH_STATIC_GEOMETRY    = (1 << 6)
};

enum EntityFlag
{
    ENTITY_ALIVE = (1 << 0)
};

enum ReplicationFlag
{
    /* If replicated as a server side entity, specified whether or not
     * data of the entity was already broadcast */
    REPLICATION_FLAG_WAS_BROADCAST      = (1 << 0),
    REPLICATION_FLAG_DONT_SEND_ROTATION = (1 << 1),
    REPLICATION_FLAG_DONT_SEND_POSITION = (1 << 2),
    REPLICATION_FLAG_DONT_SEND_VELOCITY = (1 << 3)
};

enum PhysicsComponentFlag
{
    /* Collisions will not move entity */
    PHYSICS_COMPONENT_FLAG_IS_STATIC = (1 << 0),

    /* Physics and collisions are active */
    PHYSICS_COMPONENT_FLAG_IS_ACTIVE = (1 << 1)
};

enum ColliderType
{
    COLLIDER_TYPE_OBB,
    COLLIDER_TYPE_SPHERE
};

enum CollisionEventType
{
    COLLISION_ENTER,
    COLLISION_END,
    COLLISION_CONTINUOUS
};

enum RenderModelMode
{
    RENDER_MODEL_NORMAL,
    RENDER_MODEL_BILLBOARD
};

enum CollisionWorldNodeFlag
{
    CWNODE_CHILDREN_HAVE_TRIANGLES                      = (1 << 0),
    CWNODE_CHILDREN_HAVE_STATIC_BOX_COLLIDERS    = (1 << 1)
};

#define COMPONENT_POOL_DECLARATION(component_name, component_index, component_bit)\
    typedef struct component_name##Pool component_name##Pool;\
    struct component_name##Pool\
    {\
        component_name  *components;\
        uint            num_max;\
        uint            num_reserved;\
    };

struct Entity
{
    uint32_t            id;
#if _CF_NETWORKING
    replication_id32_t    replication_id;
#endif
    uint32_t            index; /* Index in the entity storage array */
    uint32_t            transform_index;
    Entity              *next;
    Scene3D             *scene;
    uint32_t            components[NUM_MAX_COMPONENTS];
    uint16_t            component_flags;
    entity_type16_t     type;
    char                flags;

#if _CF_ENTITY_STRING_NAMES
    char                name[MAX_ENTITY_NAME_LEN];
#endif
};

struct EntityTransform
{
    Transform   transform;
    uint32_t    entity_index;
};

struct Particle
{
    Model       *model;

    vec3        position;
    vec3        rotation;
    vec3		gravity;
    vec3        velocity;
    vec3        acceleration;

    float       friction;
    float       elasticity;
    float       lifetime;

    /* Collision data */
    BoxCollider     *box_colliders;
    SphereCollider  *sphere_colliders;
};

/* Colliders' transforms are relative to parent entity transforms */

struct BoxCollider
{
    uint32_t    id;
    OBB3D       data;
    BoxCollider *next;
    vec3        dimensions; /* Half-widths */
    vec3        position;
    vec3        rotation;
	char		flags;
};

struct SphereCollider
{
    uint32_t        id;
    Sphere3D        data;
    SphereCollider  *next;
    float           radius;
    vec3            position;
    char            flags;
    uint32_t        entity_index;
};

/* A pool structure for linked lists of colliders,
 * held by a scene */
#define COLLIDER_POOL_DECLARATION(collider_type) \
    typedef struct collider_type##Pool \
    { \
        collider_type   *items; \
        uint            capacity; \
        collider_type   *free; \
    } collider_type##Pool; \
\
    static inline void \
    collider_type##Pool_init(collider_type##Pool *pool, \
        void *memory, uint capacity) \
    { \
        pool->items     = memory; \
        pool->capacity  = capacity; \
        for (collider_type *c = pool->items; \
             c < &pool->items[capacity - 1]; \
             ++c) \
        { \
            c->next = c + 1; \
        } \
        pool->items[capacity - 1].next = 0; \
        pool->free = pool->items; \
    } \
\
    static inline collider_type * \
    collider_type##Pool_reserve(collider_type##Pool *pool, uint32_t id) \
    { \
        if (pool->free) \
        { \
            collider_type *collider = pool->free; \
            pool->free              = collider->next; \
            collider->next          = 0; \
            collider->id            = id; \
            return collider; \
        } \
        else \
        { \
            return 0; \
        } \
    } \
\
    static inline void \
    collider_type##Pool_free(collider_type##Pool *pool, \
        collider_type *collider) \
    { \
        collider->next = pool->free; \
        pool->free = collider; \
    }

COLLIDER_POOL_DECLARATION(BoxCollider);
COLLIDER_POOL_DECLARATION(SphereCollider);

struct PhysicsComponent
{
    uint32_t            entity_index;
    uint32_t            transform_index; /* Note: must be updated if changed,
                                  * for example upon entity removal */
    float               friction;
    float               elasticity;
    float               max_velocity;

    char                flags;

	bool32		        gravity_active;

    vec3                velocity;
    vec3                acceleration;

    /* Collision data */
    BoxCollider         *box_colliders;
    SphereCollider      *sphere_colliders;

    void                (*collisionCallback)(
                            PhysicsComponent *component_a,
                            PhysicsComponent *component_b,
                            Entity *entity_a, Entity *entity_b,
                            enum ColliderType type_a, enum ColliderType type_b,
                            enum CollisionEventType event_type,
                            uint32_t collider_a_id, uint32_t collider_b_id);

    /* Note: if the event passed to the static collision callback is of type
     * COLLISION_EXIT, the normal will be 0. */
    void                (*staticCollisionCallback)(PhysicsComponent *component,
                            Entity *entity,
                            enum ColliderType collider, uint32_t collider_id,
                            vec3 normal, enum CollisionEventType event_type);
};

struct ModelLightCount
{
    ubyte directional:4, point:4;
};

struct RenderModel
{
    /* Directly modifiable variables */
    Model                       *model;
    Material                    *material;
    MaterialArray               *material_array; /* Overrides single materials */
    Transform                   relative_transform;
    int                         shader;
    bool32                      visible;

    float                       outline_normal_offset;

    /* Probably don't want to touch these */
    RenderModel                 *pool_prev, *pool_next, *parent_next;
    mat4x4                      mat_model;
    uint32_t                    id;
    DirectionalLightComponent   *directional_lights[MAX_ENTITY_DIRECTIONAL_LIGHTS];
    PointLightComponent         *point_lights[MAX_ENTITY_POINT_LIGHTS];
    struct ModelLightCount      light_count;
    char                        mode;
};

struct RenderModelPool
{
    RenderModel *models;
    RenderModel *free;
    RenderModel *reserved;
    uint        capacity;
};

struct Animator
{
    ModelAnimation      *model_animation;
    RenderModel         *render_model;
    uint32_t            id;
    uint                current_model_frame;
    bool32              loop;
    float               time_passed;
    float               speed;

    Animator            *pool_prev, *pool_next, *parent_next;
};

struct AnimatorPool
{
    Animator    *animators;
    Animator    *free;
    Animator    *reserved;
    uint        capacity;
};

struct ModelComponent
{
    uint32_t                    entity_index;
    uint32_t                    transform_index;
    bool32                      visible;
    RenderModel                 *render_models;
    Animator                    *animators;
};

struct DirectionalLightComponent
{
    uint32_t    entity_index;
    uint32_t    transform_index;

    vec3        offset;
    vec3        position;
    vec3        direction;
    vec3        ambient;
    vec3        diffuse;
    vec3        specular;
    float       radius;
    bool8       active;
};

struct PointLightComponent
{
    uint32_t entity_index;
    uint32_t transform_index;

    float radius;
    vec3 offset;
    vec3 cached_position;
    vec3 diffuse;
    vec3 specular;
    vec3 ambient;

    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;

    bool8 active;
};

struct ScriptComponent
{
    uint32_t    entity_index;
    void        *data;
    void        (*callback)(Entity *entity, void *data);
};

struct CollisionEvent
{
    PhysicsComponent            *component_a;
    PhysicsComponent            *component_b;
    uint32_t                    collider_a;
    uint32_t                    collider_b;
    vec3                        normal;
    uint32_t                    normal_owner;
    float                       penetration;
    /* type: COLLISION_ENTER or COLLISION_END */
    enum CollisionEventType     type;
    uint8_t                     collider_a_type;
    uint8_t                     collider_b_type;
};

struct ResolvableColliderPair
{
    PhysicsComponent    *component_a;
    PhysicsComponent    *component_b;
    uint32_t            collider_a;
    uint32_t            collider_b;
    uint8_t             collider_a_type;
    uint8_t             collider_b_type;
};

/* Always pushed to the index of the entity with the lower index */
struct CollisionCacheEntry
{
    CollisionCacheEntry     *next;
    uint32_t                timestamp;
    uint32_t                entity_a_index;
    uint32_t                entity_b_index;
    uint32_t                collider_a;
    uint32_t                collider_b;
    uint8_t                 collider_a_type;
    uint8_t                 collider_b_type;
};

struct CollisionCache
{
    CollisionCacheEntry     *pool;
    CollisionCacheEntry     *free;
    CollisionCacheEntry     **table; /* Look-up table */
    ResolvableColliderPair  *resolvable_pairs;
    uint                    num_resolvable_pairs;
    uint                    num_max_resolvable_pairs;
    uint                    capacity;
};

struct CollisionTriangle
{
    Triangle            data;
    CollisionTriangle   *next;
};

struct CollisionWorldNode
{
    CollisionWorldNode  *children[8];
    CollisionWorldNode  *parent;
    CollisionTriangle   *triangles;
    StaticBoxCollider   *static_box_colliders;
    float               dimensions;
    vec3                center;
    char                flags;
#ifdef _DEBUG
    char                depth;
#endif
};

struct CollisionWorld
{
    CollisionWorldNode  *nodes;
    uint                max_nodes;
    CollisionWorldNode  *tree;
    uint                dimensions;
    CollisionTriangle   *triangles;
    uint                max_triangles;
    uint                num_triangles;
    StaticBoxCollider   *static_box_colliders;
    uint                max_static_box_colliders;
    uint                num_static_box_colliders;
};

struct StaticBoxCollider
{
    OBB3D               data;
    StaticBoxCollider   *next;
};

COMPONENT_POOL_DECLARATION(PhysicsComponent,
    PHYSICS_COMPONENT_INDEX,
    PHYSICS_COMPONENT_BIT);

COMPONENT_POOL_DECLARATION(ModelComponent,
    MODEL_COMPONENT_INDEX,
    MODEL_COMPONENT_BIT);

COMPONENT_POOL_DECLARATION(DirectionalLightComponent,
    DIRECTIONAL_LIGHT_COMPONENT_INDEX,
    DIRECTIONAL_LIGHT_COMPONENT_BIT);

COMPONENT_POOL_DECLARATION(PointLightComponent,
    POINT_LIGHT_COMPONENT_INDEX,
    POINT_LIGHT_COMPONENT_BIT);

COMPONENT_POOL_DECLARATION(ScriptComponent,
    SCRIPT_COMPONENT_INDEX,
    SCRIPT_COMPONENT_BIT);

struct EntityPool
{
    Entity                          *entities;
    EntityTransform                 *transforms;
    Entity                          *free;
    Entity                          *reserved;
    Entity                          **removables;
    EntityLifetime                  *lifetimes;
    uint                            num_lifetimes;
    uint                            num_max_entities;
    uint                            num_reserved;
    uint                            num_removables;

    /* Components */
    PhysicsComponentPool            physics;
    ModelComponentPool              models;
    DirectionalLightComponentPool   lights;
    PointLightComponentPool         point_lights;
    ScriptComponentPool             scripts;

    /* Event buffers */
    CollisionEvent                  *collision_events;
    uint                            num_max_collision_events;
    uint                            num_collision_events;

    CollisionCache                  collision_cache;
};

struct ReplicationTableEntry
{
    Entity                  *entity;
    ReplicationTableEntry   *lookup_prev, *lookup_next;
    ReplicationTableEntry   *reserved_next, *reserved_prev;
    replication_bitmask_t   flags;
    uint16_t                latest_position_update;
    uint16_t                latest_velocity_update;
};

struct ReplicationTable
{
    ReplicationTableEntry   *all_entries;
    ReplicationTableEntry   *free_entries;
    ReplicationTableEntry   *reserved_entries;
    ReplicationTableEntry   **lookup_table;
    uint                    num_reserved;
    uint                    size;
};

struct ScenePhysicsLimits
{
    float axis_y_min_val;
    float gravity_strength;
    float min_y_value;
    float max_y_value;
	vec3  gravity_direction;
};

struct SceneWorkRange
{
    Scene3D *scene;
    double  dt; /* Not used by all the functions yet, TODO: fix */
    uint    begin, end;
};

struct Scene3D
{
    bool32                      initialized;
    SimpleMemoryBlock           memory;
    Entity                      entity;
    EntityPool                  entities;
    uint                        running_entity_id;
    GLint                       render_viewport[4];
    float                       bg_color[4];
    vec3                        camera_position;
    bool32                      use_viewport_override;
    ScenePhysicsLimits          physics_limits;
    RenderModelPool             render_model_pool;
    BoxColliderPool             box_collider_pool;
    SphereColliderPool          sphere_collider_pool;
    AnimatorPool                animator_pool;
    CollisionWorld              collision_world;
    SceneWorkRange              *work_ranges;
    uint                        max_work_ranges;
    double                      delta;
    double                      physics_step;
    double                      physics_timer;

     /* Debug rendering */
    bool32                      render_collision_octree;
    bool32                      render_collision_geometry;
    bool32                      render_dynamic_colliders;

    /* Skybox */
    Texture                     *skybox_texture;
    vec3                        skybox_scale;
    bool32                      have_skybox;
    mat4x4                      skybox_rotation;

    /* Index 3 is ambient strength */
    vec4                        ambient_light_color;

    bool32                      is_multiplayer;
#if _CF_NETWORKING
    uint32_t                    running_replication_id;
    ReplicationTable            replication_table;
    SimpleMemoryBlock           message_buffer;
    Mutex                       message_buffer_mutex;

    /* Locked when entities are being updated */
    Mutex                       entity_access_mutex;

    /* Replication callbacks */

    /* Called when a replicated entity is created */
    void (*on_replicated_entity_creation_callback)(Entity *e);
#endif
};

struct EntityLifetime
{
    Entity      *entity;
    uint32_t    id;
    double      timeleft;
};

static inline bool32
Entity_hasComponent(Entity *entity, enum ComponentBit component);

static inline EntityTransform *
Entity_getTransform(Entity *entity);

void
Entity_setPosition(Entity *entity, float x, float y, float z);

void
Entity_setRotation(Entity *entity, float x, float y, float z);

void
Entity_lookAt(Entity *entity, vec3 target_pos);

static inline void
Entity_setScale(Entity *entity, float x, float  y, float z);

static inline void
Entity_getForwardVector(Entity *entity, vec3 ret);

static inline void
Entity_getNonNormalizedForwardVector(Entity *entity, vec3 ret);

static inline void
Entity_setLifetime(Entity *entity, double lifetime);

void
Entity_lookAt(Entity *entity, vec3 position);

void
Entity_strafe(Entity *entity, float speed);

void
Entity_collisionPhysics(CollisionEvent *event);

void
Entity_makeReplicated(Entity *entity);

void
Entity_makeExistingReplicated(Entity *entity,
    replication_id32_t replication_id);

static inline bool32
Entity_isReplicated(Entity *entity);


static inline void
Entity_setReplicationFlags(Entity *e, char flags);

static inline void
PhysicsComponent_setCollisionCallback(PhysicsComponent *component,
    void (*collisionCallback)(PhysicsComponent*, PhysicsComponent*,
    Entity*, Entity*, enum ColliderType, enum ColliderType,
    enum CollisionEventType, uint32_t, uint32_t));

static inline void
PhysicsComponent_setStaticCollisionCallback(PhysicsComponent *component,
    void (*staticCollisionCallback)(PhysicsComponent *component,
    Entity *entity,
    enum ColliderType collider, uint32_t collider_id,
    vec3 normal, enum CollisionEventType event_type));

static inline PhysicsComponent *
Entity_getPhysicsComponent(Entity *entity);

static inline ModelComponent *
Entity_getModelComponent(Entity *entity);

static inline DirectionalLightComponent *
Entity_getDirectionalLightComponent(Entity *entity);

static inline PointLightComponent *
Entity_getPointLightComponent(Entity *entity);

static inline ScriptComponent *
Entity_getScriptComponent(Entity *entity);

static inline void
EntityTransform_setPosition(EntityTransform *et, float x, float y, float z);

void
EntityPool_init(EntityPool *pool, Scene3D *scene, uint num_entities,
    SimpleMemoryBlock *memory);

Entity *
EntityPool_reserve(EntityPool *pool, uint id);

void
EntityPool_free(EntityPool *pool, Entity *entity);

static inline Entity *
EntityPool_getEntityByIndex(EntityPool *pool, uint32_t index);

PhysicsComponent *
Entity_attachPhysicsComponent(Entity *entity);

ModelComponent *
Entity_attachModelComponent(Entity *entity);

DirectionalLightComponent *
Entity_attachDirectionalLightComponent(Entity *entity);

PointLightComponent *
Entity_attachPointLightComponent(Entity *entity);

ScriptComponent *
Entity_attachScriptComponent(Entity *entity);

#if _CF_ENTITY_STRING_NAMES
    static inline void
    Entity_setName(Entity *e, const char *name)
    {
        strncpy(e->name, name, MAX_ENTITY_NAME_LEN);
    }

    static inline const char *
    Entity_getName(Entity *e){return e->name[0] != '\0' ? e->name : "[no name]";}
#else
    static inline void
    Entity_setName(Entity *e, const char *name){};

    static inline const char *
    Entity_getName(Entity *e){return 0;}
#endif

void
PhysicsComponent_applyForce(PhysicsComponent *component,
    vec3 direction, float strength);

void
ScriptComponent_setCallback(ScriptComponent *script,
    void (*callback)(Entity *entity, void *data));

void *
ScriptComponent_allocateData(ScriptComponent *script, size_t size);

static inline void
CollisionWorldNode_getAABB3D(CollisionWorldNode *node, AABB3D *ret);

static inline bool32
PhysicsComponent_isStatic(PhysicsComponent *c);

static inline bool32
PhysicsComponent_isActive(PhysicsComponent *c);

static inline void
PhysicsComponent_setStaticity(PhysicsComponent *c, bool32 val);

static inline void
PhysicsComponent_setActivity(PhysicsComponent *c, bool32 val);

BoxCollider *
PhysicsComponent_attachBoxCollider(PhysicsComponent *component,
    Entity *entity,
    const char *identifier);

BoxCollider *
PhysicsComponent_getBoxCollider(PhysicsComponent *component,
    const char *identifier);

BoxCollider *
PhysicsComponent_getBoxColliderByHash(PhysicsComponent *component,
    uint32_t hash);

SphereCollider *
PhysicsComponent_attachSphereCollider(PhysicsComponent *component,
    Entity *entity,
    uint32_t identifier);

SphereCollider *
PhysicsComponent_attachSphereColliderByName(PhysicsComponent *component,
    Entity *entity,
    const char *identifier);

SphereCollider *
PhysicsComponent_getSphereCollider(PhysicsComponent *component,
    const char *identifier);

SphereCollider *
PhysicsComponent_getSphereColliderByHash(PhysicsComponent *component,
    uint32_t hash);

void
PhysicsComponent_applyImpactForce(PhysicsComponent *component, vec3 direction, float force);

RenderModel *
ModelComponent_attachRenderModel(ModelComponent *component,
    Scene3D *scene, uint32_t id);

RenderModel *
ModelComponent_attachNamedRenderModel(ModelComponent *component,
    Scene3D *scene, const char *name);

Animator *
ModelComponent_attachAnimator(ModelComponent *component,
    Scene3D *scene, RenderModel *rendermodel, uint32_t id);

Animator *
ModelComponent_attachNamedAnimator(ModelComponent *component,
    Scene3D *scene, RenderModel *rendermodel, const char *name);

static inline RenderModel *
ModelComponent_getRenderModelByName(ModelComponent *component, const char *name);

static inline RenderModel *
ModelComponent_getRenderModelById(ModelComponent *component, uint32_t id);

static inline Animator *
ModelComponent_getAnimatorByName(ModelComponent *component, const char *name);

static inline Animator *
ModelComponent_getAnimatorById(ModelComponent *component, uint32_t id);

static inline void
Animator_giveName(Animator *animator, const char *name);

static inline void
Animator_giveId(Animator *animator, uint32_t id);

static inline RenderModel *
ModelComponent_getDefaultRenderModel(ModelComponent *component);

static inline void
DirectionalLightComponent_setDiffuse(DirectionalLightComponent *light,
    float r, float g, float b);

static inline void
DirectionalLightComponent_setSpecular(DirectionalLightComponent *light,
    float r, float g, float b);

static inline void
DirectionalLightComponent_setAmbient(DirectionalLightComponent *light,
    float r, float g, float b);

static inline void
DirectionalLightComponent_setDirection(DirectionalLightComponent *light,
    float x, float y, float z, bool32 normalize);

static inline void
PointLightComponent_setDiffuse(PointLightComponent *light,
    float r, float g, float b);

static inline void
PointLightComponent_setSpecular(PointLightComponent *light,
    float r, float g, float b);

static inline void
PointLightComponent_setAmbient(PointLightComponent *light,
    float r, float g, float b);

static inline Entity *
ReplicationTable_get(ReplicationTable *table, uint32_t replication_id);

static inline ReplicationTableEntry *
ReplicationTable_getEntry(ReplicationTable *table, uint32_t replication_id);

int
Scene3D_init(Scene3D *scene,
    void *memory, size_t memory_size,
    uint max_entities,
    bool32 is_multiplayer);

void
Scene3D_clear(Scene3D *scene);

int
Scene3D_buildCollisionWorldFromModel(Scene3D *scene, Model *model, vec3 offset,
    uint tree_depth, uint max_num_static_box_colliders);

int
Scene3D_addStaticBoxCollidersToWorld(Scene3D *scene, float pos_x, float pos_y, float pos_z,
    float dimension_x, float dimension_y, float dimension_z, float rotation_x, float rotation_y, float rotation_z);

float
Scene3D_castRayToStaticGeometry(Scene3D *scene, vec3 origin, vec3 direction, float lenght, vec3 ret_hitpoint, float *distance_from_origin);

float
Scene3D_castRayToStaticGeometryAndGetNormal(Scene3D *scene, vec3 origin, vec3 direction, float lenght, vec3 ret_hitpoint, float *distance_from_origin, vec3 normal);

SphereCollider *
Scene3D_castRayToSphereColliders(Scene3D *scene, vec3 origin, vec3 endpoint, float *distance_from_origin, uint32_t *ignored_ids, uint num_ignored_ids);

static inline Entity *
Scene3D_getReplicatedEntity(Scene3D *scene, uint32_t replication_id);

/* Passing 0 as the viewport parameter turns viewport overriding off,
 * making the renderer use the full window size as the viewport instead. */
void
Scene3D_setRenderViewport(Scene3D *scene, Viewport viewport);

static inline void
Scene3D_setBackgroundColor(Scene3D *scene, float r, float g, float b, float a);

static inline void
Scene3D_setAmbientColor(Scene3D *scene, float r, float g, float b, float strength);

static inline void
Scene3D_setGravity(Scene3D *scene, vec3 gravity_direction, float gravity_strength);

static inline Entity *
Scene3D_getEntityByIndex(Scene3D *scene, uint32_t index);

Entity *
Scene3D_createEntity(Scene3D *scene, float x, float y, float z);

Entity *
Scene3D_createReplicatedServerEntity(Scene3D *scene, entity_type16_t type,
    float x, float y, float z);

void
Scene3D_removeEntity(Scene3D *scene, Entity *entity);

void
Scene3D_setSkyboxTexture(Scene3D *scene, Texture *texture);

void
Scene3D_setSkyboxScale(Scene3D *scene, float scale_x, float scale_y, float scale_z);

void
Scene3D_updateSimulation(Scene3D *scene, double dt);

void
Scene3D_updatePhysics(Scene3D *s, double dt);

void
Scene3D_updateCollisions(Scene3D *s);

void
Scene3D_updateScripts(Scene3D *s);

void
Scene3D_updateAnimators(Scene3D *s, double dt);

void
Scene3D_updateLightPositions(Scene3D *s, double dt);

void
Scene3D_updateModels(Scene3D *s);

void
Scene3D_updateLifetimes(Scene3D *s, double dt);

void
Scene3D_removeEntityInner(Scene3D *s, Entity *ep);

void
Scene3D_updateServerSimulation(Scene3D *scene, Server *server, double dt);

void
Scene3D_updateClientSimulation(Scene3D *scene, Client *client, double dt);

static inline void
Scene3D_updateStaticCollisionsForSingleEntity(Scene3D *s, Entity *e, double dt);

static inline void
Scene3D_setDebugCollisionOctreeRendering(Scene3D *scene, bool32 option);

static inline void
Scene3D_setDebugCollisionGeometryRendering(Scene3D *scene, bool32 option);

static inline void
Scene3D_rotateSkybox(Scene3D *scene, float x_angle, float y_angle, float z_angle);

static inline uint
Scene3D_getNumReplicatedEntities(Scene3D *scene);

static inline int
Scene3D_willPhysicsUpdate(Scene3D *s, double dt);
/* Check if the physics will run after dt time is added to the physics timer
 * and return the amount of rounds that will be run */

void
updatePhysicsForEntityRange(void *args);

void
doStaticCollisionForEntityRange(void *range_struct_ptr);

int
AnimatorPool_init(AnimatorPool *animatorpool, Animator *memory, uint capacity);

void
Animator_playAnimation(Animator *animator, ModelAnimation *animation, bool32 loop);

static inline void
BoxCollider_setDimensions(BoxCollider *collider, Entity *entity,
    float x, float y, float z);

static inline void
BoxCollider_setAxisLock(BoxCollider *collider, bool32 x, bool32 y, bool32 z);

static inline void
BoxCollider_makeSimpleTrigger(BoxCollider *c);

static inline void
SphereCollider_makeSimpleTrigger(SphereCollider *c);

static inline void
SphereCollider_setAxisLock(SphereCollider *collider, bool32 x, bool32 y, bool32 z);

void
RenderModelPool_init(RenderModelPool *pool, void *memory, uint capacity);

static inline RenderModel *
RenderModelPool_reserve(RenderModelPool *pool);

static inline void
RenderModelPool_free(RenderModelPool *pool, RenderModel *mesh);

static inline void
RenderModel_giveName(RenderModel *model, const char *name);

static inline void
RenderModel_giveId(RenderModel *model, uint32_t id);

static inline Animator *
AnimatorPool_reserve(AnimatorPool *pool);

static inline void
AnimatorPool_free(AnimatorPool *pool, Animator *animator);



/* Inlines */
static inline Entity *
EntityPool_getEntityByIndex(EntityPool *pool, uint32_t index)
{
    if (index >= pool->num_max_entities)
    {
        DEBUG_PRINTF("Warning: attempted to get entity of index outside of pool limits (%u).\n",
            index);
        return 0;
    }
    return &pool->entities[index];
}

static inline void
CollisionWorldNode_getAABB3D(CollisionWorldNode *node, AABB3D *aabb)
{
    for (int i = 0; i < 3; ++i)
        aabb->min[i] = node->center[i] - node->dimensions;
    for (int i = 0; i < 3; ++i)
        aabb->max[i] = node->center[i] + node->dimensions;
}

static inline Entity *
ReplicationTable_get(ReplicationTable *table, uint32_t replication_id)
{
    uint index = replication_id % table->size;

    ReplicationTableEntry *entry = table->lookup_table[index];

    while (entry)
    {
        if (entry->entity->replication_id == replication_id)
            return entry->entity;

        entry = entry->lookup_next;
    }

    return 0;
}

static inline ReplicationTableEntry *
ReplicationTable_getEntry(ReplicationTable *table, uint32_t replication_id)
{
    uint index = replication_id % table->size;

    ReplicationTableEntry *entry = table->lookup_table[index];

    while (entry)
    {
        if (entry->entity->replication_id == replication_id)
            return entry;

        entry = entry->lookup_next;
    }

    return 0;
}

static inline Entity *
Scene3D_getReplicatedEntity(Scene3D *scene, uint32_t replication_id)
{
#if _CF_NETWORKING
    ASSERT(scene->is_multiplayer);
    if (replication_id == ENTITY_NOT_REPLICATED) return 0;
    return ReplicationTable_get(&scene->replication_table, replication_id);
#else
    return 0;
#endif
}

static inline void
Scene3D_setBackgroundColor(Scene3D *scene, float r, float g, float b, float a)
{
    scene->bg_color[0] = r;
    scene->bg_color[1] = g;
    scene->bg_color[2] = b;
    scene->bg_color[3] = a;
}

static inline void
Scene3D_setAmbientColor(Scene3D *scene, float r, float g, float b, float strength)
{
    scene->ambient_light_color[0] = r;
    scene->ambient_light_color[1] = g;
    scene->ambient_light_color[2] = b;
    scene->ambient_light_color[3] = strength;
}

static inline void
Scene3D_setGravity(Scene3D *scene, vec3 gravity_direction, float gravity_strength)
{
    vec3_copy(scene->physics_limits.gravity_direction, gravity_direction);
    scene->physics_limits.gravity_strength = gravity_strength;
}
static inline Entity *
Scene3D_getEntityByIndex(Scene3D *scene, uint32_t index)
{
    if (index >= scene->entities.num_max_entities)
        return 0;

    Entity *e = &scene->entities.entities[index];

    if (GET_BITFLAG(e->flags, char, ENTITY_ALIVE))
        return e;
    else
        return 0;
}

static inline void
Scene3D_updateStaticCollisionsForSingleEntity(Scene3D *s, Entity *e, double dt)
{
    SceneWorkRange rs;
    rs.scene    = s;
    rs.dt       = dt;
    rs.begin    = e->components[PHYSICS_COMPONENT_INDEX];
    rs.end      = rs.begin + 1;
    doStaticCollisionForEntityRange(&rs);
}

static inline void
Scene3D_updatePhysicsForSingleEntity(Scene3D *s, Entity *e, double dt)
{
    SceneWorkRange rs;
    rs.scene    = s;
    rs.dt       = dt;
    rs.begin    = e->components[PHYSICS_COMPONENT_INDEX];
    rs.end      = rs.begin + 1;
    updatePhysicsForEntityRange(&rs);
}

static inline void
Scene3D_setDebugCollisionOctreeRendering(Scene3D *scene, bool32 option)
{
    scene->render_collision_octree = option;
}

static inline void
Scene3D_setDebugCollisionGeometryRendering(Scene3D *scene, bool32 option)
{
    scene->render_collision_geometry = option;
}

static inline void
Scene3D_rotateSkybox(Scene3D *scene, float x_angle, float y_angle, float z_angle)
{
    mat4x4_identity(scene->skybox_rotation);
    mat4x4_rotate(scene->skybox_rotation, scene->skybox_rotation, 1.0f, 0.0f, 0.0f, x_angle);
    mat4x4_rotate(scene->skybox_rotation, scene->skybox_rotation, 0.0f, 1.0f, 0.0f, y_angle);
    mat4x4_rotate(scene->skybox_rotation, scene->skybox_rotation, 0.0f, 0.0f, 1.0f, z_angle);
}

static inline uint
Scene3D_getNumReplicatedEntities(Scene3D *scene)
{
    return scene->replication_table.num_reserved;
}

static inline int
Scene3D_willPhysicsUpdate(Scene3D *s, double dt)
{
#if _CF_FIXED_PHYSICS_TIMESTEP
    if (s->physics_step > 0)
    {
        double next_ptimer = s->physics_timer + dt;
        return (int)(next_ptimer / s->physics_step);
    }
    return 0;
#else
    return 1;
#endif
}

static inline bool32
Entity_hasComponent(Entity *entity, enum ComponentBit component)
{
    return (entity->component_flags & component) == component;
}

static inline EntityTransform *
Entity_getTransform(Entity *entity)
{
    return &entity->scene->entities.transforms[entity->transform_index];
}

static inline void
Entity_setScale(Entity *entity, float x, float  y, float z)
{
    EntityTransform *et = Entity_getTransform(entity);
    et->transform.scale[0] = x;
    et->transform.scale[1] = y;
    et->transform.scale[2] = z;
}

static inline void
Entity_getForwardVector(Entity *entity, vec3 ret)
{
    Transform *t = &Entity_getTransform(entity)->transform;
    Transform_getForwardVector(t, ret);
}

static inline void
Entity_getNonNormalizedForwardVector(Entity *entity, vec3 ret)
{
    Transform *t = &Entity_getTransform(entity)->transform;
    Transform_getNonNormalizedForwardVector(t, ret);
}

static inline void
Entity_setLifetime(Entity *entity, double lifetime)
{
    ASSERT(entity->scene->entities.num_lifetimes < entity->scene->entities.num_max_entities);

    entity->scene->entities.lifetimes[entity->scene->entities.num_lifetimes].timeleft = lifetime;
    entity->scene->entities.lifetimes[entity->scene->entities.num_lifetimes].entity = entity;
    entity->scene->entities.lifetimes[entity->scene->entities.num_lifetimes].id = entity->id;
    entity->scene->entities.num_lifetimes += 1;
}

static inline bool32
Entity_isReplicated(Entity *entity)
{
#if _CF_NETWORKING
    return entity->replication_id != ENTITY_NOT_REPLICATED;
#else
    return 0;
#endif

}

static inline void
PhysicsComponent_setCollisionCallback(PhysicsComponent *component, 
    void (*collisionCallback)(PhysicsComponent*, PhysicsComponent*, 
    Entity*, Entity*, enum ColliderType, enum ColliderType, 
    enum CollisionEventType, uint32_t, uint32_t))
{
    component->collisionCallback = collisionCallback;
}

static inline void
PhysicsComponent_setStaticCollisionCallback(PhysicsComponent *component,
    void (*staticCollisionCallback)(PhysicsComponent *component,
    Entity *entity,
    enum ColliderType collider, uint32_t collider_id,
    vec3 normal, enum CollisionEventType event_type))
{
    component->staticCollisionCallback = staticCollisionCallback;
}

static inline PhysicsComponent *
Entity_getPhysicsComponent(Entity *entity)
{
    if (Entity_hasComponent(entity, PHYSICS_COMPONENT_BIT))
    {
        uint32_t index = entity->components[PHYSICS_COMPONENT_INDEX];
        return &entity->scene->entities.physics.components[index];
    }
    else
    {
        return 0;
    }
}

static inline ModelComponent *
Entity_getModelComponent(Entity *entity)
{
    if (Entity_hasComponent(entity, MODEL_COMPONENT_BIT))
    {
        uint32_t index = entity->components[MODEL_COMPONENT_INDEX];
        return &entity->scene->entities.models.components[index];
    }
    else
        return 0;
}

static inline DirectionalLightComponent *
Entity_getDirectionalLightComponent(Entity *entity)
{
    if (Entity_hasComponent(entity, DIRECTIONAL_LIGHT_COMPONENT_BIT))
    {
        uint32_t index = entity->components[DIRECTIONAL_LIGHT_COMPONENT_INDEX];
        return &entity->scene->entities.lights.components[index];
    }
    return 0;
}

static inline PointLightComponent *
Entity_getPointLightComponent(Entity *entity)
{
    if (Entity_hasComponent(entity, POINT_LIGHT_COMPONENT_BIT))
    {
        uint32_t index = entity->components[POINT_LIGHT_COMPONENT_INDEX];
        return &entity->scene->entities.point_lights.components[index];
    }
    return 0;
}

static inline ScriptComponent *
Entity_getScriptComponent(Entity *entity)
{
    if (Entity_hasComponent(entity, SCRIPT_COMPONENT_BIT))
    {
        uint32_t index = entity->components[SCRIPT_COMPONENT_INDEX];
        return &entity->scene->entities.scripts.components[index];
    }
    return 0;
}

static inline void
Entity_setReplicationFlags(Entity *e, char flags)
{
    if (e->replication_id == ENTITY_NOT_REPLICATED) return;
    ReplicationTableEntry *entry = ReplicationTable_getEntry(
        &e->scene->replication_table, e->replication_id);
    entry->flags = flags;
}

static inline void
EntityTransform_setPosition(EntityTransform *et, float x, float y, float z)
{
    et->transform.pos[0] = x;
    et->transform.pos[1] = y;
    et->transform.pos[2] = z;
}

static inline bool32
PhysicsComponent_isStatic(PhysicsComponent *c)
{
    return (c->flags & (char)PHYSICS_COMPONENT_FLAG_IS_STATIC) == \
        (char)PHYSICS_COMPONENT_FLAG_IS_STATIC;
}

static inline bool32
PhysicsComponent_isActive(PhysicsComponent *c)
{
    return (c->flags & (char)PHYSICS_COMPONENT_FLAG_IS_ACTIVE) == \
        (char)PHYSICS_COMPONENT_FLAG_IS_ACTIVE;
}

static inline void
PhysicsComponent_setStaticity(PhysicsComponent *c, bool32 val)
{
    if (val)
        c->flags = c->flags | (char)PHYSICS_COMPONENT_FLAG_IS_STATIC;
    else
        c->flags = c->flags & ~(char)PHYSICS_COMPONENT_FLAG_IS_STATIC;
}

static inline void
PhysicsComponent_setActivity(PhysicsComponent *c, bool32 val)
{
    if (val)
        c->flags = c->flags | (char)PHYSICS_COMPONENT_FLAG_IS_ACTIVE;
    else
        c->flags = c->flags & ~(char)PHYSICS_COMPONENT_FLAG_IS_ACTIVE;
}

static inline Animator *
ModelComponent_getAnimatorByName(ModelComponent *component, const char *name)
{
    uint32_t hash = hashFromString(name);

    for (Animator *a = component->animators; a; a = a->parent_next)
        if (a->id == hash)
            return a;

    return 0;
}

static inline Animator *
ModelComponent_getAnimatorById(ModelComponent *component, uint32_t id)
{
    for (Animator *a = component->animators; a; a = a->parent_next)
        if (a->id == id)
            return a;

    return 0;
}

static inline RenderModel *
ModelComponent_getRenderModelByName(ModelComponent *component, const char *name)
{
    uint32_t hash = hashFromString(name);

    for (RenderModel *rm = component->render_models; rm; rm = rm->parent_next)
        if (rm->id == hash)
            return rm;

    return 0;
}

static inline RenderModel *
ModelComponent_getRenderModelById(ModelComponent *component, uint32_t id)
{
    for (RenderModel *rm = component->render_models; rm; rm = rm->parent_next)
        if (rm->id == id)
            return rm;

    return 0;
}

static inline RenderModel *
ModelComponent_getDefaultRenderModel(ModelComponent *component)
{
    return component->render_models;
}

static inline void
DirectionalLightComponent_setDiffuse(DirectionalLightComponent *light, float r, float g, float b)
{
    light->diffuse[0] = r;
    light->diffuse[1] = g;
    light->diffuse[2] = b;
}

static inline void
DirectionalLightComponent_setSpecular(DirectionalLightComponent *light, float r, float g, float b)
{
    light->specular[0] = r;
    light->specular[1] = g;
    light->specular[2] = b;
}

static inline void
DirectionalLightComponent_setAmbient(DirectionalLightComponent *light, float r, float g, float b)
{
    light->ambient[0] = r;
    light->ambient[1] = g;
    light->ambient[2] = b;
}

static inline void
DirectionalLightComponent_setDirection(DirectionalLightComponent *light, float x, float y, float z, bool32 normalize)
{
    light->direction[0] = x;
    light->direction[1] = y;
    light->direction[2] = z;
    if (normalize) vec3_norm(light->direction, light->direction);
}

static inline void
PointLightComponent_setDiffuse(PointLightComponent *light, float r, float g, float b)
{
    light->diffuse[0] = r;
    light->diffuse[1] = g;
    light->diffuse[2] = b;
}

static inline void
PointLightComponent_setSpecular(PointLightComponent *light, float r, float g, float b)
{
    light->specular[0] = r;
    light->specular[1] = g;
    light->specular[2] = b;
}

static inline void
PointLightComponent_setAmbient(PointLightComponent *light, float r, float g, float b)
{
    light->ambient[0] = r;
    light->ambient[1] = g;
    light->ambient[2] = b;
}

static inline void
BoxCollider_setDimensions(BoxCollider *collider, Entity *entity,
    float x, float y, float z)
{
    EntityTransform *et = Entity_getTransform(entity);
    collider->dimensions[0] = x * et->transform.scale[0];
    collider->dimensions[1] = y * et->transform.scale[1];
    collider->dimensions[2] = z * et->transform.scale[2];
}

static inline void
BoxCollider_setAxisLock(BoxCollider *collider, bool32 x, bool32 y, bool32 z)
{
	collider->flags = 0;
	if (x) SET_BITFLAG_ON(collider->flags, char, COLLIDER_AXIS_LOCK_X_BIT);
	if (y) SET_BITFLAG_ON(collider->flags, char, COLLIDER_AXIS_LOCK_Y_BIT);
	if (z) SET_BITFLAG_ON(collider->flags, char, COLLIDER_AXIS_LOCK_Z_BIT);
}

static inline void
SphereCollider_setAxisLock(SphereCollider *collider, bool32 x, bool32 y, bool32 z)
{
    collider->flags = 0;
    if (x) SET_BITFLAG_ON(collider->flags, char, COLLIDER_AXIS_LOCK_X_BIT);
    if (y) SET_BITFLAG_ON(collider->flags, char, COLLIDER_AXIS_LOCK_Y_BIT);
    if (z) SET_BITFLAG_ON(collider->flags, char, COLLIDER_AXIS_LOCK_Z_BIT);
}

static inline void
BoxCollider_makeSimpleTrigger(BoxCollider *c)
{
    if (c)
        SET_BITFLAG_ON(c->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY);
    else
        DEBUG_PRINTF("Attempted to make box collider a simple trigger, but "
            "given box was null!\n");
}

static inline void
SphereCollider_makeSimpleTrigger(SphereCollider *c)
{
    if (c)
        SET_BITFLAG_ON(c->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY);
    else
        DEBUG_PRINTF("Attempted to make sphere collider a simple trigger, but "
            "given sphere was null!\n");
}

static inline void
RenderModel_giveName(RenderModel *model, const char *name)
{
    model->id = hashFromString(name);
}

static inline void
RenderModel_giveId(RenderModel *model, uint32_t id)
{
    model->id = id;
}

static inline void
Animator_giveName(Animator *animator, const char *name)
{
    animator->id = hashFromString(name);
}

static inline void
Animator_giveId(Animator *animator, uint32_t id)
{
    animator->id = id;
}

static inline RenderModel *
RenderModelPool_reserve(RenderModelPool *pool)
{
    RenderModel *model = pool->free;
    if (!model) return model;

    pool->free = model->pool_next;
    if (pool->free) pool->free->pool_prev = 0;

    /* Initial empty values */
    model->model                    = 0;
    model->material                 = 0;
    model->material_array           = 0;
    model->shader                   = SHADER3D_OPAQUE_GOURAUD;
    model->id                       = 0;
    model->mode                     = RENDER_MODEL_NORMAL;
    model->outline_normal_offset    = 0.03f;

    memset(&model->relative_transform, 0, sizeof(Transform));
    model->relative_transform.scale[0] = 1.0f;
    model->relative_transform.scale[1] = 1.0f;
    model->relative_transform.scale[2] = 1.0f;

    if (pool->reserved)
        pool->reserved->pool_prev = model;

    model->pool_next    = pool->reserved;
    pool->reserved      = model;
    model->pool_prev    = 0;

    model->parent_next = 0;

    return model;
}

static inline void
RenderModelPool_free(RenderModelPool *pool, RenderModel *model)
{
    if (model->pool_prev)
        model->pool_prev->pool_next = model->pool_next;
    else
        pool->reserved = model->pool_next;

    if (model->pool_next)
        model->pool_next->pool_prev = model->pool_prev;

    if (pool->free)
        pool->free->pool_prev = model;

    model->pool_next    = pool->free;
    model->pool_prev    = 0;
    pool->free          = model;

    /* Note: do not set parent_next to anything here! It will mess up
     * removal of multiple models from an entity. */
}

static inline Animator *
AnimatorPool_reserve(AnimatorPool *pool)
{
    Animator *animator = pool->free;
    if (!animator) return 0;

    pool->free = animator->pool_next;
    if (pool->free) pool->free->pool_prev = 0;

    /* Initial empty values */
    animator->model_animation = 0;

    if (pool->reserved)
        pool->reserved->pool_prev = animator;

    animator->pool_next = pool->reserved;
    pool->reserved = animator;
    animator->pool_prev = 0;

    return animator;
}

static inline void
AnimatorPool_free(AnimatorPool *pool, Animator *animator)
{
    if (animator->pool_prev)
        animator->pool_prev->pool_next = animator->pool_next;
    else
        pool->reserved = animator->pool_next;

    if (animator->pool_next)
        animator->pool_next->pool_prev = animator->pool_prev;

    if (pool->free)
        pool->free->pool_prev = animator;

    animator->pool_next = pool->free;
    animator->pool_prev = 0;
    pool->free = animator;
}
