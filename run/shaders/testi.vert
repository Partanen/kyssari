#version 330 core

#define NUM_MAX_DIRECTIONAL_LIGHTS 2
#define NUM_MAX_POINT_LIGHTS 4

struct Material
{
    sampler2D   diffuse;
    sampler2D   specular;
    float       shine;
    vec3        color;
};

layout (std140) uniform Shader3DSharedMatrices
{
    mat4 projection;
    mat4 look_at;
    vec3 camera_pos;
};

layout (std140) uniform Shader3DDirectionalLights
{
    vec3 dir_ambient[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_direction[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_diffuse[NUM_MAX_DIRECTIONAL_LIGHTS];
    vec3 dir_specular[NUM_MAX_DIRECTIONAL_LIGHTS];
    int num_directional_lights;
};

layout (std140) uniform Shader3DPointLights
{
    vec3 pnt_position[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_diffuse[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_specular[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_ambient[NUM_MAX_POINT_LIGHTS];
    vec3 pnt_constant_linear_quadratic[NUM_MAX_POINT_LIGHTS];
    int num_point_lights;
};

layout (std140) uniform Shader3DSceneAmbientLighting
{
    vec4 scene_ambient_light;
};

uniform mat4        model;
uniform Material    material;

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 in_tex_coord;
layout (location = 2) in vec3 in_normal;

out vec3 frag_pos;
out vec2 tex_coord;

out vec3 combined_diffuse;
out vec3 combined_ambient;

vec3 normal;

void
calculateDirectionalLight(vec3 p_ambient, vec3 p_direction,
    vec3 p_diffuse, vec3 p_specular, vec3 view_direction)
{
    /* Diffuse lighting */
    float diffuse_strength = max(dot(normal, normalize(-p_direction)), 0.0f);
    vec3 diffuse = diffuse_strength * p_diffuse;

    /* Ambient lighing */
    vec3 ambient = p_ambient;

    combined_diffuse += diffuse;
    combined_ambient += ambient;
}

void
calculatePointLight(vec3 p_position, vec3 p_diffuse, vec3 p_specular,
    vec3 p_ambient, vec3 p_constant_linear_quadratic, vec3 view_direction)
{
    vec3 light_direction    = normalize(p_position - frag_pos);
    vec3 reflect_direction  = reflect(-light_direction, normal);
    float distance          = length(p_position - frag_pos);
    float attenuation = clamp(1.0f / (p_constant_linear_quadratic.x +
                                p_constant_linear_quadratic.y * distance +
                                p_constant_linear_quadratic.z * distance * distance), 0, 1);

    /* Diffuse lighting */
    float diffuse_strength = max(dot(normal, -light_direction), 0.0f);
    vec3 diffuse = diffuse_strength * p_diffuse;

    /* Ambient lighting */
    vec3 ambient = p_ambient;

    combined_diffuse += diffuse;
    combined_ambient += ambient;
}

void main(void)
{
    frag_pos = vec3(model * vec4(pos, 1.0f));
    tex_coord = in_tex_coord;

    normal = normalize(mat3(transpose(inverse(model))) * in_normal);
    vec3 view_direction = normalize(camera_pos - frag_pos);

    combined_diffuse = vec3(0.0f, 0.0f, 0.0f);
    combined_ambient = vec3(0.0f, 0.0f, 0.0f);

    for (int i = 0; i < NUM_MAX_DIRECTIONAL_LIGHTS; ++i)
    {
        calculateDirectionalLight(dir_ambient[i], dir_direction[i],
            dir_diffuse[i], dir_specular[i], view_direction);
    }

    vec3 scene_ambient =scene_ambient_light.w \
        * vec3(
        scene_ambient_light.x,
        scene_ambient_light.y,
        scene_ambient_light.z);

    combined_ambient += scene_ambient;

    gl_Position = projection * look_at * model * vec4(pos, 1.0f);
}
