#include "collision.h"

bool32
testOBB3DOBB3D(OBB3D *obb_a, OBB3D *obb_b)
{
    float   r, ra, rb;
    mat3x3  mat_rot, mat_abs_rot;
    vec3    t, tv;
    /* Rotation matrix expressing b in a's coordinate frame */
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
           mat_rot[i][j] = vec3_mul_inner(obb_a->rotation[i], obb_b->rotation[j]);

    /* Translation vector */
    {
        vec3_sub(t, obb_b->center, obb_a->center);
        /* tv: t relative to obb_a */
        tv[0] = vec3_mul_inner(t, obb_a->rotation[0]);
        tv[1] = vec3_mul_inner(t, obb_a->rotation[1]);
        tv[2] = vec3_mul_inner(t, obb_a->rotation[2]);
    }

    /* Common subexpressions */
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            mat_abs_rot[i][j] = ABS(mat_rot[i][j]) + FLT_EPSILON;

    /* Test axes L = A0, L = A1, L = A2 */
    for (int i = 0; i < 3; ++i)
    {
        r = tv[i];
        ra = obb_a->dimensions[i];
        rb = obb_b->dimensions[0] * mat_abs_rot[i][0] + obb_b->dimensions[1] * \
            mat_abs_rot[i][1] + obb_b->dimensions[2] * mat_abs_rot[i][2];

        if (ABS(r) > ra + rb)
        {
            return 0;
        }
    }

    /* Test axes L = B0, L = B1, L = B2 */
    for (int i = 0; i < 3; ++i)
    {
        r  = tv[0] * mat_rot[0][i] + tv[1] * mat_rot[1][i] + tv[2] * mat_rot[2][i];
        ra = obb_a->dimensions[0] * mat_abs_rot[0][i] + obb_a->dimensions[1] * \
            mat_abs_rot[1][i] + obb_a->dimensions[2] * mat_abs_rot[2][i];
        rb = obb_b->dimensions[i];

        if (ABS(r) > ra + rb)
        {
            return 0;
        }
    }

    /* Test axis L = A0 x B0 */
    r  = tv[2] * mat_rot[1][0] - tv[1] * mat_rot[2][0];
    ra = obb_a->dimensions[1] * mat_abs_rot[2][0] + obb_a->dimensions[2] * mat_abs_rot[1][0];
    rb = obb_b->dimensions[1] * mat_abs_rot[0][2] + obb_b->dimensions[2] * mat_abs_rot[0][1];

    if (r > ra + rb)
    {
        return 0;
    }

    /* Test axis L = A0 x B1 */
    r  = tv[2] * mat_rot[1][1] - tv[1] * mat_rot[2][1];
    ra = obb_a->dimensions[1] * mat_abs_rot[2][1] + obb_a->dimensions[2] * mat_abs_rot[1][1];
    rb = obb_b->dimensions[0] * mat_abs_rot[0][2] + obb_b->dimensions[2] * mat_abs_rot[0][0];

    if (r > ra + rb)
    {
        return 0;
    }

    /* Test axis L = A0 x B2 */
    r  = tv[2] * mat_rot[1][2] - tv[1] * mat_rot[2][2];
    ra = obb_a->dimensions[1] * mat_abs_rot[2][2] + obb_a->dimensions[2] * mat_abs_rot[1][2];
    rb = obb_b->dimensions[0] * mat_abs_rot[0][1] + obb_b->dimensions[1] * mat_abs_rot[0][0];

    if (r > ra + rb)
    {
        return 0;
    }

    /* Test axis L = A1 * B0 */
    r  = tv[0] * mat_rot[2][0] - tv[2] * mat_rot[0][0];
    ra = obb_a->dimensions[0] * mat_abs_rot[2][0] + obb_a->dimensions[2] * mat_abs_rot[0][0];
    rb = obb_b->dimensions[1] * mat_abs_rot[1][2] + obb_b->dimensions[2] * mat_abs_rot[1][1];

    if (r > ra + rb)
    {
        return 0;
    }

    /* Test axis L = A1 x B1 */
    r  = tv[0] * mat_rot[2][1] - tv[2] * mat_rot[0][1];
    ra = obb_a->dimensions[0] * mat_abs_rot[2][1] + obb_a->dimensions[2] * mat_abs_rot[0][1];
    rb = obb_b->dimensions[0] * mat_abs_rot[1][2] + obb_b->dimensions[2] * mat_abs_rot[1][0];

    if (r > ra + rb)
    {
        return 0;
    }

    /* Test axis L = A1 x B2 */
    r  = tv[0] * mat_rot[2][2] - tv[2] * mat_rot[0][2];
    ra = obb_a->dimensions[0] * mat_abs_rot[2][2] + obb_a->dimensions[2] * mat_abs_rot[0][2];
    rb = obb_b->dimensions[0] * mat_abs_rot[1][1] + obb_b->dimensions[1] * mat_abs_rot[1][0];

    if (r > ra + rb)
    {
        return 0;
    }

    /* Test axis L = A2 x B0 */
    r  = tv[1] * mat_rot[0][0] - tv[0] * mat_rot[1][0];
    ra = obb_a->dimensions[0] * mat_abs_rot[1][0] + obb_a->dimensions[1] * mat_abs_rot[0][0];
    rb = obb_b->dimensions[1] * mat_abs_rot[2][2] + obb_b->dimensions[2] * mat_abs_rot[2][1];

    if (r > ra + rb)
    {
        return 0;
    }

    /* Test axis L = A2 x B1 */
    r  = tv[1] * mat_rot[0][1] - tv[0] * mat_rot[1][1];
    ra = obb_a->dimensions[0] * mat_abs_rot[1][1] + obb_a->dimensions[1] * mat_abs_rot[0][1];
    rb = obb_b->dimensions[0] * mat_abs_rot[2][2] + obb_b->dimensions[2] * mat_abs_rot[2][0];

    if (r > ra + rb)
    {
        return 0;
    }

    /* Test axis L = A2 x B2 */
    r  = tv[1] * mat_rot[0][2] - tv[0] * mat_rot[1][2];
    ra = obb_a->dimensions[0] * mat_abs_rot[1][2] + obb_a->dimensions[1] * mat_abs_rot[0][2];
    rb = obb_b->dimensions[0] * mat_abs_rot[2][1] + obb_b->dimensions[1] * mat_abs_rot[2][0];

    if (r > ra + rb)
    {
        return 0;
    }

    return 1;
}

/* To give credit where its due, this is mostly based on
 * the code described in Real Time Collision Detection
 * by Christer Ericson and the box-box collision algorithms
 * in the Open Dynamics Library. */
int
testOBB3DOBB3DAndGetData(OBB3D *obb_a, OBB3D *obb_b,
    vec3 normal, float *penetration_depth)
{
    #define SEP_MUL 1.05f
    float   r, ra, rb, tmp_separation, least_separation, l;
    int     least_separating_axis = 0;
    mat3x3  mat_rot, mat_abs_rot;
    vec3    normal_c, t, tv;
    float   *normal_rot = 0;
    bool32 invert_normal = 0;

    /* Rotation matrix expressing b in a's coordinate frame */
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
           mat_rot[i][j] = vec3_mul_inner(obb_a->rotation[i], obb_b->rotation[j]);

    /* Translation vector */
    {
        vec3_sub(t, obb_b->center, obb_a->center);
        /* tv: t relative to obb_a */
        tv[0] = vec3_mul_inner(t, obb_a->rotation[0]);
        tv[1] = vec3_mul_inner(t, obb_a->rotation[1]);
        tv[2] = vec3_mul_inner(t, obb_a->rotation[2]);
    }

    /* Common subexpressions */
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            mat_abs_rot[i][j] = ABS(mat_rot[i][j]) + FLT_EPSILON;

    /* Test axes L = A0, L = A1, L = A2 */
    for (int i = 0; i < 3; ++i)
    {
        r = tv[i];
        ra = obb_a->dimensions[i];
        rb = obb_b->dimensions[0] * mat_abs_rot[i][0] + obb_b->dimensions[1] * \
            mat_abs_rot[i][1] + obb_b->dimensions[2] * mat_abs_rot[i][2];

        if (ABS(r) > ra + rb)
        {
            return 0;
        }
        else
        {
            tmp_separation = (ABS(r) - (ra + rb));

            if (tmp_separation <= 0)
            {
                if (!least_separating_axis
                || (least_separating_axis != 0 && tmp_separation > least_separation))
                {
                    least_separation        = tmp_separation;
                    *penetration_depth      = least_separation;
                    normal_rot              = obb_a->rotation[i];
                    invert_normal           = r < 0;
                    least_separating_axis   = 1 + i;
                }
            }
        }
    }

    /* Test axes L = B0, L = B1, L = B2 */
    for (int i = 0; i < 3; ++i)
    {
        r  = tv[0] * mat_rot[0][i] + tv[1] * mat_rot[1][i] + tv[2] * mat_rot[2][i];
        ra = obb_a->dimensions[0] * mat_abs_rot[0][i] + obb_a->dimensions[1] * \
            mat_abs_rot[1][i] + obb_a->dimensions[2] * mat_abs_rot[2][i];
        rb = obb_b->dimensions[i];

        if (ABS(r) > ra + rb)
        {
            return 0;
        }
        else
        {
            tmp_separation = (ABS(r) - (ra + rb));

            if (tmp_separation <= 0)
            {
                if (!least_separating_axis
                || (least_separating_axis != 0 && tmp_separation > least_separation))
                {
                    least_separation        = tmp_separation;
                    *penetration_depth      = least_separation;
                    normal_rot              = obb_b->rotation[i];
                    invert_normal           = r < 0;
                    least_separating_axis   = 4 + i;
                }
            }
        }
    }

    #define EBIN_MACRO(n1, n2, n3, axis_num) \
        tmp_separation = (ABS(r) - (ra + rb)); \
        if (tmp_separation > 0) \
        { \
            l = sqrtf(POWER_OF_2(n1) + POWER_OF_2(n2) + POWER_OF_2(n3)); \
            if (l > 0.0f) \
            { \
                tmp_separation /= l; \
                if (!least_separating_axis \
                || (least_separating_axis != 0 \
                    && SEP_MUL * tmp_separation > least_separation)) \
                { \
                    least_separation        = tmp_separation; \
                    *penetration_depth      = least_separation; \
                    normal_rot              = 0; \
                    normal_c[0]             = n1 / l; \
                    normal_c[1]             = n2 / l; \
                    normal_c[2]             = n3 / l; \
                    invert_normal           = r < 0; \
                    least_separating_axis   = axis_num; \
                } \
            } \
        }


    /* Test axis L = A0 x B0 */
    r  = tv[2] * mat_rot[1][0] - tv[1] * mat_rot[2][0];
    ra = obb_a->dimensions[1] * mat_abs_rot[2][0] + obb_a->dimensions[2] * mat_abs_rot[1][0];
    rb = obb_b->dimensions[1] * mat_abs_rot[0][2] + obb_b->dimensions[2] * mat_abs_rot[0][1];

    if (r > ra + rb)
    {
        return 0;
    }
    else
    {
        //0, -R31, R11
        EBIN_MACRO(0, -mat_rot[2][0], mat_rot[1][0], 7);
    }

    /* Test axis L = A0 x B1 */
    r  = tv[2] * mat_rot[1][1] - tv[1] * mat_rot[2][1];
    ra = obb_a->dimensions[1] * mat_abs_rot[2][1] + obb_a->dimensions[2] * mat_abs_rot[1][1];
    rb = obb_b->dimensions[0] * mat_abs_rot[0][2] + obb_b->dimensions[2] * mat_abs_rot[0][0];

    if (r > ra + rb)
    {
        return 0;
    }
    else
    {
        //0, -R32, R12
        EBIN_MACRO(0, -mat_rot[2][1], mat_rot[1][1], 8);
    }

    /* Test axis L = A0 x B2 */
    r  = tv[2] * mat_rot[1][2] - tv[1] * mat_rot[2][2];
    ra = obb_a->dimensions[1] * mat_abs_rot[2][2] + obb_a->dimensions[2] * mat_abs_rot[1][2];
    rb = obb_b->dimensions[0] * mat_abs_rot[0][1] + obb_b->dimensions[1] * mat_abs_rot[0][0];

    if (r > ra + rb)
    {
        return 0;
    }
    else
    {
        //0, -R33, R23
        EBIN_MACRO(0, -mat_rot[2][2], mat_rot[1][2], 9);
    }

    /* Test axis L = A1 * B0 */
    r  = tv[0] * mat_rot[2][0] - tv[2] * mat_rot[0][0];
    ra = obb_a->dimensions[0] * mat_abs_rot[2][0] + obb_a->dimensions[2] * mat_abs_rot[0][0];
    rb = obb_b->dimensions[1] * mat_abs_rot[1][2] + obb_b->dimensions[2] * mat_abs_rot[1][1];

    if (r > ra + rb)
    {
        return 0;
    }
    else
    {
        //R31, 0, -R11
        EBIN_MACRO(mat_rot[2][0], 0, -mat_rot[0][0], 10);
    }

    /* Test axis L = A1 x B1 */
    r  = tv[0] * mat_rot[2][1] - tv[2] * mat_rot[0][1];
    ra = obb_a->dimensions[0] * mat_abs_rot[2][1] + obb_a->dimensions[2] * mat_abs_rot[0][1];
    rb = obb_b->dimensions[0] * mat_abs_rot[1][2] + obb_b->dimensions[2] * mat_abs_rot[1][0];

    if (r > ra + rb)
    {
        return 0;
    }
    else
    {
        //R32, 0, -R12
        EBIN_MACRO(mat_rot[2][1], 0, -mat_rot[0][0], 11);
    }

    /* Test axis L = A1 x B2 */
    r  = tv[0] * mat_rot[2][2] - tv[2] * mat_rot[0][2];
    ra = obb_a->dimensions[0] * mat_abs_rot[2][2] + obb_a->dimensions[2] * mat_abs_rot[0][2];
    rb = obb_b->dimensions[0] * mat_abs_rot[1][1] + obb_b->dimensions[1] * mat_abs_rot[1][0];

    if (r > ra + rb)
    {
        return 0;
    }
    else
    {
        //R33, 0, -R13
        EBIN_MACRO(mat_rot[2][2], 0, -mat_rot[0][2], 12);
    }

    /* Test axis L = A2 x B0 */
    r  = tv[1] * mat_rot[0][0] - tv[0] * mat_rot[1][0];
    ra = obb_a->dimensions[0] * mat_abs_rot[1][0] + obb_a->dimensions[1] * mat_abs_rot[0][0];
    rb = obb_b->dimensions[1] * mat_abs_rot[2][2] + obb_b->dimensions[2] * mat_abs_rot[2][1];

    if (r > ra + rb)
    {
        return 0;
    }
    else
    {
        //-R21, R11, 0
        EBIN_MACRO(-mat_rot[0][0], mat_rot[1][0], 0, 13);
    }

    /* Test axis L = A2 x B1 */
    r  = tv[1] * mat_rot[0][1] - tv[0] * mat_rot[1][1];
    ra = obb_a->dimensions[0] * mat_abs_rot[1][1] + obb_a->dimensions[1] * mat_abs_rot[0][1];
    rb = obb_b->dimensions[0] * mat_abs_rot[2][2] + obb_b->dimensions[2] * mat_abs_rot[2][0];

    if (r > ra + rb)
    {
        return 0;

    }
    else
    {
        //-R22, R12, 0
        EBIN_MACRO(-mat_rot[0][1], mat_rot[1][1], 0, 14);
    }

    /* Test axis L = A2 x B2 */
    r  = tv[1] * mat_rot[0][2] - tv[0] * mat_rot[1][2];
    ra = obb_a->dimensions[0] * mat_abs_rot[1][2] + obb_a->dimensions[1] * mat_abs_rot[0][2];
    rb = obb_b->dimensions[0] * mat_abs_rot[2][1] + obb_b->dimensions[1] * mat_abs_rot[2][0];

    if (r > ra + rb)
    {
        return 0;
    }
    else
    {
        //-R23, R13, 0
        EBIN_MACRO(-mat_rot[0][2], mat_rot[1][2], 0, 15);
    }

    ASSERT(least_separating_axis != -1);

    if (normal_rot)
    {
        normal[0] = normal_rot[0];
        normal[1] = normal_rot[1];
        normal[2] = normal_rot[2];
    }
    else
    {
        /* Swap due to example being reversed */
        vec3_mul_mat3x3(normal, normal_c, obb_a->rotation);

        float tmp = normal[1];
        normal[1] = normal[2];
        normal[2] = tmp;
    }

    if (invert_normal)
        vec3_scale(normal, normal, -1.0f);

    return least_separating_axis;
}

/* OBB-triangle test based on Tomas Akenine-M�ller's AABB-triangle code */
bool32
testOBB3DTriangle(OBB3D *obb, vec3 ta, vec3 tb, vec3 tc, vec3 normal, float *penetration)
{
    #define TEST_AXIS_X01(a, b, fa, fb) \
        p0 = a * v0[1] - b * v0[2]; \
        p2 = a * v2[1] - b * v2[2]; \
        if (p0 < p2)    {min = p0; max = p2;} \
        else            {min = p2; max = p0;} \
        r = fa * obb->dimensions[1] + fb * obb->dimensions[2]; \
        if (min > r || max < -r) \
        { \
            return 0; \
        }

    #define TEST_AXIS_Y02(a, b, fa, fb) \
        p0 = -a * v0[0] + b * v0[2]; \
        p2 = -a * v2[0] + b * v2[2]; \
        if (p0 < p2)    {min = p0; max = p2;} \
        else            {min = p2; max = p0;} \
        r = fa * obb->dimensions[0] + fb * obb->dimensions[2]; \
        if (min > r || max < -r) \
        { \
            return 0; \
        }

    #define TEST_AXIS_Z12(a, b, fa, fb) \
        p1 = a * v1[0] - b * v1[1]; \
        p2 = a * v2[0] - b * v2[1]; \
        if (p2 < p1)    {min = p2; max = p1;} \
        else            {min = p1; max = p2;} \
        r = fa * obb->dimensions[0] + fb * obb->dimensions[1]; \
        if (min > r || max < -r) \
        { \
            return 0; \
        }

    #define TEST_AXIS_X2(a, b, fa, fb) \
        p0 = a * v0[1] - b * v0[2]; \
        p1 = a * v1[1] - b * v1[2]; \
        if (p0 < p1)    {min = p0; max = p1;} \
        else            {min = p1; max = p0;} \
        r = fa * obb->dimensions[1] + fb * obb->dimensions[2]; \
        if (min > r || max < -r) \
        { \
            return 0; \
        }

    #define TEST_AXIS_Z0(a, b, fa, fb) \
        p0 = a * v0[0] - b * v0[1]; \
        p1 = a * v1[0] - b * v1[1]; \
        if (p0 < p1)    {min = p0; max = p1;} \
        else            {min = p1; max = p0;} \
        r = fa * obb->dimensions[0] + fb * obb->dimensions[1]; \
        if (min > r || max < -r) \
        { \
            return 0; \
        }

    #define TEST_AXIS_Y1(a, b, fa, fb) \
        p0 = -a * v0[0] + b * v0[2]; \
        p1 = -a * v1[0] + b * v1[2]; \
        if (p0 < p1)    {min = p0; max = p1;} \
        else            {min = p1; max = p0;} \
        r = fa * obb->dimensions[0] + fb * obb->dimensions[2]; \
        if (min > r || max < -r) \
        { \
            return 0; \
        }

    #define FIND_MIN_MAX(x0, x1, x2, min, max) \
        max = x0; \
        min = max; \
        if (x1 < min) min = x1; \
        if (x1 > max) max = x1; \
        if (x2 < min) min = x2; \
        if (x2 > max) max = x2;

    mat3x3 rot;

    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            rot[i][j] = obb->rotation[i][j] + FLT_EPSILON;

    float p0, p1, p2, r, fex, fey, fez, min, max;

    /* Express triangle in the space of obb */
    vec3 v0, v1, v2;
    vec3_sub(v0, ta, obb->center);
    vec3_sub(v1, tb, obb->center);
    vec3_sub(v2, tc, obb->center);

    vec3_mul_mat3x3(v0, v0, rot);
    vec3_mul_mat3x3(v1, v1, rot);
    vec3_mul_mat3x3(v2, v2, rot);

    /* Triangle edge vectors */
    vec3 e0; vec3_sub(e0, v1, v0);
    vec3 e1; vec3_sub(e1, v2, v1);
    vec3 e2; vec3_sub(e2, v0, v2);

    /* Test 9 tests first */
    fex = ABS(e0[0]);
    fey = ABS(e0[1]);
    fez = ABS(e0[2]);

    TEST_AXIS_X01(e0[2], e0[1], fez, fey);
    TEST_AXIS_Y02(e0[2], e0[0], fez, fex);
    TEST_AXIS_Z12(e0[1], e0[0], fey, fex);

    fex = ABS(e1[0]);
    fey = ABS(e1[1]);
    fez = ABS(e1[2]);

    TEST_AXIS_X01(e1[2], e1[1], fez, fey);
    TEST_AXIS_Y02(e1[2], e1[0], fez, fex);
    TEST_AXIS_Z0 (e1[1], e1[0], fey, fex);


    fex = ABS(e2[0]);
    fey = ABS(e2[1]);
    fez = ABS(e2[2]);

    TEST_AXIS_X2 (e2[2], e2[1], fez, fey);
    TEST_AXIS_Y1 (e2[2], e2[0], fez, fex);
    TEST_AXIS_Z12(e2[1], e2[0], fey, fex);

    /* Test overlap in the directions x, y, z
     * Find the min and max of the traingle in each direction, then test for that direction,
     * as if we were teseting a minimal aabb around the triagnle against the obb */

    FIND_MIN_MAX(v0[0], v1[0], v2[0], min, max);
    if (min > obb->dimensions[0] || max < -obb->dimensions[0])
    {
        return 0;
    }

    FIND_MIN_MAX(v0[1], v1[1], v2[1], min, max);
    if (min > obb->dimensions[1] || max < -obb->dimensions[1])
    {
        return 0;
    }

    FIND_MIN_MAX(v0[2], v1[2], v2[2], min, max);
    if (min > obb->dimensions[2] || max < -obb->dimensions[2])
    {
        return 0;
    }

    /* Last, do a box-plane intersection test */

    vec3 rotated_normal;
    vec3_mul_mat3x3(rotated_normal, normal, rot);

    return testOBB3DPlane(obb, rotated_normal, v0);

    #undef TEST_AXIS_X01
    #undef TEST_AXIS_Y02
    #undef TEST_AXIS_Z12
    #undef TEST_AXIS_X2
    #undef TEST_AXIS_Z0
    #undef TEST_AXIS_Y1
    #undef FIND_MIN_MAX
}
