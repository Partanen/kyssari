#include "main.h"
#include "../../shared/shared.c"
#include "../../shared/packets.h"
#include "menuscreen.c"
#include "spgamescreen.c"
#include "mpgamescreen.c"
#include "loadingscreen.c"

SimQueue                sim_queue           = {0};
Game                    game                = {0};
Client                  client              = {0};
InputBuffer             input_buffer        = {0};
struct PermanentAssets  permanent_assets    = {0};
struct IngameAssets     ig_assets           = {0};
struct GameData         game_data           = {0};
struct MessageBuffer    message_buffer      = {0};
struct GraphicsConfig   graphics_config     = {0};
struct GUIWindowStyle   stat_window_style   = {0};
struct KeyConfig        key_config;

int     max_input_replays;
int     max_input_rollback;
float   position_x_limit;
float   position_y_limit;
float   position_z_limit;
int     network_tick_rate;

const char *uneducational_str = "For un-educational use only.";
const char *tips_of_the_day[] =
{
    "Press the left mouse button to shoot.",

    "Press 'forward' to move forward.",

    "Breathing helps you survive.",

    "Drugs are bad for your gaming performance,\nwith the exception of some "
        "amphetamines.",

    "You are now breathing manually.",

    "Praise our Lord and Saviour, Turo.",

    "Buying one of those expensive gaming chairs would\nsurely boost your "
        "gaming performance, goyim...",

    "If this game was made in Unity, it would be much better.",

    "Red leds make your computer faster.",

    "I want to believe.",

    "It's them.",

    "Anime isn't real.",

    "Crossdressing doesn't actually improve programmer skills.",

    "Cow milk as human nutrition is a conspiracy.",

    "How can money be real if our eyes aren't real?",

    "Braces go on the next line.",

    "This game was more or less inspired by Daikatana.",

    "NaN",

    "Weebdev best dev.",

    "Or something like that.",

    "jkljkhkjklhjklhkljhjklljlh",

    "Blue leds make your computer run cooler.",

    "Green leds make your computer more economic.",

    "Only pricks drive BMWs.",

    "I want to move to Japan where adults watch cartoons just like me.",

    "You may not like it, but this is what peak performance looks like."
};

const int NUM_TIPS_OF_THE_DAY =
    (sizeof(tips_of_the_day) / sizeof(const char *));

int CEL_SHADER_OPAQUE       = -1;
int CEL_SHADER_TRANSPARENT  = -1;

static void
_onClientUpdateCallback(Client *client);

static int
_onReceiveMessageCallback(Client *c, message_t msg_type, uint16_t msg_id,
    void *msg, int size, bool32 received_before);

static void
_cfgCallback(const char *opt, const char *val);

int
main(int argc, char **argv)
{
    DEBUG_PRINTF("%i\n", NUM_TIPS_OF_THE_DAY);
    DEBUG_PRINTF("Starting Karistys version %s...\n", KS_VERSION_STR);

    if (initGame(argc, argv) != 0)
        return 1;

    if (kys_run(argc, argv, game.screens[LOADING_SCREEN]) != 0)
        return 2;

    if (IS_CLIENT_ACTIVE())
        Client_disconnect(&client);

    return 0;
}



static void
_copyAnimation(ModelAnimation *dst, ModelAnimation *src, Material *mat)
{
    memcpy(dst, src, sizeof(ModelAnimation));
    dst->frames = allocatePermanentMemory(dst->num_frames *
        sizeof(ModelAnimationFrame));
    memcpy(dst->frames, src->frames, dst->num_frames * sizeof(ModelAnimationFrame));
    ASSERT(dst->frames);
    for (int i = 0; i < dst->num_frames; ++i)
        dst->frames[i].material = mat;
}

int
loadPooledIngameAssets()
{
    /*TEXTURES*/
    LOAD_INGAME_TEXTURE("pirate",       "assets/textures/piratetexture.png");
    LOAD_INGAME_TEXTURE("anarchist",    "assets/textures/anarchisttexture.png");
    LOAD_INGAME_TEXTURE("native",       "assets/textures/nativetexture.png");
    LOAD_INGAME_TEXTURE("officer",      "assets/textures/officertexture.png");
    LOAD_INGAME_TEXTURE("rocket launcher", "assets/textures/rocket_launcher_texture.png");
    LOAD_INGAME_TEXTURE("smg", "assets/textures/smg_texture.png");
    LOAD_INGAME_TEXTURE("smg bullet", "assets/textures/smg_bullet_texture.png");
    LOAD_INGAME_TEXTURE("railgun", "assets/textures/railgun_texture.png");
    LOAD_INGAME_TEXTURE("machete", "assets/textures/machete_texture.png");
    LOAD_INGAME_TEXTURE("hp icon", "assets/textures/hp_icon.png");
    LOAD_INGAME_TEXTURE("muzzlefire", "assets/textures/effects/smg_muzzleflash.png");
    LOAD_INGAME_TEXTURE("rocketmuzzle1", "assets/textures/effects/rocketflash1.png");
    LOAD_INGAME_TEXTURE("rocketmuzzle2", "assets/textures/effects/rocketflash2.png");
    LOAD_INGAME_TEXTURE("rocketmuzzle3", "assets/textures/effects/rocketflash3.png");
    LOAD_INGAME_TEXTURE("rocketmuzzle4", "assets/textures/effects/rocketflash4.png");
    LOAD_INGAME_TEXTURE("rocketmuzzle5", "assets/textures/effects/rocketflash5.png");
    LOAD_INGAME_TEXTURE("rocketmuzzle6", "assets/textures/effects/rocketflash6.png");
    LOAD_INGAME_TEXTURE("medkit", "assets/textures/medkit_texture.png");
    LOAD_INGAME_TEXTURE("rocket ammo", "assets/textures/rocket_ammo_texture.png");
    LOAD_INGAME_TEXTURE("smg ammo", "assets/textures/smg_ammo_texture.png");
    LOAD_INGAME_TEXTURE("railgun ammo", "assets/textures/railgun_ammo_texture.png");
    LOAD_INGAME_TEXTURE("explosion1", "assets/textures/effects/explosion1.png");
    LOAD_INGAME_TEXTURE("explosion2", "assets/textures/effects/explosion2.png");
    LOAD_INGAME_TEXTURE("explosion3", "assets/textures/effects/explosion3.png");
    LOAD_INGAME_TEXTURE("explosion4", "assets/textures/effects/explosion4.png");
    LOAD_INGAME_TEXTURE("explosion5", "assets/textures/effects/explosion5.png");
    LOAD_INGAME_TEXTURE("explosion6", "assets/textures/effects/explosion6.png");
    LOAD_INGAME_TEXTURE("explosion7", "assets/textures/effects/explosion9.png");
    LOAD_INGAME_TEXTURE("explosion8", "assets/textures/effects/explosion12.png");
    LOAD_INGAME_TEXTURE("explosion9", "assets/textures/effects/explosion15.png");
    LOAD_INGAME_TEXTURE("explosion10", "assets/textures/effects/explosion18.png");
    LOAD_INGAME_TEXTURE("bullethole", "assets/textures/effects/bullethole.png");
    LOAD_INGAME_TEXTURE("railgun tracer1", "assets/textures/effects/blue_spark1.png");
    LOAD_INGAME_TEXTURE("railgun tracer2", "assets/textures/effects/blue_spark2.png");
    LOAD_INGAME_TEXTURE("railgun tracer3", "assets/textures/effects/blue_spark3.png");
    LOAD_INGAME_TEXTURE("explosion spark", "assets/textures/effects/single_spark3.png");
    LOAD_INGAME_TEXTURE("railgun spark", "assets/textures/effects/blue_spark2.png");

    LOAD_INGAME_TEXTURE("invisible", "assets/textures/invisible.png");
    LOAD_INGAME_TEXTURE("basebuilding", "assets/textures/map/abandonebasebuilding.png");
    LOAD_INGAME_TEXTURE("basefloor", "assets/textures/map/abandonedbasefloor.png");
    LOAD_INGAME_TEXTURE("airpipe", "assets/textures/map/airpipe.png");
    LOAD_INGAME_TEXTURE("barracks", "assets/textures/map/barracks.png");
    LOAD_INGAME_TEXTURE("borderfence", "assets/textures/map/borderfence.png");
    LOAD_INGAME_TEXTURE("containerbox", "assets/textures/map/containerbox.png");
    LOAD_INGAME_TEXTURE("containertank", "assets/textures/map/containertank.png");
    LOAD_INGAME_TEXTURE("cratebarrel1", "assets/textures/map/cratebarreltype1.png");
    LOAD_INGAME_TEXTURE("cratebarrel2", "assets/textures/map/cratebarreltype2.png");
    LOAD_INGAME_TEXTURE("fences", "assets/textures/map/fences.png");
    LOAD_INGAME_TEXTURE("metalbeams", "assets/textures/map/metalbeams.png");
    LOAD_INGAME_TEXTURE("metalstuff", "assets/textures/map/metalstuff.png");
    LOAD_INGAME_TEXTURE("quarrypipes", "assets/textures/map/quarrypipes.png");
    LOAD_INGAME_TEXTURE("railings", "assets/textures/map/railings.png");
    LOAD_INGAME_TEXTURE("runway", "assets/textures/map/runway.png");
    LOAD_INGAME_TEXTURE("shipfuselage", "assets/textures/map/shipfuselage.png");
    LOAD_INGAME_TEXTURE("shipparts", "assets/textures/map/shipparts.png");
    LOAD_INGAME_TEXTURE("signs", "assets/textures/map/signs.png");
    LOAD_INGAME_TEXTURE("stairscaffolding", "assets/textures/map/stairscaffolding.png");
    LOAD_INGAME_TEXTURE("tanks", "assets/textures/map/tanks.png");
    LOAD_INGAME_TEXTURE("trashpile", "assets/textures/map/trashpile.png");
    LOAD_INGAME_TEXTURE("woodenbeams", "assets/textures/map/woodenbeams.png");
    LOAD_INGAME_TEXTURE("woodenbridges", "assets/textures/map/woodenbridges.png");
    LOAD_INGAME_TEXTURE("stairs", "assets/textures/map/stairs.png");
    LOAD_INGAME_TEXTURE("level1cliffs",
        "assets/textures/map/terraincliffs.png");
    LOAD_INGAME_TEXTURE("level1ground1",
        "assets/textures/map/terrainground.png");
    LOAD_INGAME_TEXTURE("level1ground2",
        "assets/textures/map/terrainoutside.png");
    LOAD_INGAME_TEXTURE("rocks", "assets/textures/map/rocks.png");

    /*MATERIALS*/
    LOAD_INGAME_MATERIAL("rocket launcher",
        GET_INGAME_TEXTURE("rocket launcher"),
        GET_INGAME_TEXTURE("rocket launcher"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("pirate",
        GET_INGAME_TEXTURE("pirate"),
        GET_INGAME_TEXTURE("pirate"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("anarchist",
        GET_INGAME_TEXTURE("anarchist"),
        GET_INGAME_TEXTURE("anarchist"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("native",
        GET_INGAME_TEXTURE("native"),
        GET_INGAME_TEXTURE("native"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("officer",
        GET_INGAME_TEXTURE("officer"),
        GET_INGAME_TEXTURE("officer"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("smg",
        GET_INGAME_TEXTURE("smg"),
        GET_INGAME_TEXTURE("smg"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("smg bullet",
        GET_INGAME_TEXTURE("smg bullet"),
        GET_INGAME_TEXTURE("smg bullet"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("railgun",
        GET_INGAME_TEXTURE("railgun"),
        GET_INGAME_TEXTURE("railgun"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("machete",
        GET_INGAME_TEXTURE("machete"),
        GET_INGAME_TEXTURE("machete"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("rocket ammo",
        GET_INGAME_TEXTURE("rocket ammo"),
        GET_INGAME_TEXTURE("rocket ammo"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("smg ammo",
        GET_INGAME_TEXTURE("smg ammo"),
        GET_INGAME_TEXTURE("smg ammo"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("railgun ammo",
        GET_INGAME_TEXTURE("railgun ammo"),
        GET_INGAME_TEXTURE("railgun ammo"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("medkit",
        GET_INGAME_TEXTURE("medkit"),
        GET_INGAME_TEXTURE("medkit"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("explosion1",
        GET_INGAME_TEXTURE("explosion1"),
        GET_INGAME_TEXTURE("explosion1"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion2",
        GET_INGAME_TEXTURE("explosion2"),
        GET_INGAME_TEXTURE("explosion2"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion3",
        GET_INGAME_TEXTURE("explosion3"),
        GET_INGAME_TEXTURE("explosion3"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion4",
        GET_INGAME_TEXTURE("explosion4"),
        GET_INGAME_TEXTURE("explosion4"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion5",
        GET_INGAME_TEXTURE("explosion5"),
        GET_INGAME_TEXTURE("explosion5"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion6",
        GET_INGAME_TEXTURE("explosion6"),
        GET_INGAME_TEXTURE("explosion6"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion7",
        GET_INGAME_TEXTURE("explosion7"),
        GET_INGAME_TEXTURE("explosion7"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion8",
        GET_INGAME_TEXTURE("explosion8"),
        GET_INGAME_TEXTURE("explosion8"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion9",
        GET_INGAME_TEXTURE("explosion9"),
        GET_INGAME_TEXTURE("explosion9"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("explosion10",
        GET_INGAME_TEXTURE("explosion10"),
        GET_INGAME_TEXTURE("explosion10"), 1.0f, 1.0f, 1.0f, 1.0f);

    LOAD_INGAME_MATERIAL("bullethole",
        GET_INGAME_TEXTURE("bullethole"),
        GET_INGAME_TEXTURE("bullethole"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("railgun tracer1",
        GET_INGAME_TEXTURE("railgun tracer1"),
        GET_INGAME_TEXTURE("railgun tracer1"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("railgun tracer2",
        GET_INGAME_TEXTURE("railgun tracer2"),
        GET_INGAME_TEXTURE("railgun tracer2"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("railgun tracer3",
        GET_INGAME_TEXTURE("railgun tracer3"),
        GET_INGAME_TEXTURE("railgun tracer3"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("red material",
        0, 0, 0.8f, 0.8f, 0.2f, 0.2f);

    LOAD_INGAME_MATERIAL("muzzlefire",
        GET_INGAME_TEXTURE("muzzlefire"),
        GET_INGAME_TEXTURE("muzzlefire"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("explosion spark",
        GET_INGAME_TEXTURE("explosion spark"),
        GET_INGAME_TEXTURE("explosion spark"), 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("railgun spark",
        GET_INGAME_TEXTURE("railgun spark"),
        GET_INGAME_TEXTURE("railgun spark"), 0.0f, 0.0f, 0.0f, 0.0f);

    /* map materials*/
    LOAD_INGAME_MATERIAL("level1cliffs",
        GET_INGAME_TEXTURE("level1cliffs"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("level1ground1",
        GET_INGAME_TEXTURE("level1ground1"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("level1ground2",
        GET_INGAME_TEXTURE("level1ground2"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("basebuilding",
        GET_INGAME_TEXTURE("basebuilding"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("basefloor",
        GET_INGAME_TEXTURE("basefloor"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("airpipe",
        GET_INGAME_TEXTURE("airpipe"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("barracks",
        GET_INGAME_TEXTURE("barracks"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("borderfence",
        GET_INGAME_TEXTURE("borderfence"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("containerbox",
        GET_INGAME_TEXTURE("containerbox"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("containertank",
        GET_INGAME_TEXTURE("containertank"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("cratebarrel1",
        GET_INGAME_TEXTURE("cratebarrel1"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("cratebarrel2",
        GET_INGAME_TEXTURE("cratebarrel2"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("fences",
        GET_INGAME_TEXTURE("fences"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("metalbeams",
        GET_INGAME_TEXTURE("metalbeams"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("metalstuff",
        GET_INGAME_TEXTURE("metalstuff"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("quarrypipes",
        GET_INGAME_TEXTURE("quarrypipes"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("railings",
        GET_INGAME_TEXTURE("railings"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("runway",
        GET_INGAME_TEXTURE("runway"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("shipfuselage",
        GET_INGAME_TEXTURE("shipfuselage"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("shipparts",
        GET_INGAME_TEXTURE("shipparts"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("signs",
        GET_INGAME_TEXTURE("signs"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("stairscaffolding",
        GET_INGAME_TEXTURE("stairscaffolding"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("tanks",
        GET_INGAME_TEXTURE("tanks"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("trashpile",
        GET_INGAME_TEXTURE("trashpile"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("woodenbeams",
        GET_INGAME_TEXTURE("woodenbeams"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("woodenbridges",
        GET_INGAME_TEXTURE("woodenbridges"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("stairs",
        GET_INGAME_TEXTURE("stairs"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("invisible",
        GET_INGAME_TEXTURE("invisible"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    LOAD_INGAME_MATERIAL("rocks",
        GET_INGAME_TEXTURE("rocks"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);

    {
    /* Rocketmuzzles*/
    LOAD_INGAME_MATERIAL("rocketmuzzle1",
        GET_INGAME_TEXTURE("rocketmuzzle1"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);
    LOAD_INGAME_MATERIAL("rocketmuzzle2",
        GET_INGAME_TEXTURE("rocketmuzzle2"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);
    LOAD_INGAME_MATERIAL("rocketmuzzle3",
        GET_INGAME_TEXTURE("rocketmuzzle3"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);
    LOAD_INGAME_MATERIAL("rocketmuzzle4",
        GET_INGAME_TEXTURE("rocketmuzzle4"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);
    LOAD_INGAME_MATERIAL("rocketmuzzle5",
        GET_INGAME_TEXTURE("rocketmuzzle5"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);
    LOAD_INGAME_MATERIAL("rocketmuzzle6",
        GET_INGAME_TEXTURE("rocketmuzzle6"),
        0, 0.0f, 0.0f, 0.0f, 0.0f);
    }

    {
        Material *mat_rocks = GET_INGAME_MATERIAL("rocks");
        Material *mat_cliffs = GET_INGAME_MATERIAL("level1cliffs");
        Material *mat_ground1 = GET_INGAME_MATERIAL("level1ground1");
        Material *mat_ground2 = GET_INGAME_MATERIAL("level1ground2");
        Material *mat_basebuilding = GET_INGAME_MATERIAL("basebuilding");
        Material *mat_basefloor = GET_INGAME_MATERIAL("basefloor");
        Material *mat_airpipe = GET_INGAME_MATERIAL("airpipe");
        Material *mat_barracks = GET_INGAME_MATERIAL("barracks");
        Material *mat_borderfence = GET_INGAME_MATERIAL("borderfence");
        Material *mat_containerbox = GET_INGAME_MATERIAL("containerbox");
        Material *mat_containertank = GET_INGAME_MATERIAL("containertank");
        Material *mat_cratebarrel1 = GET_INGAME_MATERIAL("cratebarrel1");
        Material *mat_cratebarrel2 = GET_INGAME_MATERIAL("cratebarrel2");
        Material *mat_fences = GET_INGAME_MATERIAL("fences");
        Material *mat_metalbeams = GET_INGAME_MATERIAL("metalbeams");
        Material *mat_metalstuff = GET_INGAME_MATERIAL("metalstuff");
        Material *mat_quarrypipes = GET_INGAME_MATERIAL("quarrypipes");
        /*Material *mat_railings = GET_INGAME_MATERIAL("railings");*/
        Material *mat_runway = GET_INGAME_MATERIAL("runway");
        Material *mat_shipfuselage = GET_INGAME_MATERIAL("shipfuselage");
        Material *mat_shipparts = GET_INGAME_MATERIAL("shipparts");
        Material *mat_signs = GET_INGAME_MATERIAL("signs");
        Material *mat_stairscaffolding = GET_INGAME_MATERIAL("stairscaffolding");
        Material *mat_tanks = GET_INGAME_MATERIAL("tanks");
        Material *mat_trashpile = GET_INGAME_MATERIAL("trashpile");
        Material *mat_woodenbeams = GET_INGAME_MATERIAL("woodenbeams");
        Material *mat_woodenbridges = GET_INGAME_MATERIAL("woodenbridges");
        Material *mat_stairs = GET_INGAME_MATERIAL("stairs");
        /*Material *mat_invisible = GET_INGAME_MATERIAL("invisible");*/

        Material *materials[30];

        materials[0] = mat_rocks;
        materials[1] = mat_fences;
        materials[2] = mat_runway;
        materials[3] = mat_ground2;
        materials[4] = mat_airpipe;
        materials[5] = mat_basefloor;
        materials[6] = mat_stairs;
        materials[7] = mat_stairscaffolding;
        materials[8] = mat_airpipe;
        materials[9] = mat_woodenbridges;
        materials[10] = mat_ground1;
        materials[11] = mat_trashpile;
        materials[12] = mat_woodenbeams;
        materials[13] = mat_metalstuff;
        materials[14] = mat_woodenbeams;
        materials[15] = mat_barracks;
        materials[16] = mat_metalbeams;
        materials[17] = mat_borderfence;
        materials[18] = mat_cratebarrel1;
        materials[19] = mat_cratebarrel2;
        materials[20] = mat_containertank;
        materials[21] = mat_containerbox;
        materials[22] = mat_signs;
        materials[23] = mat_quarrypipes;
        materials[24] = mat_tanks;
        materials[25] = mat_basebuilding;
        materials[26] = mat_cliffs;
        materials[27] = mat_woodenbeams;
        materials[28] = mat_shipfuselage;
        materials[29] = mat_shipparts;

        AssetPool_loadMaterialArray(&game.ingame_asset_pool, "level1",
            materials, 30);

        Material *nocol_materials[2];

        nocol_materials[0] = mat_stairs;
        nocol_materials[1] = mat_woodenbridges;
        AssetPool_loadMaterialArray(&game.ingame_asset_pool, "level2", nocol_materials, 2);
    }

    /* Animations */
    {
        static ModelAnimationFrame stand_frames[2];
        stand_frames[0].model = GET_INGAME_MODEL("stand");
        stand_frames[1].model = GET_INGAME_MODEL("stand");
        for (int i = 0; i < 2; ++i)
        {
            stand_frames[i].material = GET_INGAME_MATERIAL("pirate");
            stand_frames[i].material_array = 0;
        }
        ig_assets.stand_animation.frames = stand_frames;
        ig_assets.stand_animation.num_frames = 2;
        ig_assets.stand_animation.duration = 1.0f;

        static ModelAnimationFrame jump_frames_up[4];
        jump_frames_up[0].model = GET_INGAME_MODEL("jump1");
        jump_frames_up[1].model = GET_INGAME_MODEL("jump2");
        jump_frames_up[2].model = GET_INGAME_MODEL("jump3");
        jump_frames_up[3].model = GET_INGAME_MODEL("jump4");
        for (int i = 0; i < 4; ++i)
        {
            jump_frames_up[i].material = GET_INGAME_MATERIAL("pirate");
            jump_frames_up[i].material_array = 0;
        }
        ig_assets.jump_up_animation.frames = jump_frames_up;
        ig_assets.jump_up_animation.num_frames = 4;
        ig_assets.jump_up_animation.duration = 0.075f;

        static ModelAnimationFrame jump_frames_down[1];
        jump_frames_down[0].model = GET_INGAME_MODEL("jump5");
        jump_frames_down[0].material = GET_INGAME_MATERIAL("pirate");
        jump_frames_down[0].material_array = 0;
        ig_assets.jump_down_animation.frames = jump_frames_down;
        ig_assets.jump_down_animation.num_frames = 1;
        ig_assets.jump_down_animation.duration = 1.0f;

        static ModelAnimationFrame dash_frames[1];
        dash_frames[0].model = GET_INGAME_MODEL("dash");
        dash_frames[0].material = GET_INGAME_MATERIAL("pirate");
        dash_frames[0].material_array = 0;
        ig_assets.dash_animation.frames = dash_frames;
        ig_assets.dash_animation.num_frames = 1;
        ig_assets.dash_animation.duration = 1.0f;

        static ModelAnimationFrame dashback_frames[1];
        dashback_frames[0].model = GET_INGAME_MODEL("dashback");
        dashback_frames[0].material = GET_INGAME_MATERIAL("pirate");
        dashback_frames[0].material_array = 0;
        ig_assets.dashback_animation.frames = dashback_frames;
        ig_assets.dashback_animation.num_frames = 1;
        ig_assets.dashback_animation.duration = 1.0f;

        static ModelAnimationFrame run_frames[8];
        run_frames[0].model = GET_INGAME_MODEL("run1");
        run_frames[1].model = GET_INGAME_MODEL("run2");
        run_frames[2].model = GET_INGAME_MODEL("run3");
        run_frames[3].model = GET_INGAME_MODEL("run4");
        run_frames[4].model = GET_INGAME_MODEL("run5");
        run_frames[5].model = GET_INGAME_MODEL("run6");
        run_frames[6].model = GET_INGAME_MODEL("run7");
        run_frames[7].model = GET_INGAME_MODEL("run8");
        for (int i = 0; i < 8; ++i)
        {
            run_frames[i].material = GET_INGAME_MATERIAL("pirate");
            run_frames[i].material_array = 0;
        }

        ig_assets.run_animation.frames = run_frames;
        ig_assets.run_animation.num_frames = 8;
        ig_assets.run_animation.duration = 0.05f;

        static ModelAnimationFrame strafeleft_frames[9];
        strafeleft_frames[0].model = GET_INGAME_MODEL("strafeleft1");
        strafeleft_frames[1].model = GET_INGAME_MODEL("strafeleft2");
        strafeleft_frames[2].model = GET_INGAME_MODEL("strafeleft3");
        strafeleft_frames[3].model = GET_INGAME_MODEL("strafeleft4");
        strafeleft_frames[4].model = GET_INGAME_MODEL("strafeleft5");
        strafeleft_frames[5].model = GET_INGAME_MODEL("strafeleft6");
        strafeleft_frames[6].model = GET_INGAME_MODEL("strafeleft7");
        strafeleft_frames[7].model = GET_INGAME_MODEL("strafeleft8");
        strafeleft_frames[8].model = GET_INGAME_MODEL("strafeleft9");

        ig_assets.strafe_left_animation.frames = strafeleft_frames;
        ig_assets.strafe_left_animation.num_frames = 9;
        ig_assets.strafe_left_animation.duration = 0.04f;

        static ModelAnimationFrame straferight_frames[9];
        straferight_frames[0].model = GET_INGAME_MODEL("straferight1");
        straferight_frames[1].model = GET_INGAME_MODEL("straferight2");
        straferight_frames[2].model = GET_INGAME_MODEL("straferight3");
        straferight_frames[3].model = GET_INGAME_MODEL("straferight4");
        straferight_frames[4].model = GET_INGAME_MODEL("straferight5");
        straferight_frames[5].model = GET_INGAME_MODEL("straferight6");
        straferight_frames[6].model = GET_INGAME_MODEL("straferight7");
        straferight_frames[7].model = GET_INGAME_MODEL("straferight8");
        straferight_frames[8].model = GET_INGAME_MODEL("straferight9");

        ig_assets.strafe_right_animation.frames = straferight_frames;
        ig_assets.strafe_right_animation.num_frames = 9;
        ig_assets.strafe_right_animation.duration = 0.04f;

        for (int i = 0; i < 9; ++i)
        {
            strafeleft_frames[i].material = GET_INGAME_MATERIAL("pirate");
            strafeleft_frames[i].material_array = 0;

            straferight_frames[i].material = GET_INGAME_MATERIAL("pirate");
            straferight_frames[i].material_array = 0;
        }

        static ModelAnimationFrame rocket_muzzle_frames[6];
        for (int i = 0; i < 6; ++i)
        {
            rocket_muzzle_frames[i].model = getDefaultPlaneModel();
            rocket_muzzle_frames[i].material_array = 0;
        }
        rocket_muzzle_frames[0].material = GET_INGAME_MATERIAL("rocketmuzzle1");
        rocket_muzzle_frames[1].material = GET_INGAME_MATERIAL("rocketmuzzle2");
        rocket_muzzle_frames[2].material = GET_INGAME_MATERIAL("rocketmuzzle3");
        rocket_muzzle_frames[3].material = GET_INGAME_MATERIAL("rocketmuzzle4");
        rocket_muzzle_frames[4].material = GET_INGAME_MATERIAL("rocketmuzzle5");
        rocket_muzzle_frames[5].material = GET_INGAME_MATERIAL("rocketmuzzle6");

        ig_assets.rocket_muzzle_animation.frames = rocket_muzzle_frames;
        ig_assets.rocket_muzzle_animation.num_frames = 6;
        ig_assets.rocket_muzzle_animation.duration = 0.03f;

        static ModelAnimationFrame smg_muzzle_frames[2];
        for (int i = 0; i < 2; ++i)
        {
            smg_muzzle_frames[i].model = getDefaultPlaneModel();
            smg_muzzle_frames[i].material_array = 0;
        }
        smg_muzzle_frames[0].material = GET_INGAME_MATERIAL("muzzlefire");
        smg_muzzle_frames[1].material = GET_INGAME_MATERIAL("muzzlefire");

        ig_assets.smg_muzzle_animation.frames = smg_muzzle_frames;
        ig_assets.smg_muzzle_animation.num_frames = 2;
        ig_assets.smg_muzzle_animation.duration = 0.075f;

        static ModelAnimationFrame railgun_muzzle_frames[3];
        for (int i = 0; i < 3; ++i)
        {
            railgun_muzzle_frames[i].model = getDefaultPlaneModel();
            railgun_muzzle_frames[i].material_array = 0;
        }
        railgun_muzzle_frames[0].material = GET_INGAME_MATERIAL("railgun tracer1");
        railgun_muzzle_frames[1].material = GET_INGAME_MATERIAL("railgun tracer2");
        railgun_muzzle_frames[2].material = GET_INGAME_MATERIAL("railgun tracer3");

        ig_assets.railgun_muzzle_animation.frames = railgun_muzzle_frames;
        ig_assets.railgun_muzzle_animation.num_frames = 3;
        ig_assets.railgun_muzzle_animation.duration = 0.01f;

        static ModelAnimationFrame explosion_frames[10];
        for (int i = 0; i < 10; ++i)
        {
            explosion_frames[i].model = GET_INGAME_MODEL("explosion");
            explosion_frames[i].material_array = 0;
        }
        explosion_frames[0].material = GET_INGAME_MATERIAL("explosion1");
        explosion_frames[1].material = GET_INGAME_MATERIAL("explosion2");
        explosion_frames[2].material = GET_INGAME_MATERIAL("explosion3");
        explosion_frames[3].material = GET_INGAME_MATERIAL("explosion4");
        explosion_frames[4].material = GET_INGAME_MATERIAL("explosion5");
        explosion_frames[5].material = GET_INGAME_MATERIAL("explosion6");
        explosion_frames[6].material = GET_INGAME_MATERIAL("explosion7");
        explosion_frames[7].material = GET_INGAME_MATERIAL("explosion8");
        explosion_frames[8].material = GET_INGAME_MATERIAL("explosion9");
        explosion_frames[9].material = GET_INGAME_MATERIAL("explosion10");

        ig_assets.explosion_animation.frames = explosion_frames;
        ig_assets.explosion_animation.num_frames = 10;
        ig_assets.explosion_animation.duration = 0.01f;

        /* Create the other char animations :DDD */
#   define iga ig_assets
        Material *mat1 = GET_INGAME_MATERIAL("anarchist");
        Material *mat2 = GET_INGAME_MATERIAL("native");
        Material *mat3 = GET_INGAME_MATERIAL("officer");

        _copyAnimation(&iga.stand_animation_anarchist, &iga.stand_animation, mat1);
        _copyAnimation(&iga.stand_animation_native, &iga.stand_animation, mat2);
        _copyAnimation(&iga.stand_animation_officer, &iga.stand_animation, mat3);

        _copyAnimation(&iga.jump_up_animation_anarchist, &iga.jump_up_animation, mat1);
        _copyAnimation(&iga.jump_up_animation_native, &iga.jump_up_animation, mat2);
        _copyAnimation(&iga.jump_up_animation_officer, &iga.jump_up_animation, mat3);

        _copyAnimation(&iga.jump_down_animation_anarchist, &iga.jump_down_animation, mat1);
        _copyAnimation(&iga.jump_down_animation_native, &iga.jump_down_animation, mat2);
        _copyAnimation(&iga.jump_down_animation_officer, &iga.jump_down_animation, mat3);

        _copyAnimation(&iga.dash_animation_anarchist, &iga.dash_animation, mat1);
        _copyAnimation(&iga.dash_animation_native, &iga.dash_animation, mat2);
        _copyAnimation(&iga.dash_animation_officer, &iga.dash_animation, mat3);

        _copyAnimation(&iga.dashback_animation_anarchist, &iga.dashback_animation, mat1);
        _copyAnimation(&iga.dashback_animation_native, &iga.dashback_animation, mat2);
        _copyAnimation(&iga.dashback_animation_officer, &iga.dashback_animation, mat3);

        _copyAnimation(&iga.run_animation_anarchist, &iga.run_animation, mat1);
        _copyAnimation(&iga.run_animation_native, &iga.run_animation, mat2);
        _copyAnimation(&iga.run_animation_officer, &iga.run_animation, mat3);

        _copyAnimation(&iga.strafe_left_animation_anarchist, &iga.strafe_left_animation, mat1);
        _copyAnimation(&iga.strafe_left_animation_native, &iga.strafe_left_animation, mat2);
        _copyAnimation(&iga.strafe_left_animation_officer, &iga.strafe_left_animation, mat3);

        _copyAnimation(&iga.strafe_right_animation_anarchist, &iga.strafe_right_animation, mat1);
        _copyAnimation(&iga.strafe_right_animation_native, &iga.strafe_right_animation, mat2);
        _copyAnimation(&iga.strafe_right_animation_officer, &iga.strafe_right_animation, mat3);
#   undef iga
    }

    return 0;
}

static thread_ret_t
_loadPooledIngameAssetsThreadedPortionCallback(void *args)
{
    /*MODELS*/
    LOAD_INGAME_MODEL("map", LEVEL_1_COLLISION_MAP_FILE_PATH);
    LOAD_INGAME_MODEL("map nocol", "assets/models/sps_level1_stairs_nocol.obj");
    LOAD_INGAME_MODEL("rocket launcher", "assets/rocket_launcher.obj");
    LOAD_INGAME_MODEL("rocket projectile", "assets/rocket_bullet.obj");
    LOAD_INGAME_MODEL("smg", "assets/smg.obj");
    game.loading_screen_data.percent_complete = 10;

    LOAD_INGAME_MODEL("smg bullet", "assets/smg_bullet.obj");
    LOAD_INGAME_MODEL("railgun", "assets/railgun.obj");
    LOAD_INGAME_MODEL("character", "assets/basepose.obj");
    LOAD_INGAME_MODEL("machete handle", "assets/models/machete2.obj");
    LOAD_INGAME_MODEL("machete blade", "assets/machete_blade.obj");
    game.loading_screen_data.percent_complete = 20;

    LOAD_INGAME_MODEL("explosion", "assets/models/explosion_ball.obj");
    LOAD_INGAME_MODEL("stand", "assets/models/stand.obj");
    LOAD_INGAME_MODEL("dash", "assets/models/dash.obj");
    LOAD_INGAME_MODEL("dashback", "assets/models/dashback.obj");
    LOAD_INGAME_MODEL("jump1", "assets/models/jump1.obj");
    game.loading_screen_data.percent_complete = 30;

    LOAD_INGAME_MODEL("jump2", "assets/models/jump2.obj");
    LOAD_INGAME_MODEL("jump3", "assets/models/jump3.obj");
    LOAD_INGAME_MODEL("jump4", "assets/models/jump4.obj");
    LOAD_INGAME_MODEL("jump5", "assets/models/jump5.obj");
    LOAD_INGAME_MODEL("run1", "assets/models/testrun1.obj");
    game.loading_screen_data.percent_complete = 40;

    LOAD_INGAME_MODEL("run2", "assets/models/testrun2.obj");
    LOAD_INGAME_MODEL("run3", "assets/models/testrun3.obj");
    LOAD_INGAME_MODEL("run4", "assets/models/testrun4.obj");
    LOAD_INGAME_MODEL("run5", "assets/models/testrun5.obj");
    LOAD_INGAME_MODEL("run6", "assets/models/testrun6.obj");
    game.loading_screen_data.percent_complete = 50;

    LOAD_INGAME_MODEL("run7", "assets/models/testrun7.obj");
    LOAD_INGAME_MODEL("run8", "assets/models/testrun8.obj");
    LOAD_INGAME_MODEL("strafeleft1", "assets/models/strafeleft1.obj");
    LOAD_INGAME_MODEL("strafeleft2", "assets/models/strafeleft2.obj");
    LOAD_INGAME_MODEL("strafeleft3", "assets/models/strafeleft3.obj");
    game.loading_screen_data.percent_complete = 60;

    LOAD_INGAME_MODEL("strafeleft4", "assets/models/strafeleft4.obj");
    LOAD_INGAME_MODEL("strafeleft5", "assets/models/strafeleft5.obj");
    LOAD_INGAME_MODEL("strafeleft6", "assets/models/strafeleft6.obj");
    LOAD_INGAME_MODEL("strafeleft7", "assets/models/strafeleft7.obj");
    LOAD_INGAME_MODEL("strafeleft8", "assets/models/strafeleft8.obj");
    game.loading_screen_data.percent_complete = 70;

    LOAD_INGAME_MODEL("strafeleft9", "assets/models/strafeleft9.obj");
    LOAD_INGAME_MODEL("straferight1", "assets/models/straferight1.obj");
    LOAD_INGAME_MODEL("straferight2", "assets/models/straferight2.obj");
    LOAD_INGAME_MODEL("straferight3", "assets/models/straferight3.obj");
    LOAD_INGAME_MODEL("straferight4", "assets/models/straferight4.obj");
    game.loading_screen_data.percent_complete = 80;

    LOAD_INGAME_MODEL("straferight5", "assets/models/straferight5.obj");
    LOAD_INGAME_MODEL("straferight6", "assets/models/straferight6.obj");
    LOAD_INGAME_MODEL("straferight7", "assets/models/straferight7.obj");
    LOAD_INGAME_MODEL("straferight8", "assets/models/straferight8.obj");
    LOAD_INGAME_MODEL("straferight9", "assets/models/straferight9.obj");
    game.loading_screen_data.percent_complete = 90;

    LOAD_INGAME_MODEL("medkit", "assets/medkit1.obj");
    LOAD_INGAME_MODEL("rocket ammo", "assets/rocket_ammo.obj");
    LOAD_INGAME_MODEL("smg ammo", "assets/smg_ammo.obj");
    LOAD_INGAME_MODEL("railgun ammo", "assets/railgun_ammo.obj");

    /* Sound effects */
    permanent_assets.se_railgun = loadSoundEffect(
        "assets/sound_effects/railgunz.wav");

    permanent_assets.se_steps = loadSoundEffect(
        "assets/sound_effects/steps.wav");
    SoundEffect_setVolume(permanent_assets.se_steps, 128);

    permanent_assets.se_death_scream1 = loadSoundEffect(
        "assets/sound_effects/death_scream1.wav");
    SoundEffect_setVolume(permanent_assets.se_death_scream1, 64);

    permanent_assets.se_hit_scream1 = loadSoundEffect(
        "assets/sound_effects/hit_scream1.wav");
    SoundEffect_setVolume(permanent_assets.se_hit_scream1, 64);

    permanent_assets.se_hit_scream2 = loadSoundEffect(
        "assets/sound_effects/hit_scream2.wav");

    permanent_assets.se_fwump = loadSoundEffect("assets/sound_effects/fwump.wav");
    SoundEffect_setVolume(permanent_assets.se_fwump, 64);

    permanent_assets.se_explosion = loadSoundEffect("assets/sound_effects/explosion.wav");
    SoundEffect_setVolume(permanent_assets.se_explosion, 64);

    permanent_assets.se_smg = loadSoundEffect("assets/sound_effects/smg3.wav");
    SoundEffect_setVolume(permanent_assets.se_smg, 32);

    game.loading_screen_data.percent_complete   = 100;

    return 0;
}

int
loadPooledIngameAssetsThreadedPortion()
{
    if (game.loaded_startup_data) /* Already loaded */
    {
        DEBUG_PRINTF("%s: already loaded - returning 0.\n", __func__);
        return 0;
    }

    if (Thread_create(&game.load_thread,
        _loadPooledIngameAssetsThreadedPortionCallback, 0) != 0)
        return 1;

    return 0;
}

int
loadPermanentAssets()
{
    permanent_assets.tex_skybox_level1 = loadCubemapTexture(
        "assets/textures/skybox/xpos.png",
        "assets/textures/skybox/xneg.png",
        "assets/textures/skybox/ypos.png",
        "assets/textures/skybox/yneg.png",
        "assets/textures/skybox/zpos.png",
        "assets/textures/skybox/zneg.png");

    permanent_assets.tex_karistys_title = loadTexture(
        "assets/textures/karistyslogo_small.png");

    permanent_assets.sfont_roboto = loadSpriteFont(
        "assets/fonts/Roboto-Regular.ttf", 24, BYTE_COLOR_WHITE);

    permanent_assets.crosshair_texture = loadTexture(
        "assets/textures/cursor.png");
    permanent_assets.hitscreen_texture = loadTexture(
        "assets/textures/ui/hitscreen.png");
    permanent_assets.rocket_selected_texture = loadTexture(
        "assets/textures/ui/rocket_select.png");
    permanent_assets.smg_selected_texture = loadTexture(
        "assets/textures/ui/smg_select.png");
    permanent_assets.railgun_selected_texture = loadTexture(
        "assets/textures/ui/railgun_select.png");
    permanent_assets.machete_selected_texture = loadTexture(
        "assets/textures/ui/machete_select.png");
    permanent_assets.tex_button_sheet1 = loadTexture(
        "assets/textures/ui/button_sheet1.png");
    permanent_assets.tex_button_sheet2 = loadTexture(
        "assets/textures/ui/button_sheet2.png");
    if (!permanent_assets.tex_button_sheet2)
        DEBUG_PRINTF("NONONO!\n");

    default_progressbar_style->border_texture = loadTexture(
        "assets/textures/ui/healthbar_base.png");
    default_progressbar_style->background_texture = loadTexture(
        "assets/textures/ui/healthbar_back.png");
    default_progressbar_style->fill_texture = loadTexture(
        "assets/textures/ui/healthbar_fill.png");

#   define SET_CLIP(_c, _x, _y, _w, _h) \
        (_c)[0] = (_x); \
        (_c)[1] = (_y); \
        (_c)[2] = (_w); \
        (_c)[3] = (_h);

    SET_CLIP(permanent_assets.clip_resume_n, 970, 464, 704, 126);
    SET_CLIP(permanent_assets.clip_resume_h, 970, 590, 704, 126);

    SET_CLIP(permanent_assets.clip_ragequit_n, 970, 716, 542, 126);
    SET_CLIP(permanent_assets.clip_ragequit_h, 970, 842, 542, 126);


    SET_CLIP(permanent_assets.clip_demo_n, 0, 12, 568, 128);
    SET_CLIP(permanent_assets.clip_demo_h, 568, 12, 568, 128);
    SET_CLIP(permanent_assets.clip_multiplayer_n, 0, 138, 568, 134);
    SET_CLIP(permanent_assets.clip_multiplayer_h, 568, 138, 568, 134);
    SET_CLIP(permanent_assets.clip_credits_n, 0, 256, 568, 128);
    SET_CLIP(permanent_assets.clip_credits_h, 568, 256, 568, 128);
    SET_CLIP(permanent_assets.clip_quit_n, 0, 414, 568, 134);
    SET_CLIP(permanent_assets.clip_quit_h, 568, 414, 568, 134);

    return 0;

#   undef SET_CLIP
}

int
parseServerAddressFromFile(Address *ret_addr, const char *path)
{
    FILE *file = fopen(path, "r");
    if (!file) return 1;

    char buf[64];
    int len;

    /* Find position for word "hostname" */
    rewind(file);
    const char *cmp_str = "hostname = ";
    const int l = strlen(cmp_str);
    char c;
    int num_correct = 0;

    while (num_correct != l)
    {
        c = getc(file);

        if (c == '#')
        {
            num_correct = 0;
            do {c = getc(file);} while (c != EOF && c != '\n');
        }
        if (c == EOF) break;
        if (c == cmp_str[num_correct]) ++num_correct;
    }

    if (num_correct != l)
    {
        fclose(file);
        return 2;
    }

    buf[0] = '\0';
    fgets(buf, 64, file);
    len = strlen(buf);

    fclose(file);

    if (len <= 0) return 3;

    /* Remove the first new line */
    for (char *ch = buf; (*ch) != '\0'; ++ch)
    {
        if ((*ch) == '\n') {*ch = '\0'; break;}
    }

    struct sockaddr_in in_address = {0};
    if (inet_pton(AF_INET, buf, &in_address.sin_addr) != 1)
        return 4;

    Address_initFromUint32(ret_addr, (uint32_t)in_address.sin_addr.s_addr,
        KYS_SERVER_PORT);

    return 0;
}

int
connectToServer()
{
    if (kys_initNetworking() != 0) return 1;

    int client_ret = Client_init(&client);
    if (client_ret != 0 && client_ret != CLIENT_ERROR_ALREADY_INITIALIZED)
        return 2;

    Client_setFPS(&client, network_tick_rate);

    client.on_receive_message_callback  = _onReceiveMessageCallback;
    client.on_update_callback           = _onClientUpdateCallback;

    /* Temporary :DD */
    for (ReplicationTableEntry *rte =
            game.game_scene.replication_table.reserved_entries;
         rte;
         rte = rte->reserved_next)
    {
        Scene3D_removeEntity(&game.game_scene, rte->entity);
        DEBUG_PRINTF("Removing a replicated entity!\n");
    }

    game.game_scene.running_replication_id = 0;
    game.mp_data.pawn = 0;

    Address addr;

    int parse_ret = parseServerAddressFromFile(&addr, "connect.conf");
    if (parse_ret != 0)
    {
        DEBUG_PRINTF("Error reading connect.conf (code %i) - "
            "defaulting to localhost.\n", parse_ret);
        Address_init(&addr, 127, 0, 0, 1, KYS_SERVER_PORT);
    }

    DEBUG_PRINTF("Attempting to connect to server address %i.%i.%i.%i...\n",
        (int)Address_getX(&addr), (int)Address_getY(&addr),
        (int)Address_getZ(&addr), (int)Address_getW(&addr));

    InputBuffer_clear(&input_buffer);
    MessageBuffer_clear();
    resetMultiplayerData();

    if (Client_connect(&client, &addr) != 0)
        return 4;

    return 0;
}

void
sendInputBufferContents()
{
    if (input_buffer.num_items <= 0) return;

    ByteBuffer  b;
    void        *msg;
    bool32      msg_queue_full = 0;

    Mutex_lock(&input_buffer.mutex);

    int num_bytes = (int)KS_CS_MSG_INPUT_BUFFER_CONTENTS_MIN_SIZE + \
        input_buffer.size_in_bytes + \
        input_buffer.num_items * (sizeof(char) + \
        sizeof(input_sequence_t));

    msg = Client_queueMessage(&client, KS_CS_MSG_INPUT_BUFFER_CONTENTS,
        num_bytes);

    if (!msg)
    {
        DEBUG_PRINTF("Failed to create input msg!\n");
        return;
    }

    ByteBuffer_init(&b, msg, num_bytes);
    ByteBuffer_write(&b, ubyte, input_buffer.num_items);

    ClientInput *inp;

    for (ClientInput **inp_ptr = input_buffer.new_items;
         inp_ptr < &input_buffer.new_items[input_buffer.num_items] && !msg_queue_full;
         ++inp_ptr)
    {
        inp = *inp_ptr;

        switch (inp->type)
        {
            case INPUT_MOVEMENT:
            {
                ByteBuffer_write(&b, char, (char)INPUT_MOVEMENT);
                ByteBuffer_write(&b, input_sequence_t,
                    (input_sequence_t)inp->sequence);
                ByteBuffer_write(&b, kb_state_t, inp->data.movement.kb_state);
                ByteBuffer_write(&b, float, inp->data.movement.orientation);
            }
                break;
        }
    }

    input_buffer.num_items      = 0;
    input_buffer.size_in_bytes  = 0;

    Mutex_unlock(&input_buffer.mutex);
}

static void
_onClientUpdateCallback(Client *client)
{
    sendInputBufferContents();
    MessageBuffer_sendAllContents();
}

BulletType
createBulletType(BaseBulletType *base,
    Model *model, Material *material,
    float scale, vec3 relative_rotation)
{
    BulletType b;

    b.base      = *base;
    b.model     = model;
    b.material  = material;
    b.scale     = scale;
    vec3_copy(b.relative_rotation, relative_rotation);

    return b;
}

WeaponType
createWeaponType(BaseWeaponType *base,
    Model *model, Material *material,
    BulletType *bullet_type,
    float animation_duration, float muzzlefire_duration,
    vec3 relative_position, vec3 relative_rotation,
    void (*sp_on_shoot_callback)(enum KsWeaponType type, Entity *pawn))
{
    WeaponType w;

    w.base                  = *base;
    w.model                 = model;
    w.material              = material;
    w.pos_x                 = relative_position[0];
    w.pos_y                 = relative_position[1];
    w.pos_z                 = relative_position[2];
    w.rot_x                 = relative_rotation[0];
    w.rot_y                 = relative_rotation[1];
    w.rot_z                 = relative_rotation[2];
    w.sp_on_shoot_callback  = sp_on_shoot_callback;
    w.animation_duration    = animation_duration;
    w.muzzlefire_duration   = muzzlefire_duration;

    return w;
}

void
setPlayerPawnWeaponLocal(Entity *pawn, enum KsWeaponType weapon)
{
    ASSERT(weapon >= 0 && weapon < NUM_BASE_WEAPON_TYPES);

    ScriptComponent *sc = Entity_getScriptComponent(pawn);  ASSERT(sc);
    PlayerScriptData *d = (PlayerScriptData*)sc->data;      ASSERT(d);
    ModelComponent *m   = Entity_getModelComponent(pawn);   ASSERT(m);

    d->weapon = weapon;

    RenderModel *rm = ModelComponent_getDefaultRenderModel(m);

    setPlayerPawnVisualWeaponModel(rm, weapon, d);
}

Entity *
createMapEntity(Scene3D *scene, Model *model,Model *no_collision_model,
    MaterialArray *material_array, MaterialArray *nocol_material_array,
    int shader, float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(scene, x, y, z);
    ModelComponent *m = Entity_attachModelComponent(e);

    RenderModel *rm = ModelComponent_getDefaultRenderModel(m);

    rm->model           = model;
    rm->material_array  = material_array;
    m->visible          = 1;
    rm->shader          = shader;
    rm->outline_normal_offset = 0.05f;

    RenderModel *rm_nocol = ModelComponent_attachRenderModel(m, scene, 0);

    rm_nocol->model = no_collision_model;
    rm_nocol->visible = 1;
    rm_nocol->material_array = nocol_material_array;
    rm_nocol->shader = CEL_SHADER_OPAQUE;

    return e;
}

Entity *
createBulletEntity(Scene3D *scene3d, enum KsWeaponType weapon,
    vec3 pos, vec3 rot, vec3 dir)
{
    BulletType *type = GET_BULLET_TYPE(weapon);

    Entity *e       = Scene3D_createEntity(scene3d, pos[0], pos[1], pos[2]);
    Transform *t    = &Entity_getTransform(e)->transform;

    Entity_setLifetime(e, type->base.lifetime);

    t->scale[0] = type->scale;
    t->scale[1] = type->scale;
    t->scale[2] = type->scale;

    t->rot[0]   = rot[0];
    t->rot[1]   = rot[1];
    t->rot[2]   = rot[2];

    PhysicsComponent *pc    = Entity_attachPhysicsComponent(e);
    pc->gravity_active      = 0;
    pc->friction            = 0.0f;
    PhysicsComponent_setActivity(pc, 1);
    vec3_copy(pc->velocity, dir);
    vec3_norm(pc->velocity, pc->velocity);
    vec3_scale(pc->velocity, pc->velocity, type->base.speed);

    ModelComponent *m = Entity_attachModelComponent(e);
    m->visible = 1;
    RenderModel *rm = ModelComponent_getDefaultRenderModel(m);
    rm->shader      = CEL_SHADER_OPAQUE;
    rm->model       = type->model;
    rm->material    = type->material;

    vec3_copy(rm->relative_transform.rot, type->relative_rotation);

    ScriptComponent *sc = Entity_attachScriptComponent(e);
    BulletScriptData *d = ScriptComponent_allocateData(sc,
        sizeof(BulletScriptData));

    d->timer = 0;
    d->bullet_velocity = type->base.speed;

    return e;
}

Entity *
createDirectionalLightEntity(Scene3D *scene, float x, float y, float z,
    float dir_x, float dir_y, float dir_z)
{
    Entity *e = Scene3D_createEntity(scene, x, y, z);

    DirectionalLightComponent *l = Entity_attachDirectionalLightComponent(e);
    DirectionalLightComponent_setDiffuse(l, 0.4f, 0.4f, 0.4f);
    DirectionalLightComponent_setSpecular(l, 0.1f, 0.1f, 0.1f);
    DirectionalLightComponent_setDirection(l, dir_x, dir_y, dir_z, 1);
    l->radius = 50.0f;

    EntityTransform *et = Entity_getTransform(e);
    et->transform.scale[0] = 1.0f;
    et->transform.scale[1] = 1.0f;
    et->transform.scale[2] = 1.0f;

    return e;
}

void
createEffect(Entity *entity, Model* model,
    Material *material, vec3 pos, vec3 velocity, vec3 rotation, float scale, float lifetime,
    bool32 gravity_active, bool32 is_billboard)
{
    Entity *e = Scene3D_createEntity(entity->scene, pos[0],
        pos[1], pos[2]);

    ModelComponent *m = Entity_attachModelComponent(e);
    RenderModel *rm = ModelComponent_attachRenderModel(m, entity->scene, 1);
    rm->model = model;
    rm->material = material;
    vec3_copy(rm->relative_transform.rot, rotation);
    vec3_set(rm->relative_transform.scale, scale, scale, scale);
    rm->shader = SHADER3D_BILLBOARD;

    if (is_billboard)
        rm->mode = RENDER_MODEL_BILLBOARD;
    else
        rm->mode = RENDER_MODEL_NORMAL;

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);

    pc->gravity_active = gravity_active;
    vec3_copy(pc->velocity, velocity);

    Entity_setLifetime(e, lifetime);
}

int
MessageBuffer_init()
{
    void *mem = allocatePermanentMemory(2048);
    if (!mem) return 1;

    SimpleMemoryBlock_init(&message_buffer.memory, mem, 2048);
    Mutex_init(&message_buffer.mutex);
    message_buffer.num_items = 0;

    return 0;
}

void *
MessageBuffer_queueMessageUnsafe(message_t type, int size)
{
    if (message_buffer.num_items >= MAX_MESSAGE_BUFFER_ITEMS)
        return 0;

    void *msg = SimpleMemoryBlock_malloc(&message_buffer.memory, (size_t)size);
    if (!msg) return 0;

    struct MessageBufferItem *item = &message_buffer.items[
        message_buffer.num_items];

    item->type      = type;
    item->data      = msg;
    item->size      = size;

    ++message_buffer.num_items;

    return msg;
}

void
MessageBuffer_sendAllContents()
{
    Mutex_lock(&message_buffer.mutex);

    struct MessageBufferItem *item;
    void *msg;

    for (uint i = 0; i < message_buffer.num_items; ++i)
    {
        item = &message_buffer.items[i];

        msg = Client_queueMessage(&client, item->type, item->size);

        if (!msg)
        {
            DEBUG_PRINTF("Failed to queue a message from the output buffer!\n");
        }
        else
        {
            memcpy(msg, item->data, (size_t)item->size);
        }
    }

    MessageBuffer_clear();
    Mutex_unlock(&message_buffer.mutex);
}

void
client_initPlayerPawn(Entity *e)
{
    shared_initPlayerPawn(e);
    ScriptComponent *sc = Entity_attachScriptComponent(e);
    PlayerScriptData *sd = ScriptComponent_allocateData(sc,
        sizeof(PlayerScriptData));
    shared_initBasePlayerScriptData(&sd->base);
    ScriptComponent_setCallback(sc, sp_playerScript);

    /* Client specific script initialization */
    sd->muzzlefire_timer    = 0.0f;
    sd->animation_timer     = 0.0f;
    sd->movement_state      = PAWN_MOVEMENT_STATE_STAND;
    sd->base.is_grounded = 0;
    sd->base.can_shoot = 0;
    sd->base.is_hit = 0;
    sd->base.is_hit_timer = 0.0f;
    sd->base.f_toggled = 0;
    sd->base.is_dashing = 0;

    ModelComponent *m = Entity_attachModelComponent(e);
    RenderModel *rm = ModelComponent_attachRenderModel(m, e->scene,
        PLAYER_RENDER_MODEL_ID_MUZZLEFIRE);
    rm->material    = GET_INGAME_MATERIAL("muzzlefire");
    rm->shader      = SHADER3D_BILLBOARD;
    rm->mode        = RENDER_MODEL_BILLBOARD;

    rm->relative_transform.pos[0] = -1.4f;
    rm->relative_transform.pos[1] = -0.3f;
    rm->relative_transform.pos[2] = -3.25f;
    rm->relative_transform.scale[0] = 0.5f;
    rm->relative_transform.scale[1] = 0.5f;
    rm->relative_transform.scale[2] = 0.5f;

    ModelComponent_attachAnimator(m, e->scene, rm,
        PLAYER_ANIMATOR_ID_MUZZLEFIRE);

    rm = ModelComponent_getDefaultRenderModel(m);
    rm->visible = 1;
    rm->shader = CEL_SHADER_OPAQUE;
    rm->outline_normal_offset = 0.02f;
    ModelComponent_attachAnimator(m, e->scene, rm, PLAYER_ANIMATOR_ID_WEAPON);

    setPlayerPawnWeaponLocal(e, DEFAULT_WEAPON);
    vec3_set(rm->relative_transform.scale, 0.1f, 0.1f, 0.1f);
}

void
resetMultiplayerData()
{
    memset(&game.mp_data, 0, sizeof(game.mp_data));
    game.mp_data.health = MAX_HEALTH;
}

void
initGameData()
{
    vec3 rel_pos;
    vec3 rel_rot;

    /* Bullet types */
    vec3_set(rel_rot, 0.0f, (float)DEG_TO_RAD(-90.0f), 0.0f);

    game_data.bullet_types[ROCKET_LAUNCHER] = createBulletType( 
        GET_BASE_BULLET_TYPE(ROCKET_LAUNCHER),
        GET_INGAME_MODEL("rocket projectile"),
        GET_INGAME_MATERIAL("rocket projectile"),
        0.5f, rel_rot);

    game_data.bullet_types[SMG] = createBulletType( 
        GET_BASE_BULLET_TYPE(SMG),
        GET_INGAME_MODEL("smg bullet"),
        GET_INGAME_MATERIAL("muzzlefire"),
        0.3f, rel_rot);

    game_data.bullet_types[RAILGUN] = createBulletType( 
        GET_BASE_BULLET_TYPE(RAILGUN),
        getDefaultPlaneModel(),
        GET_INGAME_MATERIAL("railgun tracer"),
        0.5f, rel_rot);

    game_data.bullet_types[MACHETE] = createBulletType( 
        GET_BASE_BULLET_TYPE(MACHETE),
        getDefaultBallModel(),
        GET_INGAME_MATERIAL("red material"),
        0.3f, rel_rot);

    /* Weapon types */
    vec3_set(rel_pos, -5.0f, -5.0f, -6.0f);
    vec3_set(rel_rot, 0.0f, -1.6f, 0.0f);

    game_data.weapon_types[ROCKET_LAUNCHER] = createWeaponType(
        GET_BASE_WEAPON_TYPE(ROCKET_LAUNCHER),
        GET_INGAME_MODEL("rocket launcher"),
        GET_INGAME_MATERIAL("rocket launcher"),
        GET_BULLET_TYPE(ROCKET_LAUNCHER),
        COOLDOWN_ROCKET_LAUNCHER, 0.2f, rel_pos, rel_rot,
        sp_onShootCallback_RocketLauncher);

    vec3_set(rel_pos, -4.5f, -5.0f, -4.5f);
    vec3_set(rel_rot, 0.0f, -1.6f, 0.0f);

    game_data.weapon_types[SMG] = createWeaponType(
        GET_BASE_WEAPON_TYPE(SMG),
        GET_INGAME_MODEL("smg"),
        GET_INGAME_MATERIAL("smg"),
        GET_BULLET_TYPE(SMG),
        COOLDOWN_SMG, 0.05f, rel_pos, rel_rot,
        sp_onShootCallback_SMG);

    vec3_set(rel_pos, -4.0f, -5.0f, -8.0f);
    vec3_set(rel_rot, 0.0f, -1.7f, 0.0f);

    game_data.weapon_types[RAILGUN] = createWeaponType(
        GET_BASE_WEAPON_TYPE(RAILGUN),
        GET_INGAME_MODEL("railgun"),
        GET_INGAME_MATERIAL("railgun"),
        GET_BULLET_TYPE(RAILGUN),
        COOLDOWN_RAILGUN, 0.05f, rel_pos, rel_rot,
        sp_onShootCallback_Railgun);

    vec3_set(rel_pos, -5.0f, -5.5f, -5.0f);
    vec3_set(rel_rot, 0.0f, -1.6f, 0.0f);

    game_data.weapon_types[MACHETE] = createWeaponType(
        GET_BASE_WEAPON_TYPE(MACHETE),
        GET_INGAME_MODEL("machete handle"),
        GET_INGAME_MATERIAL("machete"),
        GET_BULLET_TYPE(MACHETE),
        COOLDOWN_MACHETE, 0.0f, rel_pos, rel_rot,
        sp_onShootCallback_Machete);

    game.loaded_map_id = KS_MAP_NONE;
}

void
SimQueue_init()
{
    sim_queue.num_tasks = 0;
    Mutex_init(&sim_queue.mutex);
}

void
SimQueue_executeContents()
{
    if (sim_queue.num_tasks <= 0) return;

    Mutex_lock(&sim_queue.mutex);

    for (uint i = 0; i < sim_queue.num_tasks; ++i)
    {
        switch (sim_queue.tasks[i].type)
        {
            case SIM_REMOVE_ENTITY:
            {
                if (sim_queue.tasks[i].data.remove_entity.entity->id ==
                    sim_queue.tasks[i].data.remove_entity.id)
                {
                    Scene3D_removeEntity(&game.game_scene,
                        sim_queue.tasks[i].data.remove_entity.entity);
                    DEBUG_PRINTF("SimQueue: removed an entity!\n");
                }
            }
                break;
            case SIM_SPAWN_ITEM:
            {
                int index   = sim_queue.tasks[i].data.spawn_item.index;
                float *pos  = sim_queue.tasks[i].data.spawn_item.pos;

                if (!game.mp_data.item_spawns[index].entity)
                {
                    game.mp_data.item_spawns[index].entity =
                        mp_createItemEntity(&game.game_scene,
                            sim_queue.tasks[i].data.spawn_item.type,
                            pos[0], pos[1], pos[2]);
                }

                Transform *t = &Entity_getTransform(
                    game.mp_data.item_spawns[index].entity)->transform;
                vec3_copy(t->pos, pos);
                ModelComponent *mc = Entity_getModelComponent(
                    game.mp_data.item_spawns[index].entity);
                ModelComponent_getDefaultRenderModel(mc)->visible = 1;
            }
                break;
            case SIM_COLLIDE_ITEM:
            {
                int index = sim_queue.tasks[i].data.collide_item.item_index;
                /*enum KsItemType type =
                    sim_queue.tasks[i].data.collide_item.item_type;*/
                replication_id32_t rid =
                    sim_queue.tasks[i].data.collide_item.item_type;

                Entity *e = Scene3D_getReplicatedEntity(&game.game_scene, rid);


                if (e)
                {
                    /* Do something to entity based on item type */
                    if (mp_getPlayerPawn()
                    && mp_getPlayerPawn()->id == e->id)
                    {
                        MPGameScreen_playPowerupText();
                    }
                }

                Entity *item = game.mp_data.item_spawns[index].entity;

                if (item)
                {
                    ModelComponent *mc = Entity_getModelComponent(item);
                    ModelComponent_getDefaultRenderModel(mc)->visible = 0;
                }
            }
                break;
            case SIM_SWAP_OTHER_PAWN_WEAPON:
            {
                Entity *e = Scene3D_getReplicatedEntity(&game.game_scene,
                    sim_queue.tasks[i].data.swap_other_pawn_weapon.rid);
                if (!e)
                    break;

                ModelComponent *m = Entity_getModelComponent(e);
                RenderModel *rm = ModelComponent_getRenderModelById(m, 2);

                WeaponType *w = GET_WEAPON_TYPE(
                    sim_queue.tasks[i].data.swap_other_pawn_weapon.w);

                rm->model = w->model;
                rm->material = w->material;

                mp_setOtherPawnWeaponRenderModelTransform(rm,
                    sim_queue.tasks[i].data.swap_other_pawn_weapon.w);
            }
                break;
            case SIM_EXPLODE_ROCKET:
            {
                mp_createRocketExplosion(
                    sim_queue.tasks[i].data.explode_rocket.pos);
            }
                break;
            case SIM_SNAP_PAWN_STATE:
            {
                float *pos = sim_queue.tasks[i].data.snap_pawn_state.pos;
                float *vel = sim_queue.tasks[i].data.snap_pawn_state.vel;

                input_sequence_t start, end;

                if (input_buffer.running_sequence >= MAX_INPUT_REPLAYS)
                    start   = input_buffer.running_sequence - MAX_INPUT_REPLAYS;
                else
                    start = 65535 - (MAX_INPUT_REPLAYS - input_buffer.running_sequence);

                end = input_buffer.running_sequence;

                StoredClientInput *inp;

                for (input_sequence_t i = start;
                     i != end;
                     i = (i != 65535 ? i + 1 : 0))
                {
                    inp = &input_buffer.sent_items[i % MAX_SENT_INPUT_ITEMS];

                    inp->input.data.movement.kb_state = 0;
                    vec3_copy(inp->position, pos);
                    vec3_copy(inp->velocity, vel);
                }

                Entity *pawn = mp_getPlayerPawn();

                if (pawn)
                {
                    Transform *t = &Entity_getTransform(pawn)->transform;
                    PhysicsComponent *pc = Entity_getPhysicsComponent(pawn);
                    vec3_copy(t->pos, pos);
                    vec3_copy(pc->velocity, vel);
                }
            }
                break;
        }
    }

    sim_queue.num_tasks = 0;

    Mutex_unlock(&sim_queue.mutex);
}

void
mp_setOtherPawnMovementStateAndAnimation(Entity *e, int state)
{
    if (state < 0 || state > NUM_PAWN_MOVEMENT_STATES)
    {
        DEBUG_PRINTF("mp_playerAnimationUpdate: incorrect range of state!\n");
        return;
    }

    ScriptComponent *sc = Entity_getScriptComponent(e);
    PlayerScriptData *pd = (PlayerScriptData*)sc->data;
    ModelComponent *m = Entity_getModelComponent(e);
    Animator *an = ModelComponent_getAnimatorByName(m, "character");

    char character_type = pd->character_type;

    if (pd->movement_state != state)
    {
        switch (state)
        {
            case PAWN_MOVEMENT_STATE_STAND:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.stand_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.stand_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.stand_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.stand_animation_officer, 1);
                        break;
                }
            }
                break;
            case PAWN_MOVEMENT_STATE_FORWARD:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.run_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.run_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.run_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.run_animation_officer, 1);
                        break;
                }
            }
                break;
            case PAWN_MOVEMENT_STATE_LEFT:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.strafe_left_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.strafe_left_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.strafe_left_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.strafe_left_animation_officer, 1);
                        break;
                }
            }
                break;
            case PAWN_MOVEMENT_STATE_RIGHT:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.strafe_right_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.strafe_right_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.strafe_right_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.strafe_right_animation_officer, 1);
                        break;
                }
                break;
            }
            case PAWN_MOVEMENT_STATE_JUMP_UP:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.jump_up_animation, 0);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.jump_up_animation_anarchist, 0);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.jump_up_animation_native, 0);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.jump_up_animation_officer, 0);
                        break;
                }
                break;
            }
            case PAWN_MOVEMENT_STATE_JUMP_DOWN:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.jump_down_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.jump_down_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.jump_down_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.jump_down_animation_officer, 1);
                        break;
                }
                break;
            }
            case PAWN_MOVEMENT_STATE_BACKWARD:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.run_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.run_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.run_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.run_animation_officer, 1);
                        break;
                }
                break;
            }
            case PAWN_MOVEMENT_STATE_DASH:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.dash_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.dash_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.dash_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.dash_animation_officer, 1);
                        break;
                }
                break;
            }
            case PAWN_MOVEMENT_STATE_DASH_BACK:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.dashback_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.dashback_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.dashback_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.dashback_animation_officer, 1);
                        break;
                }
                break;
            }
            /* When initialized, an enemy character's animation is
             * NUM_PAWN_MOVEMENT_STATES + 1 */
            default:
            {
                switch (character_type)
                {
                    case CHARACTER_TYPE_PIRATE:
                        Animator_playAnimation(an, &ig_assets.stand_animation, 1);
                        break;
                    case CHARACTER_TYPE_ANARCHIST:
                        Animator_playAnimation(an, &ig_assets.stand_animation_anarchist, 1);
                        break;
                    case CHARACTER_TYPE_NATIVE:
                        Animator_playAnimation(an, &ig_assets.stand_animation_native, 1);
                        break;
                    case CHARACTER_TYPE_OFFICER:
                        Animator_playAnimation(an, &ig_assets.stand_animation_officer, 1);
                        break;
                }
                break;
            }
        }

        pd->movement_state = state;
    }
}

int
initGame(int argc, char **argv)
{
    max_input_replays   = MAX_INPUT_REPLAYS;
    max_input_rollback  = MAX_INPUT_ROLLBACK;
    position_x_limit    = 5000.f;
    position_y_limit    = 5000.f;
    position_z_limit    = 5000.f;
    network_tick_rate   = 30;

    if (parseConfigFile("config.cfg", _cfgCallback))
        {DEBUG_PRINTF("config.cfg not found.\n");}
    else
        {DEBUG_PRINTF("config.cfg found.\n");}

    srand((uint)time(0));

    int flags = INIT_FLAG_RESIZABLE_WINDOW | INIT_FLAG_WINDOW_MAXIMIZED;
    int error = kys_init(argc, argv, "Karistys", MEGABYTES(256), MEGABYTES(32),
        flags, 1024, 768);

    if (error != 0)
        return 1;

    InputBuffer_init(&input_buffer);

    if (MessageBuffer_init() != 0)
    {
        DEBUG_PRINTF("Failed to init message buffer.\n");
        return 2;
    }

    char *vertexcode    = readTextFileToBuffer("shaders/opaque_gouraud.vert");
    char *fragcode      = readTextFileToBuffer("shaders/opaque_gouraud.frag");
    CEL_SHADER_OPAQUE = Renderer_createShader3D(&engine.renderer,
        SHADER3D_TYPE_OPAQUE_OUTLINE, vertexcode, fragcode);
    kfree(fragcode);
    kfree(vertexcode);

    vertexcode = readTextFileToBuffer("shaders/opaque_gouraud.vert");
    fragcode = readTextFileToBuffer("shaders/transparent_gouraud.frag");

    CEL_SHADER_TRANSPARENT = Renderer_createShader3D(&engine.renderer,
        SHADER3D_TYPE_OPAQUE_OUTLINE, vertexcode, fragcode);

    kfree(fragcode);
    kfree(vertexcode);

    if (AssetPool_init(&game.ingame_asset_pool, 32, 32, 6000, 6000, 64, 64, 32,
        512, INGAME_ASSETPOOL_MEMORY_SIZE))
        return 3;

    game.scene_memory = allocatePermanentMemory(LEVEL_SCENE_MEMORY_SIZE);

    if (!game.scene_memory)
    {
        DEBUG_PRINTF("Failed to allocate enough memory for the level scene!\n");
        return 4;
    }

    game.scene_memory_size = LEVEL_SCENE_MEMORY_SIZE;

    key_config.move_forward     = SDL_SCANCODE_W;
    key_config.move_left        = SDL_SCANCODE_A;
    key_config.move_right       = SDL_SCANCODE_D;
    key_config.move_back        = SDL_SCANCODE_S;
    key_config.toggle           = SDL_SCANCODE_F;
    key_config.jump             = SDL_SCANCODE_SPACE;
    key_config.dash             = MOUSE_BUTTON_RIGHT;
    key_config.shoot            = MOUSE_BUTTON_LEFT;
    key_config.weaponswap_next  = SDL_SCANCODE_E;
    key_config.weaponswap_prev  = SDL_SCANCODE_Q;
    key_config.show_stat_window = SDL_SCANCODE_TAB;

    graphics_config.field_of_view = (float)DEFAULT_FOV;
    graphics_config.draw_distance = (float)DEFAULT_VIEW_DISTANCE;

    if (createLoadingScreen() != 0) return 5;

    Mutex_init(&game.latest_verified_input_mutex);
    SimQueue_init();

    initMenuWindowStyle();
    initStatWindowStyle();

    setRenderBackgroundColor(0.0f, 0.0f, 0.0f, 1.0f);

    setVsync(1);
    game.screen_after_load = MENU_SCREEN;
    printf("screen after load: %d\n", game.screen_after_load);

    return 0;
}

static int
_onReceiveMessageCallback(Client *c, message_t msg_type, uint16_t msg_id,
    void *msg, int size, bool32 received_before)
{
    #define CHECK_SIZE(_type) \
        if (size < (int)_type##_SIZE) return -1;

    char *data = (char*)msg;

    switch (msg_type)
    {
        case KS_SC_MSG_ATTACHED_TO_GAME:
        {
            CHECK_SIZE(KS_SC_MSG_ATTACHED_TO_GAME);

            if (!received_before)
            {
                char map_id = *data;
                DEBUG_PRINTF("Client was attached to map id %i by server.\n",
                    (int)map_id);
                game.multiplayer_state = MP_STATE_LOADING;
            }

            return KS_SC_MSG_ATTACHED_TO_GAME_SIZE;
        }
            break;
        case KS_SC_MSG_OWN_PAWN_INFORM:
        {
            CHECK_SIZE(KS_SC_MSG_OWN_PAWN_INFORM);

            if (!received_before)
            {
                if (game.mp_data.pawn)
                {
                    DEBUG_PRINTF("Server informed of pawn, but already had one!\n");
                    return KS_SC_MSG_OWN_PAWN_INFORM_SIZE;
                }

                ByteBuffer buf; ByteBuffer_init(&buf, msg,
                    KS_SC_MSG_OWN_PAWN_INFORM_SIZE);

                replication_id32_t  id;
                char                character_type;
                vec3                position;

                ByteBuffer_read(&buf, replication_id32_t, &id);
                ByteBuffer_read(&buf, char, &character_type);
                ByteBuffer_readBytes(&buf, &position, sizeof(vec3));

                if (id != ENTITY_NOT_REPLICATED)
                {
                    Mutex_lock(&game.game_scene.entity_access_mutex);
                    game.mp_data.pawn = Scene3D_createEntity(&game.game_scene,
                        position[0], position[1], position[2]);
                    ASSERT(game.mp_data.pawn);
                    Entity_makeExistingReplicated(game.mp_data.pawn, id);
                    client_initPlayerPawn(game.mp_data.pawn);

                    Mutex_unlock(&game.game_scene.entity_access_mutex);

                    DEBUG_PRINTF("Received pawn data for player - new "
                        "replication id: %u!\n", id);
                }
                else
                {
                    DEBUG_PRINTF("Received pawn data, but it's replication id "
                        "was invalid!\n");
                }
            }

            return KS_SC_MSG_OWN_PAWN_INFORM_SIZE;
        }
            break;
        case KS_SC_MSG_OTHERS_PAWN_INFORM:
        {
            CHECK_SIZE(KS_SC_MSG_OTHERS_PAWN_INFORM);

            if (!received_before)
            {
                DEBUG_PRINTF("Got informed of another pawn!\n");

                Mutex_lock(&game.game_scene.entity_access_mutex);

                replication_id32_t rid;
                char character_type;
                vec3 position, velocity;
                ubyte orientation;

                ByteBuffer buf; ByteBuffer_init(&buf, msg,
                    KS_SC_MSG_OTHERS_PAWN_INFORM_SIZE);
                ByteBuffer_read(&buf, replication_id32_t, &rid);
                ByteBuffer_read(&buf, char, &character_type);
                ByteBuffer_readBytes(&buf, position, sizeof(vec3));
                ByteBuffer_readBytes(&buf, velocity, sizeof(vec3));
                ByteBuffer_read(&buf, ubyte, &orientation);

                mp_createOtherPlayersPawn(&game.game_scene, rid, position,
                    velocity, character_type);

                Mutex_unlock(&game.game_scene.entity_access_mutex);
            }

            return KS_SC_MSG_OTHERS_PAWN_INFORM_SIZE;
        }
            break;
        case KS_SC_MSG_OTHERS_PAWN_REMOVED:
        {
            CHECK_SIZE(KS_SC_MSG_OTHERS_PAWN_REMOVED);

            if (!received_before)
            {
                replication_id32_t ri = *(replication_id32_t*)data;

                Entity *e = ReplicationTable_get(
                    &game.game_scene.replication_table, ri);

                if (ri)
                    SimQueue_queueRemoveEntityTask(e);

                DEBUG_PRINTF("Removed another player's pawn on server "
                    "request.\n");
            }

            return KS_SC_MSG_OTHERS_PAWN_REMOVED_SIZE;
        }
            break;
        case KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE:
        {
            CHECK_SIZE(KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE);

            if (!received_before)
            {
                replication_id32_t  rid;
                vec3                pos, vel;
                ubyte               orientation;
                char                weapon_and_animation;

                ByteBuffer buf; ByteBuffer_init(&buf, msg,
                    KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE_SIZE);

                ByteBuffer_read(&buf, replication_id32_t, &rid);
                ByteBuffer_readBytes(&buf, &pos, sizeof(vec3));
                ByteBuffer_readBytes(&buf, &vel, sizeof(vec3));
                ByteBuffer_read(&buf, ubyte, &orientation);
                ByteBuffer_read(&buf, char, &weapon_and_animation);

                union WeaponAndAnimation
                {
                    struct
                    {
                        int weapon:3;
                        int movement_state: 5;
                    } data;
                };

                union WeaponAndAnimation *wa_byte =
                    (union WeaponAndAnimation*)&weapon_and_animation;

                Mutex_lock(&game.game_scene.entity_access_mutex);

                Entity *e = Scene3D_getReplicatedEntity(&game.game_scene, rid);

                if (e)
                {
                    Transform *t            = &Entity_getTransform(e)->transform;
                    PhysicsComponent *pc    = Entity_getPhysicsComponent(e);

                    vec3_copy(t->pos, pos);
                    vec3_copy(pc->velocity, vel);

                    const float full_rad = (float)(2.0f * M_PI);
                    t->rot[1] = ((float)orientation / 255.0f) \
                        * full_rad;

                    mp_setOtherPawnMovementStateAndAnimation(e,
                        wa_byte->data.movement_state);
                }
                else
                {
                    DEBUG_PRINTF("Received a state update for an entity that "
                        "didn't exist on the client's side!\n");
                }

                Mutex_unlock(&game.game_scene.entity_access_mutex);
            }

            return KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE_SIZE;
        }
            break;
        case KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE:
        {
            CHECK_SIZE(KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE);

            if (!received_before)
            {
                Mutex_lock(&game.latest_verified_input_mutex);

                input_sequence_t seq =
                    *(input_sequence_t*)(data + 2 * sizeof(vec3));

#define CHECK_INP_SEQ_EARLY 0

#if CHECK_INP_SEQ_EARLY
                if (IS_SEQUENCE_GREATER(seq,
                    game.mp_data.latest_verified_input_sequence))
                {
#endif
                    game.mp_data.latest_verified_input_sequence = seq;
                    game.mp_data.have_new_verified_input = 1;

                    float *pos = (float*)data;

                    if (ABS(pos[0]) < position_x_limit
                    &&  ABS(pos[1]) < position_y_limit
                    &&  ABS(pos[2]) < position_z_limit)
                    {
                        vec3_copy(game.mp_data.latest_verified_position, pos);
                        vec3_copy(game.mp_data.latest_verified_velocity,
                            (float*)(data + sizeof(vec3)));
                    }
                    else
                    {
                        DEBUG_PRINTF("Received invalid position data from "
                            "server (%f, %f, %f)\n", pos[0], pos[1], pos[2]);
                    }
#if CHECK_INP_SEQ_EARLY
                }
                else
                {
                    DEBUG_PRINTF("Received an out of order verified input "
                        "sequence.\n");
                }
#endif

#undef CHECK_INP_SEQ_EARLY

                Mutex_unlock(&game.latest_verified_input_mutex);
            }

            return KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE_SIZE;
        }
            break;
        case KS_SC_MSG_SNAP_OWN_PAWN_STATE:
        {
            CHECK_SIZE(KS_SC_MSG_SNAP_OWN_PAWN_STATE);

            if (!received_before)
            {
                SimQueue_queueSnapPawnStateTask((float*)msg, 
                    (float*)msg + 3);
            }

            return KS_SC_MSG_SNAP_OWN_PAWN_STATE_SIZE;
        }
            break;
        case KS_SC_MSG_OTHER_PAWN_SWAPPED_WEAPON:
        {
            CHECK_SIZE(KS_SC_MSG_OTHER_PAWN_SWAPPED_WEAPON);

            if (!received_before)
            {
                DEBUG_PRINTF("Entity %u swapped weapon to %i\n",
                    *(replication_id32_t*)msg, (int)*((char*)msg +
                    sizeof(replication_id32_t)));
                SimQueue_queueSwapOtherPawnWeaponTask((int)*((char*)msg +
                    sizeof(replication_id32_t)),
                    *(replication_id32_t*)msg);
            }

            return KS_SC_MSG_OTHER_PAWN_SWAPPED_WEAPON_SIZE;
        }
            break;
        case KS_SC_MSG_OWN_HEALTH_INFORM:
        {
            CHECK_SIZE(KS_SC_MSG_OWN_HEALTH_INFORM);

            if (!received_before)
            {
                Mutex_lock(&game.game_scene.entity_access_mutex);
                int new_health = (int)*(char*)msg;
                if (game.mp_data.health > new_health)
                {
                    ScriptComponent *sc = Entity_getScriptComponent(mp_getPlayerPawn());
                    PlayerScriptData *pd = (PlayerScriptData*)sc->data;

                    pd->base.is_hit = 1;
                    pd->base.is_hit_timer = HITSCREEN_DURATION / 4;
                }
                game.mp_data.health = new_health;
                Mutex_unlock(&game.game_scene.entity_access_mutex);
            }

            return KS_SC_MSG_OWN_HEALTH_INFORM_SIZE;
        }
            break;
        case KS_SC_MSG_PAWN_DEATH_INFORM:
        {
            CHECK_SIZE(KS_SC_MSG_PAWN_DEATH_INFORM);

            if (!received_before)
            {
                Mutex_lock(&game.game_scene.entity_access_mutex);

                ScriptComponent *sc = Entity_getScriptComponent(mp_getPlayerPawn());
                PlayerScriptData *pd = (PlayerScriptData*)sc->data;

                pd->base.is_hit = 1;
                pd->base.is_hit_timer = HITSCREEN_DURATION;

                Mutex_unlock(&game.game_scene.entity_access_mutex);
                DEBUG_PRINTF("Received a pawn death inform!\n");
            }

            return KS_SC_MSG_PAWN_DEATH_INFORM_SIZE;
        }
            break;
        case KS_SC_MSG_ITEM_SPAWNED:
        {
            CHECK_SIZE(KS_SC_MSG_ITEM_SPAWNED);

            if (!received_before)
            {
                char index = *data;
                char type  = *(data + 1);
                float *pos = (float*)(data + 2);

                if (type >= NUM_ITEM_TYPES || index >= MAX_ITEMS_PER_LEVEL)
                    DEBUG_PRINTF("Received invalid data for an item spawn!\n");
                else
                    SimQueue_queueSpawnItemTask((int)index, (int)type, pos);

                DEBUG_PRINTF("Received item! type: %i\n", (int)type);
            }

            return KS_SC_MSG_ITEM_SPAWNED_SIZE;
        }
            break;
        case KS_SC_MSG_FRAG_COUNT:
        {
            CHECK_SIZE(KS_SC_MSG_FRAG_COUNT);
            DEBUG_PRINTF("New frag count received: %u\n", *(uint*)msg);
            game.mp_data.num_frags = *(uint*)msg;
            return KS_SC_MSG_FRAG_COUNT_SIZE;
        }
            break;
        case KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN:
        {
            CHECK_SIZE(KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN);

            DEBUG_PRINTF("Received an item-collided-with-pawn message.\n");

            int index   = (int)*(data);
            int type    = (int)*(data + 1);
            replication_id32_t rid = *(replication_id32_t*)(data + 2);

            if (type >= 0 && type < NUM_ITEM_TYPES
            && index >= 0 && index < MAX_ITEMS_PER_LEVEL
            && rid != ENTITY_NOT_REPLICATED)
            {
                SimQueue_queueCollideItemTask(index, type, rid);
            }
            else
            {
                DEBUG_PRINTF("Item-collided-with-pawn message data was "
                    "invalid!\n");
            }

            return KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN_SIZE;
        }
            break;
        case KS_SC_MSG_FRAG_INFORM:
        {
            CHECK_SIZE(KS_SC_MSG_FRAG_INFORM);

            if (!received_before)
            {
                replication_id32_t fragger = *(replication_id32_t*)data;
                replication_id32_t fraggee = *((replication_id32_t*)data + 1);

                if (mp_getPlayerPawn())
                {
                    if (mp_getPlayerPawn()->replication_id == fragger
                    && fraggee != mp_getPlayerPawn()->replication_id)
                    {
                        MPGameScreen_playFragText();
                        ++game.mp_data.num_frags;
                    }
                    else if (mp_getPlayerPawn()->replication_id != fragger
                    && mp_getPlayerPawn()->replication_id == fraggee)
                    {
                        MPGameScreen_playServedText();
                        Mutex_lock(&game.game_scene.entity_access_mutex);
                        ScriptComponent *sc = Entity_getScriptComponent(mp_getPlayerPawn());
                        PlayerScriptData *pd = (PlayerScriptData*)sc->data;

                        pd->base.is_hit = 1;
                        pd->base.is_hit_timer = HITSCREEN_DURATION;
                        Mutex_unlock(&game.game_scene.entity_access_mutex);
                        /* Not sure if this is threadsafe... */
                        SoundEffect_play(permanent_assets.se_death_scream1,
                            SOUND_CHANNEL_SCREAM, 0);
                    }
                }
            }

            return KS_SC_MSG_FRAG_INFORM_SIZE;
        }
            break;
        case KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER:
        {
            CHECK_SIZE(KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER);

            if (!received_before)
            {
                Mutex_lock(&game.game_scene.entity_access_mutex);

                Entity *e = Scene3D_getReplicatedEntity(
                    &game.game_scene, *(replication_id32_t*)data);

                if (e)
                {
                    mp_shootAsOtherPawnRocketLauncher(e,
                        (float*)(data + sizeof(replication_id32_t)),
                        (float*)(data + sizeof(replication_id32_t) + sizeof(vec3)));
                }

                Mutex_unlock(&game.game_scene.entity_access_mutex);
            }

            return KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER_SIZE;
        }
        break;

        case KS_SC_MSG_OTHER_PAWN_SHOT_SMG:
        {
            CHECK_SIZE(KS_SC_MSG_OTHER_PAWN_SHOT_SMG);

            if (!received_before)
            {
                Mutex_lock(&game.game_scene.entity_access_mutex);

                Entity *e = Scene3D_getReplicatedEntity(&game.game_scene,
                    *(replication_id32_t*)data);

                if (e)
                {
                    mp_shootAsOtherPawnSMG(e,
                        (float*)(data + sizeof(replication_id32_t)),
                        (float*)(data + sizeof(replication_id32_t) + sizeof(vec3)));
                }

                Mutex_unlock(&game.game_scene.entity_access_mutex);
            }

            return KS_SC_MSG_OTHER_PAWN_SHOT_SMG_SIZE;
        }
            break;
        case KS_SC_MSG_OTHER_PAWN_SHOT_RAILGUN:
        {
            CHECK_SIZE(KS_SC_MSG_OTHER_PAWN_SHOT_RAILGUN);

            if (!received_before)
            {
                Mutex_lock(&game.game_scene.entity_access_mutex);

                Entity *e = Scene3D_getReplicatedEntity(&game.game_scene,
                    *(replication_id32_t*)data);

                if (e)
                {
                    mp_shootAsOtherPawnRailgun(e,
                        (float*)(data + sizeof(replication_id32_t)),
                        (float*)(data + sizeof(replication_id32_t) + sizeof(vec3)));
                }

                Mutex_unlock(&game.game_scene.entity_access_mutex);
            }

            return KS_SC_MSG_OTHER_PAWN_SHOT_RAILGUN_SIZE;
        }
            break;
        case KS_SC_MSG_OTHER_PAWN_HIT_MACHETE:
        {
            CHECK_SIZE(KS_SC_MSG_OTHER_PAWN_HIT_MACHETE);

            if (!received_before)
            {
                Mutex_lock(&game.game_scene.entity_access_mutex);

                Entity *e = Scene3D_getReplicatedEntity(&game.game_scene,
                    *(replication_id32_t*)data);

                if (e)
                {
                    DEBUG_PRINTF("MAKE MACHETE SHOOTING\n");
                }

                Mutex_unlock(&game.game_scene.entity_access_mutex);
            }

            return KS_SC_MSG_OTHER_PAWN_HIT_MACHETE_SIZE;
        }
            break;
        case KS_SC_MSG_OTHERS_ROCKET_EXPLOSION:
        {
            CHECK_SIZE(KS_SC_MSG_OTHERS_ROCKET_EXPLOSION);

            if (!received_before)
            {
                DEBUG_PRINTF("ROCKET EXPLOSION!\n");
                SimQueue_queueRocketExplosion((float*)data);
            }
            return KS_SC_MSG_OTHERS_ROCKET_EXPLOSION_SIZE;
        }
            break;
    }

    DEBUG_PRINTF("_onReceiveMessageCallback(): unknown message type %u!\n",
        (uint)msg_type);

    return -1;

    #undef CHECK_SIZE
}

void
initMenuWindowStyle()
{
    default_window_style->show_title           = 0;
    default_window_style->font                 = getDefaultSpriteFont();
    default_window_style->border_width_top     = 2;
    default_window_style->border_width_left    = 2;
    default_window_style->border_width_right   = 2;
    default_window_style->border_width_bottom  = 2;

    setColorf(default_window_style->color_background_normal,
        0.05f, 0.05f, 0.05f, 1.00f);
    setColorf(default_window_style->color_background_active,
        0.05f, 0.05f, 0.05f, 1.00f);
    setColorf(default_window_style->color_border_normal,
        0.635f, 0.287f, 0.067f, 1.00f);
    setColorf(default_window_style->color_border_active,
        0.635f, 0.287f, 0.067f, 1.00f);
}

void
initStatWindowStyle()
{
    stat_window_style.show_title = 0;
    stat_window_style.font = getDefaultSpriteFont();
    stat_window_style.border_width_top = 2;
    stat_window_style.border_width_left = 2;
    stat_window_style.border_width_right = 2;
    stat_window_style.border_width_bottom = 2;

    setColorf(stat_window_style.color_background_normal,
        0.05f, 0.05f, 0.05f, 0.40f);
    setColorf(stat_window_style.color_background_active,
        0.05f, 0.05f, 0.05f, 0.40f);
    setColorf(stat_window_style.color_border_normal,
        0.635f, 0.287f, 0.067f, 0.00f);
    setColorf(stat_window_style.color_border_active,
        0.635f, 0.287f, 0.067f, 0.00f);
}

void
drawGameMenu(bool32 *is_menu_visible)
{
#       define pa permanent_assets

        static const int menu_w = 356;
        static const int menu_h = 256;

        GUI_beginEmptyWindow("menu", getWindowWidth() / 2 - menu_w / 2,
            getWindowHeight() / 2 - menu_h / 2, menu_w, menu_h);

        const float bscale = 0.5f;

        GUI_setNextNormalButtonTextureParams(pa.tex_button_sheet1,
            pa.clip_resume_n, 0, 0, bscale, bscale);

        GUI_setNextHoveredButtonTextureParams(pa.tex_button_sheet1,
            pa.clip_resume_h, 0, 0, bscale, bscale);

        if (GUI_button("##resume",
            menu_w / 2 - (int)((float)pa.clip_resume_n[2] * bscale) / 2,
            10,
            pa.clip_resume_n[2],
            pa.clip_resume_n[3]))
        {
            *is_menu_visible = 0;
            setRelativeMouseMode(1);
        }

        GUI_setNextNormalButtonTextureParams(permanent_assets.tex_button_sheet1,
            pa.clip_ragequit_n, 0, 0, bscale, bscale);

        GUI_setNextHoveredButtonTextureParams(pa.tex_button_sheet1,
            pa.clip_ragequit_h, 0, 0, bscale, bscale);

        if (GUI_button("##quit",
            menu_w / 2 - (int)((float)pa.clip_ragequit_n[2] * bscale) / 2,
            10 + (int)((float)pa.clip_resume_n[3] * bscale),
            pa.clip_ragequit_n[2],
            pa.clip_ragequit_n[3]))
        {
            setScreen(game.screens[MENU_SCREEN]);
        }

        GUI_endWindow();

#       undef pa
}

static void
_cfgCallback(const char *opt, const char *val)
{
    if (!strcmp(opt, "max input replays"))
    {
        int num = atoi(val);
        if (num > 0) /* Arbitrary number */
        {
            max_input_replays = num;
            DEBUG_PRINTF("max_input_replays set to %d\n", num);
        }
    }
    else if (!strcmp(opt, "max input rollback"))
    {
        int num = atoi(val);
        if (val > 0)
        {
            max_input_rollback = num;
            DEBUG_PRINTF("max_input_rollback set to %d\n", num);
        }
    }
    else if (!strcmp(opt, "position x limit"))
    {
        int num = atoi(val);
        if (num > 0)
        {
            position_x_limit = (float)num;
            DEBUG_PRINTF("position_x_limit set to %d\n", num);
        }
    }
    else if (!strcmp(opt, "position y limit"))
    {
        int num = atoi(val);
        if (num > 0)
        {
            position_y_limit = (float)num;
            DEBUG_PRINTF("position_y_limit set to %d\n", num);
        }
    }
    else if (!strcmp(opt, "position z limit"))
    {
        int num = atoi(val);
        if (num > 0)
        {
            position_z_limit = (float)num;
            DEBUG_PRINTF("position_z_limit set to %d\n", num);
        }
    }
    else if (!strcmp(opt, "network tick rate"))
    {
        int num = atoi(val);
        if (num > 0)
        {
            network_tick_rate = num;
            DEBUG_PRINTF("network_tick_rate set to %d\n", num);
        }
    }
}
