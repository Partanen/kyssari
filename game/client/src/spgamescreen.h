#pragma once

/* Forwards declaration(s) */
typedef struct BulletType   BulletType;
typedef struct Game         Game;

typedef struct SPGameScreenData SPGameScreenData;
typedef struct EnemyData        EnemyData;
//typedef struct BulletData       BulletData;


struct EnemyData
{
    int     hp;
    int     travel_distance;
    float   speed;
};

//struct BulletData
//{
//    float   timer; // make this zero
//    float   dist_to_hit;
//    float   bullet_velocity;
//    int     damage;
//    int     wep_type;
//
//    vec3    explosion_pos;
//    vec3    last_position;
//};

struct SPGameScreenData
{
    AnimatorPool    animatorpool;

    Camera          camera;

    Entity          *player;

    int             p1_score, p2_score;
    char            player_hp_text[128], p1_score_text[128], p2_score_text[128];

    bool32          camera_follow_on;
    bool32          show_position_text;
    bool32          is_menu_visible;
};

Entity *
sp_createEnemy(Scene3D *scene3d, SPGameScreenData *screendata, Model *enemymodel,
    Material *enemymaterial, float x, float y, float z);

void
createExplosion(Scene3D *scene3d, float x, float y, float z, vec3 pos);

Entity *
createCube(Scene3D *scene3d, float x, float y, float z, float scale);

Entity *
sp_createItem(Scene3D *scene3d, SPGameScreenData *screendata, float x, float y,
    float z, float scale, float radius, enum KsItemType item_id);

void
GameScreen_update(Screen *screen);

void
GameScreen_open(Screen *screen);

void
GameScreen_close(Screen *screen);

int
createGameScreen();

void
moveEntity(Entity *entity, float speed);

int
sp_initGameSceneForLevel1();

void
sp_enemyTakeDamage(Entity *entity, int amount);

/* Returns true if shot was successful */
bool32
sp_shootAsPlayerPawn(Entity *e);

void
sp_playerScript(Entity *e, void *data);

void
sp_onShootCallback_RocketLauncher(enum KsWeaponType type, Entity *pawn);

void
sp_onShootCallback_SMG(enum KsWeaponType type, Entity *pawn);

void
sp_onShootCallback_Railgun(enum KsWeaponType type, Entity *pawn);

void
sp_onShootCallback_Machete(enum KsWeaponType type, Entity *pawn);

void
sp_enemyScript(Entity *e, void *data);

void
sp_explosionScript(Entity *entity, void *data);

void
sp_itemScript(Entity *entity, void *data);

void
sp_rocketBulletScript(Entity *entity, void *scriptdata);

void
sp_dynamicObjectBulletRay(Entity *entity, void *scripdata);

void
sp_removeCallback(PhysicsComponent *component_a,
    PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b,
    enum ColliderType collider_a_type,
    enum ColliderType collider_b_type,
    enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id);

void
sp_explosionCallback(PhysicsComponent *component_a,
    PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b,
    enum ColliderType collider_a_type,
    enum ColliderType collider_b_type,
    enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id);

void
sp_itemCollisionCallback(PhysicsComponent *component_a,
    PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b,
    enum ColliderType collider_a_type,
    enum ColliderType collider_b_type,
    enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id);

Entity *
sp_createOtherPlayersPawn(Scene3D *scene, vec3 pos, vec3 vel, char char_type);
/* Create a dummy replica of another player in the multiplayer thing */
