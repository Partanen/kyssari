#pragma once
#include "types.h"

#ifdef _WANGBLOWS
    #include "sys_win32.h"
#elif defined(_UNIX)
    #include "sys_unix.h"
#endif

typedef struct MemoryPool           MemoryPool;
typedef struct MemoryBlockHeader    MemoryBlockHeader;
typedef struct DynamicMemoryBlock   DynamicMemoryBlock;
typedef struct SimpleMemoryBlock    SimpleMemoryBlock;

 /* A large pool of memory that will persist throughout the lifetime
  * of the program once allocated. Initialization uses system calls.  */
struct MemoryPool
{
    void    *start;
    void    *offset;
    size_t  total_size;
    Mutex   mutex;
};

struct MemoryBlockHeader
{
    size_t              size;
    MemoryBlockHeader   *next;
    MemoryBlockHeader   *prev;
#ifdef _DEBUG
    bool8               is_in_use;
#endif
};

/* A smaller portion of a MemoryPool, allowing for dynamic,
 * thread-safe mallocs and frees */
struct DynamicMemoryBlock
{
    void                *start;
    size_t              total_size;
    MemoryBlockHeader   *headers;
    Mutex               mutex;
    uint32_t            id;
};

/* A simple stack allocator without a way to free memory, other
 * for moving the offset pointer back. */
struct SimpleMemoryBlock
{
    void    *start;
    void    *offset;
    size_t  total_size;
};

/* Return 0 on success, non-0 on failure */
int
MemoryPool_init(MemoryPool *pool, size_t size);

void *
MemoryPool_allocateBlock(MemoryPool *pool, size_t size);

int
DynamicMemoryBlock_init(DynamicMemoryBlock *block, MemoryPool *pool, size_t size);

/* Clear an existing block, invalidating anything it contains */
void
DynamicMemoryBlock_clear(DynamicMemoryBlock *block);

/* Note: this is threadsafe. Its also O(n) in terms of performance, so
 * not exactly very efficient */
void *
DynamicMemoryBlock_malloc(DynamicMemoryBlock *block, size_t size);

/* Leave the mutex unlocked */
void *
DynamicMemoryBlock_unsafeMalloc(DynamicMemoryBlock *block, size_t size);

void
DynamicMemoryBlock_free(DynamicMemoryBlock *block, void *target);

/* Leave the mutex unlocked */
void
DynamicMemoryBlock_unsafeFree(DynamicMemoryBlock *block, void *target);

/* Acts like the standard realloc */
void *
DynamicMemoryBlock_realloc(DynamicMemoryBlock *block, void *target, size_t size);

/* Realloc without mutex protection */
void *
DynamicMemoryBlock_unsafeRealloc(DynamicMemoryBlock *block, void *target,
    size_t size);

void
SimpleMemoryBlock_init(SimpleMemoryBlock *block,
    void *start, size_t total_size);

void *
SimpleMemoryBlock_malloc(SimpleMemoryBlock *block, size_t size);

static inline void
SimpleMemoryBlock_clear(SimpleMemoryBlock *block);

size_t
SimpleMemoryBlock_getFreeSpace(SimpleMemoryBlock *block);

static inline size_t
SimpleMemoryBlock_getAllocatedSize(SimpleMemoryBlock *block);




static inline void
SimpleMemoryBlock_clear(SimpleMemoryBlock *block)
{
    block->offset = block->start;
}

static inline size_t
SimpleMemoryBlock_getAllocatedSize(SimpleMemoryBlock *block)
{
    return (size_t)((char*)block->offset - (char*)block->start);
}
