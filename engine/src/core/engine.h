#pragma once
#include "../compile_flags.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdlib.h>
#include "types.h"
#include "memory.h"
#include "../render/renderer.h"
#include "assets.h"
#include "scene3d.h"
#include "gui.h"
#include "socket.h"

#if _CF_ALLOW_TEXT_MODE
    #ifdef _UNIX
        #include <ncurses.h>
    #elif defined(_WANGBLOWS)
        #include <curses.h>
    #endif
#endif

#define NUM_MAX_THREADS 8
#define NUM_JOBS 64
#define MOUSE_BUTTON_LEFT  SDL_BUTTON(SDL_BUTTON_LEFT)
#define MOUSE_BUTTON_RIGHT SDL_BUTTON(SDL_BUTTON_RIGHT)
#define SEQUENCE_BUFFER_SIZE 1024
#define MOUSE_UPDATE_STEP (1.0f / 60)

#if _CF_ALLOW_TEXT_MODE
    #define KYS_IS_TEXT_MODE() (engine.graphics_mode == KYS_GRAPHICS_TEXT)
#else
    #define KYS_IS_TEXT_MODE() 0
#endif

enum KysError
{
    KYS_ERROR_SDL_INIT = 1,
    KYS_ERROR_IMG_INIT,
    KYS_ERROR_TTF_INIT,
    KYS_ERROR_RENDERER_INIT,
    KYS_ERROR_SYS_MEM_ALLOC_FAILURE,
    KYS_ERROR_DYNAMIC_ALLOC_FAILURE,
    KYS_ERROR_LIB_LOAD,
    KYS_ERROR_AUDIO_INIT,
    KYS_ERROR_THREADS
};

enum InitFlag
{
	INIT_FLAG_RESIZABLE_WINDOW    = (1 << 0),
    INIT_FLAG_FULLSCREEN          = (1 << 1),
    INIT_FLAG_FULLSCREEN_DESKTOP  = (1 << 2),
    INIT_FLAG_WINDOW_MAXIMIZED    = (1 << 3),
    INIT_FLAG_TEXT_MODE           = (1 << 4)
};

enum KysGraphicsMode
{
    KYS_GRAPHICS_DESKTOP,
    KYS_GRAPHICS_TEXT
};

typedef struct Input		Input;
typedef struct Window       Window;
typedef struct Screen		Screen;
typedef struct Clock        Clock;
typedef struct ThreadJob    ThreadJob;
typedef struct ThreadPool   ThreadPool;
typedef struct Engine       Engine;

static inline bool32
isKeyPressed(SDL_Scancode scancode);

static inline double
getDelta();

static inline uint32_t
getTimePassed();

static inline long_uint
getCurrentFrameNum();

static inline void
setVsync(bool32 on);

/* Set to 0 for no limit */
static inline void
setFPSLimit(uint limit);

static inline void
renderScene3D(Scene3D *scene, Camera *camera, Viewport viewport);

static inline void *
allocatePermanentMemory(size_t size);

static inline void *
allocateDynamicMemory(size_t size);

static inline void
setRelativeMouseMode(bool32 relative);

static inline bool32
isRelativeMouseMode();

static inline int *
getMousePosition();

static inline int *
getLastMousePosition();

static inline int *
getMouseRelativeMovement();

static inline bool32
mouseMovedThisFrame();

static inline uint32_t
getMouseButtonState(int button_mask);

static inline bool32
mouseButtonDownThisFrame(int button);

static inline bool32
mouseButtonUpThisFrame(int button);

static inline DynamicMemoryBlock *
getDynamicMemoryBlock();

static inline uint
getWindowWidth();

static inline uint
getWindowHeight();

static inline void *
allocateArray(size_t len, size_t unit_size);

static inline void *
allocatePermanentArray(size_t len, size_t unit_size);

static inline void *
kmalloc(size_t size);

static inline void
kfree(void *target);

static inline void
setRenderBackgroundColor(float r, float g, float b, float a);

static inline void
setMouseSensitivity(int value);

static inline Texture *
loadTexture(const char *path);

static inline Texture *
loadTextureFromText(const char *path, Font *font,
    Color *color, int wrap_width);

static inline SoundEffect *
loadSoundEffect(const char *path);

static inline Texture *
loadCubemapTexture(
    const char *path_pos_x, const char *path_neg_x,
    const char *path_pos_y, const char *path_neg_y,
    const char *path_pos_z, const char *path_neg_z);

static inline BaseMesh *
loadBaseMesh(const char *path);

static inline Font *
loadFont(const char *path, int size);

static inline SpriteFont *
loadSpriteFont(const char *path, uint size, Color color);

static inline BaseMesh *
getDefaultCubeMesh();

static inline BaseMesh *
getDefaultTriangleMesh();

static inline BaseMesh *
getDefaultBallMesh();

static inline BaseMesh *
getDefaultPlaneMesh();

static inline SpriteFont *
getDefaultSpriteFont();

static inline void
renderSpriteGroup(SpriteGroup *group);

static inline void
renderText(const char *text, SpriteFont *font, float x, float y, float *color);

static inline void
renderTexture(Texture *texture, float x, float y);

static inline bool32
imr_begin(GLenum mode);

static inline bool32
imr_end();

static inline bool32
imr_vertex3f(float x, float y, float z);

static inline void
imr_identity();

static inline void
imr_perspective(float field_of_view, float aspect,
    float near_clip, float far_clip);

static inline void
imr_lookAt(vec3 eye, vec3 target, vec3 up);

static inline void
imr_lookAtCamera(Camera *camera);

static inline void
imr_updateMatrix();

static inline void
imr_lineWidth(float w);

static inline void
imr_color(float r, float g, float b, float a);

static inline void
renderRect(float x, float y, float w, float h,
	float r, float g, float b, float a);

static inline int
createThreadedJob(void (*func), void *args);

void
Window_handleEvent(Window *window, SDL_Event *event);

/* Flags: see enum InitFlag */
int
kys_init(int argc, char **argv, const char *window_title,
    size_t permanent_memory, size_t dynamic_memory,
    int flags, int window_width, int window_height);

int
kys_run(int argc, char **argv, Screen *first_screen);

void
kys_beginShutdown();

/* Can be called at any time, and as many times as one's heart desires (even
 * if kys_init() was not called. */
int
kys_initNetworking();

Screen *
createScreen(void (*updateAndRender)(Screen *screen),
	void *screen_data);

void
setScreen(Screen *screen);

void
Clock_init(Clock *clock, uint fps_limit);

void
Clock_tick(Clock *clock);

int
ThreadPool_initAndRun(ThreadPool *pool, uint num_threads);

void
ThreadPool_shutdown(ThreadPool *pool);

bool32
ThreadPool_haveUnfinishedWork(ThreadPool *pool);

int
ThreadPool_addWork(ThreadPool *pool, void (*func), void *args);

void
ThreadPool_doWork(ThreadPool *pool);

struct Input
{
	const Uint8 *keyboard_state;
    int         mouse_pos[2];
    int         mouse_last_pos[2];
    int         mouse_relative_movement[2];
    int         mouse_sensitivity;
    double      sensitivity_timer;
    bool32      mouse_moved_this_frame;
    uint32_t    mouse_button_state;
    uint32_t    last_mouse_button_state;
};

struct Window
{
   SDL_Window *sdl_window;
   uint       width, height;
};

struct Screen
{
	void *screen_data;

	void (*updateAndRender)(Screen *screen);
    void (*onOpen)(Screen *screen);
    void (*onClose)(Screen *screen);

    void (*updateAndRenderTextMode)(Screen *screen);
    void (*onOpenTextMode)(Screen *screen);
    void (*onCloseTextMode)(Screen *screen);
};

struct Clock
{
	double      delta;      /* Format: seconds */
    uint64_t    last_tick;
    double      fps;
    uint        target_fps;
	long_uint	frame_num;
    uint32_t    time_passed;
};

struct ThreadJob
{
    void        (*func)(void*);
    void        *args;
    ThreadJob   *next;
};

struct ThreadPool
{
    volatile bool32     running;
    Thread              threads[NUM_MAX_THREADS];
    uint                num_threads;
    Mutex               mutex;
    ConditionVariable   cvar;

    ThreadJob           jobs[NUM_JOBS];
    ThreadJob           *free;
    ThreadJob           *waiting;
    volatile int32_t    num_incomplete;
    volatile int32_t    num_threads_started;
};

#include "network.h"

struct Engine
{
	Input			        input;
	Window                  window;
    Screen                  *next_screen;
	Screen			        *current_screen;
    ThreadPool              thread_pool;
	Clock		            clock;
	volatile bool32         running;
    Renderer                renderer;
    Assets                  assets;
    AudioSystem             audio_system;
    enum KysGraphicsMode    graphics_mode;

    MemoryPool              memory;
    DynamicMemoryBlock      dynamic_memory;
    DynamicMemoryBlock      permanent_memory;
};

extern Engine engine;

static inline bool32
isKeyPressed(SDL_Scancode scancode)
{
    if (KYS_IS_TEXT_MODE()) return 0;
    return engine.input.keyboard_state[scancode];
}

static inline double
getDelta()
{
    return engine.clock.delta;
}

static inline uint32_t
getTimePassed()
{
    return engine.clock.time_passed;
}

static inline long_uint
getCurrentFrameNum()
{
    return engine.clock.frame_num;
}

static inline void
setVsync(bool32 on)
{
    SDL_GL_SetSwapInterval(on ? 1 : 0);
}

static inline void
setFPSLimit(uint limit)
{
    engine.clock.target_fps = limit;
}

static inline Texture *
loadTextureFromText(const char *text, Font *font,
    Color *color, int wrap_width)
{
    return Assets_createTextureFromText(&engine.assets,
        font, color, text, wrap_width);
}

static inline Texture *
loadCubemapTexture(const char *path_pos_x, const char *path_neg_x, 
    const char *path_pos_y, const char *path_neg_y, 
    const char *path_pos_z, const char *path_neg_z)
{
    return Assets_loadCubemapTexture(&engine.assets, path_pos_x, path_neg_x,
        path_pos_y, path_neg_y, path_pos_z, path_neg_z);
}

static inline Texture *
updateTextureFromText(Texture *texture, const char *text,
    Font *font, Color *color, int wrap_width)
{
    return Assets_updateTextureFromText(&engine.assets,
        texture, font, color, text, wrap_width);
}

static inline void
renderScene3D(Scene3D *scene, Camera *camera, Viewport viewport)
{
    Renderer_renderScene3D(&engine.renderer, scene, camera, viewport);
}

static inline void
renderSpriteGroup(SpriteGroup *group)
{
    Renderer_renderSpriteGroup(&engine.renderer, group);
}

static inline void
renderText(const char *text, SpriteFont *font, float x, float y, float *color)
{
    if (!text) return;
    Renderer_renderText(&engine.renderer, font, text, x, y, color);
}

static inline void
renderTexture(Texture *texture, float x, float y)
{
    if (!texture) return;
    SpriteBatch_drawSprite(&engine.renderer.sprite_batch,
        texture, x, y, 0);
}

#if _CF_ALLOW_IMMEDIATE_RENDER

static inline bool32
imr_begin(GLenum mode)
{
    return ImmediateRenderState_begin(&engine.renderer.immediate_state, mode);
}

static inline bool32
imr_end()
{
    return ImmediateRenderState_end(&engine.renderer.immediate_state);
}

static inline bool32
imr_vertex3f(float x, float y, float z)
{
    return ImmediateRenderState_vertex3f(&engine.renderer.immediate_state, x, y, z);
}

static inline void
imr_identity()
{
    ImmediateRenderState_identity(&engine.renderer.immediate_state);
}

static inline void
imr_perspective(float field_of_view, float aspect,
    float near_clip, float far_clip)
{
    ImmediateRenderState_perspective(&engine.renderer.immediate_state,
        field_of_view, aspect, near_clip, far_clip);
}

static inline void
imr_lookAt(vec3 eye, vec3 target, vec3 up)
{
    ImmediateRenderState_lookAt(&engine.renderer.immediate_state,
        eye, target, up);
}

static inline void
imr_lookAtCamera(Camera *camera)
{
    ImmediateRenderState_lookAtCamera(&engine.renderer.immediate_state, camera);
}

static inline void
imr_ortho(float left, float right, float bottom,
    float top, float near_clip, float far_clip)
{
    ImmediateRenderState_ortho(&engine.renderer.immediate_state,
        left, right, bottom, top, near_clip, far_clip);
}

static inline void
imr_updateMatrix()
{
    ImmediateRenderState_updateMatrix(&engine.renderer.immediate_state);
}

static inline void
imr_lineWidth(float w)
{
    engine.renderer.immediate_state.line_width = w;
}

static inline void
imr_color(float r, float g, float b, float a)
{
    ImmediateRenderState_color(&engine.renderer.immediate_state,
        r, g, b, a);
}

#else

/* Dummy immediate render functions for when they're not defined in the
 * compile options. */

static inline bool32 imr_begin(GLenum mode){return 0;}
static inline bool32 imr_end(){return 0;}
static inline bool32 imr_vertex3f(float x, float y, float z){return 0;}
static inline void imr_identity(){}
static inline void imr_perspective(float field_of_view, float aspect,
    float near_clip, float far_clip){}
static inline void imr_lookAt(vec3 eye, vec3 target, vec3 up){}
static inline void imr_lookAtCamera(Camera *camera){}
static inline void imr_ortho(float left, float right, float bottom,
    float top, float near_clip, float far_clip){}
static inline void imr_updateMatrix(){}
static inline void imr_lineWidth(float w){}
static inline void imr_color(float r, float g, float b, float a){};

#endif

static inline void
renderRect(float x, float y, float w, float h,
	float r, float g, float b, float a)
{
	float color[4] = {r, g, b, a};
	SpriteBatch_drawRect(&engine.renderer.sprite_batch, x, y, w, h, color);
}

static inline void *
allocatePermanentMemory(size_t size)
{
    return DynamicMemoryBlock_malloc(&engine.permanent_memory, size);
}

static inline void *
allocateDynamicMemory(size_t size)
{
    return DynamicMemoryBlock_malloc(&engine.dynamic_memory, size);
}

static inline void
setRelativeMouseMode(bool32 relative)
{
    SDL_SetRelativeMouseMode(relative ? SDL_TRUE : SDL_FALSE);
}

static inline void
setMouseSensitivity(int value)
{
    engine.input.mouse_sensitivity = value;
}

static inline bool32
isRelativeMouseMode()
{
    if (KYS_IS_TEXT_MODE()) return 0;
    SDL_bool res = SDL_GetRelativeMouseMode();
    return res == SDL_TRUE ? 1 : 0;
}

static inline int *
getMousePosition()
{
    return engine.input.mouse_pos;
}

static inline int *
getLastMousePosition()
{
    return engine.input.mouse_last_pos;
}

static inline int *
getMouseRelativeMovement()
{
    return engine.input.mouse_relative_movement;
}

static inline bool32
mouseMovedThisFrame()
{
    if (KYS_IS_TEXT_MODE()) return 0;
    return engine.input.mouse_moved_this_frame;
}

static inline uint32_t
getMouseButtonState(int button_mask)
{
    return engine.input.mouse_button_state & button_mask;
}

static inline bool32
mouseButtonDownThisFrame(int button)
{
    if (KYS_IS_TEXT_MODE()) return 0;
    int button_down_now = engine.input.mouse_button_state & button;
    int button_was_down = engine.input.last_mouse_button_state & button;
    return (button_down_now && !button_was_down);
}

static inline bool32
mouseButtonUpThisFrame(int button)
{
    if (KYS_IS_TEXT_MODE()) return 0;
    int button_down_now = engine.input.mouse_button_state & button;
    int button_was_down = engine.input.last_mouse_button_state & button;
    return (!button_down_now && button_was_down);
}

static inline DynamicMemoryBlock *
getDynamicMemoryBlock()
{
    return &engine.dynamic_memory;
}

static inline uint
getWindowWidth()
{
    return engine.window.width;
}

static inline uint
getWindowHeight()
{
    return engine.window.height;
}

static inline void *
allocateArray(size_t len, size_t unit_size)
{
    return DynamicMemoryBlock_malloc(&engine.dynamic_memory, len * unit_size);
}

static inline void *
allocatePermanentArray(size_t len, size_t unit_size)
{
    return DynamicMemoryBlock_malloc(&engine.permanent_memory, len * unit_size);
}

static inline void *
kmalloc(size_t size)
{
    return DynamicMemoryBlock_malloc(&engine.dynamic_memory, size);
}

static inline void
kfree(void *target)
{
    DynamicMemoryBlock_free(&engine.dynamic_memory, target);
}

static inline void
setRenderBackgroundColor(float r, float g, float b, float a)
{
    Renderer_setBackgroundColor(&engine.renderer, r, g, b, a);
}

static inline Texture *
loadTexture(const char *path)
{
    return Assets_loadTexture(&engine.assets, path);
}

static inline SoundEffect *
loadSoundEffect(const char *path)
{
    return Assets_loadSoundEffect(&engine.assets, path);
}

static inline BaseMesh *
loadBaseMesh(const char *path)
{
    return Assets_loadBaseMesh(&engine.assets, path);
}

static inline Model *
loadModel(const char *path)
{
    return Assets_loadModel(&engine.assets, path);
}

static inline Font *
loadFont(const char *path, int size)
{
    return Assets_loadFont(&engine.assets, path, size);
}

static inline SpriteFont *
loadSpriteFont(const char *path, uint size, Color color)
{
    return Assets_loadSpriteFont(&engine.assets, path, size, color);
}

static inline BaseMesh *
getDefaultCubeMesh()
{
    return &engine.assets.mesh_cube;
}

static inline BaseMesh *
getDefaultTriangleMesh()
{
    return &engine.assets.mesh_triangle;
}

static inline BaseMesh *
getDefaultBallMesh()
{
    return engine.assets.mesh_ball;
}

static inline BaseMesh *
getDefaultPlaneMesh()
{
    return &engine.assets.mesh_plane;
};

static inline Model *
getDefaultCubeModel()
{
    return &engine.assets.model_cube;
}

static inline Model *
getDefaultTriangleModel()
{
    return &engine.assets.model_triangle;
}

static inline Model *
getDefaultBallModel()
{
    return engine.assets.model_ball;
}

static inline Model *
getDefaultPlaneModel()
{
    return &engine.assets.model_plane;
};

static inline SpriteFont *
getDefaultSpriteFont()
{
    return &engine.assets.default_sprite_font;
}

static inline int
createThreadedJob(void (*func), void *args)
{
    return ThreadPool_addWork(&engine.thread_pool, func, args);
}
