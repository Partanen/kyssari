#version 330 core

layout (std140) uniform Shader3DSharedMatrices
{
    mat4 projection;
    mat4 look_at;
    vec3 camera_pos;
};

uniform mat4 model;

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 in_tex_coord;
layout (location = 2) in vec3 in_normal;

out vec3 frag_pos;
out vec2 tex_coord;
out vec3 normal;

void main(void)
{
    frag_pos    = vec3(model * vec4(pos, 1.0f));
    tex_coord   = in_tex_coord;
    normal      = normalize(mat3(transpose(inverse(model))) * in_normal);
    gl_Position = projection * look_at * model * vec4(pos, 1.0f);
}
