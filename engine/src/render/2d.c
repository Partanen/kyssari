#include "2d.h"
#include "../core/engine.h"

VECTOR_DEFINITION(SpriteData);

int
SpriteGroup_init(SpriteGroup *group, size_t num_initial_sprites,
    DynamicMemoryBlock *block)
{
    size_t num_sub_arrays = num_initial_sprites / SPRITE_GROUP_SUB_ARRAY_SIZE;
    if (num_initial_sprites % SPRITE_GROUP_SUB_ARRAY_SIZE > 0)
        ++num_sub_arrays;

    ArrayList_Sprite_init(&group->sprites, SPRITE_GROUP_SUB_ARRAY_SIZE,
        num_sub_arrays, block);
    ArrayList_Sprite_resize(&group->sprites, SPRITE_GROUP_SUB_ARRAY_SIZE);
    Vector_SpriteData_init(&group->sprite_data, num_initial_sprites, block);

    Sprite *sprite;

    for (size_t i = 0; i < group->sprites.capacity - 1; ++i)
    {
        sprite          = ArrayList_Sprite_at(&group->sprites, i);
        sprite->next    = ArrayList_Sprite_at(&group->sprites, i + 1);
        sprite->group   = group;
    }

    ArrayList_Sprite_at(&group->sprites, group->sprites.num_items - 1)->next = 0;
    ArrayList_Sprite_at(&group->sprites, group->sprites.num_items - 1)->group = group;
    group->free = ArrayList_Sprite_at(&group->sprites, 0);
    group->reserved = 0;

    group->use_viewport_override = 0;
    group->background_color[0] = 0.0f;
    group->background_color[1] = 0.0f;
    group->background_color[2] = 0.0f;
    group->background_color[3] = 0.0f;

    return 0;
}

void
SpriteGroup_setViewport(SpriteGroup *group, Viewport viewport)
{
    if (viewport)
    {
        group->viewport[0] = 0;
        group->viewport[1] = 0;
        group->viewport[2] = 0;
        group->viewport[3] = 0;
        group->use_viewport_override = 1;
    }
    else
    {
        group->use_viewport_override = 0;
    }
}

Sprite *
SpriteGroup_reserve(SpriteGroup *group)
{
    if (!group->free)
    {
        group->free = ArrayList_Sprite_pushEmpty(&group->sprites);
        Sprite *sprite1, *sprite2, *sprite3;
        for (size_t i = group->sprites.num_items; i < group->sprites.capacity - 1; ++i)
        {
            sprite1 = ArrayList_Sprite_at(&group->sprites, i);
            sprite2 = ArrayList_Sprite_at(&group->sprites, i + 1);
            sprite3 = ArrayList_Sprite_at(&group->sprites, i - 1);
            sprite1->next = sprite2;
            sprite1->prev = sprite3;
            sprite1->index = i;
            sprite1->group = group;
        }

        /* Last sprite */
        sprite1 = ArrayList_Sprite_at(&group->sprites, group->sprites.capacity - 1);
        sprite1->index  = group->sprites.capacity - 1;
        sprite1->prev   = ArrayList_Sprite_at(&group->sprites, sprite1->index - 1);
        sprite1->next   = 0;
        sprite1->group  = group;

        /* First sprite */
        sprite1 = ArrayList_Sprite_at(&group->sprites, group->sprites.capacity - 1);
        sprite1->index  = group->sprites.capacity - group->sprites.sub_array_size;
        sprite1->prev = 0;
        sprite1->next = ArrayList_Sprite_at(&group->sprites, sprite1->index + 1);
        sprite1->group  = group;
    }

    Sprite *ret = group->free;

    group->free = ret->next;
    ret->next = group->reserved;
    group->reserved = ret;

    ret->data_index = group->sprite_data.num_items;
    SpriteData *data = Vector_SpriteData_pushEmpty(&group->sprite_data);
    data->sprite_index = ret->index;

    data->flip      = SPRITE_FLIP_NONE;
    data->rot       = 0.0f;
    data->scale[0]  = 1.0f;
    data->scale[1]  = 1.0f;
    data->color[0]  = 1.0f;
    data->color[1]  = 1.0f;
    data->color[2]  = 1.0f;
    data->color[3]  = 1.0f;
    data->texture   = 0;
    data->layer     = 0;
    data->visible   = 1;

    group->need_sort = 1;

    return ret;
}

void
Sprite_free(Sprite *sprite)
{
    sprite->prev->next = sprite->next;
    sprite->next->prev = sprite->prev;
    sprite->prev = 0;
    sprite->next = sprite->group->free;
    sprite->group->free = sprite;
}
