#include "socket.h"

void
Address_init(Address *addr,
    ubyte x, ubyte y, ubyte z, ubyte w,
    ushort port)
{
    addr->address   = (x << 24) | (y << 16) | (z << 8) | w;
    addr->port      = port;

    memset(&addr->addr_in, 0, sizeof(addr->addr_in));
    addr->addr_in.sin_family        = AF_INET;
    addr->addr_in.sin_port          = htons(port);
    addr->addr_in.sin_addr.s_addr   = htonl(addr->address);
}

void
Address_initFromUint32(Address *addr, uint32_t xyzw, ushort port)
{
    char *p = (char*)&xyzw;
    Address_init(addr, p[0], p[1], p[2], p[3], port);
}

Address
createAddress(ubyte x, ubyte y, ubyte z, ubyte w, ushort port)
{
    Address addr;
    Address_init(&addr, x, y, z, w, port);
    return addr;
}

int
Socket_open(Socket *sock)
{
    if (sock->open)
        Socket_close(sock);

    sock->fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (sock->fd <= 0)
    {
        char buf[512];
        int error_len = sys_getSockErrorString(buf, 512);

        if (error_len > 0)
            DEBUG_PRINTF("socket() error: %s\n", buf);
        else
            DEBUG_PRINTF("socket() error.\n");

        return SOCK_ERROR_CREATE;
    }

    int y = 1;
    if (setsockopt(sock->fd, SOL_SOCKET, SO_REUSEADDR,
        (const char *)&y, sizeof(y)) < 0)
    {
        DEBUG_PRINTF("setsockopt() error: setting socket reusable failed.");
        return SOCK_ERROR_SETTING_OPTIONS;
    }

    sock->port = 0;
    sock->open = 1;

    return 0;
}

int
Socket_bind(Socket *sock, ushort port)
{
    if (!sock->open)
    {
        DEBUG_PRINTF("Socket_bind() error: socket not open.");
        return SOCK_ERROR_NOT_OPEN;
    }

    if (port < 1024)
    {
        DEBUG_PRINTF("Socket_bind() error: port given is a system port.\n");
        return SOCK_ERROR_BAD_PORT;
    }

    struct sockaddr_in addr;
    addr.sin_family         = AF_INET;
    addr.sin_addr.s_addr    = INADDR_ANY;
    addr.sin_port           = htons(port);

    if (bind(sock->fd,
        (struct sockaddr*)&addr,
        sizeof(struct sockaddr_in)) < 0)
    {
        char buf[512];
        int error_len = sys_getSockErrorString(buf, 512);

        if (error_len > 0)
            DEBUG_PRINTF("bind() error: %s\n", buf);
        else
            DEBUG_PRINTF("bind() error.");

        sys_closeSocket(sock->fd);

        return SOCK_ERROR_BIND;
    }

    sock->port = port;

    return 0;
}

int
Socket_setNonBlocking(Socket *sock)
{
    if (sys_setSocketNonBlocking(sock->fd) != 0)
    {
        DEBUG_PRINTF("Failed to set socket non blocking!\n");
        return SOCK_ERROR_SETTING_OPTIONS;
    }

    return 0;
}

void
Socket_close(Socket *sock)
{
    if (!sock->open)
        return;

    sys_closeSocket(sock->fd);
    sock->open = 0;
}

int
Socket_receiveFrom(Socket *socket,
    char *buf, size_t buf_size,
    Address *from)
{
    socklen_t sender_len = sizeof(from->addr_in);

    memset(&from->addr_in, 0, sizeof(from->addr_in));
    from->addr_in.sin_family = AF_INET;
    int num_bytes = recvfrom(socket->fd, buf, buf_size, 0,
        (struct sockaddr*)&from->addr_in, &sender_len);

    from->address = ntohl(from->addr_in.sin_addr.s_addr);
    from->port    = ntohs(from->addr_in.sin_port);

#if 0
    from->addr_in.sin_addr.s_addr   = sender.sin_addr.s_addr;//htonl(from->address);
    from->addr_in.sin_port          = sender.sin_port;//htons(from->port);
#endif

    return num_bytes;
}

