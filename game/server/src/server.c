#include "server.h"
#include "../../../engine/src/core/engine.h"
#include "../../shared/shared.c"
#include "../../shared/packets.h"

const char *_true_str   = "true";
const char *_false_str  = "false";
#define BOOL_STR(_arg)(_arg ? _true_str : _false_str)

static thread_ret_t
_Game_simulationMain(void *args);

static bool32
_onConnectionRequestCallback(Server *s, Address *a);

static void
_onAcceptClientCallback(Server *s, ServerClient *sc);

static void
_onClientDisconnectedCallback(Server *s, ServerClient *sc);

static int
_onReceiveMessageCallback(Server *s, ServerClient *sc, message_t msg_type,
    uint16_t msg_id, void *msg, int size, bool32 received_before);

static void
_onUpdateCallback(Server *s);

static inline void
_pushPlayerSnapshot(Game *g, Player *p, vec3 pos);

static void
_MainMenu_redrawTextModeBaseWindow();

static void
_MainMenu_Text_redrawNetworkWindow();

static void
_MainMenu_Text_redrawHelpWindow();

static void
_MainMenu_Text_updateNetworkWindow();

static void
_informPlayersOfItemRespawn(int index, enum KsItemType type,  vec3 pos);

static void
_informPlayersOfItemCollision(int index, enum KsItemType type,
    replication_id32_t pawn_rid);

static void
_updateScene(double dt);

MainMenu            main_menu                   = {0};
Server              ks_server                   = {0};
struct GameData     game_data                   = {0};
Game                ks_game                     = {0};
Screen              *ks_main_menu_screen        = 0;
AssetPool           ks_ingame_assets            = {0};
WeaponType          weapon_types[NUM_BASE_WEAPON_TYPES];
int                 input_diff_tolerance        = 8;
int                 input_diff_rollback         = 4;

int                 network_tick_rate;

int main(int argc, char **argv)
{
    int kys_init_flags = INIT_FLAG_RESIZABLE_WINDOW;

    if (kys_init(argc, argv, "Karistys server", PERMANENT_MEMORY_AMOUNT,
        DYNAMIC_MEMORY_AMOUNT, kys_init_flags, 800, 600) != 0)
    {
        return 1;
    }

    if (ks_init() != 0)
        return 2;

    if (kys_run(argc, argv, ks_main_menu_screen) != 0)
        return 3;

    return 0;
}

static void
_readConfigCallback(const char *opt, const char *val)
{
    if (!strcmp(opt, "network tick rate"))
    {
        int num = atoi(val);
        if (num > 0) network_tick_rate = (float)num;
    }
}

int
ks_init()
{
    srand((uint)time(0));

    network_tick_rate = 30;
    parseConfigFile("server_config.cfg", _readConfigCallback);

    if (initMainMenu() != 0)                return 1;
    if (kys_initNetworking() != 0)
    {
        char log[512];
        int len = sys_getSockErrorString(log, 512);
        if (len)
            DEBUG_PRINTF("Failed to init networking - error: %s\n", log);
        else
            DEBUG_PRINTF("Failed to init networking.\n");
        return 2;
    }

    int server_res = Server_init(&ks_server, 4);
    if (server_res != 0)
    {
        DEBUG_PRINTF("ks_init(): failed to init server - error %i.\n",
            server_res);
        return 3;
    }

    Server_setFPS(&ks_server, network_tick_rate);

    ks_server.on_connection_request_callback    = _onConnectionRequestCallback;
    ks_server.on_accept_client_callback         = _onAcceptClientCallback;
    ks_server.on_client_disconnected_callback   = _onClientDisconnectedCallback;
    ks_server.on_receive_message_callback       = _onReceiveMessageCallback;
    ks_server.on_update_callback                = _onUpdateCallback;

    if (AssetPool_init(&ks_ingame_assets,
        32, 32, 512, 512, 64, 64, 24, 512,
        INGAME_ASSETPOOL_MEMORY_SIZE) != 0)
    {
        DEBUG_PRINTF("Failed to init the asset pool.\n");
        return 4;
    }

    ks_loadIngameAssets();
    shared_initSharedGameData();
    GameData_init();

    if (Game_init(&ks_game) != 0) return 5;

    return 0;
}

void
ks_quit()
{
    if (ks_game.running)
        Game_stop(&ks_game);
    kys_beginShutdown();
}

int
ks_beginNetworking()
{
    if (Server_run(&ks_server) != 0)
        return 3;

    return 0;
}

int
ks_quitNetworking()
{
    return 0;
}

void
ks_loadIngameAssets()
{
    AssetPool_clear(&ks_ingame_assets); /* NOTE: The clear function is not fully
        implemented yet! */
    if (!LOAD_INGAME_MODEL("map", LEVEL_1_COLLISION_MAP_FILE_PATH))
        DEBUG_PRINTF("Failed to load map model!\n");

    if (!LOAD_INGAME_TEXTURE("rocketlauncher", "assets/textures/rocket_launcher_texture.png"))
        DEBUG_PRINTF("Failed to load rocketlauncher texture!\n");
    if (!LOAD_INGAME_MATERIAL("rocket launcher",
        GET_INGAME_TEXTURE("rocket launcher"),
        GET_INGAME_TEXTURE("rocket launcher"), 0.0f, 0.0f, 0.0f, 0.0f))
        DEBUG_PRINTF("Failed to load rocketlauncher material!\n");
    if (!LOAD_INGAME_MODEL("rocket launcher", "assets/rocket_launcher.obj"))
        DEBUG_PRINTF("Failed to load rocketlauncher model!\n");
}

void
InputExecutionBuffer_init(InputExecutionBuffer *ieb)
{
    ieb->first  = 0;
    ieb->num    = 0;
    Mutex_init(&ieb->mutex);
}

static void
_executePlayersInputs()
{
    double physics_dt = ks_game.scene.physics_step;
    Transform   *t;
    Player      *p;
    float       tmp_rot0, tmp_rot2;

    for (Player **pp = ks_game.active_players;
        pp < &ks_game.active_players[ks_game.num_players];
        ++pp)
    {
        p = *pp;

        if (p->input_buffer.num > 0)
        {
            Mutex_lock(&p->input_buffer.mutex);

            int         index;
            ClientInput *inp;

            index   = p->input_buffer.first % (int)MAX_INPUT_BUFFER_ITEMS;
            inp     = &p->input_buffer.items[index];

            if (!IS_SEQUENCE_GREATER(inp->sequence,
                p->input_buffer.latest_applied_input))
            {
                DEBUG_PRINTF("Received an out of order input from a player "
                    "- ignoring.!\n");
            }
            else
            {
                p->kb_state     = inp->data.movement.kb_state;
                p->orientation  = inp->data.movement.orientation;
            }

            /* Remember the latest applied sequence - we will send it back
             * to the player */
            p->input_buffer.latest_applied_input = inp->sequence;

            p->input_buffer.first = (p->input_buffer.first + 1) % \
                MAX_INPUT_BUFFER_ITEMS;
            --p->input_buffer.num;

            if (p->input_buffer.num > input_diff_tolerance)
            {
                DEBUG_PRINTF("Player's inputs too far behind, reverting from "
                    "%d to %d\n", p->input_buffer.num, 4);

                int diff = p->input_buffer.num - input_diff_rollback;
                p->input_buffer.first = (p->input_buffer.first + diff) % \
                    MAX_INPUT_BUFFER_ITEMS;
                p->input_buffer.num = input_diff_rollback;
            }

            Mutex_unlock(&p->input_buffer.mutex);

            t = &Entity_getTransform(p->pawn)->transform;

            /* Store the previous axes of 0 and 2, set them to 0 for more
             * predictable results, then restore them later. */
            tmp_rot0    = t->rot[0];
            tmp_rot2    = t->rot[2];
            t->rot[0]   = 0.f;
            t->rot[1]   = p->orientation;
            t->rot[2]   = 0.f;

            p->latest_movement_state = movePawnByKeyboardInput(p->pawn,
                p->kb_state);

            t->rot[0] = tmp_rot0;
            t->rot[2] = tmp_rot2;
        }
    }
}

static thread_ret_t
_Game_simulationMain(void *args)
{
    Game *g = (Game*)args;

    while (g->running)
    {
        Clock_tick(&g->simulation_clock);
        double dt = g->simulation_clock.delta;

        Mutex_lock(&ks_game.player_access_mutex);
        Mutex_lock(&ks_game.scene.entity_access_mutex);

        _updateScene(g->simulation_clock.delta);

        SimQueue_executeContents(&g->sim_queue, g);

        Mutex_unlock(&ks_game.scene.entity_access_mutex);
        Mutex_unlock(&ks_game.player_access_mutex);
    }

    return 0;
}

bool32
_onConnectionRequestCallback(Server *s, Address *a)
{
    if (!ks_game.running || ks_game.num_players >= MAX_PLAYERS_PER_GAME)
        return 0;
    return 1;
}

void
_onAcceptClientCallback(Server *s, ServerClient *sc)
{
    int res = Game_attachPlayer(&ks_game, sc);
    if (res != 0)
        {DEBUG_PRINTF("Failed to attach player to game (code %i)!\n", res);}
}

void
_onClientDisconnectedCallback(Server *s, ServerClient *sc)
{
    if (!sc->user_data) return;

    Mutex_lock(&ks_game.player_access_mutex);
    if (Game_removePlayer(&ks_game,
        (Player*)ServerClient_getUserData(sc)) != 0)
    {
        DEBUG_PRINTF("Failed to remove a player from a game!\n");
    }
    Mutex_unlock(&ks_game.player_access_mutex);
}

static int
_onReceiveMessageCallback(Server *s, ServerClient *sc, message_t msg_type,
    uint16_t msg_id, void *msg, int size, bool32 received_before)
{
#   define CHECK_SIZE(_type) \
        if (size < (int)_type##_SIZE) return -1;

    /* We don't need to lock the player access mutex here because this runs
     * on the same thread as player removal. */

    Player *player = (Player*)ServerClient_getUserData(sc);
    char *data = (char*)msg;

    switch (msg_type)
    {
        case KS_CS_MSG_INPUT_BUFFER_CONTENTS:
        {
            if (size < KS_CS_MSG_INPUT_BUFFER_CONTENTS_MIN_SIZE)
                return -1;

            ubyte               type;
            input_sequence_t    sequence;

            ubyte num_inputs = *(ubyte*)msg;
            int offset = 1;

            Mutex_lock(&player->input_buffer.mutex);

            for (ubyte i = 0; i < num_inputs && offset != -1; ++i)
            {
                if (size - offset < 1 + (int)sizeof(input_sequence_t))
                {
                    offset = -1;
                    break;
                }

                type = *(ubyte*)(data + offset);
                ++offset;

                sequence = *(input_sequence_t*)(data + offset);
                offset += (int)sizeof(input_sequence_t);

                switch (type)
                {
                    case INPUT_MOVEMENT:
                    {
                        if (size - offset < (int)(sizeof(kb_state_t) + sizeof(float)))
                        {
                            offset = -1;
                            break;
                        }

                        kb_state_t kb_state = *(kb_state_t*)(data + offset);
                        offset += (int)sizeof(kb_state_t);

                        float orientation = *(float*)(data + offset);
                        offset += (int)sizeof(float);

                        if (!received_before)
                        {
                            InputExecutionBuffer_queueMovementInputUnsafe(
                                &player->input_buffer, sequence, kb_state,
                                orientation);
                        }
                    }
                        break;
                    default:
                        offset = -1;
                        break;
                }
            }

            Mutex_unlock(&player->input_buffer.mutex);

            return offset;
        }
            break;
        case KS_CS_MSG_WEAPON_SWAPPED:
        {
            CHECK_SIZE(KS_CS_MSG_WEAPON_SWAPPED);

            if (!received_before && player->latest_weapon != (int)*data)
            {
                player->latest_weapon = (int)*data;

                Player *op;
                void *swap_msg;

                for (uint i = 0; i < ks_game.num_players; ++i)
                {
                    op = ks_game.active_players[i];
                    if (op == player) continue;

                    swap_msg = Server_queueMessage(&ks_server, op->client,
                        KS_SC_MSG_OTHER_PAWN_SWAPPED_WEAPON,
                        KS_SC_MSG_OTHER_PAWN_SWAPPED_WEAPON_SIZE);

                    if (swap_msg)
                    {
                        *(replication_id32_t*)swap_msg =
                            player->pawn->replication_id;
                        *((char*)swap_msg + sizeof(replication_id32_t)) = *data;
                    }
                    else
                    {
                        DEBUG_PRINTF("Failed to allocate a message to inform "
                            "another player of a weapon swap.\n");
                    }
                }

                DEBUG_PRINTF("A player swapped weapon to: %i\n", (int)*data);
            }

            return KS_CS_MSG_WEAPON_SWAPPED_SIZE;
        };
            break;
        case KS_CS_MSG_SHOT_ROCKET_LAUNCHER:
        {
            if (!received_before)
            {
                float *position = (float*)data;
                float *direction = (float*)(data + sizeof(vec3));

                if (Game_checkPlayerShotPositionValidity(&ks_game, player,
                    position) && player->shoot_cooldown_timer <= 0.0f)
                {
                    Game_informPlayersOfRocketShot(&ks_game, player, position,
                        direction);

                    SimQueue_queueShootRocketTask(&ks_game.sim_queue,
                        player->pawn->replication_id,
                        position, direction);

                    player->shoot_cooldown_timer = COOLDOWN_ROCKET_LAUNCHER * 0.95f;
                }
                else
                {
                    DEBUG_PRINTF("A player attempted to shoot, but shot was "
                        "invalid!\n");
                }
            }

            return KS_CS_MSG_SHOT_ROCKET_LAUNCHER_SIZE;
        }
            break;
        case KS_CS_MSG_SHOT_SMG:
        {
            if (!received_before)
            {
                float *position = (float*)data;
                float *direction = (float*)(data + sizeof(vec3));

                if (Game_checkPlayerShotPositionValidity(&ks_game, player,
                    position) && player->shoot_cooldown_timer <= 0.0f)
                {
                    Game_informPlayersOfSMGShot(&ks_game, player, position,
                        direction);

                    SimQueue_queueShootSMGTask(&ks_game.sim_queue,
                        player->pawn->replication_id,
                        position, direction);

                    player->shoot_cooldown_timer = COOLDOWN_SMG * 0.95f;
                }
            }

            return KS_CS_MSG_SHOT_SMG_SIZE;
        }
            break;
        case KS_CS_MSG_SHOT_RAILGUN:
        {
            if (!received_before)
            {
                float *position = (float*)data;
                float *direction = (float*)(data + sizeof(vec3));

                if (Game_checkPlayerShotPositionValidity(&ks_game, player,
                    position) && player->shoot_cooldown_timer <= 0.0f)
                {
                    Game_informPlayersOfSMGShot(&ks_game, player, position,
                        direction);

                    SimQueue_queueShootRailgunTask(&ks_game.sim_queue,
                        player->pawn->replication_id,
                        position, direction);

                    player->shoot_cooldown_timer = COOLDOWN_RAILGUN * 0.95f;
                }
            }

            return KS_CS_MSG_SHOT_RAILGUN_SIZE;
        }
            break;

        case KS_CS_MSG_HIT_MACHETE:
        {
            if (!received_before)
            {
                DEBUG_PRINTF("Player hit with machete (not implemented)!\n");
            }

            return KS_CS_MSG_HIT_MACHETE_SIZE;
        }
            break;
    }

    return -1;

#   undef CHECK_SIZE
}

static void
_onUpdateCallback(Server *s)
{
    /*Mutex_lock(&ks_game.scene.entity_access_mutex);*/

    void                *msg;
    ByteBuffer          buf;

    buf.max_size = KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE_SIZE;

    Player *p;
    for (uint i = 0; i < ks_game.num_players; ++i)
    {
        p = ks_game.active_players[i];

        /* Decrease weapon cooldowns */
        if (p->shoot_cooldown_timer > 0.0f)
            p->shoot_cooldown_timer -= s->clock.delta;

        msg = Server_queueMessage(&ks_server, p->client,
            KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE,
            KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE_SIZE);

        if (!msg)
        {
            DEBUG_PRINTF("Failed to queue an own pawn position and velocity "
                "update.\n");
            continue;
        }

        ByteBuffer_init(&buf, msg,
            KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE_SIZE);

        vec3 ver_pos, ver_vel;

        Mutex_lock(&p->latest_input_mutex);

        if (p->have_new_verified_state)
        {
            KS_SC_MSG_OWN_PAWN_POSITION_AND_VELOCITY_UPDATE_serialize(&buf,
                p->latest_verified_position,
                p->latest_verified_velocity,
                p->latest_verified_input);
            _pushPlayerSnapshot(&ks_game, p, p->latest_verified_position);
            p->have_new_verified_state = 0;
        }

        vec3_copy(ver_pos, p->latest_verified_position);
        vec3_copy(ver_vel, p->latest_verified_velocity);

        Mutex_unlock(&p->latest_input_mutex);

        /* Inform other clients of the state of this entity */
        Player *op;
        for (uint i = 0; i < ks_game.num_players; ++i)
        {
            op = ks_game.active_players[i];

            if (op == p) continue;

            msg = Server_queueMessage(&ks_server, op->client,
                KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE,
                KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE_SIZE);

            if (msg)
            {
                ByteBuffer_init(&buf, msg,
                    KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE_SIZE);

                const float full_rad = (float)(M_PI * 2.0f);
                float orientf = p->latest_orientation / full_rad;
                ubyte orient = (ubyte)(RADIAN_CLAMPF(orientf * 255.0f));

                /* Note: lock mutex here? */
                KS_SC_MSG_OTHERS_PAWN_STATE_UPDATE_serialize(&buf,
                    p->pawn->replication_id,
                    ver_pos, ver_vel, orient,
                    p->latest_weapon,
                    p->latest_movement_state);
            }
        }
    }

    /*Mutex_unlock(&ks_game.scene.entity_access_mutex);*/

    /* Send any messages sent from the simulation thread */
    OutputBuffer_sendAllContents(&ks_game.output_buffer);
}

static inline void
_pushPlayerSnapshot(Game *g, Player *p, vec3 pos)
{
    PlayerSnapshotBuffer *b = GET_PLAYER_SNAPSHOTS(g, p);
    uint index = b->num % 32;
    b->items[index].timestamp = getTimePassed();
    vec3_copy(b->items[index].pos, pos);
    ++b->num;
}

int
OutputBuffer_init(OutputBuffer *ob)
{
    void *mem = allocatePermanentMemory(MAX_PLAYERS_PER_GAME * KYS_MTU);
    if (!mem) return 1;

    SimpleMemoryBlock_init(&ob->memory, mem, MAX_PLAYERS_PER_GAME * KYS_MTU);
    Mutex_init(&ob->mutex);
    ob->num_items = 0;

    return 0;
}

void *
OutputBuffer_queueMessageUnsafe(OutputBuffer *ob, ServerClient *client,
    message_t type, int size)
{
    DEBUG_PRINTF("Queued: size is %i.\n", size);
    if (ob->num_items >= MAX_OUTPUT_BUFFER_ITEMS)
        {DEBUG_PRINTF("OutputBuffer: fulL!"); return 0;}

    void *msg = SimpleMemoryBlock_malloc(&ob->memory, (size_t)size);
    if (!msg)
    {
        DEBUG_PRINTF("%lu, %lu, %lu\n", (size_t)ob->memory.offset, (size_t)ob->memory.start,
            ob->memory.total_size);
        DEBUG_PRINTF("OutputBuffer: out of memory!\n");
        return 0;
    }

    struct OutputItem *item = &ob->items[ob->num_items];

    item->client    = client;
    item->type      = type;
    item->data      = msg;
    item->size      = size;

    ++ob->num_items;

    return msg;
}

void
OutputBuffer_sendAllContents(OutputBuffer *ob)
{
    Mutex_lock(&ob->mutex);

    struct OutputItem *item;
    void *msg;

    for (uint i = 0; i < ob->num_items; ++i)
    {
        item = &ob->items[i];

        msg = Server_queueMessage(&ks_server, item->client,
            item->type, item->size);

        if (!msg) {DEBUG_PRINTF("Failed to queue a message from the output "
             "buffer!\n"); break;};

        memcpy(msg, item->data, (size_t)item->size);
    }

    ob->num_items = 0;
    SimpleMemoryBlock_clear(&ob->memory);

    Mutex_unlock(&ob->mutex);
}

void
SimQueue_init(SimQueue *q)
{
    q->num_tasks = 0;
    Mutex_init(&q->mutex);
}

void
SimQueue_executeContents(SimQueue *q, Game *game)
{
    if (q->num_tasks <= 0) return;

    Mutex_lock(&q->mutex);

    for (uint i = 0; i < q->num_tasks; ++i)
    {
        switch (q->tasks[i].type)
        {
            case SIM_REMOVE_ENTITY:
            {
                if (q->tasks[i].data.remove_entity.entity->id ==
                    q->tasks[i].data.remove_entity.id)
                {
                    Scene3D_removeEntity(&game->scene,
                        q->tasks[i].data.remove_entity.entity);
                }
            }
                break;
            case SIM_SHOOT_ROCKET:
            {
                server_shootRocket(
                    q->tasks[i].data.shoot_something.shooter_rid,
                    q->tasks[i].data.shoot_something.pos,
                    q->tasks[i].data.shoot_something.dir,
                    game);
            }
                break;

            case SIM_SHOOT_SMG:
            {
                server_shootSMG(
                    q->tasks[i].data.shoot_something.shooter_rid,
                    q->tasks[i].data.shoot_something.pos,
                    q->tasks[i].data.shoot_something.dir,
                    game);
            }
                break;
            case SIM_SHOOT_RAILGUN:
            {
                server_shootRailgun(
                    q->tasks[i].data.shoot_something.shooter_rid,
                    q->tasks[i].data.shoot_something.pos,
                    q->tasks[i].data.shoot_something.dir,
                    game);
            }
                break;
        }
    }

    q->num_tasks = 0;

    Mutex_unlock(&q->mutex);
}

void
GameData_init()
{
    /* Level 1 respawn points */
    vec3 *spawn_pts = game_data.spawn_points_level1;

    vec3_set(spawn_pts[0],   0.00f,   10.00f,    0.00f);
    vec3_set(spawn_pts[1],  55.20f,   16.70f,    6.94f);
    vec3_set(spawn_pts[2],  10.89f,   27.98f,   26.87f);
    vec3_set(spawn_pts[3],  46.59f,   16.73f, -105.20f);
    vec3_set(spawn_pts[4], -58.05f,   22.78f,  -46.63f);

    /* Level 1 item spawns */
    KsItemSpawnDefinition *defs = game_data.items_level1;

    defs[0].type        = ITEM_MEDKIT;
    defs[0].cooldown    = 5000;
    vec3_set(defs[0].pos, -0.213, 21.774, -11.871);

    defs[1].type        = ITEM_ROCKET_AMMO;
    defs[1].cooldown    = 5000;
    vec3_set(defs[1].pos, -31.074, 22.403, -21.289);

    defs[2].type        = ITEM_SMG_AMMO;
    defs[2].cooldown    = 5000;
    vec3_set(defs[2].pos, -1.270, 13.149, -138.215);

    defs[3].type        = ITEM_RAILGUN_AMMO;
    defs[3].cooldown    = 5000;
    vec3_set(defs[3].pos, -11.817, 10.116, 21.890);
}

int
Game_init(Game *game)
{
    game->scene_memory = allocatePermanentMemory(GAME_SCENE_MEMORY_SIZE);
    if (!game->scene_memory) return 1;

    game->scene_memory_size = GAME_SCENE_MEMORY_SIZE;

    if (initSceneForLevel1(&game->scene, game->scene_memory,
        GAME_SCENE_MEMORY_SIZE) != 0)
        return 1;

    for (int32_t i = 0; i < (int32_t)MAX_PLAYERS_PER_GAME; ++i)
    {
        Mutex_init(&game->players[i].latest_input_mutex);
        InputExecutionBuffer_init(&game->players[i].input_buffer);
    }

    Mutex_init(&game->player_access_mutex);
    Clock_init(&game->simulation_clock, SERVER_TICK_RATE);
    SimQueue_init(&game->sim_queue);
    if (OutputBuffer_init(&game->output_buffer) != 0) return 2;

    memset(game->spawn_point_timers, 0, sizeof(game->spawn_point_timers)),

    game->running_player_id = 0;

    return 0;
}

int
Game_start(Game *game)
{
    if (game->running) return 1;

    for (int i = 0; i < MAX_PLAYERS_PER_GAME; ++i)
    {
        /* Don't call memset here to not fuck the mutexes up btw famalam */
        game->players[i].reserved   = 0;
        game->active_players[i]     = 0;
    }

    game->running = 1;
    Thread_create(&game->simulation_thread, _Game_simulationMain, game);

    return 0;
}

void
Game_stop(Game *game)
{
    game->running = 0;
    Thread_join(&game->simulation_thread);
}

int
Game_attachPlayer(Game *game, ServerClient *client)
{
    if (!game->running) return 1;
    if (game->num_players >= MAX_PLAYERS_PER_GAME) return 2;

    Mutex_lock(&game->player_access_mutex);

    /* Find a free player slot */
    Player *p = 0;

    for (uint i = 0; i < MAX_PLAYERS_PER_GAME; ++i)
    {
        if (!game->players[i].reserved)
        {
            p = &game->players[i];
            break;
        }
    }

    if (!p)
    {
        Mutex_unlock(&game->player_access_mutex);
        return 3;
    }

    p->reserved                 = 1;
    p->id                       = game->running_player_id++;
    p->pawn                     = 0;
    p->client                   = client;
    p->state                    = PLAYER_STATE_LOADING;
    p->latest_weapon            = DEFAULT_WEAPON;
    p->latest_movement_state    = DEFAULT_PAWN_MOVEMENT_STATE;
    p->health                   = MAX_HEALTH;
    p->num_frags                = 0;
    p->shoot_cooldown_timer     = 0.0f;
    p->have_new_verified_state  = 1;
    p->character_type           = rand() % NUM_CHARACTER_TYPES;

    DEBUG_PRINTF("Server: character type is %i\n", (int)p->character_type);

    InputExecutionBuffer_clear(&p->input_buffer);

    ServerClient_setUserData(client, p);
    game->active_players[game->num_players] = p;
    ++game->num_players;

    Mutex_lock(&game->scene.entity_access_mutex);

    /* Create a pawn for the player */
    vec3 spawn_position;
    Game_getRandomSpawnPoint(game, spawn_position);

    /* Snapshots */
    PlayerSnapshotBuffer *snapshots = GET_PLAYER_SNAPSHOTS(game, p);
    snapshots->num = 0;

    uint32_t time_passed = getTimePassed();

    for (int i = 0; i < 32; ++i)
    {
        vec3_copy(snapshots->items[i].pos, spawn_position);
        snapshots->items[i].timestamp = time_passed;
    }

    p->pawn = Scene3D_createEntity(&game->scene, spawn_position[0],
        spawn_position[1], spawn_position[2]);
    ASSERT(p->pawn);
    server_initPlayerPawn(p->pawn);

    void *msg = Server_queueMessage(&ks_server, client,
        KS_SC_MSG_ATTACHED_TO_GAME, KS_SC_MSG_ATTACHED_TO_GAME_SIZE);

    if (msg)
        *(char*)msg = KS_MAP_1;

    /* Inform the player of their own pawn */
    msg = Server_queueMessage(&ks_server, client,
        KS_SC_MSG_OWN_PAWN_INFORM, KS_SC_MSG_OWN_PAWN_INFORM_SIZE);

    if (msg)
    {
        ByteBuffer b;
        ByteBuffer_init(&b, msg, KS_SC_MSG_OWN_PAWN_INFORM_SIZE);
        KS_SC_MSG_OWN_PAWN_INFORM_serialize(&b,
            p->pawn->replication_id, p->character_type, spawn_position);
    }
    else
    {
        DEBUG_PRINTF("Failed to allocate pawn inform message!\n");
    }

    /* Inform the player of items in the level */
    ByteBuffer      buf;
    ScriptComponent *sc;
    ItemSpawnScriptData *item_script_data;
    buf.max_size = KS_SC_MSG_ITEM_SPAWNED_SIZE;

    for (uint i = 0; i < NUM_ITEMS_LEVEL1; ++i)
    {
        sc = Entity_getScriptComponent(game->item_spawns[i]);
        item_script_data = (ItemSpawnScriptData*)sc->data;

        if (item_script_data->next_spawn_time != ITEM_IS_SPAWNED)
            continue;


        msg = Server_queueMessage(&ks_server, client, KS_SC_MSG_ITEM_SPAWNED,
            KS_SC_MSG_ITEM_SPAWNED_SIZE);

        if (!msg)
        {
            DEBUG_PRINTF("Failed to inform a new player of a power up!\n");
            continue;
        }

        buf.offset = 0;
        buf.memory = msg;
        KS_SC_MSG_ITEM_SPAWNED_serialize(&buf, item_script_data->index,
            game_data.items_level1[i].type, game_data.items_level1[i].pos);
    }

    Mutex_unlock(&game->scene.entity_access_mutex);

    /* Inform other players of the new player's pawn*/
    Player      *op;
    buf.max_size = KS_SC_MSG_OTHERS_PAWN_INFORM_SIZE;

    for (uint i = 0; i < ks_game.num_players; ++i)
    {
        op = ks_game.active_players[i];

        if (op == p) continue;

        msg = Server_queueMessage(&ks_server, op->client,
            KS_SC_MSG_OTHERS_PAWN_INFORM, KS_SC_MSG_OTHERS_PAWN_INFORM_SIZE);

        if (!msg)
        {
            DEBUG_PRINTF("Failed to allocate message to inform another player "
                "of new players pawn.\n");
            continue;
        }

        buf.offset = 0;
        buf.memory = msg;
        static vec3 velocity = {0.0f, 0.0f, 0.0f};

        KS_SC_MSG_OTHERS_PAWN_INFORM_serialize(&buf, p->pawn->replication_id,
            p->character_type, /* Always set type to 0 for now */
            spawn_position, velocity,
            0); /* Always set orientation to 0 for now */

        DEBUG_PRINTF("Informed old player of new player's pawn.\n");

        msg = Server_queueMessage(&ks_server, p->client,
            KS_SC_MSG_OTHERS_PAWN_INFORM, KS_SC_MSG_OTHERS_PAWN_INFORM_SIZE);

        if (!msg)
        {
            DEBUG_PRINTF("Failed to allocate message to inform new player "
                "of another players pawn.\n");
            continue;
        }

        buf.offset = 0;
        buf.memory = msg;

        KS_SC_MSG_OTHERS_PAWN_INFORM_serialize(&buf, op->pawn->replication_id,
            op->character_type, /* Always set type to 0 for now */
            p->latest_verified_position, p->latest_verified_velocity,
            0); /* Always set orientation to 0 for now */

        DEBUG_PRINTF("Informed new player of old player's pawn.\n");
    }

    Mutex_unlock(&game->player_access_mutex);

    return 0;
}

int
Game_removePlayer(Game *game, Player *player)
{
    ASSERT(game->num_players > 0);

    /* The caller locks the mutex */

    for (uint i = 0; i < game->num_players; ++i)
    {
        if (game->active_players[i] == player)
        {
            for (uint j = i; j < game->num_players - 1; ++j)
                game->active_players[j] = game->active_players[j + 1];

            replication_id32_t rid = ENTITY_NOT_REPLICATED;

            if (player->pawn)
            {
                rid = player->pawn->replication_id;
                SimQueue_queueRemoveEntityTask(&game->sim_queue, player->pawn);
                player->pawn        = 0;
                player->reserved    = 0;
            }

            /* TODO: INFORM OTHER PLAYERS OF THE LOST ENTITY HERE! */
            game->num_players--;

            if (rid != ENTITY_NOT_REPLICATED)
            {
                Player *op;
                uint num_players = ks_game.num_players;
                for (uint j = 0; j < num_players; ++j)
                {
                    op = ks_game.active_players[j];
                    void *msg = Server_queueMessage(&ks_server, op->client,
                        KS_SC_MSG_OTHERS_PAWN_REMOVED,
                        KS_SC_MSG_OTHERS_PAWN_REMOVED_SIZE);

                    if (!msg)
                    {
                        DEBUG_PRINTF("Failed to allocate a message to inform a "
                            "player of another player's removed pawn!\n");
                        continue;
                    }

                    *(replication_id32_t*)msg = rid;
                }
            }

            return 0;
        }
    }

    return 1;
}

void
Game_getRandomSpawnPoint(Game *game, vec3 ret)
{
    /* Only use level 1 for now */
    int index = rand() % NUM_SPAWN_POINTS_LEVEL1;

    uint32_t time_passed = getTimePassed();
    if (time_passed - game->spawn_point_timers[index] >= SPAWN_POINT_RE_USE_TIME)
    {
        int new_index = -1;

        for (int i = 0; i < NUM_SPAWN_POINTS_LEVEL1; ++i)
        {
            if (time_passed - game->spawn_point_timers[i] >= SPAWN_POINT_RE_USE_TIME)
            {
                new_index = i;
                break;
            }
        }

        if (new_index != -1) index = new_index;
    }

    vec3_copy(ret, game_data.spawn_points_level1[index]);
    game->spawn_point_timers[index] = time_passed;
}

/* Note: when run on the main thread, must have claimed a lock of the
 * player access mutex of the game */
void
Game_killPlayersPawn(Game *game, Player *p, replication_id32_t killer)
{
    p->health = MAX_HEALTH;

    Mutex_lock(&p->latest_input_mutex);
    Transform *t = &Entity_getTransform(p->pawn)->transform;
    Game_getRandomSpawnPoint(game, t->pos);
    vec3_copy(p->latest_verified_position, t->pos);
    vec3_set(p->latest_verified_velocity, 0.0f, 0.0f, 0.0f);
    PhysicsComponent *pc = Entity_getPhysicsComponent(p->pawn);
    vec3_set(pc->velocity, 0.0f, 0.0f, 0.0f);
    p->have_new_verified_state = 1;
    Mutex_unlock(&p->latest_input_mutex);

    Mutex_lock(&game->output_buffer.mutex);

    void *msg = OutputBuffer_queueMessageUnsafe(&game->output_buffer, p->client,
        KS_SC_MSG_SNAP_OWN_PAWN_STATE, KS_SC_MSG_SNAP_OWN_PAWN_STATE_SIZE);

    if (msg)
    {
        ByteBuffer b;
        ByteBuffer_init(&b, msg, KS_SC_MSG_SNAP_OWN_PAWN_STATE_SIZE);

        KS_SC_MSG_SNAP_OWN_PAWN_STATE_serialize(&b,
            p->latest_verified_position,
            p->latest_verified_velocity);
    }

    msg = OutputBuffer_queueMessageUnsafe(&game->output_buffer, p->client,
        KS_SC_MSG_OWN_HEALTH_INFORM, KS_SC_MSG_OWN_HEALTH_INFORM_SIZE);

    if (msg)
        *(char*)msg = (char)p->health;
    else
        DEBUG_PRINTF("Failed to send health inform message to player!\n");

    /* Inform ALL players of the death, including this one */
    Player *op;
    for (uint i = 0; i < game->num_players; ++i)
    {
        op = game->active_players[i];

        msg = OutputBuffer_queueMessageUnsafe(&game->output_buffer, op->client,
            KS_SC_MSG_FRAG_INFORM, KS_SC_MSG_FRAG_INFORM_SIZE);

        if (msg)
        {
            *(replication_id32_t*)msg = killer;
            *((replication_id32_t*)msg + 1) = p->pawn->replication_id;
        }
        else
            DEBUG_PRINTF("Failed to inform a player of a pawn death.\n");
    }

    Mutex_unlock(&game->output_buffer.mutex);
}

bool32
Game_checkPlayerShotPositionValidity(Game *g, Player *p, vec3 pos)
{
    return 1; /* :DDDDDDD */

    uint32_t time_passed    = getTimePassed();
    uint32_t latency        = p->client->connection.average_latency;
    uint32_t target_time    = time_passed - latency;

    PlayerSnapshotBuffer *buf = GET_PLAYER_SNAPSHOTS(g, p);

    /* Leaving this stupid like this for the moment to get the demo out */

    vec3 diff; vec3_sub(diff, p->latest_verified_position, pos);

    const float shoot_treshold = 1.5f;

    if (vec3_mul_inner(diff, diff) < shoot_treshold)
    {
        return 1;
    }

    DEBUG_PRINTF("Player's shot position was invalid - len: %f!\n",
        vec3_len(diff));
    return 0;
}

int
initSceneForLevel1(Scene3D *scene, void *memory, size_t memory_size)
{
    Scene3D_clear(scene);
    Scene3D_init(scene, memory,
        memory_size, MAX_ENTITIES_LEVEL_1, 1);
    DEBUG_PRINTF("%f\n", scene->physics_timer);

    if (shared_buildCollisionWorldForLevel1(scene,
        GET_INGAME_MODEL("map")) != 0)
    {
        return 1;
    }

    shared_setDefaultScenePhysicsLimits(scene);

    KsItemSpawnDefinition *def;
    for (int i = 0; i < (int)NUM_ITEMS_LEVEL1; ++i)
    {
        def = &game_data.items_level1[i];

        switch (def->type)
        {
            case ITEM_MEDKIT:
            {
                ks_game.item_spawns[i] = server_createMedkitEntity(scene,
                    i, def->cooldown, def->pos[0], def->pos[1], def->pos[2]);
            }
                break;
            case ITEM_ROCKET_AMMO:
            {
                ks_game.item_spawns[i] = server_createRocketAmmoEntity(scene,
                    i, def->cooldown, def->pos[0], def->pos[1], def->pos[2]);
            }
                break;
            case ITEM_SMG_AMMO:
            {
                ks_game.item_spawns[i] = server_createSMGAmmoEntity(scene,
                    i, def->cooldown, def->pos[0], def->pos[1], def->pos[2]);
            }
                break;
            case ITEM_RAILGUN_AMMO:
            {
                ks_game.item_spawns[i] = server_createRailgunAmmoEntity(scene,
                    i, def->cooldown, def->pos[0], def->pos[1], def->pos[2]);
            }
                break;
            default:
            {
                ASSERT(0);
            }
                break;
        }
    }

    return 0;
}

static void
_MainMenu_redrawTextModeBaseWindow()
{
    clear();
    int len = strlen(main_menu.title_str);
    mvprintw(0, main_menu.text_mode.term_width / 2 - len / 2, main_menu.title_str,
        KS_VERSION_STR);
    mvprintw(main_menu.text_mode.term_height - 1, 0, "h: toggle help - q: quit");
    move(main_menu.text_mode.term_height - 1, main_menu.text_mode.term_width - 1);
}

static void
_MainMenu_Text_redrawHelpWindow()
{
    wclear(main_menu.text_mode.help_win);
    wmove(main_menu.text_mode.help_win, 1,
        getmaxx(main_menu.text_mode.help_win) / 2 - 2);
    wprintw(main_menu.text_mode.help_win, "Help");

    wmove(main_menu.text_mode.help_win, 2, 1);
    wprintw(main_menu.text_mode.help_win,
        "r: enable networking\n"
        " s: disable networking\n");

    wmove(main_menu.text_mode.help_win, 3, 1);
    wprintw(main_menu.text_mode.help_win,
        "a,d,A,D: set input diff\n tolerance");

    wborder(main_menu.text_mode.help_win,
        '|', '|', '-', '-', '*', '*', '*', '*');
}

static void
_MainMenu_Text_redrawNetworkWindow()
{
    wclear(main_menu.text_mode.network_win);
    wmove(main_menu.text_mode.network_win, 1,
        getmaxx(main_menu.text_mode.network_win) / 2 - 7);
    wprintw(main_menu.text_mode.network_win, "Network status\n");
}

static void
_MainMenu_Text_updateNetworkWindow()
{
    int y = 2;

    wmove(main_menu.text_mode.network_win, y++, 1);
    wprintw(main_menu.text_mode.network_win,
        "Enabled: %s\n", BOOL_STR(ks_server.running));

    wmove(main_menu.text_mode.network_win, y++, 1);
    wprintw(main_menu.text_mode.network_win,
        "Game running: %s\n", BOOL_STR(ks_game.running));

    wmove(main_menu.text_mode.network_win, y++, 1);
    wprintw(main_menu.text_mode.network_win,
        "Bytes received: %lu\n", ks_server.num_bytes_received);

    wmove(main_menu.text_mode.network_win, y++, 1);
    wprintw(main_menu.text_mode.network_win,
        "Bytes sent: %lu\n", ks_server.num_bytes_sent);

    wmove(main_menu.text_mode.network_win, y++, 1);
    wprintw(main_menu.text_mode.network_win,
        "Clients: %lu\n", ks_server.num_active_clients);

    wmove(main_menu.text_mode.network_win, y++, 1);
    wprintw(main_menu.text_mode.network_win,
        "inp. diff. tol.: %d\n", input_diff_tolerance);

    wmove(main_menu.text_mode.network_win, y++, 1);
    wprintw(main_menu.text_mode.network_win,
        "inp. diff. rollback.: %d\n", input_diff_rollback);

    wborder(main_menu.text_mode.network_win,
        '|', '|', '-', '-', '*', '*', '*', '*');
    wrefresh(main_menu.text_mode.network_win);
}

int
initMainMenu()
{
    ks_main_menu_screen = createScreen(updateAndRenderMainMenu, 0);
    main_menu.title_str = "Karistys server - version %s\n";

    if (!ks_main_menu_screen)
        return 1;

    ks_main_menu_screen->updateAndRenderTextMode =
        updateAndRenderMainMenuTextMode;

    if (KYS_IS_TEXT_MODE())
    {
        getmaxyx(stdscr, main_menu.text_mode.term_height, main_menu.text_mode.term_width);
        main_menu.text_mode.network_win = newwin(10, 26, 1, 1);
        main_menu.text_mode.help_win    = newwin(10, 26, 1,
            main_menu.text_mode.term_width - 26);
        _MainMenu_redrawTextModeBaseWindow();
        _MainMenu_Text_redrawHelpWindow();
        _MainMenu_Text_redrawNetworkWindow();
        wclear(main_menu.text_mode.network_win);
        _MainMenu_Text_redrawNetworkWindow();
        refresh();
        wrefresh(main_menu.text_mode.network_win);
        timeout(5);
    }
    else
    {
    }

    return 0;
}

void
updateAndRenderMainMenu(Screen *screen)
{
    (void)screen;

    setRenderBackgroundColor(0.2f, 0.2f, 0.35f, 1.0f);

    GUI_beginUpdate();

    GUI_setDefaultTextScale(2.0f);

    char text_buffer[256];
    sprintf(text_buffer, main_menu.title_str, KS_VERSION_STR);
    GUI_textLine(text_buffer, 15, 0);

    if (GUI_button("quit", 50, 50, 50, 35))
        ks_quit();

    if (GUI_button("begin networking", 50, 100, 75, 35))
    {
        ks_beginNetworking();
        Game_start(&ks_game);
    }

    if (isKeyPressed(SDL_SCANCODE_R))
    {
        ks_beginNetworking();
        Game_start(&ks_game);
    }

    if (isKeyPressed(SDL_SCANCODE_Q))
        ks_quit();

    GUI_endUpdate();
}

void
updateAndRenderMainMenuTextMode(Screen *screen)
{
    (void)screen;

    int term_x, term_y;
    getmaxyx(stdscr, term_y, term_x);

    if (term_x != main_menu.text_mode.term_width || term_y != main_menu.text_mode.term_height)
    {
        main_menu.text_mode.term_width    = term_x;
        main_menu.text_mode.term_height   = term_y;

        delwin(main_menu.text_mode.network_win);
        delwin(main_menu.text_mode.help_win);
        main_menu.text_mode.network_win = newwin(10, 25, 1, 1);
        main_menu.text_mode.help_win    = newwin(10, 25, 1,
            main_menu.text_mode.term_width - 25);

        _MainMenu_redrawTextModeBaseWindow();
        _MainMenu_Text_redrawNetworkWindow();

        refresh();
        wrefresh(main_menu.text_mode.network_win);

        if (main_menu.text_mode.show_help_win)
        {
            _MainMenu_Text_redrawHelpWindow();
            wrefresh(main_menu.text_mode.help_win);
        }
    }

    move(term_y - 1, term_x - 1);

    _MainMenu_Text_updateNetworkWindow();

    int ch = getch();

    switch (ch)
    {
        case 'q':
            ks_quit();
            break;
        case 'h':
        {
            if (main_menu.text_mode.show_help_win)
            {
                wclear(main_menu.text_mode.help_win);
                wrefresh(main_menu.text_mode.help_win);
                main_menu.text_mode.show_help_win = 0;
            }
            else
            {
                _MainMenu_Text_redrawHelpWindow();
                wrefresh(main_menu.text_mode.help_win);
                main_menu.text_mode.show_help_win = 1;
            }
        }
            break;
        case 'r':
            ks_beginNetworking();
            Game_start(&ks_game);
            break;
        case 's':
            ks_quitNetworking();
            break;
        case 'a':
        {
            if (input_diff_tolerance > input_diff_rollback
            && input_diff_tolerance > 0)
                --input_diff_tolerance;
        }
            break;
        case 'd':
            input_diff_tolerance++;
            break;
        case 'A':
            if (input_diff_rollback > 0)
                input_diff_rollback--;
            break;
        case 'D':
        {
            if (input_diff_rollback < input_diff_tolerance)
                input_diff_rollback++;
        }
            break;
        case 'e':
            main_menu.text_mode.show_help_win = 0;
            break;
    }

}

static void
playerScript(Entity *e, void *d)
{
    Transform *t = &Entity_getTransform(e)->transform;
    t->rot[0] = 0.f;
    t->rot[2] = 0.f;
}

void
server_initPlayerPawn(Entity *e)
{
    shared_initPlayerPawn(e);
    ScriptComponent *s = Entity_attachScriptComponent(e);
    ScriptComponent_allocateData(s, sizeof(PlayerScriptData));
    shared_initBasePlayerScriptData(&((PlayerScriptData*)s->data)->base);
    ScriptComponent_setCallback(s, playerScript);

    Entity_makeReplicated(e);

    replication_bitmask_t flags =
        REPLICATION_FLAG_DONT_SEND_ROTATION | \
        REPLICATION_FLAG_DONT_SEND_POSITION | \
        REPLICATION_FLAG_DONT_SEND_VELOCITY;

    Entity_setReplicationFlags(e, flags);
}

void
server_shootRocket(replication_id32_t shooter_rid, vec3 player_pos,
    vec3 forward, Game *game)
{
    BaseBulletType *bbt = GET_BASE_BULLET_TYPE(ROCKET_LAUNCHER);
    Entity *e = Scene3D_createEntity(&game->scene, player_pos[0], player_pos[1], player_pos[2]);
    Transform *t = &Entity_getTransform(e)->transform;

    Entity_setLifetime(e, bbt->lifetime);

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
    pc->gravity_active = 0;
    pc->friction = 0.0f;
    PhysicsComponent_setActivity(pc, 1);
    vec3_copy(pc->velocity, forward);
    vec3_norm(pc->velocity, pc->velocity);
    vec3_scale(pc->velocity, pc->velocity, bbt->speed);

    ScriptComponent *sc = Entity_attachScriptComponent(e);
    BulletScriptData *d = ScriptComponent_allocateData(sc, sizeof(BulletScriptData));
    d->timer = 0;
    d->bullet_velocity = bbt->speed;
    d->shooter_rid = shooter_rid;

    vec3_copy(((BulletScriptData*)sc->data)->last_position, player_pos);
    ScriptComponent_setCallback(sc, server_rocketSkyBulletScript);

    float dist_to_triangle = Scene3D_castRayToStaticGeometry(&game->scene,
        player_pos, forward, ROCKET_RAY_LENGTH,
        ((BulletScriptData*)sc->data)->explosion_pos,
        &((BulletScriptData*)sc->data)->dist_to_hit);

    if (dist_to_triangle != 0)
    {
        ScriptComponent_setCallback(sc, server_rocketStaticColliderBulletScript);
    }
}

void
server_shootSMG(replication_id32_t rid, vec3 player_pos, vec3 forward, Game *game)
{
    vec3 hit_pos, endpoint, temp;
    float dist_to_triangle, dist_to_hit;
    vec3_scale(temp, forward, SMG_RAY_LENGTH);
    vec3_add(endpoint, player_pos, temp);

    dist_to_triangle = Scene3D_castRayToStaticGeometry(&game->scene,
        player_pos, forward, SMG_RAY_LENGTH, hit_pos, &dist_to_hit);

    Player *enemy;
    PhysicsComponent *pc;

    for (uint i = 0; i < ks_game.num_players; ++i)
    {
        enemy = ks_game.active_players[i];

        if (enemy->pawn->replication_id == rid)
            continue;

        pc = Entity_getPhysicsComponent(enemy->pawn);
        for (SphereCollider *sc = pc->sphere_colliders; sc; sc = sc->next)
        {
            if (testSphere3DRayWithEndpoint(&sc->data, endpoint, player_pos, &dist_to_hit))
            {
                if (dist_to_triangle >= dist_to_hit || dist_to_triangle == 0)
                {
                    DEBUG_PRINTF("SMG hit a player (should we lock a mutex "
                        "here?)!\n");

                    Game_dealDamageToPlayer(&ks_game, enemy, rid, 20);
                }
            }
        }
    }
}

void
server_shootRailgun(replication_id32_t rid, vec3 player_pos, vec3 forward, Game *game)
{
    vec3 hit_pos, endpoint, temp;
    float dist_to_triangle, dist_to_hit;
    vec3_scale(temp, forward, RAILGUN_RAY_LENGTH);
    vec3_add(endpoint, player_pos, temp);

    dist_to_triangle = Scene3D_castRayToStaticGeometry(&game->scene, player_pos,
        forward, RAILGUN_RAY_LENGTH, hit_pos, &dist_to_hit);

    Player *enemy;
    PhysicsComponent *pc;

    for (uint i = 0; i < ks_game.num_players; ++i)
    {
        enemy = ks_game.active_players[i];

        if (enemy->pawn->replication_id == rid)
            continue;

        pc = Entity_getPhysicsComponent(enemy->pawn);
        for (SphereCollider *sc = pc->sphere_colliders; sc; sc = sc->next)
        {
            if (testSphere3DRayWithEndpoint(&sc->data, endpoint, player_pos,
                &dist_to_hit))
            {
                if (dist_to_triangle >= dist_to_hit || dist_to_triangle == 0)
                {
                    DEBUG_PRINTF("Railgun hit a player (should we lock a mutex "
                        "here?)!\n");
                    Game_dealDamageToPlayer(&ks_game, enemy, rid, 100);
                }
            }
        }

    }
}

void
server_rocketSkyBulletScript(Entity *entity, void *scriptdata)
{
    BulletScriptData *bsd = (BulletScriptData*)scriptdata;
    EntityTransform *et = Entity_getTransform(entity);
    bsd->timer += entity->scene->delta;

    if (bsd->timer >= bsd->dist_to_hit / bsd->bullet_velocity)
    {
        server_createRocketExplosionEntity(entity->scene,
            et->transform.pos, bsd->shooter_rid);

        Game_informPlayersOfRocketExplosion(&ks_game,
            bsd->shooter_rid, bsd->explosion_pos);

        Scene3D_removeEntity(entity->scene, entity);

        return;
    }

    if (!vec3_equals(bsd->last_position, et->transform.pos))
    {
        Player *enemy;
        PhysicsComponent *pc;

        for (uint i = 0; i < ks_game.num_players; ++i)
        {
            enemy = ks_game.active_players[i];

            if (enemy->pawn->replication_id == bsd->shooter_rid)
                continue;

            pc = Entity_getPhysicsComponent(enemy->pawn);
            for (SphereCollider *sc = pc->sphere_colliders; sc; sc = sc->next)
            {
                if (testSphere3DRayWithEndpoint(&sc->data, bsd->last_position,
                    et->transform.pos, &bsd->dist_to_hit))
                {
                    server_createRocketExplosionEntity(entity->scene,
                        et->transform.pos, bsd->shooter_rid);

                    Game_informPlayersOfRocketExplosion(&ks_game,
                        bsd->shooter_rid, bsd->explosion_pos);

                    Scene3D_removeEntity(entity->scene, entity);

                    return;
                }
            }
        }
    }

    vec3_copy(bsd->last_position, et->transform.pos);
}

int
Game_dealDamageToPlayer(Game *game, Player *p, replication_id32_t killer,
    int damage)
{
    p->health -= damage;
    if (p->health <= 0)
    {
        p->health = 0;
        Game_killPlayersPawn(game, p, killer);
    }
    Mutex_lock(&game->output_buffer.mutex);
    void *msg = OutputBuffer_queueMessageUnsafe(&game->output_buffer,
       p->client, KS_SC_MSG_OWN_HEALTH_INFORM,
       KS_SC_MSG_OWN_HEALTH_INFORM_SIZE);
    *(int*)msg = p->health;
    Mutex_unlock(&game->output_buffer.mutex);
    return p->health;
}

void
server_rocketStaticColliderBulletScript(Entity *entity, void *scriptdata)
{
    BulletScriptData *bsd = (BulletScriptData*)scriptdata;
    double delta = entity->scene->delta;
    EntityTransform *et = Entity_getTransform(entity);
    float time = bsd->dist_to_hit / bsd->bullet_velocity;

    bsd->timer += (float)delta;

    if (bsd->timer >= time)
    {
        server_createRocketExplosionEntity(&ks_game.scene,
            bsd->explosion_pos, bsd->shooter_rid);

        Game_informPlayersOfRocketExplosion(&ks_game,
            bsd->shooter_rid, bsd->explosion_pos);

        Scene3D_removeEntity(entity->scene, entity);

        return;
    }

    if (!vec3_equals(bsd->last_position, et->transform.pos))
    {
        Player              *enemy;
        PhysicsComponent    *pc;

        for (uint i = 0; i < ks_game.num_players; ++i)
        {
            enemy = ks_game.active_players[i];

            if (enemy->pawn->replication_id == bsd->shooter_rid) continue;

            pc = Entity_getPhysicsComponent(enemy->pawn);

            for (SphereCollider *sc = pc->sphere_colliders; sc; sc = sc->next)
            {
                if (testSphere3DRayWithEndpoint(&sc->data, bsd->last_position,
                    et->transform.pos, &bsd->dist_to_hit))
                {
                    vec3_copy(bsd->explosion_pos, et->transform.pos);
                    server_createRocketExplosionEntity(&ks_game.scene,
                        bsd->last_position, bsd->shooter_rid);
                    Game_informPlayersOfRocketExplosion(&ks_game,
                        bsd->shooter_rid, bsd->explosion_pos);
                    Game_dealDamageToPlayer(&ks_game, enemy, bsd->shooter_rid,
                        50);
                    Scene3D_removeEntity(entity->scene, entity);
                    return;
                }
            }
        }
    }

    vec3_copy(bsd->last_position, et->transform.pos);
}

static void
_informPlayersOfItemRespawn(int index, enum KsItemType type,  vec3 pos)
{
    /*Mutex_lock(&ks_game.player_access_mutex);*/
    Mutex_lock(&ks_game.output_buffer.mutex);

    void        *msg;
    ByteBuffer  b;
    Player      *p;
    b.max_size = KS_SC_MSG_ITEM_SPAWNED_SIZE;
    for (uint i = 0; i < ks_game.num_players; ++i)
    {
        p = ks_game.active_players[i];

        msg = OutputBuffer_queueMessageUnsafe(&ks_game.output_buffer, p->client,
            KS_SC_MSG_ITEM_SPAWNED, KS_SC_MSG_ITEM_SPAWNED_SIZE);

        if (!msg)
        {
            DEBUG_PRINTF("Failed to some players of a spawned item!\n");
            break;
        }

        b.offset = 0;
        b.memory = msg;
        KS_SC_MSG_ITEM_SPAWNED_serialize(&b, index, type, pos);
    }

    Mutex_unlock(&ks_game.output_buffer.mutex);
    /*Mutex_unlock(&ks_game.player_access_mutex);*/
}

void
server_medkitScript(Entity *e, void *data_ptr)
{
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)data_ptr;

    if (data->next_spawn_time != ITEM_IS_SPAWNED
    && getTimePassed() >= data->next_spawn_time)
    {
        data->next_spawn_time = ITEM_IS_SPAWNED;
        Transform *t = &Entity_getTransform(e)->transform;
        _informPlayersOfItemRespawn(data->index, ITEM_MEDKIT, t->pos);
        DEBUG_PRINTF("Medkit respawned.\n");
    }
}

void
server_rocketAmmoScript(Entity *e, void *data_ptr)
{
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)data_ptr;

    if (data->next_spawn_time != ITEM_IS_SPAWNED
    && getTimePassed() >= data->next_spawn_time)
    {
        data->next_spawn_time = ITEM_IS_SPAWNED;
        Transform *t = &Entity_getTransform(e)->transform;
        _informPlayersOfItemRespawn(data->index, ITEM_ROCKET_AMMO, t->pos);
        DEBUG_PRINTF("Rocket ammo respawned.\n");
    }
}

void
server_SMGAmmoScript(Entity *e, void *data_ptr)
{
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)data_ptr;

    if (data->next_spawn_time != ITEM_IS_SPAWNED
    && getTimePassed() >= data->next_spawn_time)
    {
        data->next_spawn_time = ITEM_IS_SPAWNED;
        Transform *t = &Entity_getTransform(e)->transform;
        _informPlayersOfItemRespawn(data->index, ITEM_SMG_AMMO, t->pos);
        DEBUG_PRINTF("SMG ammo respawned.\n");
    }
}

void
server_railgunAmmoScript(Entity *e, void *data_ptr)
{
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)data_ptr;

    if (data->next_spawn_time != ITEM_IS_SPAWNED
    && getTimePassed() >= data->next_spawn_time)
    {
        data->next_spawn_time = ITEM_IS_SPAWNED;
        Transform *t = &Entity_getTransform(e)->transform;
        _informPlayersOfItemRespawn(data->index, ITEM_RAILGUN_AMMO, t->pos);
        DEBUG_PRINTF("Railgun ammo respawned.\n");
    }
}

static void
_informPlayersOfItemCollision(int index, enum KsItemType type,
    replication_id32_t pawn_rid)
{
    /*Mutex_lock(&ks_game.player_access_mutex);*/
    Mutex_lock(&ks_game.output_buffer.mutex);

    void        *msg;
    ByteBuffer  b;
    Player      *p;
    b.max_size = KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN_SIZE;

    for (uint i = 0; i < ks_game.num_players; ++i)
    {
        p = ks_game.active_players[i];

        msg = OutputBuffer_queueMessageUnsafe(&ks_game.output_buffer, p->client,
            KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN,
            KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN_SIZE);

        if (!msg)
        {
            DEBUG_PRINTF("Failed to inform some players of a collided item!\n");
            break;
        }

        b.offset = 0;
        b.memory = msg;
        KS_SC_MSG_ITEM_COLLIDED_WITH_PAWN_serialize(&b, index, type, pawn_rid);
    }

    Mutex_unlock(&ks_game.output_buffer.mutex);
    /*Mutex_unlock(&ks_game.player_access_mutex);*/
}

static void
_updateScene(double dt)
{
    Scene3D *s = &ks_game.scene;

    s->delta = dt;

    /* Velocity, acceleration etc. calculations */
    bool32 is_first_frame = s->physics_timer == 0.0f;
    s->physics_timer += dt;

#if _CF_FIXED_PHYSICS_TIMESTEP
#else
#   error Server timestep must be fixed
#endif

    if (s->physics_timer >= s->physics_step || is_first_frame)
    {
        int num_steps;

        if (!is_first_frame)
            num_steps = (int)(s->physics_timer / s->physics_step);
        else
            num_steps = 1;

        Player              *p;
        Transform           *t;
        PhysicsComponent    *pc;

        float rot0, rot1;

        for (int i = 0; i < num_steps; ++i)
        {
            _executePlayersInputs();

            Scene3D_updatePhysics(s, s->physics_step);
            s->physics_timer = s->physics_timer - s->physics_step;

            /* Collision detection:
             * - Begin with preliminary detection
             * - Do more precise testing and generate events and cache entries
             * - Handle the generated collision events: physics responses and
             *   callbacks
             * - Remove old cache entries that are no longer valid */
            Scene3D_updateCollisions(s);
        }

        for (Player **pp = ks_game.active_players;
             pp < &ks_game.active_players[ks_game.num_players];
             ++pp)
        {
            p   = *pp;
            t   = &Entity_getTransform(p->pawn)->transform;
            pc  = Entity_getPhysicsComponent(p->pawn);

            Mutex_lock(&p->latest_input_mutex);
            p->latest_verified_input = p->input_buffer.latest_applied_input;
            vec3_copy(p->latest_verified_position, t->pos);
            vec3_copy(p->latest_verified_velocity, pc->velocity);
            p->latest_orientation = t->rot[1];
            p->kb_state = 0;
            p->have_new_verified_state = 1;
            Mutex_unlock(&p->latest_input_mutex);
        }
    }

    Scene3D_updateScripts(s);
    Scene3D_updateAnimators(s, dt);
    Scene3D_updateLightPositions(s, dt);
    Scene3D_updateModels(s);
    Scene3D_updateLifetimes(s, dt);

    /* Remove... removed entities */
    for (Entity **ep = s->entities.removables;
         ep < &s->entities.removables[s->entities.num_removables];
         ++ep)
    {
        Scene3D_removeEntityInner(s, *ep);
    }

    s->entities.num_removables = 0;
}

void
server_medkitCollisionCallback(PhysicsComponent *pca, PhysicsComponent *pcb,
    Entity *ea, Entity *eb,
    enum ColliderType cta, enum ColliderType ctb,
    enum CollisionEventType event_type,
    uint32_t collider_a, uint32_t collider_b)
{
    if (eb->type != ENTITY_TYPE_PLAYER_PAWN) return;

    ScriptComponent *sc = Entity_getScriptComponent(ea);
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)sc->data;

    if (data->next_spawn_time == ITEM_IS_SPAWNED)
    {
        data->next_spawn_time = getTimePassed() + data->cooldown;

        _informPlayersOfItemCollision(data->index, ITEM_MEDKIT,
            eb->replication_id);

        DEBUG_PRINTF("A player pawn collided with a medkit.\n");
    }
}


void
server_rocketAmmoCollisionCallback(PhysicsComponent *pca, PhysicsComponent *pcb,
    Entity *ea, Entity *eb,
    enum ColliderType cta, enum ColliderType ctb,
    enum CollisionEventType event_type,
    uint32_t collider_a, uint32_t collider_b)
{
    if (eb->type != ENTITY_TYPE_PLAYER_PAWN) return;

    ScriptComponent *sc = Entity_getScriptComponent(ea);
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)sc->data;

    if (data->next_spawn_time == ITEM_IS_SPAWNED)
    {
        data->next_spawn_time = getTimePassed() + data->cooldown;

        _informPlayersOfItemCollision(data->index, ITEM_ROCKET_AMMO,
            eb->replication_id);

        DEBUG_PRINTF("A player pawn collided with a rocket launcher ammo item.\n");
    }
}

void
server_SMGCollisionCallback(PhysicsComponent *pca, PhysicsComponent *pcb,
    Entity *ea, Entity *eb,
    enum ColliderType cta, enum ColliderType ctb,
    enum CollisionEventType event_type,
    uint32_t collider_a, uint32_t collider_b)
{
    if (eb->type != ENTITY_TYPE_PLAYER_PAWN) return;

    ScriptComponent *sc = Entity_getScriptComponent(ea);
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)sc->data;

    if (data->next_spawn_time == ITEM_IS_SPAWNED)
    {
        data->next_spawn_time = getTimePassed() + data->cooldown;

        _informPlayersOfItemCollision(data->index, ITEM_SMG_AMMO,
            eb->replication_id);

        DEBUG_PRINTF("A player pawn collided with an SMG ammo item.\n");
    }
}

void
server_railgunCollisionCallback(PhysicsComponent *pca, PhysicsComponent *pcb,
    Entity *ea, Entity *eb,
    enum ColliderType cta, enum ColliderType ctb,
    enum CollisionEventType event_type,
    uint32_t collider_a, uint32_t collider_b)
{
    if (eb->type != ENTITY_TYPE_PLAYER_PAWN) return;

    ScriptComponent *sc = Entity_getScriptComponent(ea);
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)sc->data;

    if (data->next_spawn_time == ITEM_IS_SPAWNED)
    {
        data->next_spawn_time = getTimePassed() + data->cooldown;

        _informPlayersOfItemCollision(data->index, ITEM_RAILGUN_AMMO,
            eb->replication_id);

        DEBUG_PRINTF("A player pawn collided with a railgun ammo item.\n");
    }
}

Entity *
server_createMedkitEntity(Scene3D *scene, int index, uint32_t cooldown,
    float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(scene, x, y, z);
    if (!e) return 0;
    Entity_setScale(e, 2.0f, 2.0f, 2.0f);

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
    pc->gravity_active = 0;

    /*PhysicsComponent_setCollisionCallback(pc, server_medkitCollisionCallback);*/

    SphereCollider *sp = PhysicsComponent_attachSphereCollider(pc, e, 0);
    SET_BITFLAG_ON(sp->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY);
    sp->radius = MEDKIT_RADIUS;

    ScriptComponent *sc = Entity_attachScriptComponent(e);
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)ScriptComponent_allocateData(sc,
        sizeof(ItemSpawnScriptData));
    ScriptComponent_setCallback(sc, server_medkitScript);
    data->index = index;

    data->next_spawn_time   = 0;
    data->cooldown          = cooldown;

    return e;
}

Entity *
server_createRocketAmmoEntity(Scene3D *scene, int index, uint32_t cooldown,
    float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(scene, x, y, z);
    if (!e) return 0;
    Entity_setScale(e, 0.5f, 0.5f, 0.5f);

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
    pc->gravity_active = 0;

#if 0
    PhysicsComponent_setCollisionCallback(pc,
        server_rocketAmmoCollisionCallback);
#endif

    SphereCollider *sphere = PhysicsComponent_attachSphereCollider(pc, e, 0);
    SET_BITFLAG_ON(sphere->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY);
    sphere->radius = ROCKET_AMMO_RADIUS;

    ScriptComponent *sc = Entity_attachScriptComponent(e);
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)ScriptComponent_allocateData(sc,
        sizeof(ItemSpawnScriptData));
    ScriptComponent_setCallback(sc, server_rocketAmmoScript);
    data->index = index;

    data->next_spawn_time   = 0;
    data->cooldown          = cooldown;

    return e;
}

Entity *
server_createSMGAmmoEntity(Scene3D *scene, int index, uint32_t cooldown,
    float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(scene, x, y, z);
    if (!e) return 0;
    Entity_setScale(e, 0.5f, 0.5f, 0.5f);

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
    pc->gravity_active = 0;

#if 0
    PhysicsComponent_setCollisionCallback(pc,
        server_SMGCollisionCallback);
#endif

    SphereCollider *sphere = PhysicsComponent_attachSphereCollider(pc, e, 0);
    SET_BITFLAG_ON(sphere->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY);
    sphere->radius = SMG_AMMO_RADIUS;

    ScriptComponent *sc = Entity_attachScriptComponent(e);
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)ScriptComponent_allocateData(sc,
        sizeof(ItemSpawnScriptData));
    ScriptComponent_setCallback(sc, server_SMGAmmoScript);
    data->index = index;

    data->next_spawn_time   = 0;
    data->cooldown          = cooldown;

    return e;
}

Entity *
server_createRailgunAmmoEntity(Scene3D *scene, int index, uint32_t cooldown,
    float x, float y, float z)
{
    Entity *e = Scene3D_createEntity(scene, x, y, z);
    if (!e) return 0;
    Entity_setScale(e, 0.25f, 0.25f, 0.25f);

    PhysicsComponent *pc = Entity_attachPhysicsComponent(e);
    pc->gravity_active = 0;

#if 0
    PhysicsComponent_setCollisionCallback(pc,
        server_railgunCollisionCallback);
#endif

    SphereCollider *sphere = PhysicsComponent_attachSphereCollider(pc, e, 0);
    SET_BITFLAG_ON(sphere->flags, char, COLLIDER_FLAG_IS_SIMPLE_TRIGGER_ONLY);
    sphere->radius = RAILGUN_AMMO_RADIUS;

    ScriptComponent *sc = Entity_attachScriptComponent(e);
    ItemSpawnScriptData *data = (ItemSpawnScriptData*)ScriptComponent_allocateData(sc,
        sizeof(ItemSpawnScriptData));
    ScriptComponent_setCallback(sc, server_railgunAmmoScript);
    data->index = index;

    data->next_spawn_time   = 0;
    data->cooldown          = cooldown;

    return e;
}

int
Game_informPlayersOfRocketShot(Game *game, Player *shooter, vec3 pos, vec3 dir)
{
    Player *op;
    void *shot_msg;
    ByteBuffer buf;
    int num_errors = 0;

    for (uint i = 0; i < game->num_players; ++i)
    {
        op = game->active_players[i];
        if (op == shooter)
            continue;

        shot_msg = Server_queueMessage(&ks_server, op->client,
            KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER,
            KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER_SIZE);

        if (shot_msg)
        {
            ByteBuffer_init(&buf, shot_msg,
                KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER_SIZE);
            KS_SC_MSG_OTHER_PAWN_SHOT_ROCKET_LAUNCHER_serialize(&buf,
                shooter->pawn->replication_id, pos, dir);
        }
        else
            num_errors++;
    }
    return num_errors;
}

int
Game_informPlayersOfSMGShot(Game *game, Player *shooter, vec3 pos, vec3 dir)
{
    Player      *op;
    void        *shot_msg;
    ByteBuffer  buf;
    int         num_errors = 0;

    for (uint i = 0; i < game->num_players; ++i)
    {
        op = game->active_players[i];
        if (op == shooter)
            continue;

        shot_msg = Server_queueMessage(&ks_server, op->client,
            KS_SC_MSG_OTHER_PAWN_SHOT_SMG,
            KS_SC_MSG_OTHER_PAWN_SHOT_SMG_SIZE);

        if (shot_msg)
        {
            ByteBuffer_init(&buf, shot_msg,
                KS_SC_MSG_OTHER_PAWN_SHOT_SMG_SIZE);
            KS_SC_MSG_OTHER_PAWN_SHOT_SMG_serialize(&buf,
                shooter->pawn->replication_id, pos, dir);
        }
        else
            num_errors++;
    }
    return num_errors;

}

void
server_rocketExplosionScript(Entity *entity, void *scriptdata)
{
    float *timer = &((ExplosionScriptData*)scriptdata)->timer;
    double delta = entity->scene->delta;
    *(timer) += (float)delta * 15.0f;
    Entity_setScale(entity, *(timer), *(timer), *(timer));
}

void
server_explosionCollisionCallback(PhysicsComponent *component_a,
    PhysicsComponent *component_b,
    Entity *entity_a, Entity *entity_b, enum ColliderType collider_a_type,
    enum ColliderType collider_b_type, enum CollisionEventType type,
    uint32_t a_id, uint32_t b_id)
{
    if (type == COLLISION_ENTER)
    {
        vec3 pos1;
        vec3_copy(pos1, Entity_getTransform(entity_a)->transform.pos);
        vec3 pos2;
        vec3_copy(pos2, Entity_getTransform(entity_b)->transform.pos);
        vec3 direction;
        vec3_sub(direction, pos2, pos1);
        EntityTransform *et = Entity_getTransform(entity_b);
        PhysicsComponent_applyImpactForce(component_b, direction, 5.0f);

        ScriptComponent *sc = Entity_getScriptComponent(entity_a);

        if (entity_b->type == ENTITY_TYPE_PLAYER_PAWN
        &&  entity_b->replication_id != ((ExplosionScriptData*)sc->data)->shooter_rid)
        {
            Player *player = 0;
            for (uint i = 0; i < ks_game.num_players; ++i)
            {
                if (ks_game.active_players[i]->pawn
                && ks_game.active_players[i]->pawn->id == entity_b->id)
                {
                    player = ks_game.active_players[i];
                    break;
                }
            }

            if (player)
                Game_dealDamageToPlayer(&ks_game, player,
                    ((ExplosionScriptData*)sc->data)->shooter_rid, 50);
        }
    }
}

void
server_createRocketExplosionEntity(Scene3D *scene, vec3 pos,
    replication_id32_t shooter_rid)
{
    Entity *explosion = Scene3D_createEntity(scene, pos[0], pos[1], pos[2]);

    PhysicsComponent *pc = Entity_attachPhysicsComponent(explosion);
    PhysicsComponent_setActivity(pc, 1);
    PhysicsComponent_setStaticity(pc, 1);
    PhysicsComponent_setCollisionCallback(pc, server_explosionCollisionCallback);
    pc->gravity_active = 0;

    ScriptComponent *sc = Entity_attachScriptComponent(explosion);
    ScriptComponent_allocateData(sc, sizeof(ExplosionScriptData));
    ((ExplosionScriptData*)sc->data)->timer = 0;
    ((ExplosionScriptData*)sc->data)->shooter_rid = shooter_rid;
    ScriptComponent_setCallback(sc, server_rocketExplosionScript);
    Entity_setLifetime(explosion, 0.2f);

    SphereCollider *sphereCollider = PhysicsComponent_attachSphereCollider(pc,
        explosion, 0);
    sphereCollider->radius = 2.0f;
}

int
Game_informPlayersOfRocketExplosion(Game *game, replication_id32_t shooter_rid,
    vec3 pos)
{
    Player      *op;
    void        *expl_msg;
    ByteBuffer  buf;
    int         num_errors = 0;

    Mutex_lock(&game->output_buffer.mutex);

    for (uint i = 0; i < game->num_players; ++i)
    {
        op = game->active_players[i];

        if (op->pawn->replication_id == shooter_rid) continue;

        expl_msg = OutputBuffer_queueMessageUnsafe(&game->output_buffer, op->client,
            KS_SC_MSG_OTHERS_ROCKET_EXPLOSION,
            KS_SC_MSG_OTHERS_ROCKET_EXPLOSION_SIZE);

        if (expl_msg)
        {
            ByteBuffer_init(&buf, expl_msg,
                KS_SC_MSG_OTHERS_ROCKET_EXPLOSION_SIZE);
            vec3_copy((float*)expl_msg, pos);
        }
        else
            num_errors++;
    }

    Mutex_unlock(&game->output_buffer.mutex);

    return num_errors;
}
