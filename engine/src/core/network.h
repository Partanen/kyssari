#pragma once
#include "packets.h"

typedef struct Message                  Message;
typedef struct Message*                 MessagePtr;
typedef struct Packet                   Packet;
typedef struct Connection               Connection;
typedef struct ClientGame               ClientGame;
typedef struct Client                   Client;
typedef struct ReplicatedEntityTable    ReplicatedEntityTable;
typedef struct ServerGamePlayer         ServerGamePlayer;
typedef struct ServerGame               ServerGame;
typedef struct Server                   Server;
typedef struct ServerClient             ServerClient;

#define KYS_MTU                     1472
#define MAX_CLIENTS                 64
#define MAX_SERVER_GAMES            4
#define NUM_LATENCY_CACHE_ENTRIES   8

enum ClientError
{
    CLIENT_ERROR_LISTENER_OPEN = 1,
    CLIENT_ERROR_SENDER_OPEN,
    CLIENT_ERROR_LISTENER_BIND,
    CLIENT_ERROR_ALREADY_CONNECTING,
    CLIENT_ERROR_ALREADY_CONNECTED,
    CLIENT_ERROR_NOT_CONNECTED,
    CLIENT_ERROR_ALREADY_INITIALIZED,
    CLIENT_ERROR_SOCK_OPTIONS,
    CLIENT_ERROR_OUT_OF_MEMORY,
    CLIENT_ERROR_INVALID_PARAMETER
};

enum ClientStatus
{
    CLIENT_STATUS_DISCONNECTED = 0,
    CLIENT_STATUS_CONNECTING,
    CLIENT_STATUS_CONNECTED
};

enum ServerError
{
    SERVER_ERROR_ALREADY_RUNNING = 1,
    SERVER_ERROR_LISTENER_OPEN,
    SERVER_ERROR_LISTENER_BIND,
    SERVER_ERROR_ALREADY_INITIALIZED,
    SERVER_ERROR_NOT_INITIALIZED,
    SERVER_ERROR_OUT_OF_MEMORY,
    SERVER_ERROR_SOCK_OPTIONS,
    SERVER_ERROR_INVALID_PARAMETER,
    SERVER_ERROR_NOT_RUNNING
};

enum ServerClientStatus
{
    SERVER_CLIENT_UNAUTHED = 0,
    SERVER_CLIENT_AUTHED
};

struct Message
{
    message_t   type;
    uint16_t    id;
    int         size;
    uint32_t    time_last_sent;
};

struct Packet
{
    uint32_t        send_time;
    uint32_t        ack_bits; /* Not sure if we even need to store this */

    /* Store both the id and type of the message so that a possible
     * on_packet_acked callback function does not need to first look
     * the message up to find out it's type */
    struct MessageSendData
    {
        uint16_t type;
        uint16_t id;
    } messages[32];

    uint16_t        num_messages;
    uint16_t        oldest_contained_message;
};

SEQUENCE_BUFFER_DECLARATION(MessagePtr);
FIFO_QUEUE_DECLARATION(MessagePtr);
SEQUENCE_BUFFER_DECLARATION(Packet);
SEQUENCE_BUFFER_DECLARATION(uint16_t);

/* A structure to keep track of a connection to the machine identified by the
 * "address" member of the struct */
struct Connection
{
    Address                     address;
    DynamicMemoryBlock          *allocator;
    uint                        max_message_types;
    uint32_t                    time_last_received_valid_data;

    uint32_t                    average_latency;
    uint32_t                    latency_cache[NUM_LATENCY_CACHE_ENTRIES];

    /* Out-going messages */
    SequenceBuffer_MessagePtr   message_sb_out;
    FIFOQueue_MessagePtr        message_queue_out;
    uint16_t                    message_id_out;
    uint16_t                    oldest_unacked_message;
    bool32                      have_unacked_messages;

    /* Out-going packets */
    SequenceBuffer_Packet       packet_sb_out;
    uint16_t                    packet_id_out;
    uint16_t                    oldest_unacked_packet;

    /* In-coming messages */
    SequenceBuffer_uint16_t     packet_id_sb_in;

    uint16_t                    last_received_packet;
    uint32_t                    last_sent_something;

    /* Parameters */
    uint32_t                    keep_alive_frequency; /* ms */
    uint32_t                    message_resend_frequency;

    void                        (*on_receive_packet_callback)(void *packet,
                                    int size, void *receive_args);
    void                        *packet_receive_args;

    void                        (*on_packet_acked_callback)(Packet *p,
                                    void *args);
    void                        *packet_ack_args;
};

struct ClientGame
{
    bool32  is_on;
    Scene3D *scene;
};

struct Client
{
    bool32                  initialized;
    Socket                  socket;
    Connection              connection;
    Thread                  thread;
    Clock                   clock;
    MemoryPool              memory_pool;
    DynamicMemoryBlock      memory;

    volatile bool32         running;
    enum ClientStatus       status;

    uint                    last_sent_connection_request;
    ByteBuffer              buffer;

    ClientGame              game;

    /* Incoming messages */
    SequenceBuffer_uint16_t message_id_sb_in;

    /* Parameters */
    uint32_t                try_connect_timeout;
    uint32_t                server_timeout; /* Milliseconds */

    /* Callbacks */

    /* Called when a server refuses a requested connection */
    void (*on_connection_refused_callback)(Client *client,
        enum ConnectionRefuseReason reason);

    /* Called when a server accepts a requested connection */
    void (*on_connection_accepted_callback)(Client *client);

    /* Called when a non-built-in message is received.
     * This MUST return the size of the message, or < 0 if buffer_size is
     * less than the minimum size of the message. The size does not contain
     * MIN_MSG_SIZE (sizeof(message_t) + sizeof(uint16_t))*/
    int (*on_receive_message_callback)(Client *client, message_t msg_type,
        uint16_t msg_id, void *message, int size,
        bool32 received_before);

    /* Called every frame */
    void (*on_update_callback)(Client *client);

    /* Called when an ack for a packet is received from the server.
     * on_packet_acked_args are passed in as an argument.
     * If not wanted to be left null, this MUST be set after initialization,
     * but before calling Client_connect(). */
    void (*on_packet_acked_callback)(Packet *p, void *args);
    void *on_packet_acked_args;
};

struct ServerClient
{
    bool32                      reserved;
    Connection                  connection;
    SequenceBuffer_uint16_t     message_id_sb_in;
    enum ServerClientStatus     status;
    uint                        last_replied_to;
    DynamicMemoryBlock          memory;
    bool32                      is_in_game;
    ServerClient                *next, *prev;
    /* A pointer to data regarding
     * this client provided by the user */
    void                        *user_data;
};

struct ServerGamePlayer
{
    ServerClient *client;
};

struct ServerGame
{
    bool32                  running;
    Server                  *server;
    Scene3D                 *scene;
    ServerGamePlayer        players[MAX_CLIENTS];
    uint                    max_players;
    uint                    num_players;
};

struct Server
{
    bool32                      initialized;
    volatile bool32             running;
    MemoryPool                  memory_pool;
    Thread                      thread;
    Socket                      socket;
    Clock                       clock;
    size_t                      num_bytes_received;
    size_t                      num_bytes_sent;
    /* A little stupid, but this pointer exists so that the
     * server can be passed in as a void * argument to a ServerClient
     * connection's on_packet_receive callback. */
    ServerClient                *currently_handled_client;

    /* Left overs from testing this thing out initially */
    void                        *send_buffer_memory;
    ByteBuffer                  send_buffer;
    char                        *recv_buffer;
    int                         recv_buffer_size;

    ServerClient                *clients;
    ServerClient                *free_clients;
    ServerClient                *active_clients;
    uint                        max_clients;
    uint                        num_active_clients;

    ServerGame                  games[MAX_SERVER_GAMES];
    uint                        num_games;

    /* Parameters */
    uint32_t                    client_timeout; /* Milliseconds */

    /* Callbacks */

    /* When a client requests connection, the user has a chance to check
     * the address here. If the connection from this address is to be accepted, 
     * the function should return 1 - other wise 0. */
    bool32 (*on_connection_request_callback)(Server *server, Address *addr);

    /* Called when a client successfully connects to the server */
    void (*on_accept_client_callback)(Server*,ServerClient*);

    /* Called when a client is disconnected via any means, be it a time-out
     * or purposeful disconnect. Sending packets at this point is no longer
     * possible, and the client instance instance becomes invalid right after
     * this function has been called.  */
    void (*on_client_disconnected_callback)(Server*,ServerClient*);

    /* Called when a client informs the server it will be disconnecting.
     * The code sent by the client is passed in as an argument (default: 0) */
    void (*on_client_notified_disconnect)(Server*,ServerClient*,char code);

    /* Called every frame by the server */
    void (*on_update_callback)(Server *server);

    /* Called when a non-built-in message is received.
     * This MUST return the size of the message, or < 0 if buffer_size is
     * less than the minimum size of the message. The size should not contain
     * MIN_MSG_SIZE (sizeof(message_t) + sizeof(uint16_t)) */
    int (*on_receive_message_callback)(Server*, ServerClient*,
        message_t msg, uint16_t msg_id, void *message, int size,
        bool32 received_before);
};

int
Client_init(Client *client);

int
Client_connect(Client *client, Address *server);

int
Client_disconnect(Client *client);

/* Set the scene two which all received replication commands will be applied to*/
int
Client_setReplicatedScene(Client *client, Scene3D *scene);

void
Client_setFPS(Client *client, uint fps);

void *
Client_queueMessage(Client *client, message_t type, int data_size);

static inline uint32_t
Client_getAverageLatency(Client *c);

static inline bool32
Client_wasMessageAcked(Client *c, uint16_t id);

/* Once initialized, a server will not be uninitialized until the end of the
 * program. It can how ever be run or shutdown at request. */
int
Server_init(Server *server, uint max_clients);

int
Server_run(Server *server);

int
Server_shutdown(Server *server);

/* The int returned is a handle to the game and should be saved.
 * Returns < 0 if unsuccessful. */
ServerGame *
Server_createGame(Server *server, Scene3D *scene, uint max_players);

void *
Server_queueMessage(Server *server,
    ServerClient *client,
    message_t type,
    int data_size); /* Size without sizeof(message_t) + sizeof(msg_sequence) */

void
Server_setFPS(Server *server, uint fps);

/* Scene passed in can be 0 */
void
ServerGame_setReplicatedScene(ServerGame *game, Scene3D *scene);

int
ServerGame_attachClient(ServerGame *game, ServerClient *client);

int
ServerGame_informPlayersOfEntity(ServerGame *game, Entity *entity);

int
ServerGame_informPlayerOfAllEntities(ServerGame *game, ServerClient *client);

static inline uint32_t
ServerClient_getAverageLatency(ServerClient *sc);

static inline void
ServerClient_setUserData(ServerClient *sc, void *data);

static inline void *
ServerClient_getUserData(ServerClient *sc);

static inline void
ServerClient_setUserData(ServerClient *sc, void *data)
{
    ASSERT(sc->reserved && "Attempted to modify invalid ServerClient.\n");
    sc->user_data = data;
}

static inline void *
ServerClient_getUserData(ServerClient *sc)
{
    ASSERT(sc->reserved && "Attempted to read invalid ServerClient.\n");
    return sc->user_data;
};

static inline uint32_t
Client_getAverageLatency(Client *c)
{
    return c->connection.average_latency;
}

static inline bool32
Client_wasMessageAcked(Client *c, uint16_t id)
{
    return SequenceBuffer_MessagePtr_getBySequence(
        &c->connection.message_sb_out, id) == 0;
}

static inline uint32_t
ServerClient_getAverageLatency(ServerClient *sc)
{
    return sc->connection.average_latency;
}
