#pragma once
typedef struct OBB3D    OBB3D;
typedef struct Sphere3D Sphere3D;
typedef struct Plane    Plane;
typedef struct Triangle Triangle;
typedef struct AABB3D   AABB3D;

struct Sphere3D
{
    vec3    center;
    float   radius;
};

struct OBB3D
{
    vec3    center;
    mat3x3  rotation;
    vec3    dimensions;
};

struct Plane
{
    vec3    normal;
    float   dot;
};

struct Triangle
{
    vec3 points[3];
    vec3 normal;
};

struct AABB3D
{
    vec3 min, max;
};

static inline void
computeSphereAABB(AABB3D *aabb, vec3 center, float radius);

static inline float
Triangle_computePlaneDot(Triangle *t);

static inline float
Triangle_computePlaneDotNormalAndPoint(vec3 normal, vec3 point);

static inline float
computeOBB3DPlanePenetration(OBB3D *obb, vec3 p_normal, float p_dot);

static inline float
computeOBB3DTrianglePenetration(OBB3D *obb, Triangle *triangle);

static inline int
testSphere3DRay(Sphere3D *sphere, vec3 origin, vec3 direction);

static inline int
testSphere3DRayWithEndpoint(Sphere3D *sphere, vec3 origin, vec3 direction, float *dist_from_origin);

static inline int
OBB3DRay(OBB3D *obb, vec3 origin, vec3 direction, float length, vec3 hitpoint, float *hit_dist);

static inline int
testTriangle3DRay(vec3 a, vec3 b, vec3 c, vec3 origin, vec3 direction,
    vec3 ret_point, float *dist_from_origin);

static inline bool32
testSphere3DSphere3D(Sphere3D *sphere1, Sphere3D *sphere2);

static inline bool32
testSphere3DSphere3DCheap(Sphere3D *sp_a, Sphere3D *sp_b);

static inline bool32
testSphere3DSphere3DAndGetData(Sphere3D *sp_a, Sphere3D *sp_b,
    vec3 direction_from_a, float *penetration);

bool32
testOBB3DOBB3D(OBB3D *obb1, OBB3D *obb2);

/* Return values:
 * 1-3:     obb2 collides with face of obb1
 * 4-6:     obb1 collides with face of obb2
 * 7-15:    colliding edge-edge.
 * Normal is a normal of a face belonging to obb_b */
int
testOBB3DOBB3DAndGetData(OBB3D *obb_a, OBB3D *obb_b,
    vec3 collision_normal, float *separation);

static inline bool32
testSphere3DOBB3DAndGetData(Sphere3D *sphere, OBB3D *obb,
    vec3 point, vec3 ret_normal, float *penetration);

static inline bool32
testSphere3DOBB3DCheap(Sphere3D *sphere, OBB3D *obb);

static inline bool32
testAABB3DAABB3D(AABB3D *a, AABB3D *b);

bool32
testOBB3DTriangle(OBB3D *obb, vec3 ta, vec3 tb, vec3 tc, vec3 normal, float *penetration);

static inline bool32
testSphere3DTriangle(Sphere3D *sphere, vec3 ta, vec3 tb, vec3 tc, vec3 ret_pt);

static inline bool32
testSphere3DTriangleAndGetData(Sphere3D *sphere, vec3 ta, vec3 tb, vec3 tc,
    vec3 pt, float *pen);

static inline void
OBB3D_setRotation(OBB3D *obb, float x, float y, float z);

static inline void
OBB3D_closestPointToPoint(OBB3D *obb, vec3 ret, vec3 pt);

static inline float
OBB3D_getRadius(OBB3D *obb);

static inline void
Triangle_closestPointToPoint(vec3 a, vec3 b, vec3 c, vec3 p, vec3 ret);

static inline AABB3D
Triangle_getAABB3D(Triangle *triangle);

static inline AABB3D
OBB3D_getAABB3D(OBB3D *obb);

static inline bool32
AABB3D_containsAABB3D(AABB3D *container, AABB3D *contained);

static inline float
AABB3D_computeRadius(AABB3D *aabb);




static inline void
computeSphereAABB(AABB3D *aabb, vec3 center, float radius)
{
    for (int i = 0; i < 3; ++i) aabb->min[i] = center[i] - radius;
    for (int i = 0; i < 3; ++i) aabb->max[i] = center[i] + radius;
}

static inline float
Triangle_computePlaneDot(Triangle *t)
{
    return Triangle_computePlaneDotNormalAndPoint(t->normal, t->points[0]);
}

static inline float
Triangle_computePlaneDotNormalAndPoint(vec3 normal, vec3 point)
{
    return vec3_mul_inner(normal, point);
}

static inline float
computeOBB3DPlanePenetration(OBB3D *obb, vec3 p_normal, float p_dot)
{
    float radius =
        obb->dimensions[0] * ABS(vec3_mul_inner(p_normal, obb->rotation[0])) +
        obb->dimensions[1] * ABS(vec3_mul_inner(p_normal, obb->rotation[1])) +
        obb->dimensions[2] * ABS(vec3_mul_inner(p_normal, obb->rotation[1]));
    float distance_from_center = vec3_mul_inner(p_normal, obb->center) - p_dot;
    return radius - ABS(distance_from_center);
}

static inline float
computeOBB3DTrianglePenetration(OBB3D *obb, Triangle *triangle)
{
    float d = Triangle_computePlaneDot(triangle);
    return computeOBB3DPlanePenetration(obb, triangle->normal, d);
}

static inline int
testSphere3DRay(Sphere3D *sphere, vec3 origin, vec3 direction)
{
    //vec3 direction;
    //vec3_sub(direction, endpoint, origin);
    //vec3_norm(direction, direction);
    //DEBUG_PRINTF("Direction: %f. %f. %f\n", direction[0], direction[1], direction[2]);
    vec3 m;
    vec3_sub(m, origin, sphere->center);
    float c = vec3_mul_inner(m, m) - sphere->radius * sphere->radius;
    if (c <= 0.0f) return 1;
    float b = vec3_mul_inner(m, direction);
    if (b > 0.0f) return 0;
    float disc = b * b - c;
    if (disc < 0.0f) return 0;
    return 1;
}

static inline int
testSphere3DRayWithEndpoint(Sphere3D *sphere, vec3 origin, vec3 endpoint, float *dist_from_origin)
{
    float mu1, mu2;
    float abc;
    vec3 direction;
    vec3 distance_vec;
    vec3_sub(distance_vec, endpoint, sphere->center);
    vec3_sub(direction, endpoint, origin);
    float a = vec3_mul_inner(direction, direction);
    float b = 2 * (direction[0] * (origin[0] - sphere->center[0]) + direction[1] * (origin[1] - sphere->center[1]) + direction[2] * (origin[2] - sphere->center[2]));
    float c = vec3_mul_inner(sphere->center, sphere->center);
    c += vec3_mul_inner(origin, origin);
    c -= 2 * vec3_mul_inner(sphere->center, origin);
    c -= sphere->radius * sphere->radius;
    abc = b * b - 4 * a * c;
    if (ABS(a) < FLT_EPSILON || abc < 0)
        return 0;

    mu1 = (-b + sqrtf(abc)) / (2 * a);
    mu2 = (-b - sqrtf(abc)) / (2 * a);
    if ((mu1 <= 1.0f && mu1 >= -1.0f) || (mu2 <= 1.0f && mu2 >= -1.0f))
    {
        *dist_from_origin = sqrtf(POWER_OF_2(distance_vec[0]) + POWER_OF_2(distance_vec[1]) + POWER_OF_2(distance_vec[2]));
        return 1;
    }
    else
        return 0;
}

static inline int
OBB3DRay(OBB3D *obb, vec3 origin, vec3 direction, float length, vec3 hitpoint, float *hit_dist)
{
    vec3 endpoint;
    vec3 temp;
    vec3_scale(temp, direction, length);
    vec3_add(endpoint, origin, temp);
    mat3x3 rot;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            rot[i][j] = obb->rotation[i][j] + FLT_EPSILON;
        }
    }

    vec3 trans_origin;
    vec3 trans_endpoint;
    vec3_sub(trans_origin, origin, obb->center);
    vec3_sub(trans_endpoint, endpoint, obb->center);
    vec3_mul_mat3x3(trans_origin, trans_origin, rot);

    vec3 d;
    vec3_copy(d, direction);
    vec3_mul_mat3x3(d, d, rot);
    vec3_norm(d, d);


    vec3 min;
    float *max = obb->dimensions;
    vec3_scale(min, obb->dimensions, -1.0f);


    vec3 t_1;
    vec3 t_2;
    float tnear = -FLT_MAX;
    float tfar = FLT_MAX;


    for (int i = 0; i < 3; i++)
    {
        if (d[i] == 0)
        {
            if ((trans_origin[i] < min[i]) || (trans_origin[i] > max[i]))
                return 0;
        }
        else
        {
            t_1[i] = (min[i] - trans_origin[i]) / d[i];
            t_2[i] = (max[i] - trans_origin[i]) / d[i];

            if (t_1[i] > t_2[i])
            {
                vec3 temp;
                vec3_copy(temp, t_1);
                vec3_copy(t_1, t_2);
                vec3_copy(t_2, temp);
            }
            if (t_1[i] > tnear)
                tnear = t_1[i];
            if (t_2[i] < tfar)
                tfar = t_2[i];
            if ((tnear > tfar) || (tfar < 0))
                return 0;
        }
    }
    *hit_dist = tnear;
    vec3_scale(temp, direction, tnear);
    vec3_add(hitpoint, origin, temp);

    return 1;
}

static inline int
testTriangle3DRay(vec3 a, vec3 b, vec3 c, vec3 origin, vec3 direction, vec3 ret_point, float *dist_from_origin)
{
    vec3 e1, e2;
    vec3 P, Q, T;
    float det, inv_det, u, v, t;

    vec3_sub(e1, b, a);
    vec3_sub(e2, c, a);

    vec3_mul_cross(P, direction, e2);

    det = vec3_mul_inner(e1, P);

    if (det > -FLT_EPSILON && det < FLT_EPSILON)
        return 0;

    inv_det = 1.0f / det;

    vec3_sub(T, origin, a);

    u = vec3_mul_inner(T, P) * inv_det;

    if (u < 0.0f || u > 1.0f)
        return 0;
    vec3_mul_cross(Q, T, e1);

    v = vec3_mul_inner(direction, Q) * inv_det;

    if (v < 0.0f || u + v > 1.0f)
        return 0;
    t = vec3_mul_inner(e2, Q) * inv_det;

    if (t > FLT_EPSILON)
    {
        //collision piste coordinaatteina
        vec3 direction_to_length;
        vec3_scale(direction_to_length, direction, t);
        vec3_add(ret_point, origin, direction_to_length);

        // collision matka
        *dist_from_origin = t;
        return 1;
    }

    return 0;
}

static inline bool32
testSphere3DSphere3D(Sphere3D *sp_a, Sphere3D *sp_b)
{
    vec3 tmp;
    vec3_sub(tmp, sp_a->center, sp_b->center);
    return vec3_len(tmp) <= sp_a->radius + sp_b->radius;
}

static inline bool32
testSphere3DSphere3DCheap(Sphere3D *sp_a, Sphere3D *sp_b)
{
    vec3 tmp;
    vec3_sub(tmp, sp_b->center, sp_a->center);
    return vec3_mul_inner(tmp, tmp) <= POWER_OF_2(sp_a->radius) + POWER_OF_2(sp_b->radius);
}

static inline bool32
testSphere3DSphere3DAndGetData(Sphere3D *sp_a, Sphere3D *sp_b,
    vec3 direction_from_a, float *penetration)
{
    vec3_sub(direction_from_a, sp_b->center, sp_a->center);
    float distance  = vec3_len(direction_from_a);
    float rads      = sp_a->radius + sp_b->radius;

    if (distance <= rads)
    {
        vec3_norm(direction_from_a, direction_from_a);
        if (isnan(direction_from_a[0]) || isnan(direction_from_a[1])
        ||  isnan(direction_from_a[2]))
            return 0;
        *penetration = rads - distance;
        return 1;
    }

    return 0;
}

static inline bool32
testSphere3DTriangle(Sphere3D *sphere, vec3 ta, vec3 tb, vec3 tc, vec3 pt)
{
    Triangle_closestPointToPoint(ta, tb, tc, sphere->center, pt);

    vec3 tmp;
    vec3_sub(tmp, pt, sphere->center);

    return vec3_mul_inner(tmp, tmp) <= POWER_OF_2(sphere->radius);
}

static inline bool32
testSphere3DTriangleAndGetData(Sphere3D *sphere, vec3 ta, vec3 tb, vec3 tc,
    vec3 pt, float *pen)
{
    Triangle_closestPointToPoint(ta, tb, tc, sphere->center, pt);

    vec3 tmp;
    vec3_sub(tmp, pt, sphere->center);

    float d = vec3_mul_inner(tmp, tmp);

    if (d <= POWER_OF_2(sphere->radius))
    {
        *pen = -(sphere->radius - sqrtf(d));
        return 1;
    }

    return 0;
}

static inline void
Triangle_closestPointToPoint(vec3 a, vec3 b, vec3 c, vec3 p, vec3 ret)
{
    vec3 ab, ac, ap;

    vec3_sub(ab, b, a);
    vec3_sub(ac, c, a);
    vec3_sub(ap, p, a);

    float d1 = vec3_mul_inner(ab, ap);
    float d2 = vec3_mul_inner(ac, ap);

    /* Barycentric coordinates 1, 0, 0 */
    if (d1 <= 0.0f && d2 <= 0.0f)
    {
        vec3_copy(ret, a);
        return;
    }

    vec3 bp;
    vec3_sub(bp, p, b);
    float d3 = vec3_mul_inner(ab, bp);
    float d4 = vec3_mul_inner(ac, bp);

    /* Barycentric coordinates 0, 1, 0 */
    if (d3 >= 0.0f && d4 <= d3)
    {
        vec3_copy(ret, b);
        return;
    }

    float vc = d1 * d4 - d3 * d2;

    /* Barycentric coordinates 1-v, v, 0 */
    if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f)
    {
        vec3_scale(ab, ab, (d1 / (d1 - d3)));
        vec3_add(ret, a, ab);
        return;
    }

    vec3 cp;
    vec3_sub(cp, p, c);

    float d5 = vec3_mul_inner(ab, cp);
    float d6 = vec3_mul_inner(ac, cp);

    /* Barycentric coordinates 0, 0, 1 */
    if (d6 >= 0.0f && d5 <= d6)
    {
        vec3_copy(ret, c);
        return;
    }

    float vb = d5 * d2 - d1 * d6;

    /* Barycentric coordinates 1-w, 0, w */
    if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f)
    {
        float w = d2 / (d2 - d6);
        vec3_scale(ac, ac, w);
        vec3_add(ret, a, ac);
        return;
    }

    float va = d3 * d6 - d5 * d4;

    /* Barycentric coordinates 0, 1-w, w */
    if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f)
    {
        float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
        vec3 tmp;
        vec3_sub(tmp, c, b);
        vec3_scale(tmp, tmp, w);
        vec3_add(ret, b, tmp);
        return;
    }

    float denom = 1.0f / (va + vb + vc);
    float v = vb * denom;
    float w = vc * denom;

    vec3_scale(ab, ab, v);
    vec3_scale(ac, ac, w);
    vec3_add(ret, a, ab);
    vec3_add(ret, ret, ac);
}

static inline AABB3D
Triangle_getAABB3D(Triangle *triangle)
{
    AABB3D aabb;

    aabb.min[0] = triangle->points[0][0];
    if (triangle->points[1][0] < aabb.min[0]) aabb.min[0] = triangle->points[1][0];
    if (triangle->points[2][0] < aabb.min[0]) aabb.min[0] = triangle->points[2][0];

    aabb.min[1] = triangle->points[0][1];
    if (triangle->points[1][1] < aabb.min[1]) aabb.min[1] = triangle->points[1][1];
    if (triangle->points[2][1] < aabb.min[1]) aabb.min[1] = triangle->points[2][1];

    aabb.min[2] = triangle->points[0][2];
    if (triangle->points[1][2] < aabb.min[2]) aabb.min[2] = triangle->points[1][2];
    if (triangle->points[2][2] < aabb.min[2]) aabb.min[2] = triangle->points[2][2];

    aabb.max[0] = triangle->points[0][0];
    if (triangle->points[1][0] > aabb.max[0]) aabb.max[0] = triangle->points[1][0];
    if (triangle->points[2][0] > aabb.max[0]) aabb.max[0] = triangle->points[2][0];

    aabb.max[1] = triangle->points[0][1];
    if (triangle->points[1][1] > aabb.max[1]) aabb.max[1] = triangle->points[1][1];
    if (triangle->points[2][1] > aabb.max[1]) aabb.max[1] = triangle->points[2][1];

    aabb.max[2] = triangle->points[0][2];
    if (triangle->points[1][2] > aabb.max[2]) aabb.max[2] = triangle->points[1][2];
    if (triangle->points[2][2] > aabb.max[2]) aabb.max[2] = triangle->points[2][2];

    return aabb;
}

static inline AABB3D
OBB3D_getAABB3D(OBB3D *obb)
{
    AABB3D aabb =
    {
        {FLT_MAX, FLT_MAX, FLT_MAX},
        {FLT_MIN, FLT_MIN, FLT_MIN}
    };

    vec3 dimensions[8] =
    {
        {-obb->dimensions[0], obb->dimensions[1], obb->dimensions[2]},
        {-obb->dimensions[0], obb->dimensions[1], -obb->dimensions[2]},
        {-obb->dimensions[0], -obb->dimensions[1], obb->dimensions[2]},
        {-obb->dimensions[0], -obb->dimensions[1], -obb->dimensions[2]},
        {obb->dimensions[0], obb->dimensions[1], obb->dimensions[2]},
        {obb->dimensions[0], obb->dimensions[1], -obb->dimensions[2]},
        {obb->dimensions[0], -obb->dimensions[1], obb->dimensions[2]},
        {obb->dimensions[0], -obb->dimensions[1], -obb->dimensions[2]}
    };

    for (int i = 0; i < 8; ++i)
    {
        vec3_mul_mat3x3(dimensions[i], dimensions[i], obb->rotation);

        if (dimensions[i][0] < aabb.min[0]) 
            aabb.min[0] = dimensions[i][0];
        if (dimensions[i][1] < aabb.min[1])
            aabb.min[1] = dimensions[i][1];
        if (dimensions[i][2] < aabb.min[2])
            aabb.min[2] = dimensions[i][2];

        if (dimensions[i][0] > aabb.max[0])
            aabb.max[0] = dimensions[i][0];
        if (dimensions[i][1] > aabb.max[1])
            aabb.max[1] = dimensions[i][1];
        if (dimensions[i][2] > aabb.max[2])
            aabb.max[2] = dimensions[i][2];
    }


    return aabb;
}

static inline void
OBB3D_setRotation(OBB3D *obb, float x, float y, float z)
{
    mat3x3_identity(obb->rotation);
    mat3x3_rotate(obb->rotation, obb->rotation, 1.0f, 0.0f, 0.0f, x);
    mat3x3_rotate(obb->rotation, obb->rotation, 0.0f, 1.0f, 0.0f, y);
    mat3x3_rotate(obb->rotation, obb->rotation, 0.0f, 0.0f, 1.0f, z);
}

static inline void
OBB3D_closestPointToPoint(OBB3D *obb, vec3 point, vec3 ret)
{
    vec3 tmp_a, tmp_b;
    vec3_sub(tmp_a, point, obb->center);

    vec3_copy(ret, obb->center);

    float distance;

    for (int i = 0; i < 3; ++i)
    {
        distance = vec3_mul_inner(tmp_a, obb->rotation[i]);

        if (distance > obb->dimensions[i])
            distance = obb->dimensions[i];

        if (distance < -obb->dimensions[i])
            distance = -obb->dimensions[i];

        vec3_scale(tmp_b, obb->rotation[i], distance);
        vec3_add(ret, ret, tmp_b);
    }
}

/* Calculate the squared distance between point and OBB */
static inline float
OBB3D_squaredDistancePoint(OBB3D *obb, const vec3 pt)
{
    vec3 v;
    vec3_sub(v, pt, obb->center);

    float sq_distance = 0.0f;
    float d, excess;

    for (int i = 0; i < 3; ++i)
    {
        d       = vec3_mul_inner(v, obb->rotation[i]);
        excess  = 0;

        if (d < -obb->dimensions[i])
            excess = d + obb->dimensions[i];
        else if (d > obb->dimensions[i])
            excess = d - obb->dimensions[i];

        sq_distance += (excess * excess);
    }

    return sq_distance;
}

static inline float
OBB3D_getRadius(OBB3D *obb)
{
    return vec3_len(obb->dimensions);
}

/* Returns point of collision in the local space of the obb */
static inline bool32
testSphere3DOBB3DAndGetData(Sphere3D *sphere, OBB3D *obb,
    vec3 ret_point, vec3 ret_normal, float *penetration)
{
    OBB3D_closestPointToPoint(obb, sphere->center, ret_point);

    vec3 v; vec3_sub(v, ret_point, sphere->center);
    float distance_squared = vec3_mul_inner(v, v);

    if (distance_squared <= POWER_OF_2(sphere->radius))
    {
        if (!vec3_is_null(v))
        {
            vec3_norm(v, v);
        }
        vec3_copy(ret_normal, v);
        *penetration = -((float)sqrtf(distance_squared) - sphere->radius);
        //vec3_copy(ret_normal, ret_point);
        //vec3_norm(ret_normal, ret_normal);
        //vec3_scale(ret_normal, ret_normal, -1.0f);

        ///* Distance */
        //vec3 sphere_rel_center;
        //vec3_sub(sphere_rel_center, sphere->center, obb->center);
        //vec3_sub(sphere_rel_center, sphere_rel_center, ret_point);
        //*penetration = sphere->radius - vec3_len(sphere_rel_center);

        return 1;
    }

    return 0;
}

static inline bool32
testSphere3DOBB3DCheap(Sphere3D *sp, OBB3D *obb)
{
    float sq_distance = OBB3D_squaredDistancePoint(obb, sp->center);
    return sq_distance <= (sp->radius * sp->radius);
}

static inline bool32
testAABB3DAABB3D(AABB3D *a, AABB3D *b)
{
    if (a->max[0] < b->min[0] || a->min[0] > a->max[0]) return 0;
    if (a->max[1] < b->min[1] || a->min[1] > a->max[1]) return 0;
    if (a->max[2] < b->min[2] || a->min[2] > a->max[2]) return 0;
    return 1;
}

static inline bool32
AABB3D_containsAABB3D(AABB3D *a, AABB3D *b)
{
    for (int i = 0; i < 3; ++i) if (b->min[i] < a->min[i]) return 0;
    for (int i = 0; i < 3; ++i) if (b->max[i] > a->max[i]) return 0;
    return 1;
}

static inline float
AABB3D_computeRadius(AABB3D *aabb)
{
    vec3 v;
    for (int i = 0; i < 3; ++i)
        v[i] = (aabb->max[i] - aabb->min[i]) * 0.5f;
    return vec3_len(v);
}

static inline bool32
testOBB3DPlane(OBB3D *obb, vec3 p_normal, vec3 vertex)
{
    float v;
    vec3 vmin, vmax;

    for (int q = 0; q < 3; ++q)
    {
        v = vertex[q];

        if (p_normal[q] > 0.0f)
        {
            vmin[q] = -obb->dimensions[q] - v;
            vmax[q] =  obb->dimensions[q] - v;
        }
        else
        {
            vmin[q] =  obb->dimensions[q] - v;
            vmax[q] = -obb->dimensions[q] - v;
        }
    }

    if (vec3_mul_inner(p_normal, vmin) > 0.0f)
        return 0;

    if (vec3_mul_inner(p_normal, vmax) >= 0.0f)
        return 1;

    return 0;
}

static inline bool32
testAlignedBoxPlane(vec3 center, vec3 dimensions, vec3 normal, float dot)
{
    float r = dimensions[0] * ABS(normal[0]) + \
        dimensions[1] * ABS(normal[1]) + \
        dimensions[2] * ABS(normal[2]);
    float s = vec3_mul_inner(normal, center) - dot;
    return ABS(s) <= r;
}
