Kyssari engine & Karistys Quake-killer
Binary downloads:   https://kuvis.itch.io/karistys

BUILDING

Supported compilers:
- gcc (GNU/Linux)
- cl (Microsoft Windows, comes with Visual Studio. Note that this must be set
  up correctly for command line use.)

Dependencies
SDL2, SDL2_ttf, SDL2_image and SDL2_mixer.

Using your shell, navigate into the project directory. Run "make" on a
GNU/Linux system, or "nmake" on a Windows system with Visual Studio command
line utilities set up. The generated directory will be called "release".

Credits:

Kuvis       - engine programming

Lommi       - gameplay and engine programming

Jämä      - gameplay and engine programming

Pekka       - weapon, power-up, 2D art and other art

Lasse P.    - character and UI art

Lasse H.    - map design and exterior area models/props

Keyo        - sound and music
