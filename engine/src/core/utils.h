#pragma once
#include "types.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include "memory.h"

#ifdef _WANGBLOWS
    #include "sys_win32.h"
#elif defined(_UNIX)
    #include "sys_unix.h"
#endif

extern float    F_COLOR_WHITE[4];
extern float    F_COLOR_BLACK[4];
extern float    F_COLOR_SYS_BLUE[4];

extern Color    BYTE_COLOR_WHITE;
extern vec3     DEFAULT_UP_VECTOR;

#define MIN(val, min)\
    (val < min ? min : val)

#define MAX(val, max)\
    (val > max ? max : val)

#define ABS(val)\
    (val < 0 ? -1 * val : val)

#define CEIL(val)\
    (val - (float)(int)val > 0.0f ? (float)((int)(val) + 1) : val)

#define CLAMP(val, min, max)\
    (MIN(MAX(val, max), min))

#define POWER_OF_2(val) ((val) * (val))

#define DEG_TO_RAD(degrees)\
    ((double)degrees * (double)(M_PI / 180.0f))

#define RAD_TO_DEG(rad)\
	((double)rad * (double)(180.0f / M_PI))

static inline float
RADIAN_CLAMPF(float val);

#define TEST_POINT_IN_RECT(point_x, point_y, x, y, w, h) \
    (  point_x >= x && point_x <= x + w \
    && point_y >= y && point_y <= y + h)

#define SET_BITFLAG_ON(val, val_type, flag) (val = val | (val_type)flag)
#define SET_BITFLAG_OFF(val, val_type, flag) (val = val & ~(val_type)flag);
#define GET_BITFLAG(val, val_type, flag) ((val & (val_type)flag) == (val_type)flag)

#define KILOBYTES(num_bytes) (num_bytes * 1000)
#define MEGABYTES(num_bytes) (num_bytes * 1000000)

#define SIZE_OF_VERTEX (8 * sizeof(GLfloat))

typedef struct Configuration   Configuration;

/* Note: the pointer returned by this must be freed using kfree() */
char *
readTextFileToBuffer(const char *path);

void
stripSymbolsFromString(char *str, const char *symbols, int num_symbols);

int
loadConfigFile(Configuration *configuration, const char *path);

struct Configuration
{
    FILE    file;
    char    key[128], value[32];
};

String
createString(const char *text);

void
freeString(String *string);

static inline uint32_t
hashFromString(const char *str);

static inline void
setColorf(float *array, float r, float g, float b, float a);

static inline void
copyColorf(float *dest, float *src);

void
purgeTrailingSpacesFromStr(char *str);

int
parseConfigFile(const char *path,
    void (*callback)(const char *opt, const char *val));

static inline void
eulerAnglesToDirectionVector(mat4x4 ret, float yaw, float pitch, float roll)
{
	mat4x4_identity(ret);

	float x1 = sinf(yaw);
	float y1 = sinf(pitch);
	float z1 = sinf(roll);

	float x2 = cosf(yaw);
	float y2 = cosf(pitch);
	float z2 = cosf(roll);

	ret[0][0] = y2 * z2;
	ret[0][1] = -y2 * z1;
	ret[0][2] = y1;
	ret[1][0] = z2 * x1 * y1 + x2 * z1;
	ret[1][1] = x2 *z2 - x1 *y1 * z1;
	ret[1][2] = -z2 * x1;
	ret[2][0] = -z2 * y1;
	ret[2][1] = y2 * x1 + x2 * y1 * z1;
	ret[2][2] = x2 * y2 - x1 * y1 * z1;
}

static inline float
RADIAN_CLAMPF(float val)
{
    float tmp;

    if (val > (float)M_PI * 2.0f)
        tmp = val - (float)M_PI * 2.0f;
    else if (val < 0.0f)
        tmp = (float)M_PI * 2.0f - val;
    else
        tmp = val;

    return tmp;
}

static inline bool32
testSphereSphereCollision(
    float radius1, vec3 origin1,
    float radius2, vec3 origin2)
{
    vec3 distance_vec;
    vec3_sub(distance_vec, origin1, origin2);
    float distance = vec3_len(distance_vec);
    return distance < radius1 + radius2;
}

static inline uint32_t
hashFromString(const char *str)
{
    #define FNV_32_SEED 0x811C9DC5;
    #define FNV_32_PRIME ((uint32_t)0x01000193)

    uint32_t hash = FNV_32_SEED;

    for (const char *c = str; *c; ++c)
    {
        hash ^= *c;
        hash *= FNV_32_PRIME;
    }

    return hash;
}

static inline void
setColorf(float *array, float r, float g, float b, float a)
{
    array[0] = r;
    array[1] = g;
    array[2] = b;
    array[3] = a;
}

static inline void
copyColorf(float *dest, float *src)
{
    dest[0] = src[0];
    dest[1] = src[1];
    dest[2] = src[2];
    dest[3] = src[3];
}

#define QUICK_SORT_DEFINITION(type, compare_func) \
void \
quickSort_##type(type *array, uint size) \
{ \
    if (size <= 1) return; \
\
    type tmp; \
    uint pivot  = size - 1; \
    uint wall   = 0; \
\
    for (uint i = wall; i < pivot; ++i) \
    { \
        if (compare_func(array[i], array[pivot])) \
        { \
            tmp         = array[i]; \
            array[i]    = array[wall]; \
            array[wall] = tmp; \
            ++wall; \
        } \
    } \
\
    tmp             = array[pivot]; \
    array[pivot]    = array[wall]; \
    array[wall]     = tmp; \
\
    if (size > 2) \
    { \
        quickSort_##type(array, wall); \
        quickSort_##type(&array[wall], size - wall); \
    } \
}

/* Sequence buffer based on Glenn Fieldler's example, mainly used on the
 * networking side */
#define FREE_SEQUENCE_SLOT 0xFFFFFFFF

/* True if sa is greater than sb */
#define IS_SEQUENCE_GREATER(sa, sb) \
    ((((sa) > (sb)) && ((sa) - (sb) <= 32768)) || \
    (( (sa) < (sb)) && ((sb) - (sa) >  32768 )))

#define SEQUENCE_BUFFER_DECLARATION(type) \
typedef struct SequenceBuffer_##type SequenceBuffer_##type; \
struct SequenceBuffer_##type \
{ \
    type                *data; \
    uint32_t            *lookup_buffer; \
    int                 size; \
    uint16_t            sequence; \
    DynamicMemoryBlock  *allocator; \
}; \
 \
static inline int \
SequenceBuffer_##type##_init(SequenceBuffer_##type *buffer, \
    DynamicMemoryBlock *allocator, int size) \
{ \
    if (!buffer)    return 1; \
    if (!allocator) return 2; \
    if (size <= 0)  return 3; \
    buffer->allocator = allocator; \
 \
    buffer->data = DynamicMemoryBlock_malloc(allocator, \
        (uint)size * sizeof(type)); \
    if (!buffer->data) return 4; \
 \
    buffer->lookup_buffer = DynamicMemoryBlock_malloc(allocator, \
        (uint)size * sizeof(int32_t)); \
    if (!buffer->lookup_buffer) \
    { \
        DynamicMemoryBlock_free(allocator, buffer->data); \
        return 5; \
    } \
 \
    buffer->size = size; \
    buffer->sequence = 0; \
    memset(buffer->lookup_buffer, 255, (uint)size * sizeof(int32_t)); \
    return 0; \
} \
static inline void \
SequenceBuffer_##type##_clear(SequenceBuffer_##type *buffer) \
{ \
    if (!buffer) return; \
    ASSERT(buffer->lookup_buffer); \
    memset(buffer->lookup_buffer, 255, (uint)buffer->size * sizeof(int32_t)); \
    buffer->sequence = 0; \
} \
 \
static inline type * \
SequenceBuffer_##type##_getBySequence(SequenceBuffer_##type *buffer, \
    uint16_t sequence) \
{ \
    int index = (int)sequence % buffer->size; \
    return buffer->lookup_buffer[index] == (uint32_t)sequence ? \
        &buffer->data[index] : 0; \
} \
 \
static inline type * \
SequenceBuffer_##type##_insert(SequenceBuffer_##type *buffer, uint16_t sequence) \
{ \
    /* Note: there's a situation here regarding high packet loss that needs  \
     * to be dealt with, but is not dealt with currently */ \
    int index = (int)sequence % buffer->size; \
    buffer->lookup_buffer[index] = (uint32_t)sequence; \
    return &buffer->data[index]; \
} \
 \
static inline void \
SequenceBuffer_##type##_eraseBySequence(SequenceBuffer_##type *buffer, \
    uint16_t sequence) \
{ \
    int index = (int)sequence % buffer->size; \
    buffer->lookup_buffer[index] = FREE_SEQUENCE_SLOT; \
} \
static inline bool32 \
SequenceBuffer_##type##_exists(SequenceBuffer_##type *buffer, uint16_t sequence) \
{ \
    return buffer->lookup_buffer[(int)sequence % buffer->size] == (uint32_t)sequence; \
} \
 \
static inline int \
SequenceBuffer_##type##_getIndex(SequenceBuffer_##type *buffer, int16_t sequence) \
{ \
    return buffer->size % (int)sequence; \
} \
 \
static inline type * \
SequenceBuffer_##type##_atIndex(SequenceBuffer_##type *buffer, int index) \
{ \
    if (buffer->lookup_buffer[index] != FREE_SEQUENCE_SLOT) \
        return &buffer->data[index]; \
    return 0; \
}

/* A basic "first in, first out" -type queue structure */
#define FIFO_QUEUE_DECLARATION(type) \
\
typedef struct FIFOQueue_##type \
{ \
    int                 first; \
    int                 num; \
    int                 max; \
    type                *items; \
    DynamicMemoryBlock  *allocator; \
} FIFOQueue_##type; \
 \
static inline int \
FIFOQueue_##type##_init(FIFOQueue_##type *queue, \
    DynamicMemoryBlock *allocator, int size) \
{ \
    ASSERT(!queue->allocator); \
    if (!queue)         return 1; \
    if (!allocator)     return 2; \
    queue->items = DynamicMemoryBlock_malloc(allocator, \
        (uint)size * sizeof(type)); \
    if (!queue->items)  return 3; \
    queue->num      = 0; \
    queue->first    = 0; \
    queue->max      = size; \
    return 0; \
} \
 \
static inline void \
FIFOQueue_##type##_clear(FIFOQueue_##type *queue) \
{ \
    if (!queue) return; \
    if (!queue->allocator) return; \
    if (queue->items) \
        memset(queue->items, 0, (uint)queue->max * sizeof(type)); \
    queue->num      = 0; \
    queue->first    = 0; \
} \
 \
static inline void \
FIFOQueue_##type##_push(FIFOQueue_##type *queue, type *val) \
{ \
    ASSERT(queue->num < queue->max); \
    int index = (queue->first + queue->num) % queue->max; \
    queue->items[index] = *val; \
    ++queue->num; \
} \
 \
static inline type \
FIFOQueue_##type##_pop(FIFOQueue_##type *queue) \
{ \
    ASSERT(queue->num > 0); \
    type *ret = &queue->items[queue->first]; \
    queue->first = (queue->first + 1) % queue->max; \
    --queue->num; \
 \
    return *ret; \
} \
 \
static inline bool32 \
FIFOQueue_##type##_isEmpty(FIFOQueue_##type *queue) \
{ \
    return queue->num == 0; \
} \
 \
static inline bool32 \
FIFOQueue_##type##_isFull(FIFOQueue_##type *queue) \
{ \
    return queue->num == queue->max; \
}

/* Array list - a dynamic container whose elements' memory addresses do not
 * change upon resize. */
#define ARRAY_LIST_DECLARATION(type)\
    typedef struct ArrayListSubArray_##type ArrayListSubArray_##type;\
    typedef struct ArrayList_##type ArrayList_##type;\
    struct ArrayListSubArray_##type\
    {\
        type *items;\
        ArrayListSubArray_##type *next;\
    };\
    \
    struct ArrayList_##type\
    {\
        size_t                      sub_array_size;\
        size_t                      capacity;\
        size_t                      num_items;\
        DynamicMemoryBlock          *block;\
        ArrayListSubArray_##type    *head;\
    };\
    \
    static inline void\
    ArrayList_##type##_init(ArrayList_##type *list,\
        size_t sub_array_size, size_t num_arrays,\
        DynamicMemoryBlock *block)\
    {\
        ASSERT(sub_array_size > 0);\
        ASSERT(num_arrays > 0);\
        \
        list->capacity          = num_arrays * sub_array_size;\
        list->num_items         = 0;\
        list->block             = block;\
        list->sub_array_size    = sub_array_size;\
        \
        list->head = (ArrayListSubArray_##type *)DynamicMemoryBlock_malloc(block,\
            sizeof(ArrayListSubArray_##type) + sub_array_size * sizeof(type));\
        list->head->items = (type *)((char*)(list->head) + sizeof(ArrayListSubArray_##type));\
        size_t num_arrays_added = 1;\
        ArrayListSubArray_##type *prev = list->head;\
        while (num_arrays_added < num_arrays)\
        {\
            prev->next = (ArrayListSubArray_##type *)DynamicMemoryBlock_malloc(block,\
                sizeof(ArrayListSubArray_##type) + sub_array_size * sizeof(type));\
			prev->next->items = (type *)((char*)prev->next + sizeof(ArrayListSubArray_##type));\
            prev = prev->next;\
            ++num_arrays_added;\
        }\
        prev->next = 0;\
    }\
    static inline type *\
    ArrayList_##type##_push(ArrayList_##type *list, type item)\
    {\
        type *ret;\
        if (list->num_items < list->sub_array_size)\
        {\
            ret = &list->head->items[list->num_items];\
        }\
        else if (list->capacity - list->num_items > 0) \
        { \
            uint target_arr = (list->num_items) / list->sub_array_size; \
            ArrayListSubArray_##type *sa; \
            uint i = 0; \
            for (sa = list->head; \
                 sa->next && i < target_arr; \
                 sa = sa->next){++i;} \
            ret = &sa->items[list->num_items % list->sub_array_size]; \
        } \
        else \
        {\
            ArrayListSubArray_##type *sa; \
            \
            for (sa = list->head; sa->next; sa = sa->next){}; \
            \
            sa->next = DynamicMemoryBlock_malloc(list->block, \
                sizeof(ArrayListSubArray_##type) + \
                list->sub_array_size * sizeof(type)); \
            if (!sa->next) return 0; \
            \
            sa->next->next = 0; \
            sa->next->items = (type*)(sa->next + 1); \
            ret = &sa->next->items[0]; \
        }\
        \
        list->num_items++;\
        *ret = item;\
        return ret;\
    }\
    \
    static inline type *\
    ArrayList_##type##_pushEmpty(ArrayList_##type *list)\
    {\
        type item = {0}; \
        return ArrayList_##type##_push(list, item); \
    }\
    \
    static inline type *\
    ArrayList_##type##_at(ArrayList_##type *list, size_t index)\
    {\
        type *ret;\
        if (index < list->sub_array_size)\
            ret = &list->head->items[index];\
        else\
        {\
            size_t array_num = index / list->sub_array_size;\
            ArrayListSubArray_##type *sub_array = list->head;\
            while (array_num > 0)\
            {\
                sub_array = sub_array->next;\
                --array_num;\
            }\
            ret = &sub_array->items[index % list->sub_array_size];\
        }\
        return ret;\
    }\
	\
	static inline void\
	ArrayList_##type##_resize(ArrayList_##type *list, size_t new_size)\
	{\
		list->num_items = new_size;\
		\
		if (list->capacity < new_size)\
		{\
			size_t num_new_items = new_size - list->capacity;\
			size_t num_new_arrays =  num_new_items / list->sub_array_size;\
			if (num_new_items % list->sub_array_size > 0)\
				++num_new_arrays;\
			\
			ArrayListSubArray_##type *tail = list->head;\
			while (tail->next) tail = tail->next;\
			\
			for (size_t i = 0; i < num_new_arrays; ++i)\
			{\
				tail->next = (ArrayListSubArray_##type*)DynamicMemoryBlock_malloc(list->block,\
					sizeof(ArrayListSubArray_##type) + list->sub_array_size * sizeof(type));\
				tail->next->items = (type *)((char*)tail->next + sizeof(ArrayListSubArray_##type));\
				tail = tail->next;\
			}\
			\
			tail->next = 0;\
			list->capacity 	= list->capacity + num_new_arrays * list->sub_array_size;\
		}\
	}


/* Vectors
 *
 * Declaring vectors:
 * First, call the macro VECTOR_DECLARATION(type).
 * For example: VECTOR_DECLARATION(int)
 * The type must not have spaces, and if it's a pointer,
 * a new typename must be defined for it. If you need a
 * vector of, say, int pointers, first typedef "int *" as
 * "IntPtr" or something similar, then pass that to
 * VECTOR_DECLARATION().
 * In the source (.c) file, call VECTOR_DEFINITION(type).
 * Make sure the source file inludes engine.h
 *
 * Usage:
 * Vector_TYPE_init(vec, size): size is the initial capacity of the vector:
 * it will always contain 0 items at the beginning. Always call before use,
 * or zero-initialize the vector otherwise.
 *
 * Vector_TYPE_free
 * Destroy the vector, freeing up the memory.
 *
 * Vector_TYPE_resize
 * Resize the vector. If the size is less than the vector's capacity, memory
 * will not be freed. If it's greater, more will be allocated.
 *
 * Vector_TYPE_push
 * Push an item to the back of the vector, increasing size if necessary
 *
 * Vector_TYPE_insert(vec, item, index)
 * Insert item at the given index, moving forward any items from this index
 * forward.
 *
 * Vector_TYPE_erase(vec, index)
 * Erase an item, reducing size (but not freeing memory)
 *
 * Vector_TYPE_at(vec, index)
 * Get a pointer to an item at index */

#define VECTOR_DECLARATION(type)\
    typedef struct Vector_##type Vector_##type;\
    \
    struct Vector_##type\
    {\
        type                *items;\
        size_t              num_items;\
        size_t              capacity;\
        DynamicMemoryBlock  *block;\
    };\
    \
    void\
    Vector_##type##_init(Vector_##type *vec, size_t size, DynamicMemoryBlock *block);\
    \
    void\
    Vector_##type##_free(Vector_##type *vec);\
    \
    void\
    Vector_##type##_resize(Vector_##type *vec, size_t new_size);\
    \
    static inline type *\
    Vector_##type##_push(Vector_##type *vec, type item)\
    {\
        if (vec->num_items >= vec->capacity)\
        {\
            size_t new_size = (size_t)((float)vec->capacity * 1.05f);\
            if (new_size <= vec->num_items) ++new_size;\
            Vector_##type##_resize(vec, new_size);\
        }\
        vec->items[vec->num_items++] = item;\
        return &vec->items[vec->num_items - 1];\
    }\
    \
    static inline type *\
    Vector_##type##_pushEmpty(Vector_##type *vec)\
    {\
        if (vec->num_items >= vec->capacity)\
        {\
            size_t new_size = (size_t)((float)vec->capacity * 1.05f);\
            if (new_size <= vec->num_items) ++new_size;\
            Vector_##type##_resize(vec, new_size);\
        }\
        return &vec->items[vec->num_items++];\
    }\
    \
    static inline void\
    Vector_##type##_insert(Vector_##type *vec, type item, size_t index)\
    {\
        if (index >= vec->num_items)\
        {\
            Vector_##type##_push(vec, item);\
            return;\
        }\
        \
        size_t num_to_move = vec->num_items - index;\
        \
        if (vec->num_items >= vec->capacity)\
        {\
            size_t new_size = (size_t)((float)vec->capacity * 1.05f);\
            if (new_size <= vec->num_items) ++new_size;\
            Vector_##type##_resize(vec, new_size);\
        }\
        \
        if (num_to_move > 0)\
        {\
            memmove(&vec->items[index + 1], &vec->items[index],\
                num_to_move * sizeof(type));\
        }\
        vec->items[index] = item;\
        ++vec->num_items;\
    }\
    \
    static inline void\
    Vector_##type##_erase(Vector_##type *vec, size_t index)\
    {\
        if (vec->num_items == 0 || index >= vec->num_items) return;\
        size_t start_index = index + 1;\
        size_t num_to_move = vec->num_items - start_index;\
        memmove(&vec->items[index], &vec->items[start_index], num_to_move * sizeof(type));\
        --vec->num_items;\
    }\
    \
    static inline type *\
    Vector_##type##_at(Vector_##type *vec, size_t index)\
    {\
        ASSERT(index < vec->num_items);\
        return &vec->items[index];\
    }


#define VECTOR_DEFINITION(type)\
    void\
    Vector_##type##_init(Vector_##type *vec, size_t size, DynamicMemoryBlock *block)\
    {\
        if (size > 0)\
        {\
            vec->items = DynamicMemoryBlock_malloc(block, size * sizeof(type));\
            ASSERT(vec->items);\
        }\
        else\
            vec->items = 0;\
        \
        vec->num_items  = 0;\
        vec->capacity   = size;\
        vec->block      = block;\
    }\
    \
    void\
    Vector_##type##_free(Vector_##type *vec)\
    {\
        if (vec->items)\
            DynamicMemoryBlock_free(vec->block, (void*)vec->items);\
        vec->items      = 0;\
        vec->num_items  = 0;\
        vec->capacity   = 0;\
    }\
    \
    void\
    Vector_##type##_resize(Vector_##type *vec, size_t size)\
    {\
        if (size < vec->num_items)\
        {\
            vec->num_items = size;\
        }\
        else if (size > vec->num_items)\
        {\
            if (size > vec->capacity)\
            {\
                type *new_items = (type *)DynamicMemoryBlock_malloc(vec->block,\
                    size * sizeof(type));\
                ASSERT(new_items);\
                \
                if (vec->capacity > 0)\
                {\
                    memcpy((void*)new_items, (void*)vec->items,\
                        vec->num_items * sizeof(type));\
                    DynamicMemoryBlock_free(vec->block, (void*)vec->items);\
                }\
                \
                vec->items = new_items;\
                vec->capacity = size;\
            }\
        }\
    }

/* A quick, dirty and shitty hashtable implementation that will fire an
 * assertion on hash collision. */

/* You can use this to calculate the byte size of the array required for
 * your hashtable */
#define CALC_HASH_TABLE_MEMORY_SIZE(type, capacity) \
    ((size_t)(capacity * (sizeof(type) + sizeof(char))))

/* HashTable_Type usage:
 *
 * HashTable_Type_init()
 * Pass in the table, a void pointer to the memory the table can use for
 * storage as well as the maximum number of items of TYPE the table can hold.
 * You can use CALC_HASH_TABLE_MEMORY_SIZE to calculate the amount of memory
 * you should allocate for the array.
 * If a collision happens, an assertion will fire - at this point, you should
 * either increase the table's size or change the hash of the element causing
 * the collision.
 *
 * HashTable_Type_insert()
 * Pass in the table, string identifier and item to insert the item into the
 * table. If the slot is already reserved, an assertion will fire. Returns a
 * pointer to the inserted item.
 *
 * HashTable_Type_get()
 * Pass in the table and a string identifier to receive a pointer to an item
 * stored. Returns 0 if no item with this identifier has been stored yet.
 *
 * HashTable_Type_clear()
 * Marks every item of the array as "not reserved."
 */

#define HASH_TABLE_DECLARATION(type) \
typedef struct HashTable_##type \
{ \
    type        *items; \
    char        *flags; \
    uint32_t    capacity; \
} HashTable_##type; \
\
static inline void \
HashTable_##type##_init(HashTable_##type *table, \
    void *memory, uint32_t capacity) \
{ \
    table->items    = memory; \
    table->flags    = (char*)((type*)memory + capacity); \
    table->capacity = capacity; \
    memset(table->flags, 0, capacity * sizeof(char)); \
} \
\
static inline type * \
HashTable_##type##_insert(HashTable_##type *table, const char *str, type item) \
{ \
    uint32_t index = hashFromString(str) % table->capacity; \
    char *flag = &table->flags[index]; \
    if (*flag) \
    { \
        DEBUG_PRINTF("Hashtable collision: %s!\n", str); \
        ASSERT(0); \
    } \
    *flag = 1; \
    type *slot = &table->items[index]; \
    *slot = item; \
    return slot; \
} \
\
static inline type * \
HashTable_##type##_insertEmpty(HashTable_##type *table, const char *str) \
{ \
    uint32_t index = hashFromString(str) % table->capacity; \
    char *flag = &table->flags[index]; \
    if (*flag) \
    { \
        DEBUG_PRINTF("Hashtable collision: %s!\n", str); \
        ASSERT(0); \
    } \
    *flag = 1; \
    type *slot = &table->items[index]; \
    return slot; \
} \
\
static inline type * \
HashTable_##type##_get(HashTable_##type *table, const char *str) \
{ \
    uint32_t index = hashFromString(str) % table->capacity; \
    if (table->flags[index]) \
        return &table->items[index]; \
    else \
        return 0; \
} \
\
static inline void \
HashTable_##type##_clear(HashTable_##type *table) \
{ \
    memset(table->flags, 0, table->capacity * sizeof(char)); \
}
