#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "assets.h"
#include "engine.h"
#include "../render/renderer.h"

#define MAX_MESHES_PER_MODEL 512

VECTOR_DEFINITION(BaseMesh);

/* Data for basic shapes */
GLfloat _cube_base_vertices[288];
GLfloat _triangle_base_vertices[24];
GLfloat _plane_base_vertices[48];
#define NUM_BASE_SHAPE_INDICES 288
GLuint  _base_shape_indices[NUM_BASE_SHAPE_INDICES];

enum MeshAllocType
{
    MESH_ALLOC_DYNAMIC,
    MESH_ALLOC_SIMPLE
};

struct MeshLoadData
{
    struct aiMesh   *mesh;
    void            *verts_or_indices;
};

static void
processNodeGlobal(BaseMesh *meshes, uint *num_meshes, const struct aiScene *scene, struct aiNode *node);

static void
processNodeAssetPool(AssetPool *assetpool, BaseMesh *meshes, uint *num_meshes, const struct aiScene *scene, struct aiNode *node);

static void
loadAssimpMeshUVs(void *args)
{
    struct MeshLoadData *data = (struct MeshLoadData*)args;
    GLfloat *vertices = (GLfloat*)data->verts_or_indices;

    for (uint i = 0; i < data->mesh->mNumVertices; ++i)
    {
        vertices[i * 8 + 3] = data->mesh->mTextureCoords[0][i].x;
        vertices[i * 8 + 4] = data->mesh->mTextureCoords[0][i].y;
    }
}

static void
loadAssimpMeshNormals(void *args)
{
    struct MeshLoadData *data = (struct MeshLoadData*)args;
    GLfloat *vertices = (GLfloat*)data->verts_or_indices;

    for (uint i = 0; i < data->mesh->mNumVertices; ++i)
    {
        vertices[i * 8 + 5] = data->mesh->mNormals[i].x;
        vertices[i * 8 + 6] = data->mesh->mNormals[i].y;
        vertices[i * 8 + 7] = data->mesh->mNormals[i].z;
    }
}

static int
loadSingleBaseMeshInternal(const char *path,
    GLfloat **rvertices, uint *rnum_vertices,
    GLuint  **rindices,  uint *rnum_indices,
    void *allocator, int allocator_type)
{
    ASSERT(path);

    const struct aiScene *scene = aiImportFile(path,
        aiProcess_CalcTangentSpace |
        aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices |
        aiProcess_GenNormals |
        aiProcess_SortByPType);

    if (!scene)
    {
        DEBUG_PRINTF("Failed to load Assimp scene from path %s.\n", path);
        DEBUG_PRINTF(aiGetErrorString());
        return 1;
    }

    if (!scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE)
    {
        DEBUG_PRINTF("Failed to load Assimp scene: incomplete! %s.\n", path);
        aiReleaseImport(scene);
        return 2;
    }

    if (!scene->mRootNode)
    {
        DEBUG_PRINTF("Failed to load Assimp scene: no root node! %s.\n", path);
        aiReleaseImport(scene);
        return 3;
    }

    if (scene->mRootNode->mNumChildren <= 0)
    {
        DEBUG_PRINTF("Failed to load Assimp scene: no meshes in root! %s.\n", path);
        aiReleaseImport(scene);
        return 4;
    }

    struct aiNode *node = scene->mRootNode->mChildren[0];
    struct aiMesh *mesh = scene->mMeshes[node->mMeshes[0]];

    GLfloat *vertices;

    if (allocator_type == MESH_ALLOC_DYNAMIC)
    {
        vertices = kmalloc(mesh->mNumVertices * SIZE_OF_VERTEX);

        if (!vertices)
        {
            DEBUG_PRINTF("Failed to load mesh: out of memory!\n");
            return 5;
        }
    }
    else if (allocator_type == MESH_ALLOC_SIMPLE)
    {
        vertices = SimpleMemoryBlock_malloc((SimpleMemoryBlock*)allocator,
            mesh->mNumVertices * SIZE_OF_VERTEX);

        if (!vertices)
        {
            DEBUG_PRINTF("Failed to load mesh: SimpleMemoryBlock out of memory!\n");
            return 6;
        }
    }
    else
    {
        ASSERT(0);
    }

    if (!vertices)
    {
        DEBUG_PRINTF("Failed to allocate model vertices for %s!\n", path);
        aiReleaseImport(scene);
        return 7;
    }

#if 0
    struct MeshLoadData uv_data = {mesh, vertices};

    if (mesh->mNumUVComponents[0])
        createThreadedJob(loadAssimpMeshUVs, &uv_data);

    if (mesh->mNormals)
        createThreadedJob(loadAssimpMeshNormals, &uv_data);
#endif

    for (uint i = 0; i < mesh->mNumVertices; ++i)
    {
        /* Position */
        vertices[i * 8 + 0] = mesh->mVertices[i].x;
        vertices[i * 8 + 1] = mesh->mVertices[i].y;
        vertices[i * 8 + 2] = mesh->mVertices[i].z;
    }

    /* Count indices to allocate enough memory */
    uint num_indices = 0;

    for (struct aiFace *face = mesh->mFaces;
         face < &mesh->mFaces[mesh->mNumFaces];
         ++face)
    {
        num_indices += face->mNumIndices;
    }

    GLuint *indices;

    if (allocator_type == MESH_ALLOC_DYNAMIC)
    {
        indices = (GLuint*)kmalloc(num_indices * sizeof(GLuint));
    }
    else if (allocator_type == MESH_ALLOC_SIMPLE)
    {
        indices = (GLuint*)SimpleMemoryBlock_malloc(
            (SimpleMemoryBlock*)allocator, num_indices * sizeof(GLuint));
    }

    if (!indices)
    {
        DEBUG_PRINTF("Failed to allocate indices for base mesh!\n");
        aiReleaseImport(scene);
        return 8;
    }

    uint cnt = 0;

    /* Read indices */
    for (uint i = 0; i < mesh->mNumFaces; ++i)
        for (uint j = 0; j < mesh->mFaces[i].mNumIndices; ++j)
            indices[cnt++] = (GLuint)mesh->mFaces[i].mIndices[j];

    *rvertices      = vertices;
    *rindices       = indices;
    *rnum_vertices  = mesh->mNumVertices;
    *rnum_indices   = num_indices;

    /*while (ThreadPool_haveUnfinishedWork(&engine.thread_pool));*/

    aiReleaseImport(scene);

    return 0;
}

#define ASSET_POOL_ALLOC_ERROR_STR(asset_type) \
    ("AssetPool_init(): failed to allocate enough memory for" #asset_type \
    "table.\n")

int
AssetPool_init(AssetPool *assetpool, uint32_t num_spritefonts,
    uint32_t num_basemeshes,    uint32_t num_textures,
    uint32_t num_materials,     uint32_t num_material_arrays,
    uint32_t num_soundeffects,  uint32_t num_musics,
    uint32_t num_models,
    size_t additional_memory)
{
    size_t total_size = additional_memory + \
        sizeof(SpriteFont)      * num_spritefonts +
        sizeof(BaseMesh)        * num_basemeshes + \
        sizeof(Texture)         * num_textures +
        sizeof(SoundEffect)     * num_soundeffects + \
        sizeof(Music)           * num_musics +
        sizeof(Material)        * num_materials + \
        sizeof(MaterialArray)   * num_material_arrays + \
        sizeof(Model)           * num_models;

    void *mem = allocatePermanentMemory(total_size);

    if (!mem)
    {
        DEBUG_PRINTF("AssetPool_init() failed: not enough memory!\n");
        return 1;
    }

    SimpleMemoryBlock_init(&assetpool->memory, mem, total_size);

    //spritefont
    void *spritefont_malloc = SimpleMemoryBlock_malloc(&assetpool->memory,
        CALC_HASH_TABLE_MEMORY_SIZE(SpriteFont, num_spritefonts));
    if (spritefont_malloc == 0)
    {
        DEBUG_PRINTF("spritefont_malloc error\n");
        return 1;
    }
    HashTable_SpriteFont_init(&assetpool->spritefonts, spritefont_malloc, num_spritefonts);

    //basemesh
    void *basemesh_malloc = SimpleMemoryBlock_malloc(&assetpool->memory,
        CALC_HASH_TABLE_MEMORY_SIZE(BaseMesh, num_basemeshes));
    if (spritefont_malloc == 0)
    {
        DEBUG_PRINTF("BaseMesh_malloc error\n");
        return 2;
    }
    HashTable_BaseMesh_init(&assetpool->basemeshes, basemesh_malloc, num_basemeshes);

    //texture
    void *textures_malloc = SimpleMemoryBlock_malloc(&assetpool->memory,
        CALC_HASH_TABLE_MEMORY_SIZE(Texture, num_textures));
    if (textures_malloc == 0)
    {
        DEBUG_PRINTF("Textures_malloc error\n");
        return 3;
    }
    HashTable_Texture_init(&assetpool->textures, textures_malloc, num_textures);

    //material
    void *materials_malloc = SimpleMemoryBlock_malloc(&assetpool->memory,
        CALC_HASH_TABLE_MEMORY_SIZE(Material, num_materials));
    if (materials_malloc == 0)
    {
        DEBUG_PRINTF("Materials_malloc error\n");
        return 4;
    }
    HashTable_Material_init(&assetpool->materials, materials_malloc, num_materials);

    /* Material arrays */
    {
        void *p = SimpleMemoryBlock_malloc(&assetpool->memory,
            CALC_HASH_TABLE_MEMORY_SIZE(MaterialArray, num_material_arrays));

        if (!p)
        {
            DEBUG_PRINTF(ASSET_POOL_ALLOC_ERROR_STR(MaterialArray));
            return 5;
        }

        HashTable_MaterialArray_init(&assetpool->material_arrays,
            p, num_material_arrays);
    }


    //soundeffect
    void *soundeffects_malloc = SimpleMemoryBlock_malloc(&assetpool->memory,
        CALC_HASH_TABLE_MEMORY_SIZE(SoundEffect, num_soundeffects));
    if (soundeffects_malloc == 0)
    {
        DEBUG_PRINTF("Soundeffects_malloc error\n");
        return 4;
    }
    HashTable_SoundEffect_init(&assetpool->soundeffects, soundeffects_malloc, num_soundeffects);

    //music
    void *musics_malloc = SimpleMemoryBlock_malloc(&assetpool->memory,
        CALC_HASH_TABLE_MEMORY_SIZE(Music, num_musics));
    if (musics_malloc == 0)
    {
        DEBUG_PRINTF("Musics_malloc error\n");
        return 4;
    }
    HashTable_Music_init(&assetpool->musics, musics_malloc, num_musics);

    //model
    void *models_malloc = SimpleMemoryBlock_malloc(&assetpool->memory,
        CALC_HASH_TABLE_MEMORY_SIZE(Model, num_models));
    if (models_malloc == 0)
    {
        DEBUG_PRINTF("Models_malloc error\n");
        return 5;
    }
    HashTable_Model_init(&assetpool->models, models_malloc, num_models);

    return 0;
}

SpriteFont *
AssetPool_loadSpriteFont(AssetPool *assetpool, const char *name, const char *path, uint size)
{
    SpriteFont tmp;
    if (SpriteFont_init(&tmp, path, size, &BYTE_COLOR_WHITE) != 0)
    {
        DEBUG_PRINTF("AssetPool_loadSpriteFont error\n");
        return 0;
    }

    return HashTable_SpriteFont_insert(&assetpool->spritefonts, name, tmp);
}

BaseMesh *
AssetPool_loadBaseMesh(AssetPool *assetpool, const char *name, const char *path, Assets *assets)
{
    uint num_vertices;
    uint num_indices;
    GLfloat *vertices;
    GLuint *indices;

    if (loadSingleBaseMeshInternal(path,
        &vertices, &num_vertices,
        &indices,  &num_indices,
        &assetpool->memory, MESH_ALLOC_SIMPLE) != 0)
    {
        DEBUG_PRINTF("Failed to load base mesh %s from path %s\n", name, path);
        return 0;
    }

    BaseMesh *m = HashTable_BaseMesh_insertEmpty(
        &assetpool->basemeshes, name);
    BaseMesh_initFromExistingAlloc(m,
        vertices, num_vertices,
        indices, num_indices);

    Renderer_queueuBufferableBaseMesh(&engine.renderer, m);

    return m;
}

Model *
AssetPool_loadModel(AssetPool *assetpool, const char *name, const char *path)
{
    const struct aiScene *scene = aiImportFile(path,
        aiProcess_CalcTangentSpace |
        aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices |
        aiProcess_GenNormals |
        aiProcess_FlipUVs |
        aiProcess_SortByPType);

    /* TODO: the rest :) */
    if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)

    {
        DEBUG_PRINTF("Failed to load Assimp scene: incomplete! %s. \n", path);
        aiReleaseImport(scene);
        return 0;
    }

    uint num_meshes = 0;
    BaseMesh meshes[MAX_MESHES_PER_MODEL];

    struct aiNode *node = scene->mRootNode;
    processNodeAssetPool(assetpool, meshes, &num_meshes, scene, node);

    BaseMesh *allocated_meshes = SimpleMemoryBlock_malloc(
        &assetpool->memory, num_meshes * sizeof(BaseMesh));

    if (!allocated_meshes)
    {
        DEBUG_PRINTF("AssetPool_loadModel(): Failed to allocate meshes!\n");
        aiReleaseImport(scene);
        return 0;
    }

    memcpy(allocated_meshes, meshes, num_meshes * sizeof(BaseMesh));

    Model *model = HashTable_Model_insertEmpty(&assetpool->models, name);
    Model_init(model, allocated_meshes, num_meshes);

    if (num_meshes == 0)
        return 0;

    for (uint i = 0; i < model->num_meshes; ++i)
        Renderer_queueuBufferableBaseMesh(&engine.renderer, &model->meshes[i]);

    aiReleaseImport(scene);

    return model;
}

Texture *
AssetPool_loadTexture(AssetPool *assetpool, const char *name, const char *path)
{
    Texture tmp;

    SDL_Surface *bitmap = IMG_Load(path);

    if (!bitmap)
    {
        DEBUG_PRINTF("IMG_Load(): %s\n", IMG_GetError());
        return 0;
    }

    GLenum pixel_format = bitmap->format->BytesPerPixel == 4 ? GL_RGBA : GL_RGB;

    Texture_init(&tmp,
        GL_TEXTURE_2D, /* GL_TEXTURE_2D, GL_TEXTURE_CUBE_MAP, etc. */
        bitmap->pixels,
        pixel_format, /* for example, GL_RGBA */
        GL_UNSIGNED_BYTE, /* For example, GL_UNSIGNED_BYTE */
        GL_REPEAT, GL_REPEAT,
        bitmap->w, bitmap->h);

    SDL_FreeSurface(bitmap);

    return HashTable_Texture_insert(&assetpool->textures, name, tmp);
}

Material *
AssetPool_loadMaterial(AssetPool *assetpool, const char *name,
    Texture *diffuse_textures, Texture *specular,
    float shine, float r, float g, float b)
{
    Material tmp;
    if (Material_init(&tmp, diffuse_textures, specular, shine, r, g, b) != 0)
        return 0;
    return HashTable_Material_insert(&assetpool->materials, name, tmp);
}

MaterialArray *
AssetPool_loadMaterialArray(AssetPool *assetpool, const char *name,
    Material **materials, uint num_materials)
{
    if (num_materials < 1) return 0;

    MaterialArray ma;

    ma.materials = SimpleMemoryBlock_malloc(&assetpool->memory,
        num_materials * sizeof(Material*));

    if (!ma.materials)
    {
        DEBUG_PRINTF("AssetPool_loadMaterialArray(): out of memory! Asset "
            "name: %s.\n", name);
        return 0;
    }

    ma.num_materials = num_materials;

    for (uint i = 0; i < num_materials; ++i)
        ma.materials[i] = materials[i];

    return HashTable_MaterialArray_insert(&assetpool->material_arrays, name, ma);
}

SoundEffect *
AssetPool_loadSoundEffect(AssetPool *assetpool, const char *name,
    const char *path)
{
    if (!engine.audio_system.isInitialized) return 0;
    SoundEffect tmp;

    tmp.chunk = Mix_LoadWAV(path);

    if (!tmp.chunk)
    {
        DEBUG_PRINTF("AssetPool_loadSoundEffect() failed for %s.\n", path);
    }
    return HashTable_SoundEffect_insert(&assetpool->soundeffects, name, tmp);
}

Music *
AssetPool_loadMusic(AssetPool *assetpool, const char *name, const char *path)
{
    Music tmp;

    tmp.music = Mix_LoadMUS(path);

    if (!tmp.music)
    {
        DEBUG_PRINTF("AssetPool_loadMusic() failed for %s.\n", path);
        ASSERT(0);
    }
    return HashTable_Music_insert(&assetpool->musics, name, tmp);
}

void
AssetPool_clear(AssetPool *assetpool)
{
#ifdef _DEBUG
    int num_spritefonts_freed   = 0;
    int num_textures_freed      = 0;
#endif

    for (uint32_t i = 0; i < assetpool->spritefonts.capacity; ++i)
    {
        if (!assetpool->spritefonts.flags[i]) continue;

        if (assetpool->spritefonts.items[i].texture.id)
            glDeleteTextures(1, &assetpool->spritefonts.items[i].texture.id);

        #ifdef _DEBUG
        ++num_spritefonts_freed;
        #endif
    }

    for (uint32_t i = 0; i < assetpool->textures.capacity; ++i)
    {
        if (!assetpool->textures.flags[i]) continue;

        if (assetpool->textures.items[i].id)
            glDeleteTextures(1, &assetpool->textures.items[i].id);

        #ifdef _DEBUG
        ++num_textures_freed;
        #endif
    }

    /* TODO: CONTINUE WITH CLEARING MORE SHIT! */

    HashTable_SpriteFont_clear(&assetpool->spritefonts);
    HashTable_BaseMesh_clear(&assetpool->basemeshes);
    HashTable_Texture_clear(&assetpool->textures);
    HashTable_Material_clear(&assetpool->materials);
    HashTable_MaterialArray_clear(&assetpool->material_arrays);
    HashTable_SoundEffect_clear(&assetpool->soundeffects);
    HashTable_Music_clear(&assetpool->musics);
    HashTable_Model_clear(&assetpool->models);
}

int
Assets_init(Assets *assets)
{
    /* Fill in the base shape index array */
    for (uint i = 0; i < NUM_BASE_SHAPE_INDICES; ++i)
        _base_shape_indices[i] = i;

    ArrayList_Texture_init(&assets->textures,
        NUM_INITIAL_TEXTURE_ASSET_SLOTS, 1,
        getDynamicMemoryBlock());

    ArrayList_FontPtr_init(&assets->fonts,
        NUM_INITIAL_FONT_ASSET_SLOTS, 1,
        getDynamicMemoryBlock());

    ArrayList_SoundEffect_init(&assets->soundeffects,
        NUM_INITIAL_SOUNDEFFECT_ASSET_SLOTS, 1,
        getDynamicMemoryBlock());

    ArrayList_Music_init(&assets->musictracks,
        NUM_INITIAL_MUSIC_ASSET_SLOTS, 1,
        getDynamicMemoryBlock());

    ArrayList_Model_init(&assets->models, 
        NUM_INITIAL_MODEL_ASSET_SLOTS, 1, 
        getDynamicMemoryBlock());

    Vector_BaseMesh_init(&assets->base_meshes, NUM_INITIAL_TEXTURE_ASSET_SLOTS,
        getDynamicMemoryBlock());

    assets->mesh_load_pos = allocatePermanentArray(MAX_MODEL_VERTICES * 3,
        sizeof(float));
    assets->mesh_load_uv  = allocatePermanentArray(MAX_MODEL_VERTICES * 2,
        sizeof(float));
    assets->mesh_load_norm= allocatePermanentArray(MAX_MODEL_VERTICES * 3,
        sizeof(float));
    assets->vertex_buffer = allocatePermanentArray(MAX_MODEL_VERTICES,
        sizeof(Vertex));

    Mutex_init(&assets->mesh_mutex);

    /* Create the basic shapes meshes */
    BaseMesh_initStatically(&assets->mesh_cube,
        _cube_base_vertices, 36, _base_shape_indices, 36);
    Renderer_queueuBufferableBaseMesh(&engine.renderer, &assets->mesh_cube);

    BaseMesh_initStatically(&assets->mesh_triangle,
        _triangle_base_vertices, 3, _base_shape_indices, 3);
    Renderer_queueuBufferableBaseMesh(&engine.renderer, &assets->mesh_triangle);

    BaseMesh_initStatically(&assets->mesh_plane,
        _plane_base_vertices, 6, _base_shape_indices, 6);
    Renderer_queueuBufferableBaseMesh(&engine.renderer, &assets->mesh_plane);

    assets->mesh_ball = Assets_loadBaseMesh(assets, "assets/ball.nff");

    Model_init(&assets->model_cube, &assets->mesh_cube, 1);
    Model_init(&assets->model_triangle, &assets->mesh_triangle, 1);
    Model_init(&assets->model_plane, &assets->mesh_plane, 1);
    assets->model_ball = Assets_loadModel(assets, "assets/ball.nff");

    /* Fonts */
    assets->sprite_fonts = allocatePermanentArray(8, sizeof(SpriteFont));
    assets->num_sprite_fonts = 0;

    if (!assets->sprite_fonts)
    {
        assets->num_sprite_fonts = 0;
        assets->num_max_sprite_fonts = 0;
        DEBUG_PRINTF("Assets_init(): failed to allocate sprite fonts\n");
        return 1;
    }

    assets->num_max_sprite_fonts = 8;

    /* Default sprite font */
    {
        Color load_color = {255, 255, 255, 255};
        int ret = SpriteFont_init(&assets->default_sprite_font,
            "assets/fonts/munro/munro.ttf", 10, &load_color);

        if (ret != 0)
            DEBUG_PRINTF("Assets: Failed to load default sprite font!\n");
    }

    return 0;
}

Texture *
Assets_loadTexture(Assets *assets, const char *path)
{
    Texture *texture = ArrayList_Texture_pushEmpty(&assets->textures);

    SDL_Surface *bitmap = IMG_Load(path);

    if (!bitmap)
    {
        DEBUG_PRINTF("IMG_Load(): %s\n", IMG_GetError());
        return 0;
    }

	Texture_init(texture,
		GL_TEXTURE_2D, /* GL_TEXTURE_2D, GL_TEXTURE_CUBE_MAP, etc. */
		bitmap->pixels,
		GL_RGBA, /* for example, GL_RGBA */
		GL_UNSIGNED_BYTE, /* For example, GL_UNSIGNED_BYTE */
		GL_REPEAT, GL_REPEAT,
		bitmap->w, bitmap->h);

    SDL_FreeSurface(bitmap);

    return texture;
}

Texture *
Assets_loadCubemapTexture(Assets *assets,
    const char *path_pos_x, const char *path_neg_x,
    const char *path_pos_y, const char *path_neg_y,
    const char *path_pos_z, const char *path_neg_z)
{
    Texture *texture = ArrayList_Texture_pushEmpty(&assets->textures);

    const char *paths[6] = {path_pos_x, path_neg_x, 
        path_pos_y, path_neg_y, path_pos_z, path_neg_z};

    SDL_Surface *surfaces[6];
    int num_loaded = 0;

    for (int i = 0; i < 6; ++i)
    {
        surfaces[i] = IMG_Load(paths[i]);

        if (!surfaces[i])
        {
            DEBUG_PRINTF("Assets_loadCubemapTexture: failed "
                "to load image from path %s\n",
                paths[i]);

            for (SDL_Surface **surface = surfaces;
                 surface < &surfaces[num_loaded];
                 ++surface)
            {
                SDL_FreeSurface(*surface);
            }

            return 0;
        }

        ++num_loaded;
    }

    Texture_initAsCubemap(texture,
        surfaces[0]->pixels, surfaces[1]->pixels,
        surfaces[2]->pixels, surfaces[3]->pixels,
        surfaces[4]->pixels, surfaces[5]->pixels,
        GL_UNSIGNED_BYTE,
        surfaces[0]->format->BytesPerPixel == 4 ? GL_RGBA : GL_RGB, 
        GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE,
        surfaces[0]->w, surfaces[0]->h);

    for (SDL_Surface **surface = surfaces;
         surface < &surfaces[6];
         ++surface)
    {
        SDL_FreeSurface(*surface);
    }

    return texture;
}

Texture *
Assets_createTextureFromText(Assets *assets,
    Font *font, Color *color, const char *text, int wrap_width)
{
    SDL_Surface *bitmap = 0;
    Texture *texture    = 0;

    if (wrap_width > 0)
        bitmap = TTF_RenderText_Blended_Wrapped(font, text, *color, wrap_width);
    else
        bitmap = TTF_RenderText_Blended(font, text, *color);

    if (bitmap)
    {
        texture = ArrayList_Texture_pushEmpty(&assets->textures);

        glGenTextures(1, &texture->id);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture->id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
            bitmap->w,
            bitmap->h,
            0, GL_RGBA, GL_UNSIGNED_BYTE,
            bitmap->pixels);
        glBindTexture(GL_TEXTURE_2D, 0);
        texture->width  = bitmap->w;
        texture->height = bitmap->h;
        SDL_FreeSurface(bitmap);
    }
    else
    {
        DEBUG_PRINTF("TTF_RenderText_Blended(): %s\n", TTF_GetError());
    }

    return texture;
}

Texture *
Assets_updateTextureFromText(Assets *assets,
    Texture *texture,
    Font * font, Color *color,
    const char *text, int wrap_width)
{
    /* Note: There seems to be a memory leak here,
     * but I can't figure out, why */
    ASSERT(texture);

    if (texture->id)
        glDeleteTextures(1, &texture->id);

    SDL_Surface *bitmap;

    if (wrap_width > 0)
        bitmap = TTF_RenderText_Blended_Wrapped(font, text, *color, wrap_width);
    else
        bitmap = TTF_RenderText_Blended(font, text, *color);

    if (bitmap)
    {
        glGenTextures(1, &texture->id);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture->id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
            bitmap->w,
            bitmap->h,
            0, GL_RGBA, GL_UNSIGNED_BYTE,
            bitmap->pixels);
        glBindTexture(GL_TEXTURE_2D, 0);
        texture->width  = bitmap->w;
        texture->height = bitmap->h;
        SDL_FreeSurface(bitmap);
    }
    else
    {
        DEBUG_PRINTF("TTF_RenderText_Blended(): %s\n", TTF_GetError());
    }

    return texture;
}

BaseMesh *
Assets_loadBaseMesh(Assets *assets, const char *path)
{
    GLfloat *vertices;
    GLuint *indices;
    uint num_vertices;
    uint num_indices;

    if (loadSingleBaseMeshInternal(path,
        &vertices, &num_vertices,
        &indices,  &num_indices,
        0, MESH_ALLOC_DYNAMIC) != 0)
    {
        /* TODO: FREE INDICES AND VERTICES */
        DEBUG_PRINTF("Failed to load base mesh from path %s.\n", path);
        return 0;
    }

    BaseMesh *ret = Vector_BaseMesh_pushEmpty(&assets->base_meshes);
    BaseMesh_initFromExistingAlloc(ret,
        vertices, num_vertices,
        indices, num_indices);
    ret->indices = indices;
    ret->num_indices = num_indices;

    Renderer_queueuBufferableBaseMesh(&engine.renderer, ret);

    return ret;
}

Model *
Assets_loadModel(Assets *assets, const char *path)
{
    const struct aiScene *scene = aiImportFile(path,
        aiProcess_CalcTangentSpace |
        aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices |
        aiProcess_GenNormals |
        aiProcess_FlipUVs |
        aiProcess_SortByPType);

    /* TODO: the rest :) */
    if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        DEBUG_PRINTF("Failed to load Assimp scene: incomplete! %s. \n", path);
        aiReleaseImport(scene);
        return 0;
    }

    uint num_meshes = 0;
    BaseMesh meshes[MAX_MESHES_PER_MODEL];

    struct aiNode *node = scene->mRootNode;
    processNodeGlobal(meshes, &num_meshes, scene, node);

    BaseMesh *allocated_meshes = kmalloc(num_meshes * sizeof(BaseMesh));

    if (!allocated_meshes)
    {
        DEBUG_PRINTF("Assets_loadModel(): failed to allocate meshes for %s!\n",
            path);
        aiReleaseImport(scene);
        return 0;
    }

    memcpy(allocated_meshes, meshes, num_meshes * sizeof(BaseMesh));

    Model *model = ArrayList_Model_pushEmpty(&assets->models);

    if (!model)
    {
        DEBUG_PRINTF("Assets_loadModel(): failed to push model into arraylist.\n");
        aiReleaseImport(scene);
        return 0;
    }

    Model_init(model, allocated_meshes, num_meshes);

    if (num_meshes == 0)
    {
        kfree(allocated_meshes);
        aiReleaseImport(scene);
        return 0;
    }

	for (uint i = 0; i < model->num_meshes; ++i)
        Renderer_queueuBufferableBaseMesh(&engine.renderer, &model->meshes[i]);

	aiReleaseImport(scene);

    return model;
}

static void
processNodeGlobal(BaseMesh *meshes, uint *num_meshes,
    const struct aiScene *scene, struct aiNode *node)
{
    for (uint i = 0; i < node->mNumMeshes; i++)
    {
        ASSERT((*num_meshes) < MAX_MESHES_PER_MODEL);

        struct aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        GLfloat *vertices   = kmalloc(mesh->mNumVertices * SIZE_OF_VERTEX);

        if (!vertices)
        {
            DEBUG_PRINTF("processNode(): failed to load model mesh: out of "
                "memory!\n");
            return;
        }

        struct MeshLoadData load_data = {mesh, vertices};

        if (mesh->mTextureCoords[0])
            createThreadedJob(loadAssimpMeshUVs, &load_data);

        if (mesh->mNormals)
            createThreadedJob(loadAssimpMeshNormals, &load_data);

        for (uint i = 0; i < mesh->mNumVertices; i++)
        {
            vertices[i * 8 + 0] = mesh->mVertices[i].x;
            vertices[i * 8 + 1] = mesh->mVertices[i].y;
            vertices[i * 8 + 2] = mesh->mVertices[i].z;

#if 0
            if (mesh->mTextureCoords[0])
            {
                vertices[i * 8 + 3] = mesh->mTextureCoords[0][i].x;
                vertices[i * 8 + 4] = mesh->mTextureCoords[0][i].y;
            }

            if (mesh->mNormals)
            {
                vertices[i * 8 + 5] = mesh->mNormals[i].x;
                vertices[i * 8 + 6] = mesh->mNormals[i].y;
                vertices[i * 8 + 7] = mesh->mNormals[i].z;
            }
#endif
        }

        while (ThreadPool_haveUnfinishedWork(&engine.thread_pool));

        uint num_indices = 0;

        for (struct aiFace *face = mesh->mFaces;
             face < &mesh->mFaces[mesh->mNumFaces];
             ++face)
        {
            num_indices += face->mNumIndices;
        }


        GLuint *indices = (GLuint*)kmalloc(num_indices * sizeof(GLuint));


        if (!indices)
        {
            DEBUG_PRINTF("processNode(): failed to load indices for "
                "model mesh: out of memory!\n");
            return;
        }

        uint cnt = 0;

        /*Read indices*/
        for (uint i = 0; i < mesh->mNumFaces; i++)
            for (uint j = 0; j < mesh->mFaces[i].mNumIndices; ++j)
                indices[cnt++] = (GLuint)mesh->mFaces[i].mIndices[j];

        BaseMesh_initFromExistingAlloc(&meshes[*num_meshes],
            vertices, mesh->mNumVertices,
            indices,  num_indices);

        (*num_meshes)++;
    }

    for (uint i = 0; i < node->mNumChildren; i++)
        processNodeGlobal(meshes, num_meshes, scene, node->mChildren[i]);
}

static void
processNodeAssetPool(AssetPool *assetpool, BaseMesh *meshes, uint *num_meshes,
    const struct aiScene *scene, struct aiNode *node)
{
    for (uint i = 0; i < node->mNumMeshes; i++)
    {
        ASSERT((*num_meshes) < MAX_MESHES_PER_MODEL);

        struct aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        GLfloat *vertices = SimpleMemoryBlock_malloc(&assetpool->memory,
            mesh->mNumVertices * SIZE_OF_VERTEX);

        if (!vertices)
        {
            DEBUG_PRINTF("processNode(): failed to load model mesh: out of memory!\n");
            return;
        }

        for (uint i = 0; i < mesh->mNumVertices; i++)
        {
            vertices[i * 8 + 0] = mesh->mVertices[i].x;
            vertices[i * 8 + 1] = mesh->mVertices[i].y;
            vertices[i * 8 + 2] = mesh->mVertices[i].z;

            if (mesh->mTextureCoords[0])
            {
                vertices[i * 8 + 3] = mesh->mTextureCoords[0][i].x;
                vertices[i * 8 + 4] = mesh->mTextureCoords[0][i].y;
            }

            if (mesh->mNormals)
            {
                vertices[i * 8 + 5] = mesh->mNormals[i].x;
                vertices[i * 8 + 6] = mesh->mNormals[i].y;
                vertices[i * 8 + 7] = mesh->mNormals[i].z;
            }
        }

        uint num_indices = 0;

        for (struct aiFace *face = mesh->mFaces;
            face < &mesh->mFaces[mesh->mNumFaces];
            ++face)
        {
            num_indices += face->mNumIndices;
        }

        GLuint *indices = (GLuint*)SimpleMemoryBlock_malloc(&assetpool->memory,
            num_indices * sizeof(GLuint));

        if (!indices)
        {
            DEBUG_PRINTF("processNode(): failed to load indices for "
                "model mesh: out of memory!\n");
            return;
        }

        uint cnt = 0;

        /*Read indices*/
        for (uint i = 0; i < mesh->mNumFaces; i++)
            for (uint j = 0; j < mesh->mFaces[i].mNumIndices; ++j)
                indices[cnt++] = (GLuint)mesh->mFaces[i].mIndices[j];

        BaseMesh_initFromExistingAlloc(&meshes[*num_meshes],
            vertices, mesh->mNumVertices,
            indices, num_indices);

        (*num_meshes)++;
    }

    for (uint i = 0; i < node->mNumChildren; i++)
    {
        processNodeAssetPool(assetpool, meshes, num_meshes, scene,
            node->mChildren[i]);
    }
}

Font *
Assets_loadFont(Assets *assets, const char *path, int size)
{
    ASSERT(size > 0);
    TTF_Font *font = *ArrayList_FontPtr_pushEmpty(&assets->fonts);
    font = TTF_OpenFont(path, size);

    if (!font)
    {
        DEBUG_PRINTF("TTF_OpenFont(): %s\n", TTF_GetError());
    }

    return font;
}

SpriteFont *
Assets_loadSpriteFont(Assets *assets,
    const char *path,
    uint size, Color color)
{
    if (assets->num_sprite_fonts >= assets->num_max_sprite_fonts)
        return 0;

    int ret = SpriteFont_init(&assets->sprite_fonts[assets->num_sprite_fonts],
        path, size, &color);

    if (ret != 0)
    {
        DEBUG_PRINTF("Assets_loadSpriteFont() failed for %s.\n", path);
        return 0;
    }

    return &assets->sprite_fonts[assets->num_sprite_fonts++];
}

GLfloat _cube_base_vertices[] =
{
     0.5f,  0.5f, -0.5f,  1.0f,  1.0f,  0.0f,  0.0f, -1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f,  0.0f,  0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f,  0.0f,  0.0f, -1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, -1.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  1.0f,  0.0f,  0.0f, -1.0f,

     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  0.0f,  0.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  1.0f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f,  0.0f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f,  0.0f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  1.0f, -1.0f,  0.0f,  0.0f,

     0.5f, -0.5f, -0.5f,  0.0f,  1.0f,  1.0f,  0.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  1.0f,  0.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  0.0f,  1.0f,  1.0f,  0.0f,  0.0f,

     0.5f, -0.5f, -0.5f,  1.0f,  1.0f,  0.0f, -1.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  1.0f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  1.0f,  0.0f, -1.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f, -1.0f,  0.0f,

     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  1.0f,  1.0f,  1.0f,  0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  1.0f,  1.0f,  0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  1.0f,  1.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f
};

GLfloat _triangle_base_vertices[] =
{
    -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
};

GLfloat _plane_base_vertices[] =
{
    -0.5f, -0.5f, 0.0f,  0.0f,  0.0f,  0.0f,  0.0f, -1.0f,
     0.5f, -0.5f, 0.0f,  1.0f,  0.0f,  0.0f,  0.0f, -1.0f,
     0.5f,  0.5f, 0.0f,  1.0f,  1.0f,  0.0f,  0.0f, -1.0f,
     0.5f,  0.5f, 0.0f,  1.0f,  1.0f,  0.0f,  0.0f, -1.0f,
    -0.5f,  0.5f, 0.0f,  0.0f,  1.0f,  0.0f,  0.0f, -1.0f,
    -0.5f, -0.5f, 0.0f,  0.0f,  0.0f,  0.0f,  0.0f, -1.0f
};

SoundEffect *
Assets_loadSoundEffect(Assets *assets, const char *path)
{
    if (!engine.audio_system.isInitialized) return 0;

    SoundEffect *effect = ArrayList_SoundEffect_pushEmpty(&assets->soundeffects);

    effect->chunk = Mix_LoadWAV(path);

    if(!effect->chunk)
    {
        DEBUG_PRINTF("Assets_loadSoundEffect() failed for %s.\n", path);
    }
    return effect;
}

Music *
Assets_loadMusic(Assets *assets, const char *path)
{
    Music *track = ArrayList_Music_pushEmpty(&assets->musictracks);

    track->music = Mix_LoadMUS(path);

    if (!track->music)
    {
        DEBUG_PRINTF("Assets_loadMusic() failed for %s.\n", path);
        ASSERT(0);
    }
    return track;
}

#if 0
LEGACY MODEL LOADING

/* Returns a pointer to the temporary storage, and the number of verticees stored
* to variable pointed to by num_vertices */
static float *
Assets_loadBaseMeshToTempStorage(Assets *assets, const char *path, uint *num_vertices)
{
    FILE *file = fopen(path, "r");

    if (!file)
        return 0;

    bool32 have_errors = 0;
    char line_buf[256];

    uint pos_indices[3];
    uint uv_indices[3];
    uint norm_indices[3];

    uint ipos = 0;
    uint iuv = 0;
    uint inorm = 0;
    uint ifaces = 0;

    for (int r = fscanf(file, "%s", line_buf);
        r != EOF;
        r = fscanf(file, "%s", line_buf))
    {
        if (!strcmp(line_buf, "v"))
        {
            fscanf(file, "%f %f %f\n", &assets->mesh_load_pos[ipos][0],
                &assets->mesh_load_pos[ipos][1], &assets->mesh_load_pos[ipos][2]);
            ++ipos;
        }
        else if (!strcmp(line_buf, "vt"))
        {
            fscanf(file, "%f %f\n", &assets->mesh_load_uv[iuv][0], &assets->mesh_load_uv[iuv][1]);
            ++iuv;
        }
        else if (!strcmp(line_buf, "vn"))
        {
            fscanf(file, "%f %f, %f\n", &assets->mesh_load_norm[inorm][0],
                &assets->mesh_load_norm[inorm][1], &assets->mesh_load_norm[inorm][2]);
            ++inorm;
        }
        else if (!strcmp(line_buf, "f"))
        {
            fscanf(file, "%i/%i/%i %i/%i/%i %i/%i/%i",
                &pos_indices[0], &uv_indices[0], &norm_indices[0],
                &pos_indices[1], &uv_indices[1], &norm_indices[1],
                &pos_indices[2], &uv_indices[2], &norm_indices[2]);

            GLfloat *face = &assets->vertex_buffer[ifaces * 8 * 3];

            vec3 *p1 = &assets->mesh_load_pos[pos_indices[0] - 1];
            vec3_copy(*(vec3*)&(face[0]), *p1);

            vec2 *u1 = &assets->mesh_load_uv[uv_indices[0] - 1];
            vec2_copy(*(vec2*)&(face[3]), *u1);

            vec3 *n1 = &assets->mesh_load_norm[norm_indices[0] - 1];
            vec3_copy(*(vec3*)&(face[5]), *n1);

            vec3 *p2 = &assets->mesh_load_pos[pos_indices[1] - 1];
            vec3_copy(*(vec3*)&(face[8]), *p2);

            vec2 *u2 = &assets->mesh_load_uv[uv_indices[1] - 1];
            vec2_copy(*(vec2*)&(face[11]), *u2);

            vec3 *n2 = &assets->mesh_load_norm[norm_indices[1] - 1];
            vec3_copy(*(vec3*)&(face[13]), *n2);

            vec3 *p3 = &assets->mesh_load_pos[pos_indices[2] - 1];
            vec3_copy(*(vec3*)&(face[16]), *p3);

            vec2 *u3 = &assets->mesh_load_uv[uv_indices[2] - 1];
            vec2_copy(*(vec2*)&(face[19]), *u3);

            vec3 *n3 = &assets->mesh_load_norm[norm_indices[2] - 1];
            vec3_copy(*(vec3*)&(face[21]), *n3);

            ++ifaces;
        }

        if (ipos  > MAX_MODEL_VERTICES * 3
            || iuv   > MAX_MODEL_VERTICES * 2
            || inorm > MAX_MODEL_VERTICES * 3
            || ifaces > MAX_MODEL_VERTICES / 3)
        {
            have_errors = 1;
            break;
        }
    }

    fclose(file);

    if (have_errors)
    {
        DEBUG_PRINTF("Failed to load mesh from file %s: too many vertices!\n", path);
        return 0;
    }

    *num_vertices = 3 * ifaces;
    return assets->vertex_buffer;
}

BaseMesh *
Assets_loadBaseMesh(Assets *assets, const char *path)
{
    uint num_vertices;
    float *vertices = Assets_loadBaseMeshToTempStorage(
        assets, path, &num_vertices);

    if (vertices)
    {
        GLfloat *vertex_storage = kmalloc(num_vertices * sizeof(Vertex));
        BaseMesh *mesh = Vector_BaseMesh_pushEmpty(&assets->base_meshes);
        BaseMesh_init(mesh, vertex_storage, vertices, num_vertices, 0);
        return mesh;
    }
    else
    {
        return 0;
    }
}
#endif
