#pragma once
#include <GL/glew.h>
#include <stdlib.h>
#include <string.h>

typedef struct TextureBufferItem    TextureBufferItem;
typedef struct SpriteBatch          SpriteBatch;
typedef struct SpriteBuffer         SpriteBuffer;
typedef struct SpriteBufferList     SpriteBufferList;

#define FLOATS_PER_SPRITE 32

/* Forward declaration(s) */
typedef struct Texture Texture;

extern Texture *_BLANK_WHITE_TEXTURE;

typedef enum SpriteFlip
{
     SPRITE_FLIP_NONE,
     SPRITE_FLIP_HORIZONTAL,
     SPRITE_FLIP_VERTICAL,
     SPRITE_FLIP_BOTH
} SpriteFlip;

struct TextureBufferItem
{
    GLuint id;
    unsigned int repeats;
};

struct SpriteBatch
{
    GLuint              program;
    GLuint              vbo;
    GLuint              vao;
    GLuint              ebo;
    GLint               proj_loc;
    GLint               scale_loc;

    GLfloat             *vertex_buffer;
    TextureBufferItem   *texture_buffer;
    unsigned int        buffer_size;

    unsigned int        sprite_count;
    unsigned int        tex_swap_count;

    GLfloat             scale_x;
    GLfloat             scale_y;
};

struct SpriteBuffer
{
    unsigned int        sprite_count;
    unsigned int        tex_swap_count;
    GLfloat             *vertex_buffer;
    TextureBufferItem   *texture_buffer;
};

struct SpriteBufferList
{
    SpriteBuffer buffer;
    SpriteBufferList *next;
};

/* Return 0 on success */
int
SpriteBatch_init(SpriteBatch *batch, GLchar *error_log, GLint log_len);

void
SpriteBatch_dispose(SpriteBatch *batch);

/* Note: setting clip to 0 is allowed to draw a full texture */
void
SpriteBatch_drawSprite(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip);

void
SpriteBatch_drawSprite_Flip(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, SpriteFlip flip);

void
SpriteBatch_drawSprite_Scale(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float scale_x, float scale_y);

void
SpriteBatch_drawSprite_Rotate(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float angle);

void
SpriteBatch_drawSprite_Color(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float *color);

void
SpriteBatch_drawSprite_FlipScale(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, SpriteFlip flip, float scale_x, float scale_y);

void
SpriteBatch_drawSprite_FlipRotate(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, SpriteFlip flip, float angle);

void
SpriteBatch_drawSprite_FlipColor(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, SpriteFlip flip, float *color);

void
SpriteBatch_drawSprite_ScaleRotate(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float scale_x, float scale_y, float angle);

void
SpriteBatch_drawSprite_ScaleColor(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip, float scale_x, float scale_y,
    float *color);

void
SpriteBatch_drawSprite_FlipScaleRotate(SpriteBatch *batch, Texture *texture,
    float x, float y, unsigned int *clip,
    SpriteFlip flip, float scale_x, float scale_y, float angle);

void
SpriteBatch_drawRect(SpriteBatch *batch, float x, float y, float w, float h, float *color);

void
SpriteBatch_flush(SpriteBatch *batch, int *viewport);

void
SpriteBatch_flushMultipleBuffers(SpriteBatch *batch, SpriteBufferList *list, int *viewport);
