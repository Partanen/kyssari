#pragma once

typedef struct SpriteFontChar   SpriteFontChar;
typedef struct SpriteFont       SpriteFont;

struct SpriteFontChar
{
    float   advance;
    float   min_x;
    uint    clip[4];
};

struct SpriteFont
{
    float           height;
    Texture         texture;
    SpriteFontChar  characters[256];
};

int
SpriteFont_init(SpriteFont *font,
    const char *ttf_font_path,
    uint font_size,
    Color *color);

static inline float
SpriteFont_getStringWidth(SpriteFont *font, const char *str)
{
    float w = 0;

    for (const char *c = str; *c; ++c)
        w += font->characters[(int)*c].advance;

    return w;
}

static inline float
SpriteFont_getStringWidthN(SpriteFont *font, const char *str, uint num_max_chars)
{
    float w = 0;

    for (uint i = 0; i < num_max_chars && str[i]; ++i)
        w += font->characters[i].advance;

    return w;
}
