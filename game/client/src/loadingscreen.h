
typedef struct LoadingScreenData LoadingScreenData;

#define INITIAL_LOADING_SCREEN_MIN_DUR  0.0f
#define NONINIT_LOADING_SCREEN_MIN_DUR  1.5 /* Seconds */
#define LOAD_ANIM_FRAME_LEN             0.080f

struct LoadingScreenData
{
    int         percent_complete;

    /* The loading screen has some assets loaded before all other assets */
    Texture     *tex_kys_load_anim;
    int         load_anim_clips[8][4];
    double      load_anim_timer;
    int         load_anim_current_clip;

    Texture     *tex_i_want_to_believe;
    Texture     *tex_dark_layer;
    Texture     *tex_bar;

    int         tip_of_the_day;
    double      time_passed;
};

int
createLoadingScreen();

void
LoadingScreen_open(Screen *screen);

void
LoadingScreen_close(Screen *screen);

void
LoadingScreen_update(Screen *screen);

